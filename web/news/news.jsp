<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<!-- News & Events & Single Event CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/news_events_style.css" rel="stylesheet">


<%

    Session dbsessionNews = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsessionNews.beginTransaction();

    // DateFormat dateFormatNews = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatNews = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateFormatNewsM = new SimpleDateFormat("MMM");
    DateFormat dateFormatNewsD = new SimpleDateFormat("dd");
    DateFormat dateFormatNewsY = new SimpleDateFormat("Y");
    Date dateNewsM = null;
    Date dateNewsD = null;
    Date dateNewsY = null;
    String newDateNewsM = "";
    String newDateNewsD = "";
    String newDateNewsY = "";

    Query newsSQL = null;
    Object newsObj[] = null;
    String newsId = "";
    String newsTitle = "";
    String newsSlug = "";
    String newsShortDetails = "";
    String newsShortDetails1 = "";
    String newsShortDetailsX = "";
    String newsDetails = "";
    String newsDetails1 = "";
    String newsDetailsX = "";
    String newsDateTime = "";
    String featureImage = "";
    String featureImage1 = "";
    String featureImageUrl = "";
    String topNewsContainer = "";
    String topConClass = "";
    String topConClass1 = "";
    String topConClass2 = "";
    String topConClass3 = "";
    String bottomNewsContainer = "";
    String newsDetailsUrl = "";
    int nw = 1;

    newsSQL = dbsessionNews.createSQLQuery("SELECT ni.id_news, ni.news_title, ni.news_short_desc, ni.news_desc, "
            + "ni.news_date, ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4 "
            + "FROM  news_info ni ORDER BY ni.id_news DESC "
            + "LIMIT 0,10");

    if (!newsSQL.list().isEmpty()) {
        for (Iterator it1 = newsSQL.list().iterator(); it1.hasNext();) {

            newsObj = (Object[]) it1.next();
            newsId = newsObj[0].toString().trim();
            newsTitle = newsObj[1].toString().trim();
            newsShortDetails = newsObj[2].toString().trim();

            newsShortDetailsX = newsShortDetails.substring(0, 100);

            //    System.out.println("newsShortDetailsX 0,200:: " + newsShortDetailsX);
            newsShortDetails1 = newsShortDetails;

            newsDetails = newsObj[3].toString().trim();

            //  newsDetails1 = Jsoup.parse(newsDetails).text();
            newsDetails1 = newsDetails.replaceAll("\\<.*?>", "");

            newsDetailsX = newsDetails1.substring(0, 100);

            newsDateTime = newsObj[4].toString().trim();

            dateNewsM = dateFormatNews.parse(newsDateTime);
            newDateNewsM = dateFormatNewsM.format(dateNewsM);

            dateNewsD = dateFormatNews.parse(newsDateTime);
            newDateNewsD = dateFormatNewsD.format(dateNewsD);

            dateNewsY = dateFormatNews.parse(newsDateTime);
            newDateNewsY = dateFormatNewsY.format(dateNewsY);

            featureImage = newsObj[5] == null ? "" : newsObj[5].toString().trim();
            featureImageUrl = GlobalVariable.imageNewsDirLink + featureImage;

            featureImage1 = "<img width=\"200\" src=\"" + featureImageUrl + "\" alt=\"" + newsTitle + "\">";

            newsDetailsUrl = GlobalVariable.baseUrl + "/news/newsDetails.jsp?newsIdX=" + newsId;

            //       String updateProfileUrl = GlobalVariable.baseUrl + "/newsManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&newsIdX=" + newsId + "&selectedTab=profile";
            //      String updateChangePassUrl = GlobalVariable.baseUrl + "/newsManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&newsIdX=" + newsId + "&selectedTab=changePass";
          //  System.out.println("nw:: " + nw);

            bottomNewsContainer = bottomNewsContainer + ""
                    + "<div class=\"col-md-4\">"
                    + "<div class=\"card\">"
                    + "<img height=\"215\" src=\" " + featureImageUrl + "\" class=\"card-img-top\" alt=\"" + newsTitle + "\">"
                    + "<div class=\"card-body\" style=\"min-height:310px;\">"
                    + "<p class=\"news_date\"> " + newDateNewsD + " " + newDateNewsM + ", " + newDateNewsY + "</p>"
                    + "<h5 class=\"card-title my-3\">" + newsTitle + "</h5>"
                    + "<p class=\"card-text mb-3\">" + newsDetailsX + "</p>"
                    + "<a href=\"" + newsDetailsUrl + "\" class=\"btn btn-outline-success border-dark details-link\">View Details</a>"
                    + "</div>"
                    + "</div>"
                    + "</div>";

            if (nw % 3 == 0) {
                bottomNewsContainer = bottomNewsContainer + "</div><div class=\"row my-3\">";
            }

            nw++;
        }
    }
%>

<!-- Start of navigation -->
<section class="navigation_partho parallaxie text-center" style="height:150px;background:url('<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/News-Banner.jpg');position:relative;">
    <div style="position:absolute;top:0;left:0;width:100%;height:100%;background:rgba(0,0,0,0.34);">
        <h3 class="h1 pl-5 mx-5 font-weight-bold text-white pt-5">IEB News</h3>
        <nav aria-label="breadcrumb" class="w-100 pl-5">
            <ol class="breadcrumb h5" style="background-color: #e9ecef00" >
                <li class="breadcrumb-item"><a href="home_v4.html">Home</a></li>
                <li class="breadcrumb-item"><a href="home_v4.html">News & Events</a></li>
                <li class="breadcrumb-item active text-white" aria-current="page" >News</li>
            </ol>
        </nav>
    </div>
</section>
<!-- End of navigation -->

<

<!-- Start of more news section -->
<section class="other-news1 m-5">
    <div class="container">
        
        <div class="row">
            <%=bottomNewsContainer%>
        </div>       
    </div>
</section>
<!-- End of more news section -->

<!-- Start of Pagination -->
<nav class="other-news news-pagination" style="display:none;">
    <ul class="pagination justify-content-center pagination-lg">
        <li class="page-item disabled">
            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
        </li>
        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
            <a class="page-link" href="#">Next</a>
        </li>
    </ul>
</nav>
<!-- End of Pagination -->

<%
    dbsessionNews.clear();
    dbsessionNews.close();
%>

<%@ include file="../footer.jsp" %>
