<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Query"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="../header.jsp" %>

<%
    Session dbsessionNews = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsessionNews.beginTransaction();

    String newsIdX = request.getParameter("newsIdX") == null ? "" : request.getParameter("newsIdX").trim();

    // DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateFormatEventM = new SimpleDateFormat("MMM");
    DateFormat dateFormatEventD = new SimpleDateFormat("dd");
    DateFormat dateFormatEventY = new SimpleDateFormat("Y");

    Date dateNewsM = null;
    Date dateNewsD = null;
    Date dateNewsY = null;
    String newDateNewsM = "";
    String newDateNewsD = "";
    String newDateNewsY = "";

    Query newsSQL = null;
    Object newsObj[] = null;
    String newsId = "";
    String newsTitle = "";
    String newsSlug = "";
    String newsShortDetails = "";
    String newsShortDetails1 = "";
    String newsShortDetailsX = "";
    String newsDetails = "";
    String newsDateTime = "";
    String featureImage = "";
    String featureImage1 = "";
    String featureImageUrl = "";
    String newsInfoFirstContainer = "";
    String newsInfoContainer = "";
    String newsDetailsUrl = "";
    int nw = 1;

    newsSQL = dbsessionNews.createSQLQuery("SELECT ni.id_news, ni.news_title, ni.news_short_desc, ni.news_desc, "
            + "ni.news_date, ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4  "
            + "FROM  news_info ni "
            + "WHERE ni.id_news ='" + newsIdX + "'");

    if (!newsSQL.list().isEmpty()) {
        for (Iterator itNews = newsSQL.list().iterator(); itNews.hasNext();) {

            newsObj = (Object[]) itNews.next();
            newsId = newsObj[0].toString().trim();
            newsTitle = newsObj[1].toString().trim();
            newsShortDetails = newsObj[2].toString().trim();

            // newsShortDetailsX    = newsShortDetails.substring(0, 100);
            //      System.out.println(" newsShortDetailsX0,200:: " +newsShortDetailsX );
            newsShortDetails1 = newsShortDetails;

            newsDetails = newsObj[3].toString().trim();

            //   newsDateTime = newsObj[4] == null ? "" : newsObj[4].toString().trim();
            //  if (newsDateTime.equals("")) {
            // newsDateTime = newsObj[10].toString().trim();
            newsDateTime = newsObj[4] == null ? "" : newsObj[4].toString().trim();
          //  System.out.println(" newsDateTime :: " + newsDateTime);
            //   }

            dateNewsM = dateFormatEvent.parse(newsDateTime);
            newDateNewsM = dateFormatEventM.format(dateNewsM);

            dateNewsD = dateFormatEvent.parse(newsDateTime);
            newDateNewsD = dateFormatEventD.format(dateNewsD);

            dateNewsY = dateFormatEvent.parse(newsDateTime);
            newDateNewsY = dateFormatEventY.format(dateNewsY);

            featureImage = newsObj[5] == null ? "" : newsObj[5].toString().trim();
            featureImageUrl = GlobalVariable.imageNewsDirLink + featureImage;

            featureImage1 = "<img width=\"700\" src=\"" + featureImageUrl + "\" alt=\"" + newsTitle + "\">";

            newsDetailsUrl = GlobalVariable.baseUrl + "/news/newsDetails.jsp?newsIdX=" + newsId;
        }
    }

%>

<!-- Start of navigation -->
<section class="navigation_partho parallaxie text-center" style="height:150px;background:url('<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/News-Banner.jpg');position:relative;">
    <div style="position:absolute;top:0;left:0;width:100%;height:100%;background:rgba(0,0,0,0.34);">
        <h3 class="h1 pl-5 mx-5 font-weight-bold text-white pt-5">News</h3>
        <nav aria-label="breadcrumb" class="w-100 pl-5">
            <ol class="breadcrumb h5" style="background-color: #e9ecef00" >
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">News & Events</a></li>
                <li class="breadcrumb-item active text-white" aria-current="page" >News details</li>
            </ol>
        </nav>
    </div>
</section>
<!-- End of navigation -->

<!-- News front image section -->
<!--<section class="news-banner">
    <div class="conainer">
        <img src="<%//out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/success_story_img_1.jpg" class="img-fluid banner-img" alt="Front Image">
    </div>
</section>-->
<!-- News front image section -->

<!-- Start of details of news -->
<section class="news-single-content pt-0">
    <div class="container">        
        <div class="row">
            <div class="col-8">
                <div class="card my-3" style="padding: 15px;">
                    <h3 class="display-422"><%=newsTitle%></h3>
                    <p class="news_date"> <%=newDateNewsD%> <%=newDateNewsM%> <%=newDateNewsY%></p>
                    <div class="news-content">
                        <br>
                        <h3><%=featureImage1%></h3>
                        <br>
                        <%=newsDetails%>

                    </div>
                </div>
            </div>
            <div class="col-md-3 d-none d-xl-block">


                <div class="card my-3">
                    <div class="card-header text-warning h5 font-weight-bold">
                        New Archive <i class="fas fa-angle-double-down float-right" style="color:rgb(255,0,0);cursor:pointer;"></i>
                    </div>
                    <ul class="list-group list-group-flush">
                        <%
                            Query newsArchiveSQL = null;
                            Object newsArchiveObj[] = null;

                            String newsArchiveId = "";
                            String newsArchiveTitle = "";
                            String newsArchiveDetailsUrl = "";

                            newsArchiveSQL = dbsessionNews.createSQLQuery("SELECT ni.id_news, ni.news_title,"
                                    + "ni.news_date, ni.feature_image "
                                    + "FROM  news_info ni "
                                    + "WHERE  ni.id_news !='" + newsIdX + "' "
                                    + "ORDER BY ni.id_news ASC "
                                    + "LIMIT 0,10");

                            if (!newsArchiveSQL.list().isEmpty()) {
                                for (Iterator itNewsMore = newsArchiveSQL.list().iterator(); itNewsMore.hasNext();) {

                                    newsArchiveObj = (Object[]) itNewsMore.next();
                                    newsArchiveId = newsArchiveObj[0].toString().trim();
                                    newsArchiveTitle = newsArchiveObj[1].toString().trim();

                                    newsArchiveDetailsUrl = GlobalVariable.baseUrl + "/news/newsDetails.jsp?newsIdX=" + newsArchiveId;
                        %>


                        <a href="<%=newsArchiveDetailsUrl%>" ><li class="list-group-item text-wrap" ><%=newsArchiveTitle%></li></a>
                                <%
                                        }
                                    }
                                %>
                    </ul>
                </div>


            </div>
        </div>
    </div>
</section>
<!-- End of details of news -->

<!-- Start of more news section -->
<section>
    <div class="container">
        <div class="post-heading pt-5">
            <h3 class="text-uppercase font-weight-bold">More News </h3>
            <hr class="custom-horizontal-border mr-5" />
        </div>
        <div class="row mb-5">
            <%
                Query newsMoreSQL = null;
                Object newsMoreObj[] = null;

                String newsMoreId = "";
                String newsMoreTitle = "";

                String newsMoreFeatureImage = "";
                String newsMoreFeatureImageUrl = "";
                String newsMoreFeatureImage1 = "";
                String newsMoreDetailsUrl = "";

                newsMoreSQL = dbsessionNews.createSQLQuery("SELECT ni.id_news, ni.news_title,"
                        + "ni.news_date, ni.feature_image "
                        + "FROM  news_info ni "
                        + "WHERE  ni.id_news !='" + newsIdX + "' "
                        + "ORDER BY ni.id_news DESC "
                        + "LIMIT 0,4");

                if (!newsMoreSQL.list().isEmpty()) {
                    for (Iterator itNewsMore = newsMoreSQL.list().iterator(); itNewsMore.hasNext();) {

                        newsMoreObj = (Object[]) itNewsMore.next();
                        newsMoreId = newsMoreObj[0].toString().trim();
                        newsMoreTitle = newsMoreObj[1].toString().trim();

                        newsMoreFeatureImage = newsMoreObj[3] == null ? "" : newsMoreObj[3].toString().trim();
                        newsMoreFeatureImageUrl = GlobalVariable.imageNewsDirLink + newsMoreFeatureImage;

                        newsMoreFeatureImage1 = "<img width=\"200\" src=\"" + newsMoreFeatureImageUrl + "\" alt=\"" + newsMoreTitle + "\" class=\"img-fluid news-image  mb-3\" >";

                        newsMoreDetailsUrl = GlobalVariable.baseUrl + "/news/newsDetails.jsp?newsIdX=" + newsMoreId;
            %>

            <div class="col-md-3 col-sm-6">
                <div class="post-content pt-3">
                    <div class="post-content-img">
                        <%=newsMoreFeatureImage1%>
                        <div class="post-content-img-overlay d-flex align-items-center justify-content-center" style="height: 50%;"></div>
                        <a href="<%=newsMoreDetailsUrl%>" class="text-primary font-weight-bold news-link"><%=newsMoreTitle%></a>
                    </div>
                </div>
            </div>

            <%
                    }
                }
            %>




        </div>
    </div>
</section>
<!-- End of more news section -->
<%
    dbsessionNews.clear();
    dbsessionNews.close();

    %>

<%@ include file="../footer.jsp" %>
