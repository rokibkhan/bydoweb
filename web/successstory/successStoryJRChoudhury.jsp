<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="get_about_banner_Tah">
    <h2 class="display_41_saz text-white">Success Story</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#" class="text-white">About</a></li>
                <li class="breadcrumb-item active" aria-current="page">Success Story</li>
            </ol>
        </nav>
    </div>
</div>

<!-- Start of details_akass -->
	<section class="news-single-content pt-5">
		<div class="container">
			<div style="    background-color: #28a745;" class="display-4 text-white text-center">Success Story of Jamilur Reza Choudhury</div>
			<div class="row">
				<div class="col-12">
					<div class="news-content">
						<div class="media mt-5">
							<div class="text-center mr-5 wow bounceInUp">
								<img class="mr-3 " src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/Jamilur_Reza_Choudhury.jpg" alt="Generic placeholder image">
								<h5 class="mt-3 font-weight-bold"> Jamilur Reza Choudhury</h5>
								<p class="mt-3">Vice-Chancellor of<br/>University of Asia Pacific</p>
								<div class="custom-horizontal-border"></div>
							</div>
							
							<div class="media-body text-justify" >
								<p> Jamilur Reza Choudhury (born 15 November 1943) is a Bangladeshi civil engineer, professor, researcher, and education advocate. He is a former Adviser (Minister) to Caretaker Government of Bangladesh (April?June 1996). He is the vice chancellor of University of Asia Pacific. He is also the president of Bangladesh Mathematical Olympiad Committee from 2003. He was awarded Ekushey Padak by the Government of Bangladesh in the category of science and technology in 2017. He was inducted as a National Professor by the Government of Bangladesh in 2018.</p> <br/>

                                <p>

                                Choudhury was born in Sylhet (during British colonial rule) on 15 November 1943. His father, Abid Reza Choudhury (1905?1991), was a renowned civil engineer who had migrated to Dhaka in 1952 (after the Partition of India) from Rangauty, a rural area of Hailakandi (Assam, India). Abid was the first Muslim to graduate from Bengal Engineering and Science University, in 1929. His mother, Hayatun Nessa Choudhury (n�e Laskor) (1922?2010) was a homemaker, from the Nitainagar area of Hailakandi. Jamilur is the middle child of five siblings.</p> <br/>
                                <p>

                                Choudhury began school in 1950 (age 6) at the Mymensingh Zilla School. After his family moved to Dhaka from Mymensingh in 1952, he transferred to Nawabpur Government High School, and transferred once more to a private Catholic school, St Gregory's High School, in 1953. Jamilur attended Dhaka College from 1957?1959 and earned his Bachelor of Science Degree (Civil Engineering) from Bangladesh University of Engineering and Technology (BUET) in 1963. Upon graduation from BUET (First Class First with Honours), he became a lecturer in the Civil Engineering Department that same year. He earned a Masters of Science in Engineering Degree (advanced structural engineering) in 1965 and a Ph.D. in structural engineering in 1968, both at University of Southampton.

Choudhury was awarded the honorary degree of Doctor of Engineering (Honoris Causa) by Manchester University on 20 October 2010 ? the first person of Bangladeshi origin to receive this honor from a British University.</p>
							</div>
						</div>
						<hr />
						
						<div class="media-body text-justify">
								<img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/Jamilur_Reza_Choudhury aw.jpg" alt="Blog Picture 3" class="img-fluid ml-3 float-right" />
								<h5 class="mt-3 font-weight-bold"> Work:</h5> <p> Immediately after publication of results of BSC Engineering, he joined as a faculty of the former East Pakistan University of Engineering and Technology (EPUET) pending formal appointment (perhaps due to marginalization of Bengalis by the Pakis)[citation needed]. In November 1963, he formally joined as a lecturer in the Department of Civil Engineering & began his long teaching career. In September 1964 he was awarded a scholarship by Burmashell to pursue MS in structural engineering. His thesis was on "Cracks in Concrete Beam using Computer Aided Design". In 1968, he was conferred a Ph.D. degree on the topic of "Shear Wall & Structural Analysis of High rise Building". After completing Ph.D. he returned to former East Pakistan in 1968 & joined the former East Pakistan University of Engineering & Technology as an assistant professor until the demise of E. Pakistan and the birth of a new nation, Bangladesh. In 1973 he was promoted to associate professor of Bangladesh University of Engineering & Technology (BUET)and in 1976 a full professor. In 1975, he was offered Nuffield scholarship to pursue post-doctoral Fellowship at Surrey University in the UK. Until 2001 he was working as a Professor at BUET. He was also entrusted with developing a "Computer Center" at BUET and was appointed the director for about 10 years. He served as vice-chancellor of BRAC University between 2001 and 2010. Choudhury was appointed the chairman of the task force for developing Software Export & IT Infrastructure in Bangladesh from 1997 to 2000 under Ministry of Commerce. He was a ranking member of the Prime Minister's Task Force on developing Digital Bangladesh. Besides he is involved with several local and international organizations. Currently, he is working as vice-chancellor of the University of Asia Pacific (UAP) . Under his intellectual leadership, University of Asia Pacific is now regarded as one of the most respected universities in Bangladesh. He was the Technical Adviser of Padma bridge. He is the president of Bangladesh Mathematical Olympiad Committee.</p> </br>
                       
						</div>





                        </div>
                        <blockquote class="blockquote bg-light text-left p-5">
                            <p class="mb-0">"Prof. Jamilur Reza Choudhury is a foremost civil engineer, professor, researcher, and education advocate of this country. He was a former Adviser to the Caretaker Government of Bangladesh, and is the current Vice Chancellor of University of Asia Pacific. " </p>
                            <i class="fas fa-quote-right float-right"></i>
                            <footer class="blockquote-footer">Internet</footer>
                        </blockquote>
                        <div class="container" style="
    padding: 0 0 5px 0;
">
    <button class="btn-success text-bold" style="padding: 5px;"> <a style="color: #ffff" href="https://en.wikipedia.org/wiki/Jamilur_Reza_Choudhury"> Read More at Wikipedia </a></button>

</div>



					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End of details of news -->
	







<%@ include file="../footer.jsp" %>
