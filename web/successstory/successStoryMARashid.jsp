<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="get_about_banner_Tah">
    <h2 class="display_41_saz text-white">Success Story</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#" class="text-white">About</a></li>
                <li class="breadcrumb-item active" aria-current="page">Success Story</li>
            </ol>
        </nav>
    </div>
</div>

<!-- Start of details_akass -->
<section class="news-single-content pt-5">
    <div class="container">
        <div style="    background-color: #28a745;" class="display-4 text-white text-center">Success Story of M. A. Rashid</div>
        <div class="row">
            <div class="col-12">
                <div class="news-content">
                    <div class="media mt-5">
                        <div class="text-center mr-5 wow bounceInUp">
                            <img class="mr-3 " src="<%out.print(GlobalVariable.baseUrl);%>/upload/success_story_m_a_rashid.jpg" alt="Generic placeholder image">
                            <h5 class="mt-3 font-weight-bold"> M. A. Rashid</h5>
                            <p class="mt-3">First Vice-Chancellor of<br/>BUET</p>
                            <div class="custom-horizontal-border"></div>
                        </div>

                        <div class="media-body text-justify" >
                            <p> M. A. Rashid (16 January 1919 - 6 November 1981) was a Bangladeshi educator. He served as the first Vice-chancellor of Bangladesh University of Engineering and Technology during 1962-1970.He was awarded Independence Day Award in 1982 by the Government of Bangladesh.</p> <br/>

                            <p>

                                Rashid was born in Habiganj District. He passed matriculation exam from Habiganj Govt. High School and intermediate exam from Sylhet Murari Chand College in 1936 and 1938 respectively.In 1942, he completed his bachelor's in Civil Engineering from Bengal Engineering College (later Indian Institute of Engineering Science and Technology, Shibpur) at Shibpur in West Bengal.He served as an assistant engineer in the Public Works Department of the Assam government during 1942-1945. With an Indian government scholarship, he earned his masters and DSc degrees in Civil Engineering from the Carnegie Institute of Technology (later Carnegie Mellon University) in Pittsburgh, Pennsylvania. He was the first in East Bengal to have a doctorate degree in engineering.</p> <br/>

                        </div>
                    </div>
                    <hr />

                    <div class="media-body text-justify">
                        
                        <h5 class="mt-3 font-weight-bold"> Work:</h5> 

                        <p>

                            After returning from the United States in 1948 Rashid joined the Ahsanullah Engineering College (later Bangladesh University of Engineering and Technology in Dhaka as an Assistant Professor. He became a Professor in 1952 and the first Bengali principal of the college in 1954.

                            In 1958, he became a member of the Education Commission of Pakistan. After the independence of Bangladesh, he became a member of National Pay Commission, Industrial Workers Wages Commission and President's Council of Advisors and was put in charge of the Ministry of Works in 1975.</p>

                        </br>

                    </div>

                    <div class="media-body text-justify">

                        <h5 class="mt-3 font-weight-bold"> Awards:</h5> 

                        <p>Slater Memorial Gold Medal (1941).</p>
                        <p>Tate Memorial Medal (1941).</p>
                        <p>Trevor Memorial Prize and Gold Medal (1942).</p>
                        <p>Sitara-I-Pakistan (1966).</p>
                        <p>Independence Day Award (1982).</p>

                        </br>

                    </div>





                </div>
                <blockquote class="blockquote bg-light text-left p-5">
                    <p class="mb-0">"M. A. Rashid  was first Vice-chancellor of Bangladesh University of Engineering and Technology during 1962-1970" </p>
                    <i class="fas fa-quote-right float-right"></i>
                    <footer class="blockquote-footer">Internet</footer>
                </blockquote>
                <div class="container" style="
                     padding: 0 0 5px 0;
                     ">
                    <button class="btn-success text-bold" style="padding: 5px;"> <a style="color: #ffff" href="https://en.wikipedia.org/wiki/M._A._Rashid"> Read More at Wikipedia </a></button>

                </div>



            </div>
        </div>
    </div>
</div>
</section>
<!-- End of details of news -->








<%@ include file="../footer.jsp" %>
