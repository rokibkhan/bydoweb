<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="get_about_banner_Tah">
    <h2 class="display_41_saz text-white">Success Story</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#" class="text-white">About</a></li>
                <li class="breadcrumb-item active" aria-current="page">Success Story</li>
            </ol>
        </nav>
    </div>
</div>



<!-- Start of details_akass -->
<section class="news-single-content pt-5">
    <div class="container">
        <div style="    background-color: #28a745;" class="display-4 text-white text-center">Success Story of Fazlur Rahman Khan</div>
        <div class="row">
            <div class="col-12">
                <div class="news-content">
                    <div class="media mt-5">
                        <div class="text-center mr-5 wow bounceInUp">
                            <img class="mr-3 " src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/FRKhan.jpg" alt="Generic placeholder image">
                            <h5 class="mt-3 font-weight-bold"> Fazlur Rahman Khan</h5>
                            <p class="mt-3">Father of Tubular Designs</p>
                            <div class="custom-horizontal-border"></div>
                        </div>

                        <div class="media-body text-justify" >
                            <p> Fazlur Rahman Khan (Bengali: ????? ????? ???, Fozlur R�hman Khan) (3 April 1929 ? 27 March 1982) was a Bangladeshi-American structural engineer and architect, who initiated important structural systems for skyscrapers. Considered the "father of tubular designs" for high-rises, Khan was also a pioneer in computer-aided design (CAD). He was the designer of the Sears Tower, since renamed Willis Tower, the tallest building in the world from 1973 until 1998, and the 100-story John Hancock Center. </p> <br/>

                            <p>

                                Khan, more than any other individual, ushered in a renaissance in skyscraper construction during the second half of the 20th century. He has been called the "Einstein of structural engineering" and the "Greatest Structural Engineer of the 20th Century" for his innovative use of structural systems that remain fundamental to modern skyscraper design and construction. In his honor, the Council on Tall Buildings and Urban Habitat established the Fazlur Khan Lifetime Achievement Medal, as one of their CTBUH Skyscraper Awards. </p> <br/>
                            <p>

                                Although best known for skyscrapers, Khan was also an active designer of other kinds of structures, including the Hajj airport terminal, the McMath?Pierce solar telescope, and several stadium structures.</p>
                        </div>
                    </div>
                    <hr />

                    <div class="media-body text-justify">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/FR_khan_sculputure_at_Sears_tower.jpg" alt="Blog Picture 3" class="img-fluid ml-3 float-right" />
                        <h5 class="mt-3 font-weight-bold"> Biography:</h5> <p> Fazlur Rahman Khan was born on 3 April 1929 in British-ruled India, later East Pakistan, which is now Bangladesh. He was brought up in the village of Bhandarikandii, in the Faridpur District near Dhaka. His father Abdur Rahman Khan was a high school mathematics teacher and textbook author. He eventually became the Director of Public Instruction in the region of Bengal and after retirement served as Principal of Jagannath College, Dhaka.</p> </br>
                        <p>

                            Khan attended Armanitola Government High School, in Dhaka. After that, he studied Civil Engineering in Bengal Engineering and Science University, Shibpur (present day Indian Institute of Engineering Science and Technology, Shibpur), and then received his Bachelor of Civil Engineering degree from Ahsanullah Engineering College, (now Bangladesh University of Engineering and Technology). He received a Fulbright Scholarship and a Pakistan government scholarship, which enabled him to travel to the United States in 1952. There he studied at the University of Illinois at Urbana?Champaign. In three years Khan earned two master's degrees ? one in structural engineering and one in theoretical and applied mechanics ? and a PhD in structural engineering with thesis titled Analytical study of relations among various design criteria for rectangular prestressed concrete beams. In 1967, he elected to become a United States citizen. </p>
                    </div>
                    <div class="media-body text-justify">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/800px-Hancock_tower_2006.jpg" alt="Blog Picture 3" class="img-fluid mr-3 my-2 float-left" />
                        <h5 class="mt-3 font-weight-bold"> Career:</h5> <p>In 1955, employed by the architectural firm Skidmore, Owings & Merrill (SOM), he began working in Chicago. He was made a partner in 1966. Khan introduced design methods and concepts for efficient use of material in building architecture. His first building to employ the tube structure was the Chestnut De-Witt apartment building. During the 1960s and 1970s, he became noted for his designs for Chicago's 100-story John Hancock Center and 110-story Sears Tower, since renamed Willis Tower, the tallest building in the world from 1973 until 1998.</p> </br>
                        <p>

                            He believed that engineers needed a broader perspective on life, saying, "The technical man must not be lost in his own technology; he must be able to appreciate life, and life is art, drama, music, and most importantly, people."

                            Khan's personal papers, most of which were in his office at the time of his death, are held by the Ryerson & Burnham Libraries at the Art Institute of Chicago. The Fazlur Khan Collection includes manuscripts, sketches, audio cassette tapes, slides and other materials regarding his work. </p> 


                        <h5 class="mt-3 font-weight-bold"> Personal life:</h4>   <p>For enjoyment, Khan loved singing Rabindranath Tagore's poetic songs in Bengali. He and his wife, Liselotte, who immigrated from Austria, had one daughter who was born in 1960.</p> </br> <hr/>

                            <h4  class="mt-3 font-weight-bold"> Innovations</h5>  <hr/>  <p>Khan discovered that the rigid steel frame structure that had long dominated tall building design was not the only system fitting for tall buildings, marking the start of a new era of skyscraper construction.</p> </br>



                    </div>
                    <div class="finishing-img-wrapper">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/Hajj_terminal_at_Jeddah_Airport.jpeg" alt="Blog Picture 3" class="img-fluid banner-img" />
                    </div>
                    <div class="media-body text-justify">

                        <h5 class="mt-3 font-weight-bold"> Tube structural systems:</h5> <p>Khan's central innovation in skyscraper design and construction was the idea of the "tube" structural system for tall buildings, including the framed tube, trussed tube, and bundled tube variants. His "tube concept", using all the exterior wall perimeter structure of a building to simulate a thin-walled tube, revolutionized tall building design.[21] Most buildings over 40 stories constructed since the 1960s now use a tube design derived from Khan's structural engineering principles.[22][23]

                            Lateral loads (horizontal forces) such as wind forces, seismic forces, etc., begin to dominate the structural system and take on increasing importance in the overall building system as the building height increases. Wind forces become very substantial, and forces caused by earthquakes, etc. are important as well. The tubular designs resist such forces for tall buildings. Tube structures are stiff and have significant advantages over other framing systems.[24] They not only make the buildings structurally stronger and more efficient, but also significantly reduce the structural material requirements. The reduction of material makes the buildings economically more efficient and reduces environmental impact. The tubular designs enable buildings to reach even greater heights. Tubular systems allow greater interior space and further enable buildings to take on various shapes, offering added freedom to architects.[25][26] These new designs opened an economic door for contractors, engineers, architects, and investors, providing vast amounts of real estate space on minimal plots of land. Khan was among a group of engineers who encouraged a rebirth in skyscraper construction after a hiatus of over thirty years.[27][8]

                            The tubular systems have yet to reach their limit when it comes to height.[28] Another important feature of the tubular systems is that buildings can be constructed using steel or reinforced concrete, or a composite of the two, to reach greater heights. Khan pioneered the use of lightweight concrete for high-rise buildings,[29] at a time when reinforced concrete was used for mostly low-rise construction of only a few stories in height.[30] Most of Khan's designs were conceived considering pre-fabrication and repetition of components so projects could be quickly built with minimal errors.[31]

                            The population explosion, starting with the baby boom of the 1950s, created widespread concern about the amount of available living space, which Khan solved by building upward.[32] More than any other 20th-century engineer, Fazlur Rahman Khan made it possible for people to live and work in "cities in the sky." Mark Sarkisian (Director of Structural and Seismic Engineering at Skidmore, Owings & Merrill) said, "Khan was a visionary who transformed skyscrapers into sky cities while staying firmly grounded in the fundamentals of engineering."</p> <hr/>


                        <h5 class="mt-3 font-weight-bold"> Framed tube:</h4>   <p>Since 1963, the new structural system of framed tubes became highly influential in skyscraper design and construction. Khan defined the framed tube structure as "a three dimensional space structure composed of three, four, or possibly more frames, braced frames, or shear walls, joined at or near their edges to form a vertical tube-like structural system capable of resisting lateral forces in any direction by cantilevering from the foundation." Closely spaced interconnected exterior columns form the tube. Horizontal loads, for example from wind and earthquakes, are supported by the structure as a whole. About half the exterior surface is available for windows. Framed tubes allow fewer interior columns, and so create more usable floor space. The bundled tube structure is more efficient for tall buildings, lessening the penalty for height. The structural system also allows the interior columns to be smaller and the core of the building to be free of braced frames or shear walls that use valuable floor space. Where larger openings like garage doors are required, the tube frame must be interrupted, with transfer girders used to maintain structural integrity.

                                The first building to apply the tube-frame construction was the DeWitt-Chestnut Apartment Building, since renamed Plaza on DeWitt, building that Khan designed and was completed in Chicago in 1963. This laid the foundations for the framed tube structure used in the construction of the World Trade Center.</p> </br> <hr/>


                            <h5 class="mt-3 font-weight-bold">Trussed tube and X-bracing:</h4>   <p>Khan pioneered several other variants of the tube structure design. One of these was the concept of applying X-bracing to the exterior of the tube to form a trussed tube. X-bracing reduces the lateral load on a building by transferring the load into the exterior columns, and the reduced need for interior columns provides a greater usable floor space. Khan first employed exterior X-bracing on his design of the John Hancock Center in 1965, and this can be clearly seen on the building's exterior, making it an architectural icon.
                                    In contrast to earlier steel frame structures, such as the Empire State Building (1931), which required about 206 kilograms of steel per square meter and One Chase Manhattan Plaza (1961), which required around 275 kilograms of steel per square meter, the John Hancock Center was far more efficient, requiring only 145 kilograms of steel per square meter. The trussed tube concept was applied to many later skyscrapers, including the Onterie Center, Citigroup Center and Bank of China Tower.</p> </br> <hr/>






                                <h4  class="mt-3 font-weight-bold"> Life Cycle Civil Engineering</h5>  <hr/>  <p>Khan and Mark Fintel conceived ideas of shock absorbing soft-stories, for protecting structures from abnormal loading, particularly strong earthquakes, over a long period of time. This concept was a precursor to modern seismic isolation systems.[46] The structures are designed to behave naturally during earthquakes where traditional concepts of material ductility are replaced by mechanisms that allow for movement during ground shaking while protecting material elasticity.

                                The IALCCE estalablished the Fazlur R. Khan Life-Cycle Civil Engineering Medal.</p> </br>

                            <h4  class="mt-3 font-weight-bold"> Awards and chair</h5>  <hr/>  <p>Among Khan's other accomplishments, he received the Wason Medal (1971) and Alfred Lindau Award (1973) from the American Concrete Institute (ACI); the Thomas Middlebrooks Award (1972) and the Ernest Howard Award (1977) from ASCE; the Kimbrough Medal (1973) from the American Institute of Steel Construction; the Oscar Faber medal (1973) from the Institution of Structural Engineers, London; the International Award of Merit in Structural Engineering (1983) from the International Association for Bridge and Structural Engineering IABSE; the AIA Institute Honor for Distinguished Achievement (1983) from the American Institute of Architects; and the John Parmer Award (1987) from Structural Engineers Association of Illinois and Illinois Engineering Hall of Fame from Illinois Engineering Council (2006).

                            Khan was cited five times by Engineering News-Record as among those who served the best interests of the construction industry, and in 1972 he was honoured with ENR's Man of the Year award. In 1973 he was elected to the National Academy of Engineering. He received Honorary Doctorates from Northwestern University, Lehigh University, and the Swiss Federal Institute of Technology Z�rich (ETH Zurich).

                            The Council on Tall Buildings and Urban Habitat named one of their CTBUH Skyscraper Awards the Fazlur Khan Lifetime Achievement Medal after him, and other awards have been established in his honour, along with a chair at Lehigh University. Promoting educational activities and research, the Fazlur Rahman Khan Endowed Chair of Structural Engineering and Architecture honours Khan's legacy of engineering advancement and architectural sensibility. Dan Frangopol is the first holder of the chair.

                            Khan was mentioned by president Obama in 2009 in his speech in Cairo, Egypt when he cited the achievements of America's Muslim citizens.

                            Khan was the subject of the Google Doodle on April 3, 2017, marking what would have been his 88th birthday.</p> </br>





                    </div>
                    <blockquote class="blockquote bg-light text-left p-5">
                        <p class="mb-0">"Khan was a visionary who transformed skyscrapers into sky cities while staying firmly grounded in the fundamentals of engineering." </p>
                        <i class="fas fa-quote-right float-right"></i>
                        <footer class="blockquote-footer">Mark Sarkisian</footer>
                    </blockquote>
                    <div class="container" style="
                         padding: 0 0 5px 0;
                         ">
                        <button class="btn-success text-bold" style="padding: 5px;"> <a style="color: #ffff" href="https://en.wikipedia.org/wiki/Fazlur_Rahman_Khan"> Read More at Wikipedia </a></button>

                    </div>



                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of details of news -->







<%@ include file="../footer.jsp" %>
