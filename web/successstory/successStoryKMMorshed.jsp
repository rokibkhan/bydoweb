<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="get_about_banner_Tah">
    <h2 class="display_41_saz text-white">Success Story</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#" class="text-white">About</a></li>
                <li class="breadcrumb-item active" aria-current="page">Success Story</li>
            </ol>
        </nav>
    </div>
</div>




<!-- Start of details_akass -->
<section class="news-single-content pt-5">
    <div class="container">
        <div style="    background-color: #28a745;" class="display-4 text-white text-center">Success Story of Engr. Khandker Manjur Morhed
        </div>
        <div class="row">
            <div class="col-12">
                <div class="news-content">
                    <div class="media mt-5">
                        <div class="text-center mr-5 wow bounceInUp">
                            <img class="mr-3 " src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/manjurmorshed.jpg" alt="Generic placeholder image">
                            <h5 class="mt-3 font-weight-bold"> Engr. Khandker Manjur Morhed</h5>
                            <p class="mt-3">General Secretary<br/>The Institution of Engineers<br/> Bangladesh</p>
                            <div class="custom-horizontal-border"></div>
                        </div>

                        <div class="media-body text-justify" >
                            <p> Engr. Khandker Manjur Morshed was born on 15th June 1961 in Lalmonirhat, Bangladesh. He is a leader of Professional Engineers and making positive changes in Professional communities. He has a long history of working for the rights and quality of different professionals and organizations.</p> <br/>

                            <p>
                            <h5 class="mt-3 font-weight-bold">  Academic: :</h5>
                            <ul>
                                <li>
                                    Master of Technology (M. Tech), Indian Institute of Technology, Roorkee, India. December 2002.

                                </li>
                                <li>
                                    B.Sc in Civil Engineering from Bangladesh University of Engineering and Technology (BUET), Dhaka, in 1986.

                                </li>


                            </ul>

                            <p>




                        </div>
                    </div>
                    <hr />

                    <div class="media-body text-justify">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/manjurmorshed1.jpg" alt="Blog Picture 3" class="img-fluid ml-3 float-right" />
                        <p> <h5 class="mt-3 font-weight-bold">Profession:</h5>
                        Bangladesh Water Development Board, Present Position: Superintending  Engineer.

                        Involvement in Student Politics 
                        Ex. Acting President, Bangladesh Chattra League, Bangladesh University of Engineering and Technology ( BUET), 1983.
                        Ex. Vice-President, Bangladesh Chattra League, Bangladesh University of Engineering and Technology ( BUET), 1982-1986.
                        Ex. Vice-President, Bangladesh Chattra League, Nazrul Islam Hall, Bangladesh University of Engineering and Technology ( BUET), 1981-1982.
                        Ex. Elected Food Secretary, Student Union, Nazrul Islam Hall, Bangladesh University of Engineering and Technology ( BUET),( Nominated by Bangladesh Chattra League), 1982-1986.
                        Actively Participated in all Political Movement, Student Movement, Student Welfare.</p> </br>


                        The Institution of Engineers, Bangladesh ( IEB Activities):
                        Presently Elected Honorary General Secretary, 2018-19.
                        Presently Vice-President, Federation of Engineering Institute of South and Central Asia, ( FEISCA).
                        Ex. Vice-President (Academic and International Affairs),2015-2017.
                        Ex. Vice-Chairman (Administration, Professional and Social Welfare), Dhaka Centre, IEB, 2 Times. (2006-2008, 2013-14)
                        Ex. Vice-Chairman (Academic and Human Resource Development), Dhaka Centre, IEB, 1 Time. (2009-2010)
                        Ex. Central and Local Council Member, IEB,(Several Times). (1994-1995, 1996-1999, 1998-1999, 2004-2005)
                        Actively Participate in all IEB activities and professional movement.
                        Attended in various National and International Seminars, Workshop, Training.

                        <h5 class="mt-3 font-weight-bold">Professional and Social Activities:</h5>
                        Presently Joint Secretary, Bangabandhu Prokousali Parishad, Central Committee.
                        Presently General Secretary, Bangabandhu Prokousali Parishad, Bangladesh Water Development Board.
                        Presently Executive Member, Bangladesh Water and Power Engineers Association.
                        Ex. Secretary-General, Bangladesh Water and Power Engineers Association. (2011-2012)
                        Ex. Joint Secretary, Bangladesh Water and Power Engineers Association. 
                        (2008-2009)
                        Ex. Organizing Secretary, Bangladesh Water and Power Engineers Association. (2006-2007)
                        </p>






                    </div>





                </div>
                <blockquote class="blockquote bg-light text-left p-5">
                    <p class="mb-0">" Engr. Khandker Manjur Morshed was born on 15th June 1961 in Lalmonirhat, Bangladesh. He is a leader of Professional Engineers and making positive changes in Professional communities." </p>
                    <i class="fas fa-quote-right float-right"></i>
                    <footer class="blockquote-footer">Internet</footer>
                </blockquote>
                <div class="container" style="
                     padding: 0 0 5px 0;
                     ">
                    <button class="btn-success text-bold" style="padding: 5px;"> <a style="color: #ffff" href="https://en.wikipedia.org/wiki/Jamilur_Reza_Choudhury"> Read More at Wikipedia </a></button>

                </div>



            </div>
        </div>
    </div>
</div>
</section>
<!-- End of details of news -->




<%@ include file="../footer.jsp" %>
