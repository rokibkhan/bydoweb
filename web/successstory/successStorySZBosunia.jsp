<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="get_about_banner_Tah">
    <h2 class="display_41_saz text-white">Success Story</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#" class="text-white">About</a></li>
                <li class="breadcrumb-item active" aria-current="page">Success Story</li>
            </ol>
        </nav>
    </div>
</div>







	
	<!-- Start of details_akass -->
	<section class="news-single-content pt-5">
		<div class="container">
			<div style="    background-color: #28a745;" class="display-4 text-white text-center">Success Story of Prof. Dr. M. Shamim Z. Bosunia</div>
			<div class="row">
				<div class="col-12">
					<div class="news-content">
						<div class="media mt-5">
							<div class="text-center mr-5 wow bounceInUp">
								<img class="mr-3 " src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/Shaminm-Z.-Bosunia.png" alt="Generic placeholder image">
								<h5 class="mt-3 font-weight-bold"> PROF. DR. M. SHAMIM Z. BOSUNIA</h5>
								<p class="mt-3">Structural Engineering Expert</p>
								<div class="custom-horizontal-border"></div>
							</div>
							
							<div class="media-body text-justify" >
								<p> Prof. Dr. M. Shamim Z. Bosunia is a prominent academician, leader and structural engineering expert of Bangladesh. He was born on January 31, 1944 in Rangpur district. He completed his matriculation in 1959 from [Name of school, district] and intermediate education in 1961 from [Name of College, district]. He earned his bachelor?s degree in civil engineering in 1965 from BUET and joined Dept. of Civil Engineering as a lecturer in 1969. He completed his master?s in civil engineering in 1972 from BUET. The topic of his MSc thesis was Torsional Behavior of Plain and Reinforced Beams of Brick Aggregate Concrete. On a Commonwealth Scholarship, he earned his PhD in 1979 from the University of Strathclyde, UK. In his PhD research, he worked on The Deterioration of Portland Cement Paste Exposed to Sodium Chloride Environments. He became a professor of civil engineering in 1981. He retired from BUET in 2009 and is now serving as Emeritus Professor, Dept. of Civil Engineering, University of Asia Pacific (UAP), Dhaka</p> <br/>

                                <p>

Prof. Bosunia is famous in the engineering community as a specialist structural and concrete design expert. He is considered as a pioneering academician promoting modern concrete construction in Bangladesh. His areas of expertise include repair and rehabilitation of structures, durability of structural concrete, low cost housing and rural areas etc. As a leading expert concrete technologist, he made significant contribution to the first Bangladesh National Building Code (BNBC) published in 1993. Areas of his contribution include building materials, design of concrete and ferrocement structures, evaluation of existing buildings etc. Prof. Bosunia published more than thirty of journal papers, research articles etc. throughout his career which stand as excellent references to the engineering community.</p> <br/>
                             
							</div>
						</div>
						<hr />
						
						<div class="media-body text-justify">
								<img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/3.jpg" alt="Blog Picture 3" class="img-fluid ml-3 float-right" />
								<h5 class="mt-3 font-weight-bold"> Work:</h5> <p>Apart from his teaching and research activities, Prof. Bosunia also successfully served in many administrative positions. He was the Head of the Department of Civil Engineering in 1985-87 and Dean of the Faculty of Civil Engineering in 1990-92. He also served as the Director of Students Welfare in 1997-99. He also served as hall provost and advisor to different sports and games. As an expert civil engineer, he served as the Chairman of all civil engineering committees of Bangladesh Standard Testing Institution (BSTI) and played a vital role in setting up Bangladeshi standards of various construction materials. He also served as a member of the board of governors of the Housing and Building Research Institute (HBRI) as well as a member of the selection board for faculty members of DUET, CUET, KUET and RUET. Currently he is serving as an independent director of the Prime Bank Ltd. as well as the chairman of the Board of Rajshahi WASA.

</p> </br>

<p>As an expert in the field of structural engineering and concrete construction, Prof. Bosunia is rendering various consultancy services in the capacity of Team Leader or member for the last four decades. Areas of his works include the East-West Interconnector Transmission Line over Jamuna, 500 bed hospital in Bogra, repair and rehabilitation of hundreds of cyclone shelters in the coastal areas of Bangladesh and preparing master plan for cyclone shelters, repair and strengthening of Banglabandhu National Stadium and Mirpur Cricket Stadium, design of a number of high-rise buildings in the country that includes the 39-storied City Centre in Dhaka, design of Mirpur Indoor Stadium, design of various bridges of RHD and many more. Presently, he is serving as a member of the Panel of Experts for the mega projects Padma Multipurpose Bridge and the Karnaphuli Tunnel appointed by the govt. of Bangladesh. He is also the president of Bangladesh Association of Consulting Engineers (BACE).

</p> </br>

<p>Though Prof. Bosunia is an academician in the field of civil engineering, he loves sports and had been a good organizer of sporting activities. He served as a member, treasurer and later the vice-president of Bangladesh Football Federation during the period of 1986 ? 2002. As a team leader of Bangladesh Football Team, he led the national team to UK (2000), Qatar (1998) and Maldives (1996). Besides, he also acted as a special observer representing Bangladesh in the Winter Games of Universiade in 1991 in Japan and in the Beijing Asian Games in 1990.

</p> </br>
<p>As an academician and teacher, Prof. Bosunia draws immense respect and popularity from his students and others. He had been famous among his students for his unique teaching style, capability of delivering lectures with outstanding wit and humor but without sacrificing substance. His popularity and leadership capability made him the President of the Institution of Engineers for the term 2013-14.

</p> </br>




                       
						</div>





                        </div>
                        <blockquote class="blockquote bg-light text-left p-5">
                            <p class="mb-0">"Prof. Dr. M. Shamim Z. Bosunia is a prominent academician, leader and structural engineering expert of Bangladesh. He was born on January 31, 1944 in Rangpur district. " </p>
                            <i class="fas fa-quote-right float-right"></i>
                            <footer class="blockquote-footer">Internet</footer>
                        </blockquote>
                        <div class="container" style="
    padding: 0 0 5px 0;
">
    <button class="btn-success text-bold" style="padding: 5px;"> <a style="color: #ffff" href="https://en.wikipedia.org/wiki/Jamilur_Reza_Choudhury"> Read More at Wikipedia </a></button>

</div>



					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End of details of news -->




<%@ include file="../footer.jsp" %>
