<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Logger logger = Logger.getLogger("API_memberBillRequest_jsp.class");
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object objH[] = null;
    Object objAppUsr[] = null;
    Object agentObj[] = null;
    Query memSQL = null;
    Query typeSQL = null;
    Query pmtSQL = null;
    Query appSQL = null;
    Query usrSQL = null;
    Query agtSQL = null;

    Query leadCreatedCountSQL = null;
    Query leadDeliveredCountSQL = null;

    String responseCode = "";
    String responseMsg = "";
    String responseData = "";
    String regPhone = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    JSONArray jsonInnerObjArr = new JSONArray();
    JSONObject jsonInnerObj = new JSONObject();

    //    json = new JSONObject();
    //int rec = WebServiceJson.KeyValidation(key);
    
    int rec = 1;
    String checkMyKey = "mcDENNRPoOF0wuhkcj2PKQ";

    String reqKey = request.getParameter("key");
    String reqMemberId = request.getParameter("memberId");
    //   String regPaymentCode = request.getParameter("paymentCode");
    String billId = request.getParameter("billId");
    String billStatus = request.getParameter("billStatus");
    String billReference = request.getParameter("billReference");
    String billAmount = request.getParameter("billAmount");
    String billReference1 = "";
    String memberBillId = "";
    String memberBillStatus = "";
    int memberId = 0;
    int memberTypeId = 0;
    String memberPaymentAmount = "";

    logger.info("API::memberBillPayment:: reqKey :" + reqKey);
    logger.info("API::memberBillPayment:: reqMemberId :" + reqMemberId);
    logger.info("API::memberBillPayment:: billId :" + billId);
    logger.info("API::memberBillPayment:: billStatus :" + billStatus);
    logger.info("API::memberBillPayment:: billReference :" + billReference);
    logger.info("API::memberBillPayment:: billAmount :" + billAmount);

    // if (rec == 1) {
    if (checkMyKey.equals(reqKey)) {

        //check member info for member Id
        // check payment code for payment Info
        memSQL = dbsession.createSQLQuery("SELECT id FROM member where member_id= '" + reqMemberId + "'");

        if (!memSQL.list().isEmpty()) {
            memberId = Integer.parseInt(memSQL.uniqueResult().toString());

            System.out.println("API::memberBillPayment:: memberId::" + memberId);

//            typeSQL = dbsession.createSQLQuery("SELECT mt.member_type_id FROM member_type mt "
//                    + "WHERE mt.member_id='" + memberId + "' AND now() between mt.ed and mt.td");
//
//            if (!typeSQL.list().isEmpty()) {
//                memberTypeId = Integer.parseInt(typeSQL.uniqueResult().toString());
//                
//                System.out.println("memberTypeId::"+memberTypeId);
//            }
            //          System.out.println("memberTypeId 1 ::"+memberTypeId);
            pmtSQL = dbsession.createSQLQuery("SELECT mf.txn_id,mf.amount,mf.status FROM member_fee mf "
                    + "WHERE mf.txn_id ='" + billId + "' AND mf.status = '0' AND mf.member_id ='" + memberId + "'");

//                 pmtSQL = dbsession.createSQLQuery("SELECT mf.txn_id,mf.amount,mf.status FROM member_fee mf "
//                    + "WHERE mf.bill_type ='" + regPaymentCode + "' AND mf.member_id ='" + memberId + "'");
            if (!pmtSQL.list().isEmpty()) {

                if (billStatus.equals("1")) {
                    //update bill

                    billReference1 = billReference + "-A-" + billAmount;

                    logger.info("API::memberBillPayment:: billReference1 :" + billReference1);

                    Query q4 = dbsession.createSQLQuery("UPDATE  member_fee SET status='1',paid_date = now(),ref_no='" + billReference1 + "',mod_date=now() where txn_id='" + billId + "'");

                    q4.executeUpdate();
                    dbtrx.commit();
                    if (dbtrx.wasCommitted()) {
                        //   response.sendRedirect("success.jsp?sessionid=" + sessionid);
                        responseCode = "1";

                        json = new JSONObject();
                        responseMsg = "member payment updated";

                        json.put("memberId", reqMemberId);
                        json.put("billId", billId);
                        json.put("responseCode", responseCode);
                        json.put("responseData", responseData);
                        json.put("responseMsg", responseMsg);
                        jsonArr.add(json);

                    } else {

                        dbtrx.rollback();
                        responseCode = "0";

                        json = new JSONObject();
                        responseMsg = "member payment not updated";

                        json.put("memberId", reqMemberId);
                        json.put("billId", billId);
                        json.put("responseCode", responseCode);
                        json.put("responseData", responseData);
                        json.put("responseMsg", responseMsg);
                        jsonArr.add(json);
                    }

                } else {
                    //not update bill
                    responseCode = "0";

                    json = new JSONObject();
                    responseMsg = "member payment problem";

                    json.put("memberId", reqMemberId);
                    json.put("billId", billId);
                    json.put("responseCode", responseCode);
                    json.put("responseData", responseData);
                    json.put("responseMsg", responseMsg);
                    jsonArr.add(json);
                }

            } else {
                json = new JSONObject();
                responseCode = "0";
                responseMsg = "Worng payment code info";

                json.put("responseCode", responseCode);
                json.put("responseData", responseData);
                json.put("responseMsg", responseMsg);
                jsonArr.add(json);
            }

        } else {

            json = new JSONObject();
            responseCode = "0";
            responseMsg = "Worng member info";

            json.put("responseCode", responseCode);
            json.put("responseData", responseData);
            json.put("responseMsg", responseMsg);
            jsonArr.add(json);
        }

    } else {

        json = new JSONObject();
        responseCode = "0";
        responseMsg = "Worng request or key";

        json.put("responseCode", responseCode);
        json.put("responseData", responseData);
        json.put("responseMsg", responseMsg);
        jsonArr.add(json);
    }

    out.print(jsonArr);
    dbsession.flush();
    dbsession.close();


%>