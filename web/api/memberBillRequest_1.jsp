<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Logger logger = Logger.getLogger("API_memberBillRequest_jsp.class");
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object obj[] = null;
    Object objAppUsr[] = null;
    Object agentObj[] = null;
    Query memSQL = null;
    Query appSQL = null;
    Query usrSQL = null;
    Query agtSQL = null;

    Query leadCreatedCountSQL = null;
    Query leadDeliveredCountSQL = null;

    String responseCode = "";
    String responseMsg = "";
    String responseData = "";
    String regPhone = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    JSONArray jsonInnerObjArr = new JSONArray();
    JSONObject jsonInnerObj = new JSONObject();

    //    json = new JSONObject();
    //int rec = WebServiceJson.KeyValidation(key);
    int rec = 1;
//$dataInfo = "key=mcDENNRPoOF0wuhkcj2PKQ";

    String reqKey = request.getParameter("key");
    String reqMemberId = request.getParameter("memberId");
    String regPaymentCode = request.getParameter("paymentCode");
    int memberId = 0;

    logger.info("reqMemberId                         :" + reqMemberId);
    logger.info("regPaymentCode                      :" + regPaymentCode);
    if (rec == 1) {

        //check member info for member Id
        // check payment code for payment Info
        memSQL = dbsession.createSQLQuery("SELECT id FROM member where member_id= '" + reqMemberId + "'");

        if (!memSQL.list().isEmpty()) {
            memberId = Integer.parseInt(memSQL.uniqueResult().toString());
        }else{
        
            json = new JSONObject();
            responseCode = "0";
            responseMsg = "Worng member info";

            json.put("responseCode", responseCode);
            json.put("responseData", responseData);
            json.put("responseMsg", responseMsg);
            jsonArr.add(json);
        }

        
        String givenpass1 = "";

        String appUserId = "";
        String appUserOrginalId = "";
        String appUserType = "";

        appSQL = dbsession.createSQLQuery("SELECT au.USER_ORGINAL_ID,aut.TYPE "
                + "FROM app_user au "
                + "LEFT JOIN app_user_type aut ON aut.ID = au.USER_TYPE "
                + "WHERE au.STATUS = 1 AND au.USER_ID = '" + regPhone + "' AND au.USER_PASSWORD = '" + givenpass1 + "' ");

        if (!appSQL.list().isEmpty()) {
            for (Iterator itApp = appSQL.list().iterator(); itApp.hasNext();) {

                objAppUsr = (Object[]) itApp.next();
                appUserOrginalId = objAppUsr[0].toString().trim();
                appUserType = objAppUsr[1].toString().trim();
            }

            //check User Type
            // if T Technician
            if (appUserType.equalsIgnoreCase("T")) {

                String techId = "";
                String techEmpId = "";
                String techName = "";
                String techPhone = "";
                String techPicture = "";
                String profilePictureUrl = "";
                String techTypeName = "";
                String syStatus = "";
                String userStatusDesc = "";
                String userRole = "";
                String btnColorClass = "";
                String levelStatusColorClass = "";
                String techService = "";
                String techCategoryName = "";
                String techLSPName = "LSP Name";
                String techTown = "Town Name";
                String techTeritoryName = "";

                usrSQL = dbsession.createSQLQuery("SELECT t.ID,t.TECH_ID,t.TECH_REP_NAME,t.PHONE_NO,t.PROFILE_PICTURE, "
                        + "  tr.TERITORY_NAME,sc.SERVICE_CATEGORY_NAME ,st.SERVICE_TYPE_NAME "
                        + "FROM technician t "
                        + "LEFT JOIN service_category sc ON t.SERVICE_CATEGORY_ID = sc.ID "
                        + "LEFT JOIN service_type st ON t.SERVICE_TYPE_ID = st.ID "
                        + "LEFT JOIN technician_teritory tt ON t.ID = tt.TECH_REP_ID "
                        + "LEFT JOIN teritory tr ON tt.TERITORY_ID = tr.ID "
                        + "WHERE t.STATUS = 1 AND t.ID = '" + appUserOrginalId + "' "
                        + "ORDER BY t.ID ASC");
                if (!usrSQL.list().isEmpty()) {
                    for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                        obj = (Object[]) it1.next();
                        techId = obj[0].toString().trim();
                        techEmpId = obj[1].toString().trim();
                        techName = obj[2].toString().trim();
                        techPhone = obj[3].toString().trim();
                        techPicture = obj[4].toString().trim();

                        profilePictureUrl = GlobalVariable.baseUrl + "/upload/" + techPicture;

                        techTeritoryName = obj[5] == null ? "" : obj[5].toString().trim();
                        techCategoryName = obj[6].toString().trim();
                        techTypeName = obj[7].toString().trim();

                        techService = techCategoryName + " <br> " + techTypeName;

                        jsonInnerObj = new JSONObject();

                        jsonInnerObj.put("techId", techId);
                        jsonInnerObj.put("techEmpId", techEmpId);
                        jsonInnerObj.put("techName", techName);
                        jsonInnerObj.put("techPhone", techPhone);
                        jsonInnerObj.put("techPicture", techPicture);
                        jsonInnerObj.put("profilePictureUrl", profilePictureUrl);
                        jsonInnerObj.put("techTeritoryName", techTeritoryName);
                        jsonInnerObj.put("techLSPName", techLSPName);
                        jsonInnerObj.put("techCategoryName", techCategoryName);
                        jsonInnerObj.put("techTypeName", techTypeName);
                        jsonInnerObj.put("techService", techService);
                        jsonInnerObj.put("techTown", techTown);

                        jsonInnerObjArr.add(jsonInnerObj);

                    }
                    json = new JSONObject();
                    responseCode = "1";
                    responseMsg = "Login successfully";

                    json.put("responseCode", responseCode);
                    json.put("responseType", "T");
                    json.put("responseMsg", responseMsg);
                    json.put("responseData", jsonInnerObjArr);

                    jsonArr.add(json);

                } else {

                    json = new JSONObject();
                    responseCode = "0";
                    responseMsg = "Worng user or password";

                    json.put("responseCode", responseCode);
                    json.put("responseData", responseData);
                    json.put("responseMsg", responseMsg);
                    jsonArr.add(json);
                }

            }//end Technician

            //if A Agent
            if (appUserType.equalsIgnoreCase("A")) {
                String agentId = "";
                String agentEmpId = "";
                String agentName = "";
                String agentPhone = "";
                String agentPicture = "";
                String agentProfilePictureUrl = "";

                String agentService = "";

                String leadCreatedCounter = "";
                String leadDeliveredCounter = "";

                String agentBranch = "Agent Point";
                String agentCompany = "Bank Asia";

                leadCreatedCountSQL = dbsession.createSQLQuery("SELECT  count(*) FROM ticket  WHERE  ticket_category = '7' AND ticket_status ='0' AND ticket_creator= '" + appUserOrginalId + "' ");

                if (!leadCreatedCountSQL.list().isEmpty()) {
                    leadCreatedCounter = leadCreatedCountSQL.uniqueResult().toString();
                }

                leadDeliveredCountSQL = dbsession.createSQLQuery("SELECT  count(*) FROM ticket  WHERE ticket_category = '7' AND ticket_status ='4' AND ticket_creator= '" + appUserOrginalId + "' ");

                if (!leadDeliveredCountSQL.list().isEmpty()) {
                    leadDeliveredCounter = leadDeliveredCountSQL.uniqueResult().toString();
                }

                agtSQL = dbsession.createSQLQuery("SELECT a.ID,a.AGENT_ID,a.AGENT_REP_NAME,a.PHONE_NO,a.PROFILE_PICTURE "
                        + "FROM agent a "
                        + "WHERE a.STATUS = 1 AND a.ID = '" + appUserOrginalId + "' "
                        + "ORDER BY a.ID ASC");
                if (!agtSQL.list().isEmpty()) {
                    for (Iterator it1 = agtSQL.list().iterator(); it1.hasNext();) {

                        agentObj = (Object[]) it1.next();
                        agentId = agentObj[0].toString().trim();
                        agentEmpId = agentObj[1].toString().trim();
                        agentName = agentObj[2].toString().trim();
                        agentPhone = agentObj[3].toString().trim();
                        agentPicture = agentObj[4].toString().trim();

                        agentProfilePictureUrl = GlobalVariable.baseUrl + "/upload/" + agentPicture;

                        agentService = "";

                        jsonInnerObj = new JSONObject();

                        jsonInnerObj.put("agentId", agentId);
                        jsonInnerObj.put("agentEmpId", agentEmpId);
                        jsonInnerObj.put("agentName", agentName);
                        jsonInnerObj.put("agentPhone", agentPhone);
                        jsonInnerObj.put("agentPicture", agentPicture);
                        jsonInnerObj.put("profilePictureUrl", agentProfilePictureUrl);
                        jsonInnerObj.put("agentService", agentService);
                        jsonInnerObj.put("leadCreatedCounter", leadCreatedCounter);
                        jsonInnerObj.put("leadDeliveredCounter", leadDeliveredCounter);
                        jsonInnerObj.put("agentBranch", agentBranch);
                        jsonInnerObj.put("agentCompany", agentCompany);

                        jsonInnerObjArr.add(jsonInnerObj);

                    }
                    json = new JSONObject();
                    responseCode = "1";
                    responseMsg = "Login successfully";

                    json.put("responseCode", responseCode);
                    json.put("responseType", "A");
                    json.put("responseMsg", responseMsg);
                    json.put("responseData", jsonInnerObjArr);

                    jsonArr.add(json);

                } else {

                    json = new JSONObject();
                    responseCode = "0";
                    responseMsg = "Worng user or password";

                    json.put("responseCode", responseCode);
                    json.put("responseData", responseData);
                    json.put("responseMsg", responseMsg);
                    jsonArr.add(json);
                }

            }//end Agent

        } else {

            json = new JSONObject();
            responseCode = "0";
            responseMsg = "Worng user or password";

            json.put("responseCode", responseCode);
            json.put("responseData", responseData);
            json.put("responseMsg", responseMsg);
            jsonArr.add(json);
        }

    } else {

        json = new JSONObject();
        responseCode = "0";
        responseMsg = "Worng information for login";

        json.put("responseCode", responseCode);
        json.put("responseData", responseData);
        json.put("responseMsg", responseMsg);
        jsonArr.add(json);
    }

    out.print(jsonArr);
    dbsession.flush();
    dbsession.close();


%>