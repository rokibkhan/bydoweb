<%-- 
    API Name   : memberBillPayment.jsp
    Created on : March 20, 2019, 01:04:45 PM
    Author     : Tahajjat Tuhin
    responseCode:
                0:Worng Request
                1:success
                2:already paid
                3:Worng Payment Code or transaction not found
                4:Worng member ID
--%>




<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Logger logger = Logger.getLogger("API_memberBillRequest_jsp.class");
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object objH[] = null;
    Object objAppUsr[] = null;
    Object agentObj[] = null;
    Query memSQL = null;
    Query typeSQL = null;
    Query pmtSQL = null;
    Query pmtSQL_1 = null;
    Query appSQL = null;
    Query usrSQL = null;
    Query agtSQL = null;

    Query leadCreatedCountSQL = null;
    Query leadDeliveredCountSQL = null;

    String responseCode = "";
    String responseMsg = "";
    String responseData = "";
    String regPhone = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    JSONArray jsonInnerObjArr = new JSONArray();
    JSONObject jsonInnerObj = new JSONObject();

    //    json = new JSONObject();
    //int rec = WebServiceJson.KeyValidation(key);
    int rec = 1;
    String checkMyKey = "mcDENNRPoOF0wuhkcj2PKQ";

    String reqKey = request.getParameter("key");
    String reqMemberId = request.getParameter("memberId");
    String regPaymentCode = request.getParameter("paymentCode");
    String memberBillId = "";
    String memberBillStatus = "";
    int memberId = 0;
    int memberTypeId = 0;
    String memberPaymentAmount = "";

    logger.info("reqKey                         :" + reqKey);
    logger.info("reqMemberId                         :" + reqMemberId);
    logger.info("regPaymentCode                      :" + regPaymentCode);

    // if (rec == 1) {
    if (checkMyKey.equals(reqKey)) {

        //check member info for member Id
        // check payment code for payment Info
        memSQL = dbsession.createSQLQuery("SELECT id FROM member where member_id= '" + reqMemberId + "'");

        if (!memSQL.list().isEmpty()) {
            memberId = Integer.parseInt(memSQL.uniqueResult().toString());

            System.out.println("memberId::" + memberId);

//            typeSQL = dbsession.createSQLQuery("SELECT mt.member_type_id FROM member_type mt "
//                    + "WHERE mt.member_id='" + memberId + "' AND now() between mt.ed and mt.td");
//
//            if (!typeSQL.list().isEmpty()) {
//                memberTypeId = Integer.parseInt(typeSQL.uniqueResult().toString());
//                
//                System.out.println("memberTypeId::"+memberTypeId);
//            }
            //          System.out.println("memberTypeId 1 ::"+memberTypeId);
            pmtSQL = dbsession.createSQLQuery("SELECT mf.txn_id,mf.amount,mf.status FROM member_fee mf "
                    + "WHERE mf.bill_type ='" + regPaymentCode + "' AND mf.status = '0' AND mf.member_id ='" + memberId + "'");

//                 pmtSQL = dbsession.createSQLQuery("SELECT mf.txn_id,mf.amount,mf.status FROM member_fee mf "
//                    + "WHERE mf.bill_type ='" + regPaymentCode + "' AND mf.member_id ='" + memberId + "'");
            if (!pmtSQL.list().isEmpty()) {
                for (Iterator itr1 = pmtSQL.list().iterator(); itr1.hasNext();) {

                    objH = (Object[]) itr1.next();
                    memberBillId = objH[0].toString();
                    memberPaymentAmount = objH[1].toString();
                    memberBillStatus = objH[2].toString();

                }

                //   memberPaymentAmount = pmtSQL.uniqueResult().toString();
                System.out.println("memberPaymentAmount ::" + memberPaymentAmount);

                json = new JSONObject();
                responseCode = "1";
                responseMsg = "member fee info found";

                json.put("memberId", reqMemberId);
                json.put("billId", memberBillId);
                json.put("paymentAmount", memberPaymentAmount);
                json.put("responseCode", responseCode);
                json.put("responseData", responseData);
                json.put("responseMsg", responseMsg);
                jsonArr.add(json);

            } else {
                //paid or else not found

                //if paid
                pmtSQL_1 = dbsession.createSQLQuery("SELECT mf.txn_id,mf.amount,mf.status FROM member_fee mf "
                        + "WHERE mf.bill_type ='" + regPaymentCode + "' AND mf.status = '1' AND mf.member_id ='" + memberId + "'");

                if (!pmtSQL_1.list().isEmpty()) {

                    json = new JSONObject();
                    responseCode = "2";
                    responseMsg = "Already paid";

                    json.put("responseCode", responseCode);
                    json.put("responseData", responseData);
                    json.put("responseMsg", responseMsg);
                    jsonArr.add(json);
                } else {
                    json = new JSONObject();
                    responseCode = "3";
                    responseMsg = "Worng payment code info or transation not exists";

                    json.put("responseCode", responseCode);
                    json.put("responseData", responseData);
                    json.put("responseMsg", responseMsg);
                    jsonArr.add(json);
                }
            }

        } else {

            json = new JSONObject();
            responseCode = "4";
            responseMsg = "Worng member info";

            json.put("responseCode", responseCode);
            json.put("responseData", responseData);
            json.put("responseMsg", responseMsg);
            jsonArr.add(json);
        }

    } else {

        json = new JSONObject();
        responseCode = "0";
        responseMsg = "Worng request or key";

        json.put("responseCode", responseCode);
        json.put("responseData", responseData);
        json.put("responseMsg", responseMsg);
        jsonArr.add(json);
    }

    out.print(jsonArr);
    dbsession.flush();
    dbsession.close();


%>