

<%@page import="java.util.Iterator"%>

<%@page import="com.appul.entity.Member"%>

<%@page import="org.hibernate.Query"%>

<%@page import="org.hibernate.Session"%>

<%@page import="com.appul.util.HibernateUtil"%>

<%@page import="com.appul.util.GlobalVariable"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>









<%

    Session dbsessionHead = HibernateUtil.getSessionFactory().openSession();

    org.hibernate.Transaction dbtrxHead = null;

    dbtrxHead = dbsessionHead.beginTransaction();

    String sessionIdHead = "";

    String userNameHead = "";

    String memberIdHead = "";

    Query queryHead = null;

    Object[] objectHead = null;

    String memberNameHead = "";

    String memberNameLastHead = "";

    String pictureNameHead = "";

    String pictureLinkHead = "";

    Member memberHead = null;

    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdHead = session.getId();

        userNameHead = session.getAttribute("username").toString();

        memberIdHead = session.getAttribute("memberId").toString();

        queryHead = dbsessionHead.createQuery("from Member as member WHERE id=" + memberIdHead + " ");

        for (Iterator itrHead = queryHead.list().iterator(); itrHead.hasNext();) {

            memberHead = (Member) itrHead.next();

            memberNameHead = memberHead.getMemberName();

            //  System.out.println("memberNameHead :: " + memberNameHead);
            String[] arrOfStr = memberNameHead.split(" ");

            for (String a : arrOfStr) {

                //  System.out.println(a);
                memberNameLastHead = a;

            }

            pictureNameHead = memberHead.getPictureName();

            pictureLinkHead = GlobalVariable.baseUrl + "/upload/member/" + pictureNameHead;

        }

    }

    dbsessionHead.flush();

    dbsessionHead.close();





%>



<!DOCTYPE html>

<html lang="en">



    <head>

        <meta charset="UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>IEB :: The Institution of Engineers, Bangladesh </title>



        <link rel="shortcut icon" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/favicon.ico">



        <link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/bootstrap.min.css">

        <link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/slick.css">

        <link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/slick-theme.css">

        <link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/video-player/css/jquery.mb.YTPlayer.min.css">

        <link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style.css">

        <link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/responsive.css">





        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">









        <script type="text/javascript">



            function getMemberAge() {



                //  console.log('dateString :: ' + dateString);

                var memberDoB = document.getElementById('memberDoB').value;

             //   console.log('memberDoB :: ' + memberDoB);

                if (memberDoB != '') {



                    var today = new Date();

                    var birthDate = new Date(memberDoB);

                    var age = today.getFullYear() - birthDate.getFullYear();

                    var m = today.getMonth() - birthDate.getMonth();

                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {

                        age--;

                    }

                    // return age;



                    document.getElementById('memberAgeXn').value = age;

                    document.getElementById('memberAge').value = age;

                }



            }

            // console.log('age: ' + getAge("1984-10-31"));



            function ageCheckingForMembership(arg) {

             //   console.log('ageCheckingForMembership :: ' + arg);

                var membershipApplyingFor, memberAge, associateMemberAgeLimit, memberAgeLimit, fellowMemberAgeLimit;



                fellowMemberAgeLimit = '37';

                memberAgeLimit = '27';

                associateMemberAgeLimit = '15';



                membershipApplyingFor = document.getElementById('membershipApplyingFor').value;

                memberAge = document.getElementById('memberAge').value;

                if (membershipApplyingFor != '') {

                    //fellow

                    if (membershipApplyingFor == 1) {

                        if (Number(memberAge) >= Number(fellowMemberAgeLimit)) {

                            document.getElementById("membershipApplyingForErr").innerHTML = "";

                            document.getElementById("btnRegStep1").disabled = false;

                        } else {

                            document.getElementById("membershipApplyingFor").focus();

                            //  document.getElementById("membershipApplyingForErr").innerHTML = "Minimum age for Fellow is " + fellowMemberAgeLimit + " year"; 

                            $("#membershipApplyingForErr").html("Minimum age for Fellow is " + fellowMemberAgeLimit + " year.").css({"color": "red", "font-size": "10px;"});

                            document.getElementById("btnRegStep1").disabled = true;

                        }

                    }

                    // Member 

                    if (membershipApplyingFor == 2) {

                        if (Number(memberAge) >= Number(memberAgeLimit)) {

                            document.getElementById("membershipApplyingForErr").innerHTML = "";

                            document.getElementById("btnRegStep1").disabled = false;

                        } else {

                            document.getElementById("membershipApplyingFor").focus();

                            //  document.getElementById("membershipApplyingForErr").innerHTML = "Minimum age for Member is " + memberAgeLimit + " year.";

                            $("#membershipApplyingForErr").html("Minimum age for Member is " + memberAgeLimit + " year.").css({"color": "red", "font-size": "10px;"});

                            document.getElementById("btnRegStep1").disabled = true;

                        }

                    }

                    //  Associate Member  

                    if (membershipApplyingFor == 3) {

                        if (Number(memberAge) >= Number(associateMemberAgeLimit)) {

                            document.getElementById("membershipApplyingForErr").innerHTML = "";

                            document.getElementById("btnRegStep1").disabled = false;

                        } else {

                            document.getElementById("membershipApplyingFor").focus();

                            //  document.getElementById("membershipApplyingForErr").innerHTML = "Minimum age for Associate Member is " + associateMemberAgeLimit + " year.";

                            $("#membershipApplyingForErr").html("Minimum age for Associate Member is " + associateMemberAgeLimit + " year.").css({"color": "red", "font-size": "10px;"});

                            document.getElementById("btnRegStep1").disabled = true;

                        }

                    }

                } else {

                    //disable submit button

                    document.getElementById("btnRegStep1").disabled = true;

                }

            }

            function requestMemberApprovalByProposer(arg1, arg2) {



                var rupantorLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

             //   console.log("requestMemberApprovalByProposer -> arg1 :: " + arg1 + " arg2:: " + arg2);





                btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnApprovalComfirmation\" onclick=\"requestMemberApprovalByProposerConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"

                        + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";





//        taskLGModal = $('#taskLGModal');

//        taskLGModal.find("#taskLGModalTitle").text("Ticket Assign Option");

//        taskLGModal.find(".modal-dialog").removeClass("modal-lg");

//        taskLGModal.find(".modal-dialog").css("margin-top", "165px");

//        taskLGModal.modal('show');



                rupantorLGModal = $('#rupantorLGModal');

                rupantorLGModal.find("#rupantorLGModalTitle").text("Membership Request Approval Confirmation");

                rupantorLGModal.find("#rupantorLGModalBody").html("");

                rupantorLGModal.find("#rupantorLGModalFooter").html(btnInvInfo);

                rupantorLGModal.modal('show');



            }



            function requestMemberApprovalByProposerConfirm(arg1, arg2) {

                var rupantorLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

            //    console.log("requestMemberApprovalByProposer -> arg1 :: " + arg1 + " arg2:: " + arg2);



                rupantorLGModal = $('#rupantorLGModal');



                url = '<%=GlobalVariable.baseUrl%>' + '/member/memberShipApprovalRequestApproveProcess.jsp';



                $.post(url, {sessionId: arg1, requestId: arg2}, function (data) {

//

               //     console.log(data);



                    if (data[0].responseCode == 1) {



                        rupantorLGModal.modal('hide');

                        $("#acttionBox" + data[0].requestId).html("<a class=\"btn btn-primary btn-sm text-white\" ><i class=\"fa fa-check-circle\"></i> Approved</a>");

                        //	    	 	$("#globalAlertInfoBoxCon").show();

                        //$("#btnSubscribeEmailOpt").button('reset');

                        //		$("#btnSubscribeEmailOpt").button('complete') // button text will be "finished!"

                        // $("#btnSubscribeEmailOpt").addClass("disabled");

                        //  $("#msgSubscribeEmailOpt").html(data[0].responseMsg);

                        $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");

                    }

                    if (data[0].responseCode == 0) {

                        rupantorLGModal.modal('hide');

                        //  $("#btnSubscribeEmailOpt").button('reset');

                        // $("#errMsgShow").html(data[0].responseMsg);

                        //   rupantorLGModal.find("#rupantorLGModalBody").html(data[0].responseMsg);

                        // $("#globalAlertInfoBoxCon").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");

                        $("#globalAlertInfoBoxConTT").show().css({"display": "block", "margin-top": "5px"});

                        $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");



                    }







                }, "json");









            }







            function memberJobApplyAlert() {



                var rupantorLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

                //  console.log("memberJobApply -> arg1 :: " + arg1 + " arg2:: " + arg2);





                btnInvInfo = "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";







                rupantorLGModal = $('#rupantorLGModal');

                //   rupantorLGModal = $('#taskLGModal');

                rupantorLGModal.find("#rupantorLGModalTitle").text("Job Apply Confirmation");

                rupantorLGModal.find("#rupantorLGModalBody").html("<h4>Login required for apply job.</h4>");

                rupantorLGModal.find("#rupantorLGModalFooter").html(btnInvInfo);

                rupantorLGModal.modal('show');



            }



            function memberJobApply(arg1, arg2, arg3) {



                var rupantorLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

             //   console.log("memberJobApply -> arg1 :: " + arg1 + " arg2:: " + arg2);





                btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnApprovalComfirmation\" onclick=\"memberJobApplyConfirm('" + arg1 + "','" + arg2 + "','" + arg3 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"

                        + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";





                rupantorLGModal = $('#rupantorLGModal');

                //   rupantorLGModal = $('#taskLGModal');

                rupantorLGModal.find("#rupantorLGModalTitle").text("Job Apply Confirmation");

                rupantorLGModal.find("#rupantorLGModalBody").html("");

                rupantorLGModal.find("#rupantorLGModalFooter").html(btnInvInfo);

                rupantorLGModal.modal('show');



            }



            function memberJobApplyConfirm(arg1, arg2, arg3) {

                var rupantorLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

             //   console.log("memberJobApplyConfirm -> arg1 :: " + arg1 + " arg2:: " + arg2);



                rupantorLGModal = $('#rupantorLGModal');





                url = '<%=GlobalVariable.baseUrl%>' + '/jobs/memberJobApplyProcess.jsp';



                $.post(url, {sessionId: arg1, memberId: arg2, jobId: arg3}, function (data) {

//

              //      console.log(data);



                    if (data[0].responseCode == 1) {



                        rupantorLGModal.modal('hide');

                        //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);

                        $("#showJobApplyMsg" + data[0].requestId).html(data[0].responseMsgHtml);



                        $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");

                    }

                    if (data[0].responseCode == 0) {

                        rupantorLGModal.modal('hide');

                        //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);

                        $("#showJobApplyMsg" + data[0].requestId).html(data[0].responseMsgHtml);

                        $("#globalAlertInfoBoxConTT").show().css({"display": "block", "margin-top": "5px"});

                        $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");



                    }







                }, "json");

            }







            function myPictureUplaodFileTypeSize() {



                // vs_visualsearch_upload_picture

                // document.getElementById('file').files[0].name



                var fileName = document.getElementById('fileMyPic').files[0].name;

                var fileSize = document.getElementById('fileMyPic').files[0].size;

                var fileType = document.getElementById('fileMyPic').files[0].type;



                var vsUPimage = document.getElementById("fileMyPic").value;

                if (vsUPimage != '')

                {

                    var checkimg = vsUPimage.toLowerCase();

                    if (!checkimg.match(/(\.jpg|\.png|\.JPG|\.PNG|\.jpeg|\.JPEG)$/)) { // validation of file extension using regular expression before file upload                   

                        document.getElementById("fileMyPic").focus();

                        document.getElementById("fileMyPicErr").innerHTML = "Invalid file type.Only JPGE and PNG file are allowed to upload";

                        document.getElementById("btnMyPic").disabled = true;

                        return false;

                    } else {

                        document.getElementById("fileMyPicErr").innerHTML = "";

                    }









                    //10485760 //10 MB

                    // 1048576  // 1MB

                    // 2097152 // 2MB

                    // 3145728 //2MB

                    // 5242880   // 5MB

                    // 1048576

                    //   echo "Sorry, File too large.";



                    if (fileSize > 5242880)  // validation according to file size

                    {

                        document.getElementById("fileMyPicErr").innerHTML = "File size too large.Only 5MB file are allowed to upload";

                        document.getElementById("btnMyPic").disabled = true;

                        return false;

                    } else {

                        document.getElementById("fileMyPicErr").innerHTML = "";

                    }

                    document.getElementById("btnMyPic").disabled = false;

                    return true;



                } else {

                    document.getElementById("btnMyPic").disabled = true;

                    return false;

                }

            }





            function relatedDocumentUplaodFileTypeSize() {



                // vs_visualsearch_upload_picture

                // document.getElementById('file').files[0].name



                var fileName = document.getElementById('fileNID').files[0].name;

                var fileSize = document.getElementById('fileNID').files[0].size;

                var fileType = document.getElementById('fileNID').files[0].type;



                var vsUPimage = document.getElementById("fileNID").value;

                if (vsUPimage != '')

                {

                    var checkimg = vsUPimage.toLowerCase();

                    //   if (!checkimg.match(/(\.jpg|\.png|\.JPG|\.PNG|\.jpeg|\.JPEG)$/)) { // validation of file extension using regular expression before file upload

                    if (!checkimg.match(/(\.zip|\.ZIP)$/)) { // validation of file extension using regular expression before file upload

                        document.getElementById("fileNID").focus();

                        document.getElementById("fileNIDErr").innerHTML = "Invalid file type.Only ZIP file are allowed to upload";

                        document.getElementById("btnRelDoc").disabled = true;

                        return false;

                    } else {

                        document.getElementById("fileNIDErr").innerHTML = "";

                    }









                    //10485760 //10 MB

                    // 1048576  // 1MB

                    // 2097152 // 2MB

                    // 3145728 //2MB

                    // 5242880   // 5MB

                    // 1048576

                    //   echo "Sorry, File too large.";



                    if (fileSize > 5242880)  // validation according to file size

                    {

                        document.getElementById("fileNIDErr").innerHTML = "File size too large.Only 5MB file are allowed to upload";

                        document.getElementById("btnRelDoc").disabled = true;

                        return false;

                    } else {

                        document.getElementById("fileNIDErr").innerHTML = "";

                    }

                    document.getElementById("btnRelDoc").disabled = false;

                    return true;



                } else {

                    document.getElementById("btnRelDoc").disabled = true;

                    return false;

                }

            }





            function upgradationDocumentUplaodFileTypeSize() {



                // vs_visualsearch_upload_picture

                // document.getElementById('file').files[0].name



                var fileName = document.getElementById('fileNID').files[0].name;

                var fileSize = document.getElementById('fileNID').files[0].size;

                var fileType = document.getElementById('fileNID').files[0].type;



                var vsUPimage = document.getElementById("fileNID").value;

                if (vsUPimage != '')

                {

                    var checkimg = vsUPimage.toLowerCase();

                    //   if (!checkimg.match(/(\.jpg|\.png|\.JPG|\.PNG|\.jpeg|\.JPEG)$/)) { // validation of file extension using regular expression before file upload

                    //   if (!checkimg.match(/(\.zip|\.ZIP)$/)) { // validation of file extension using regular expression before file upload

                    if (!checkimg.match(/(\.pdf|\.PDF)$/)) { // validation of file extension using regular expression before file upload

                        document.getElementById("fileNID").focus();

                        document.getElementById("fileNIDErr").innerHTML = "<span class=\"alert alert-alert-danger\">Invalid file type.Only PDF file are allowed to upload</span>";

                        document.getElementById("btnRelDoc").disabled = true;

                        return false;

                    } else {

                        document.getElementById("fileNIDErr").innerHTML = "";

                    }









                    //10485760 //10 MB

                    // 1048576  // 1MB

                    // 2097152 // 2MB

                    // 3145728 //2MB

                    // 5242880   // 5MB

                    // 1048576

                    //   echo "Sorry, File too large.";



                    if (fileSize > 10485760)  // validation according to file size

                    {

                        document.getElementById("fileNIDErr").innerHTML = "<span class=\"alert alert-alert-danger\">File size too large.Only 10MB file are allowed to upload</span>";

                        document.getElementById("btnRelDoc").disabled = true;

                        return false;

                    } else {

                        document.getElementById("fileNIDErr").innerHTML = "";

                    }

                    document.getElementById("btnRelDoc").disabled = false;

                    return true;



                } else {

                    document.getElementById("btnRelDoc").disabled = true;

                    return false;

                }

            }





            function checkRegistrationProposerInfo(arg1) {



             //   console.log("checkRegistrationProposerInfo:: " + arg1);



                var propId, proposerMemberId, url;



                if (arg1 == 1) {

                    proposerMemberId = $("#proposerMemberId").val();

                    if (proposerMemberId == '' || proposerMemberId == null) {

                        $("#proposerMemberId").focus();

                        $("#proposerErr").html("Please enter Membership No");

                        return false;

                    }

                }

                if (arg1 == 2) {

                    proposerMemberId = $("#seconderMemberId").val();

                    if (proposerMemberId == '' || proposerMemberId == null) {

                        $("#seconderMemberId").focus();

                        $("#seconderErr").html("Please enter Membership No");

                        return false;

                    }

                }

                if (arg1 == 3) {

                    proposerMemberId = $("#thirdMemberId").val();

                    if (proposerMemberId == '' || proposerMemberId == null) {

                        $("#thirdMemberId").focus();

                        $("#thirdErr").html("Please enter Membership No");

                        return false;

                    }

                }

              //  console.log("proposerMemberId: " + proposerMemberId);



                url = '<%=GlobalVariable.baseUrl%>' + '/member/registrationRcomendationInfoCheckingProcess.jsp';



                $.post(url, {sessionId: arg1, proId: arg1, proposerMemberId: proposerMemberId}, function (data) {

                    console.log(data);



                    if (data[0].responseCode == 1) {



                        propId = data[0].reqProId;



                        if (propId == 1) {

                            $("#proposerId").val(data[0].proposerId);

                            $("#proposerMemberName").val(data[0].proposerName);

                            $("#proposerErr").html(data[0].responseMsg);

                        }

                        if (propId == 2) {

                            $("#seconderId").val(data[0].proposerId);

                            $("#seconderMemberName").val(data[0].proposerName);

                            $("#seconderErr").html(data[0].responseMsg);

                        }

                        if (propId == 3) {

                            $("#thirdId").val(data[0].proposerId);

                            $("#thirdMemberName").val(data[0].proposerName);

                            $("#thirdErr").html(data[0].responseMsg);

                        }

                    }

                    if (data[0].responseCode == 0) {

                        propId = data[0].reqProId;



                        if (propId == 1) {

                            $("#proposerId").val('');

                            $("#proposerMemberName").val('');

                            $("#proposerErr").html(data[0].responseMsg);

                        }

                        if (propId == 2) {

                            $("#seconderId").val('');

                            $("#seconderMemberName").val('');

                            $("#seconderErr").html(data[0].responseMsg);

                        }

                        if (propId == 3) {

                            $("#thirdId").val('');

                            $("#thirdMemberName").val('');

                            $("#thirdErr").html(data[0].responseMsg);

                        }



                    }







                }, "json");

            }









            function showHomeVideoPlay(arg1, arg2) {



             //   console.log("showHomeVideoPlay ::: arg1 ::" + arg1);

            //    console.log("showHomeVideoPlay ::: arg2 ::" + arg2);



                //   "<iframe class=\"videoIframe js-videoIframe\" data-src=\"" + videoEmbedLink + "?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv\"></iframe>"



                var videoPlayerModal, vInfo, vInfo1, arg;



                vInfo1 = "<div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">"

                        + "<iframe id=\"videoXn\" class=\"embed-responsive-item\" src=\" " + arg1 + "?enablejsapi=1\" allowfullscreen></iframe>"

                        + "</div>";



                vInfo = "<iframe class=\"videoIframe js-videoIframe\" data-src=\"" + arg1 + "?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv\"></iframe>";



                //  arg = "'" + arg1 + "','" + arg2 + "'";

                //  videoPlayerModal = $('#videoPlayerModal');

                //   videoPlayerModal.find("#videoPlayerModalTitle").text("News Delete confirmation");       

                //   videoPlayerModal.find("#videoPlayerModalBody").html(vInfo);

                //   videoPlayerModal.modal('show');





                //featured_video_shap

                //videoframeContainer

                //icon_inner



                $("#featured_video_shap" + arg2).css({"background": "none", "height": "0"});

                $("#pay_icon" + arg2).css("display", "none");

                $("#videoframeContainer" + arg2).html(vInfo1);



            }







            function showVideoPlayerModal(arg1) {



             //   console.log("showVideoPlayerModal::: " + arg1);



                var videoPlayerModal, vInfo, arg;



                vInfo = "<div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">"

                        + "<iframe class=\"embed-responsive-item\" src=\" " + arg1 + "?autoplay=1\" allowfullscreen></iframe>"

                        + "</div>";



                //  arg = "'" + arg1 + "','" + arg2 + "'";

                videoPlayerModal = $('#videoPlayerModal');

                videoPlayerModal.find("#videoPlayerModalTitle").text("News Delete confirmation");

                //  btnInfo = "<a onclick=\"eventDeleteInfoConfirm(" + arg + ")\" class=\"btn btn-primary btn-sm\">Confirm</a> ";

                videoPlayerModal.find("#videoPlayerModalBody").html(vInfo);



//            console.log(rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo));

                videoPlayerModal.modal('show');



            }



        </script>








    </head>



    <body>


        
        

        <!-- start header -->

        <header class="header">

            <div class="container">

                <div class="row">

                    <div class="col-lg-1 col-md-12 col-sm-12 header_left">

                        <div class="logo">

                            <a href="<%out.print(GlobalVariable.baseUrl);%>">

                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/logo.png" alt="IEB" class="img-fluid">

                            </a>

                        </div>

                    </div>

                    <div class="col-lg-11 col-md-12 col-sm-12 header_right">

                        <div class="btn_right mb_show">

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_menu" aria-expanded="false">



                                <span class="bar bar-1"></span>

                                <span class="bar bar-2"></span>

                                <span class="bar bar-3"></span>

                            </button>

                        </div>

                        <div class="header_top_wrap mb_hide">

                            <nav class="navbar navbar-expand-lg">

                                <ul class="navbar-nav mr-auto header_top_menu">

                                    <li>

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/pages/iebBloodBank.jsp">

                                            <span class="icon">

                                                <i class="fas fa-tint fa-lg"></i>

                                            </span>

                                            <span class="text">Blood Bank</span>

                                        </a>

                                    </li>

                                    <li>


                                        <a href="http://bit.ly/2PQ7UgG">

                                            <span class="icon">

                                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/bag_icon.png" alt="img">

                                            </span>

                                            <span class="text">Use App</span>

                                        </a>

                                    </li>

                                    <li>

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/pages/contact.jsp">

                                            <span class="icon">

                                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/contact-icon.png" alt="img">

                                            </span>

                                            <span class="text">Contact</span>

                                        </a>

                                    </li>

                                </ul>



                                <form class="form-inline search_form"  name="mainSearch" id="mainSearch" method="GET" action="<%out.print(GlobalVariable.baseUrl);%>/search/searchResult.jsp?sessionid=<%=session.getId()%>" >



                                    <input type="hidden" id="sessionid" name="sessionid" value="<%=session.getId()%>"> 





                                    <%

                                        String mainSearchStringH = request.getParameter("mainSearchString") == null ? "" : request.getParameter("mainSearchString").trim();

                                        if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {


                                    %>





                                    <label class="search_input">                                        

                                        <input style="width: 250px;"  id="mainSearchString" name="mainSearchString" value="<%=mainSearchStringH%>" class="form-control " type="search" placeholder="Search" aria-label="Search">

                                        <a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/search_icon.png" alt="img"></a>

                                    </label>

                                    <ul class="nav navbar-top-links navbar-right pull-right">



                                        <li class="dropdown">

                                            <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" style="font-size: 14px;"> 

                                                <img src="<%=pictureLinkHead%>" alt="user-img" width="36" class="img-circle">

                                                <b class="hidden-xs"><%=memberNameHead%></b> 

                                            </a>



                                            <ul class="dropdown-menu dropdown-user animated flipInY"  style="border-radius:0; border: 1px solid rgba(120,130,140,.13);right: 0;left: auto; box-shadow: 0 3px 12px rgba(0,0,0,.05) !important;-webkit-box-shadow: 0 !important;-moz-box-shadow: 0 !important;">                                

                                                <li style="padding: .1rem .5rem;"><a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberDashboard.jsp?sessionid=<%=session.getId()%>"><i class="fa fa-user" style="font-size: 14px;"></i> My Profile</a></li>                                

                                                <li role="separator" class="divider" style="height: 1px;margin: 9px 0;overflow: hidden;background-color: #e5e5e5;"></li>                                               

                                                <li style="padding: .1rem .5rem;"><a href="<%out.print(GlobalVariable.baseUrl);%>/member/logout.jsp"><i class="fa fa-power-off" style="font-size: 14px;"></i> Logout</a></li>

                                            </ul>

                                            <!-- /.dropdown-user -->

                                        </li>

                                        <!--<li class="right-side-toggle"> <a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>-->

                                        <!-- /.dropdown -->

                                    </ul>



                                    <% } else {%>





                                    <label class="search_input">

                                        <input id="mainSearchString" name="mainSearchString" value="<%=mainSearchStringH%>" class="form-control " type="search" placeholder="Search" aria-label="Search">

                                        <a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/search_icon.png" alt="img"></a>

                                    </label>





                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/member/login.jsp" class="login"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/login_icon.png" alt="">Login</a>



                                    <% } %>



                                </form>

                            </nav>

                        </div>

                        <div class="header_bottom_wrap">

                            <nav class="navbar navbar-expand-lg">



                                <div class="collapse navbar-collapse" id="main_menu">

                                    <div class="header_top_wrap mb_show">

                                        <nav class="navbar navbar-expand-lg">

                                            <ul class="navbar-nav mr-auto header_top_menu">

                                                <li>

                                                    <a style="margin-top: 5px;" href="<%out.print(GlobalVariable.baseUrl);%>/pages/iebBloodBank.jsp">

                                                        <span class="icon">

                                                            <i class="fas fa-tint fa-lg"></i>

                                                        </span>

                                                        <span class="text">Blood Bank</span>

                                                    </a>

                                                </li>

                                                <li>

                                                    <a href="http://bit.ly/2PQ7UgG">

                                                        <span class="icon">

                                                            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/bag_icon.png" alt="img">

                                                        </span>

                                                        <span class="text">Use App</span>

                                                    </a>

                                                </li>

                                                <li>

                                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/pages/contact.jsp">

                                                        <span class="icon">

                                                            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/contact-icon.png" alt="img">

                                                        </span>

                                                        <span class="text">Contact</span>

                                                    </a>

                                                </li>

                                            </ul>

                                            <form class="form-inline search_form">

                                                <label class="search_input">

                                                    <input class="form-control " type="search" placeholder="Search" aria-label="Search">

                                                    <a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/search_icon.png" alt="img"></a>

                                                </label>

                                                <a href="<%out.print(GlobalVariable.baseUrl);%>/member/login.jsp" class="login"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/login_icon.png" alt="">Login</a>

                                            </form>

                                        </nav>

                                    </div>

                                    <ul class="navbar-nav header_main_menu">                                        

                                        <li class="dropdown">

                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">About</a>

                                            <div class="dropdown-menu mega_menu" style="min-width:180px;">

                                                <ul class="mega_menu_item" style="width: 100%;">

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/pages/about.jsp">About IEB</a></li>

                                                    <hr>                                                    

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/pages/iebAmisObjective.jsp">Aims & Objective</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/upload/document/IEB_Constitution.pdf" target="_blank">IEB Constitution</a></li>

                                                    <hr>



                                                </ul>



                                            </div>

                                        </li>

                                        <li class="dropdown">

                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Membership</a>

                                            <div class="dropdown-menu mega_menu">

                                                <ul class="mega_menu_item">

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/aboutMemberShip.jsp">What is IEB Membership ?</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/aboutMemberShipCriteria.jsp">Membership Criteria</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/howToGetMemberShip.jsp">How to get Membership ?</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/aboutMemberShipFee.jsp">Membership Fee</a></li>    

                                                    <hr>                                                

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberShipForm.jsp">Membership Form</a></li>

                                                    <hr>



                                                </ul>

                                                <ul class="mega_menu_item">

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/accreditation.jsp">Accreditation</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/search.jsp">Search your membership No.</a></li>

                                                    <hr>



                                                    <%

                                                        if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

                                                    %>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberDashboard.jsp?sessionid=<%=session.getId()%>">My Profile</a></li>

                                                    <hr>

                                                    <%

                                                    } else {

                                                    %>



                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/registration.jsp">Apply Membership</a></li>

                                                    <hr>

                                                    <% } %>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/registrationCheck.jsp">Status of Application </a></li>

                                                    <hr>

                                                </ul>

                                            </div>

                                        </li>                                         






                                        <li class="dropdown">

                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Networks</a>

                                            <div class="dropdown-menu mega_menu" style="min-width:250px;">

                                                <ul class="mega_menu_item" style="width: 100%;">

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/networks/division.jsp">Engineering Division</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/networks/center.jsp">Centre</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/networks/subCenter.jsp">Sub-Center</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/networks/overseasChapter.jsp">Overseas Chapter</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/networks/studentChapter.jsp">Student Chapter</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/networks/internationalAffairs.jsp">International Affairs</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/networks/occupationalSafety.jsp">Occupational Safety board of Bangladesh </a></li>

                                                    <hr>

                                                </ul>



                                            </div>

                                        </li>



                                        <li class="dropdown">

                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Publications</a>

                                            <div class="dropdown-menu mega_menu" style="min-width:200px;">

                                                <ul class="mega_menu_item" style="width: 100%;">

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/publications/journals.jsp">Journals</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/publications/annualReports.jsp">Annual Reports</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/publications/engineeringNews.jsp?tag=Engineering">Engineering News</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/publications/magazine.jsp">Magazine</a></li>

                                                    <hr>

                                                </ul>



                                            </div>

                                        </li>

                                        <li class="dropdown">

                                            <a href="<%=GlobalVariable.baseUrl%>/committee/committee.jsp" class="dropdown-toggle" data-toggle="dropdown">Committees</a>

                                            <div class="dropdown-menu mega_menu" style="min-width:250px;">

                                                <ul class="mega_menu_item" style="width: 100%;">

                                                    <li><a href="<%=GlobalVariable.baseUrl%>/committee/committee.jsp?committeeTypeSearch=1&committeeSessionSearch=2018-2019">Central Executive Committee</a></li>

                                                    <hr>

                                                    <li><a href="<%=GlobalVariable.baseUrl%>/committee/divisionCommittee.jsp">Division Committee</a></li>

                                                    <hr>

                                                    <li><a href="<%=GlobalVariable.baseUrl%>/committee/centerCommittee.jsp">Centre  Committee</a></li>

                                                    <hr>

                                                    <li><a href="<%=GlobalVariable.baseUrl%>/committee/subCenterCommittee.jsp">Sub-Center  Committee</a></li>

                                                    <hr>

                                                    <li><a href="<%=GlobalVariable.baseUrl%>/committee/overseasCommittee.jsp">Overseas Chapter  Committee</a></li>

                                                    <hr>

                                                    <li><a href="<%=GlobalVariable.baseUrl%>/committee/studentChapterCommittee.jsp">Student Chapter  Committee</a></li>

                                                    <hr>

                                                    <li><a href="<%=GlobalVariable.baseUrl%>/committee/mohilaCommittee.jsp">Mohila Committee</a></li>

                                                    <hr>

                                                    <li><a href="<%=GlobalVariable.baseUrl%>/committee/councilMembersCommittee.jsp">Council members</a></li>

                                                    <hr>



                                                </ul>



                                            </div>

                                        </li>







                                        <li class="dropdown">

                                            <a href="#">News &amp; Events</a>

                                            <div class="dropdown-menu mega_menu" style="min-width:250px;">

                                                <ul class="mega_menu_item" style="width: 100%;">

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/news/news.jsp">News</a></li>

                                                    <hr>



                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/events/conventions.jsp">Conventions</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/events/seminars.jsp">Seminars</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/events/workshop.jsp">Workshop</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/events/conferences.jsp">Conferences</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/events/events.jsp">Events</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/notice/notice.jsp">Notice</a></li>

                                                    <hr>

                                                </ul>



                                            </div>

                                        </li>

                                        <li class="dropdown">

                                            <a href="#">Gallery</a>

                                            <div class="dropdown-menu mega_menu" style="min-width:150px;">

                                                <ul class="mega_menu_item" style="width: 100%;">

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/videogallery/videoGallery.jsp">Video Gallery</a></li>

                                                    <hr>

                                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/photogallery/photoAlbumAll.jsp">Photo Album</a></li>

                                                    <hr>

                                                </ul>

                                            </div>

                                        </li>
                                        <!-- li><a target="_blank" href="http://123.49.3.254/" style="background-color: blue; color: #fff; padding:5px 10px; border-radius: 15px">Old Site</a></li -->

                                    </ul>

                                </div>

                            </nav>

                        </div>

                    </div>

                </div>

            </div>

        </header>

        <!-- end header -->