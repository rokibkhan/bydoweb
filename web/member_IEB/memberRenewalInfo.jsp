<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    Query q2 = null;

    MemberType mtype = new MemberType();

    Member member = null;

    Object[] object5 = null;
    int memberTypeID = 0;
    String memberTypeName = "";
    String memberTypeAnnaralSubscriptionFee = "";
    Query memberTypeSQL = dbsession.createQuery("from MemberType WHERE  member_id=" + memberIdH + " AND SYSDATE() between ed and td ");

    for (Iterator memberTypeItr = memberTypeSQL.list().iterator(); memberTypeItr.hasNext();) {
        mtype = (MemberType) memberTypeItr.next();

        // memberTypeID = mtype.getId();
        memberTypeID = mtype.getMemberTypeInfo().getMemberTypeId();
        memberTypeName = mtype.getMemberTypeInfo().getMemberTypeName();
        memberTypeAnnaralSubscriptionFee = mtype.getMemberTypeInfo().getAnnaralSubscriptionFee().toString();

    }

    System.out.println("WEB:: memberRenewalInfo :: memberTypeID :: " + memberTypeID);

%>

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css"/>



<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>




<script type="text/javascript">

    function showTotalMemberShipRenewalFee(arg1, arg2, arg3) {
        var rupantorLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll, renewalType, renewalTypeId, memberTypeAnnaralSubscriptionFee, renewalTotalAmount;
        console.log("showTotalMemberShipRenewalFee -> arg1 :: " + arg1 + " arg2:: " + arg2);


        renewalTypeId = document.getElementById("memberRenewalTypeId").value;
        renewalType = "Annual";

        if (renewalTypeId != '') {




            url = '<%=GlobalVariable.baseUrl%>' + '/member/memberRenewalTotalFeeShow.jsp';

            $.post(url, {sessionId: arg1, memberId: arg2, memberTypeId: arg3, renewalType: renewalType, renewalTypeId: renewalTypeId}, function (data) {
//
                console.log(data);

                if (data[0].responseCode == 1) {

                    //  $("#mRenewalData").append(data[0].mRenewalFeeInfo);

                    //   $("#mRenewalDataX").html("");
                    //   $(data[0].mRenewalFeeInfo).insertAfter("#mRenewalData");


                    $("#mRenewalData").html(data[0].mRenewalFeeInfo);

                    //    rupantorLGModal.modal('hide');
                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
                    //    $("#showJobApplyMsg" + data[0].requestId).html(data[0].responseMsgHtml);

                    //   $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
                }
                if (data[0].responseCode == 0) {
                    //    rupantorLGModal.modal('hide');
                    //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
                    //   $("#showJobApplyMsg" + data[0].requestId).html(data[0].responseMsgHtml);
                    $("#globalAlertInfoBoxConTT").show().css({"display": "block", "margin-top": "5px"});
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");

                }



            }, "json");

        }

//else {
//
//            document.getElementById("memberTypeTotalAmount").value = '';
//        }
    }

</script>

<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Membership Upgrade Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->









                    <!-- ============ End Education information Table ================== -->

                    <div class="container">
                        <div class="row my-4">
                            <div class="col-12">
                                <div class="card-header panel_heading_saz" style="padding-top: 4px; padding-bottom: 4px;">
                                    <i class="fa fa-list-alt"></i> Add Membership Renewal Information
                                </div>
                            </div>

                        </div>
                    </div>

                    <form id="loginform" class="form-horizontal profile_form_horizontal_saz p_form" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/memberRenewalInfoSubmitData.jsp" onsubmit="return personalInfoValidationaaaa()">



                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">Renewal Type :</label>
                            <div class="col-sm-4">
                                <select name="memberRenewalTypeId" id="memberRenewalTypeId" class="form-control" required="" onchange="showTotalMemberShipRenewalFee('<%=sessionIdH%>', '<%=memberIdH%>', '<%=memberTypeID%>')">
                                    <option value="">Select Renewal Type</option>
                                    <option value="1">1 Year</option>
                                    <option value="2">2 Year</option>
                                    <option value="3">3 Year</option>
                                    <option value="5">5 Year</option>

                                    <% if (memberTypeID < 3) { %>

                                    <option value="20">Life Time</option>
                                    <%}%>
                                </select>
                            </div>
                        </div>
                        <section id="mRenewalData">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 col-form-label text-right">Total Amount</label>                            
                                <div class="col-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend"><span class="input-group-text">TK</span></div>
                                        <input name="memberTypeTotalAmount" id="memberTypeTotalAmount" type="text" class="form-control" style="padding-top: .2rem ; padding-bottom: .2rem ;" disabled>  
                                        <div class="input-group-append"><span class="input-group-text">.00</span></div>
                                    </div>                                
                                    <div id="memberTypeTotalAmountErr"></div>
                                </div>
                            </div> 
                        </section> 





                        <div class="form-group row">

                            <div class="col-sm-6 offset-sm-3">
                                <button type="submit" name="btnRegStep1" id="btnRegStep1" class="btn btn-primary mr-3">Submit</button>
                            </div>
                        </div>
                    </form>



                    <!-- ================ End billing information form ================= -->


                </div>
            </div>
        </div>
    </div>
</div>


<%
    dbsession.flush();
    dbsession.close();
%>

<%@ include file="../footer.jsp" %>
