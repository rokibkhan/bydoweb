<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    String memberName = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String checkMale = "";
    String checkFemale = "";
    String mobileNo = "";
    String userEmail = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String centerId = "";
    String pictureLink = "";
    String mAddressLine1 = "";
    String mAddressLine2 = "";
    String pAddressLine1 = "";
    String pAddressLine2 = "";
    Member member = null;
    AddressBook addressBook = null;

    q1 = dbsession.createQuery("from Member as member WHERE id=" + memberIdH + " ");

    Object[] object = null;

    for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
        member = (Member) itr.next();

        memberName = member.getMemberName();
        fatherName = member.getFatherName() == null ? "" : member.getFatherName();
        motherName = member.getMotherName() == null ? "" : member.getMotherName();
        placeOfBirth = member.getPlaceOfBirth() == null ? "" : member.getPlaceOfBirth();
        dob = member.getDob();
        gender = member.getGender().trim();
        if (gender.equals("1")) {
            gender = "Male";
            checkMale = " checked";
        } else {
            gender = "Female";
            checkFemale = " checked";
        }
        mobileNo = member.getMobile();
        phone1 = member.getPhone1();
        phone2 = member.getPhone2();
        userEmail = member.getEmailId();
        bloodGroup = member.getBloodGroup();

        pictureLink = "M_38661.jpg";

    }

    dbsession.flush();
    dbsession.close();


%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-user"></i> Edit Personal Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->
                    <p class="content_title_saz pb-2 m-3">Personal Information</p>
                    <div class="row">
                        <div class="col-sm-8">
                            <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberChangePersonalInfoSubmitData.jsp?sessionid=<%=session.getId()%>&act=add" onSubmit="return fromDataSubmitValidation()">

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Name</label>
                                    <div class="col-sm-9">
                                        <input name="memberName" id="memberName" value="<%=memberName%>" type="hidden"> 
                                        <input  value="<%=memberName%>" placeholder="Full name" type="text" class="form-control" disabled="">                                    
                                    </div>
                                </div>
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Father's Name</label>
                                    <div class="col-sm-9">
                                        <input name="fatherName" id="fatherName" value="<%=fatherName%>" placeholder="Father's name" type="text" class="form-control" required="">                                    
                                    </div>
                                </div>

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Mother's Name</label>
                                    <div class="col-sm-9">
                                        <input name="motherName" id="motherName" value="<%=motherName%>" placeholder="Mother's name" type="text" class="form-control" required="">                                    
                                    </div>
                                </div>


                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Email</label>
                                    <div class="col-sm-9">
                                        <input name="memberEmail" id="memberEmail" value="<%=userEmail%>" placeholder="Email address" type="text" class="form-control" required="">                                    
                                    </div>                                    
                                </div>

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Mobile</label>
                                    <div class="col-sm-9">
                                        <input name="memberMobile" id="memberMobile" value="<%=mobileNo%>" placeholder="Mobile number" type="text" class="form-control" required="">                                    
                                    </div>                                     
                                </div>
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Blood Group</label>
                                    <div class="col-sm-9">
                                        <%//=bloodGroup%>
                                        <select name="memberBloodGroup" id="memberBloodGroup" class="form-control" required="">
                                            <option value="">Select Blood Group</option>
                                            <%
                                                String blood1 = "";
                                                String blood2 = "";
                                                String blood3 = "";
                                                String blood4 = "";
                                                String blood5 = "";
                                                String blood6 = "";
                                                String blood7 = "";
                                                String blood8 = "";

                                                if (bloodGroup.equals("O+")) {
                                                    blood1 = " selected";
                                                }
                                                if (bloodGroup.equals("O-")) {
                                                    blood2 = " selected";
                                                }
                                                if (bloodGroup.equals("A+")) {
                                                    blood3 = " selected";
                                                }
                                                if (bloodGroup.equals("A-")) {
                                                    blood4 = " selected";
                                                }
                                                if (bloodGroup.equals("B+")) {
                                                    blood5 = " selected";
                                                }
                                                if (bloodGroup.equals("B-")) {
                                                    blood6 = " selected";
                                                }
                                                if (bloodGroup.equals("AB+")) {
                                                    blood7 = " selected";
                                                }
                                                if (bloodGroup.equals("AB-")) {
                                                    blood8 = " selected";
                                                }

                                            %>
                                            <option value="O+" <%=blood1%>>O+</option>
                                            <option value="O-" <%=blood2%>>O-</option>
                                            <option value="A+" <%=blood3%>>A+</option>
                                            <option value="A-" <%=blood4%>>A-</option>
                                            <option value="B+" <%=blood5%>>B+</option>
                                            <option value="B-" <%=blood6%>>B-</option>
                                            <option value="AB+" <%=blood7%>>AB+</option>
                                            <option value="AB-" <%=blood8%>>AB-</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Gender</label>
                                    <div class="col-sm-9">

                                        <div class="form-check-inline">
                                            <label class="form-check-label" for="radio1">
                                                <input id="customRadioInline1" name="memberGender" value="1" type="radio" class="form-check-input" <%=checkMale%>>Male
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label" for="radio2">
                                                <input id="customRadioInline1" name="memberGender" value="2" type="radio" class="form-check-input" <%=checkFemale%>>Female
                                            </label>
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">&nbsp;</label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-primary btn-sm ">Update</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <!-- ============ End personal information form ================== -->



                </div>
            </div>
        </div>
    </div>
</div>

<br/>                        


<%@ include file="../footer.jsp" %>
