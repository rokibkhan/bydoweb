<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String activityType = "";

    Query q1 = null;
    int memberObjectiveId = 0;
    String memberObjective = "";

    MemberCareerObjective memberCareerObjective = null;

    Query mcbSQL = dbsession.createQuery("from MemberCareerObjective  WHERE member_id=" + memberIdH + " ");

    Object[] object = null;

    if (!mcbSQL.list().isEmpty()) {

        for (Iterator itr = mcbSQL.list().iterator(); itr.hasNext();) {
            memberCareerObjective = (MemberCareerObjective) itr.next();

            memberObjectiveId = memberCareerObjective.getId();
            memberObjective = memberCareerObjective.getObjectives();

        }
        

    activityType = "edit";

    } else {
        activityType = "add";
    }

    System.out.println("Objective::: " + memberObjective);

    dbsession.flush();
    dbsession.close();


%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-user"></i> Add/Edit Objective Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->
                    <p class="content_title_saz pb-2 m-3">Objective Information</p>
                    <div class="row">
                        <div class="col-sm-10">
                            <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberObjectiveInfoSubmitData.jsp?sessionid=<%=session.getId()%>" onSubmit="return fromDataSubmitValidation()">

                                <div class="form-group d-flex align-items-center">                                    
                                    <div class="col-sm-11 offset-sm-1">
                                        <textarea class="form-control" id="memberObjective"  name="memberObjective" rows="9" placeholder="Objective or Short Bio" required>
                                            <%=memberObjective%>
                                        </textarea>
                                    </div>
                                </div>



                                <div class="form-group d-flex align-items-center">

                                    <div class="col-sm-9 offset-sm-1">

                                        <input id="memberObjectiveId" name="memberObjectiveId" type="hidden" value="<%=memberObjectiveId%>">
                                        <input id="act" name="act" type="hidden" value="<%=activityType%>">
                                        <button type="submit" class="btn btn-primary btn-sm ">Update</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                    <!-- ============ End personal information form ================== -->



                </div>
            </div>
        </div>
    </div>
</div>

<br/>                        


<%@ include file="../footer.jsp" %>
