<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query applyJobSQL = null;
    Object objapplyJob[] = null;
    String applyId = "";
    String applyJobId = "";
    String applySalary = "";
    String applyDate = "";
    String jobsDetailsUrl = "";
    String btnDetails = "";

    java.util.Date dob = new Date();
    String yearOfPassing = "";
    String weeks = "";
    int trainingId = 0;
    int m = 1;

    MemberProposerInfoTemp proposerTmp = null;



%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>


<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Applied Job Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->

                    <p class="content_title_saz pb-2 m-3">Apply Jobs Information</p>

                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Apply date</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%

//                                approvalSQL = dbsession.createSQLQuery("SELECT pt.id,pt.member_id tempMemberId,pt.proposer_id,pt.status,pt.request_date "
//                                        + "FROM member_proposer_info_temp pt "
//                                        + "WHERE  pt.proposer_id=" + memberIdH + " ");
                                applyJobSQL = dbsession.createSQLQuery("SELECT mj.id,mj.member_id,mj.job_id,mj.expected_salary,mj.apply_date "
                                        + "FROM member_applied_job mj "
                                        + "WHERE  mj.member_id=" + memberIdH + " ");

                                if (!applyJobSQL.list().isEmpty()) {

                                    for (Iterator itr1 = applyJobSQL.list().iterator(); itr1.hasNext();) {

                                        objapplyJob = (Object[]) itr1.next();
                                        applyId = objapplyJob[0].toString();
                                        applyJobId = objapplyJob[2].toString();

                                        Query us1 = dbsession.createSQLQuery("SELECT * FROM  job_post  WHERE ID = " + applyJobId);

                                        Iterator i1 = us1.list().iterator();
                                        Object[] o1 = (Object[]) i1.next();
                                        String jobName = o1[2].toString();

                                        applySalary = objapplyJob[3].toString();
                                        applyDate = objapplyJob[4].toString();

                                        jobsDetailsUrl = GlobalVariable.baseUrl + "/jobs/jobsDetails.jsp?jobsIdX=" + applyJobId;

                                        btnDetails = "<a  href=\"" + jobsDetailsUrl + "\" title=\"Job Details\" class=\"btn btn-primary btn-sm text-white\" ><i class=\"fa fa-info-circle\"></i> Details</a>&nbsp;";


                            %>
                            <tr>
                                <td><%=m%></td>
                                <td><%=jobName%></td>
                                <td><%=applyDate%></td>
                                <td id="acttionBox<%=applyId%>">

                                    <%=btnDetails%>

                                </td>

                            </tr>

                            <%    m++;
                                    }

                                } else {

                                }

                                dbsession.flush();
                                dbsession.close();
                                %>
                        </tbody>
                    </table>


                    <!-- ============ End Education information Table ================== -->


                </div>
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
