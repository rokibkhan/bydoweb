<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    getRegistryID regId = new getRegistryID();
    String memberSubjectAddId = regId.getID(67); //67-MEM_UNIVERSITY_SUBJECT_ID   

    Query subjectSQL = null;

    Object[] object = null;
    int responseCode = 0;
    String responseMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String subjectInfo = "";
    String univesityNameInfo = "";
    String subjectName = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    String eduMemberId = request.getParameter("eduMemberId") == null ? "" : request.getParameter("eduMemberId").trim();
    String universityId = request.getParameter("eduUniversityId") == null ? "" : request.getParameter("eduUniversityId").trim();
    String eduUniversitySubjectId = request.getParameter("eduUniversitySubjectId") == null ? "" : request.getParameter("eduUniversitySubjectId").trim();
    String eduEducationId = request.getParameter("eduEducationId") == null ? "" : request.getParameter("eduEducationId").trim();

    if (universityId != null) {

        String subjectNameSQL = "select subject_long_name from university_subject  WHERE  subject_id ='" + eduUniversitySubjectId + "'  AND university_id='" + universityId + "'";

        System.out.println("subjectNameSQL :: " + subjectNameSQL);
        subjectName = dbsession.createSQLQuery(subjectNameSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(subjectNameSQL).uniqueResult().toString();

        subjectName = subjectName.toUpperCase();

        Query memberSubjectAddSQL = dbsession.createSQLQuery("INSERT INTO member_subject_info("
                + "id,"
                + "member_id,"
                + "subject_id"
                + ") VALUES("
                + "'" + memberSubjectAddId + "',"
                + "'" + eduMemberId + "',"
                + "'" + eduUniversitySubjectId + "'"
                + "  ) ");

        memberSubjectAddSQL.executeUpdate();

        dbtrx.commit();
        if (dbtrx.wasCommitted()) {

            responseCode = 1;
            responseMsg = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<strong>Success!!!</strong> Subject Info show."
                    + "</div>";
        } else {

            responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<strong>Error!</strong> Please login and try again."
                    + "</div>";

        }

    } else {

        responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Error!</strong> Please login and try again."
                + "</div>";

    }

    json = new JSONObject();
    json.put("eduEducationId", eduEducationId);
    json.put("universityId", universityId);
    json.put("subjectId", eduUniversitySubjectId);
    json.put("subjectName", subjectName);
    json.put("responseCode", responseCode);
    json.put("responseMsg", responseMsg);
    jsonArr.add(json);
    out.print(jsonArr);

    dbsession.flush();
    dbsession.close();

%>