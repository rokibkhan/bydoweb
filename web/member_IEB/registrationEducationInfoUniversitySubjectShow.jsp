<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query subjectSQL = null;
    Query rollCheckSQL = null;
    Object[] object = null;
    int responseCode = 0;
    String responseMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String subjectInfo = "";
    String univesityRollInfo = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    String universityId = request.getParameter("universityId") == null ? "" : request.getParameter("universityId").trim();

    if (universityId != null) {

        String subjectId = "";
        String subjectName = "";
        String subjectDivisionId = "";
        String subjectIdDivisionId = "";
        String subjectOption = "";
        subjectSQL = dbsession.createSQLQuery("SELECT * FROM university_subject WHERE university_id='" + universityId + "' ORDER BY subject_long_name ASC");
        if (!subjectSQL.list().isEmpty()) {
            for (Iterator itr = subjectSQL.list().iterator(); itr.hasNext();) {
                object = (Object[]) itr.next();
                subjectId = object[0].toString();
                subjectName = object[2].toString();
                subjectDivisionId = object[5].toString();

                subjectIdDivisionId = subjectId + "-" + subjectDivisionId;

                subjectOption = subjectOption + "<option value=\"" + subjectIdDivisionId + "\">" + subjectName + "</option>";

            }
        }

        subjectInfo = "<select name=\"memberBSCUniversitySubject\" id=\"memberBSCUniversitySubject\" class=\"form-control chosen\" required>"
                + "<option value=\"\">Select Subject</option>"
                + "" + subjectOption + ""
                + "</select>";

        rollCheckSQL = dbsession.createSQLQuery("SELECT * FROM university WHERE university_id='" + universityId + "' AND check_roll ='1'");
        System.out.println("rollCheckSQL:: " + rollCheckSQL);
        if (!rollCheckSQL.list().isEmpty()) {
            univesityRollInfo = "<input type=\"hidden\" name=\"memberBSCUniversityRollNoCheck\" id=\"memberBSCUniversityRollNoCheck\" value=\"Y\" >"
                    + "<input type=\"text\" name=\"memberBSCUniversityRollNo\" id=\"memberBSCUniversityRollNo\" class=\"form-control\" placeholder=\"Student roll/ID\" required>";
        } else {
            univesityRollInfo = "<input type=\"hidden\" name=\"memberBSCUniversityRollNoCheck\" id=\"memberBSCUniversityRollNoCheck\" value=\"N\" >"
                    + "<input type=\"text\" name=\"memberBSCUniversityRollNo\" id=\"memberBSCUniversityRollNo\" class=\"form-control\" placeholder=\"Student roll/ID\" disabled=\"\">";
        }

        responseCode = 1;
        responseMsg = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Success!!!</strong> Subject Info show."
                + "</div>";

    } else {

        responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Error!</strong> Please login and try again."
                + "</div>";

    }

    json = new JSONObject();
    json.put("universityId", universityId);
    json.put("subjectInfo", subjectInfo);
    json.put("univesityRollInfo", univesityRollInfo);
    json.put("responseCode", responseCode);
    json.put("responseMsg", responseMsg);
    jsonArr.add(json);
    out.print(jsonArr);

%>