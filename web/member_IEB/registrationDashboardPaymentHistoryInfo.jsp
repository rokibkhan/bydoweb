<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String registerAppId = "";
    String memberRegId = "";
    if (session.getAttribute("registerAppId") != null && session.getAttribute("memberRegId") != null) {

        sessionIdH = session.getId();
        registerAppId = session.getAttribute("registerAppId").toString();
        memberRegId = session.getAttribute("memberRegId").toString();

    } else {

        response.sendRedirect(GlobalVariable.baseUrl + "/member/registrationCheck.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    String instituteName = "";
    String boardUniversityName = "";
    java.util.Date dob = new Date();

    String billNo = "";
    String billYear = "";
    String billAmount = "0";
    int status = 0;
    String status1 = "";
    String dueDate = "";
    String paymentDate = "";
    String billType = "";

    String tillDate = "";
    int memberFeeId = 0;

    MemberFeeTemp mpi = null;

%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);
    %>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="registrationLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Payment Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%
                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->


                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Bill No.</th>
                                <th scope="col">Payment Date</th>
                                <th scope="col">Bill Type</th>
                                <th scope="col">Bill Amount</th>
                                <th scope="col">View Bill</th>
                                <th scope="col">Status</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                int i = 1;
                                String payOnline = "";
                                q1 = dbsession.createQuery("from MemberFeeTemp WHERE  member_id=" + registerAppId + " ");

                                for (Iterator itr = q1.list().iterator();
                                        itr.hasNext();) {
                                    mpi = (MemberFeeTemp) itr.next();
                                    memberFeeId = mpi.getTxnId();
                                    billAmount = mpi.getAmount() == null ? "0" : mpi.getAmount().toString();

                                    //   billYear = mpi.getMemberFeesYear().getMemberFeesYearName() == null ? "" : mpi.getMemberFeesYear().getMemberFeesYearName();
                                    billYear = "";

                                    status = mpi.getStatus();
                                    if (status == 1) {
                                        status1 = "Paid";
                                        payOnline = "";
                                    } else {
                                        status1 = "Fail";
                                        // payOnline = "<a title=\"Pay Online\" href=\"" + GlobalVariable.baseUrl + "/member/memberPaymentProcessing.jsp?sessionid=" + session.getId() + "&act=add&&memberFeeId=" + memberFeeId + "\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-credit-card\"></i></a>";
                                    }
                                    billType = mpi.getBillType();
                                    dueDate = mpi.getDueDate() == null ? "" : mpi.getDueDate().toString();
                                    paymentDate = mpi.getPaidDate() == null ? "" : mpi.getPaidDate().toString();
                                    //<a title="View Bill" href="GlobalVariable.baseUrl/member/memberProjectExperiencesInfoSubmitData.jsp?sessionid=<%=session.getId()memberFeeId=<%=memberFeeId"  ><i class="fa fa-file"></i> </a> 
                            %>
                            <tr>
                                <td><%=i%></td>
                                <td><%=memberFeeId%></td>
                                <td><%=paymentDate%></td>
                                <td><%=billType%></td> 
                                <td><%=billAmount%></td>
                                <td style="cursor:pointer">

                                    <a title="View Bill" href="#"><i class="fa fa-file"></i> </a> 

                                </td>

                                <td><%=status1%></td> 

                            </tr>

                            <%                                i++;
                                }
                                dbsession.flush();
                                dbsession.close();

                            %>
                        </tbody>
                    </table>


                    <!-- ============ End Education information Table ================== -->


                </div>
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
