
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>



<%
    Session dbsession = null;
    String qryparam = "";;

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    getDate date = new getDate();

//    if (session.isNew()) {
//        response.sendRedirect("logout.jsp");
//    }
    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        memberIdH = session.getAttribute("memberId").toString();

        System.out.println("memberIdH " + memberIdH);

        getRegistryID regId = new getRegistryID();

        MemberTrainingInfo memberTrainingInfo = null;
        String act = request.getParameter("act").trim();

        if (act.equals("edit")) {
            String trainingId = request.getParameter("trainingId").trim();
            Query q4 = dbsession.createQuery("from MemberTrainingInfo where id='" + trainingId + "' ");
            Iterator itr4 = q4.list().iterator();
            if (itr4.hasNext()) {
                memberTrainingInfo = (MemberTrainingInfo) itr4.next();
            }

            String trainingTitle = request.getParameter("trainingTitle").trim();
            String trainingInstitute = request.getParameter("trainingInstitute").trim();
            String trainingCourse = request.getParameter("trainingCourse").trim();
            String trainingDuration = request.getParameter("trainingDuration").trim();
            String trainingYear = request.getParameter("trainingYear").trim();
//            memberTrainingInfo = new MemberTrainingInfo();
            Member member = new Member();
            int memberId1 = 0;
            String memberId = session.getAttribute("memberId").toString();
            memberId1 = Integer.parseInt(memberId);

            member.setId(memberId1);
            memberTrainingInfo.setMember(member);
            memberTrainingInfo.setMemberTrainingCourse(trainingCourse);
            memberTrainingInfo.setMemberTrainingDuration(trainingDuration);
            memberTrainingInfo.setMemberTrainingInstitute(trainingInstitute);
            memberTrainingInfo.setMemberTrainingTitle(trainingTitle);
            memberTrainingInfo.setMemberTrainingYear(trainingYear);
            dbsession.update(memberTrainingInfo);
            strMsg = "Information updated successfully.";
        } else if (act.equals("add")) {

            String trainingTitle = request.getParameter("trainingTitle").trim();
            String trainingInstitute = request.getParameter("trainingInstitute").trim();
            String trainingCourse = request.getParameter("trainingCourse").trim();
            String trainingDuration = request.getParameter("trainingDuration").trim();
            String trainingYear = request.getParameter("trainingYear").trim();
            String trainingId = regId.getID(36);
            memberTrainingInfo = new MemberTrainingInfo();
            Member member = new Member();
            int memberId1 = 0;
            int trainingId1 = 0;
            String memberId = session.getAttribute("memberId").toString();
            memberId1 = Integer.parseInt(memberId);
            trainingId1 = Integer.parseInt(trainingId);

            member.setId(memberId1);
            memberTrainingInfo.setMember(member);
            memberTrainingInfo.setId(trainingId1);
            memberTrainingInfo.setMemberTrainingCourse(trainingCourse);
            memberTrainingInfo.setMemberTrainingDuration(trainingDuration);
            memberTrainingInfo.setMemberTrainingInstitute(trainingInstitute);
            memberTrainingInfo.setMemberTrainingTitle(trainingTitle);
            memberTrainingInfo.setMemberTrainingYear(trainingYear);
            dbsession.save(memberTrainingInfo);
            strMsg = "Information saved successfully.";

        } else if (act.equals("del")) {
            String trainingId = request.getParameter("trainingId").trim();
            Query q4 = dbsession.createQuery("delete from MemberTrainingInfo where id='" + trainingId + "' ");
            q4.executeUpdate();
            strMsg = "Information removed successfully.";
        }

        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
//            strMsg = "Success!!! Member Training Information added ";
            response.sendRedirect("memberTrainingInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        } else {
            strMsg = "Error!!! When Training Information added ";
            dbtrx.rollback();
            response.sendRedirect("memberTrainingInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        }
    } else {
        System.out.println("LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

%>
