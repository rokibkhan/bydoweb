<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    String instituteName = "";
    String boardUniversityName = "";
    java.util.Date dob = new Date();
    String yearOfPassing = "";
    String resultTypeName = "";

    int publicationId = 0;
    String publicationTitle = "";
    String publicationAuthor = "";
    String publicationYear = "";
    String publicationJournal = "";
    MemberPublicationInfo mpi = null;



%>


<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>

<script type="text/javascript">
    function publicationDeleteInfo(arg1, arg2) {

        console.log("publicationDeleteInfo -> arg1 :: " + arg1 + " arg2:: " + arg2);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnDeleteComfirmation\" onclick=\"publicationDeleteInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";


        memberLGModal = $('#memberLGModal');
        //   memberLGModal = $('#taskLGModal');
        memberLGModal.find("#memberLGModalTitle").text("Publication Delete Confirmation");
        memberLGModal.find("#memberLGModalBody").html("");
        memberLGModal.find("#memberLGModalFooter").html(btnInvInfo);
        memberLGModal.modal('show');


    }



    function publicationDeleteInfoConfirm(arg1, arg2) {
        console.log("publicationDeleteInfoConfirm:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("publicationDeleteProcess.jsp", {deleteId: arg1, sessionid: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#infoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                memberLGModal.modal('hide');
            } else {

            }


        }, "json");

    }

</script>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Publications Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->


                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                        <thead>
                            <tr>
                                <th scope="col">Title</th>
                                <th scope="col">Year</th>
                                <th scope="col">Journal/Conference</th>
                                <th scope="col">Author</th>
                                <th scope="col">Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%

                                q1 = dbsession.createQuery("from MemberPublicationInfo WHERE  member_id=" + memberIdH + " ");

                                for (Iterator itr = q1.list().iterator();
                                        itr.hasNext();) {
                                    mpi = (MemberPublicationInfo) itr.next();
                                    publicationId = mpi.getId();
                                    publicationTitle = mpi.getPublicationTitle() == null ? "" : mpi.getPublicationTitle();

                                    publicationYear = mpi.getPublicationYear() == null ? "" : mpi.getPublicationYear();
                                    publicationJournal = mpi.getPublicationJournalConference() == null ? "" : mpi.getPublicationJournalConference();
                                    publicationAuthor = mpi.getPublicationAuthor() == null ? "" : mpi.getPublicationAuthor().toString();


                            %>
                             <tr id="infoBox<%=publicationId%>">
                                <td><%=publicationTitle%></td>
                                <td><%=publicationYear%></td> 
                                <td><%=publicationJournal%></td>
                                <td><%=publicationAuthor%></td>
                                <td style="cursor:pointer"><a title="Edit" href="<%out.print(GlobalVariable.baseUrl);%>/member/memberPublicationsInfoEdit.jsp?sessionid=<%=session.getId()%>&act=edit&publicationId=<%=publicationId%>"  ><i class="fa fa-edit"></i> </a> 
                                    <a title="Remove" onclick="publicationDeleteInfo(<% out.print("'" + publicationId + "','" + sessionIdH + "'");%>)" ><i class="fa fa-trash"></i> </a> 

                            </tr>

                            <%                                }

                                dbsession.flush();
                                dbsession.close();

                            %>
                        </tbody>
                    </table>


                    <!-- ============ End Publication information Table ================== -->

                    <div class="container">
                        <div class="row my-4">
                            <div class="col-12">
                                <div class="card-header panel_heading_saz" style="padding-top: 4px; padding-bottom: 4px;">
                                    <i class="fa fa-list-alt"></i> Add Publications Information
                                </div>
                            </div>

                        </div>
                    </div>

                    <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberPublicationsInfoSubmitData.jsp?sessionid=<%=session.getId()%>&act=add" onSubmit="return fromDataSubmitValidation()">
                        
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right" style="padding-top: 0;">*Journal :</label>
                            <div class="col-sm-6">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="publicationRadioJournal" id="publicationRadioJournal" value="Journal ">
                                    <label class="form-check-label" for="gridRadios1">
                                        Journal 
                                    </label>

                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="publicationRadioJournal" id="publicationRadioJournal" value="Conference">
                                    <label class="form-check-label" for="gridRadios2">
                                        Conference
                                    </label>

                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Publication Title :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="publicationTitle" name="publicationTitle" placeholder="Publication Title">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-3 col-form-label text-right">*Authors :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="publicationAuthor" name="publicationAuthor"  placeholder="Authors">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Journal/Conference Name:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="publicationJournalCon" name="publicationJournalCon"  placeholder="Journal/Conference Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Volume :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="publicationVolume" name="publicationVolume" placeholder="Volume">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Number :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="publicationNumber"  name="publicationNumber"  placeholder="Number">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Page :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="publicationPage" name="publicationPage"   placeholder="Page">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Year :  :</label>
                            <div class="col-sm-6">
                                <select id="publicationYear" name="publicationYear"   class="custom-select">
                                    <option value="year" selected>Year</option>
                                    <option value="1960">1960</option>
                                    <option value="1961">1961</option>
                                    <option value="1962">1962</option>
                                    <option value="1963">1963</option>
                                    <option value="1964">1964</option>
                                    <option value="1965">1965</option>
                                    <option value="1966">1966</option>
                                    <option value="1967">1967</option>
                                    <option value="1968">1968</option>
                                    <option value="1969">1969</option>
                                    <option value="1970">1970</option>
                                    <option value="1971">1971</option>
                                    <option value="1972">1972</option>
                                    <option value="1973">1973</option>
                                    <option value="1974">1974</option>
                                    <option value="1975">1975</option>
                                    <option value="1976">1976</option>
                                    <option value="1977">1977</option>
                                    <option value="1978">1978</option>
                                    <option value="1979">1979</option>
                                    <option value="1980">1980</option>
                                    <option value="1981">1981</option>
                                    <option value="1982">1982</option>
                                    <option value="1983">1983</option>
                                    <option value="1984">1984</option>
                                    <option value="1985">1985</option>
                                    <option value="1986">1986</option>
                                    <option value="1987">1987</option>
                                    <option value="1988">1988</option>
                                    <option value="1989">1989</option>
                                    <option value="1990">1990</option>
                                    <option value="1991">1991</option>
                                    <option value="1992">1992</option>
                                    <option value="1993">1993</option>
                                    <option value="1994">1994</option>
                                    <option value="1995">1995</option>
                                    <option value="1996">1996</option>
                                    <option value="1997">1997</option>
                                    <option value="1998">1998</option>
                                    <option value="1999">1999</option>
                                    <option value="2000">2000</option>
                                    <option value="2001">2001</option>
                                    <option value="2002">2002</option>
                                    <option value="2003">2003</option>
                                    <option value="2004">2004</option>
                                    <option value="2005">2005</option>
                                    <option value="2006">2006</option>
                                    <option value="2007">2007</option>
                                    <option value="2008">2008</option>
                                    <option value="2009">2009</option>
                                    <option value="2010">2010</option>
                                    <option value="2011">2011</option>
                                    <option value="2012">2012</option>
                                    <option value="2013">2013</option>
                                    <option value="2014">2014</option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>									
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Month :</label>
                            <div class="col-sm-6">
                                <select id="publicationMonth" name="publicationMonth"   class="custom-select">
                                    <option value="month" selected>Month</option>
                                    <option value="Jan">Jan</option>
                                    <option value="Feb">Feb</option>
                                    <option value="Mar">Mar</option>
                                    <option value="Apr">Apr</option>
                                    <option value="May">May</option>
                                    <option value="Jun">Jun</option>
                                    <option value="Jul">Jul</option>
                                    <option value="Aug">Aug</option>
                                    <option value="Sep">Sep</option>
                                    <option value="Oct">Oct</option>
                                    <option value="Nov">Nov</option>
                                    <option value="Dec">Dec</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Publisher :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="publicationPublisher" name="publicationPublisher"   placeholder="Publisher">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Publication URL :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="publicationURL" name="publicationURL"   placeholder="Publication URL">
                            </div>
                        </div>

                       

                        <div class="form-group row">

                            <div class="col-sm-6 offset-sm-3">
                                <button type="submit" class="btn btn-primary mr-3">Submit</button>
                                <button type="button" class="btn btn-primary">Reset</button>
                            </div>
                        </div>
                    </form>

                    <!-- ============ End Education information Table ================== -->

                </div>
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
