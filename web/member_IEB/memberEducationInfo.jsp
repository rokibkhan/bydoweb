<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query educationSQL = null;
    Query q2 = null;
    int degreeId = 0;
    String degreeName = "";
    String degreeName1 = "";
    String subjectName = "";
    String subjectNameBtn = "";
    String instituteName = "";
    int boardUniversityId = 0;
    String boardUniversityName = "";
    String boardUniversityName1 = "";
    java.util.Date dob = new Date();
    String yearOfPassing = "";
    String resultTypeName = "";
    String result = "";

    MemberEducationInfo mei = null;
    int subjectDivisionId = 0;
    String subjectDivisionName = "";
    String subjectDivisionFullName = "";

    educationSQL = dbsession.createQuery("from MemberEducationInfo where  member_id=" + memberIdH + " ");

    Member member = null;
    String memberSubdivisionName = "";
    String memberSubdivisionFullName = "";
    int educationfInfoId = 0;

    SubDivision subj = null;
    Query subjectDivisionSQL = null;

    String eduEditBtn = "";


%>


<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>


<br>
<script type="text/javascript">
    function educationDeleteInfo(arg1, arg2) {

        console.log("educationDeleteInfo -> arg1 :: " + arg1 + " arg2:: " + arg2);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnDeleteComfirmation\" onclick=\"educationDeleteInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";


        memberLGModal = $('#memberLGModal');
        memberLGModal.find("#memberLGModalTitle").text("Education Delete Confirmation");
        memberLGModal.find("#memberLGModalBody").html("");
        memberLGModal.find("#memberLGModalFooter").html(btnInvInfo);
        memberLGModal.modal('show');
    }



    function educationDeleteInfoConfirm(arg1, arg2) {
        console.log("educationDeleteInfoConfirm:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("educationDeleteProcess.jsp", {deleteId: arg1, sessionid: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#infoBox" + data[0].selectInvId).html("<td colspan=\"6\" style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                memberLGModal.modal('hide');
            } else {

            }


        }, "json");

    }


    function showUniversityBoardInfo(arg1, arg2) {
        console.log("showUniversityBoardSubjectInfo:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("educationUnivrsityInfoShow.jsp", {sessionid: arg1, degreeTypeId: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#infoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                //  memberLGModal.modal('hide');
            }


        }, "json");

    }


    function memberEducationBscSubjetInfoAdd(arg1, arg2, arg3, arg4) {

        console.log("memberEducationBscSubjetInfoAdd -> arg1 :: " + arg1 + " arg2:: " + arg2 + " arg3:: " + arg3 + " arg4:: " + arg4);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        memberLGModal = $('#rupantorLGModal');
        memberLGModal.find("#rupantorLGModalTitle").text("Add B.Sc Subject Information");

        $.post("memberEducationUnivrsitySubjetInfoShow.jsp", {sessionid: arg1, eduMemberId: arg2, eduUniversityId: arg3, eduEducationId: arg4}, function (data) {

            console.log(data);


            memberLGModal.find("#rupantorLGModalBody").html(data[0].universitySubjectInfo);
            memberLGModal.find("#rupantorLGModalFooter").html(data[0].btnInvInfo);


            memberLGModal.modal('show');


            //  if (data[0].responseMsgCode == 1) {
            //   $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
            //     $("#infoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
            //  memberLGModal.modal('hide');
            //  }


        }, "json");


    }


    function memberEducationBscSubjetInfoAddConfirm(arg1, arg2, arg3, arg4) {

        console.log("memberEducationBscSubjetInfoAddConfirm -> arg1 :: " + arg1 + " arg2:: " + arg2 + " arg3:: " + arg3);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;


        memberLGModal = $('#rupantorLGModal');



        var memberBSCUniversitySubjectId = document.getElementById("memberBSCUniversitySubjectId").value;

        if (memberBSCUniversitySubjectId != '') {


            $.post("memberEducationUnivrsitySubjetInfoAddConfirm.jsp", {sessionid: arg1, eduMemberId: arg2, eduUniversityId: arg3, eduEducationId: arg4, eduUniversitySubjectId: memberBSCUniversitySubjectId}, function (data) {

                console.log(data);



                if (data[0].responseCode == 1) {
                    $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                    //  $("#infoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                    $("#infoBoxSubject" + data[0].eduEducationId).html(data[0].subjectName);

                    memberLGModal.modal('hide');
                }


            }, "json");

        }

    }

</script>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Education Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->



                    <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                        <thead>
                            <tr>
                                <th scope="col">Degree</th>
                                <th scope="col">Subject</th>
                                <th scope="col">Board/University</th>
                                <th scope="col">Year of Passing</th>
                                <th scope="col">Score/Class</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <%

                                for (Iterator itr = educationSQL.list().iterator(); itr.hasNext();) {
                                    mei = (MemberEducationInfo) itr.next();

                                    educationfInfoId = mei.getId();
                                    degreeId = mei.getDegree().getDegreeId();
                                    degreeName1 = mei.getDegree().getDegreeName();
                                    degreeName = degreeName1.toUpperCase();

                                    instituteName = mei.getInstituteName() == null ? "" : mei.getInstituteName();

                                    boardUniversityId = mei.getUniversity().getUniversityId();

                                    //    boardUniversityName = mei.getUniversity().getUniversityLongName();
                                    boardUniversityName1 = mei.getUniversity().getUniversityLongName();
                                    boardUniversityName = boardUniversityName1.toUpperCase();
                                    yearOfPassing = mei.getYearOfPassing();
                                    resultTypeName = mei.getResultType().getResultTypeName();
                                    result = mei.getResult();

                                    //  degreeName1 = degreeName + memberSubdivisionFullName;
                                    //    degreeName1 = degreeName;
                                    //    degreeName = degreeName1.toUpperCase();
                                    Object[] subjectObject = null;

                                    //  String subjectName = "";
                                    String subjectUniversityName = "";

                                    Query subjectSQL = dbsession.createSQLQuery("SELECT usb.subject_long_name,u.university_long_name FROM "
                                            + "member_subject_info msb,university_subject usb,university u "
                                            + "WHERE msb.subject_id = usb.subject_id "
                                            + "AND usb.university_id = u.university_id "
                                            + "AND msb.member_id='" + memberIdH + "'");

                                    //   System.out.println("subjectSQL :: " + subjectSQL);
                                    if (!subjectSQL.list().isEmpty()) {

                                        for (Iterator subjectItr = subjectSQL.list().iterator(); subjectItr.hasNext();) {

                                            subjectObject = (Object[]) subjectItr.next();

                                            subjectName = subjectObject[0].toString();

                                            subjectUniversityName = subjectObject[1].toString();
                                        }

                                    }

                                    if (degreeId == 3) {
                                        if (subjectName.equals("")) {
                                            subjectNameBtn = "<a class=\"btn btn-primary btn-sm text-white\" title=\"Add Subject\" onclick=\"memberEducationBscSubjetInfoAdd('" + sessionIdH + "','" + memberIdH + "','" + boardUniversityId + "','" + educationfInfoId + "')\" ><i class=\"fa fa-plus\"> Add subject</a>";
                                        } else {
                                            subjectNameBtn = subjectName.toUpperCase();
                                        }
                                    } else {
                                        subjectNameBtn = "";
                                    }

                                    //    eduEditBtn = "<a title=\"Edit\" href=\" " + GlobalVariable.baseUrl + "/member/memberEducationInfoEdit.jsp?sessionid=" + session.getId() + "&act=add&educationfInfoId=" + educationfInfoId + "\"  ><i class=\"fa fa-edit\"></i> </a>";

                            %>
                            <tr id="infoBox<%=educationfInfoId%>">
                                <td><%=degreeName%></td>
                                <td id="infoBoxSubject<%=educationfInfoId%>"><%=subjectNameBtn%></td>
                                <td><%=boardUniversityName%></td>
                                <td><%=yearOfPassing%></td>
                                <td><%=result%></td>
                                <td style="cursor:pointer">

                                    <%=eduEditBtn%>
                                    <%
                                        if (degreeId != 3) {
                                    %>
                                    <a title="Remove" onclick="educationDeleteInfo(<% out.print("'" + educationfInfoId + "','" + sessionIdH + "'");%>)" ><i class="fa fa-trash"></i> </a> 

                                    <% } %>
                                </td>

                            </tr>
                            <%
                                }


                            %>

                        </tbody>
                    </table>


                    <!-- ============ End Education information Table ================== -->

                    <div class="container">
                        <div class="row my-4">
                            <div class="col-12">
                                <div class="card-header panel_heading_saz" style="padding-top: 4px; padding-bottom: 4px;">
                                    <i class="fa fa-list-alt"></i> Add Education Information
                                </div>
                            </div>

                        </div>
                    </div>



                    <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberEducationInfoSubmitData.jsp?sessionid=<%=session.getId()%>&act=add" onSubmit="return fromDataSubmitValidation()">

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">Degree :</label>
                            <div class="col-sm-6">
                                <select name="memberDegreeTypeId" id="memberDegreeTypeId" onchange="showUniversityBoardSubjectInfo('<%=sessionIdH%>', this.value)" class="form-control" required="">
                                    <option value="">Select Degree</option>
                                    <%
                                        Object[] object5 = null;
                                        String degreeIdX = "";
                                        String degreeNameX = "";
                                        Query q5 = dbsession.createSQLQuery("select * FROM degree ORDER BY degree_id DESC");
                                        for (Iterator itr5 = q5.list().iterator(); itr5.hasNext();) {
                                            object5 = (Object[]) itr5.next();
                                            degreeIdX = object5[0].toString();
                                            degreeNameX = object5[1].toString();
                                    %>
                                    <option value="<%=degreeIdX%>"><%=degreeNameX%></option>

                                    <%
                                        }

                                    %>

                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Institution :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="institutionName" name="institutionName" placeholder="Institution" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Board/University :</label>
                            <div class="col-sm-6">
                                <select name="memberBoardUniversity" id="memberBoardUniversity" class="form-control chosen" required="">
                                    <option value="">Select Board/University</option>
                                    <%                                        Object[] object6 = null;
                                        String universityId = "";
                                        String universityShortName = "";
                                        String universityLongName = "";
                                        String universityName = "";
                                        String universityName1 = "";
                                        String degreeNameX1 = "";
                                        Query q6 = dbsession.createSQLQuery("select * FROM university ORDER BY university_short_name ASC");
                                        for (Iterator itr6 = q6.list().iterator(); itr6.hasNext();) {
                                            object6 = (Object[]) itr6.next();
                                            universityId = object6[0].toString();
                                            universityShortName = object6[1].toString();
                                            universityLongName = object6[2].toString() == null ? "" : object6[2].toString();

                                            if (universityLongName.equals("")) {
                                                universityName = universityShortName;
                                            } else {
                                                universityName = universityLongName + "(" + universityShortName + ")";
                                            }

                                            universityName1 = universityName.toUpperCase();

                                    %>
                                    <option value="<%=universityId%>"><%=universityName1%></option>

                                    <%
                                        }

                                    %>

                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Year of Passing :</label>
                            <div class="col-sm-6">
                                <select id="memberPassingYear" name="memberPassingYear"   class="custom-select" required="">
                                    <option value="" selected>Year</option>

                                    <%                                        DateFormat formatterFileYear = new SimpleDateFormat("yyyy");
                                        Date dateFileToday = new Date();
                                        String currentYear1 = formatterFileYear.format(dateFileToday);
                                        int currentYear = Integer.parseInt(currentYear1);

                                        System.out.println("currentYear :: " + currentYear);

                                        int y = 0;
                                        for (y = currentYear; y >= 1960; y--) {
                                    %>
                                    <option value="<%=y%>"><%=y%></option>

                                    <%
                                        }

                                    %>

                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Result :</label>
                            <div class="col-sm-3">          
                                <select name="memberResultTypeId" id="memberResultTypeId" class="form-control" required="">
                                    <option value="">Select Result Type </option>
                                    <%                                        Object[] object7 = null;
                                        String resultTypeIdX = "";
                                        String resultTypeNameX = "";
                                        Query q7 = dbsession.createSQLQuery("select * FROM result_type ORDER BY result_type_id ASC");
                                        for (Iterator itr7 = q7.list().iterator(); itr7.hasNext();) {
                                            object7 = (Object[]) itr7.next();
                                            resultTypeIdX = object7[0].toString();
                                            resultTypeNameX = object7[1].toString();
                                    %>
                                    <option value="<%=resultTypeIdX%>"><%=resultTypeNameX%></option>

                                    <%
                                        }

                                        dbsession.flush();
                                        dbsession.close();
                                    %>

                                </select>
                            </div>
                            <div class="col-sm-3">
                                <input name="memberScoreClass" id="memberScoreClass"  type="text" class="form-control" placeholder="Result Score/Class"  required>

                            </div>
                        </div>


                        <div class="form-group row">

                            <div class="col-sm-6 offset-sm-3">
                                <button type="submit" class="btn btn-primary mr-3">Submit</button>
                                <button type="button" class="btn btn-primary">Reset</button>
                            </div>
                        </div>



                    </form>
                    <!-- ================ End billing information form ================= -->

                </div>
            </div>
        </div>
    </div>
</div>




<%@ include file="../footer.jsp" %>
