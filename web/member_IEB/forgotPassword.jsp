<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);

    }

  //  System.out.println("Header sessionIdH :: " + sessionIdH);
 //   System.out.println("Header userNameH :: " + userNameH);
 //   System.out.println("Header userStoreIdH :: " + memberIdH);

//    String sessionid = request.getParameter("sessionid").trim();
//
//    sessionid = session.getId();
//    String uid = session.getAttribute("username").toString().toUpperCase();    
//    String userStoreId = session.getAttribute("memberId").toString();
//    String sessionidH = request.getParameter("sessionid").trim();
//
//    sessionidH = session.getId();
//    String uidH = session.getAttribute("username").toString().toUpperCase();    
    //   String userStoreIdH = session.getAttribute("memberId").toString();
//System.out.println("uidH :: "+uidH);
    

%>
<!-- Font Awesome CSS -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

<!-- Login & Register & About CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about.css">




<!-- Starting of login form -->
<section id="home-section">
    <div class="dark-overlay">
        <div class="home-inner-Tahajjatt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 d-none d-lg-block">
                        <h1 class="display-4">Forgot Password Instructions</h1>
                        <div class="d-flex flex-row">
                            <div class="p-4 align-self-start">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="p-4 align-self-end lead">
                                Member ID format Example: If you are Fellow, Member or Associate Member,  Member ID Should start with F, M or A, then put the numerical character without using and special characters like (/,-, space). Example: F54787. If your ID number has only 4 digits numerical character, then just add a "0" before the numerical character and after F, M or A, Example: M09239.
                            </div>
                        </div>

                        <div class="d-flex flex-row">
                            <div class="p-4 align-self-start">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="p-4 align-self-end lead">
                                Use the phone number, Which one you used to apply for membership.
                            </div>
                        </div>

                        <div class="d-flex flex-row">
                            <div class="p-4 align-self-start">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="p-4 align-self-end lead">
                                If you don't know the email address or Phone number then contact with our Administrator at- (Phone Number) to Recover Your Account. 
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 bg-primary1" style="background-color: #fff;">
                        <div class="login-form">
                            <div class="card m-3 bg-primary1" style="background-color: #fff;">
                                <div class="card-body text-center">
                                    <h2 class="card-title text-blue" style="font-size: 35px;"> Forgot Password</h2>
                                    <!--                                    <p class="card-text">Please enter your email and password</p>-->
                                </div>
                            </div>
                            <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/fotgotPasswordSubmitData.jsp">
                                <div class ="row globalAlertInfoBoxConParentTT">
                                    <%
                                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                                        String msgDispalyConT, msgInfoText, sLinkOpt;
                                        if (!strMsg.equals("")) {
                                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                                            msgInfoText = strMsg;

                                        } else {
                                            msgDispalyConT = "style=\"display: none;\"";
                                            msgInfoText = "";
                                        }
                                    %>

                                    <!-- .globalAlertInfoBoxConTT start -->
                                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                                            <%=msgInfoText%>
                                        </div>
                                    </div>
                                    <!-- .globalAlertInfoBoxConTT end -->

                                </div>
                                <div class="form-group text-center">
                                    <input id="memberID" name="memberID" type="text" class="form-control"  placeholder="Member ID (Ex. F01234, M12345, A12345)" style="width: 300px; height: 35px; padding: 7px;">
                                </div>

                                <div class="form-group text-center">
                                    <input id="memberEmail" name="memberEmail" type="text" class="form-control"  placeholder="Email Address" style="width: 300px; height: 35px; padding: 7px;">
                                </div>
                                <div class="form-group text-center">
                                    <input id="memberMobile" name="memberMobile" type="text" class="form-control"  placeholder="Mobile Number (Ex. 015XXXXXXXX)" style="width: 300px; height: 35px; padding: 7px;">
                                </div>
                                <div class="form-group text-center">
                                    <button style="padding-top: .275rem; padding-bottom: .275rem;" type="submit" class="btn btn-primary mx-auto font-weight-bold text-white">Submit</button>
                                </div>
                                <div class="form-group text-left" style="margin-bottom: 10px; padding-left: 27px;">
                                    <a class="text-blue" href="<%=GlobalVariable.baseUrl%>/member/login.jsp" style="text-decoration: none;">Login</a>
                                </div>
                                <div class="form-group text-left"  style="padding-left: 27px;">
                                    <a class="text-blue" href="<%=GlobalVariable.baseUrl%>/member/registration.jsp" style="text-decoration: none;">Apply Membership</a>
                                </div>

                            </form>


                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</section>

<!-- Ending of login form -->


<%@ include file="../footer.jsp" %>
