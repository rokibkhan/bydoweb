<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    String instituteName = "";
    String boardUniversityName = "";
    java.util.Date dob = new Date();

    String projectStartDate = "";
    String projectEndDate = "";
    String pictureLink = "";
    String projectTitle = "";
    String projectCategory = "";
    String memberRole = "";
    String projectDetails = "";

    String tillDate = "";

    String projectInfoId = request.getParameter("projectInfoId") == null ? "" : request.getParameter("projectInfoId").trim();
    MemberProjectInfo mpi = null;


%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Project Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->
 

                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                      
                        <tbody>
                            <%

                                q1 = dbsession.createQuery("from MemberProjectInfo WHERE  member_id=" + memberIdH + " and id='" + projectInfoId + "'  ");

                                for (Iterator itr = q1.list().iterator();
                                        itr.hasNext();) {
                                    mpi = (MemberProjectInfo) itr.next();
                                    projectTitle = mpi.getMemberProjectTitle() == null ? "" : mpi.getMemberProjectTitle();

                                    projectCategory = mpi.getMemberProjectCategory() == null ? "" : mpi.getMemberProjectCategory();
                                    projectDetails = mpi.getMemberProjectDetails() == null ? "" : mpi.getMemberProjectDetails();
                                    memberRole = mpi.getMemberProjectMemberRole() == null ? "" : mpi.getMemberProjectMemberRole();
                                    projectStartDate = mpi.getMemberProjectStartDate().toString() == null ? "" : mpi.getMemberProjectStartDate().toString();
                                    projectEndDate = mpi.getMemberProjectEndDate().toString() == null ? "" : mpi.getMemberProjectEndDate().toString();

                                }

                                dbsession.flush();
                                dbsession.close();

                            %>
                        </tbody>
                    </table>


                    <!-- ============ End Education information Table ================== -->

                    <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberProjectExperiencesInfoSubmitData.jsp?sessionid=<%=session.getId()%>&act=edit" onSubmit="return fromDataSubmitValidation()">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label mx-3">*Name of the Project :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="projectName" name="projectName" placeholder="Name of the Project" required="" value="<%=projectTitle%>">
                                <input type="hidden" id="projectInfoId" name="projectInfoId" value="<%=projectInfoId%>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-3 col-form-label mx-3">*Member Role :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="memberRole" name="memberRole"  placeholder="Member Role" required="" value="<%=memberRole%>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-3 col-form-label mx-3">*Category :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="projectCategory" name="projectCategory"  placeholder="Category" required="" value="<%=projectCategory%>">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label mx-3">*Description :<p class="text-muted">(Maximum 200 characters)</p></label>
                            <div class="col-sm-6">
                                <textarea class="form-control" id="projectDetails" name="projectDetails" maxlength="200" required value="<%=projectDetails%>"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

                            <label for="inputEmail3" class="col-sm-3 col-form-label mx-3">*Start Date :</label>
                            <div class="col-sm-6">

                                <input type="text" class="form-control" id="startDate" name="startDate"  placeholder="yyyy-mm-dd" required="" value="<%=projectStartDate%>"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                            </div>
                            <!-- Date Picker Plugin JavaScript -->
                            <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
                            <script type="text/javascript">
                        jQuery('#startDate').datepicker({
                            autoclose: true,
                            todayHighlight: true,
                            format: 'yyyy-mm-dd'
                        });
                            </script>  
                        </div>

                        <div class="form-group row">
                            <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

                            <label for="inputEmail3" class="col-sm-3 col-form-label mx-3">*End Date :</label>
                            <div class="col-sm-6">

                                <input type="text" class="form-control" id="endDate" name="endDate"  placeholder="yyyy-mm-dd" required="" value="<%=projectEndDate%>"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                            </div>
                            <!-- Date Picker Plugin JavaScript -->
                            <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
                            <script type="text/javascript">
                        jQuery('#endDate').datepicker({
                            autoclose: true,
                            todayHighlight: true,
                            format: 'yyyy-mm-dd'
                        });
                            </script>  
                        </div>



                        <div class="form-group row text-right ml-5">
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary mr-3">Submit</button>
                                <button type="button" class="btn btn-primary">Reset</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
