<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    String instituteName = "";
    String boardUniversityName = "";
    java.util.Date dob = new Date();
    String yearOfPassing = "";
    String resultTypeName = "";

    int conferenceId = 0;
    String conferenceTitle = "";
    String conferenceRole = "";
    String conferenceStartDate = "";
    String conferenceEndDate = "";
    String conferenceDetails = "";
    String conferenceUrl = "";
    MemberConferenceInfo mci = null;



%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script> 
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    function conferenceDeleteInfo(arg1, arg2) {

        console.log("conferenceDeleteInfo -> arg1 :: " + arg1 + " arg2:: " + arg2);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnDeleteComfirmation\" onclick=\"conferenceDeleteInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";


        memberLGModal = $('#memberLGModal');
        //   memberLGModal = $('#taskLGModal');
        memberLGModal.find("#memberLGModalTitle").text("Conferance Delete Confirmation");
        memberLGModal.find("#memberLGModalBody").html("");
        memberLGModal.find("#memberLGModalFooter").html(btnInvInfo);
        memberLGModal.modal('show');


    }



    function conferenceDeleteInfoConfirm(arg1, arg2) {
        console.log("conferenceDeleteInfoConfirm:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("conferenceDeleteProcess.jsp", {deleteId: arg1, sessionid: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#infoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                memberLGModal.modal('hide');
            } else {

            }


        }, "json");

    }

</script>


<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Conference/Workshop Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->


                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                        <thead>
                            <tr>
                                <th scope="col">Title</th>
                                <th scope="col">Your Role</th>
                                <th scope="col">Start Date</th>
                                <th scope="col">End Date</th>
                                <th scope="col">URL</th>
                                <th scope="col">Details</th>
                                <th scope="col">Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%

                                q1 = dbsession.createQuery("from MemberConferenceInfo WHERE  member_id=" + memberIdH + " ");

                                for (Iterator itr = q1.list().iterator();
                                        itr.hasNext();) {
                                    mci = (MemberConferenceInfo) itr.next();
                                    conferenceId = mci.getId();
                                    conferenceTitle = mci.getConferenceTitle() == null ? "" : mci.getConferenceTitle();
                                    conferenceRole = mci.getConferenceMemberRole() == null ? "" : mci.getConferenceMemberRole();
                                    conferenceStartDate = mci.getConferenceStartDate().toString() == null ? "" : mci.getConferenceStartDate().toString();
                                    conferenceEndDate = mci.getConferenceEndDate().toString() == null ? "" : mci.getConferenceEndDate().toString();
                                    conferenceDetails = mci.getConferenceDetails() == null ? "" : mci.getConferenceDetails().toString();
                                    conferenceUrl = mci.getConferenceUrl() == null ? "" : mci.getConferenceUrl();


                            %>
                            <tr id="infoBox<%=conferenceId%>">
                                <td><%=conferenceTitle%></td>
                                <td><%=conferenceRole%></td> 
                                <td><%=conferenceStartDate%></td>
                                <td><%=conferenceEndDate%></td>
                                <td><%=conferenceUrl%></td>
                                <td><%=conferenceDetails%></td>
                                <td style="cursor:pointer"><a title="Edit" href="<%out.print(GlobalVariable.baseUrl);%>/member/memberConferenceInfoEdit.jsp?sessionid=<%=session.getId()%>&act=edit&conferenceId=<%=conferenceId%>"  ><i class="fa fa-edit"></i> </a> 
                                    <a title="Remove" onclick="conferenceDeleteInfo(<% out.print("'" + conferenceId + "','" + sessionIdH + "'");%>)" ><i class="fa fa-trash"></i> </a> 

                            </tr>

                            <%                                }

                                dbsession.flush();
                                dbsession.close();

                            %>
                        </tbody>
                    </table>


                    <!-- ============ End Publication information Table ================== -->

                    <div class="container">
                        <div class="row my-4">
                            <div class="col-12">
                                <div class="card-header panel_heading_saz" style="padding-top: 4px; padding-bottom: 4px;">
                                    <i class="fa fa-list-alt"></i> Add Conference Information
                                </div>
                            </div>

                        </div>
                    </div>


                    <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberConferenceInfoSubmitData.jsp?sessionid=<%=session.getId()%>&act=add" onSubmit="return fromDataSubmitValidation()">


                        <!--                        <div class="form-group row">
                                                    <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Type :</label>
                                                    <div class="col-sm-6">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="conferenceRadioWorkshop" id="conferenceRadioWorkshop" value="Conference" required="" checked="">
                                                            <label class="form-check-label" for="gridRadios1">
                                                                Conference 
                                                            </label>
                        
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="conferenceRadioWorkshop" id="conferenceRadioWorkshop" value="Workshop" required="">
                                                            <label class="form-check-label" for="gridRadios2">
                                                                Workshop
                                                            </label>
                        
                                                        </div>
                                                    </div>
                                                </div> -->

                        <input type="hidden" name="conferenceRadioWorkshop" id="conferenceRadioWorkshop" value="Conference">

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Title :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="conferenceTitle" name="conferenceTitle" placeholder="Conference Title" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-3 col-form-label text-right">*Your Role :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="memberRole" name="memberRole"  placeholder="Your Role" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Details  :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="conferenceDetails" name="conferenceDetails" placeholder="Conference Details" required="">
                            </div>
                        </div>


                        <div class="form-group row">
                            <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Start Date :</label>
                            <div class="col-sm-6">

                                <input type="text" class="form-control" id="startDate" name="startDate"  placeholder="yyyy-mm-dd" required=""> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                            </div>
                            <!-- Date Picker Plugin JavaScript -->
                            <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
                            <script type="text/javascript">
                        jQuery('#startDate').datepicker({
                            autoclose: true,
                            todayHighlight: true,
                            format: 'yyyy-mm-dd'
                        });
                            </script>  
                        </div>

                        <div class="form-group row">
                            <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*End Date :</label>
                            <div class="col-sm-6">

                                <input type="text" class="form-control" id="endDate" name="endDate"  placeholder="yyyy-mm-dd" required=""> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                            </div>
                            <!-- Date Picker Plugin JavaScript -->
                            <script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
                            <script type="text/javascript">
                        jQuery('#endDate').datepicker({
                            autoclose: true,
                            todayHighlight: true,
                            format: 'yyyy-mm-dd'
                        });
                            </script>  
                        </div>





                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">Conference URL :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="conferenceURL" name="conferenceURL"   placeholder="Conference URL" >
                            </div>
                        </div>



                        <div class="form-group row">

                            <div class="col-sm-6 offset-sm-3">
                                <button type="submit" class="btn btn-primary mr-3">Submit</button>
                                <button type="button" class="btn btn-primary">Reset</button>
                            </div>
                        </div>
                    </form>

                    <!-- ============ End Education information Table ================== -->

                </div>
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
