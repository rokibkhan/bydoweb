<%@ include file="../header.jsp" %>


<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="about_banner_membership">
    <h2 class="display_41_saz text-white">about membership</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Membership</a></li>
                <li class="breadcrumb-item active" aria-current="page">About Membership</li>
            </ol>
        </nav>
    </div>
</div>










<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="news_cont_saz">
                <p>
                    <img style="width: 40%;" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/ieb_building_image.jpg" alt=""/>
                    Every professional engineer of Bangladesh is invited to join the Institution of Engineers, Bangladesh IEB. It has 41,545 members at the present moment, representing the nation-wide engineering community.
                </p><br>

                <p>
                    Join IEB for participating in the advancement of engineering profession and become a member of a vibrant community that is proud to lead the country into the 21st century.
                </p><br>

                <p>
                    IEB membership has three categories - Fellowship, Membership and Associate Membership. These categories are based on the experience of the member in the engineering field. There are 10,462 Fellows, 22,839 Members and 8,244 Associate Members of IEB, at present.
                </p>
            </div>
        </div>

        <div class="col-md-3">
            <div class="news_cont_saz">
                <ul class="membership_panel_saz">
                    <li><p>Membership</p></li>
                    <li><a href="<%=GlobalVariable.baseUrl%>/member/aboutMemberShip.jsp">What is IEB Membership</a></li>
                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/aboutMemberShipCriteria.jsp">Membership Criteria</a></li>

                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/howToGetMemberShip.jsp">How to get Membership</a></li>

                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/aboutMemberShipFee.jsp">Membership Fee</a></li>    

                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberShipForm.jsp">Membership Form</a></li>

                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/accreditation.jsp">Accreditation</a></li>

                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/search.jsp">Search your membership No.</a></li>

                    <%
                        if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {
                    %>
                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberDashboard.jsp?sessionid=<%=session.getId()%>">My Profile</a></li>

                    <%
                    } else {
                    %>

                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/registration.jsp">Apply Membership</a></li>

                    <% } %>
                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/registrationCheck.jsp">Status of Application </a></li>


                </ul>
            </div>
        </div>
    </div>
</div>

<%@ include file="../footer.jsp" %>
