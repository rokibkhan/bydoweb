
<%@page import="com.ssl.commerz.SSLCommerz"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>



<%
    Session dbsession = null;
    String qryparam = "";;

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    getDate date = new getDate();

    Query memberFeeSQL = null;
    String strMsg = "";

    int billNo = 0;
    String billYear = "";
    String billAmount = "0";
    int status = 0;
    String status1 = "";
    java.util.Date dueDate = null;
    java.util.Date paymentDate = null;
    String billType = "";

    String tillDate = "";
    MemberFee mFee = null;
    String membershipCusName = "";
    String membershipCusEmail = "";
    String membershipCusPhone = "";
    String membershipCusAdd1 = "";
    String membershipCusCity = "";
    String membershipCusState = "";
    String membershipCusPostCode = "";
    String membershipCusCountry = "";

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    MemberFee memberFee = null;

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        memberIdH = session.getAttribute("memberId").toString();

        System.out.println("memberIdH " + memberIdH);

        String memberTypeTotalAmount = request.getParameter("memberTypeTotalAmount") == null ? "" : request.getParameter("memberTypeTotalAmount").trim();

        if (!memberTypeTotalAmount.equals("")) {

            String dueSumSQL = "select sum(amount) from member_fee  WHERE  member_id=" + memberIdH + " AND status ='0'";

            String totalPaymentDue = dbsession.createSQLQuery(dueSumSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(dueSumSQL).uniqueResult().toString();

            Query totalTransIdSQL = null;
            Object[] totalTransIdObj = null;
            String totalTransIdFeeId = "";
            String totalTransIdFeeIdTotal = "";
            int totalTransIdFeeIdCount = 0;
            int m = 1;
            totalTransIdSQL = dbsession.createSQLQuery("SELECT * FROM member_fee  WHERE  member_id=" + memberIdH + " AND status ='0'");

            if (!totalTransIdSQL.list().isEmpty()) {
                for (Iterator totalTransIdItr = totalTransIdSQL.list().iterator(); totalTransIdItr.hasNext();) {
                    totalTransIdObj = (Object[]) totalTransIdItr.next();

                    totalTransIdFeeId = totalTransIdObj[0].toString();

                    if (m == 1) {
                        totalTransIdFeeIdTotal = totalTransIdFeeIdTotal + totalTransIdFeeId;
                    } else {
                        totalTransIdFeeIdTotal = totalTransIdFeeIdTotal + "-" + totalTransIdFeeId;
                    }

                    m++;
                    totalTransIdFeeIdCount++;
                }
            } else {
                totalTransIdFeeIdTotal = "";
            }

            Map<String, String> postData = new HashMap<String, String>();

            //  response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
            //   response.sendRedirect("registrationFeePaymentOption.jsp?regTempId=" + memberRegId + "&regMemId=" + memberVerifyId + "&regStep=11&strMsg=" + strMsg + "#section9");
            String strMsgSuccess = "Transaction successfully completed";
            String successPageUrl = GlobalVariable.baseUrl + "/member/memberPaymentProcessingTotalAmountPaymentSuccess.jsp?sessionid=" + session.getId() + "&regTempId=" + memberIdH + "&regMemId=" + memberIdH + "&transId=" + totalTransIdFeeIdTotal + "&transIdCount=" + totalTransIdFeeIdCount + "&strMsg=" + strMsgSuccess;

            String strMsFail = "Transaction fail";
            String failPageUrl = GlobalVariable.baseUrl + "/member/memberPaymentProcessingTotalAmountPaymentFail.jsp?sessionid=" + session.getId() + "&regTempId=" + memberIdH + "&regMemId=" + memberIdH + "&transId=" + totalTransIdFeeIdTotal + "&transIdCount=" + totalTransIdFeeIdCount + "&strMsg=" + strMsFail;

            String strMsCancel = "Transaction fail";
            String cancelPageUrl = GlobalVariable.baseUrl + "/member/memberPaymentProcessingTotalAmountPaymentCancel.jsp?sessionid=" + session.getId() + "&regTempId=" + memberIdH + "&regMemId=" + memberIdH + "&transId=" + totalTransIdFeeIdTotal + "&transIdCount=" + totalTransIdFeeIdCount + "&strMsg=" + strMsCancel;

            postData.put("total_amount", totalPaymentDue);
            postData.put("tran_id", totalTransIdFeeIdTotal);
            postData.put("success_url", successPageUrl);
            postData.put("fail_url", failPageUrl);
            postData.put("cancel_url", cancelPageUrl);
            postData.put("version", "3.00");
            postData.put("cus_name", membershipCusName);
            postData.put("cus_email", membershipCusEmail);
            postData.put("cus_add1", membershipCusAdd1);
            //   postData.put("cus_add2", "Address Line Tw");
            postData.put("cus_city", membershipCusCity);
            postData.put("cus_state", membershipCusState);
            postData.put("cus_postcode", membershipCusPostCode);
            postData.put("cus_country", membershipCusCountry);
            postData.put("cus_phone", membershipCusPhone);

            postData.put("cus_fax", "0171111111");
            postData.put("ship_name", "ABC XY");
            postData.put("ship_add1", "Address Line On");
            postData.put("ship_add2", "Address Line Tw");
            postData.put("ship_city", "City Nam");
            postData.put("ship_state", "State Nam");
            postData.put("ship_postcode", "Post Cod");
            postData.put("ship_country", "Countr");

//            postData.put("value_a", "ref00");
//            postData.put("value_b", "ref00");
//            postData.put("value_c", "ref00");
//            postData.put("value_d", "ref00");
            /**
             * Provide your SSL Commerz store Id and Password by this following
             * constructor. If Test Mode then insert true and false otherwise.
             */
            //     SSLCommerz sslcz = new SSLCommerz("ieb5c9c8b9a864bf", "ieb5c9c8b9a864bf@ssl", true);
            SSLCommerz sslcz = new SSLCommerz(GlobalVariable.SSLCommerzStoreId, GlobalVariable.SSLCommerzStorePass, Boolean.parseBoolean(GlobalVariable.SSLCommerzStoreMode));

            /**
             * If user want to get Gate way list then pass isGetGatewayList
             * parameter as true If user want to get URL as returned response,
             * pass false.
             */
            String paymentResponse = sslcz.initiateTransaction(postData, false);

            System.out.println("paymentResponse::" + paymentResponse);

            response.sendRedirect(paymentResponse);

        } else {
            strMsg = "Error!!! Payment Failed";
            response.sendRedirect("memberPaymentHistoryInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        }
    } else {
        System.out.println("LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

%>
