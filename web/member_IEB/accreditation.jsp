<%@ include file="../header.jsp" %>


<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="about_banner_membership">
    <h2 class="display_41_saz text-white">accreditation</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Membership</a></li>
                <li class="breadcrumb-item active" aria-current="page">Accreditation</li>
            </ol>
        </nav>
    </div>
</div>



<section class="news-single-content pt-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="news-content news_cont_saz">
                    <h2 class="display-41 display_41_saz">List of Accredited Program of the various Universities</h2><br>
                    <div class="table-responsive">
                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">Bangladesh</p></td>
                                </tr>
                                <tr>
                                    <td><strong>Sl.</strong></td>
                                    <td><strong>Institute Name</strong></td>
                                    <td><strong>Program Name</strong></td>
                                    <td><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp; &nbsp;1<span style="white-space:pre">	</span></td>
                                    <td>Ahsanullah University of Science and Technology (AUST)</td>
                                    <td>B.Sc. in Mechanical Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Mechanical Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Southeast University (SEU)</td>
                                    <td>B.Sc. in Electrical and Electronic Engineering&nbsp;<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Electrical&nbsp; Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>International University of Business Agriculture and Technology (IUBAT)</td>
                                    <td>B.Sc. in Civil Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Civil Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Islamic University of Technology (IUT)</td>
                                    <td>B.Sc. in Civil Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Civil Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Southern University Bangladesh (SUB)</td>
                                    <td>B.Sc. in Civil Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Civil Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Daffodil International University (DIU)</td>
                                    <td>B.Sc. in Textile Engineering&nbsp;<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Textile Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>East West University (EWU)</td>
                                    <td>B.Sc. In Information and Communication Engineering (B.Sc. In ICE) Day time</td>
                                    <td>Computer Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>American International University- Bangladesh (AIUB)</td>
                                    <td>B.Sc. in Computer Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Computer Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>American International University- Bangladesh (AIUB)</td>
                                    <td>B.Sc. in Electrical and Electronic Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Electrical&nbsp; Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">10</td>
                                    <td>American International University- Bangladesh (AIUB)</td>
                                    <td>B.Sc. in  Electrical and Communication Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Electrical&nbsp; Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>11</td>
                                    <td>BRAC University</td>
                                    <td>B.Sc. in Computer Science and Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Computer Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>12</td>
                                    <td>BRAC University</td>
                                    <td>B.Sc. in  Electrical and Communication Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Electrical Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>13</td>
                                    <td>Daffodil International University (DIU)</td>
                                    <td>B.Sc. in Computer Science and Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Computer Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>14</td>
                                    <td>Daffodil International University (DIU)</td>
                                    <td>B.Sc. in Electronics and Telecommunication Engineering(Daytime)</td>
                                    <td>Electrical Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">15</td>
                                    <td>East West University (EWU)</td>
                                    <td>B.Sc. in Electronics and Telecommunication Engineering(Daytime)</td>
                                    <td>Electrical&nbsp; Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">16</td>
                                    <td>East West University (EWU)</td>
                                    <td>B.Sc. in Computer Science and Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Computer Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">17</td>
                                    <td>International Islamic University, Chittagong (IIUC)</td>
                                    <td>B.Sc in Computer &amp; Communication Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Computer Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>18</td>
                                    <td>International Islamic University, Chittagong (IIUC)</td>
                                    <td>B.Sc. in Computer Science and Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Computer Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">19</td>
                                    <td>Military Institute of Science &amp; Technology(MIST)</td>
                                    <td>B.Sc. in Civil Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Civil Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">20</td>
                                    <td>Military Institute of Science &amp; Technology(MIST)</td>
                                    <td>B.Sc. in Mechanical Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Mechanical Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>21</td>
                                    <td>Military Institute of Science &amp; Technology(MIST)</td>
                                    <td>B.Sc. in Electrical, Electronic and Communication Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Electrical&nbsp; Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>22</td>
                                    <td>Military Institute of Science &amp; Technology(MIST)</td>
                                    <td>B.Sc. in Computer Science and Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Computer Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>23</td>
                                    <td>Stamford University Bangladesh</td>
                                    <td>B.Sc. in Civil Engineering(Daytime)</td>
                                    <td>Civil Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>24</td>
                                    <td>Shahjalal University of Science &amp; Technology (SUST)</td>
                                    <td>B.Sc. in Chemical &amp; Polymer Science Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Chemical Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">25</td>
                                    <td>Shahjalal University of Science &amp; Technology (SUST)</td>
                                    <td>B.Sc. in Civil Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Civil Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">26</td>
                                    <td>Shahjalal University of Science and Technolgy (SUST)</td>
                                    <td>B.Sc. in Industrial and Production Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Mechanical Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">27</td>
                                    <td>University of Asia Pacific (UAP)</td>
                                    <td>B.Sc. in Civil Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Civil Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">&nbsp;28</td>
                                    <td>University of Asia Pacific (UAP)</td>
                                    <td>B.Sc. in&nbsp;Computer Science and Engineering(CSE) Day time</td>
                                    <td>Computer Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">29</td>
                                    <td>University of Asia Pacific (UAP)</td>
                                    <td>B.Sc. in Electrical &amp; Electronics  Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Electrical&nbsp; Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">30</td>
                                    <td>United International University (UIU)</td>
                                    <td>B.Sc. in Electrical &amp; Electronics  Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Electrical&nbsp; Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">31</td>
                                    <td>Stamford University Bangladesh</td>
                                    <td>B.Sc. in Electrical &amp; Electronics  Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Electrical&nbsp; Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">32</td>
                                    <td>Military Institute of Science &amp; Technology(MIST)</td>
                                    <td>B.Sc. in Aeronautical Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Mechanical&nbsp; Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">33</td>
                                    <td>United International University (UIU)</td>
                                    <td>B.Sc. in Computer Science and Engineering(CSE) <font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Computer Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">34</td>
                                    <td>BRAC University</td>
                                    <td>B.Sc. in Electrical &amp; Electronics  Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Electrical&nbsp; Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">35</td>
                                    <td>Eastern University</td>
                                    <td>B.Sc. in Electrical &amp; Electronics  Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Electrical&nbsp; Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>36</td>
                                    <td>North South University (NSU)</td>
                                    <td>B.Sc. Electronics and Telecommunication Engineering (BSETE)<font face="Arial, Helvetica, sans-serif" color="#4c4758">(</font><span style="color: rgb(76, 71, 88); font-family: Arial, Helvetica, sans-serif; background-color: rgb(247, 249, 245); ">Day time)</span></td>
                                    <td>Electrical  Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>37</td>
                                    <td>North South University (NSU)</td>
                                    <td>B.Sc. Electrical and Electronics Engineering (BSEEE)<font face="Arial, Helvetica, sans-serif" color="#4c4758">(</font><span style="color: rgb(76, 71, 88); font-family: Arial, Helvetica, sans-serif; background-color: rgb(247, 249, 245); ">Day time)</span></td>
                                    <td>Electrical  Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>38</td>
                                    <td>North South University (NSU)</td>
                                    <td>B.Sc. in Computer Science and Engineering(CSE)<font face="Arial, Helvetica, sans-serif" color="#4c4758">(</font><span style="color: rgb(76, 71, 88); font-family: Arial, Helvetica, sans-serif; background-color: rgb(247, 249, 245); ">Day time)</span></td>
                                    <td>Computer Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>39</td>
                                    <td>Daffodil International University (DIU)</td>
                                    <td>B.Sc. Electrical and Electronics Engineering (BSEEE)<font face="Arial, Helvetica, sans-serif" color="#4c4758">(</font><span style="color: rgb(76, 71, 88); font-family: Arial, Helvetica, sans-serif; background-color: rgb(247, 249, 245); ">Day time)</span></td>
                                    <td>Electrical Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">40</td>
                                    <td>Independent University Bangladesh(IUB)</td>
                                    <td>B.Sc. in Electrical &amp; Electronics  Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Electrical&nbsp; Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">41</td>
                                    <td>Independent University Bangladesh(IUB)</td>
                                    <td>B.Sc. Electronics &amp; Telecommunication Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Electrical&nbsp; Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">42</td>
                                    <td>Stamford University Bangladesh</td>
                                    <td>B.Sc. in Computer Science &amp; Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Computer &nbsp;Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">43</td>
                                    <td>Independent University Bangladesh (IUB)</td>
                                    <td>B.Sc. in Computer&nbsp; Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Computer &nbsp;Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">44</td>
                                    <td>University of Liberal Arts Bangladesh (ULAB)</td>
                                    <td>B.Sc. in Computer Science &amp; Engineering<font face="Arial, Helvetica, sans-serif" color="#4c4758">(Day time)</span></td>
                                    <td>Computer &nbsp;Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>45</td>
                                    <td>University of Liberal Arts Bangladesh(ULAB)</td>
                                    <td>B.Sc. Electronics and Telecommunication Engineering (BSETE)<font face="Arial, Helvetica, sans-serif" color="#4c4758">(</font><span style="color: rgb(76, 71, 88); font-family: Arial, Helvetica, sans-serif; background-color: rgb(247, 249, 245); ">Day time)</span></td>
                                    <td>Electrical Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>46</td>
                                    <td>International Islamic University Chittagong (IIUC)</td>
                                    <td>B.Sc. Electrical and Electronic Engineering (EEE)<font face="Arial, Helvetica, sans-serif" color="#4c4758">(</font><span style="color: rgb(76, 71, 88); font-family: Arial, Helvetica, sans-serif; background-color: rgb(247, 249, 245); ">Day time)</span></td>
                                    <td>Electrical Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>47</td>
                                    <td>Khulna University (KU)</td>
                                    <td>B.Sc. Electronics and Communication Engineering (ECE)<font face="Arial, Helvetica, sans-serif" color="#4c4758">(</font><span style="color: rgb(76, 71, 88); font-family: Arial, Helvetica, sans-serif; background-color: rgb(247, 249, 245); ">Day time)</span></td>
                                    <td>Electrical Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>48</td>
                                    <td>Green University of Bangladesh (GUB) )</td>
                                    <td>B.Sc. Computer Science and Engineering (CSE)<font face="Arial, Helvetica, sans-serif" color="#4c4758">(</font><span style="color: rgb(76, 71, 88); font-family: Arial, Helvetica, sans-serif; background-color: rgb(247, 249, 245); ">Day time)</span></td>
                                    <td>Computer Engineering Division</td>
                                </tr>
                                <tr>
                                    <td>49</td>
                                    <td>Green University of Bangladesh (GUB) )</td>
                                    <td>B.Sc. Electrical and Electronic Engineering (EEE)<font face="Arial, Helvetica, sans-serif" color="#4c4758">(</font><span style="color: rgb(76, 71, 88); font-family: Arial, Helvetica, sans-serif; background-color: rgb(247, 249, 245); ">Day time)</span></td>
                                    <td>Electrical Engineering Division</td>
                                </tr>
                                <tr>
                                    <td class="style3 dept" height="18">50</td>
                                    <td>East West University (EWU)</td>
                                    <td>B.Sc. Electrical and Electronic Engineering (EEE)</td>
                                    <td>Electrical&nbsp; Engineering Division</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <p class="subs_fee_title"><small>Also graduate engineers from the following institutions are eligible to be member of   IEB</small></p>

                    <p style="padding: 20px 0; line-height: 24px;">
                        List of degree recognized by The Institute of Engineers, bangladesh (IEB)
                    </p>

                    <div class="table-responsive">

                        <table class="table table-bordered table_saz2">

                            <tr>
                                <td><strong>Sl.</strong></td>
                                <td><strong>Institute Name</strong></td>
                                <td><strong>Degree Recognized</strong></td>
                                <td><strong>Discipline</strong></td>
                            </tr>
                            <tbody>
                                <tr>
                                    <td>01</td>
                                    <td>BUET</td>
                                    <td>B.Sc. Engineering</td>
                                    <td>
                                        <ul style="list-style: number;">
                                            <li>Civil</li>
                                            <li>Mech.</li>
                                            <li>EEE</li>
                                            <li>Chem.</li>
                                            <li>CSE</li>
                                            <li>Metallurgical</li>
                                            <li>Naval Arch. & Marine Engg.</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>02</td>
                                    <td>CUET</td>
                                    <td>B.Sc. Engineering</td>
                                    <td>
                                        <ul style="list-style: number;">
                                            <li>Civil</li>
                                            <li>Mech.</li>
                                            <li>EEE</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>03</td>
                                    <td>KUET</td>
                                    <td>B.Sc. Engineering</td>
                                    <td>
                                        <ul style="list-style: number;">
                                            <li>Civil</li>
                                            <li>Mech.</li>
                                            <li>EEE</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>04</td>
                                    <td>RUET</td>
                                    <td>B.Sc. Engineering</td>
                                    <td>
                                        <ul style="list-style: number;">
                                            <li>Civil</li>
                                            <li>Mech.</li>
                                            <li>EEE</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>05</td>
                                    <td>DUET</td>
                                    <td>B.Sc. Engineering</td>
                                    <td>
                                        <ul style="list-style: number;">
                                            <li>Civil</li>
                                            <li>Mech.</li>
                                            <li>EEE</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>06</td>
                                    <td>BUTEX</td>
                                    <td>B.Sc. Engineering</td>
                                    <td>
                                        <ul style="list-style: number;">
                                            <li>Textile</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>07</td>
                                    <td>IUT</td>
                                    <td>B.Sc. Engineering</td>
                                    <td>
                                        <ul style="list-style: number;">
                                            <li>CIT (CSE Division)</li>
                                            <li>Mech.</li>
                                            <li>EEE</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>08</td>
                                    <td>AMIE</td>
                                    <td>B.Sc. Engineering</td>
                                    <td>
                                        <ul style="list-style: number;">
                                            <li>Civil</li>
                                            <li>EEE</li>
                                            <li>Mech.</li>
                                            <li>Chem.</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>09</td>
                                    <td>AUST</td>
                                    <td>B.Sc. Engineering</td>
                                    <td>
                                        <ul style="list-style: number;">
                                            <li>Civil</li>
                                            <li>CSE</li>
                                            <li>EEE</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>SUST</td>
                                    <td>B.Sc. Engineering</td>
                                    <td>
                                        <ul style="list-style: number;">
                                            <li>CSE</li>
                                        </ul>
                                    </td>
                                </tr>
                                
                            </tbody>

                        </table>

                    </div>

                    <br><br><p class="subs_fee_title">Equivalent Degrees</p>
                    <div class="table-responsive">
                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">India</p></td>
                                </tr>
                                <tr>
                                    <td><strong>Sl.</strong></td>
                                    <td><strong>Institute Name</strong></td>
                                    <td><strong>Program Name</strong></td>
                                    <td><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Sardar Vallabhbhai Regional Collage of Engineering Technology, Gujrat</td>
                                    <td>B.Sc. in Civil Engineering&nbsp;</td>
                                    <td>Civil Engineering</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Annamalai University</td>
                                    <td>B.Sc Engineering</td>
                                    <td>
                                        <p>i)Civil  II)Chemical iii)Civil &amp; Structural</p>
                                        <p>iv) electrical</p>
                                        <p>v)Electrical  &amp; electronics</p>
                                        <p>vi)Electronics &amp; instrumentation  vii)Mechanical</p>
                                        <p>viii) Mechanical Production</p>
                                        <p>ix) B. Tech in Chemical engineering</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Avadh University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i)B.  Tech in Civil</p>
                                        <p>ii) B. Tech in Computer &amp; Engineering iii) B. Tech in  Electrical</p>
                                        <p>iv) B. Tech in Electronics</p>
                                        <p>v) B. Tech in Mechanical</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Awadesh Pratap Singh University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)	Civil ii)	Electrical iii)	Mechanical</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Banaras Hindu University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i) 	B. Tech in Civil</p>
                                        <p>ii)	B. Tech in Chemical</p>
                                        <p>iii)	B. Tech in Civil &amp;  municipal</p>
                                        <p>iv)	B. Tech in Electrical</p>
                                        <p>v)	B. Tech in Mechanical  vi)	B.  Tech in Metallurgical vii)	B. Tech in Mining</p>
                                        <p>viii)	Chemical</p>
                                        <p>ix)	Civil  &amp; Municipal x)	Electrical</p>
                                        <p>xi)	Mechanical&nbsp;</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Bangalore University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i) 	Chemical  ii)	Civil</p>
                                        <p>iii)	Civil &amp; Transportation</p>
                                        <p>iv)	 Electrical</p>
                                        <p>v)	 Electronics</p>
                                        <p>vi)	Industrial &amp; production</p>
                                        <p>vii)	Mechanical</p>
                                        <p>viii)	 B.Tech in Textile</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>Bhagalpur University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i)	Civil</p>
                                        <p>ii)	Electrical</p>
                                        <p>iii)	Mechanical</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>Bharathiar University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i) 	Civil</p>
                                        <p>ii)	Electronics &amp; communication</p>
                                        <p>iii)	Electrical &amp;  electronics</p>
                                        <p>iv)	Mechanical</p>
                                        <p>v)	Metallurgical</p>
                                        <p>vi)	Production</p>
                                        <p>vii)	B.Tech  in Chemical</p>
                                        <p>viii)	B.Tech in Textile Technology</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>Bharathidasan University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i) 	Civil</p>
                                        <p>ii)	Electronics &amp; communication</p>
                                        <p>iii)	Electrical &amp;  electronics</p>
                                        <p>iv)	Mechanical</p>
                                        <p>v)	Metallurgical</p>
                                        <p>vi)	Production</p>
                                        <p>vii)	B.Tech  in Chemical</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>Bhopal University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i)	Civil ii)	Electrical</p>
                                        <p>iii)	Electronics</p>
                                        <p>iv)	Mechanical</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>11</td>
                                    <td>Bihar University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i)	Civil ii)	Chemical</p>
                                        <p>iii)	Electrical</p>
                                        <p>iv)	Mechanical</p>
                                        <p>v)	Mechanical and Production</p>
                                        <p>vi)	Production</p>
                                        <p>vii)	Telecommunication</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>12</td>
                                    <td>Bombay University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i) 	Chemical</p>
                                        <p>ii)	Civil</p>
                                        <p>iii)	Electrical</p>
                                        <p>iv)	Mechanical</p>
                                        <p>v)	Production</p>
                                        <p>vi)	 Textile Technology</p>
                                        <p>vii)	Computer Technology</p>
                                        <p>viii)	Electronics</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>13</td>
                                    <td>Burdwan University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i)	Civil</p>
                                        <p>ii)	Chemical</p>
                                        <p>iii)	Mechanical</p>
                                        <p>iv)	Mechanical</p>
                                        <p>v)	Textile Technology</p>
                                        <p>vi)	Electrical</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>14</td>
                                    <td>Calcutta University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i) 	Civil ii)	Electrical</p>
                                        <p>iii)	Electronics Telecommunication</p>
                                        <p>iv)	Mechanical</p>
                                        <p>v)	Metallurgical</p>
                                        <p>vi)	Mining</p>
                                        <p>vii)	B. Metallurgical</p>
                                        <p>viii)	B.Tech in Radio  physics &amp; Electronics</p>
                                        <p>ix)	B.Tech in Chemical Engg. &amp; Chemical</p>
                                        <p>x)	B.Tech in Ceramic</p>
                                        <p>xi)	B.Tech in Oil Te</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>15</td>
                                    <td>Calcutta University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i) 	Civil ii)	Chemical</p>
                                        <p>iii)	Electrical</p>
                                        <p>iv)	Mechanical</p>
                                        <p>v)	B.Tech in Civil</p>
                                        <p>vi)	B.Tech in Chemical</p>
                                        <p>vii)	B.Tech in Electrical</p>
                                        <p>viii)	B.Tech in  Mechanized</p>
                                        <p>ix)	B.Tech in Production   cum Plant</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>16</td>
                                    <td>Cochin University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i)	Civil ii)	Chemical</p>
                                        <p>iii)	Mechanical</p>
                                        <p>iv)	Electrical</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>17</td>
                                    <td>Delhi University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>
                                        <p>i)	Chemical</p>
                                        <p>ii)	B.Tech in Civil</p>
                                        <p>iii)	B.Tech in Electrical</p>
                                        <p>iv)	B.Tech in Mechanical</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>18</td>
                                    <td>Sardar vallavhvhai regional college of Engineering  Technology, Gujrat, India</td>
                                    <td>B.Sc in (Civil)</td>
                                    <td>Civil Engineering</td>
                                </tr>
                                <tr>
                                    <td>19</td>
                                    <td>Dilbrugarh University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>20</td>
                                    <td>Gauhati University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>21</td>
                                    <td>Gorakhpur University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>22</td>
                                    <td>Govind Ballah Pant of Agriculture &amp; Technology University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>23</td>
                                    <td>Gujarat University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>24</td>
                                    <td>Gulbarga University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>25</td>
                                    <td>Guru Ghasidas University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>26</td>
                                    <td>JdavpurUniversity</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>27</td>
                                    <td>Jawaharalal Nehru University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>28</td>
                                    <td>Jawaharalal Nehru Krishi Vishwavidyalaya</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>29</td>
                                    <td>Jawaharalal Nehru Technology University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>30</td>
                                    <td>Jiwaji University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>31</td>
                                    <td>Jodhpur University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>32</td>
                                    <td>Kakatiya University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>33</td>
                                    <td>Kanpur University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>34</td>
                                    <td>Karanatak University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>35</td>
                                    <td>Kashmir University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>36</td>
                                    <td>Kerala University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>37</td>
                                    <td>Kurukshetra University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>38</td>
                                    <td>Madras University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>39</td>
                                    <td>Madurai University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>40</td>
                                    <td>Maharaja Sayajirai if barodaUniversity</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>41</td>
                                    <td>Maharshi University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>Recognized  by the Institution of Engineers, India vide their letter Memo/Equi/25  Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd meeting held  on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>42</td>
                                    <td>Mahatma Phule Krishi Vidyapeeth</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>Recognized  by the Institution of Engineers, India vide their letter Memo/Equi/25  Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd meeting held  on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>43</td>
                                    <td>Mangalore University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>Recognized  by the Institution of Engineers, India vide their letter Memo/Equi/25  Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd meeting held  on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>44</td>
                                    <td>Marathwada University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>Recognized  by the Institution of Engineers, India vide their letter Memo/Equi/25  Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd meeting held  on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>45</td>
                                    <td>Mysore University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>Recognized  by the Institution of Engineers, India vide their letter Memo/Equi/25  Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd meeting held  on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>46</td>
                                    <td>M.L. sukhadia University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>Recognized  by the Institution of Engineers, India vide their letter Memo/Equi/25  Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd meeting held  on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>47</td>
                                    <td>Nagpur University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>Recognized  by the Institution of Engineers, India vide their letter Memo/Equi/25  Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd meeting held  on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>48</td>
                                    <td>North Bengal University</td>
                                    <td>&nbsp;</td>
                                    <td>Recognized  by the Institution of Engineers, India vide their letter Memo/Equi/25  Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd meeting held  on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>49</td>
                                    <td>Orissa Agriculture &amp; technology University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>50</td>
                                    <td>Osmania University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>51</td>
                                    <td>Panjab University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>52</td>
                                    <td>Patna University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>53</td>
                                    <td>Pcona University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>54</td>
                                    <td>Punjab Agriculture University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>55</td>
                                    <td>Punjabi University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>56</td>
                                    <td>Punjabrao University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>57</td>
                                    <td>Rajasthan University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>58</td>
                                    <td>Rajasthan Agriculture University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>59</td>
                                    <td>Ranchi University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>60</td>
                                    <td>Rani University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>61</td>
                                    <td>Ravishankar University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>62</td>
                                    <td>Roorkee University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>63</td>
                                    <td>Sambalpur University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>64</td>
                                    <td>Sardar University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>65</td>
                                    <td>Saugor University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>66</td>
                                    <td>Saurashtra University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>67</td>
                                    <td>Shivaji University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>68</td>
                                    <td>South Gujrat University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>69</td>
                                    <td>Sri Venkateswara University</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>70</td>
                                    <td>College of Military Engineering, kirkee</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>71</td>
                                    <td>College of Military Engineering, kirkee, and military college of Electronics and Mechanical Engineering, Secunderabad</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>72</td>
                                    <td>College of Military Engineering,kirkee and Military college of Telecommunication engineering , Mhow</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>73</td>
                                    <td>Dayalbagh Educational Institute, Agra</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>74</td>
                                    <td>Directorate of Marine engineering Training, Government of India</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>75</td>
                                    <td>Indian Institute of science, Bangalore</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>76</td>
                                    <td>Indian Institute of Technology, Bombay</td>
                                    <td>B.Tech. Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>77</td>
                                    <td>Indian Institute of Technology ,Delhi</td>
                                    <td>B.Tech. Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>78</td>
                                    <td>Indian Institute of Technology , Kanpur</td>
                                    <td>B.Tech. Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>79</td>
                                    <td>Indian Institute of Technology, kharagpur</td>
                                    <td>B.Tech. Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>80</td>
                                    <td>Indian Institute of Technology, Madras</td>
                                    <td>B.Tech. Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>81</td>
                                    <td>Indian Railways Institute of Mechanical &amp;  Electrical Engineering</td>
                                    <td>B.Tech. Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>82</td>
                                    <td>Indian School of Mines, Dhanbad</td>
                                    <td>AISM,BSc Engineering B.Tech Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>83</td>
                                    <td>Madras Institute of Technology Madras</td>
                                    <td>Diploma</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>84</td>
                                    <td>Punjab Engineering College  Chandigarh</td>
                                    <td>Adv. Diploma</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>85</td>
                                    <td>Thapar Institute of Engineering and  Technology , Patiala</td>
                                    <td>B.Sc  Engineering</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>86</td>
                                    <td>Thomson Civil Engineering College, Roorkee</td>
                                    <td>Diploma</td>
                                    <td>i)  Recognized by the Institution of Engineers, India vide their letter  Memo/Equi/25 Ra dt 18.5.92 ii) Endorsed by Equivalent committee, IEB 2nd  meeting held on 22.4.99</td>
                                </tr>
                                <tr>
                                    <td>87</td>
                                    <td>B.P. poddar Institute of management &amp;  Technology ,Kolkata,India</td>
                                    <td>B.Sc in Engg (ECE)</td>
                                    <td>ECE</td>
                                </tr>
                                <tr>
                                    <td>88</td>
                                    <td>AGRA</td>
                                    <td>B.Sc (Eng.) in          Ch1 E2, M2, B.Text.</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>89</td>
                                    <td>Madra institute of technology, madras</td>
                                    <td>Ae, Au, El,  IT, PT)</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>90</td>
                                    <td>Zakir hussain  college of engineering and technology, aligarh3</td>
                                    <td>B.Sc. (eng.) in Ch,C,E,M</td>
                                    <td>(Ch,C4, E4, M4)</td>
                                </tr>
                                <tr>
                                    <td>91</td>
                                    <td>ANNAMALAI</td>
                                    <td>C,Ch,CS,E,EE,Ein,M,Mp B.Sc. (Tech.) in ch</td>
                                    <td>Department od engineering (Ch,C,CS,EE,Ein,M.MP)</td>
                                </tr>
                                <tr>
                                    <td>92</td>
                                    <td>Allahbad  agricultural institute, allababad (Ag) J.K institute  of applied  physics, allahbabad (ET) motilal neharu regional engineering college  allahabbad</td>
                                    <td>B.Sc .( agricultural engineering)5 M.Sc. (Tech). in electronics and radio engineering B.Tech. in ET          B.E. in C.E.M</td>
                                    <td>(C,E,M)</td>
                                </tr>
                                <tr>
                                    <td>93</td>
                                    <td>GH Government engineering college, rewa</td>
                                    <td>B.E. in C,E,M</td>
                                    <td>(C,E,M)</td>
                                </tr>
                                <tr>
                                    <td>94</td>
                                    <td>University college of engineering waltair</td>
                                    <td>B.E. in                                            C,E,EC,EE,M,TC       b.Tech. in Ch</td>
                                    <td>(C,EE,M,Ch )</td>
                                </tr>
                                <tr>
                                    <td>95</td>
                                    <td>Institute of technology varanasi</td>
                                    <td>BANARAS HINDU      B.Tech. in C, ch,</td>
                                    <td>(,CCh,E,M,Met,min)</td>
                                </tr>
                                <tr>
                                    <td>96</td>
                                    <td>Alagppa chettiar college of technology, guindy, madras8</td>
                                    <td>B.E.in C,EC,EE,M,P  B.Tech in</td>
                                    <td>(Ch, text)</td>
                                </tr>
                                <tr>
                                    <td>97</td>
                                    <td>Malaviya National Institute of Technology</td>
                                    <td>B.E.in Electrical Engg.</td>
                                    <td>Electrical Engg</td>
                                </tr>
                                <tr>
                                    <td>98</td>
                                    <td>West Bengal University of Technology</td>
                                    <td>B.E.in Electrical Engg.</td>
                                    <td>Electrical Engg</td>
                                </tr>
                                <tr>
                                    <td>99</td>
                                    <td>National Institute of Technology</td>
                                    <td>B.E.in Electrical Engg.</td>
                                    <td>Electrical Engg</td>
                                </tr>
                                <tr>
                                    <td>100</td>
                                    <td>KIIT University</td>
                                    <td>B.E.in Computer Science &amp; Engg.</td>
                                    <td>Computer Science &amp; Engg</td>
                                </tr>
                                <tr>
                                    <td>101</td>
                                    <td>Sharda University</td>
                                    <td>B.E.in Civil Engg.</td>
                                    <td>Civil Engg.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">Australia</p></td>
                                </tr>
                                <tr>
                                    <td class="head" style="text-align: center;" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>1</p>
                                    </td>
                                    <td class="style3 dept">
                                        <p>Curtin University of Technoloy</p>
                                    </td>
                                    <td class="style3 dept">
                                        <p>&nbsp;B.Sc Engineering</p>
                                    </td>
                                    <td>
                                        <p><span>Civil Engg.</span>&nbsp;</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>


                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">Japan</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" class="head" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Shizuoka University</td>
                                    <td>B.Sc Engineering</td>
                                    <td><span>Mechanical Engineering</span>&nbsp;&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>


                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">Pakistan</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" class="head" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Mehran University of Engineering Technology,</td>
                                    <td>B.Sc in Engg(CSE)</td>
                                    <td>CSE</td>
                                </tr>
                            </tbody>
                        </table>


                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">Malaysia</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" class="head" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Multimedia University , Malaysia</td>
                                    <td>B.Sc in (Electrical)</td>
                                    <td>Electrical Engineering</td>
                                </tr>
                            </tbody>
                        </table>


                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">Russia (USSR)</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" class="head" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>1</p>
                                        <p>2</p>
                                        <p>3</p>
                                        <p>4</p>
                                    </td>
                                    <td>
                                        <p>Moscow state Academy of Instrument making &amp;  Information Sciences, USSR</p>
                                        <p>KIEV Polytechnic Institute, USSR</p>
                                        <p>Vladimir State University, Russia</p>
                                        <p>D.I. Mendeleyev Russian University of Chemical Technology</p>
                                    </td>
                                    <td>
                                        <p>M.Sc in Engg (CSE)</p>
                                        <p>B.Sc Engineering&nbsp;</p>
                                        <p>M.Sc in Engg (CSE)</p>
                                        <p>M. Sc. in Engg.</p>
                                    </td>
                                    <td>
                                        <p>CSE</p>
                                        <p>&nbsp;Electronics Engineering</p>
                                        <p>CSE</p>
                                        <p>Chemical &amp; Biotechnology Engineering</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>


                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">Thailand</p></td>
                                </tr>
                                <tr>
                                    <td class="head" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>1</p>
                                        <p>2</p>
                                    </td>
                                    <td>
                                        <p>Assumptson University,Bangkok, Thailand</p>
                                        <p>Thammasat University</p>
                                    </td>
                                    <td>
                                        <p>B.Sc Engineering (Electronics)</p>
                                        <p>B.Sc Engg Mechanica Engineering</p>
                                    </td>
                                    <td>
                                        <p>Electronics Engineering</p>
                                        <p>Mechanical Engineering<span style="text-align: center;">&nbsp;</span></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>


                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">USA</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" class="head" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>University of Southern California , USA</td>
                                    <td>B.Sc Chemical engineering</td>
                                    <td><span>Chemical engineering</span>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>National University San Diego, California ,USA</td>
                                    <td>B.Sc Manufacturing Engineering Technology</td>
                                    <td><span>Manufacturing Engineering&nbsp;</span>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>New York University</td>
                                    <td>B.Sc Civil engineering</td>
                                    <td>Civil engineering</td>
                                </tr>
                            </tbody>
                        </table>


                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">Bulgaria</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" class="head" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Higher Institute of Chemical Technological,Bourgas</td>
                                    <td>M.Sc Engineering Chemical</td>
                                    <td>Chemical Engineering</td>
                                </tr>
                            </tbody>
                        </table>


                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">Azarbaizan</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" class="head" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Azerbaijan Polytechnic Institute (former USSR)</td>
                                    <td>M. Sc in computer Engineering</td>
                                    <td>Computer Engineering</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Azerbaijan Institution of Petroleum &amp; Chemistry</td>
                                    <td>M.Sc in Chemical</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="4" height="24">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>



                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">Ukraine</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" class="head" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>1</p>
                                        <p>2</p>
                                    </td>
                                    <td>
                                        <p>Kharkov State Technology University, Ukraine</p>
                                        <p>Kharkov National University of Radio Electronics</p>
                                    </td>
                                    <td>
                                        <p>B.Sc in Engg (ECE)</p>
                                        <p>B.Sc Engineering&nbsp;</p>
                                    </td>
                                    <td>
                                        <p>CSE</p>
                                        <p>Computer Intellect Systems &amp; Networks</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>



                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">Finland</p></td>
                                </tr>
                                <tr>
                                    <td class="head" style="text-align: center;" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td class="style3 dept">Helsinki Metropolia University</td>
                                    <td class="style3 dept">&nbsp;B.Sc Engineering</td>
                                    <td class="style3 dept">Information Technology</td>
                                </tr>
                            </tbody>
                        </table>



                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">Canada</p></td>
                                </tr>
                                <tr>
                                    <td class="head" style="text-align: center;" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td class="style3 dept">Ryerson University</td>
                                    <td class="style3 dept">B.Sc Engg&nbsp;</td>
                                    <td>Civil Engg.</td>
                                </tr>
                            </tbody>
                        </table>



                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">China</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" class="head" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>1</p>
                                        <p>2</p>
                                    </td>
                                    <td>
                                        <p>Beijing University of Ports and Telecommunication</p>
                                        <p>Nanchang Hangkong University</p>
                                    </td>
                                    <td>
                                        <p>B.Sc Engg computer Engineering</p>
                                        <p>&nbsp;B.Sc Engineering</p>
                                    </td>
                                    <td>
                                        <p><span>Computer Engineering</span></p>
                                        <p>Aeronautical Engineering</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>



                        <table class="table table-bordered table_saz2">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="7"><p class="country_name">UK</p></td>
                                </tr>
                                <tr>
                                    <td class="head" style="text-align: center;" width="5%"><strong>Sl.</strong></td>
                                    <td class="head" width="32%"><strong>Institute Name</strong></td>
                                    <td class="head" width="32%"><strong>Program Name</strong></td>
                                    <td class="head" width="31%"><strong>Division</strong></td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>1</p>
                                        <p>2</p>
                                    </td>
                                    <td class="style3 dept">
                                        <p>University of Greenwich</p>
                                        <p>Queen Mary University</p>
                                    </td>
                                    <td class="style3 dept">
                                        <p>&nbsp;B.Sc Engineering</p>
                                        <p>B.Sc Engineering</p>
                                    </td>
                                    <td>
                                        <p><span>Electrical &amp; Electronics Engineering</span></p>
                                        <p>&nbsp;Electrical &amp; Electronics Engineering</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<%@ include file="../footer.jsp" %>
