<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query subjectSQL = null;
    Object[] object = null;
    int responseCode = 0;
    String responseMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String subjectInfo = "";
    String univesityNameInfo = "";
    String universitySubjectInfo = "";
    String btnInvInfo = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();

    String eduMemberId = request.getParameter("eduMemberId") == null ? "" : request.getParameter("eduMemberId").trim();
    String universityId = request.getParameter("eduUniversityId") == null ? "" : request.getParameter("eduUniversityId").trim();
    String eduEducationId = request.getParameter("eduEducationId") == null ? "" : request.getParameter("eduEducationId").trim();

    String universityNameSQL = "select university_long_name from university  WHERE  university_id=" + universityId + "";

    univesityNameInfo = dbsession.createSQLQuery(universityNameSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(universityNameSQL).uniqueResult().toString();

    if (universityId != null) {

        String subjectId = "";
        String subjectName = "";
        String subjectDivisionId = "";
        String subjectIdDivisionId = "";
        String subjectOption = "";
        subjectSQL = dbsession.createSQLQuery("SELECT * FROM university_subject WHERE university_id='" + universityId + "' ORDER BY subject_long_name ASC");
        if (!subjectSQL.list().isEmpty()) {
            for (Iterator itr = subjectSQL.list().iterator(); itr.hasNext();) {
                object = (Object[]) itr.next();
                subjectId = object[0].toString();
                subjectName = object[2].toString();
                subjectDivisionId = object[5].toString();

                // subjectIdDivisionId = subjectId + "-" + subjectDivisionId;
                //  subjectOption = subjectOption + "<option value=\"" + subjectIdDivisionId + "\">" + subjectName + "</option>";
                subjectOption = subjectOption + "<option value=\"" + subjectId + "\">" + subjectName + "</option>";

            }
        }

        subjectInfo = "<select name=\"memberBSCUniversitySubjectId\" id=\"memberBSCUniversitySubjectId\" class=\"form-control chosen\" required>"
                + "<option value=\"\">Select Subject</option>"
                + "" + subjectOption + ""
                + "</select>";

        universitySubjectInfo = "<div class=\"form-group row\">"
                + "<label for=\"inputEmail3\" class=\"col-md-2 col-form-label text-right\">University</label>"
                + "<div class=\"col-md-10\"><input type=\"text\" class=\"form-control\" value=\"" + univesityNameInfo + "\" disabled=\"\"></div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"inputEmail3\" class=\"col-md-2 col-form-label text-right\">Subject</label>"
                + "<div class=\"col-md-10\">" + subjectInfo + "</div>"
                + "</div>";

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnmEducationInfoAddConfirm\" onclick=\"memberEducationBscSubjetInfoAddConfirm('" + session.getId() + "','" + eduMemberId + "','" + universityId + "','" + eduEducationId + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

        responseCode = 1;
        responseMsg = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Success!!!</strong> Subject Info show."
                + "</div>";

    } else {

        universitySubjectInfo = "<div class=\"form-group row\">"
                + "<label for=\"inputEmail3\" class=\"col-md-2 col-form-label text-right\">University</label>"
                + "<div class=\"col-md-10\"><input type=\"text\" class=\"form-control\" value=\"" + univesityNameInfo + "\" disabled=\"\"></div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"inputEmail3\" class=\"col-md-2 col-form-label text-right\">Subject</label>"
                + "<div class=\"col-md-10\">" + subjectInfo + "</div>"
                + "</div>";

        btnInvInfo = "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";

        responseMsg = "<div class=\"alert alert-danger alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<strong>Error!</strong> Please login and try again."
                + "</div>";

    }

    json = new JSONObject();
    json.put("universityId", universityId);
    json.put("subjectInfo", subjectInfo);
    json.put("univesityNameInfo", univesityNameInfo);
    json.put("universitySubjectInfo", universitySubjectInfo);
    json.put("btnInvInfo", btnInvInfo);
    json.put("responseCode", responseCode);
    json.put("responseMsg", responseMsg);
    jsonArr.add(json);
    out.print(jsonArr);

    dbsession.flush();
    dbsession.close();

%>