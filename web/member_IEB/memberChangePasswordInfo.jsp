<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    String memberName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String checkMale = "";
    String checkFemale = "";
    String mobileNo = "";
    String userEmail = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String centerId = "";
    String pictureLink = "";
    String mAddressLine1 = "";
    String mAddressLine2 = "";
    String pAddressLine1 = "";
    String pAddressLine2 = "";
    Member member = null;
    AddressBook addressBook = null;

    q1 = dbsession.createQuery("from Member as member WHERE id=" + memberIdH + " ");

    Object[] object = null;

    for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
        member = (Member) itr.next();

        memberName = member.getMemberName();
        placeOfBirth = member.getPlaceOfBirth() == null ? "" : member.getPlaceOfBirth();
        dob = member.getDob();
        gender = member.getGender().trim();
        if (gender.equals("1")) {
            gender = "Male";
            checkMale = " checked";
        } else {
            gender = "Female";
            checkFemale = " checked";
        }
        mobileNo = member.getMobile();
        phone1 = member.getPhone1();
        phone2 = member.getPhone2();
        userEmail = member.getEmailId();
        bloodGroup = member.getBloodGroup();

        pictureLink = "M_38661.jpg";

    }

    

    dbsession.flush();
    dbsession.close();


%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-key"></i> Change Password Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%
                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->
                    <p class="content_title_saz pb-2 m-3">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-8">
                            <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberChangePasswordInfoSubmitData.jsp?sessionid=<%=session.getId()%>&act=add" onSubmit="return fromDataSubmitValidation()">


                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-4 ml-4 font-weight-bold">Old Password</label>
                                    <div class="col-sm-8">
                                        <input id="oldPassword" name="oldPassword"  type="password" class="form-control input-sm" placeholder="Old password" required>
                                        <div id="oldPasswordErr" class="help-block with-errors"></div>
                                    </div>                                    
                                </div>
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-4 ml-4 font-weight-bold">New Password</label>
                                    <div class="col-sm-8">
                                        <input id="newPassword" name="newPassword" type="password" class="form-control input-sm"  placeholder="New password" required>
                                        <div id="newPasswordErr" class="help-block with-errors"></div>
                                    </div>                                    
                                </div>
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-4 ml-4 font-weight-bold">Retype New Password</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control input-sm" id="newPassword1" name="newPassword1" placeholder="Retype new password" required>
                                        <div class="help-block with-errors"></div>
                                    </div>                                    
                                </div>

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-4 ml-4 font-weight-bold">&nbsp;</label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-primary btn-sm ">Update</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="col-sm-4">
                            &nbsp;
                        </div>
                    </div>
                    <!-- ============ End personal information form ================== -->

                </div>
            </div>
        </div>
    </div>
</div>

<br/>                        


<%@ include file="../footer.jsp" %>
