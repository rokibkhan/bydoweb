<%@page import="java.time.Period"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.time.LocalDate"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    Query q1 = null;
    Query q2 = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";
    getRegistryID regId = new getRegistryID();
    String subscribeId = regId.getID(14);

    String sessionId = request.getParameter("sessionId").trim();
    String memberId = request.getParameter("memberId").trim();
    String memberTypeId = request.getParameter("memberTypeId").trim();
    String renewalType = request.getParameter("renewalType").trim();
    String renewalTypeId = request.getParameter("renewalTypeId").trim();

    //String requestId = request.getParameter("requestId").trim();
    String requestId = "";

    String mMemberTypeName = "";
    String mMemberTypeId = "";
    Query mMemberTypeSQL = null;
    Object[] mMemberTypeObj = null;
    mMemberTypeSQL = dbsession.createSQLQuery("SELECT mti.member_type_id,mti.member_type_name from member_type mt, member_type_info mti WHERE mt.member_type_id = mti.member_type_id AND mt.member_id='" + memberId + "' AND SYSDATE() between mt.ed and mt.td");

    for (Iterator mMemberTypeItr = mMemberTypeSQL.list().iterator(); mMemberTypeItr.hasNext();) {

        mMemberTypeObj = (Object[]) mMemberTypeItr.next();
        mMemberTypeId = mMemberTypeObj[0].toString();
        mMemberTypeName = mMemberTypeObj[1].toString();

    }

    //  double memberTotalAmount = 0.0d;
    double memberTypeTotalAmount = 0.0d;

    String name = "";
    System.out.println("sessionId " + sessionId);
    System.out.println("WEB :: memberRenewalTotalFeeShow :: sessionId ::" + sessionId);
    System.out.println("WEB :: memberRenewalTotalFeeShow :: memberId ::" + memberId);
    System.out.println("WEB :: memberRenewalTotalFeeShow :: memberTypeId ::" + memberTypeId);
    System.out.println("WEB :: memberRenewalTotalFeeShow :: renewalType ::" + renewalType);
    System.out.println("WEB :: memberRenewalTotalFeeShow :: renewalTypeId ::" + renewalTypeId);

    String mRenewalFeeSQL = "SELECT annaral_subscription_fee FROM member_type_info  WHERE  member_type_id='" + memberTypeId + "'";
    System.out.println("mRenewalFeeSQL :: " + mRenewalFeeSQL);
    String membershipAnnualSubscriptionFee = dbsession.createSQLQuery(mRenewalFeeSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(mRenewalFeeSQL).uniqueResult().toString();

    System.out.println("WEB :: memberRenewalTotalFeeShow :: membershipAnnualSubscriptionFee :: " + membershipAnnualSubscriptionFee);

    SimpleDateFormat dateFormatToday = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat dateFormatM = new SimpleDateFormat("MM");
    SimpleDateFormat dateFormatY = new SimpleDateFormat("yyyy");
    Date dateToday = new Date();
    String todayDate = dateFormatToday.format(dateToday);

    String todayMonth = dateFormatM.format(dateToday);
    //      String todayMonth = "1";

    String todayYear = dateFormatY.format(dateToday);

    String todayMemberFeesYearNameId = "";
    String todayMemberFeesYearName = "";

    String todayNextMemberFeesYearNameId = "";
    String todayNextMemberFeesYearName = "";
    int todayNextMemberFeesYearName1 = 0;
    int todayNextMemberFeesYearName2 = 0;
    String todayNextFeeYearSQL = "";

    String todayNextTwoMemberFeesYearNameId = "";
    String todayNextTwoMemberFeesYearName = "";
    int todayNextTwoMemberFeesYearName1 = 0;
    int todayNextTwoMemberFeesYearName2 = 0;
    String todayNextTwoFeeYearSQL = "";

    String todayNextThreeMemberFeesYearNameId = "";
    String todayNextThreeMemberFeesYearName = "";
    int todayNextThreeMemberFeesYearName1 = 0;
    int todayNextThreeMemberFeesYearName2 = 0;
    String todayNextThreeFeeYearSQL = "";

    String todayNextFiveMemberFeesYearNameId = "";
    String todayNextFiveMemberFeesYearName = "";
    int todayNextFiveMemberFeesYearName1 = 0;
    int todayNextFiveMemberFeesYearName2 = 0;
    String todayNextFiveFeeYearSQL = "";

    String todayMonthCheck = "";
    int currentYearValue = 0;

    if (Integer.parseInt(todayMonth) <= 6) {
        todayMonthCheck = "1-6-Month-January-June";
        currentYearValue = Integer.parseInt(todayYear) - 1;
        todayMemberFeesYearName = currentYearValue + "-" + todayYear;

        todayNextMemberFeesYearName1 = currentYearValue + 1;
        todayNextMemberFeesYearName2 = currentYearValue + 2;
        todayNextMemberFeesYearName = todayNextMemberFeesYearName1 + "-" + todayNextMemberFeesYearName2;
        todayNextFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextMemberFeesYearName + "'";
        todayNextMemberFeesYearNameId = dbsession.createSQLQuery(todayNextFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextFeeYearSQL).uniqueResult().toString();

        todayNextTwoMemberFeesYearName1 = currentYearValue + 2;
        todayNextTwoMemberFeesYearName2 = currentYearValue + 3;
        todayNextTwoMemberFeesYearName = todayNextTwoMemberFeesYearName1 + "-" + todayNextTwoMemberFeesYearName2;
        todayNextTwoFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextTwoMemberFeesYearName + "'";
        todayNextTwoMemberFeesYearNameId = dbsession.createSQLQuery(todayNextTwoFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextTwoFeeYearSQL).uniqueResult().toString();

        todayNextThreeMemberFeesYearName1 = currentYearValue + 3;
        todayNextThreeMemberFeesYearName2 = currentYearValue + 4;
        todayNextThreeMemberFeesYearName = todayNextThreeMemberFeesYearName1 + "-" + todayNextThreeMemberFeesYearName2;
        todayNextThreeFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextThreeMemberFeesYearName + "'";
        todayNextThreeMemberFeesYearNameId = dbsession.createSQLQuery(todayNextThreeFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextThreeFeeYearSQL).uniqueResult().toString();

        todayNextFiveMemberFeesYearName1 = currentYearValue + 5;
        todayNextFiveMemberFeesYearName2 = currentYearValue + 6;
        todayNextFiveMemberFeesYearName = todayNextFiveMemberFeesYearName1 + "-" + todayNextFiveMemberFeesYearName2;
        todayNextFiveFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextFiveMemberFeesYearName + "'";
        todayNextFiveMemberFeesYearNameId = dbsession.createSQLQuery(todayNextFiveFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextFiveFeeYearSQL).uniqueResult().toString();

    }

    if (Integer.parseInt(todayMonth) > 6 && Integer.parseInt(todayMonth) <= 12) {
        todayMonthCheck = "7-12-Month-July-December";
        currentYearValue = Integer.parseInt(todayYear) + 1;
        todayMemberFeesYearName = todayYear + "-" + currentYearValue;

        todayNextMemberFeesYearName1 = currentYearValue;
        todayNextMemberFeesYearName2 = currentYearValue + 1;
        todayNextMemberFeesYearName = todayNextMemberFeesYearName1 + "-" + todayNextMemberFeesYearName2;
        todayNextFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextMemberFeesYearName + "'";
        todayNextMemberFeesYearNameId = dbsession.createSQLQuery(todayNextFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextFeeYearSQL).uniqueResult().toString();

        todayNextTwoMemberFeesYearName1 = currentYearValue + 1;
        todayNextTwoMemberFeesYearName2 = currentYearValue + 2;
        todayNextTwoMemberFeesYearName = todayNextTwoMemberFeesYearName1 + "-" + todayNextTwoMemberFeesYearName2;
        todayNextTwoFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextTwoMemberFeesYearName + "'";
        todayNextTwoMemberFeesYearNameId = dbsession.createSQLQuery(todayNextTwoFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextTwoFeeYearSQL).uniqueResult().toString();

        todayNextThreeMemberFeesYearName1 = currentYearValue + 2;
        todayNextThreeMemberFeesYearName2 = currentYearValue + 3;
        todayNextThreeMemberFeesYearName = todayNextThreeMemberFeesYearName1 + "-" + todayNextThreeMemberFeesYearName2;
        todayNextThreeFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextThreeMemberFeesYearName + "'";
        todayNextThreeMemberFeesYearNameId = dbsession.createSQLQuery(todayNextThreeFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextThreeFeeYearSQL).uniqueResult().toString();

        todayNextFiveMemberFeesYearName1 = currentYearValue + 4;
        todayNextFiveMemberFeesYearName2 = currentYearValue + 5;
        todayNextFiveMemberFeesYearName = todayNextFiveMemberFeesYearName1 + "-" + todayNextFiveMemberFeesYearName2;
        todayNextFiveFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextFiveMemberFeesYearName + "'";
        todayNextFiveMemberFeesYearNameId = dbsession.createSQLQuery(todayNextFiveFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextFiveFeeYearSQL).uniqueResult().toString();

    }

    String todayFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayMemberFeesYearName + "'";
    todayMemberFeesYearNameId = dbsession.createSQLQuery(todayFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayFeeYearSQL).uniqueResult().toString();

    System.out.println("WEB :: memberRenewalTotalFeeShow :: todayNextMemberFeesYearName :: " + todayNextMemberFeesYearName);
    System.out.println("WEB :: memberRenewalTotalFeeShow :: todayNextMemberFeesYearNameId :: " + todayNextMemberFeesYearNameId);

    System.out.println("WEB :: memberRenewalTotalFeeShow :: todayNextTwoMemberFeesYearName :: " + todayNextTwoMemberFeesYearName);
    System.out.println("WEB :: memberRenewalTotalFeeShow :: todayNextTwoMemberFeesYearNameId :: " + todayNextTwoMemberFeesYearNameId);

    System.out.println("WEB :: memberRenewalTotalFeeShow :: todayNextThreeMemberFeesYearName :: " + todayNextThreeMemberFeesYearName);
    System.out.println("WEB :: memberRenewalTotalFeeShow :: todayNextThreeMemberFeesYearNameId :: " + todayNextThreeMemberFeesYearNameId);

    System.out.println("WEB :: memberRenewalTotalFeeShow :: todayNextFiveMemberFeesYearName :: " + todayNextFiveMemberFeesYearName);
    System.out.println("WEB :: memberRenewalTotalFeeShow :: todayNextFiveMemberFeesYearNameId :: " + todayNextFiveMemberFeesYearNameId);

    String mRenewalFeeInfoYearContainer = "";

    //1 Year
    if (renewalTypeId.equals("1")) {

        memberTypeTotalAmount = Double.valueOf(renewalTypeId) * Double.valueOf(membershipAnnualSubscriptionFee);

        mRenewalFeeInfoYearContainer = "<div class=\"form-group row\">"
                + "<label for=\"memberTypeName\" class=\"col-sm-3 col-form-label text-right\">Member Type</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\"></span></div>"
                + "<input name=\"memberTypeName\" id=\"memberTypeName\" value=\"" + mMemberTypeName + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\"></span></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"memberTypeName\" class=\"col-sm-3 col-form-label text-right\">Annual Fee</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\"></span></div>"
                + "<input name=\"memberTypeName\" id=\"memberTypeName\" value=\"" + membershipAnnualSubscriptionFee + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\"></span></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"memberTypeTotalAmount\" class=\"col-sm-3 col-form-label text-right\">Total Amount</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group mb-3\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\">TK</span></div>"
                + "<input name=\"memberTypeTotalAmountXXXX\" id=\"memberTypeTotalAmountXXXX\" value=\"" + memberTypeTotalAmount + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ; padding-bottom: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\">.00</span></div>"
                + "</div>"
                + "<div id=\"memberTypeTotalAmountErr\"></div>"
                + "</div>"
                + "<input name=\"memberTypeID\" id=\"memberTypeID\" value=\"" + memberTypeId + "\"  type=\"hidden\">"
                + "<input name=\"memberTypeAnnaralSubscriptionFee\" id=\"memberTypeAnnaralSubscriptionFee\" value=\"" + membershipAnnualSubscriptionFee + "\"  type=\"hidden\">"
                + "<input name=\"memberNID\" id=\"memberNID\" value=\"" + memberId + "\"  type=\"hidden\">"
                + "<input name=\"memberTotalAmount\" id=\"memberTotalAmount\" value=\"" + memberTypeTotalAmount + "\"  type=\"hidden\">"
                + "<input name=\"memberFeeYearName\" id=\"memberFeeYearName\" value=\"" + todayNextMemberFeesYearName + "\"  type=\"hidden\">"
                + "<input name=\"memberFeeYearNameId\" id=\"memberFeeYearNameId\" value=\"" + todayNextMemberFeesYearNameId + "\"  type=\"hidden\">"
                + "</div>";

    }
    //2 Years
    if (renewalTypeId.equals("2")) {

        memberTypeTotalAmount = Double.valueOf(renewalTypeId) * Double.valueOf(membershipAnnualSubscriptionFee);

        mRenewalFeeInfoYearContainer = "<div class=\"form-group row\">"
                + "<label for=\"memberTypeName\" class=\"col-sm-3 col-form-label text-right\">Member Type</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\"></span></div>"
                + "<input name=\"memberTypeName\" id=\"memberTypeName\" value=\"" + mMemberTypeName + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\"></span></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"memberTypeName\" class=\"col-sm-3 col-form-label text-right\">Annual Fee</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\"></span></div>"
                + "<input name=\"memberTypeName\" id=\"memberTypeName\" value=\"" + membershipAnnualSubscriptionFee + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\"></span></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"memberTypeTotalAmount\" class=\"col-sm-3 col-form-label text-right\">Total Amount</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group mb-3\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\">TK</span></div>"
                + "<input name=\"memberTypeTotalAmountXXXX\" id=\"memberTypeTotalAmountXXXX\" value=\"" + memberTypeTotalAmount + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ; padding-bottom: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\">.00</span></div>"
                + "</div>"
                + "</div>"
                + "<input name=\"memberTypeID\" id=\"memberTypeID\" value=\"" + memberTypeId + "\"  type=\"hidden\">"
                + "<input name=\"memberTypeAnnaralSubscriptionFee\" id=\"memberTypeAnnaralSubscriptionFee\" value=\"" + membershipAnnualSubscriptionFee + "\"  type=\"hidden\">"
                + "<input name=\"memberNID\" id=\"memberNID\" value=\"" + memberId + "\"  type=\"hidden\">"
                + "<input name=\"memberTotalAmount\" id=\"memberTotalAmount\" value=\"" + memberTypeTotalAmount + "\"  type=\"hidden\">"
                + "<input name=\"memberFeeYearName\" id=\"memberFeeYearName\" value=\"" + todayNextTwoMemberFeesYearName + "\"  type=\"hidden\">"
                + "<input name=\"memberFeeYearNameId\" id=\"memberFeeYearNameId\" value=\"" + todayNextTwoMemberFeesYearNameId + "\"  type=\"hidden\">"
                + "</div>";

    }
    //3 Years
    if (renewalTypeId.equals("3")) {

        memberTypeTotalAmount = Double.valueOf(renewalTypeId) * Double.valueOf(membershipAnnualSubscriptionFee);

        mRenewalFeeInfoYearContainer = "<div class=\"form-group row\">"
                + "<label for=\"memberTypeName\" class=\"col-sm-3 col-form-label text-right\">Member Type</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\"></span></div>"
                + "<input name=\"memberTypeName\" id=\"memberTypeName\" value=\"" + mMemberTypeName + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\"></span></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"memberTypeName\" class=\"col-sm-3 col-form-label text-right\">Annual Fee</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\"></span></div>"
                + "<input name=\"memberTypeName\" id=\"memberTypeName\" value=\"" + membershipAnnualSubscriptionFee + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\"></span></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"memberTypeTotalAmount\" class=\"col-sm-3 col-form-label text-right\">Total Amount</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group mb-3\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\">TK</span></div>"
                + "<input name=\"memberTypeTotalAmountXXXX\" id=\"memberTypeTotalAmountXXXX\" value=\"" + memberTypeTotalAmount + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ; padding-bottom: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\">.00</span></div>"
                + "</div>"
                + "</div>"
                + "<input name=\"memberTypeID\" id=\"memberTypeID\" value=\"" + memberTypeId + "\"  type=\"hidden\">"
                + "<input name=\"memberTypeAnnaralSubscriptionFee\" id=\"memberTypeAnnaralSubscriptionFee\" value=\"" + membershipAnnualSubscriptionFee + "\"  type=\"hidden\">"
                + "<input name=\"memberNID\" id=\"memberNID\" value=\"" + memberId + "\"  type=\"hidden\">"
                + "<input name=\"memberTotalAmount\" id=\"memberTotalAmount\" value=\"" + memberTypeTotalAmount + "\"  type=\"hidden\">"
                + "<input name=\"memberFeeYearName\" id=\"memberFeeYearName\" value=\"" + todayNextThreeMemberFeesYearName + "\"  type=\"hidden\">"
                + "<input name=\"memberFeeYearNameId\" id=\"memberFeeYearNameId\" value=\"" + todayNextThreeMemberFeesYearNameId + "\"  type=\"hidden\">"
                + "</div>";

    }

    //5 Years
    if (renewalTypeId.equals("5")) {

        memberTypeTotalAmount = Double.valueOf(renewalTypeId) * Double.valueOf(membershipAnnualSubscriptionFee);

        mRenewalFeeInfoYearContainer = "<div class=\"form-group row\">"
                + "<label for=\"memberTypeName\" class=\"col-sm-3 col-form-label text-right\">Member Type</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\"></span></div>"
                + "<input name=\"memberTypeName\" id=\"memberTypeName\" value=\"" + mMemberTypeName + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\"></span></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"memberTypeName\" class=\"col-sm-3 col-form-label text-right\">Annual Fee</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\"></span></div>"
                + "<input name=\"memberTypeName\" id=\"memberTypeName\" value=\"" + membershipAnnualSubscriptionFee + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\"></span></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"memberTypeTotalAmount\" class=\"col-sm-3 col-form-label text-right\">Total Amount</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group mb-3\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\">TK</span></div>"
                + "<input name=\"memberTypeTotalAmountXXXX\" id=\"memberTypeTotalAmountXXXX\" value=\"" + memberTypeTotalAmount + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ; padding-bottom: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\">.00</span></div>"
                + "</div>"
                + "</div>"
                + "<input name=\"memberTypeID\" id=\"memberTypeID\" value=\"" + memberTypeId + "\"  type=\"hidden\">"
                + "<input name=\"memberTypeAnnaralSubscriptionFee\" id=\"memberTypeAnnaralSubscriptionFee\" value=\"" + membershipAnnualSubscriptionFee + "\"  type=\"hidden\">"
                + "<input name=\"memberNID\" id=\"memberNID\" value=\"" + memberId + "\"  type=\"hidden\">"
                + "<input name=\"memberTotalAmount\" id=\"memberTotalAmount\" value=\"" + memberTypeTotalAmount + "\"  type=\"hidden\">"
                + "<input name=\"memberFeeYearName\" id=\"memberFeeYearName\" value=\"" + todayNextFiveMemberFeesYearName + "\"  type=\"hidden\">"
                + "<input name=\"memberFeeYearNameId\" id=\"memberFeeYearNameId\" value=\"" + todayNextFiveMemberFeesYearNameId + "\"  type=\"hidden\">"
                + "</div>";

    }

    //Life Member
    if (renewalTypeId.equals("20")) {

        int memberAge = 0;
        int memberAgeNextYear = 0;
        String memberAgeFee = "";
        String totalDueSumSQL = "";
        String memberTotalDue = "";

        String totalAdvanceSumSQL = "";
        String memberTotalAdvance = "";
        //  String memberTotalAmount = "";

        String ageFeeAmountSQL = "";
        String ageFeeAmount = "";

        String mMemberTypeAgeLimitSQL = "";
        String mMemberTypeAgeLimit = "";

//        Member member = null;
//        Query qMember = null;
//        java.util.Date memberDob = new Date();
//        qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
//        for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
//            member = (Member) itr0.next();
//            memberDob = member.getDob();
//        }
//        System.out.println("TTTTT :: memberDob ::  " + memberDob);
        String mMemberDOBSQL = "SELECT dob FROM member  WHERE  id='" + memberId + "'";
        System.out.println("mMemberDOBSQL:: " + mMemberDOBSQL);
        String mMemberDOBnn = dbsession.createSQLQuery(mMemberDOBSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(mMemberDOBSQL).uniqueResult().toString();

        System.out.println("TTTTT :: mMemberDOBnn ::  " + mMemberDOBnn);

        mMemberTypeAgeLimitSQL = "SELECT member_age FROM lifemember_age_limit  WHERE  member_type_id=" + mMemberTypeId + " AND SYSDATE() between ed and td";
        System.out.println("mMemberTypeAgeLimitSQL:: " + mMemberTypeAgeLimitSQL);
        mMemberTypeAgeLimit = dbsession.createSQLQuery(mMemberTypeAgeLimitSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(mMemberTypeAgeLimitSQL).uniqueResult().toString();

        //Calculate Age 
        //using Calendar Object
        //  String strDate = dateFormat.format(memberDob);
        //  String strDate = dateFormat.format(mMemberDOBnn);
        Date d = dateFormat.parse(mMemberDOBnn);
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int date = c.get(Calendar.DATE);
        LocalDate l1 = LocalDate.of(year, month, date);
        LocalDate now1 = LocalDate.now();
        Period diff1 = Period.between(l1, now1);

        memberAge = diff1.getYears();

        memberAgeNextYear = memberAge + 1;

        System.out.println("memberAge :: " + memberAge);
        System.out.println("memberAgeNextYear :: " + memberAgeNextYear);

        //check age Limit for Life lifemember_age_limit
        //Show age wise fee info form life_age_amount
        ageFeeAmountSQL = "SELECT amount FROM lifemember_age_fee  WHERE  age='" + memberAgeNextYear + "' AND member_type_id ='" + mMemberTypeId + "'  AND SYSDATE() between ed and td";
        System.out.println("ageFeeAmountSQL :: " + ageFeeAmountSQL);
        ageFeeAmount = dbsession.createSQLQuery(ageFeeAmountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(ageFeeAmountSQL).uniqueResult().toString();

        System.out.println("ageFeeAmount :: " + ageFeeAmount);
        //    memberAgeFee = "12050";
        memberAgeFee = ageFeeAmount;
        //total due
        //  String totalPaymentDue = "0";
        totalDueSumSQL = "SELECT sum(amount) FROM member_fee  WHERE  member_id=" + memberId + " AND status ='0'";
        System.out.println("totalDueSumSQL :: " + totalDueSumSQL);
        memberTotalDue = dbsession.createSQLQuery(totalDueSumSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(totalDueSumSQL).uniqueResult().toString();
        //   totalPaymentDue = Integer.parseInt(totalPaymentDue1);

        System.out.println("Life Member Fee Info :: memberTotalDue :: " + memberTotalDue);

        SimpleDateFormat dateFormatTodayLife = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormatMLife = new SimpleDateFormat("MM");
        SimpleDateFormat dateFormatYLife = new SimpleDateFormat("yyyy");
        Date dateTodayLife = new Date();
        String todayDateLife = dateFormatTodayLife.format(dateToday);
        String todayMonthLife = dateFormatMLife.format(dateToday);
        //   String todayMonthLife = "1";
        String todayYearLife = dateFormatYLife.format(dateTodayLife);

        String todayMemberFeesYearNameIdLife = "";
        String todayMemberFeesYearNameLife = "";

        String todayMonthCheckLife = "";
        int currentYearValueLife = 0;

        if (Integer.parseInt(todayMonthLife) <= 6) {
            todayMonthCheckLife = "1-6-Month";
            currentYearValueLife = Integer.parseInt(todayYearLife) - 1;
            todayMemberFeesYearNameLife = currentYearValueLife + "-" + todayYearLife;
        }

        if (Integer.parseInt(todayMonthLife) > 6 && Integer.parseInt(todayMonthLife) <= 12) {
            todayMonthCheckLife = "7-12-Month";
            currentYearValueLife = Integer.parseInt(todayYearLife) + 1;
            todayMemberFeesYearNameLife = todayYearLife + "-" + currentYearValueLife;
        }

        String todayFeeYearSQLLife = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayMemberFeesYearNameLife + "'";
        todayMemberFeesYearNameIdLife = dbsession.createSQLQuery(todayFeeYearSQLLife).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayFeeYearSQLLife).uniqueResult().toString();

        //  String totalPaymentDue = "0";
        totalAdvanceSumSQL = "SELECT sum(amount) FROM member_fee  WHERE  member_id=" + memberId + " AND fee_year > '" + todayMemberFeesYearNameIdLife + "' AND status ='1'";
        memberTotalAdvance = dbsession.createSQLQuery(totalAdvanceSumSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(totalAdvanceSumSQL).uniqueResult().toString();
        //   totalPaymentDue = Integer.parseInt(totalPaymentDue1);

        System.out.println("Life Member Fee Info :: memberTotalAdvance :: " + memberTotalAdvance);

        memberTypeTotalAmount = (Double.valueOf(memberAgeFee) + Double.valueOf(memberTotalDue)) - Double.valueOf(memberTotalAdvance);

        //   memberTypeTotalAmount = Double.valueOf(renewalTypeId) * Double.valueOf(membershipAnnualSubscriptionFee);
        mRenewalFeeInfoYearContainer = "<div class=\"form-group row\">"
                + "<label for=\"memberTypeName\" class=\"col-sm-3 col-form-label text-right\">Member Type</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\"></span></div>"
                + "<input name=\"memberTypeName\" id=\"memberTypeName\" value=\"" + mMemberTypeName + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\"></span></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"memberTypeName\" class=\"col-sm-3 col-form-label text-right\">Life Member Fee</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\"></span></div>"
                + "<input name=\"memberTypeName\" id=\"memberTypeName\" value=\"" + memberAgeFee + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\"></span></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"memberTypeName\" class=\"col-sm-3 col-form-label text-right\">Total Due</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\"></span></div>"
                + "<input name=\"memberTypeName\" id=\"memberTypeName\" value=\"" + memberTotalDue + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\"></span></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"memberTypeName\" class=\"col-sm-3 col-form-label text-right\">Total Advance</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\"></span></div>"
                + "<input name=\"memberTypeName\" id=\"memberTypeName\" value=\"" + memberTotalAdvance + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\"></span></div>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"form-group row\">"
                + "<label for=\"memberTypeTotalAmount\" class=\"col-sm-3 col-form-label text-right\">Total Amount</label>"
                + "<div class=\"col-4\">"
                + "<div class=\"input-group mb-3\">"
                + "<div class=\"input-group-prepend\"><span class=\"input-group-text\">TK</span></div>"
                + "<input name=\"memberTypeTotalAmountXXXX\" id=\"memberTypeTotalAmountXXXX\" value=\"" + memberTypeTotalAmount + "\" type=\"text\" class=\"form-control\" style=\"padding-top: .2rem ; padding-bottom: .2rem ;\" disabled>"
                + "<div class=\"input-group-append\"><span class=\"input-group-text\">.00</span></div>"
                + "</div>"
                + "</div>"
                + "<input name=\"memberTypeID\" id=\"memberTypeID\" value=\"" + memberTypeId + "\"  type=\"hidden\">"
                + "<input name=\"memberTypeAnnaralSubscriptionFee\" id=\"memberTypeAnnaralSubscriptionFee\" value=\"" + membershipAnnualSubscriptionFee + "\"  type=\"hidden\">"
                + "<input name=\"memberNID\" id=\"memberNID\" value=\"" + memberId + "\"  type=\"hidden\">"
                + "<input name=\"memberTotalAmount\" id=\"memberTotalAmount\" value=\"" + memberTypeTotalAmount + "\"  type=\"hidden\">"
                + "<input name=\"memberFeeYearName\" id=\"memberFeeYearName\" value=\"Life\"  type=\"hidden\">"
                + "<input name=\"memberFeeYearNameId\" id=\"memberFeeYearNameId\" value=\"20\"  type=\"hidden\">"
                + "</div>";
    }

    responseCode = "1";
    responseMsg = "Success!!! Renewal info found successfully";
    responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
            + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
            + "</div>";

    json.put("responseCode", responseCode);
    json.put("responseMsg", responseMsg);
    json.put("responseMsgHtml", responseMsgHtml);
    json.put("mRenewalFeeInfo", mRenewalFeeInfoYearContainer);
    json.put("requestId", requestId);
    jsonArr.add(json);


    /*

    String mRenewalFeeInfoContainer = "";

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date dateX = new Date();
    String adddate = dateFormatX.format(dateX);
    String adduser = "";
    String addterm = InetAddress.getLocalHost().getHostName().toString();
    String addip = InetAddress.getLocalHost().getHostAddress().toString();

    q1 = dbsession.createSQLQuery("SELECT * FROM member_proposer_info_temp WHERE status = '0' AND id='" + requestId + "'");
    if (!q1.list().isEmpty()) {

        Query q4 = dbsession.createSQLQuery("UPDATE  member_proposer_info_temp SET status='1',app_rej_date='" + adddate + "' WHERE id='" + requestId + "'");

        q4.executeUpdate();

        dbtrx.commit();
        if (dbtrx.wasCommitted()) {
            responseCode = "1";
            responseMsg = "Success!!! Request approved successfully";
            responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                    + "</div>";

        } else {
            responseCode = "0";
            responseMsg = "Error!!! When Request approve in system";

            responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i>" + responseMsg + "</span>"
                    + "</div>";
        }

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", requestId);
        jsonArr.add(json);

    } else {

        responseCode = "0";
        responseMsg = "Error!!! Request already approved";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", requestId);
        jsonArr.add(json);
    }

     */
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>