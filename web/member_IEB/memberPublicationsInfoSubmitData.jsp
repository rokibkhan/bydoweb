
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>



<%
    Session dbsession = null;
    String qryparam = "";;

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    getDate date = new getDate();

//    if (session.isNew()) {
//        response.sendRedirect("logout.jsp");
//    }
    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        memberIdH = session.getAttribute("memberId").toString();

        System.out.println("memberIdH " + memberIdH);

        getRegistryID regId = new getRegistryID();

        MemberPublicationInfo memberPublicationInfo = null;
        String act = request.getParameter("act").trim();

        if (act.equals("edit")) {
            String publicationId = request.getParameter("publicationId").trim();
            Query q4 = dbsession.createQuery("from MemberPublicationInfo where id='" + publicationId + "' ");
            Iterator itr4 = q4.list().iterator();
            if (itr4.hasNext()) {
                memberPublicationInfo = (MemberPublicationInfo) itr4.next();
            }

            String publicationTitle = request.getParameter("publicationTitle").trim();
            String publicationAuthor = request.getParameter("publicationAuthor").trim();
            String publicationRadioJournal = request.getParameter("publicationRadioJournal").trim();
            String publicationJournalCon = request.getParameter("publicationJournalCon").trim();
            String publicationVolume = request.getParameter("publicationVolume") == null ? "" : request.getParameter("publicationVolume").trim();
            String publicationNumber = request.getParameter("publicationNumber") == null ? "" : request.getParameter("publicationNumber").trim();
            String publicationPage = request.getParameter("publicationPage") == null ? "" : request.getParameter("publicationPage").trim();
            String publicationYear = request.getParameter("publicationYear") == null ? "" : request.getParameter("publicationYear").trim();
            String publicationMonth = request.getParameter("publicationMonth") == null ? "" : request.getParameter("publicationMonth").trim();
            String publicationPublisher = request.getParameter("publicationPublisher") == null ? "" : request.getParameter("publicationPublisher").trim();
            String publicationURL = request.getParameter("publicationURL") == null ? "" : request.getParameter("publicationURL").trim();
 
           Member member = new Member();
            int memberId1 = 0;
            String memberId = session.getAttribute("memberId").toString();
            System.out.println("#######memberId : "+memberId);
            memberId1 = Integer.parseInt(memberId);

            member.setId(memberId1);
            memberPublicationInfo.setMember(member);
            memberPublicationInfo.setPublicationTitle(publicationTitle);
            memberPublicationInfo.setPublicationAuthor(publicationAuthor);
            memberPublicationInfo.setPublicationJournalConference(publicationRadioJournal);
            memberPublicationInfo.setPublicationJournalConferenceName(publicationJournalCon);
            memberPublicationInfo.setPublicationVolume(publicationVolume);
//            memberPublicationInfo.setPublicationNumber(publicationNumber);
            memberPublicationInfo.setPublicationPage(publicationPage);
            memberPublicationInfo.setPublicationYear(publicationYear);
            memberPublicationInfo.setPublicationMonth(publicationMonth);
            memberPublicationInfo.setPublicationPublisher(publicationPublisher);
            memberPublicationInfo.setPublicationUrl(publicationURL);
            dbsession.update(memberPublicationInfo);
            strMsg = "Information updated successfully.";
        } else if (act.equals("add")) {

            String publicationTitle = request.getParameter("publicationTitle").trim();
            String publicationAuthor = request.getParameter("publicationAuthor").trim();
            String publicationRadioJournal = request.getParameter("publicationRadioJournal").trim();
            String publicationJournalCon = request.getParameter("publicationJournalCon").trim();
            String publicationVolume = request.getParameter("publicationVolume") == null ? "" : request.getParameter("publicationVolume").trim();
            String publicationNumber = request.getParameter("publicationNumber") == null ? "" : request.getParameter("publicationNumber").trim();
            String publicationPage = request.getParameter("publicationPage") == null ? "" : request.getParameter("publicationPage").trim();
            String publicationYear = request.getParameter("publicationYear") == null ? "" : request.getParameter("publicationYear").trim();
            String publicationMonth = request.getParameter("publicationMonth") == null ? "" : request.getParameter("publicationMonth").trim();
            String publicationPublisher = request.getParameter("publicationPublisher") == null ? "" : request.getParameter("publicationPublisher").trim();
            String publicationURL = request.getParameter("publicationURL") == null ? "" : request.getParameter("publicationURL").trim();

            String publicationId = regId.getID(41);
            memberPublicationInfo = new MemberPublicationInfo();
            Member member = new Member();
            int memberId1 = 0;
            int publicationId1 = 0;
            String memberId = session.getAttribute("memberId").toString();
            memberId1 = Integer.parseInt(memberId);
            publicationId1 = Integer.parseInt(publicationId);

            member.setId(memberId1);
            memberPublicationInfo.setId(publicationId1);
            memberPublicationInfo.setMember(member);
            memberPublicationInfo.setPublicationTitle(publicationTitle);
            memberPublicationInfo.setPublicationAuthor(publicationAuthor);
            memberPublicationInfo.setPublicationJournalConference(publicationRadioJournal);
            memberPublicationInfo.setPublicationJournalConferenceName(publicationJournalCon);
            memberPublicationInfo.setPublicationVolume(publicationVolume);
//            memberPublicationInfo.setPublicationNumber(publicationNumber);
            memberPublicationInfo.setPublicationPage(publicationPage);
            memberPublicationInfo.setPublicationYear(publicationYear);
            memberPublicationInfo.setPublicationMonth(publicationMonth);
            memberPublicationInfo.setPublicationPublisher(publicationPublisher);
            memberPublicationInfo.setPublicationUrl(publicationURL);
            dbsession.save(memberPublicationInfo);
            strMsg = "Information saved successfully.";

        } else if (act.equals("del")) {
            String publicationId = request.getParameter("publicationId").trim();
            Query q4 = dbsession.createQuery("delete from MemberPublicationInfo where id='" + publicationId + "' ");
            q4.executeUpdate();
            strMsg = "Information removed successfully.";
        }

        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
//            strMsg = "Success!!! Member Training Information added ";
            response.sendRedirect("memberPublicationsInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        } else {
            strMsg = "Error!!! When Publication Information added ";
            dbtrx.rollback();
            response.sendRedirect("memberPublicationsInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        }
    } else {
        System.out.println("LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

%>
