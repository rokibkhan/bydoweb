<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);

    }

    System.out.println("Header sessionIdH :: " + sessionIdH);
    System.out.println("Header userNameH :: " + userNameH);
    System.out.println("Header userStoreIdH :: " + memberIdH);

//    String sessionid = request.getParameter("sessionid").trim();
//
//    sessionid = session.getId();
//    String uid = session.getAttribute("username").toString().toUpperCase();    
//    String userStoreId = session.getAttribute("memberId").toString();
//    String sessionidH = request.getParameter("sessionid").trim();
//
//    sessionidH = session.getId();
//    String uidH = session.getAttribute("username").toString().toUpperCase();    
    //   String userStoreIdH = session.getAttribute("memberId").toString();
//System.out.println("uidH :: "+uidH);
    String regMemberTempId = request.getParameter("regTempId") == null ? "" : request.getParameter("regTempId").trim();
    String regMemId = request.getParameter("regMemId") == null ? "" : request.getParameter("regMemId").trim();
    String regStep = request.getParameter("regStep") == null ? "" : request.getParameter("regStep").trim();

    String showTabOption;
    if (regStep.equals("6")) {
        showTabOption = " show";

    } else {
        showTabOption = "";
    }

    Query q1 = null;
    Object[] object = null;

    String memberName = "";
    String fatherName = "";
    String motherName = "";
    String datefBirth = "";
    String placeOfBirth = "";
    String gender = "";
    String age = "";
    String nationality = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String mobileNumber = "";
    String userEmail = "";
    String centerId = "";
    String pictureName = "";
    String pictureLink = "";

    String membershipApplyingFor = "";
    String presentIEBmembershipNumber = "";

    q1 = dbsession.createSQLQuery("select * from member_temp WHERE id= " + regMemId + "");
    for (Iterator itr2 = q1.list().iterator(); itr2.hasNext();) {
        object = (Object[]) itr2.next();
        memberName = object[4].toString();
        fatherName = object[5] == null ? "" : object[5].toString();
        motherName = object[6] == null ? "" : object[6].toString();
        placeOfBirth = object[7].toString();
        datefBirth = object[8].toString();
        age = object[9] == null ? "" : object[9].toString();
        nationality = object[10] == null ? "" : object[10].toString();
        gender = object[11].toString();
        membershipApplyingFor = object[12] == null ? "" : object[12].toString();
        presentIEBmembershipNumber = object[13] == null ? "" : object[13].toString();

        phone1 = object[14] == null ? "" : object[14].toString();
        phone2 = object[15] == null ? "" : object[15].toString();
        mobileNumber = object[16] == null ? "" : object[16].toString();
        userEmail = object[17].toString();
        pictureName = object[25].toString();

        pictureLink = "<img src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + memberName + "\" class=\"img-fluid img_1\">";

    }


%>

<%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }
%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<!-- Login & Register & About CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about.css">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">

    function showDivisionWisSubDivisionInfo(arg1) {
        console.log("agr1:: " + arg1);

        $.post("registrationDivisionWisSubDivisionInfoShow.jsp", {divisionId: arg1}, function (data) {

            if (data[0].responseCode == 1) {
                $("#showSubDivision").html(data[0].subDivisionInfo);
            }

        }, "json");

    }

</script>

<!-- Start of navigation -->
<div class="about_banner_membership">
    <h2 class="display_41_saz text-white"> Registration</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Membership</a></li>
                <li class="breadcrumb-item active" aria-current="page">Registration</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of navigation -->

<!-- Starting of Registration form -->
<div class="registration">
    <div class="container bg-white my-3 py-3">
        <!--        <h2 class="text-center mx-auto my-5 font-weight-bold text-dark" style="font-size:3.0rem;"><span style="color: #0044cc;">MEMBERSHIP</span> FROM</h2>-->
        <div class="row globalAlertInfoBoxConParentTT">
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                    <%=msgInfoText%>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h3 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-check-circle fa-lg"></i>  <span class="h4">Section 1: Personal Information</span>
                                </button>
                            </h3>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="#">
                                    <div class="form-row form-group m-0">
                                        <div class="col-6">
                                            <label for="inputName">Name:</label>
                                            <%=memberName%>
                                        </div>
                                        <div class="col-6">
                                            <label for="inputName">Father Name:</label>
                                            <%=fatherName%>
                                        </div>
                                    </div>

                                    <div class="form-row form-group m-0">
                                        <div class="col-6">
                                            <label for="inputName">Mother Name:</label>
                                            <%=motherName%>
                                        </div>
                                        <div class="col-6">
                                            <label for="inputName">Gender:</label>
                                            <%=gender%>
                                        </div>
                                    </div>

                                    <div class="form-row form-group m-0">
                                        <div class="col-6">
                                            <label for="inputDOB">Date of Birth:</label>
                                            <%=datefBirth%>
                                        </div>
                                        <div class="col-6">
                                            <label for="inputAge">Age:</label>
                                            <%=age%>
                                        </div>
                                    </div>
                                    <div class="form-row form-group m-0">
                                        <div class="col-6">
                                            <label for="inputPOB">Place of Birth:</label>
                                            <%=placeOfBirth%>
                                        </div>
                                        <div class="col-6">
                                            <label for="inputAge">Nationality:</label>
                                            <%=nationality%>
                                        </div>
                                    </div>

                                    <div class="form-row form-group m-0">
                                        <div class="col-6">
                                            <label for="inputPOB">Office Phone:</label>
                                            <%=phone1%>
                                        </div>
                                        <div class="col-6">
                                            <label for="inputAge">Residence Phone:</label>
                                            <%=phone2%>
                                        </div>
                                    </div>
                                    <div class="form-row form-group m-0">
                                        <div class="col-6">
                                            <label for="inputPOB">Mobile:</label>
                                            <%=mobileNumber%>
                                        </div>
                                        <div class="col-6">
                                            <label for="inputAge">Email:</label>
                                            <%=userEmail%>
                                        </div>
                                    </div>







                                    <div class="form-row form-group m-0">
                                        <div class="col-12">
                                            <label for="inputName">Membership Applying For:</label>
                                            <%=membershipApplyingFor%>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12">
                                            <label for="inputName">Present IEB Membership Number:</span></label>
                                            <%=presentIEBmembershipNumber%>
                                        </div>
                                    </div>




                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingOneVerify">
                            <h3 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOneVerify" aria-expanded="true" aria-controls="collapseOneVerify">
                                    <i class="fa fa-check-circle fa-lg"></i>  <span class="h4">Section 2: Verification</span>
                                </button>
                            </h3>
                        </div>

                        <div id="collapseOneVerify" class="collapse" aria-labelledby="headingOneVerify" data-parent="#accordion">
                            <div class="card-body">
                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationVerificationSubmitData.jsp">
                                    <p class="m-2">Please check your mobile inbox and add verification code and click verify button </p>

                                    <div class="form-row form-group">
                                        <label class="col-2 text-right" for="inputName">Reg No:</label>
                                        <div class="col-6">
                                            <p><%=regMemberTempId%></p>
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <label class="col-2 text-right" for="inputName">Mobile:</label>
                                        <div class="col-4">
                                            <p><%=mobileNumber%></p>
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <label class="col-2 text-right" for="inputName">Verification</label>
                                        
                                        <div class="col-3">
                                            <button type="submit" class="btn btn-primary" disabled="">Already Verified</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingFourMyPicture">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseMyPicture" aria-expanded="false" aria-controls="collapseMyPicture">
                                    <i class="fa fa-check-circle fa-lg"></i>  <span class="h4">Section 3: My Picture Upload</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseMyPicture" class="collapse" aria-labelledby="headingMyPicture" data-parent="#accordion">
                            <div class="card-body">

                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/RegistrationMyPictureUpload" enctype="multipart/form-data">
                                    <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                    <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                    <div class="row">
                                        <div class="col-10 offset-1">
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-2 text-right" for="inputName" style="margin-top: 0.25rem;">My Picture</label>


                                                <div class="col-4">
                                                    <%=pictureLink%>
                                                </div>
                                                <div class="col-4">
                                                    &nbsp;
                                                </div>
                                            </div> 


                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">

                                            <div class="form-row form-group">                                                
                                                <div class="col-2 offset-5">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div> 


                                    <div class="row">
                                        <div class="col-12">
                                            <p class="text-dark" style="font-size: 0.8rem;">Note |: Only .JPGE and .PNG files allow and file not more then 5MB.</p>
                                        </div>
                                    </div>



                                </form>

                            </div>
                        </div>
                    </div>                    


                    <div class="card">
                        <div class="card-header" id="headingOneAddressInfo">
                            <h3 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOneAddressInfo" aria-expanded="true" aria-controls="collapseOneAddressInfo">
                                    <i class="fa fa-check-circle fa-lg"></i>  <span class="h4">Section 4: Address Information</span>
                                </button>
                            </h3>
                        </div>

                        <div id="collapseOneAddressInfo" class="collapse" aria-labelledby="headingOneAddressInfo" data-parent="#accordion">
                            <div class="card-body">
                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationAddressInfoSubmitData.jsp">
                                    <div class="row">


                                        <%
                                            Query madrSQL = null;
                                            Object[] madrObject = null;

                                            Query padrSQL = null;
                                            Object[] padrObject = null;

                                            int mAddressId = 0;
                                            String mCountryId = "";
                                            String mDistrictId = "";
                                            String mThanaId = "";
                                            String mThanaName = "";
                                            String mThanaDistrictName = "";
                                            String mAddressLine1 = "";
                                            String mAddressLine2 = "";
                                            String mZipOffice = "";
                                            String mZipCode = "";

                                            int pAddressId = 0;
                                            String pCountryId = "";
                                            String pDistrictId = "";
                                            String pThanaId = "";
                                            String pThanaName = "";
                                            String pThanaDistrictName = "";
                                            String pAddressLine1 = "";
                                            String pAddressLine2 = "";
                                            String pZipOffice = "";
                                            String pZipCode = "";

                                            String mAddressStr = "";
                                            String pAddressStr = "";

                                            Thana thana = null;

                                            Query mailAddrSQL = dbsession.createSQLQuery("select *from address_book WHERE id=(select address_id from member_address_temp where member_id= " + regMemId + " and address_Type='M' ) ");
                                            for (Iterator itr2 = mailAddrSQL.list().iterator(); itr2.hasNext();) {
                                                madrObject = (Object[]) itr2.next();
                                                mAddressLine1 = madrObject[1].toString();
                                                mAddressLine2 = madrObject[2].toString();
                                                mThanaId = madrObject[3].toString();

                                                Query mThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
                                                for (Iterator itrmThana = mThanaSQL.list().iterator(); itrmThana.hasNext();) {
                                                    thana = (Thana) itrmThana.next();

                                                    mThanaName = thana.getThanaName();
                                                    mThanaDistrictName = thana.getDistrict().getDistrictName();
                                                }

                                                mZipCode = madrObject[4].toString();
                                                mZipOffice = madrObject[8].toString();

                                                mAddressStr = mAddressLine1 + "," + mAddressLine2 + "<br>" + mThanaName + "," + mThanaDistrictName + "<br>" + mZipOffice + "-" + mZipCode;

                                            }
                                            Query permAddrSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address_temp where member_id= " + regMemId + " and address_Type='P' ) ");
                                            for (Iterator itr3 = permAddrSQL.list().iterator(); itr3.hasNext();) {

                                                padrObject = (Object[]) itr3.next();
                                                pAddressLine1 = padrObject[1].toString();
                                                pAddressLine2 = padrObject[2].toString();
                                                pThanaId = padrObject[3].toString();

                                                Query pThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
                                                for (Iterator itrpThana = pThanaSQL.list().iterator(); itrpThana.hasNext();) {
                                                    thana = (Thana) itrpThana.next();

                                                    pThanaName = thana.getThanaName();
                                                    pThanaDistrictName = thana.getDistrict().getDistrictName();
                                                }

                                                pZipCode = padrObject[4].toString();
                                                pZipOffice = padrObject[8].toString();

                                                pAddressStr = pAddressLine1 + "," + pAddressLine2 + "<br>" + pThanaName + "," + pThanaDistrictName + "<br>" + pZipOffice + "-" + pZipCode;

                                            }

                                        %>


                                        <div class="col-6">
                                            <p class="m-2 text-center">Mailing Address</p>
                                            <address class="m-2 text-center">
                                                <%=mAddressStr%>
                                            </address>

                                        </div>
                                        <div class="col-6">
                                            <p class="m-2 text-center">Permanent Address</p>

                                            <address class="m-2 text-center">
                                                <%=pAddressStr%>
                                            </address>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <i class="fa fa-check-circle fa-lg"></i>  <span class="h4">Section 5: Education Information</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">

                                <div class="row">
                                    <%
                                        Query educationSQL = null;
                                        Query q2 = null;
                                        int degreeId = 0;
                                        String degreeName = "";
                                        String instituteName = "";
                                        String boardUniversityName = "";
                                        //java.util.Date dob = new Date();
                                        String yearOfPassing = "";
                                        String resultTypeName = "";
                                        String result = "";

                                        MemberEducationInfoTemp mei = null;

                                        educationSQL = dbsession.createQuery("from MemberEducationInfoTemp where  member_id=" + regMemId + " ");

                                        MemberTemp membertemp = null;
                                        String memberSubdivisionName = "";
                                        String memberSubdivisionFullName = "";
                                        int educationfInfoId = 0;
                                    %>


                                    <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                        <thead>
                                            <tr>
                                                <th scope="col">Degree</th>
                                                <th scope="col">Board/University</th>
                                                <th scope="col">Year of Passing</th>
                                                <th scope="col">Score/Class</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                for (Iterator itr = educationSQL.list().iterator(); itr.hasNext();) {
                                                    mei = (MemberEducationInfoTemp) itr.next();

                                                    educationfInfoId = mei.getId();
                                                    degreeId = mei.getDegree().getDegreeId();
                                                    degreeName = mei.getDegree().getDegreeName();

                                                    instituteName = mei.getInstituteName() == null ? "" : mei.getInstituteName();

                                                    boardUniversityName = mei.getUniversity().getUniversityLongName();
                                                    yearOfPassing = mei.getYearOfPassing();
                                                    resultTypeName = mei.getResultType().getResultTypeName();
                                                    result = mei.getResult();

                                                   

                                                    degreeName = degreeName + memberSubdivisionFullName;
                                            %>
                                            <tr id="infoBox<%=educationfInfoId%>">
                                                <td><%=degreeName%></td>
                                                <td><%=boardUniversityName%></td>
                                                <td><%=yearOfPassing%></td>
                                                <td><%=result%></td>


                                            </tr>
                                            <%
                                                }


                                            %>

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <span class="h4">Section 6: Field of Engineering</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse <%=showTabOption%>" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">

                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationFieldEngineeringSubmitData.jsp">
                                    <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                    <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                    <div class="row">
                                        <div class="col-5">
                                            <div class="form-row form-group">
                                                <label class="col-3 col-form-label text-right" for="inputName">Division:</label>
                                                <div class="col-8">
                                                    <select name="memberDivisionId" id="memberDivisionId" onchange="showDivisionWisSubDivisionInfo(this.value)" class="form-control" required>
                                                        <option value="">Select Division</option>
                                                        <%
                                                            Object[] objectDiv = null;
                                                            String divisionId = "";
                                                            String divisionName = "";
                                                            String divisionOptions = "";
                                                            Query divisionSQL = dbsession.createSQLQuery("select * FROM member_division ORDER BY mem_division_name ASC");
                                                            for (Iterator itr9 = divisionSQL.list().iterator(); itr9.hasNext();) {
                                                                objectDiv = (Object[]) itr9.next();
                                                                divisionId = objectDiv[0].toString();
                                                                divisionName = objectDiv[1].toString();
                                                                divisionOptions = divisionOptions + "<option value=\"" + divisionId + "\">" + divisionName + "</option>";

                                                            }
                                                        %>
                                                        <%=divisionOptions%>
                                                    </select>
                                                </div>                                                    
                                            </div>                                                   
                                        </div>
                                        <div class="col-5">
                                            <div class="form-row form-group">
                                                <label class="col-3 col-form-label text-right " for="inputName">Sub-Division:</label>
                                                <div class="col-8" id="showSubDivision">
                                                    <select name="memberSubDivision" id="memberSubDivision" class="form-control" required>
                                                        <option value="">Select Sub-Division</option>

                                                    </select>
                                                </div>                                                    
                                            </div>                                                   
                                        </div> 
                                        <div class="col-2">
                                            <button type="submit" class="btn btn-block btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>



                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <span class="h4">Section 7: Professional Record</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">

                               <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationProfessionalInfoSubmitData.jsp">
                                    <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                    <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                    
                                    <ul class="list-inline" id="professionRecordList">
                                        <li id="proRecList1">

                                            <div class="form-row">
                                                <div class="form-group col-md-3">
                                                    <label for="inputNationality">Organization *</label>
                                                    <input name="memProfOrgName1" id="memProfOrgName1" type="text" class="form-control" placeholder="Oranization name" required disabled>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputPOB">Designation *</label>
                                                    <input name="memProfDesignation1" id="memProfDesignation1" type="text" class="form-control" placeholder="Designation name" required disabled>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputPOB">Start Date *</label>
                                                    <input type="text" class="form-control" id="startDate1" name="startDate1" placeholder="yyyy-mm-dd" required="" disabled> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputPOB">End Date *</label>
                                                    <input type="text" class="form-control" id="endDate1" name="endDate1"  placeholder="yyyy-mm-dd" required="" disabled> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                                </div>
                                            </div>
                                            <script type="text/javascript">

                                                jQuery('#startDate1').datepicker({
                                                    autoclose: true,
                                                    todayHighlight: true,
                                                    format: 'yyyy-mm-dd'
                                                });
                                                jQuery('#endDate1').datepicker({
                                                    autoclose: true,
                                                    todayHighlight: true,
                                                    format: 'yyyy-mm-dd'
                                                });

                                            </script>  


                                        </li>
                                    </ul>
                                    <div class="form-row"> 
                                        <div class="form-group col-md-6">
                                            <a onclick="addMoreProfessionInfo();" class="btn btn-success text-white disabled"><i class="fas fa-plus-circle"></i> Add record</a>

                                            &nbsp;&nbsp;
                                            <a id="deleteMoreProfessionInfoBtn" onclick="deleteMoreProfessionInfo();" class="btn btn-danger text-white disabled"><i class="fas fa-minus-circle"></i> Delete record</a>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">

                                            <div class="form-row form-group">                                                
                                                <div class="col-2 offset-5">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div>     
                                </form>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFourDocuments">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourDocuments" aria-expanded="false" aria-controls="collapseFourDocuments">
                                    <span class="h4">Section 8: Related Documents Upload</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFourDocuments" class="collapse" aria-labelledby="headingFourDocuments" data-parent="#accordion">
                            <div class="card-body">

                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/RegistrationRelatedDocumentsUpload" enctype="multipart/form-data">
                                    <input type="hidden" name="memberRegId" id="memberRegId" value="1<%//=regMemberTempId%>" >
                                    <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                    <div class="row">
                                        <div class="col-10 offset-1">
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-2 text-right" for="inputName" style="margin-top: 0.25rem;">Required Document</label>

                                                <input name="documentTypeNID" id="documentTypeNID" value="1"  type="hidden">
                                                <div class="col-4">
                                                    <input name="fileNID" id="fileNID" onchange="relatedDocumentUplaodFileTypeSize();"  type="file" class="form-control input-sm" style="padding-top: .2rem ; padding-bottom: .2rem ;"  required disabled="">  
                                                    <div id="fileNIDErr"></div>
                                                </div>
                                                <div class="col-4">
                                                    &nbsp;
                                                </div>
                                            </div> 


                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">

                                            <div class="form-row form-group">                                                
                                                <div class="col-2 offset-5">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div> 



                                   <div class="row">
                                        <div class="col-12">
                                            <p class="text-dark" style="font-size: 0.8rem;">Note |: Only .zip files allow. Please add below documents when create zip file.  
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   i. Certificates: SSC, HSC/Diploma, B.Sc. Engineering copy
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  ii. Transcripts: HSC/Diploma, B.Sc. Engineering copy
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii. Certificates of the others professional bodies (if any) copy
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii. Photocopy of NID/Smart card copy
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  iv. Recognized training record of last two (2) years copy
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   v. Details records of 150 words (member) and 200 words (fellow) on experience demonstrating competencies & abilities. copy
                                            </p>
                                        </div>
                                    </div>



                                </form>

                            </div>
                        </div>
                    </div>                


                    <div class="card">
                        <div class="card-header" id="headingFourRecom">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourRecom" aria-expanded="false" aria-controls="collapseFourRecom">
                                    <span class="h4">Section 9: Recommendation Info</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFourRecom" class="collapse" aria-labelledby="headingFourRecom" data-parent="#accordion">
                            <div class="card-body">

                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationRecomendationInfoSubmitData.jsp">
                                    <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                    <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >

                                    <div class="row">
                                        <div class="col-4">
                                            <p class="m-2 text-center text-dark">Proposer</p>
                                            <div class="form-row form-group">
                                                <div class="col-8 offset-2">
                                                    
                                                    <input name="proposerMemberId" id="proposerMemberId" type="text" class="form-control" placeholder="Membership No" required disabled>
                                                </div>                                                    
                                            </div> 
                                            <div class="form-row form-group">
                                                <div class="col-8 offset-2">
                                                    <input name="proposerMemberName" id="proposerMemberName" type="text" class="form-control" placeholder="Member name" required disabled>
                                                </div>                                                    
                                            </div>    

                                        </div>
                                        <div class="col-4">
                                            <p class="m-2 text-center text-dark">Seconder |</p>
                                            <div class="form-row form-group">
                                                <div class="col-8 offset-2">
                                                    <input name="seconderMemberId" id="seconderMemberId" type="text" class="form-control" placeholder="Membership No" required<input name="proposerMemberName" id="proposerMemberName" type="text" class="form-control" placeholder="Member name" required disabled>
                                                </div>                                                    
                                            </div> 
                                            <div class="form-row form-group">
                                                <div class="col-8 offset-2">
                                                    <input name="seconderMemberName" id="seconderMemberName" type="text" class="form-control" placeholder="Member name" required disabled>
                                                    
                                                </div>                                                    
                                            </div>    

                                        </div>
                                        <div class="col-4">
                                            <p class="m-2 text-center text-dark">Seconder ||</p>
                                            <div class="form-row form-group">
                                                <div class="col-8 offset-2">
                                                    <input name="thirdMemberId" id="thirdMemberId" type="text" class="form-control" placeholder="Membership No"<input name="proposerMemberName" id="proposerMemberName" type="text" class="form-control" placeholder="Member name" required disabled>
                                                </div>                                                    
                                            </div> 
                                            <div class="form-row form-group">
                                                <div class="col-8 offset-2">
                                                    <input name="thirdMemberName" id="thirdMemberName" type="text" class="form-control" placeholder="Member name" disabled>
                                                    
                                                </div>                                                    
                                            </div>    

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">

                                            <div class="form-row form-group">                                                
                                                <div class="col-2 offset-5">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div>     
                                </form>
                                <div class="row">
                                    <div class="col-12">
                                        <p class="text-dark" style="font-size: 0.8rem;">Note |: Proposer and Seconder must be at least 
                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i.Two Fellows and one Member for Fellowship
                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ii.One Fellow and two Members for Fellowship
                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii.Two Members for Associate Membership
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<!-- Ending of Registration form -->

<%

    dbsession.flush();
    dbsession.close();
%>

<%@ include file="../footer.jsp" %>
