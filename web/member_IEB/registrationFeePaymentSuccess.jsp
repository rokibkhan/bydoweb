<%@page import="com.ssl.commerz.parametermappings.SSLCommerzValidatorResponse"%>
<%@page import="com.ssl.commerz.TransactionResponseValidator"%>
<%@page import="com.ssl.commerz.SSLCommerz"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>
<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);

    }

    System.out.println("Header sessionIdH :: " + sessionIdH);
    System.out.println("Header userNameH :: " + userNameH);
    System.out.println("Header userStoreIdH :: " + memberIdH);

//    String sessionid = request.getParameter("sessionid").trim();
//
//    sessionid = session.getId();
//    String uid = session.getAttribute("username").toString().toUpperCase();    
//    String userStoreId = session.getAttribute("memberId").toString();
//    String sessionidH = request.getParameter("sessionid").trim();
//
//    sessionidH = session.getId();
//    String uidH = session.getAttribute("username").toString().toUpperCase();    
    //   String userStoreIdH = session.getAttribute("memberId").toString();
//System.out.println("uidH :: "+uidH);
    String regMemberTempId = request.getParameter("regTempId") == null ? "" : request.getParameter("regTempId").trim();
    String regMemId = request.getParameter("regMemId") == null ? "" : request.getParameter("regMemId").trim();
    String regStep = request.getParameter("regStep") == null ? "" : request.getParameter("regStep").trim();
    String transId = request.getParameter("transId") == null ? "" : request.getParameter("transId").trim();

    String verify_sign = request.getParameter("verify_sign");

    System.out.println("verify_sign :: " + verify_sign);


    Enumeration paramNames = request.getParameterNames();
    Map<String, String> postData = new HashMap<String, String>();
    while (paramNames.hasMoreElements()) {
        String paramName = (String) paramNames.nextElement();
        //  out.print("<tr><td>" + paramName + "</td>\n");
        //   String paramValue = request.getHeader(paramName);
        String paramValue = request.getParameter(paramName);
        //  out.println("<td> " + paramValue + "</td></tr>\n");

        postData.put(paramName, paramValue);
    }
    //  out.print("</table>");

//    if (regMemberTempId.equals("")) {
//
//        response.sendRedirect(GlobalVariable.baseUrl + "/member/registration.jsp");
//
//    }
    String showTabOption;
    if (regStep.equals("15")) {
        showTabOption = " show";

    } else {
        showTabOption = "";
    }

    Query q1 = null;
    Object[] object = null;

    String memberName = "";
    String fatherName = "";
    String motherName = "";
    String datefBirth = "";
    String placeOfBirth = "";
    String gender = "";
    String gender1 = "";
    String age = "";
    String nationality = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String mobileNumber = "";
    String userEmail = "";
    String centerId = "";
    String pictureName = "";
    String pictureLink = "";
    String mAddressLine1 = "";
    String mAddressLine2 = "";
    String pAddressLine1 = "";
    String pAddressLine2 = "";
    String membershipApplyingFor = "";
    String membershipApplyingFor1 = "";
    String presentIEBmembershipNumber = "";
    String divisionId = "";
    String subDivisionId = "";
    String memberTempPass = "";

    q1 = dbsession.createSQLQuery("select * from member_temp WHERE id= " + regMemId + "");
    for (Iterator itr2 = q1.list().iterator(); itr2.hasNext();) {
        object = (Object[]) itr2.next();
        memberName = object[4].toString();
        fatherName = object[5] == null ? "" : object[5].toString();
        motherName = object[6] == null ? "" : object[6].toString();
        placeOfBirth = object[7].toString();
        datefBirth = object[8].toString();
        age = object[9] == null ? "" : object[9].toString();
        nationality = object[10] == null ? "" : object[10].toString();
        gender1 = object[11].toString();
        if (gender1.equals("M")) {
            gender = "Male";
        } else {
            gender = "Female";
        }
        membershipApplyingFor1 = object[13] == null ? "" : object[13].toString();
        if (membershipApplyingFor1.equals("1")) {
            membershipApplyingFor = " Fellow ";
        } else if (membershipApplyingFor1.equals("2")) {
            membershipApplyingFor = "Member";
        } else {
            membershipApplyingFor = "Associate Member";
        }
        presentIEBmembershipNumber = object[14] == null ? "" : object[14].toString();

        phone1 = object[15] == null ? "" : object[15].toString();
        phone2 = object[16] == null ? "" : object[16].toString();
        mobileNumber = object[17] == null ? "" : object[17].toString();
        userEmail = object[18].toString();
        pictureName = object[25].toString();

        pictureLink = "<img src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + memberName + "\" class=\"img-fluid img_1\">";

        divisionId = object[24].toString() == null ? "" : object[24].toString();
        // subDivisionId = object[28].toString() == null ? "" : object[28].toString();

        memberTempPass = object[3].toString() == null ? "" : object[3].toString();

    }

    Query madrSQL = null;
    Object[] madrObject = null;

    Query padrSQL = null;
    Object[] padrObject = null;

    int mAddressId = 0;
    String mCountryId = "";
    String mDistrictId = "";
    String mThanaId = "";
    String mThanaName = "";
    String mThanaDistrictName = "";
    String mZipOffice = "";
    String mZipCode = "";

    int pAddressId = 0;
    String pCountryId = "";
    String pDistrictId = "";
    String pThanaId = "";
    String pThanaName = "";
    String pThanaDistrictName = "";
    String pZipOffice = "";
    String pZipCode = "";

    String mAddressStr = "";
    String pAddressStr = "";

    Thana thana = null;

    String membershipCusCountry = "Bangladesh";

    Query mailAddrSQL = dbsession.createSQLQuery("select *from address_book WHERE id=(select address_id from member_address_temp where member_id= " + regMemId + " and address_Type='M' ) ");
    for (Iterator itr2 = mailAddrSQL.list().iterator(); itr2.hasNext();) {
        object = (Object[]) itr2.next();
        mAddressLine1 = object[1].toString();
        mAddressLine2 = object[2].toString();
        mThanaId = object[3].toString();

        Query mThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
        for (Iterator itrmThana = mThanaSQL.list().iterator(); itrmThana.hasNext();) {
            thana = (Thana) itrmThana.next();

            mThanaName = thana.getThanaName();
            mThanaDistrictName = thana.getDistrict().getDistrictName();
        }

        mZipCode = object[4].toString();
        mZipOffice = object[8].toString();

        mAddressStr = mAddressLine1 + "," + mAddressLine2 + "<br>" + mThanaName + "," + mThanaDistrictName + "<br>" + mZipOffice + "-" + mZipCode;

    }
    Query permAddrSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address_temp where member_id= " + regMemId + " and address_Type='P' ) ");
    for (Iterator itr3 = permAddrSQL.list().iterator(); itr3.hasNext();) {

        object = (Object[]) itr3.next();
        pAddressLine1 = object[1].toString();
        pAddressLine2 = object[2].toString();
        pThanaId = object[3].toString();

        Query pThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
        for (Iterator itrpThana = pThanaSQL.list().iterator(); itrpThana.hasNext();) {
            thana = (Thana) itrpThana.next();

            pThanaName = thana.getThanaName();
            pThanaDistrictName = thana.getDistrict().getDistrictName();
        }

        pZipCode = object[4].toString();
        pZipOffice = object[8].toString();

        pAddressStr = pAddressLine1 + "," + pAddressLine2 + "<br>" + pThanaName + "," + pThanaDistrictName + "<br>" + pZipOffice + "-" + pZipCode;

    }

    Query feeSQL = null;
    Object[] feeObject = null;

    String memberShipFeeTransId = "";
    String memberShipFeeMemberId = "";
    String memberShipFeePaidDate = "";
    String memberShipFeeAmount = "";
    String memberShipFeeCurrency = "BDT";

    // feeSQL = dbsession.createSQLQuery("select * from member_fee_temp WHERE txn_id= " + transId + " AND status = 0");
    feeSQL = dbsession.createSQLQuery("select * from member_fee_temp WHERE txn_id= " + transId + " AND status = 0");
    //  if (!feeSQL.list().isEmpty()) {
    for (Iterator feeItr = feeSQL.list().iterator(); feeItr.hasNext();) {
        feeObject = (Object[]) feeItr.next();
        memberShipFeeTransId = feeObject[0].toString();
        memberShipFeeMemberId = feeObject[1].toString();
        memberShipFeePaidDate = feeObject[5].toString();
        memberShipFeeAmount = feeObject[7].toString();

        //    System.out.println("memberShipFeeTransId :: " + memberShipFeeTransId);
        //   System.out.println("memberShipFeeMemberId :: " + memberShipFeeMemberId);
        //   System.out.println("memberShipFeePaidDate :: " + memberShipFeePaidDate);
        //  System.out.println("memberShipFeeAmount :: " + memberShipFeeAmount);
    }
    //   }

    // com.ssl.commerz.TransactionResponseValidator.receiveSuccessResponse
//check transactionId from SSL Commerce
    /**
     * If following order validation returns true, then process transaction as
     * success. if this following validation returns false , then query status
     * if failed of canceled. Check request.get("status") for this purpose
     */
    //  TransactionResponseValidator transactionValidationResponse = new TransactionResponseValidator();
    //   boolean transactionValidationResponseBol = transactionValidationResponse.receiveSuccessResponse(postData);
    //   System.out.println("transactionValidationResponseVVV::" + transactionValidationResponseBol);
    SSLCommerz sslcz = new SSLCommerz(GlobalVariable.SSLCommerzStoreId, GlobalVariable.SSLCommerzStorePass, Boolean.parseBoolean(GlobalVariable.SSLCommerzStoreMode));
    boolean transactionValidationResponseBol = sslcz.orderValidate(memberShipFeeTransId, memberShipFeeAmount, memberShipFeeCurrency, postData);
  
    //  boolean transactionValidationResponseBol = false;
    if (transactionValidationResponseBol == true) {

        //update  fee table Status and ref_no, ref_description
        String tranId = request.getParameter("tran_id");
        String valId = request.getParameter("val_id");
        String cardType = request.getParameter("card_type");
        String cardNo = request.getParameter("card_no");
        String bankTransactionID = request.getParameter("bank_tran_id");
        String cardIssuerBank = request.getParameter("card_issuer");
        String cardBrand = request.getParameter("card_brand");
        String cardBrandTranDate = request.getParameter("tran_date");
        String cardBrandTranCurrencyType = request.getParameter("currency_type");
        String cardBrandTranCurrencyAmount = request.getParameter("currency_amount");

        String ref_description = "tranId::" + tranId + " ::valId::" + valId + " ::cardType::" + cardType + " ::cardNo::" + cardNo + " ::bankTransactionID::" + bankTransactionID + " ::cardIssuerBank::" + cardIssuerBank + " ::cardBrand::" + cardBrand + " ::cardBrandTranDate::" + cardBrandTranDate + " ::cardBrandTranCurrencyAmount::" + cardBrandTranCurrencyAmount + " ::cardBrandTranCurrencyType::" + cardBrandTranCurrencyType;
        String ref_no = bankTransactionID;
        String status = "1";

        Query feeUpdSQL = dbsession.createSQLQuery("UPDATE  member_fee_temp SET ref_no='" + ref_no + "',ref_description ='" + ref_description + "',mod_user ='" + regMemId + "',mod_date=now(),status='1' WHERE txn_id='" + transId + "'");

        feeUpdSQL.executeUpdate();
        dbtrx.commit();

//        if (dbtrx.wasCommitted()) {
//
//        } else {
//            //update Error
//        }
        //SSLCommerz sslcz = new SSLCommerz(GlobalVariable.SSLCommerzStoreId, GlobalVariable.SSLCommerzStorePass, Boolean.parseBoolean(GlobalVariable.SSLCommerzStoreMode));
        /**
         * If following order validation returns true, then process transaction
         * as success. if this following validation returns false , then query
         * status if failed of canceled. Check request.get("status") for this
         * purpose
         */
        //boolean transactionValidationResponse = sslcz.orderValidate(memberShipFeeTransId, memberShipFeeAmount, memberShipFeeCurrency, postData);
        //  System.out.println("transactionValidationResponse::" + transactionValidationResponse);
        //  System.out.println("transactionValidationResponseBol::" + transactionValidationResponseBol);
//    Query feeSQL = dbsession.createSQLQuery("select * from member_fee_temp WHERE txn_id= " + transId + " AND status = 0 ");
//    if (!feeSQL.list().isEmpty()) {
//        //update fee talve and ref_no, ref_description
//    }

%>

<%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }
%>


<%@ include file="../header.jsp" %>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Login & Register & About CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about.css">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>


<!-- Start of navigation -->
<div class="about_banner_membership">
    <h2 class="display_41_saz text-white"> Registration Complete </h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Membership</a></li>
                <li class="breadcrumb-item active" aria-current="page">Registration Complete</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of navigation -->
<!-- Starting of Registration form -->
<div class="registration">
    <div class="container bg-white my-3 py-3">
        <!--        <h2 class="text-center mx-auto my-5 font-weight-bold text-dark" style="font-size:3.0rem;"><span style="color: #0044cc;">MEMBERSHIP</span> FROM</h2>-->
        <div class="row globalAlertInfoBoxConParentTT">
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                    <%=msgInfoText%>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <div class="card">


                        <p style="padding: 20px;">Your Member application now complete. When your proposer  are approved your application then our 
                            registration committee verify your information and process next step.</p>


                        <p style="padding-top: 20px;padding-left: 20px;">REG ID: <%=regMemberTempId%></p>
                        <p style="padding-left: 20px; padding-bottom: 20px;">Password: <%=memberTempPass%></p>

                        <p style="padding-left: 20px;">Please check your membership approval status below link</p>
                        <p style="padding-left: 20px;"><strong>Link:</strong> <a href="<%=GlobalVariable.baseUrl%>/member/registrationCheck.jsp" target="_blank"><%=GlobalVariable.baseUrl%>/member/registrationCheck.jsp</a>
                            <br><br>


                    </div>
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h3 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 1: Personal Information</span>
                                </button>
                            </h3>
                        </div>

                        <div id="collapseOne" class="collapse <%=showTabOption%>" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="#">
                                    <div class="form-row form-group m-0">
                                        <div class="col-6">
                                            <label for="inputName">Name:</label>
                                            <%=memberName%>
                                        </div>
                                        <div class="col-6">
                                            <label for="inputName">Father Name:</label>
                                            <%=fatherName%>
                                        </div>
                                    </div>

                                    <div class="form-row form-group m-0">
                                        <div class="col-6">
                                            <label for="inputName">Mother Name:</label>
                                            <%=motherName%>
                                        </div>
                                        <div class="col-6">
                                            <label for="inputName">Gender:</label>
                                            <%=gender%>
                                        </div>
                                    </div>

                                    <div class="form-row form-group m-0">
                                        <div class="col-6">
                                            <label for="inputDOB">Date of Birth:</label>
                                            <%=datefBirth%>
                                        </div>
                                        <div class="col-6">
                                            <label for="inputAge">Age:</label>
                                            <%=age%>
                                        </div>
                                    </div>
                                    <div class="form-row form-group m-0">
                                        <div class="col-6">
                                            <label for="inputPOB">Place of Birth:</label>
                                            <%=placeOfBirth%>
                                        </div>
                                        <div class="col-6">
                                            <label for="inputAge">Nationality:</label>
                                            <%=nationality%>
                                        </div>
                                    </div>

                                    <div class="form-row form-group m-0">
                                        <div class="col-6">
                                            <label for="inputPOB">Office Phone:</label>
                                            <%=phone1%>
                                        </div>
                                        <div class="col-6">
                                            <label for="inputAge">Residence Phone:</label>
                                            <%=phone2%>
                                        </div>
                                    </div>
                                    <div class="form-row form-group m-0">
                                        <div class="col-6">
                                            <label for="inputPOB">Mobile:</label>
                                            <%=mobileNumber%>
                                        </div>
                                        <div class="col-6">
                                            <label for="inputAge">Email:</label>
                                            <%=userEmail%>
                                        </div>
                                    </div>







                                    <div class="form-row form-group m-0">
                                        <div class="col-12">
                                            <label for="inputName">Membership Applying For:</label>
                                            <%=membershipApplyingFor%>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12">
                                            <label for="inputName">Present IEB Membership Number:</span></label>
                                            <%=presentIEBmembershipNumber%>
                                        </div>
                                    </div>




                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingOneVerify">
                            <h3 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOneVerify" aria-expanded="true" aria-controls="collapseOneVerify">
                                    <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 2: Verification</span>
                                </button>
                            </h3>
                        </div>

                        <div id="collapseOneVerify" class="collapse <%=showTabOption%>" aria-labelledby="headingOneVerify" data-parent="#accordion">
                            <div class="card-body">
                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationVerificationSubmitData.jsp">
                                    <p class="m-2">Please check your mobile inbox and add verification code and click verify button </p>

                                    <div class="form-row form-group">
                                        <label class="col-2 text-right" for="inputName">Reg No:</label>
                                        <div class="col-6">
                                            <p><%=regMemberTempId%></p>
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <label class="col-2 text-right" for="inputName">Email:</label>
                                        <div class="col-4">
                                            <p><%=userEmail%></p>
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <label class="col-2 text-right" for="inputName">Verification</label>

                                        <div class="col-3">
                                            <button type="submit" class="btn btn-primary" disabled="">Already Verified</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingFourMyPicture">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseMyPicture" aria-expanded="false" aria-controls="collapseMyPicture">
                                    <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 3: My Picture Upload</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseMyPicture" class="collapse <%=showTabOption%>" aria-labelledby="headingMyPicture" data-parent="#accordion">
                            <div class="card-body">

                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/RegistrationMyPictureUpload" enctype="multipart/form-data">
                                    <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                    <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                    <div class="row">
                                        <div class="col-10 offset-1">
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-2 text-right" for="inputName" style="margin-top: 0.25rem;">My Picture</label>


                                                <div class="col-4">
                                                    <%=pictureLink%>
                                                </div>
                                                <div class="col-4">
                                                    &nbsp;
                                                </div>
                                            </div> 


                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">

                                            <div class="form-row form-group">                                                
                                                <div class="col-2 offset-5">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div> 


                                    <div class="row">
                                        <div class="col-12">
                                            <p class="text-dark" style="font-size: 0.8rem;">Note |: Only .JPGE and .PNG files allow and file not more then 5MB.</p>
                                        </div>
                                    </div>



                                </form>

                            </div>
                        </div>
                    </div> 
                    <div class="card">
                        <div class="card-header" id="headingOneAddressInfo">
                            <h3 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOneAddressInfo" aria-expanded="true" aria-controls="collapseOneAddressInfo">
                                    <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 4: Address Information</span>
                                </button>
                            </h3>
                        </div>

                        <div id="collapseOneAddressInfo" class="collapse <%=showTabOption%>" aria-labelledby="headingOneAddressInfo" data-parent="#accordion">
                            <div class="card-body">
                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationAddressInfoSubmitData.jsp">



                                    <div class="row">
                                        <div class="col-6">
                                            <p class="m-2 text-center">Mailing Address</p>

                                            <div class="form-row form-group">                                                
                                                <div class="col-8 offset-4"><%=mAddressStr%></div>
                                            </div>


                                        </div>
                                        <div class="col-6">
                                            <p class="m-2 text-center">Permanent Address</p>

                                            <div class="form-row form-group">                                                
                                                <div class="col-8 offset-4"><%=pAddressStr%></div>
                                            </div>


                                        </div>
                                    </div>



                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 5: Education Information</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse <%=showTabOption%>" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">

                                <div class="row">
                                    <%
                                        Query educationSQL = null;
                                        int degreeId = 0;
                                        String degreeName = "";
                                        String instituteName = "";
                                        String boardUniversityName = "";
                                        //java.util.Date dob = new Date();
                                        String yearOfPassing = "";
                                        String resultTypeName = "";
                                        String result = "";

                                        MemberEducationInfoTemp mei = null;

                                        educationSQL = dbsession.createQuery("from MemberEducationInfoTemp where  member_id=" + regMemId + " ");

                                        MemberTemp membertemp = null;
                                        String memberSubdivisionName = "";
                                        String memberSubdivisionFullName = "";
                                        int educationfInfoId = 0;
                                    %>


                                    <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                        <thead>
                                            <tr>
                                                <th scope="col">Degree</th>
                                                <th scope="col">Board/University</th>
                                                <th scope="col">Year of Passing</th>
                                                <th scope="col">Score/Class</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                for (Iterator itr = educationSQL.list().iterator(); itr.hasNext();) {
                                                    mei = (MemberEducationInfoTemp) itr.next();

                                                    educationfInfoId = mei.getId();
                                                    degreeId = mei.getDegree().getDegreeId();
                                                    degreeName = mei.getDegree().getDegreeName();

                                                    instituteName = mei.getInstituteName() == null ? "" : mei.getInstituteName();

                                                    boardUniversityName = mei.getUniversity().getUniversityLongName();
                                                    yearOfPassing = mei.getYearOfPassing();
                                                    resultTypeName = mei.getResultType().getResultTypeName();
                                                    result = mei.getResult();

                                                    degreeName = degreeName + memberSubdivisionFullName;
                                            %>
                                            <tr>
                                                <td><%=degreeName%></td>
                                                <td><%=boardUniversityName%></td>
                                                <td><%=yearOfPassing%></td>
                                                <td><%=result%></td>


                                            </tr>
                                            <%
                                                }

                                            %>

                                        </tbody>
                                    </table>

                                </div>

                                <div class="row">
                                    <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                        <thead>
                                            <tr>                                                
                                                <th scope="col">Subject</th>
                                                <th scope="col">University</th>
                                            </tr>
                                            <%                                                Object[] subjectObject = null;
                                                String subjectName = "";
                                                String subjectUniversityName = "";

                                                Query subjectSQL = dbsession.createSQLQuery("SELECT usb.subject_long_name,u.university_long_name FROM "
                                                        + "member_subject_info_temp msb,university_subject usb,university u "
                                                        + "WHERE msb.subject_id = usb.subject_id "
                                                        + "AND usb.university_id = u.university_id "
                                                        + "AND msb.member_id='" + regMemId + "'");
                                                if (!subjectSQL.list().isEmpty()) {
                                                    for (Iterator subjectItr = subjectSQL.list().iterator(); subjectItr.hasNext();) {
                                                        subjectObject = (Object[]) subjectItr.next();
                                                        subjectName = subjectObject[0].toString();
                                                        subjectUniversityName = subjectObject[1].toString();
                                            %>

                                            <tr>
                                                <td><%=subjectName%></td>
                                                <td><%=subjectUniversityName%></td>
                                            </tr>

                                            <%
                                                    }
                                                }
                                            %>

                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 6: Professional Record</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse <%=showTabOption%>" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">

                                <%
                                    Query profTempSQL = null;
                                    //    Query q2 = null;
                                    int profTempId = 0;
                                    String organizationName = "";
                                    String designationName = "";
                                    String startDateProTemp = "";
                                    String endDateProTemp = "";

                                    MemberProfessionalInfoTemp profTemp = null;

                                    profTempSQL = dbsession.createQuery("from MemberProfessionalInfoTemp where  member_id=" + regMemId + " ");


                                %>
                                <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                                    <thead>
                                        <tr>
                                            <th scope="col">Organization</th>
                                            <th scope="col">Designation</th>
                                            <th scope="col">Start Date</th>
                                            <th scope="col">End Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <%                                                for (Iterator profTempItr = profTempSQL.list().iterator(); profTempItr.hasNext();) {
                                                profTemp = (MemberProfessionalInfoTemp) profTempItr.next();

                                                organizationName = profTemp.getCompanyName().toString();
                                                designationName = profTemp.getDesignation().toString();
                                                startDateProTemp = profTemp.getFromDate().toString();
                                                endDateProTemp = profTemp.getTillDate().toString();


                                        %>
                                        <tr>
                                            <td><%=organizationName%></td>
                                            <td><%=designationName%></td>
                                            <td><%=startDateProTemp%></td>
                                            <td><%=endDateProTemp%></td>

                                        </tr>
                                        <%
                                            }

                                        %>

                                    </tbody>
                                </table>



                            </div>
                        </div>
                    </div>
                    <div class="card" id="section8">
                        <div class="card-header" id="headingFourDocuments">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourDocuments" aria-expanded="false" aria-controls="collapseFourDocuments">
                                    <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 7: Related Documents Upload</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFourDocuments" class="collapse  <%=showTabOption%>" aria-labelledby="headingFourDocuments" data-parent="#accordion">
                            <div class="card-body">

                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/RegistrationRelatedDocumentsUpload" enctype="multipart/form-data">
                                    <input type="hidden" name="memberRegId" id="memberRegId" value="1<%//=regMemberTempId%>" >
                                    <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                    <div class="row">
                                        <%
                                            Query relDocSQL = null;
                                            Object[] relDocbject = null;

                                            String documentId = "";
                                            String documentName = "";

                                            relDocSQL = dbsession.createSQLQuery("SELECT * FROM member_document_info_temp WHERE member_id = '" + regMemId + "'");
                                            for (Iterator relDocItr = relDocSQL.list().iterator();
                                                    relDocItr.hasNext();) {
                                                relDocbject = (Object[]) relDocItr.next();

                                                documentId = relDocbject[2].toString();
                                                documentName = relDocbject[3].toString();
                                            }

                                        %>
                                        <div class="col-10 offset-1">
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName" style="">Required Document : </label>

                                                <input name="documentTypeNID" id="documentTypeNID" value="1"  type="hidden">
                                                <div class="col-8">
                                                    <%=documentName%>
                                                </div>

                                            </div> 


                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">

                                            <div class="form-row form-group">                                                
                                                <div class="col-2 offset-5">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div> 


                                    <div class="row">
                                        <div class="col-12">
                                            <p class="text-dark" style="font-size: 0.8rem;">Note |: Only .zip files allow. Please add below documents when create zip file.  
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i. NID or Equivalent document copy
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ii. S. S. C / Equivalent  Certificate and Mark Sheet copy
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii. H. S. C / Equivalent  Certificate and Mark Sheet copy
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii. B. Sc. Engg. Certificate and Mark Sheet copy
                                            </p>
                                        </div>
                                    </div>



                                </form>

                            </div>
                        </div>
                    </div>                 

                    <div class="card">
                        <div class="card-header" id="headingFourRecom">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourRecom" aria-expanded="false" aria-controls="collapseFourRecom">
                                    <i class="fa fa-check-circle fa-lg"></i>   <span class="h4">Section 8: Recommendation Info</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFourRecom" class="collapse  <%=showTabOption%>" aria-labelledby="headingFourRecom" data-parent="#accordion">
                            <div class="card-body">

                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationRecomendationInfoSubmitData.jsp">
                                    <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                    <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >

                                    <%
                                        Query recSQL = null;
                                        Object[] recObject = null;

                                        Query recMemSQL = null;
                                        Object[] recMemObject = null;
                                        String proposerId = "";
                                        int proposerId1 = 0;
                                        int proposerId2 = 0;
                                        int proposerId3 = 0;
                                        String proposerStatus1 = "";
                                        String proposerStatus2 = "";
                                        String proposerStatus3 = "";
                                        String recMemberId = "";
                                        String recMemberName = "";
                                        String recMemberStr1 = "";
                                        String recMemberStr2 = "";
                                        String recMemberStr3 = "";
                                        int r = 1;
                                        recSQL = dbsession.createSQLQuery("SELECT * FROM member_proposer_info_temp WHERE member_id = '" + regMemId + "'");
                                        for (Iterator recItr = recSQL.list().iterator();
                                                recItr.hasNext();) {
                                            recObject = (Object[]) recItr.next();

                                            proposerId = recObject[2].toString();

                                            //   proposerId1 = Integer.parseInt(recObject[2].toString());
                                            // proposerId2 = Integer.parseInt(recObject[3].toString());
                                            //  proposerId3 = Integer.parseInt(recObject[4].toString());
                                            //    proposerId3 = recObject[4] == null ? 0 : Integer.parseInt(recObject[4].toString());
                                            //   recMemSQL = dbsession.createSQLQuery("SELECT * FROM member WHERE id IN (" + proposerId1 + "," + proposerId2 + "," + proposerId3 + ") ORDER BY FIELD(id, " + proposerId1 + "," + proposerId2 + "," + proposerId3 + ")");
                                            recMemSQL = dbsession.createSQLQuery("SELECT * FROM member WHERE id = '" + proposerId + "'");

                                            for (Iterator recMemItr = recMemSQL.list().iterator(); recMemItr.hasNext();) {
                                                recMemObject = (Object[]) recMemItr.next();
                                                recMemberId = recMemObject[1].toString();
                                                recMemberName = recMemObject[2].toString();
                                                if (r == 1) {
                                                    recMemberStr1 = "<p class=\"font-weight-bold\">" + recMemberName + "</p><p class=\"font-weight-bold\">" + recMemberId + "</p>";
                                                }
                                                if (r == 2) {
                                                    recMemberStr2 = "<p class=\"font-weight-bold\">" + recMemberName + "</p><p class=\"font-weight-bold\">" + recMemberId + "</p>";
                                                }
                                                if (r == 3) {
                                                    recMemberStr3 = "<p class=\"font-weight-bold\">" + recMemberName + "</p><p class=\"font-weight-bold\">" + recMemberId + "</p>";
                                                }

                                            }
                                            r++;
                                        }
                                    %>


                                    <div class="row">
                                        <div class="col-4">
                                            <p class="m-2 text-center text-dark font-weight-bold">Proposer</p>

                                            <div class="form-row">
                                                <div class="col-12 text-center">
                                                    <%=recMemberStr1%>
                                                </div>                                                    
                                            </div>    

                                        </div>
                                        <div class="col-4">
                                            <p class="m-2 text-center text-dark font-weight-bold">Seconder |</p>
                                            <div class="form-row">
                                                <div class="col-12 text-center">
                                                    <%=recMemberStr2%>
                                                </div>                                                    
                                            </div> 


                                        </div>
                                        <div class="col-4">
                                            <p class="m-2 text-center text-dark font-weight-bold">Seconder ||</p>
                                            <div class="form-row">
                                                <div class="col-12 text-center">
                                                    <%=recMemberStr3%>
                                                </div>                                                    
                                            </div> 


                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">

                                            <div class="form-row form-group">                                                
                                                <div class="col-2 offset-5">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div>     
                                </form>
                                <div class="row">
                                    <div class="col-12">
                                        <p class="text-dark" style="font-size: 0.8rem;">Note |: Proposer and Seconder must be at least 
                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i.Two Fellows and one Member for Fellowship
                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ii.One Fellow and two Members for Fellowship
                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii.Two Members for Associate Membership
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Ending of Registration form -->



<%@ include file="../footer.jsp" %>


<%
        dbsession.flush();
        dbsession.close();

    } else {

        //error
        String strMsgPay = "Error!!! Your tanasaction is not valid.Please check and try again.";
        //  dbtrx.rollback();
        response.sendRedirect(GlobalVariable.baseUrl + "/member/registrationFeePaymentOption.jsp?regTempId=" + regMemberTempId + "&regMemId=" + regMemId + "&regStep=11&strMsg=" + strMsgPay + "#section9");
    }


%>