<%@ include file="../header.jsp" %>


<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="about_banner_membership">
    <h2 class="display_41_saz text-white">membership criteria</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Membership</a></li>
                <li class="breadcrumb-item active" aria-current="page">Membership Criteria</li>
            </ol>
        </nav>
    </div>
</div>


<section class="news-single-content pt-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="news_cont_saz">


                    


                    <p><u><b>Please enclose attested copies with membership form</b></u></p>

                    <ul class="required_docs">
                        <li>Recent passport size photo (2 copies)</li>
                        <li>Certificates: SSC, HSC/Diploma, B.Sc. Engineering</li>
                        <li>Transcripts: HSC/Diploma, B.Sc. Engineering</li>
                        <li>Certificates of the others professional bodies (if any)</li>
                        <li>Photocopy of NID/Smart card</li>
                        <li>Recognized training record of last two (2) years</li>
                        <li>Details records of 1500 words (member) and 2000 words (fellow) on experience demonstrating competencies & abilities.</li>
                    </ul>


                    <p><u>N. B. : </u>Applicant must be shown  "Original Cetificates & Transcripts" when submit his/her membership form at IEB HQ, centre of sub-centre.</p>

                    



                    <p><strong>&emsp;Membership of The Institution shall consist of two classes:</strong></p>
                    <ul class="required_docs_criteria">
                        <li>Corporate and</li>
                        <li>Non-Corporate</li>
                    </ul><br>


                    <p><strong>&emsp;Only Corporate members shall have the right to vote, to requisition meetings and to be elected to the Council. Corporate members shall be divided into</strong></p>
                    <ul class="required_docs_criteria">
                        <li>Fellows and</li>
                        <li>Members</li>
                    </ul>


                    <p><strong>&emsp;Non-Corporate members shall be divided into five categories, viz.</strong></p>
                    <ul class="required_docs_criteria">
                        <li>Honorary Member</li>
                        <li>Associate Members</li>
                        <li>Students</li>
                        <li>Affiliates and</li>
                        <li>Subscribers</li>
                    </ul>


                    <p><strong>&emsp;Abbreviated Titles.</strong></p>
                    <p class="short_desc_criteria_saz">Fellows,. Members & Associate members shall be entitled to use the title "Engr." before their names Members shall be entitled to the exclusive use after their names the following abbreviated      designation   to   indicate   the   class   of Membership :</p>
                    <ul class="required_docs_criteria">
                        <li>Honorary Member</li>
                        <li>Associate Members</li>
                        <li>Students</li>
                        <li>Affiliates and</li>
                        <li>Subscribers</li>
                    </ul>
                    <table class="abbreviated_table">
                        <thead>
                            <tr>
                                <th>Honorary Members </th>
                                <th>Hon. MIEB </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Fellows</td>
                                <td>FIEB</td>
                            </tr>
                            <tr>
                                <td>Members</td>
                                <td>MIEB</td>
                            </tr>
                            <tr>
                                <td>Associate Members  </td>
                                <td>AMIEB</td>
                            </tr>
                        </tbody>
                    </table>
                    <p class="short_desc_criteria_saz">
                        No person who has ceased to be a member of The Institution is entitled to make use of the title or any designation. Any person, who is not a Member or ceases to be a Member of The  Institution, using any of the above designation may be liable to legal action.
                    </p>


                    <p><strong>&emsp;Honorary Members </strong></p>
                    <ul class="required_docs_criteria">
                        <li>Persons of acknowledgement eminence in engineering or science related thereto, but who are not engaged in the practice of that profession, the conferment of this distinction may, in exceptional cases, be for life time.</li>
                        <li>Persons of distinction whom The Institution desires to honour for services rendered thereto, or whose association herewith is deemed to be of  benefit to The Institution. Head of the State or Government of Bangladesh or any other country may be elected in the Annual General Meeting on the recommendation of the Council as Honorary Members during the tenure of their respective offices.</li>
                        <li>Persons with high educational qualification and occupying a prominent position in any profession or society who shall either have made some noteworthy contribution to the science, engineering, art, literature, social sciences, environment etc. or shall have materially advanced the practice of engineering, science and/or socio-economic & cultural advancement of the society may be elected in the Annual General Meeting on the recommendation of the Council as Honorary Members.</li>
                    </ul>


                    <p><strong>&emsp;Honorary Fellows </strong></p>
                    <p class="short_desc_criteria_saz">Every candidate for admission into The Institution as a Fellow or transfer from  a Member to  a Fellow  shall  produce evidence satisfactory to the Council that he fulfils the following conditions : </p>
                    <ul class="required_docs_criteria">
                        <li>Age : He shall not be less than 37 (thirty seven) years of age. </li>
                        <li>Occupation :   He shall be engaged in the profession if engineering after having held before his application for election or transfer, a position of high responsibility.</li>
                        <li>Qualification : He shall have one of the   following qualifications</li>
                    </ul>
                    
                    <ul class="required_docs_criteria">
                        <li>He shall be a Member of The Institution for at least 5 (five) years. </li>
                        <li>He shall have at least 12 (twelve) years experience in a position if responsibility   in  teaching,   research  &   development, design, planning, engineering management and/or the execution if important engineering works substantiated through a detailed report of 2000 words on experience demonstrating competencies and applicants abilities and recognized training record for last two years, which will be assessed by Membership Committee Assessment Team.</li>
                    </ul>
                    OR 
                    <ul class="required_docs_criteria">
                        <li>He shall have fulfilled conditions necessary for Membership. </li>
                        <li>He shall have had suitable education and training in engineering and shall have at least fifteen years experience in a position of responsibility in teaching, research & development, design, planning, engineering management and/or execution of important engineering works and substantiated through a detailed report.</li>
                    </ul>
                    



                    <p><strong>&emsp;Members : </strong></p>
                    <p class="short_desc_criteria_saz">Every candidate for election to the class if member or transfer from an  Associate  member   into  this   class   shall  produce  evidence satisfactory to the Council that he fulfils the following conditions :</p>
                    <ul class="required_docs_criteria">
                        <li>Age : He shall not be less than 27 (twenty seven) years of age, on the date of application.</li>
                        <li>Occupation : At the time if his application for election he shall   be   actually   engaged   in   teaching,   research   & development, design, planning, engineering management and/or the execution of engineering works.</li>
                        <li>Qualification : Qualifications recognized by the Institution and included in the rules and syllabi shall be accepted for various classes of membership. Other qualification may be accepted in consultation with the Equivalence Committee</li>
                    </ul>
                    
                    <ul class="required_docs_criteria">
                        <li>He shall be an Associate Member or have passed Section 'B' Examination of The Institution, followed by at least two years experience under a Corporate member if which he shall have at least one year's experience in engineering activities.  </li>
                    </ul>
                    OR 
                    <ul class="required_docs_criteria">
                        <li>He shall have completed a course if studies in engineering leading to a degree recognized by the Council or as exempting him from Section "A" & "B" Examination if the Institution and shall have at least 3 (three) years practical experience after graduation under the guidance of a Corporate Member or a teaching experience if 3 (three) years. Post Graduate Studies leading to a Master/ and Doctor if Engineering degrees from a recognized educational institution  shall  be  counted  as  one  and  three years  practical experience respectively.</li>
                        <li>He shall have a Master/ or Doctor if Engineering Degree from a recognized educational institution after having his Bachelor Degree in any allied subject and shall have four and two years if practical experience respectively in the field if engineering activities.</li>
                        <li>Recognized training record for the last (02) two years to be submitted with report of minimum 1500 (fifteen hundred) words demonstrating competencies and applicants abilities, which will be assessed by Membership Committee Assessment Team.</li>
                    </ul>
                    



                    <p><strong>&emsp; Associate Members :  </strong></p>
                    <p class="short_desc_criteria_saz">Every candidate for attachment to The Institution as an Associate member or for transfer from Student to this class shall satisfy the following conditions :     </p>
                    
                    <ul class="required_docs_criteria">
                        <li>Qualification: He shall have completed a course if studies in engineering leading to a degree and shall have received such degree recognized by the Council and other qualification may be accepted in consultation with the Equivalence Committee. </li>
                    </ul>
                    OR
                    <ul class="required_docs_criteria">
                        <li>He shall have passed the Section "B" Examination of The Institution of Engineers, Bangladesh which is included in the Rules and Syllabi for Associate Membership and Membership Examination or of any other Institution or Society, the Examinations of which are recognized by the Council.
                            He shall have to submit the following information with the application form: Date of Birth, Nationality, Fathers name, Mothers name, Merital status and spouse name, Blood group, Copies of Educational Certificates, Transcripts, Studentship Roll/ID number, Registration number. The Membership Committee will assess and verify the documents and signature with the NID.
                        </li>
                    </ul>
                    



                    <p><strong>&emsp; Students : </strong></p>
                    <p class="short_desc_criteria_saz">Every candidate for attachment to The Institution as a Student with the object of becoming engaged in the design, construction and other works as an engineer shall not be less than 17 (seventeen) years of age and shall have one following qualifications at the time of application.</p>
                    <ul class="required_docs_criteria">
                        <li>He shall be attending a regular course of studies leading to a degree in engineering.     </li>
                        <li>He shall have passed Higher Secondary Certificate Examination in at least 2nd Division having secured a minimum of 45%  marks in each of the following subjects: Physics, Chemistry and Mathematics or shall have passed an equivalent examination with equivalent grades.  </li>
                        <li>each of the following subjects: Physics, Chemistry and Mathematics or shall have passed an equivalent examination with equivalent grades.   
                            A student admitted under this clause will be recognized as an approved candidate for the purpose of article 60 (Examination) of the Constitution, if he has completed satisfactorily an article of apprenticeship of two years in an  engineering  organization/firm recognized  by The Institution.</li>
                        <li>He is a student in a technical institution recognized by The Institution and must be in his final year. </li>
                    </ul>



                    <p><strong>&emsp; Affiliates: </strong></p>
                    <p class="short_desc_criteria_saz">
                        Persons who are not engineers by education and profession and do not come under any class of membership rules specified above, but through their connection with engineering are qualified to work with   engineers   in   the   advancement   of  engineering   science, knowledge or practice may be permitted to be attached with The Institution as Affiliates. No person who in the opinion of the council is eligible to be a Fellow, Member, Associate Member, or a Student shall be admitted as an Affiliate.     
                    </p>



                    <p><strong>&emsp; Subscribers :      </strong></p>
                    <p class="short_desc_criteria_saz">
                        The Council may at its discretion attach to The Institution as a Subscriber any State, Department of Government, Public Body, Registered Company, Firm or individual, not eligible to be a Fellow, Member, Associate Member, Affiliate or a Student who may desire to be so attached. Each Subscriber shall be entitled to attend or (if the Subscriber be other than an individual) to depute representatives to attend the Convention.      

                    </p>




                    <p><strong>&emsp;Admission of Member : </strong></p>
                    <p class="short_desc_criteria_saz">
                        Every candidate enrollment or for transfer from one class to another including a candidate who is already a member of another class shall apply in the prescribed application from to The Institution. Candidates fulfilling the requirements as prescribed above shall be admitted to The Institution as per Bye-Laws and on payment of the prescribed fees.  

                    </p>




                    <p><strong>&emsp;Cessation of Membership :  </strong></p>
                    <p class="short_desc_criteria_saz">
                        Any member who has not paid his subscription for consecutive two years shall seize to be member of The Institution. He shall have to pay full entrance   fee   for  re-enrollment  including   all   arrear subscription, provided that he may be exempted from payment of the arrears by the Council.

                    </p>




                    <p><strong>&emsp; 	Resignation :</strong></p>
                    <p class="short_desc_criteria_saz">
                        Any member may resign his membership by written communication to the Honorary General Secretary who shall put up the same to the authority which has the power to sanction the admission or enrollment for consideration. The resignation shall take effect from the date of receiving the application or such other date at the discretion of the authority concerned.

                    </p>




                    <p><strong>&emsp;Professional Conduct and Code of Ethics : </strong></p>
                    <p class="short_desc_criteria_saz">
                        Fellows, Members, Associate Members, Students and Affiliates are required to order their conduct so as to uphold the reputation of The Institution and the dignity of the profession of Engineers and shall observe and be bound by the Code of Ethics as laid down in the Bye-Laws from time to time. The Professional Conduct and Code of Ethics shall be supervised by the Ethics Committee formed by the Council.

                    </p>






                    <p><strong>&emsp;Disciplinary Action and Appeal: </strong></p>
                    <p class="short_desc_criteria_saz">
                        Any alleged breach of the Code of Ethics by any Member will be brought before the Ethics Committee of The Institution/. Ethics Committee will inquire into the allegations as per provisions of the Bye-Laws allowing the concerned Member to defend and will submit his findings and recommendations to the/ Council. The Honorary General Secretary will then place the same in the next meeting of the Council.

                    </p>
                    <ul class="required_docs_criteria">
                        <li>The Council after due consideration of the findings of the Ethics Committee shall decide to take appropriate action as per Bye-Laws. In case of suspension, at least 2/3'd of the members present in the Council Meeting shall vote in favor of such an action. Other penal action may be taken by majority votes. However, in case of permanent suspension of membership, Council's recommendation shall have to be confirmed in the Next Annual General Meeting pending which the suspension will be Temporary. <br>Council decision shall be communicated to the concerned Member, under certificate of posting, within 10 days by the Honorary General Secretary. Any Member penalized as above shall have the right to appeal the Council within 90 days from the dated of communication, for review.</li>
                        <li>Any Fellow, Member, Associate Member, Student or Affiliate convicted by a competent tribunal or authority of felony, embezzlement, larceny, misdemeanor or other offence will stand expelled from The Institution, with the concurrence of the Ethics Committee, from the date of judgment by the competent Tribunal/authority. </li>
                    </ul>




                    <p><strong>&emsp;Rights and Privilege :  </strong></p>
                    <p class="short_desc_criteria_saz">
                        Members of/ The Institution shall, subject to this Constitution and the Bye-Laws made there under, enjoy all the rights and privileges as may be conferred by The Institution from time to time.

                    </p><br>

                    <p style="font-style: italic;">The rights and privileges of every member/ shall be personal to himself and shall not be transferable by his own act or operation of law.</p>
                    



                    <h2 class="who_join_saz">Who Can Join</h2>
                    <p class="short_desc_criteria_saz">Every candidate for election to the class of Member or transfer from an Associate Member into this class shall satisfy the following qualifications:</p>
                    <ul class="required_docs_criteria">
                        <li><span style="color: #990000; font-weight: bold;">Age :</span> He shall not be less than 25 (twentyfive) years of age. In case of Associate Member of 2 (two) years standing, the age limit may be relaxed to 24 (twentyfour) years.</li>
                        <li><span style="color: #990000; font-weight: bold;">Occupation:</span> At the time of his application for election he shall be actuallly engaged in the design and/or construction of engineering work or engaged as a teacher of engineering in a technical university or college or istitute recognized by the Council or engaged in engineering research </li>
                        <li><span style="color: #990000; font-weight: bold;">Qualification:</span> He shall have one of the following qualifications:</li>
                        <li>He shall have passed Section ?B? Examination of The Institution follwed by at least two years experience under a Corporate Member of which he shall have at least two years experience in engineering activities.</li>
                        <li>He shall have completed a course of studies in engineering leading to a degree recognized by the Council as exempting him from Section ?A? & ?B? </li>
                    </ul>
                </div>
            </div>
        </div><br><br>
    </div>
</section>


<%@ include file="../footer.jsp" %>
