<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    Query q2 = null;
    int degreeId = 0;
    String degreeName = "";
    String instituteName = "";
    String boardUniversityName = "";
    java.util.Date dob = new Date();
    String yearOfPassing = "";
    String resultTypeName = "";
    String result = "";

    MemberEducationInfo mei = null;
    
    String educationfInfoId = request.getParameter("educationfInfoId").trim();

    q1 = dbsession.createQuery("from MemberEducationInfo where  member_id=" + memberIdH + " and id='" + educationfInfoId + "'");

    Member member = null;
    String memberSubdivisionName = "";
    String memberSubdivisionFullName = ""; 
int universityId1 = 0;
int resultTypeId1 = 0;

    

%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css"/>

<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">


<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Education Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%
                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->



                   

                            <%
                                for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                                    mei = (MemberEducationInfo) itr.next();
 
                                    degreeId = mei.getDegree().getDegreeId();
                                    degreeName = mei.getDegree().getDegreeName();

                                    instituteName = mei.getInstituteName() == null ? "" : mei.getInstituteName();

                                    boardUniversityName = mei.getUniversity().getUniversityLongName();
                                    universityId1 = mei.getUniversity().getUniversityId();
                                    yearOfPassing = mei.getYearOfPassing();
                                    resultTypeName = mei.getResultType().getResultTypeName();
                                    resultTypeId1 = mei.getResultType().getResultTypeId();
                                    result = mei.getResult();

                                    if (degreeId == 3 || degreeId == 4) {

                                        q2 = dbsession.createQuery("from Member as member WHERE id=" + memberIdH + " ");

                                        for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
                                            member = (Member) itr2.next();

                                            memberSubdivisionName = member.getSubDivision().getSubDivisionName();
                                            memberSubdivisionFullName = member.getSubDivision().getFullName();
                                        }
                                    }else{
                                        memberSubdivisionFullName = "";
                                    }

                                    degreeName = degreeName + memberSubdivisionFullName;
                         
                                }


                            %>

                     
                    <!-- ============ End Education information Table ================== -->


                    <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberEducationInfoSubmitData.jsp?sessionid=<%=session.getId()%>&act=edit" onSubmit="return fromDataSubmitValidation()">

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label mx-3">Degree :</label>
                            <div class="col-sm-6">
                                <select name="memberDegreeTypeId" id="memberDegreeTypeId" class="form-control" required="">
                                    <option value="<%=degreeId%>"><%=degreeName%></option>
                                    <%                                        Object[] object5 = null;
                                        String degreeIdX = "";
                                        String degreeNameX = "";
                                        Query q5 = dbsession.createSQLQuery("select * FROM degree ORDER BY degree_id DESC");
                                        for (Iterator itr5 = q5.list().iterator(); itr5.hasNext();) {
                                            object5 = (Object[]) itr5.next();
                                            degreeIdX = object5[0].toString();
                                            degreeNameX = object5[1].toString();
                                    %>
                                    <option value="<%=degreeIdX%>"><%=degreeNameX%></option>

                                    <%
                                        }

                                    %>

                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label mx-3">*Institution :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="institutionName" name="institutionName" placeholder="Institution"  value="<%=instituteName%>" required="">
                                <input type="hidden"  id="educationfInfoId" name="educationfInfoId"   value="<%=educationfInfoId%>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label mx-3">*Board/University :</label>
                            <div class="col-sm-6">
                                <select name="memberBoardUniversity" id="memberBoardUniversity" class="form-control chosen">
                                    <option value="<%=universityId1%>" ><%=boardUniversityName%></option>
                                    <%                                        Object[] object6 = null;
                                        String universityId = "";
                                        String universityShortName = "";
                                        String universityLongName = "";
                                        String universityName = "";
                                        String degreeNameX1 = "";
                                        Query q6 = dbsession.createSQLQuery("select * FROM university ORDER BY university_short_name ASC");
                                        for (Iterator itr6 = q6.list().iterator(); itr6.hasNext();) {
                                            object6 = (Object[]) itr6.next();
                                            universityId = object6[0].toString();
                                            universityShortName = object6[1].toString();
                                            universityLongName = object6[2].toString() == null ? "" : object6[2].toString();

                                            if (universityLongName.equals("")) {
                                                universityName = universityShortName;
                                            } else {
                                                universityName = universityLongName + "(" + universityShortName + ")";
                                            }

                                    %>
                                    <option value="<%=universityId%>"><%=universityName%></option>

                                    <%
                                        }

                                    %>

                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label mx-3">*Year of Passing :</label>
                            <div class="col-sm-6">
                                <select id="memberPassingYear" name="memberPassingYear"   class="custom-select" required="">
                                    <option value="<%=yearOfPassing%>" selected><%=yearOfPassing%></option>
                                    <option value="1960">1960</option>
                                    <option value="1961">1961</option>
                                    <option value="1962">1962</option>
                                    <option value="1963">1963</option>
                                    <option value="1964">1964</option>
                                    <option value="1965">1965</option>
                                    <option value="1966">1966</option>
                                    <option value="1967">1967</option>
                                    <option value="1968">1968</option>
                                    <option value="1969">1969</option>
                                    <option value="1970">1970</option>
                                    <option value="1971">1971</option>
                                    <option value="1972">1972</option>
                                    <option value="1973">1973</option>
                                    <option value="1974">1974</option>
                                    <option value="1975">1975</option>
                                    <option value="1976">1976</option>
                                    <option value="1977">1977</option>
                                    <option value="1978">1978</option>
                                    <option value="1979">1979</option>
                                    <option value="1980">1980</option>
                                    <option value="1981">1981</option>
                                    <option value="1982">1982</option>
                                    <option value="1983">1983</option>
                                    <option value="1984">1984</option>
                                    <option value="1985">1985</option>
                                    <option value="1986">1986</option>
                                    <option value="1987">1987</option>
                                    <option value="1988">1988</option>
                                    <option value="1989">1989</option>
                                    <option value="1990">1990</option>
                                    <option value="1991">1991</option>
                                    <option value="1992">1992</option>
                                    <option value="1993">1993</option>
                                    <option value="1994">1994</option>
                                    <option value="1995">1995</option>
                                    <option value="1996">1996</option>
                                    <option value="1997">1997</option>
                                    <option value="1998">1998</option>
                                    <option value="1999">1999</option>
                                    <option value="2000">2000</option>
                                    <option value="2001">2001</option>
                                    <option value="2002">2002</option>
                                    <option value="2003">2003</option>
                                    <option value="2004">2004</option>
                                    <option value="2005">2005</option>
                                    <option value="2006">2006</option>
                                    <option value="2007">2007</option>
                                    <option value="2008">2008</option>
                                    <option value="2009">2009</option>
                                    <option value="2010">2010</option>
                                    <option value="2011">2011</option>
                                    <option value="2012">2012</option>
                                    <option value="2013">2013</option>
                                    <option value="2014">2014</option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>									
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label mx-3">Result Type :</label>
                            <div class="col-sm-6">          
                                <select name="memberResultTypeId" id="memberResultTypeId" class="form-control" required="">
                                    <option value="<%=resultTypeId1%>"><%=resultTypeName%> </option>
                                    <%                                        Object[] object7 = null;
                                        String resultTypeIdX = "";
                                        String resultTypeNameX = "";
                                        Query q7 = dbsession.createSQLQuery("select * FROM result_type ORDER BY result_type_id ASC");
                                        for (Iterator itr7 = q7.list().iterator(); itr7.hasNext();) {
                                            object7 = (Object[]) itr7.next();
                                            resultTypeIdX = object7[0].toString();
                                            resultTypeNameX = object7[1].toString();
                                    %>
                                    <option value="<%=resultTypeIdX%>"><%=resultTypeNameX%></option>

                                    <%
                                        }

                                        dbsession.flush();
                                        dbsession.close();
                                    %>

                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label mx-3">*Score/Class :</label>
                            <div class="col-sm-6">
                                <input name="memberScoreClass" id="memberScoreClass"  type="text" class="form-control" placeholder="Score/Class"  required value="<%=result%>">
                            </div>
                        </div>


                        <div class="form-group row text-right ml-5">
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary mr-3">Submit</button>
                                <button type="button" class="btn btn-primary">Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- ================ End billing information form ================= -->

                </div>
            </div>
        </div>
    </div>
</div>

<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.jquery.js"></script>
<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.js"></script>
<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/init.js"></script>



<%@ include file="../footer.jsp" %>
