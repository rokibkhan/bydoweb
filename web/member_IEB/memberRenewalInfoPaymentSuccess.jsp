<%@page import="com.ssl.commerz.parametermappings.SSLCommerzValidatorResponse"%>
<%@page import="com.ssl.commerz.TransactionResponseValidator"%>
<%@page import="com.ssl.commerz.SSLCommerz"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>
<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String strMsg = "";

    MemberFee mFee = null;

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    MemberFee memberFee = null;

    String regMemberTempId = request.getParameter("regTempId") == null ? "" : request.getParameter("regTempId").trim();
    String regMemId = request.getParameter("regMemId") == null ? "" : request.getParameter("regMemId").trim();
    String transId = request.getParameter("transId") == null ? "" : request.getParameter("transId").trim();
    String txnFeeYearId = request.getParameter("txnFeeYearId") == null ? "" : request.getParameter("txnFeeYearId").trim();

    String verify_sign = request.getParameter("verify_sign");

    System.out.println("WB :: memberRenewalInfoPaymentSuccess :: regMemberTempId :: " + regMemberTempId);
    System.out.println("WB :: memberRenewalInfoPaymentSuccess :: regMemId :: " + regMemId);
    System.out.println("WB :: memberRenewalInfoPaymentSuccess :: transId :: " + transId);
    System.out.println("WB :: txnFeeYearId :: " + txnFeeYearId);
    System.out.println("WB :: verify_sign :: " + verify_sign);

    Enumeration paramNames = request.getParameterNames();
    Map<String, String> postData = new HashMap<String, String>();
    while (paramNames.hasMoreElements()) {
        String paramName = (String) paramNames.nextElement();
        //  out.print("<tr><td>" + paramName + "</td>\n");
        //   String paramValue = request.getHeader(paramName);
        String paramValue = request.getParameter(paramName);
        //  out.println("<td> " + paramValue + "</td></tr>\n");

        postData.put(paramName, paramValue);
    }

    String memberShipFeeAmount = "";
    String memberShipFeeCurrency = "BDT";

    String txnFeeAmountSQL = "select amount from member_fee  WHERE  txn_id= '" + transId + "' AND member_id='" + regMemId + "' AND status ='2'";

    memberShipFeeAmount = dbsession.createSQLQuery(txnFeeAmountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(txnFeeAmountSQL).uniqueResult().toString();

    String transIdSQL = transIdSQL = " txn_id='" + transId + "'";

//check transactionId from SSL Commerce
    SSLCommerz sslcz = new SSLCommerz(GlobalVariable.SSLCommerzStoreId, GlobalVariable.SSLCommerzStorePass, Boolean.parseBoolean(GlobalVariable.SSLCommerzStoreMode));

    boolean transactionValidationResponseBol = sslcz.orderValidate(transId, memberShipFeeAmount, memberShipFeeCurrency, postData);

    //  boolean transactionValidationResponseBol = false;
    if (transactionValidationResponseBol == true) {

        //update  fee table Status and ref_no, ref_description
        String tranId = request.getParameter("tran_id");
        String valId = request.getParameter("val_id");
        String cardType = request.getParameter("card_type");
        String cardNo = request.getParameter("card_no");
        String bankTransactionID = request.getParameter("bank_tran_id");
        String cardIssuerBank = request.getParameter("card_issuer");
        String cardBrand = request.getParameter("card_brand");
        String cardBrandTranDate = request.getParameter("tran_date");
        String cardBrandTranCurrencyType = request.getParameter("currency_type");
        String cardBrandTranCurrencyAmount = request.getParameter("currency_amount");

        String ref_description = "tranId::" + tranId + " ::valId::" + valId + " ::cardType::" + cardType + " ::cardNo::" + cardNo + " ::bankTransactionID::" + bankTransactionID + " ::cardIssuerBank::" + cardIssuerBank + " ::cardBrand::" + cardBrand + " ::cardBrandTranDate::" + cardBrandTranDate + " ::cardBrandTranCurrencyAmount::" + cardBrandTranCurrencyAmount + " ::cardBrandTranCurrencyType::" + cardBrandTranCurrencyType;
        String ref_no = bankTransactionID;
        String status = "1";
        String pay_option = "1";

        // Query feeUpdSQL = dbsession.createSQLQuery("UPDATE  member_fee SET ref_no='" + ref_no + "',ref_description ='" + ref_description + "',pay_option = '" + pay_option + "',mod_user ='" + regMemId + "',mod_date=now(),status='1' WHERE txn_id='" + transId + "'");
        Query feeUpdSQL = dbsession.createSQLQuery("UPDATE  member_fee SET ref_no='" + ref_no + "',ref_description ='" + ref_description + "',pay_option = '" + pay_option + "',mod_user ='" + regMemId + "',mod_date=now(),status='1' WHERE " + transIdSQL + "");

        feeUpdSQL.executeUpdate();

        //Life Member
        if (txnFeeYearId.equals("20")) {

            //update member life status
            Query memberLifeUpdSQL = dbsession.createSQLQuery("UPDATE  member_life_info SET life_status='1',update_time=now() WHERE member_id='" + regMemId + "'");
            memberLifeUpdSQL.executeUpdate();
        }

        dbtrx.commit();

        dbsession.flush();
        dbsession.close();
        //error
        strMsg = "Success!!! Your tanasaction is complete.";
        //  dbtrx.rollback();
        // response.sendRedirect(GlobalVariable.baseUrl + "/member/registrationFeePaymentOption.jsp?regTempId=" + regMemberTempId + "&regMemId=" + regMemId + "&regStep=11&strMsg=" + strMsgPay + "");
        response.sendRedirect("memberPaymentHistoryInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

    } else {

        //error
        strMsg = "Error!!! Your tanasaction is not valid.Please check and try again.";
        //  dbtrx.rollback();
        // response.sendRedirect(GlobalVariable.baseUrl + "/member/registrationFeePaymentOption.jsp?regTempId=" + regMemberTempId + "&regMemId=" + regMemId + "&regStep=11&strMsg=" + strMsgPay + "");
        response.sendRedirect("memberPaymentHistoryInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
    }

%>
