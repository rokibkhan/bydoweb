<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.util.Date" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>



<%
    Session dbsession = null;
    String qryparam = "";;

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("forgotPasswordSubmitData_jsp.class");

    getRegistryID getId = new getRegistryID();

    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    Query profSQL = null;

    Query profSQL_01 = null;
    Query profSQL_02 = null;
    Query profSQL_03 = null;

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date dateX = new Date();
    String adddate = dateFormatX.format(dateX);

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date();
    String entryDate = dateFormat.format(date);

    SendSMS sendsms = new SendSMS();
    SendEmail sendemail = new SendEmail();

    RandomString randNumber = new RandomString();

    Random random = new Random();
    int rand1 = Math.abs(random.nextInt());
    int rand2 = Math.abs(random.nextInt());

    long uniqueNumber = System.currentTimeMillis();

    String memberUniqueNumber = uniqueNumber + "-" + rand1 + "-" + rand2;

    //String pinCode = randNumber.randomString(4,2);
    String member_pass_code_phone = randNumber.randomString(4, 2);
    String member_pass_code_email = randNumber.randomString(6, 2);

//        logger.info("WEB :: forgotPasswordSubmitData ::memberIdH " + memberIdH);
    String memberID = request.getParameter("memberID") == null ? "" : request.getParameter("memberID").trim();
    String memberEmail = request.getParameter("memberEmail") == null ? "" : request.getParameter("memberEmail").trim();
    String memberMobile = request.getParameter("memberMobile") == null ? "" : request.getParameter("memberMobile").trim();

    // check empty or null
    if ((memberID != null && !memberID.isEmpty()) && (memberEmail != null && !memberEmail.isEmpty()) && (memberMobile != null && !memberMobile.isEmpty())) {

        //check Member Id
        Query varifySQL = dbsession.createSQLQuery("select * from member WHERE member_id ='" + memberID + "' AND status ='1'");
        if (!varifySQL.list().isEmpty()) {
            //success

            //check Phone number and member ID
            Query varifyPhoneSQL = dbsession.createSQLQuery("select id from member WHERE member_id ='" + memberID + "' AND mobile = '" + memberMobile + "' AND status ='1'");
            int memberId = 0;
            if (!varifyPhoneSQL.list().isEmpty()) {
                //send SMS

                memberId = Integer.parseInt(varifyPhoneSQL.uniqueResult().toString());

                logger.info("WEB :: forgotPasswordSubmitData ::  memberId:: " + memberId);

                String memberName = "";

                //  String smsMsg = "Your verification code is" + member_temp_pass_phone + " Email code is " + member_temp_pass_email;
                String smsMsg = "Your IEB temporary password is " + member_pass_code_phone + ".Please change password after successfully login.";
                //  String receipentMobileNo = "+8801755656354";
                String receipentMobileNo = memberMobile;
                logger.info("WEB :: forgotPasswordSubmitData ::SMS : " + smsMsg);
                logger.info("WEB :: forgotPasswordSubmitData ::receipentMobileNo : " + receipentMobileNo);
                //    System.out.println(SendSMS(sms, receipentMobileNo));

                //  String sendsms1 = sendsms.sendSMS(smsMsg, receipentMobileNo);
                String sendsms1 = sendsms.sendSMSOpt(smsMsg, receipentMobileNo);

                logger.info("WEB :: forgotPasswordSubmitData ::sendsms1::" + sendsms1);

                //update password in system
                String newPassEnc = new Encryption().getEncrypt(member_pass_code_phone);

                Query q4 = dbsession.createSQLQuery("UPDATE  member_credential SET member_key ='" + newPassEnc + "',update_time=now(),status= '0' WHERE member_id='" + memberId + "'");

                q4.executeUpdate();
                dbtrx.commit();

                String messageSubject = "IEB Email Verification";

                String messageText = "<div style=\"width: 410px;height: 30px;  background-color: #202F64;border-style: solid; border-color: #202F64;  \" ></div> "
                        + " <div style=\"width: 400px;height: 420px;  border-style: solid; border-color: #202F64;  padding-left: 10px;\" > "
                        + "<br>"
                        + "Dear " + memberName + ", "
                        + "<br><br>"
                        + "Please receive this note with best wishes.  "
                        + "<br><br>"
                        + "<div style=\"width: 370px;height: 40px; background-color: #A7D7D7 ;padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px\" >"
                        + "<div style=\"font-weight: bold;\" > " + "Verification Code.        " + member_pass_code_email + "</div>"
                        + "</div > "
                        + "<br><br>"
                        + "Please review and let us know if you have any questions. please contact our support."
                        + "<br><br>"
                        + "Thanks"
                        + "<br>"
                        + "IEB Registration Board"
                        + "<br>"
                        + "</div > "
                        + "<div style=\"width: 410px;height: 30px;  background-color: #202F64;border-style: solid; border-color: #202F64;  \" ></div> ";

                //emial send to email address
                //   String sendemail1 = sendemail.sendEmail(messageSubject, messageText, memberEmail, "2");
                //   logger.info("WEB :: forgotPasswordSubmitData ::sendemail::" + sendemail1);
                //strMsg = "Success!!! Password sent to your mobile number.";
                //   response.sendRedirect("forgotPassword.jsp?strMsg=" + strMsg);
                if (dbtrx.wasCommitted()) {
                    strMsg = "Success!!! Password sent to your mobile number.";
                    response.sendRedirect("forgotPassword.jsp?strMsg=" + strMsg);
                } else {
                    dbtrx.rollback();
                    strMsg = "Error!!! When recovery your password.Please try again";
                    response.sendRedirect("forgotPassword.jsp?strMsg=" + strMsg);
                }

            } else {
                //error
                strMsg = "Error!!! Mobile number is wrong.Please follow the instructions listed in the left. ";
                response.sendRedirect("forgotPassword.jsp?strMsg=" + strMsg);
            }


            /*
            int registerAppId = 0;
            Query regSQL = dbsession.createSQLQuery("select id from member_temp WHERE member_temp_pass ='" + memberVerifyId + "' AND member_temp_id ='" + memberRegId + "' AND status = '0'");
            if (!regSQL.list().isEmpty()) {
                registerAppId = Integer.parseInt(regSQL.uniqueResult().toString());

                logger.info("WEB :: forgotPasswordSubmitData ::  registerAppId:: " + registerAppId);

                //    HttpSession session = request.getSession(true);
                session.setAttribute("registerAppId", registerAppId);
                session.setAttribute("memberRegId", memberRegId);

                response.sendRedirect("registrationDashboard.jsp?sessionid=" + session.getId() + "&registerAppId=" + registerAppId);

            } else {
                strMsg = "Success!!! Your Application already approvved.";
                response.sendRedirect("registrationCheck.jsp?strMsg=" + strMsg);
            }
             */
        } else {
            //error
            strMsg = "Error!!! Member ID is wrong.Please follow the instructions listed in the left. ";
            response.sendRedirect("forgotPassword.jsp?strMsg=" + strMsg);
        }

    } else {
        //error
        strMsg = "Error!!! Member ID and Email and Mobile Number is required.Please follow the instructions listed in the left. ";
        response.sendRedirect("forgotPassword.jsp?strMsg=" + strMsg);
    }

    dbsession.clear();
    dbsession.close();

%>
