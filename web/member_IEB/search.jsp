<%@ include file="../header.jsp" %>


<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/photo_contact_success_style.css" rel="stylesheet" type="text/css"/>

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>


<!-- Start of navigation -->
<div class="about_banner_membership">
    <h2 class="display_41_saz text-white"> Seach Your Membership Number</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Membership</a></li>
                <li class="breadcrumb-item active" aria-current="page">Search</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of navigation -->

<%

    Session dbsessionMemberSearch = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxMemberSearch = dbsessionMemberSearch.beginTransaction();

    Query queryCommittee = null;
    Object committeeObj[] = null;

    Object memSearchObj[] = null;
    Object memSearchEduObj[] = null;

    String memberId = "";
    String memberShipId = "";
    String memberName = "";
    String memberEmail = "";
    String memberPhone = "";
    String memberPicture = "";
    String memberPictureUrl = "";
    String memberCenterName = "";
    String memberDivisionName = "";
    String profilePicture = "";
    String profilePictureUrl = "";
    String searchString = "";

    String sessionIdSearch = "";

    String memberUniversityShortName = "";
    String memberUniversityFullName = "";
    String memberUniversityPassingYear = "";

    String memberProfileUrl = "";

    String membershipId = request.getParameter("membershipId") == null ? "" : request.getParameter("membershipId").trim();
    String memberNameEmailMobile = request.getParameter("memberNameEmailMobile") == null ? "" : request.getParameter("memberNameEmailMobile").trim();
    String sessionIdSearchYn = request.getParameter("searchToken") == null ? "" : request.getParameter("searchToken").trim();

    if ((membershipId != null && !membershipId.isEmpty()) && (memberNameEmailMobile != null && !memberNameEmailMobile.isEmpty())) {

        searchString = " WHERE m.member_division_id = md.mem_division_id AND m.center_id = c.center_id AND (m.member_id = '" + membershipId + "' OR (m.member_name LIKE '%" + memberNameEmailMobile + "%' OR m.email_id LIKE '%" + memberNameEmailMobile + "%' OR m.mobile LIKE '%" + memberNameEmailMobile + "%')) ";
    } else {

        if ((membershipId != null && !membershipId.isEmpty())) {

            searchString = " WHERE m.member_division_id = md.mem_division_id AND m.center_id = c.center_id AND m.member_id = '" + membershipId + "' ";
        }

        if ((memberNameEmailMobile != null && !memberNameEmailMobile.isEmpty())) {

            searchString = " WHERE m.member_division_id = md.mem_division_id AND m.center_id = c.center_id AND (m.member_name LIKE '%" + memberNameEmailMobile + "%' OR m.email_id LIKE '%" + memberNameEmailMobile + "%' OR m.mobile LIKE '%" + memberNameEmailMobile + "%' )";
        }

    }

    // System.out.println("membershipId ::" + membershipId);
    //  System.out.println("memberNameEmailMobile ::" + memberNameEmailMobile);
//    System.out.println("searchString ::" + searchString);
    sessionIdSearch = session.getId();
%>

<div class="registration">
    <!-- Start search Section -->
    <section id="search" class="bg-custom-partho py-3" >
        <div class="container">
            <div class="row">
                <div class="col-lg-8 p-3 ">
                    <h3 class="font-weight-bold mb-2">Find Your Membership Number</h3>
                    <form class="form-group"  name="memberSearch" id="memberSearch" method="get" action="<%out.print(GlobalVariable.baseUrl);%>/member/search.jsp" >

                        <input value="<%=sessionIdSearch%>" id="searchToken" name="searchToken" type="hidden">

                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <span class="input-group-text padding-right" style="border-radius:0;">Member ID</span>
                                </div>
                                <input value="<%=membershipId%>" id="membershipId" name="membershipId" type="text" class="form-control" style="border-radius:0;" size="4">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" style="border-radius:0;">Name/Email/Mobile</span>
                                </div>
                                <input value="<%=memberNameEmailMobile%>" id="memberNameEmailMobile" name="memberNameEmailMobile" type="text" class="form-control" style="border-radius:0;">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block btn-lg">
                            <i class="fas fa-search pr-2"></i> Search
                        </button>
                    </form>
                </div>
                <div class="col-md-4 d-none d-lg-block">
                    <div class="card-header text-warning h4 font-weight-bold">
                        Quick Links <i class="fas fa-angle-double-down ml-5 float-right" style="color:rgb(255,0,0);cursor:pointer;"></i>
                    </div>
                    <ul class="list-group list-group-flush">
                        <a class="text-danger lead" href="#"><li class="list-group-item">What is IEB Membership</li></a>
                        <a class="text-danger lead" href="#"><li class="list-group-item">Membership Criteria</li></a>
                        <a class="text-danger lead" href="#"><li class="list-group-item">How to get Membership</li></a>
                    </ul>
                </div>		
            </div>
        </div>
    </section>
    <!-- Start search Section -->

    <!-- Result Section -->

    <section id="result" class="py-3">
        <div class="container">
            <%

                //  sessionIdSearch
                if (sessionIdSearch.equalsIgnoreCase(sessionIdSearchYn)) {

                    //  System.out.println("AAAA:: OK");
                    if ((searchString != null && !searchString.isEmpty())) {

                        Query memSearchSQL = dbsessionMemberSearch.createSQLQuery("SELECT "
                                + "m.id,m.member_id,m.member_name ,m.email_id,m.mobile,m.picture_name,  "
                                + "c.center_name,md.mem_division_name   "
                                + "FROM  member m ,member_division md,center c  "
                                + "" + searchString + " "
                                + "ORDER BY m.id ASC LIMIT 0 ,5");

                        //+ "ORDER BY cm.committee_showing_order ASC");
                        //  System.out.println("SearchSQL::" + memSearchSQL);
                        int sm = 1;
                        for (Iterator memSearchIt = memSearchSQL.list().iterator(); memSearchIt.hasNext();) {

                            memSearchObj = (Object[]) memSearchIt.next();
                            memberId = memSearchObj[0].toString().trim();
                            memberShipId = memSearchObj[1].toString().trim();
                            memberName = memSearchObj[2].toString().trim();
                            memberEmail = memSearchObj[3].toString().trim();
                            memberPhone = memSearchObj[4].toString().trim();
                            memberPicture = memSearchObj[5].toString().trim();
                            memberPictureUrl = GlobalVariable.imageMemberDirLink + memberPicture;

                            memberCenterName = memSearchObj[6] == null ? "" : memSearchObj[6].toString();

                            memberDivisionName = memSearchObj[7] == null ? "" : memSearchObj[7].toString();

                            Query memSearchEduSQL = dbsessionMemberSearch.createSQLQuery("SELECT "
                                    + "u.university_short_name,u.university_long_name,mei.year_of_passing  "
                                    + "FROM  member_education_info mei,university u  "
                                    + "WHERE mei.degree_type_id = '3' "
                                    + "AND mei.member_id = " + memberId + " "
                                    + "AND mei.board_university_id = u.university_id "
                                    + "");

                            for (Iterator memSearchEduIt = memSearchEduSQL.list().iterator(); memSearchEduIt.hasNext();) {

                                memSearchEduObj = (Object[]) memSearchEduIt.next();
                                memberUniversityShortName = memSearchEduObj[0] == null ? "" : memSearchEduObj[0].toString();
                                memberUniversityFullName = memSearchEduObj[1] == null ? "" : memSearchEduObj[1].toString();
                                memberUniversityPassingYear = memSearchEduObj[2] == null ? "" : memSearchEduObj[2].toString();

                            }

                            memberProfileUrl = GlobalVariable.baseUrl + "/member/search.jsp?membershipId=" + memberShipId;
            %>

            <div class="row">
                <div class="col">
                    <table class="table">
                        <thead class="text-primary font-weight-bold h4">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" class="text-center" colspan="2">Details</th>
                                <th scope="col" class="text-center"> Picture</th>
                            </tr>
                        </thead>
                        <tbody>                            

                            <tr>
                                <th scope="row" rowspan="6" class="custom-border-bottom"><%=sm%></th>
                                <td colspan="2"><strong>Member ID: </strong><%=memberShipId%></td>
                                <td rowspan="6">
                                    <img width="250" src="<%=memberPictureUrl%>" alt="<%=memberName%>" class="img-fluid">
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Name: </strong><%=memberName%></td>
                            </tr>

                            <tr>
                                <td><strong>Email: </strong><%=memberEmail%></td>
                            </tr>
                            <tr>
                                <td><strong>Division: </strong><%=memberDivisionName%></td>
                            </tr>
                            <tr>
                                <td><strong>Center: </strong><%=memberCenterName%></td>
                            </tr>

                            <tr>
                                <td class="custom-border-bottom"><strong>Institution: </strong><%=memberUniversityShortName%></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <%
                    sm++;
                }

            %>





        </div>
    </section>
    <%    }
        }
        dbsessionMemberSearch.clear();
        dbsessionMemberSearch.close();
    %>

</div>
<%@ include file="../footer.jsp" %>
