<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query approvalSQL = null;
    Object objApprova[] = null;
    String requestId = "";
    String requestMemId = "";
    String requestStatus = "";
    String requestStatusOpt = "";
    String requestDate = "";
    String requestName = "";
    java.util.Date dob = new Date();
    String yearOfPassing = "";
    String weeks = "";
    int trainingId = 0;
    int m = 1;

    String pictureName = "";
    String pictureLink = "";

    MemberProposerInfoTemp proposerTmp = null;



%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>


<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Membership Approval Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->

                    <p class="content_title_saz pb-2 m-3">Request Information</p>

                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                        <thead>
                            <tr>
                                <th class="col-1">#</th>
                                <th class="col-1">Picture</th>
                                <th class="col-3">Name</th>
                                <th class="col-1">Req. date</th>
                                <th class="col-1">App. date</th>
                                <th class="col-5">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%

                                approvalSQL = dbsession.createSQLQuery("SELECT pt.id,pt.member_id tempMemberId,pt.proposer_id,pt.status,pt.request_date, "
                                        + "mt.member_name,mt.picture_name  "
                                        + "FROM member_proposer_info_temp pt,member_temp mt "
                                        + "WHERE  pt.member_id = mt.member_id AND pt.proposer_id=" + memberIdH + " ");

                                if (!approvalSQL.list().isEmpty()) {

                                    for (Iterator itr1 = approvalSQL.list().iterator(); itr1.hasNext();) {

                                        objApprova = (Object[]) itr1.next();
                                        requestId = objApprova[0].toString();
                                        requestMemId = objApprova[1].toString();
                                        requestStatus = objApprova[3].toString();

                                        String btnDetails = "<a  href=\"" + GlobalVariable.baseUrl + "/member/memberShipApprovalRequestDetails.jsp?sessionid=" + session.getId() + "&reqMemId=" + requestMemId + "\" title=\"Details\" class=\"btn btn-primary btn-sm text-white\" ><i class=\"fa fa-info-circle\"></i> Details</a>&nbsp;";

                                        String btnApprovDecline = "";
                                        if (requestStatus.equals("0")) {
                                            requestStatusOpt = "";
                                            btnApprovDecline = "<a onclick=\"requestMemberApprovalByProposer('" + session.getId() + "','" + requestId + "')\" title=\"Request Appreve\"class=\"btn btn-primary btn-sm text-white\" ><i class=\"fa fa-check-circle\"></i> Approve</a> &nbsp;"
                                                    + "<a title=\"Request Decline\"class=\"btn btn-danger btn-sm text-white\" ><i class=\"fa fa-times-circle\"></i> Decline</a>";

                                        }

                                        if (requestStatus.equals("1")) {
                                            btnApprovDecline = "";
                                            requestStatusOpt = "<a class=\"btn btn-success btn-sm text-white\" ><i class=\"fa fa-check-circle\"></i> Approved</a>";

                                        }

                                        if (requestStatus.equals("2")) {
                                            btnApprovDecline = "";
                                            requestStatusOpt = "<a class=\"btn btn-danger btn-sm text-white\" ><i class=\"fa fa-times-circle\"></i> Declined</a>";

                                        }

                                        requestDate = objApprova[4].toString();

                                        requestName = objApprova[5].toString();

                                        pictureName = objApprova[6].toString();

                                        pictureLink = "<img style=\"width:50px;\" src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + requestName + "\" class=\"img-fluid img_1\">";


                            %>
                            <tr>
                                <td><%=m%></td>
                                <td> 

                                    <%=pictureLink%>
                                </td>
                                <td><%=requestName%></td>
                                <td><%=requestDate%></td>
                                <td></td>
                                <td id="acttionBox<%=requestId%>" style="padding: .5rem !important;">

                                    <%=btnDetails%>
                                    <%=btnApprovDecline%>
                                    <%=requestStatusOpt%>
                                </td>

                            </tr>

                            <%    m++;
                                    }

                                } else {

                                }
                                dbsession.flush();
                                dbsession.close();
                            %>
                        </tbody>
                    </table>


                    <!-- ============ End Education information Table ================== -->


                </div>
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
