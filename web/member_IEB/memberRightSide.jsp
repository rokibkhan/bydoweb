<%@page import="com.appul.util.GlobalVariable"%>
<%
   // String sessionIdH = session.getId();
%>
<div class="card card-info">
    <div class="card-header panel_heading_saz"><i class="fa fa-list-alt"></i> My IEB</div>
    <div class="list-group">
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberDashboard.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-user"></i> Personal Information</a>
        <!--a href="change-info.html" class="list-group-item"><i class="fa fa-info-circle"></i> Change Information</a -->

        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberEducationInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fal fa-book-reader"></i> Education Information</a>
        <a href="#" class="list-group-item"><i class="fa fa-th-large"></i> Training Information</a>
        <a href="#" class="list-group-item"><i class="fa fa-th-large"></i> Professional Information</a>
        <a href="#" class="list-group-item"><i class="fa fa-th-large"></i> Publications</a>
        <a href="#" class="list-group-item"><i class="fa fa-th-large"></i> Project Experiences</a>
        <a href="#" class="list-group-item"><i class="fa fa-th-large"></i> Certificate Information</a>
        <a href="#" class="list-group-item"><i class="fa fa-th-large"></i> Preview Profile</a>
        <a href="#" class="list-group-item"><i class="fa fa-th-large"></i> Renew Membership</a>
        <a href="#" class="list-group-item"><i class="fa fa-th-large"></i> Membership Upgradation</a>
        <a href="my-wishlist.html" class="list-group-item"><i class="fa fa-credit-card"></i> Payment History</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberChangePasswordInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-key"></i> Change Password</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/logout.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-power-off"></i> Logout</a>
    </div>
</div>