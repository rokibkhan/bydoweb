<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    String instituteName = "";
    String boardUniversityName = "";
    java.util.Date dob = new Date();
    String yearOfPassing = "";
    String resultTypeName = "";
    String result = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String centerId = "";
    int certificateId = 0;
    String certificateShortName = "";
    String certificateLongName = "";
    String certificateFileName = "";
    MemberCertificate mci = null;



%>

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>

<br>


<script type="text/javascript">
    function certificateDeleteInfo(arg1, arg2) {

        console.log("certificateDeleteInfo -> arg1 :: " + arg1 + " arg2:: " + arg2);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnDeleteComfirmation\" onclick=\"certificateDeleteInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";


        memberLGModal = $('#memberLGModal');
        //   memberLGModal = $('#taskLGModal');
        memberLGModal.find("#memberLGModalTitle").text("Certificate Delete Confirmation");
        memberLGModal.find("#memberLGModalBody").html("");
        memberLGModal.find("#memberLGModalFooter").html(btnInvInfo);
        memberLGModal.modal('show');


    }



    function certificateDeleteInfoConfirm(arg1, arg2) {
        console.log("certificateDeleteInfoConfirm:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("certificateDeleteProcess.jsp", {deleteId: arg1, sessionid: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#infoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                memberLGModal.modal('hide');
            } else {

            }


        }, "json");

    }

</script>


<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Certificate Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->
                    <p class="content_title_saz pb-2 m-3">Certificate</p>

                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                        <thead>
                            <tr>
                                <th scope="col">Certificate Short Name</th>
                                <th scope="col">Certificate Name</th>
                                <th scope="col">Download</th>
                                <th scope="col">Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%

                                q1 = dbsession.createQuery("from MemberCertificate WHERE  member_id=" + memberIdH + " ");

                                for (Iterator itr = q1.list().iterator();
                                        itr.hasNext();) {
                                    mci = (MemberCertificate) itr.next();
                                    certificateId = mci.getId();
                                    certificateLongName = mci.getCertificateLongName() == null ? "" : mci.getCertificateLongName();
                                    certificateShortName = mci.getCertificateShortName() == null ? "" : mci.getCertificateShortName();
                                    certificateFileName = mci.getCertificateFileName() == null ? "" : mci.getCertificateFileName();


                            %>
                            <tr id="infoBox<%=certificateId%>">
                                <td><%=certificateShortName%></td>
                                <td><%=certificateLongName%></td> 
                                <td><a title="View" href="<%out.print(GlobalVariable.baseUrl);%>/upload/photos/<%=certificateFileName%>?sessionid=<%=session.getId()%>&act=del&certificateId=<%=certificateId%>" target="_blank" ><%=certificateFileName%> </a> </td> 
                                <td style="cursor:pointer">

                                    <a title="Remove" onclick="certificateDeleteInfo(<% out.print("'" + certificateId + "','" + sessionIdH + "'");%>)" ><i class="fa fa-trash"></i> </a> 

                                </td>

                            </tr>

                            <%                                }

                                dbsession.flush();
                                dbsession.close();


                            %>
                        </tbody>
                    </table>



                    <!-- ============ End Education information Table ================== -->

                    <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="<% out.print(GlobalVariable.baseUrl);%>/MemberCertificateUpload?sessionid=<%=session.getId()%>&act=add" onSubmit="return fromDataSubmitValidation()" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">*Certificate Short Name :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="certificateShortName" name="certificateShortName" placeholder="Certificate Short Name" required="">
                                <input type="hidden" class="form-control" id="memberId" name="memberId" value="<%=memberIdH%>">

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-3 col-form-label text-right">*Certificate  Name :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="certificateLongName" name="certificateLongName"  placeholder="Certificate Name" required="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-3 col-form-label text-right">*Upload Certificate :</label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" id="certificateFileName" name="certificateFileName"  placeholder="certificateFileName" required="">
                            </div>
                        </div>

                        <div class="form-group row">                            
                            <div class="col-sm-6 offset-sm-3">
                                <button type="submit" class="btn btn-primary">Upload</button>
                            </div>
                        </div>






                    </form>
                    <!-- ================ End billing information form ================= -->


                </div>
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
