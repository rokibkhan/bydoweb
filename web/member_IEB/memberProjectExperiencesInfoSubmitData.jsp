
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>



<%
    Session dbsession = null;
    String qryparam = "";;

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    getDate date = new getDate();

//    if (session.isNew()) {
//        response.sendRedirect("logout.jsp");
//    }
    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        memberIdH = session.getAttribute("memberId").toString();

        System.out.println("memberIdH " + memberIdH);

        getRegistryID regId = new getRegistryID();

        MemberProjectInfo memberProjectInfo = null;
        String act = request.getParameter("act").trim();

        if (act.equals("edit")) {
            String projectInfoId = request.getParameter("projectInfoId").trim();
            Query q4 = dbsession.createQuery("from MemberProjectInfo where id='" + projectInfoId + "' ");
            Iterator itr4 = q4.list().iterator();
            if (itr4.hasNext()) {
                memberProjectInfo = (MemberProjectInfo) itr4.next();
            }

            String projectName = request.getParameter("projectName").trim();
            String memberRole = request.getParameter("memberRole").trim();
            String projectCategory = request.getParameter("projectCategory").trim();
            String projectDetails = request.getParameter("projectDetails").trim();
            String startDate = request.getParameter("startDate").trim();
            String endDate = request.getParameter("endDate").trim();

            java.util.Date startDate1 = new SimpleDateFormat("yyyy-mm-dd").parse(startDate);
            java.util.Date endDate1 = new SimpleDateFormat("yyyy-mm-dd").parse(endDate);

//            memberProjectInfo = new MemberProjectInfo();
            Member member = new Member();
            int memberId1 = 0;
            String memberId = session.getAttribute("memberId").toString();
            memberId1 = Integer.parseInt(memberId);

            member.setId(memberId1);
            memberProjectInfo.setMember(member);
            memberProjectInfo.setMemberProjectTitle(projectName);
            memberProjectInfo.setMemberProjectMemberRole(memberRole);
            memberProjectInfo.setMemberProjectCategory(projectCategory);
            memberProjectInfo.setMemberProjectDetails(projectDetails);
            memberProjectInfo.setMemberProjectStartDate(startDate1);
            memberProjectInfo.setMemberProjectEndDate(endDate1);
            dbsession.update(memberProjectInfo);
            strMsg = "Information updated successfully.";
        } else if (act.equals("add")) {

            String projectName = request.getParameter("projectName").trim();
            String memberRole = request.getParameter("memberRole").trim();
            String projectCategory = request.getParameter("projectCategory").trim();
            String projectDetails = request.getParameter("projectDetails").trim();
            String startDate = request.getParameter("startDate").trim();
            String endDate = request.getParameter("endDate").trim();

            java.util.Date startDate1 = new SimpleDateFormat("yyyy-mm-dd").parse(startDate);
            java.util.Date endDate1 = new SimpleDateFormat("yyyy-mm-dd").parse(endDate);

            String projectInfoId = regId.getID(38);
            memberProjectInfo = new MemberProjectInfo();
            Member member = new Member();
            int memberId1 = 0;
            int projectInfoId1 = 0;
            String memberId = session.getAttribute("memberId").toString();
            memberId1 = Integer.parseInt(memberId);
            projectInfoId1 = Integer.parseInt(projectInfoId);

            member.setId(memberId1);
            memberProjectInfo.setId(projectInfoId1);
            memberProjectInfo.setMember(member);
            memberProjectInfo.setMemberProjectTitle(projectName);
            memberProjectInfo.setMemberProjectMemberRole(memberRole);
            memberProjectInfo.setMemberProjectCategory(projectCategory);
            memberProjectInfo.setMemberProjectDetails(projectDetails);
            memberProjectInfo.setMemberProjectStartDate(startDate1);
            memberProjectInfo.setMemberProjectEndDate(endDate1);
            dbsession.save(memberProjectInfo);
            strMsg = "Information saved successfully.";

        } else if (act.equals("del")) {
            String projectInfoId = request.getParameter("projectInfoId").trim();
            Query q4 = dbsession.createQuery("delete from MemberProjectInfo where id='" + projectInfoId + "' ");
            q4.executeUpdate();
            strMsg = "Information removed successfully.";
        }

        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
//            strMsg = "Success!!! Member Training Information added ";
            response.sendRedirect("memberProjectExperiencesInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        } else {
            strMsg = "Error!!! When Professional Information added ";
            dbtrx.rollback();
            response.sendRedirect("memberProjectExperiencesInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        }
    } else {
        System.out.println("LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

%>
