<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String registerAppId = "";
    String memberRegId = "";
    if (session.getAttribute("registerAppId") != null && session.getAttribute("memberRegId") != null) {

        sessionIdH = session.getId();
        registerAppId = session.getAttribute("registerAppId").toString();
        memberRegId = session.getAttribute("memberRegId").toString();

    } else {

        response.sendRedirect(GlobalVariable.baseUrl + "/member/registrationCheck.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    int memberObjectiveId = 0;
    String memberObjective = "";

    Query q1 = null;
    String memberName = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String userEmail = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String centerId = "";
    String pictureLink = "";
    String mAddressLine1 = "";
    String mAddressLine2 = "";
    String pAddressLine1 = "";
    String pAddressLine2 = "";
    MemberTemp member = null;
    AddressBook addressBook = null;

    q1 = dbsession.createQuery("from MemberTemp as member WHERE id=" + registerAppId + " ");

    Object[] object = null;

    for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
        member = (MemberTemp) itr.next();

        memberName = member.getMemberName();
        fatherName = member.getFatherName() == null ? "" : member.getFatherName();
        motherName = member.getMotherName() == null ? "" : member.getMotherName();
        placeOfBirth = member.getPlaceOfBirth() == null ? "" : member.getPlaceOfBirth();
        dob = member.getDob();
        gender = member.getGender().trim();
        if (gender.equals("M")) {
            gender = "Male";
        } else {
            gender = "Female";
        }
        mobileNo = member.getMobile();
        phone1 = member.getPhone1();
        phone2 = member.getPhone2();
        userEmail = member.getEmailId();
        bloodGroup = member.getBloodGroup();

        pictureLink = member.getPictureName();

    }

    Query q2 = dbsession.createSQLQuery("select *from address_book WHERE id=(select address_id from member_address_temp where member_id= " + registerAppId + " and address_Type='M' ) ");
    for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
        object = (Object[]) itr2.next();
        mAddressLine1 = object[1].toString();
        mAddressLine2 = object[2].toString();

    }
    Query q3 = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address_temp where member_id= " + registerAppId + " and address_Type='P' ) ");
    for (Iterator itr3 = q3.list().iterator(); itr3.hasNext();) {

        object = (Object[]) itr3.next();
        pAddressLine1 = object[1].toString();
        pAddressLine2 = object[2].toString();

    }

    dbsession.flush();
    dbsession.close();


%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="registrationLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-user"></i> My Profile</div>
                <div class="card-block" style="min-height: 533px;">

                    <p class="content_title_saz pb-2 m-3" style="border: none;">Application Status: Pending</p>
                    

                    <p class="content_title_saz pb-2 m-3">Personal Information </p>
                    <div class="row">
                        <div class="col-sm-8">
                            <form class="profile_form_horizontal_saz p_form">
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">REG ID</label>
                                    <div class="col-sm-9"><p><%=memberRegId%></p></div>
                                </div>

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Name</label>
                                    <div class="col-sm-9"><p><%=memberName%></p></div>
                                </div> 
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Father's Name</label>
                                    <div class="col-sm-9"><p><%=fatherName%></p></div>
                                </div> 
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Mother's Name</label>
                                    <div class="col-sm-9"><p><%=motherName%></p></div>
                                </div>

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Email</label>
                                    <div class="col-sm-9"><p><%=userEmail%></p></div>
                                </div>

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Mobile</label>
                                    <div class="col-sm-9"><p><%=mobileNo%></p></div>
                                </div>
                                

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Gender</label>
                                    <div class="col-sm-9"><p><%=gender%></p></div>
                                </div>


                            </form>
                        </div>
                        <div class="col-sm-4">
                            <img style="width: 150px;" src="<%out.print(GlobalVariable.baseUrl);%>/upload/member/<%=pictureLink%>" alt="<%=memberName%>"/>
                        </div>
                    </div>
                    <!-- ============ End personal information form ================== -->



                    <p class="content_title_saz pb-2 m-3">Mailing Address </p>

                    <form class=" profile_form_horizontal_saz">

                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2 ml-3 font-weight-bold">Address</label>
                            <div class="col-sm-9">
                                <p><%=mAddressLine1 + " " + mAddressLine2%>

                                </p>
                            </div>
                        </div>


                    </form>
                    <!-- ================ End billing information form ================= -->

                    <p class="content_title_saz pb-2 m-3">Permanent Address </p>

                    <form class=" profile_form_horizontal_saz">

                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2 ml-3 font-weight-bold">Address</label>
                            <div class="col-sm-9"><p><%=pAddressLine1 + " " + pAddressLine2%></p></div>
                        </div>



                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<br/>                        


<%@ include file="../footer.jsp" %>
