<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.appul.util.GlobalVariable"%>

<div class="card card-info">
    <div class="card-header panel_heading_saz"><i class="fa fa-list-alt"></i> My IEB</div>
    <div class="list-group">
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/registrationDashboard.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-info-circle"></i> Personal Information</a>

        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/registrationDashboardEducationInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-university"></i> Educational Information</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/registrationDashboardProfessionalInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-briefcase"></i> Professional Information</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/registrationDashboardProposerInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-university"></i> Proposer Information</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/registrationDashboardPaymentHistoryInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-credit-card"></i> Payment History</a>

        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/logout.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-power-off"></i> Logout</a>
    </div>
</div>


