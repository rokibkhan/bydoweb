<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.appul.util.GlobalVariable"%>
<%
    Session dbsessionLeftSide = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxLeftSide = null;
    dbtrxLeftSide = dbsessionLeftSide.beginTransaction();

    String memberLifeLeftSideSQL = "SELECT life_status  FROM member_life_info  WHERE  member_id='" + session.getAttribute("memberId").toString() + "'";

    String memberLifeStatusLeftSide = dbsessionLeftSide.createSQLQuery(memberLifeLeftSideSQL).uniqueResult() == null ? "0" : dbsessionLeftSide.createSQLQuery(memberLifeLeftSideSQL).uniqueResult().toString();

    System.out.println("WEB :: memberLifeStatusLeftSide :: " + memberLifeStatusLeftSide);

//    Object objAprvCount[] = null;
//    // String sessionIdH = session.getId();
//    //  String approvalRequstCounter = "<span class=\"badge\" style=\"background: #007bff; color:#fff;\">1</span>";
//    String approvalRequstCounter = "";
//
//    Query appCountSQL = dbsessionLeftSide.createSQLQuery("SELECT count(*) AS fff FROM member_proposer_info_temp pt WHERE  pt.proposer_id='" + session.getAttribute("memberId").toString() + "' AND pt.status = '0'");
//
//    String approvalRequstCounter1 = "";
//
//    if (!appCountSQL.list().isEmpty()) {
//
//        for (Iterator itrApp = appCountSQL.list().iterator(); itrApp.hasNext();) {
//
//            objAprvCount = (Object[]) itrApp.next();
//            approvalRequstCounter1 = objAprvCount[0].toString();
//            approvalRequstCounter = "<span class=\"badge\" style=\"background: #007bff; color:#fff;\">" + approvalRequstCounter1 + "</span>";
//        }
//
//    }
    SessionImpl sessionImpl = (SessionImpl) dbsessionLeftSide;
    Connection con = sessionImpl.connection();
    Statement stmt = null;

    //  String approvalRequstCounter = "<span class=\"badge\" style=\"background: #007bff; color:#fff;\">1</span>";
    String approvalRequstCounterBox = "";
    int approvalRequstCounter = 0;
    try {
        stmt = con.createStatement();
        String appCountSQL = "SELECT COUNT(*) FROM member_proposer_info_temp pt WHERE  pt.proposer_id='" + session.getAttribute("memberId").toString() + "' AND pt.status = '0'";
        ResultSet rs = stmt.executeQuery(appCountSQL);
        //Extact result from ResultSet rs
        while (rs.next()) {
            //System.out.println("COUNT(*)=" + rs.getInt("COUNT(*)"));

            approvalRequstCounter = rs.getInt("COUNT(*)");

        }
        // close ResultSet rs
        rs.close();
    } catch (SQLException s) {
        s.printStackTrace();
    }
    // close Connection and Statement
    con.close();
    stmt.close();

    if (approvalRequstCounter != 0) {
        approvalRequstCounterBox = "<span class=\"badge\" style=\"background: #007bff; color:#fff;\">" + approvalRequstCounter + "</span>";
    } else {
        approvalRequstCounterBox = "";
    }

//    Object[] memberLifeObj = null;
//    String memberLifeStatus = "";
//    Query memberLifeSQL = dbsessionLeftSide.createSQLQuery("SELECT * FROM member_life_info where member_id='" + session.getAttribute("memberId").toString() + "'");
//    System.out.println("LeftSide memberLifeSQL ::" + memberLifeSQL);
//    for (Iterator memberLifeItr = memberLifeSQL.list().iterator(); memberLifeItr.hasNext();) {
//        memberLifeObj = (Object[]) memberLifeItr.next();
//        memberLifeStatus = memberLifeObj[2].toString();
////        if (memberLifeStatus.equals("1")) {
////            memberLifeStatusText = "Life";
////        } else {
////            memberLifeStatusText = "";
////        }
//    }
   


%>
<div class="card card-info">
    <div class="card-header panel_heading_saz"><i class="fa fa-list-alt"></i> My IEB</div>
    <div class="list-group">
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberDashboard.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-info-circle"></i> Personal Information</a>

        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberEducationInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-university"></i> Educational Information</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberTrainingInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-building"></i> Training Information</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberProfessionalInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-briefcase"></i> Professional Information</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberProjectExperiencesInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-users"></i> Project Experiences</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberPublicationsInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-sticky-note"></i> Publications</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberConferenceInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-sticky-note"></i> Conference/Workshop</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberCertificateInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-certificate"></i> Certificate Information</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberSignatureInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-sticky-note"></i> Signature Info</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberProfilePreviewInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-search"></i> Preview Profile</a>
        <%if (memberLifeStatusLeftSide.equals("0")) {%>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberRenewalInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-th-large"></i> Renew Membership</a>
        <% }%>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberUpgradationInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-chevron-up"></i> Membership Upgradation</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberShipApprovalRequestList.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-life-ring"></i> Approval Request <%=approvalRequstCounterBox%></a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberAppliedJobList.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fab fa-asymmetrik"></i> Applied Job List</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberPaymentHistoryInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-credit-card"></i> Payment History</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberChangePasswordInfo.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-key"></i> Change Password</a>
        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/logout.jsp?sessionid=<%=session.getId()%>" class="list-group-item"><i class="fa fa-power-off"></i> Logout</a>
    </div>
</div>


<%

    dbsessionLeftSide.flush();
    dbsessionLeftSide.close();
%>