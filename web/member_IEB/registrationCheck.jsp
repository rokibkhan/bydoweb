<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);

    }

    System.out.println("Header sessionIdH :: " + sessionIdH);
    System.out.println("Header userNameH :: " + userNameH);
    System.out.println("Header userStoreIdH :: " + memberIdH);

//    String sessionid = request.getParameter("sessionid").trim();
//
//    sessionid = session.getId();
//    String uid = session.getAttribute("username").toString().toUpperCase();    
//    String userStoreId = session.getAttribute("memberId").toString();
//    String sessionidH = request.getParameter("sessionid").trim();
//
//    sessionidH = session.getId();
//    String uidH = session.getAttribute("username").toString().toUpperCase();    
    //   String userStoreIdH = session.getAttribute("memberId").toString();
//System.out.println("uidH :: "+uidH);
    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }


%>
<!-- Login & Register & About CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about.css">
<!-- Starting of login form -->

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="about_banner_membership">
    <h2 class="display_41_saz text-white">Membership Application Check</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Membership</a></li>
                <li class="breadcrumb-item active" aria-current="page">Status Check</li>
            </ol>
        </nav>
    </div>
</div>

<div class="container">
    <div class="login-form">
        <div class="card m-3">
            <div class="card-body text-center">
                <p class="card-text m-3">Please enter your Registration number and and mobile verification code</p>

                <div class="row globalAlertInfoBoxConParentTT">
                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->
                </div>


                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationCheckSubmitData.jsp">

                    <div style="display: block1; font-size:  small;color: red;text-align: center; " >                
                        <%
                            String rtnUser = "";

                            try {
                                rtnUser = session.getAttribute("rtnUser").toString();
                                if (rtnUser.equalsIgnoreCase("1") || rtnUser != null) {
                                    out.println("Member ID or Password does not match! ");
                                } else {
                                    out.print(" ");
                                }
                            } catch (Exception ex) {
                                // out.print(ex.getMessage());
                            }

                        %>

                    </div>

                    <div class="form-group text-center">
                        <input id="memberRegId" name="memberRegId" type="text" class="form-control"  placeholder="Registration ID">
                    </div>

                    <div class="form-group text-center">
                        <input id="password" name="password" type="password" class="form-control d-inline-flex"  placeholder="Password">
                    </div>

                    <button type="submit" class="btn btn-lg mx-auto submit-button font-weight-bold text-white">Login</button>

                </form>

            </div>
        </div>
    </div>
</div>
<!-- Ending of login form -->


<%@ include file="../footer.jsp" %>
