<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    String instituteName = "";
    String courseTitle = "";
    String trainingTitle = "";
    java.util.Date dob = new Date();
    String yearOfPassing = "";
    String weeks = "";

    MemberTrainingInfo mei = null;



%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Training Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();

                        String trainingId = request.getParameter("trainingId") == null ? "" : request.getParameter("trainingId").trim();

                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->
 

                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">

                        <tbody>
                            <%

                                q1 = dbsession.createQuery("from MemberTrainingInfo where  member_id=" + memberIdH + " and id='" + trainingId + "' ");

                                for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                                    mei = (MemberTrainingInfo) itr.next();

                                    instituteName = mei.getMemberTrainingInstitute() == null ? "" : mei.getMemberTrainingInstitute();
                                    trainingTitle = mei.getMemberTrainingTitle() == null ? "" : mei.getMemberTrainingTitle();
                                    courseTitle = mei.getMemberTrainingCourse() == null ? "" : mei.getMemberTrainingCourse();
                                    yearOfPassing = mei.getMemberTrainingYear() == null ? "" : mei.getMemberTrainingYear();
                                    weeks = mei.getMemberTrainingDuration() == null ? "" : mei.getMemberTrainingDuration();

                                }
                                dbsession.flush();
                                dbsession.close();

                            %>
                        </tbody>
                    </table>


                    <!-- ============ End Education information Table ================== -->

                    <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberTrainingInfoSubmitData.jsp?sessionid=<%=session.getId()%>&act=edit" onSubmit="return fromDataSubmitValidation()">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label mx-3">*Training Title :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="trainingTitle" name="trainingTitle" placeholder="Training Title" required="" value="<%=trainingTitle%>">
                                <input type="hidden" class="form-control" id="trainingId" name="trainingId"  value="<%=trainingId%>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label mx-3">*Training Institute with Country :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="trainingInstitute" name="trainingInstitute" placeholder="Training Institute with Country" required="" value="<%=instituteName%>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-4 col-form-label mx-3">*Name of Topic/Course :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="trainingCourse" name="trainingCourse" placeholder="Name of Topic/Course" required="" value="<%=courseTitle%>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label mx-3">*Duration (Week) :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="trainingDuration" name="trainingDuration" placeholder="Duration (Week)" required="" value="<%=weeks%>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label mx-3">*Training Year :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="trainingYear" name="trainingYear"  placeholder="Training Year" required="" value="<%=yearOfPassing%>">
                            </div>
                        </div>


                        <div class="form-group row text-right ml-5">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary mr-3">Submit</button>
                                <button type="button" class="btn btn-primary">Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- ================ End billing information form ================= -->

                </div>
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
