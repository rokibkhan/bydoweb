<%@ include file="../header.jsp" %>


<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="about_banner_membership">
    <h2 class="display_41_saz text-white">membership Fee</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Membership</a></li>
                <li class="breadcrumb-item active" aria-current="page">Membership Fee</li>
            </ol>
        </nav>
    </div>
</div>







<section class="news-single-content pt-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="news-content news_cont_saz">
                    <p class="subs_fee_title text-center"><strong>Fees For New Member</strong></p><br>
                    <div class="table-responsive">
                        <table class="table table-bordered table_saz">
                            <thead>
                                <tr style="background: cadetblue;">
                                    <th>Category</th>
                                    <th colspan="6">Subscription Rate</th>
                                    <th>Others</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td>Entrace fee (Tk.)</td>
                                    <td>Annual subscription (Tk.)</td>
                                    <td>Diploma fee (Tk.)</td>
                                    <td>ID card (Tk.)</td>
                                    <td>Total (Tk.)</td>
                                    <td>Age in years (at least)</td>
                                    <td>Experience (minimum)</td>
                                </tr>
                                <tr>
                                    <td>Fellow</td>
                                    <td>1,000/-</td>
                                    <td>1,000/-</td>
                                    <td>200/-</td>
                                    <td>100/-</td>
                                    <td>2,300/-</td>
                                    <td>37</td>
                                    <td>12 years (if associate member/member) <br>20 years (if non member) </td>
                                </tr>
                                <tr>
                                    <td>Member</td>
                                    <td>1,000/-</td>
                                    <td>600/-</td>
                                    <td>200/-</td>
                                    <td>100/-</td>
                                    <td>1,900/-</td>
                                    <td>27</td>
                                    <td>2 years (if associate member) <br>3 years (if non member) </td>
                                </tr>
                                <tr>
                                    <td>Associate Member</td>
                                    <td>300/-</td>
                                    <td>300/-</td>
                                    <td>200/-</td>
                                    <td>100/-</td>
                                    <td>900/-</td>
                                    <td>-</td>
                                    <td>B.Sc Engg. or Equivalent</td>
                                </tr>
                            </tbody>
                        </table>
                    </div><br>


                    <p class="subs_fee_title text-center"><strong>Fees For the Members who are living Abroad</strong></p><br>
                    <div class="table-responsive">
                        <table class="table table-bordered table_saz">
                            <thead>
                                <tr style="background: cadetblue;">
                                    <th>&nbsp;</th>
                                    <th>Entrance Fee</th>
                                    <th>Annual Subscription</th>
                                    <th>Diploma Fee</th>
                                    <th>Re-enrolment Fee</th>
                                    <th>Total Fee</th>
                                    <th>EBP</th>
                                </tr>
                            </thead>

                            <tbody>                                        
                                <tr>
                                    <td>Fellow</td>
                                    <td>50 US</td>
                                    <td>20 US</td>
                                    <td>20 US</td>
                                    <td>70US</td>
                                    <td>190 US</td>
                                    <td>10 US</td>
                                </tr>
                                <tr>
                                    <td>Member</td>
                                    <td>50 US</td>
                                    <td>20 US</td>
                                    <td>20 US</td>
                                    <td>70 US</td>
                                    <td>160 US</td>
                                    <td>10 US</td>
                                </tr>
                                <tr>
                                    <td>Associate Member</td>
                                    <td>25 US</td>
                                    <td>40 US</td>
                                    <td>20US</td>
                                    <td>35 US</td>
                                    <td>120 US</td>
                                    <td>10 US</td>
                                </tr>
                            </tbody>
                        </table>
                    </div><br><br>
                </div>
            </div>
        </div>
    </div>
</section>

<%@ include file="../footer.jsp" %>
