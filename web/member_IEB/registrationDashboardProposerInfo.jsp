<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String registerAppId = "";
    String memberRegId = "";
    if (session.getAttribute("registerAppId") != null && session.getAttribute("memberRegId") != null) {

        sessionIdH = session.getId();
        registerAppId = session.getAttribute("registerAppId").toString();
        memberRegId = session.getAttribute("memberRegId").toString();

    } else {

        response.sendRedirect(GlobalVariable.baseUrl + "/member/registrationCheck.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    Query q2 = null;
    int degreeId = 0;
    String degreeName = "";
    String instituteName = "";
    String boardUniversityName = "";
    java.util.Date dob = new Date();
    String yearOfPassing = "";
    String resultTypeName = "";
    String result = "";

    MemberEducationInfoTemp mei = null;

    q1 = dbsession.createQuery("from MemberEducationInfoTemp where  member_id=" + registerAppId + " ");

    MemberTemp member = null;
    String memberSubdivisionName = "";
    String memberSubdivisionFullName = "";
    int educationfInfoId = 0;

    

%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css"/>

<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">


<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="registrationLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Proposer Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->

                    <br><br>
                    <%
                        Query recSQL = null;
                        Object[] recObject = null;

                        Query recMemSQL = null;
                        Object[] recMemObject = null;
                        String proposerId = "";
                        String proposerStatus = "";
                        String requestStatusOpt = "";
                        int proposerId1 = 0;
                        int proposerId2 = 0;
                        int proposerId3 = 0;
                        String proposerStatus1 = "";
                        String proposerStatus2 = "";
                        String proposerStatus3 = "";
                        String recMemberId = "";
                        String recMemberName = "";
                        String recMemberStr1 = "";
                        String recMemberStr2 = "";
                        String recMemberStr3 = "";
                        int r = 1;
                        recSQL = dbsession.createSQLQuery("SELECT * FROM member_proposer_info_temp WHERE member_id = '" + registerAppId + "'");
                        for (Iterator recItr = recSQL.list().iterator(); recItr.hasNext();) {
                            recObject = (Object[]) recItr.next();

                            proposerId = recObject[2].toString();
                            proposerStatus = recObject[3].toString();

                            
                            if (proposerStatus.equals("0")) {
                                requestStatusOpt = "<a class=\"btn btn-primary btn-sm text-white\" ><i class=\"fa fa-undo\"></i> Pending</a>";
                                        

                            }

                            if (proposerStatus.equals("1")) {
                                requestStatusOpt = "<a class=\"btn btn-success btn-sm text-white\" ><i class=\"fa fa-check-circle\"></i> Approved</a>";

                            }

                            if (proposerStatus.equals("2")) {
                                requestStatusOpt = "<a class=\"btn btn-danger btn-sm text-white\" ><i class=\"fa fa-times-circle\"></i> Declined</a>";

                            }

                            //   proposerId1 = Integer.parseInt(recObject[2].toString());
                            // proposerId2 = Integer.parseInt(recObject[3].toString());
                            //  proposerId3 = Integer.parseInt(recObject[4].toString());
                            //    proposerId3 = recObject[4] == null ? 0 : Integer.parseInt(recObject[4].toString());
                            //   recMemSQL = dbsession.createSQLQuery("SELECT * FROM member WHERE id IN (" + proposerId1 + "," + proposerId2 + "," + proposerId3 + ") ORDER BY FIELD(id, " + proposerId1 + "," + proposerId2 + "," + proposerId3 + ")");
                            recMemSQL = dbsession.createSQLQuery("SELECT * FROM member WHERE id = '" + proposerId + "'");

                            for (Iterator recMemItr = recMemSQL.list().iterator(); recMemItr.hasNext();) {
                                recMemObject = (Object[]) recMemItr.next();
                                recMemberId = recMemObject[1].toString();
                                recMemberName = recMemObject[2].toString();
                                if (r == 1) {
                                    recMemberStr1 = "<p class=\"font-weight-bold\">" + recMemberName + "</p><p class=\"font-weight-bold\">" + recMemberId + "</p>"+ requestStatusOpt  +"";
                                }
                                if (r == 2) {
                                    recMemberStr2 = "<p class=\"font-weight-bold\">" + recMemberName + "</p><p class=\"font-weight-bold\">" + recMemberId + "</p>"+ requestStatusOpt  +"";
                                }
                                if (r == 3) {
                                    recMemberStr3 = "<p class=\"font-weight-bold\">" + recMemberName + "</p><p class=\"font-weight-bold\">" + recMemberId + "</p>"+ requestStatusOpt  +"";
                                }

                            }
                            r++;
                        }
                    %>

                    <div class="row">
                        <div class="col-4">
                            <p class="m-2 text-center text-dark font-weight-bold">Proposer</p>

                            <div class="form-row">
                                <div class="col-12 text-center">
                                    <%=recMemberStr1%>
                                </div>                                                    
                            </div>    

                        </div>
                        <div class="col-4">
                            <p class="m-2 text-center text-dark font-weight-bold">Seconder |</p>
                            <div class="form-row">
                                <div class="col-12 text-center">
                                    <%=recMemberStr2%>
                                </div>                                                    
                            </div> 


                        </div>
                        <div class="col-4">
                            <p class="m-2 text-center text-dark font-weight-bold">Seconder ||</p>
                            <div class="form-row">
                                <div class="col-12 text-center">
                                    <%=recMemberStr3%>
                                </div>                                                    
                            </div> 


                        </div>
                    </div>






                    <!-- ================ End billing information form ================= -->

                </div>
            </div>
        </div>
    </div>
</div>

<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.jquery.js"></script>
<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.js"></script>
<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/init.js"></script>

<%
    dbsession.clear();
    dbsession.close();
%>

<%@ include file="../footer.jsp" %>
