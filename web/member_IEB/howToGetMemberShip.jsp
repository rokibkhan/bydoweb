<%@ include file="../header.jsp" %>


<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="about_banner_membership">
    <h2 class="display_41_saz text-white">how to get membership</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Membership</a></li>
                <li class="breadcrumb-item active" aria-current="page">How to get Membership</li>
            </ol>
        </nav>
    </div>
</div>

<section class="news-single-content pt-0">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="news-content how_get_member_rules">
                    <p>
                        <a target="_blank" href="<%=GlobalVariable.baseUrl%>/member/registration.jsp">Online Membership Application</a> or you have to follow the steps:
                    </p><br>

                    <ul class="getting_membership_rules_saz">
                        <li><a target="_blank" href="<%=GlobalVariable.baseUrl%>/member/memberShipForm.jsp">Download the form</a> or collect the form from IEB office</li>
                        <li>Fill up that form</li>
                        <li>Submit that to IEB Membership section along with the documents you need to bring which are mentioned in the form. If you are living outside Bangladesh please contact with membership section (<a href="http://www.iebbd.org/" target="_blank">membership@iebbd.org</a>)</li>
                    </ul>

                    <p class="about_fees_saz"><a target="_blank" href="<%=GlobalVariable.baseUrl%>/member/aboutMemberShipFee.jsp">Click here to Know about Fees</a></p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="news_cont_saz">
                    <ul class="membership_panel_saz">
                        <li><p>Membership</p></li>
                        <li><a href="<%=GlobalVariable.baseUrl%>/member/aboutMemberShip.jsp">What is IEB Membership</a></li>
                        <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/aboutMemberShipCriteria.jsp">Membership Criteria</a></li>

                        <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/howToGetMemberShip.jsp">How to get Membership</a></li>

                        <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/aboutMemberShipFee.jsp">Membership Fee</a></li>    

                        <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberShipForm.jsp">Membership Form</a></li>

                        <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/accreditation.jsp">Accreditation</a></li>

                        <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/search.jsp">Search your membership No.</a></li>

                        <%
                            if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {
                        %>
                        <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberDashboard.jsp?sessionid=<%=session.getId()%>">My Profile</a></li>

                        <%
                        } else {
                        %>

                        <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/registration.jsp">Apply Membership</a></li>

                        <% } %>
                        <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/registrationCheck.jsp">Status of Application </a></li>


                    </ul>
                </div>
            </div><br><br>
        </div>
    </div>
</section>









<%@ include file="../footer.jsp" %>
