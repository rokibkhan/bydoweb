<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

    } else {

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String reqMemId = request.getParameter("reqMemId") == null ? "" : request.getParameter("reqMemId").trim();

    Query approvalSQL = null;
    Object objApprova[] = null;
    String requestId = "";
    String requestMemId = "";
    String requestStatus = "";
    String requestStatusOpt = "";
    String btnApprovDeclineBack = "";

    approvalSQL = dbsession.createSQLQuery("SELECT pt.id,pt.member_id tempMemberId,pt.proposer_id,pt.status,pt.request_date "
            + "FROM member_proposer_info_temp pt "
            + "WHERE pt.member_id = '" + reqMemId + "' AND pt.proposer_id=" + memberIdH + " ");

    if (!approvalSQL.list().isEmpty()) {

        for (Iterator itr1 = approvalSQL.list().iterator(); itr1.hasNext();) {

            objApprova = (Object[]) itr1.next();
            requestId = objApprova[0].toString();
            requestStatus = objApprova[3].toString();

            if (requestStatus.equals("0")) {
                btnApprovDeclineBack = "<a href=\"" + GlobalVariable.baseUrl + "/member/memberShipApprovalRequestList.jsp?sessionid=" + session.getId() + "\" title=\"Back Request List\" class=\"btn btn-warning btn-sm text-white\" ><i class=\"fa fa-arrow-circle-left\"></i> Back</a> &nbsp;"
                        + "<a onclick=\"requestMemberApprovalByProposer('" + session.getId() + "','" + requestId + "')\" title=\"Request Appreve\"class=\"btn btn-primary btn-sm text-white\" ><i class=\"fa fa-check-circle\"></i> Approve</a> &nbsp;"
                        + "<a title=\"Request Decline\"class=\"btn btn-danger btn-sm text-white\" ><i class=\"fa fa-times-circle\"></i> Decline</a>";

            }

            if (requestStatus.equals("1")) {
                btnApprovDeclineBack = "<a class=\"btn btn-primary btn-sm text-white\" ><i class=\"fa fa-check-circle\"></i> Approved</a>";

            }

            if (requestStatus.equals("2")) {
                btnApprovDeclineBack = "<a class=\"btn btn-danger btn-sm text-white\" ><i class=\"fa fa-times-circle\"></i> Declined</a>";

            }

        }
    }

    System.out.println("Req Id::" + reqMemId);

    Query reqMemQuery = null;
    Object[] reqMemObject = null;

    Query reqMemMailingAddrQuery = null;
    Object[] reqMemMailingAddrObject = null;

    Query reqMemParmanentAddrQuery = null;
    Object[] reqMemParmanentAddrObject = null;

    String regId = "";
    String memberName = "";
    String fatherName = "";
    String motherName = "";
    String datefBirth = "";
    String placeOfBirth = "";
    String gender = "";
    String gender1 = "";
    String age = "";
    String nationality = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String mobileNumber = "";
    String userEmail = "";
    String centerId = "";
    String pictureLink = "";
    String mAddressLine1 = "";
    String mAddressLine2 = "";
    String pAddressLine1 = "";
    String pAddressLine2 = "";
    String membershipApplyingFor = "";
    String presentIEBmembershipNumber = "";

    reqMemQuery = dbsession.createSQLQuery("select * from member_temp WHERE id= " + reqMemId + "");
    for (Iterator itr2 = reqMemQuery.list().iterator(); itr2.hasNext();) {
        reqMemObject = (Object[]) itr2.next();
        regId = reqMemObject[2].toString();
        memberName = reqMemObject[4].toString();
        fatherName = reqMemObject[5] == null ? "" : reqMemObject[5].toString();
        motherName = reqMemObject[6] == null ? "" : reqMemObject[6].toString();

        placeOfBirth = reqMemObject[7].toString();
        datefBirth = reqMemObject[8].toString();
        age = reqMemObject[9] == null ? "" : reqMemObject[9].toString();

        nationality = reqMemObject[10] == null ? "" : reqMemObject[10].toString();

        gender1 = reqMemObject[11].toString();
        if (gender1.equals("M")) {
            gender = "Male";
        } else {
            gender = "Female";
        }
        membershipApplyingFor = reqMemObject[12] == null ? "" : reqMemObject[12].toString();
        presentIEBmembershipNumber = reqMemObject[13] == null ? "" : reqMemObject[13].toString();

        phone1 = reqMemObject[14] == null ? "" : reqMemObject[14].toString();
        phone2 = reqMemObject[15] == null ? "" : reqMemObject[15].toString();
        mobileNumber = reqMemObject[17] == null ? "" : reqMemObject[17].toString();
        userEmail = reqMemObject[18] == null ? "" : reqMemObject[18].toString();

    }

    reqMemMailingAddrQuery = dbsession.createSQLQuery("select *from address_book WHERE id=(select address_id from member_address_temp where member_id= " + reqMemId + " and address_Type='M' ) ");
    for (Iterator itr2 = reqMemMailingAddrQuery.list().iterator(); itr2.hasNext();) {
        reqMemMailingAddrObject = (Object[]) itr2.next();
        mAddressLine1 = reqMemMailingAddrObject[1].toString();
        mAddressLine2 = reqMemMailingAddrObject[2].toString();

    }
    reqMemParmanentAddrQuery = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address_temp where member_id= " + reqMemId + " and address_Type='P' ) ");
    for (Iterator itr3 = reqMemParmanentAddrQuery.list().iterator(); itr3.hasNext();) {

        reqMemParmanentAddrObject = (Object[]) itr3.next();
        pAddressLine1 = reqMemParmanentAddrObject[1].toString();
        pAddressLine2 = reqMemParmanentAddrObject[2].toString();

    }

    Query reqMemEducationQuery = null;
    Object[] reqMemEducationObject = null;

    Query reqMemEducationQuery1 = null;
    Object[] reqMemEducationObject1 = null;

    String reqEduId = "";
    String reqEduName = "";
    String reqEduDegreeTypeId = "";
    String reqEduInstituteName = "";
    String reqEduBoardUniversity = "";
    String reqYearPassing = "";
    String reqEduYearPassing = "";
    String reqEduResultTypeId = "";
    String reqEduResult = "";

    /*

    int degreeId = 0;
    String degreeName = "";
    String instituteName = "";
    String boardUniversityName = "";
    java.util.Date dob = new Date();
    String yearOfPassing = "";
    String resultTypeName = "";
    String result = "";

    MemberEducationInfo mei = null;

    reqMemEducationQuery = dbsession.createQuery("from MemberEducationInfo where  member_id=" + memberIdH + " ");

    Member member = null;
    String memberSubdivisionName = "";
    String memberSubdivisionFullName = "";

    for (Iterator itrEdi = reqMemEducationQuery.list().iterator(); itrEdi.hasNext();) {
        mei = (MemberEducationInfo) itrEdi.next();

        degreeId = mei.getDegree().getDegreeId();
        degreeName = mei.getDegree().getDegreeName();

        instituteName = mei.getInstituteName() == null ? "" : mei.getInstituteName();

        boardUniversityName = mei.getUniversity().getUniversityLongName();
        yearOfPassing = mei.getYearOfPassing();
        resultTypeName = mei.getResultType().getResultTypeName();
        result = mei.getResult();

        if (degreeId == 2 || degreeId == 3) {

            reqMemEducationQuery1 = dbsession.createQuery("from MemberTemp as member WHERE id=" + reqMemId + " ");

            for (Iterator itr2 = reqMemEducationQuery1.list().iterator(); itr2.hasNext();) {
                member = (Member) itr2.next();

                memberSubdivisionName = member.getSubDivision().getSubDivisionName();
                memberSubdivisionFullName = member.getSubDivision().getFullName();
            }
        }

        degreeName = degreeName + memberSubdivisionFullName;
        
    }
    
     */
    

%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Membership Approval Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;

                        if (!strMsg.equals(
                                "")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->

                    <p class="content_title_saz pb-2 m-3">Personal Information </p>
                    <div class="row">
                        <div class="col-sm-8">
                            <form class="profile_form_horizontal_saz p_form">
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Reg ID</label>
                                    <div class="col-sm-9"><p><%=regId%></p></div>
                                </div>

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Name</label>
                                    <div class="col-sm-9"><p><%=memberName%></p></div>
                                </div> 
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Father's Name</label>
                                    <div class="col-sm-9"><p><%=fatherName%></p></div>
                                </div> 
                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Mother's Name</label>
                                    <div class="col-sm-9"><p><%=motherName%></p></div>
                                </div>

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Email</label>
                                    <div class="col-sm-9"><p><%=userEmail%></p></div>
                                </div>

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Mobile</label>
                                    <div class="col-sm-9"><p><%=mobileNumber%></p></div>
                                </div>


                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">Gender</label>
                                    <div class="col-sm-9"><p><%=gender%></p></div>
                                </div>


                            </form>
                        </div>
                        <div class="col-sm-4">
                            <img style="width: 150px;" src="<%out.print(GlobalVariable.baseUrl);%>/upload/no_image.png" alt="<%=memberName%>"/>

                        </div>
                    </div>
                    <!-- ============ End personal information form ================== -->


                    <%
                        Query madrSQL = null;
                        Object[] madrObject = null;

                        Query padrSQL = null;
                        Object[] padrObject = null;

                        int mAddressId = 0;
                        String mCountryId = "";
                        String mDistrictId = "";
                        String mThanaId = "";
                        String mThanaName = "";
                        String mThanaDistrictName = "";
                        String mZipOffice = "";
                        String mZipCode = "";

                        int pAddressId = 0;
                        String pCountryId = "";
                        String pDistrictId = "";
                        String pThanaId = "";
                        String pThanaName = "";
                        String pThanaDistrictName = "";
                        String pZipOffice = "";
                        String pZipCode = "";

                        String mAddressStr = "";
                        String pAddressStr = "";

                        Thana thana = null;

                        Query mailAddrSQL = dbsession.createSQLQuery("select *from address_book WHERE id=(select address_id from member_address_temp where member_id= " + reqMemId + " and address_Type='M' ) ");
                        for (Iterator itr2 = mailAddrSQL.list().iterator(); itr2.hasNext();) {
                            madrObject = (Object[]) itr2.next();
                            mAddressLine1 = madrObject[1].toString();
                            mAddressLine2 = madrObject[2].toString();
                            mThanaId = madrObject[3].toString();

                            Query mThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
                            for (Iterator itrmThana = mThanaSQL.list().iterator(); itrmThana.hasNext();) {
                                thana = (Thana) itrmThana.next();

                                mThanaName = thana.getThanaName();
                                mThanaDistrictName = thana.getDistrict().getDistrictName();
                            }

                            mZipCode = madrObject[4].toString();
                            mZipOffice = madrObject[8].toString();

                            mAddressStr = mAddressLine1 + "," + mAddressLine2 + "<br>" + mThanaName + "," + mThanaDistrictName + "<br>" + mZipOffice + "-" + mZipCode;

                        }
                        Query permAddrSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address_temp where member_id= " + reqMemId + " and address_Type='P' ) ");
                        for (Iterator itr3 = permAddrSQL.list().iterator(); itr3.hasNext();) {

                            padrObject = (Object[]) itr3.next();
                            pAddressLine1 = padrObject[1].toString();
                            pAddressLine2 = padrObject[2].toString();
                            pThanaId = padrObject[3].toString();

                            Query pThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
                            for (Iterator itrpThana = pThanaSQL.list().iterator(); itrpThana.hasNext();) {
                                thana = (Thana) itrpThana.next();

                                pThanaName = thana.getThanaName();
                                pThanaDistrictName = thana.getDistrict().getDistrictName();
                            }

                            pZipCode = padrObject[4].toString();
                            pZipOffice = padrObject[8].toString();

                            pAddressStr = pAddressLine1 + "," + pAddressLine2 + "<br>" + pThanaName + "," + pThanaDistrictName + "<br>" + pZipOffice + "-" + pZipCode;

                        }

                    %>



                    <p class="content_title_saz pb-2 m-3">Mailing Address </p>

                    <form class=" profile_form_horizontal_saz">

                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2 ml-3 font-weight-bold">Address</label>
                            <div class="col-sm-9">
                                <p><%=mAddressStr%>

                                </p>
                            </div>
                        </div>


                    </form>
                    <!-- ================ End billing information form ================= -->

                    <p class="content_title_saz pb-2 m-3">Permanent Address </p>

                    <form class=" profile_form_horizontal_saz">

                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2 ml-3 font-weight-bold">Address</label>
                            <div class="col-sm-9"><p><%=pAddressStr%></p></div>
                        </div>



                    </form>

                    <p id="acttionBox<%=requestId%>" class="text-center"><%=btnApprovDeclineBack%></p>
                    
                    <br>


                </div>
            </div>
        </div>
    </div>
</div>

<br/>                        
<%
    dbsession.clear();
    dbsession.close();

%>

<%@ include file="../footer.jsp" %>
