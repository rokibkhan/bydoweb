<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String registerAppId = "";
    String memberRegId = "";
    if (session.getAttribute("registerAppId") != null && session.getAttribute("memberRegId") != null) {

        sessionIdH = session.getId();
        registerAppId = session.getAttribute("registerAppId").toString();
        memberRegId = session.getAttribute("memberRegId").toString();

    } else {

        response.sendRedirect(GlobalVariable.baseUrl + "/member/registrationCheck.jsp");
        return;
    }


    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    String organizationName = "";
    String organizationType = "";
    String organizationAddress = "";
    String designationName = "";
    String fromDate = "";
    String toDate = "";
    java.util.Date dob = new Date();

    String result = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String centerId = "";
    String pictureLink = "";
    String mAddressLine1 = "";
    String mAddressLine2 = "";
    String pAddressLine1 = "";
    String pAddressLine2 = "";
    String startDate = "";
    String tillDate = "";
    int profInfoId = 0;
    MemberProfessionalInfoTemp mpi = null;

    


%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="registrationLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Professional Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;

                        if (!strMsg.equals(
                                "")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->

                    <p class="content_title_saz pb-2 m-3">Professional Record</p>

                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                        <thead>
                            <tr>
                                <th scope="col">Name of The Organization</th>
                                <th scope="col">Designation</th>
                                <th scope="col">From Date</th>
                                <th scope="col">To Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%

                                q1 = dbsession.createQuery("from MemberProfessionalInfoTemp WHERE  member_id=" + registerAppId + " ");

                                for (Iterator itr = q1.list().iterator();
                                        itr.hasNext();) {
                                    mpi = (MemberProfessionalInfoTemp) itr.next();
                                    profInfoId = mpi.getId();
                                    organizationName = mpi.getCompanyName() == null ? "" : mpi.getCompanyName();

                                    organizationType = mpi.getCompanyType() == null ? "" : mpi.getCompanyType();
                                    designationName = mpi.getDesignation() == null ? "" : mpi.getDesignation();
                                    startDate = mpi.getFromDate().toString() == null ? "" : mpi.getFromDate().toString();
                                    tillDate = mpi.getTillDate().toString() == null ? "" : mpi.getTillDate().toString();
                                    organizationAddress = mpi.getCompanyAddress() == null ? "" : mpi.getCompanyAddress();


                            %>
                            <tr>
                                <td><%=organizationName%></td>
                                <td><%=designationName%></td> 
                                <td><%=startDate%></td>
                                <td><%=tillDate%></td>
                                
                            </tr>

                            <%                                }

                                dbsession.flush();
                                dbsession.close();


                            %>
                        </tbody>
                    </table>


                    <!-- ================ End billing information form ================= -->

                </div>
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
