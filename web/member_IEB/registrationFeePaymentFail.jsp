<%@page import="com.ssl.commerz.parametermappings.SSLCommerzValidatorResponse"%>
<%@page import="com.ssl.commerz.TransactionResponseValidator"%>
<%@page import="com.ssl.commerz.SSLCommerz"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);

    }

    System.out.println("Header sessionIdH :: " + sessionIdH);
    System.out.println("Header userNameH :: " + userNameH);
    System.out.println("Header userStoreIdH :: " + memberIdH);

//    String sessionid = request.getParameter("sessionid").trim();
//
//    sessionid = session.getId();
//    String uid = session.getAttribute("username").toString().toUpperCase();    
//    String userStoreId = session.getAttribute("memberId").toString();
//    String sessionidH = request.getParameter("sessionid").trim();
//
//    sessionidH = session.getId();
//    String uidH = session.getAttribute("username").toString().toUpperCase();    
    //   String userStoreIdH = session.getAttribute("memberId").toString();
//System.out.println("uidH :: "+uidH);
    String regMemberTempId = request.getParameter("regTempId") == null ? "" : request.getParameter("regTempId").trim();
    String regMemId = request.getParameter("regMemId") == null ? "" : request.getParameter("regMemId").trim();
    String regStep = request.getParameter("regStep") == null ? "" : request.getParameter("regStep").trim();
    String transId = request.getParameter("transId") == null ? "" : request.getParameter("transId").trim();

    String verify_sign = request.getParameter("verify_sign");

    System.out.println("verify_sign :: " + verify_sign);

    //request.getParameterNames();
    //  response.sendRedirect(GlobalVariable.baseUrl + "/userManagement/login.jsp");
    //   response.sendRedirect("registrationFeePaymentOption.jsp?regTempId=" + memberRegId + "&regMemId=" + memberVerifyId + "&regStep=11&strMsg=" + strMsg + "#section9");
//    String strMsgSuccess = "IEB membership registration fee transaction successfully completed";
//    String successPageUrl = GlobalVariable.baseUrl + "/member/registrationFeePaymentSuccess.jsp?sessionid=" + session.getId() + "&regTempId=" + regMemberTempId + "&regMemId=" + regMemId + "&regStep=15&transId=" + transId + "&strMsg=" + strMsgSuccess + "#section15";
//
//    String strMsFail = "IEB membership registration fee transaction fail";
//    String failPageUrl = GlobalVariable.baseUrl + "/member/registrationFeePaymentFail.jsp?sessionid=" + session.getId() + "&regTempId=" + regMemberTempId + "&regMemId=" + regMemId + "&regStep=15&transId=" + transId + "&strMsg=" + strMsFail + "#section9";
//
//    String strMsCancel = "IEB membership registration fee transaction fail";
//    String cancelPageUrl = GlobalVariable.baseUrl + "/member/registrationFeePaymentCancel.jsp?sessionid=" + session.getId() + "&regTempId=" + regMemberTempId + "&regMemId=" + regMemId + "&regStep=15&transId=" + transId + "&strMsg=" + strMsCancel + "#section9";
    //  out.print("<table width = \"100%\" border = \"1\" align = \"center\"><tr bgcolor = \"#949494\"><th>Param Name</th><th>Param Value(s)</th></tr>");
    Enumeration paramNames = request.getParameterNames();
    Map<String, String> postData = new HashMap<String, String>();
    while (paramNames.hasMoreElements()) {
        String paramName = (String) paramNames.nextElement();
        //  out.print("<tr><td>" + paramName + "</td>\n");
        //   String paramValue = request.getHeader(paramName);
        String paramValue = request.getParameter(paramName);
        //   out.println("<td> " + paramValue + "</td></tr>\n");

        postData.put(paramName, paramValue);
    }
    //   out.print("</table>");

//    if (regMemberTempId.equals("")) {
//
//        response.sendRedirect(GlobalVariable.baseUrl + "/member/registration.jsp");
//
//    }
    String showTabOption;
    if (regStep.equals("15")) {
        showTabOption = " show";

    } else {
        showTabOption = "";
    }

    Query q1 = null;
    Object[] object = null;

    String memberName = "";
    String fatherName = "";
    String motherName = "";
    String datefBirth = "";
    String placeOfBirth = "";
    String gender = "";
    String gender1 = "";
    String age = "";
    String nationality = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String mobileNumber = "";
    String userEmail = "";
    String centerId = "";
    String pictureName = "";
    String pictureLink = "";
    String mAddressLine1 = "";
    String mAddressLine2 = "";
    String pAddressLine1 = "";
    String pAddressLine2 = "";
    String membershipApplyingFor = "";
    String membershipApplyingFor1 = "";
    String presentIEBmembershipNumber = "";
    String divisionId = "";
    String subDivisionId = "";
    String memberTempPass = "";

    q1 = dbsession.createSQLQuery("select * from member_temp WHERE id= " + regMemId + "");
    for (Iterator itr2 = q1.list().iterator(); itr2.hasNext();) {
        object = (Object[]) itr2.next();
        memberName = object[4].toString();
        fatherName = object[5] == null ? "" : object[5].toString();
        motherName = object[6] == null ? "" : object[6].toString();
        placeOfBirth = object[7].toString();
        datefBirth = object[8].toString();
        age = object[9] == null ? "" : object[9].toString();
        nationality = object[10] == null ? "" : object[10].toString();
        gender1 = object[11].toString();
        if (gender1.equals("M")) {
            gender = "Male";
        } else {
            gender = "Female";
        }
        membershipApplyingFor1 = object[13] == null ? "" : object[13].toString();
        if (membershipApplyingFor1.equals("1")) {
            membershipApplyingFor = " Fellow ";
        } else if (membershipApplyingFor1.equals("2")) {
            membershipApplyingFor = "Member";
        } else {
            membershipApplyingFor = "Associate Member";
        }
        presentIEBmembershipNumber = object[14] == null ? "" : object[14].toString();

        phone1 = object[15] == null ? "" : object[15].toString();
        phone2 = object[16] == null ? "" : object[16].toString();
        mobileNumber = object[17] == null ? "" : object[17].toString();
        userEmail = object[18].toString();
        pictureName = object[25].toString();

        pictureLink = "<img src=\"" + GlobalVariable.imageMemberDirLink + pictureName + "\" alt=\"" + memberName + "\" class=\"img-fluid img_1\">";

        divisionId = object[24].toString() == null ? "" : object[24].toString();
        // subDivisionId = object[28].toString() == null ? "" : object[28].toString();

        memberTempPass = object[3].toString() == null ? "" : object[3].toString();

    }

    Query madrSQL = null;
    Object[] madrObject = null;

    Query padrSQL = null;
    Object[] padrObject = null;

    int mAddressId = 0;
    String mCountryId = "";
    String mDistrictId = "";
    String mThanaId = "";
    String mThanaName = "";
    String mThanaDistrictName = "";
    String mZipOffice = "";
    String mZipCode = "";

    int pAddressId = 0;
    String pCountryId = "";
    String pDistrictId = "";
    String pThanaId = "";
    String pThanaName = "";
    String pThanaDistrictName = "";
    String pZipOffice = "";
    String pZipCode = "";

    String mAddressStr = "";
    String pAddressStr = "";

    Thana thana = null;

    String membershipCusCountry = "Bangladesh";

    Query mailAddrSQL = dbsession.createSQLQuery("select *from address_book WHERE id=(select address_id from member_address_temp where member_id= " + regMemId + " and address_Type='M' ) ");
    for (Iterator itr2 = mailAddrSQL.list().iterator(); itr2.hasNext();) {
        object = (Object[]) itr2.next();
        mAddressLine1 = object[1].toString();
        mAddressLine2 = object[2].toString();
        mThanaId = object[3].toString();

        Query mThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
        for (Iterator itrmThana = mThanaSQL.list().iterator(); itrmThana.hasNext();) {
            thana = (Thana) itrmThana.next();

            mThanaName = thana.getThanaName();
            mThanaDistrictName = thana.getDistrict().getDistrictName();
        }

        mZipCode = object[4].toString();
        mZipOffice = object[8].toString();

        mAddressStr = mAddressLine1 + "," + mAddressLine2 + "<br>" + mThanaName + "," + mThanaDistrictName + "<br>" + mZipOffice + "-" + mZipCode;

    }
    Query permAddrSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address_temp where member_id= " + regMemId + " and address_Type='P' ) ");
    for (Iterator itr3 = permAddrSQL.list().iterator(); itr3.hasNext();) {

        object = (Object[]) itr3.next();
        pAddressLine1 = object[1].toString();
        pAddressLine2 = object[2].toString();
        pThanaId = object[3].toString();

        Query pThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
        for (Iterator itrpThana = pThanaSQL.list().iterator(); itrpThana.hasNext();) {
            thana = (Thana) itrpThana.next();

            pThanaName = thana.getThanaName();
            pThanaDistrictName = thana.getDistrict().getDistrictName();
        }

        pZipCode = object[4].toString();
        pZipOffice = object[8].toString();

        pAddressStr = pAddressLine1 + "," + pAddressLine2 + "<br>" + pThanaName + "," + pThanaDistrictName + "<br>" + pZipOffice + "-" + pZipCode;

    }

    Query feeSQL = null;
    Object[] feeObject = null;

    String memberShipFeeTransId = "";
    String memberShipFeeMemberId = "";
    String memberShipFeePaidDate = "";
    String memberShipFeeAmount = "";
    String memberShipFeeCurrency = "BDT";

    // feeSQL = dbsession.createSQLQuery("select * from member_fee_temp WHERE txn_id= " + transId + " AND status = 0");
    feeSQL = dbsession.createSQLQuery("select * from member_fee_temp WHERE txn_id= " + transId + " AND status = 0");
    //  if (!feeSQL.list().isEmpty()) {
    for (Iterator feeItr = feeSQL.list().iterator(); feeItr.hasNext();) {
        feeObject = (Object[]) feeItr.next();
        memberShipFeeTransId = feeObject[0].toString();
        memberShipFeeMemberId = feeObject[1].toString();
        memberShipFeePaidDate = feeObject[5].toString();
        memberShipFeeAmount = feeObject[7].toString();

        System.out.println("memberShipFeeTransId :: " + memberShipFeeTransId);
        System.out.println("memberShipFeeMemberId :: " + memberShipFeeMemberId);
        System.out.println("memberShipFeePaidDate :: " + memberShipFeePaidDate);
        System.out.println("memberShipFeeAmount :: " + memberShipFeeAmount);

    }
    //   }

    // com.ssl.commerz.TransactionResponseValidator.receiveSuccessResponse
//check transactionId from SSL Commerce
    /**
     * If following order validation returns true, then process transaction as
     * success. if this following validation returns false , then query status
     * if failed of canceled. Check request.get("status") for this purpose
     */
    //  TransactionResponseValidator transactionValidationResponse = new TransactionResponseValidator();
    //   boolean transactionValidationResponseBol = transactionValidationResponse.receiveSuccessResponse(postData);
    //   System.out.println("transactionValidationResponseVVV::" + transactionValidationResponseBol);
    // SSLCommerz sslcz = new SSLCommerz(GlobalVariable.SSLCommerzStoreId, GlobalVariable.SSLCommerzStorePass, Boolean.parseBoolean(GlobalVariable.SSLCommerzStoreMode));
    //  boolean transactionValidationResponseBol = sslcz.orderValidate(memberShipFeeTransId, memberShipFeeAmount, memberShipFeeCurrency, postData);
    //   System.out.println("transactionValidationResponseVVV::" + transactionValidationResponseBol);
    //update  fee table Status and ref_no, ref_description
    String tranId = request.getParameter("tran_id");
    String errorMsg = request.getParameter("error");
    String cardType = request.getParameter("card_type");
    String cardNo = request.getParameter("card_no");
    String bankTransactionID = request.getParameter("bank_tran_id");
    String cardIssuerBank = request.getParameter("card_issuer");
    String cardBrand = request.getParameter("card_brand");
    String cardBrandTranDate = request.getParameter("tran_date");
    String cardBrandTranCurrencyType = request.getParameter("currency_type");
    String cardBrandTranCurrencyAmount = request.getParameter("currency_amount");

    String ref_description = "tranId::" + tranId + " ::errorMsg::" + errorMsg + " ::cardType::" + cardType + " ::cardNo::" + cardNo + " ::bankTransactionID::" + bankTransactionID + " ::cardIssuerBank::" + cardIssuerBank + " ::cardBrand::" + cardBrand + " ::cardBrandTranDate::" + cardBrandTranDate + " ::cardBrandTranCurrencyAmount::" + cardBrandTranCurrencyAmount + " ::cardBrandTranCurrencyType::" + cardBrandTranCurrencyType;
    String ref_no = bankTransactionID;
    String status = "2";

    Query feeUpdSQL = dbsession.createSQLQuery("UPDATE  member_fee_temp SET ref_no='" + ref_no + "',ref_description ='" + ref_description + "',mod_user ='" + regMemId + "',mod_date=now(),status='" + status + "' WHERE txn_id='" + transId + "'");

    feeUpdSQL.executeUpdate();
    dbtrx.commit();

//        if (dbtrx.wasCommitted()) {
//
//        } else {
//            //update Error
//        }
    //error
    String strMsgPay = "Error!!! When Payment transaction fail.Please check and try again.";

    response.sendRedirect(GlobalVariable.baseUrl + "/member/registrationFeePaymentOption.jsp?regTempId=" + regMemberTempId + "&regMemId=" + regMemId + "&regStep=11&strMsg=" + strMsgPay + "#section9");

    dbsession.flush();
    dbsession.close();
%>