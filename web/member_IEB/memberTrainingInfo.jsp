<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    String instituteName = "";
    String courseTitle = "";
    String trainingTitle = "";
    java.util.Date dob = new Date();
    String yearOfPassing = "";
    String weeks = "";
    int trainingId = 0;

    MemberTrainingInfo mei = null;



%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>

<script type="text/javascript">
    function trainingDeleteInfo(arg1, arg2) {

        console.log("trainingDeleteInfo -> arg1 :: " + arg1 + " arg2:: " + arg2);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnDeleteComfirmation\" onclick=\"trainingDeleteInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";


        memberLGModal = $('#memberLGModal');
        //   memberLGModal = $('#taskLGModal');
        memberLGModal.find("#memberLGModalTitle").text("Training Delete Confirmation");
        memberLGModal.find("#memberLGModalBody").html("");
        memberLGModal.find("#memberLGModalFooter").html(btnInvInfo);
        memberLGModal.modal('show');


    }



    function trainingDeleteInfoConfirm(arg1, arg2) {
        console.log("trainingDeleteInfoConfirm:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("trainingDeleteProcess.jsp", {deleteId: arg1, sessionid: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#infoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                memberLGModal.modal('hide');
            } else {

            }


        }, "json");

    }

</script>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Training Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->

                    <p class="content_title_saz pb-2 m-3">Training Information</p>

                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                        <thead>
                            <tr>
                                <th scope="col">Training Institute with Country</th>
                                <th scope="col">Training Title</th>
                                <th scope="col">Training Area</th>
                                <th scope="col">Duration (Week)</th>
                                <th scope="col">Training Year</th>
                                <th scope="col">Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%

                                q1 = dbsession.createQuery("from MemberTrainingInfo where  member_id=" + memberIdH + " ");

                                for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                                    mei = (MemberTrainingInfo) itr.next();

                                    trainingId = mei.getId();
                                    instituteName = mei.getMemberTrainingInstitute() == null ? "" : mei.getMemberTrainingInstitute();
                                    trainingTitle = mei.getMemberTrainingTitle() == null ? "" : mei.getMemberTrainingTitle();
                                    courseTitle = mei.getMemberTrainingCourse() == null ? "" : mei.getMemberTrainingCourse();
                                    yearOfPassing = mei.getMemberTrainingYear() == null ? "" : mei.getMemberTrainingYear();
                                    weeks = mei.getMemberTrainingDuration() == null ? "" : mei.getMemberTrainingDuration();


                            %>
                            <tr id="infoBox<%=trainingId%>">
                                <td><%=instituteName%></td>
                                <td><%=trainingTitle%></td>
                                <td><%=courseTitle%></td>
                                <td><%=weeks%></td>
                                <td><%=yearOfPassing%></td>
                                <td style="cursor:pointer">

                                    <a title="Edit" href="<%out.print(GlobalVariable.baseUrl);%>/member/memberTrainingInfoEdit.jsp?sessionid=<%=session.getId()%>&act=add&trainingId=<%=trainingId%>"  ><i class="fa fa-edit"></i> </a> 

                                    <a title="Remove" onclick="trainingDeleteInfo(<% out.print("'" + trainingId + "','" + sessionIdH + "'");%>)" ><i class="fa fa-trash"></i> </a> 


                            </tr>

                            <%                                }
                                dbsession.flush();
                                dbsession.close();

                            %>
                        </tbody>
                    </table>


                    <!-- ============ End Education information Table ================== -->

                    <div class="container">
                        <div class="row my-4">
                            <div class="col-12">
                                <div class="card-header panel_heading_saz" style="padding-top: 4px; padding-bottom: 4px;">
                                    <i class="fa fa-list-alt"></i> Add Training Information
                                </div>
                            </div>

                        </div>
                    </div>

                    <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberTrainingInfoSubmitData.jsp?sessionid=<%=session.getId()%>&act=add" onSubmit="return fromDataSubmitValidation()">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label text-right">*Training Title :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="trainingTitle" name="trainingTitle" placeholder="Training Title" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label text-right">*Training Institute with Country :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="trainingInstitute" name="trainingInstitute" placeholder="Training Institute with Country" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-4 col-form-label text-right">*Training Area :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="trainingCourse" name="trainingCourse" placeholder="Name of Topic/Course" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label text-right">*Duration :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="trainingDuration" name="trainingDuration" placeholder="Duration" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label text-right">*Training Year :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="trainingYear" name="trainingYear"  placeholder="Training Year" required="">
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-sm-6 offset-sm-4">
                                <button type="submit" class="btn btn-primary mr-3">Submit</button>
                                <button type="button" class="btn btn-primary">Reset</button>
                            </div>
                        </div>



                    </form>
                    <!-- ================ End billing information form ================= -->

                </div>
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
