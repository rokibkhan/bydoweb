<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    Query q2 = null;
    String instituteName = "";
    String boardUniversityName = "";
    java.util.Date dob = new Date();
    String yearOfPassing = "";
    String resultTypeName = "";
    String result = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String centerId = "";
    String pictureLink = "";
    String signatureImageName = "";
    String mAddressLine1 = "";
    String mAddressLine2 = "";
    String pAddressLine1 = "";
    String pAddressLine2 = "";
    MemberEducationInfo mei = null;

    Member member = null;

    Query q3 = null;
    String memberName = "";
    String fatherName = "";
    String motherName = "";
    String memberNo = "";
    String placeOfBirth = "";
    String nationality = "";
    String gender = "";
    String mobileNo = "";
    String userEmail = "";
    AddressBook addressBook = null;
    MemberType memberType = null;
    MemberSignature memberSignature = null;
    String memberType1 = "";

    q1 = dbsession.createQuery("from Member as member WHERE id=" + memberIdH + " ");

    Object[] object = null;

    for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
        member = (Member) itr.next();

        memberName = member.getMemberName();
        fatherName = member.getFatherName() == null ? "" : member.getFatherName();
        motherName = member.getMotherName() == null ? "" : member.getMotherName();

        placeOfBirth = member.getPlaceOfBirth() == null ? "" : member.getPlaceOfBirth();
        dob = member.getDob();
        gender = member.getGender().trim();
        if (gender.equals("1")) {
            gender = "Male";
        } else {
            gender = "Female";
        }
        nationality = member.getCountryCode();
        if (nationality.equalsIgnoreCase("1")) {
            nationality = "Bangladeshi";
        }
        mobileNo = member.getMobile();
        phone1 = member.getPhone1();
        phone2 = member.getPhone2();
        userEmail = member.getEmailId();
        bloodGroup = member.getBloodGroup();
        memberNo = member.getMemberId();

        pictureLink = member.getPictureName();

    }

    q2 = dbsession.createSQLQuery("select *from address_book WHERE id=(select address_id from member_address where member_id= " + memberIdH + " and address_Type='M' ) ");
    for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
        object = (Object[]) itr2.next();
        mAddressLine1 = object[1].toString() == null ? "" : object[1].toString();
        mAddressLine2 = object[2].toString() == null ? "" : object[2].toString();

    }
    q3 = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address where member_id= " + memberIdH + " and address_Type='P' ) ");
    for (Iterator itr3 = q3.list().iterator(); itr3.hasNext();) {

        object = (Object[]) itr3.next();
        pAddressLine1 = object[1].toString() == null ? "" : object[1].toString();
        pAddressLine2 = object[2].toString() == null ? "" : object[2].toString();

    }

    q3 = dbsession.createQuery("from MemberType where member_id= " + memberIdH + " and now() between ed and td ");
    for (Iterator itr3 = q3.list().iterator(); itr3.hasNext();) {

        memberType = (MemberType) itr3.next();
        memberType1 = memberType.getMemberTypeInfo().getMemberTypeName();

    }

    q3 = dbsession.createQuery("from MemberSignature where member_id= " + memberIdH + "  ");
    for (Iterator itr3 = q3.list().iterator(); itr3.hasNext();) {

        memberSignature = (MemberSignature) itr3.next();
        signatureImageName = memberSignature.getSignatureFileName();

    }



%>




<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Profile Preview</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->
                    <div class="container">
                        <div class="row my-5">
                            <div class="col-10">
                                <div class="info text-center">
                                    <h5> <%=memberName%> </h5>
                                    <p><%=mAddressLine1 + " " + mAddressLine2%> </p>
                                    <p>Mob. : <%=mobileNo%> </p>
                                    <p>Email: <%=userEmail%> </p>
                                    <p class="font-weight-bold">Membership Type: <%=memberType1%> (<%=memberNo%>) </p>
                                </div>
                            </div>
                            <div class="col-2">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/upload/member/<%=pictureLink%>" width="150px;" class="float-right img-fluid"/>
                            </div>

                        </div>
                    </div>

                    <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberProfilePrint.jsp?sessionid=<%=session.getId()%>&act=add&memberId=<%=memberIdH%>&pictureName=<%=pictureLink%>&signatureImageName=<%=signatureImageName%>" target="_blank" onSubmit="return fromDataSubmitValidation()">

                        <div class="container">
                            <div class="row my-4">
                                <div class="col-9">
                                    <div class="card-header panel_heading_saz"><i class="fa fa-list-alt"></i> Personal Information</div>
                                </div>
                                <div class="col-3">
                                    <button type="submit" class="btn btn-primary mr-3">Download Profile</button>  

                                </div>
                            </div>
                        </div>

                    </form>



                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <label class="form-control-label col-3 font-weight-bold">Father's Name:</label>
                                    <div class="col-9"><p><%=fatherName%></p></div>
                                </div>
                                <div class="row">
                                    <label class="form-control-label col-3 font-weight-bold">Mother's Name:</label>
                                    <div class="col-9"><p><%=motherName%></p></div>
                                </div>

                                <div class="row">
                                    <label class="form-control-label col-3 font-weight-bold">Nationality :</label>
                                    <div class="col-3"><p><%=nationality%></p></div>
                                    <label class="form-control-label col-3 font-weight-bold">Blood Group :</label>
                                    <div class="col-3"><p><%=bloodGroup%></p></div>
                                </div>
                                <div class="row">
                                    <label class="form-control-label col-3 font-weight-bold">Date of Birth :</label>
                                    <div class="col-3"><p><%=dob%></p></div>
                                    <label class="form-control-label col-3 font-weight-bold">Gender :</label>
                                    <div class="col-3"><p><%=gender%></p></div>
                                </div>

                                <div class="row">
                                    <label class="form-control-label col-12 font-weight-bold">Permanent Address:</label>  
                                    <div class="col-9"><p><%=pAddressLine1 + " " + pAddressLine2%> </p></div>
                                </div>


                            </div>
                        </div>
                    </div>                    
                    <!--.container personal info -->     

                    <div class="container">
                        <div class="row my-4">
                            <div class="col-sm-12">
                                <div class="card-header panel_heading_saz"><i class="fa fa-university"></i> Academic Career</div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="my-3 row">
                            <div class="col-sm-12">
                                <table class="table table-hover table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col">Degree</th>
                                            <th scope="col">Board/University</th>
                                            <th scope="col">Year of Passing</th>
                                            <th scope="col">Score/Class</th> 
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <%
                                            int degreeId = 0;
                                            String degreeName = "";
                                            q1 = dbsession.createQuery("from MemberEducationInfo where  member_id=" + memberIdH + " ");

                                            String memberSubdivisionName = "";
                                            String memberSubdivisionFullName = "";
                                            for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                                                mei = (MemberEducationInfo) itr.next();

                                                degreeId = mei.getDegree().getDegreeId();
                                                degreeName = mei.getDegree().getDegreeName();

                                                instituteName = mei.getInstituteName() == null ? "" : mei.getInstituteName();

                                                boardUniversityName = mei.getUniversity().getUniversityLongName();
                                                yearOfPassing = mei.getYearOfPassing();
                                                resultTypeName = mei.getResultType().getResultTypeName();
                                                result = mei.getResult();

                                                if (degreeId == 2 || degreeId == 3) {

                                                    q2 = dbsession.createQuery("from Member as member WHERE id=" + memberIdH + " ");

                                                    for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
                                                        member = (Member) itr2.next();

                                                        memberSubdivisionName = member.getSubDivision().getSubDivisionName();
                                                        memberSubdivisionFullName = member.getSubDivision().getFullName();
                                                    }
                                                }

                                                degreeName = degreeName + memberSubdivisionFullName;
                                        %>
                                        <tr>
                                            <td><%=degreeName%></td>
                                            <td><%=boardUniversityName%></td>
                                            <td><%=yearOfPassing%></td>
                                            <td><%=result%></td>

                                        </tr>
                                        <%
                                            }


                                        %>

                                    </tbody>
                                </table>
                            </div>
                        </div>  
                    </div> 
                    <!--.container academic info --> 

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-header panel_heading_saz"><i class="fa fa-building"></i> Training Information</div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="my-3 row">
                            <div class="col-sm-12">
                                <table class="table table-hover table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col">Training Institute with Country</th>
                                            <th scope="col">Training Title</th>
                                            <th scope="col">Name of Topic/Course</th>
                                            <th scope="col">Duration (Week)</th>
                                            <th scope="col">Training Year</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%                                            String courseTitle = "";
                                            String trainingTitle = "";
                                            String weeks = "";
                                            int trainingId = 0;

                                            MemberTrainingInfo memberTrainingInfo = null;

                                            q1 = dbsession.createQuery("from MemberTrainingInfo where  member_id=" + memberIdH + " ");

                                            for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                                                memberTrainingInfo = (MemberTrainingInfo) itr.next();

                                                trainingId = memberTrainingInfo.getId();
                                                instituteName = memberTrainingInfo.getMemberTrainingInstitute() == null ? "" : memberTrainingInfo.getMemberTrainingInstitute();
                                                trainingTitle = memberTrainingInfo.getMemberTrainingTitle() == null ? "" : memberTrainingInfo.getMemberTrainingTitle();
                                                courseTitle = memberTrainingInfo.getMemberTrainingCourse() == null ? "" : memberTrainingInfo.getMemberTrainingCourse();
                                                yearOfPassing = memberTrainingInfo.getMemberTrainingYear() == null ? "" : memberTrainingInfo.getMemberTrainingYear();
                                                weeks = memberTrainingInfo.getMemberTrainingDuration() == null ? "" : memberTrainingInfo.getMemberTrainingDuration();


                                        %>
                                        <tr>
                                            <td><%=instituteName%></td>
                                            <td><%=trainingTitle%></td>
                                            <td><%=courseTitle%></td>
                                            <td><%=weeks%></td>
                                            <td><%=yearOfPassing%></td>

                                        </tr>

                                        <%                                }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                        </div>  
                    </div> 

                    <!--.container training info --> 
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-header panel_heading_saz"><i class="fa fa-briefcase"></i> Professional Experience</div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="my-3 row">
                            <div class="col-sm-12">
                                <table class="table table-hover table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col">Name of The Organization</th>
                                            <th scope="col">Designation</th>
                                            <th scope="col">From Date</th>
                                            <th scope="col">To Date</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            String organizationName = "";
                                            String organizationType = "";
                                            String organizationAddress = "";
                                            String designationName = "";
                                            String startDate = "";
                                            String tillDate = "";
                                            int profInfoId = 0;
                                            MemberProfessionalInfo memberProfessionalInfo = null;
                                            q1 = dbsession.createQuery("from MemberProfessionalInfo WHERE  member_id=" + memberIdH + " ");

                                            for (Iterator itr = q1.list().iterator();
                                                    itr.hasNext();) {
                                                memberProfessionalInfo = (MemberProfessionalInfo) itr.next();
                                                profInfoId = memberProfessionalInfo.getId();
                                                organizationName = memberProfessionalInfo.getCompanyName() == null ? "" : memberProfessionalInfo.getCompanyName();

                                                organizationType = memberProfessionalInfo.getCompanyType() == null ? "" : memberProfessionalInfo.getCompanyType();
                                                designationName = memberProfessionalInfo.getDesignation() == null ? "" : memberProfessionalInfo.getDesignation();
                                                startDate = memberProfessionalInfo.getFromDate().toString() == null ? "" : memberProfessionalInfo.getFromDate().toString();
                                                tillDate = memberProfessionalInfo.getTillDate().toString() == null ? "" : memberProfessionalInfo.getTillDate().toString();
                                                if (tillDate.equalsIgnoreCase("2099-12-31")) {
                                                    tillDate = "Till Now";
                                                }
                                                organizationAddress = memberProfessionalInfo.getCompanyAddress() == null ? "" : memberProfessionalInfo.getCompanyAddress();


                                        %>
                                        <tr>
                                            <td><%=organizationName%></td>
                                            <td><%=designationName%></td> 
                                            <td><%=startDate%></td>
                                            <td><%=tillDate%></td>

                                        </tr>

                                        <%                                }

                                        %>
                                    </tbody>
                                </table>
                            </div>
                        </div>  
                    </div> 
                    <!--.container professional info --> 
                    <div class="container">
                        <div class="row my-3">
                            <div class="col-sm-12">
                                <div class="card-header panel_heading_saz"><i class="fa fa-sticky-note"></i> Research/Publication Information</div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="my-3 row">
                            <div class="col-sm-12">
                                <table class="table table-hover table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col">Title</th>
                                            <th scope="col">Year</th>
                                            <th scope="col">Journal/Conference</th>
                                            <th scope="col">Author</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%                                            int publicationId = 0;
                                            String publicationTitle = "";
                                            String publicationAuthor = "";
                                            String publicationYear = "";
                                            String publicationJournal = "";
                                            MemberPublicationInfo memberPublicationInfo = null;
                                            q1 = dbsession.createQuery("from MemberPublicationInfo WHERE  member_id=" + memberIdH + " ");

                                            for (Iterator itr = q1.list().iterator();
                                                    itr.hasNext();) {
                                                memberPublicationInfo = (MemberPublicationInfo) itr.next();
                                                publicationId = memberPublicationInfo.getId();
                                                publicationTitle = memberPublicationInfo.getPublicationTitle() == null ? "" : memberPublicationInfo.getPublicationTitle();

                                                publicationYear = memberPublicationInfo.getPublicationYear() == null ? "" : memberPublicationInfo.getPublicationYear();
                                                publicationJournal = memberPublicationInfo.getPublicationJournalConference() == null ? "" : memberPublicationInfo.getPublicationJournalConference();
                                                publicationAuthor = memberPublicationInfo.getPublicationAuthor() == null ? "" : memberPublicationInfo.getPublicationAuthor().toString();


                                        %>
                                        <tr>
                                            <td><%=publicationTitle%></td>
                                            <td><%=publicationYear%></td> 
                                            <td><%=publicationJournal%></td>
                                            <td><%=publicationAuthor%></td>

                                        </tr>

                                        <%                                }

                                        %>
                                    </tbody>
                                </table>
                            </div>
                        </div>  
                    </div> 




                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-header panel_heading_saz"><i class="fa fa-users"></i> Project Experiences</div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="my-3 row">
                            <div class="col-sm-12">
                                <table class="table table-hover table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col">Project Title</th>
                                            <th scope="col">Category</th>
                                            <th scope="col">Role</th>
                                            <th scope="col">Details</th>
                                            <th scope="col">Start Date</th>
                                            <th scope="col">End Date</th>  
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%                                            String projectStartDate = "";
                                            String projectEndDate = "";
                                            String projectTitle = "";
                                            String projectCategory = "";
                                            String memberRole = "";
                                            String projectDetails = "";

                                            int projectInfoId = 0;
                                            MemberProjectInfo memberProjectInfo = null;

                                            q1 = dbsession.createQuery("from MemberProjectInfo WHERE  member_id=" + memberIdH + " ");

                                            for (Iterator itr = q1.list().iterator();
                                                    itr.hasNext();) {
                                                memberProjectInfo = (MemberProjectInfo) itr.next();
                                                projectInfoId = memberProjectInfo.getId();
                                                projectTitle = memberProjectInfo.getMemberProjectTitle() == null ? "" : memberProjectInfo.getMemberProjectTitle();

                                                projectCategory = memberProjectInfo.getMemberProjectCategory() == null ? "" : memberProjectInfo.getMemberProjectCategory();
                                                projectDetails = memberProjectInfo.getMemberProjectDetails() == null ? "" : memberProjectInfo.getMemberProjectDetails();
                                                memberRole = memberProjectInfo.getMemberProjectMemberRole() == null ? "" : memberProjectInfo.getMemberProjectMemberRole();
                                                projectStartDate = memberProjectInfo.getMemberProjectStartDate().toString() == null ? "" : memberProjectInfo.getMemberProjectStartDate().toString();
                                                projectEndDate = memberProjectInfo.getMemberProjectEndDate().toString() == null ? "" : memberProjectInfo.getMemberProjectEndDate().toString();


                                        %>
                                        <tr>
                                            <td><%=projectTitle%></td>
                                            <td><%=projectCategory%></td>
                                            <td><%=memberRole%></td>
                                            <td><%=projectDetails%></td> 
                                            <td><%=projectStartDate%></td>
                                            <td><%=projectEndDate%></td> 

                                        </tr>

                                        <%                                }

                                            dbsession.flush();
                                            dbsession.close();

                                        %>
                                    </tbody>
                                </table>
                            </div>
                        </div>  
                    </div> 





                </div>
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
