
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="javax.security.auth.login.Configuration"%>
<%@page import="java.net.InetAddress"%>

<%@page import="java.net.InetAddress"%>
<%@page import="java.math.BigDecimal"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>



<%
    Session dbsession = null;
    String qryparam = "";;

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    getDate date = new getDate();

//    if (session.isNew()) {
//        response.sendRedirect("logout.jsp");
//    }
    String username = null;
    String logstat = null;
    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    //  String sessionid = request.getParameter("sessionid").trim();
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString().toUpperCase();
        memberIdH = session.getAttribute("memberId").toString();

        System.out.println("memberIdH " + memberIdH);

        getRegistryID regId = new getRegistryID();

        MemberConferenceInfo memberConferenceInfo = null;
        String act = request.getParameter("act").trim();

        if (act.equals("edit")) {
            String conferenceId = request.getParameter("conferenceId").trim();
            
            Query q4 = dbsession.createQuery("from MemberConferenceInfo where id='" + conferenceId + "' ");
            Iterator itr4 = q4.list().iterator();
            if (itr4.hasNext()) {
                memberConferenceInfo = (MemberConferenceInfo) itr4.next();
            }
            System.out.println("memberConferenceInfo: "+memberConferenceInfo.getConferenceDetails());

            String conferenceTitle = request.getParameter("conferenceTitle").trim();
            String memberRole = request.getParameter("memberRole").trim();
            String conferenceRadioWorkshop = request.getParameter("conferenceRadioWorkshop").trim();
            String conferenceDetails = request.getParameter("conferenceDetails") == null ? "" : request.getParameter("conferenceDetails").trim();
            String conferenceURL = request.getParameter("conferenceURL") == null ? "" : request.getParameter("conferenceURL").trim();
            String startDate = request.getParameter("startDate") == null ? "" : request.getParameter("startDate").trim();
            String endDate = request.getParameter("endDate") == null ? "" : request.getParameter("endDate").trim();

            java.util.Date startDate1 = new SimpleDateFormat("yyyy-mm-dd").parse(startDate);
            java.util.Date endDate1 = new SimpleDateFormat("yyyy-mm-dd").parse(endDate);
 
            Member member = new Member();
            int memberId1 = 0;
            String memberId = session.getAttribute("memberId").toString();
            memberId1 = Integer.parseInt(memberId);

            member.setId(memberId1);
            memberConferenceInfo.setMember(member);
            memberConferenceInfo.setConferenceDetails(conferenceDetails);
            memberConferenceInfo.setConferenceEndDate(endDate1);
            memberConferenceInfo.setConferenceStartDate(startDate1);
            memberConferenceInfo.setConferenceTitle(conferenceTitle);
            memberConferenceInfo.setConferenceUrl(conferenceURL);
            memberConferenceInfo.setConferenceMemberRole(memberRole);
            dbsession.update(memberConferenceInfo);
            strMsg = "Information updated successfully.";
        } else if (act.equals("add")) {

            String conferenceTitle = request.getParameter("conferenceTitle").trim();
            String memberRole = request.getParameter("memberRole").trim();
            String conferenceRadioWorkshop = request.getParameter("conferenceRadioWorkshop").trim();
            String conferenceDetails = request.getParameter("conferenceDetails") == null ? "" : request.getParameter("conferenceDetails").trim();
            String conferenceURL = request.getParameter("conferenceURL") == null ? "" : request.getParameter("conferenceURL").trim();
            String startDate = request.getParameter("startDate") == null ? "" : request.getParameter("startDate").trim();
            String endDate = request.getParameter("endDate") == null ? "" : request.getParameter("endDate").trim();

            java.util.Date startDate1 = new SimpleDateFormat("yyyy-mm-dd").parse(startDate);
            java.util.Date endDate1 = new SimpleDateFormat("yyyy-mm-dd").parse(endDate);
            String conferenceId = regId.getID(40);
            memberConferenceInfo = new MemberConferenceInfo();
            Member member = new Member();
            int memberId1 = 0;
            int conferenceId1 = 0;
            String memberId = session.getAttribute("memberId").toString();
            memberId1 = Integer.parseInt(memberId);
            conferenceId1 = Integer.parseInt(conferenceId);
            member.setId(memberId1);
            memberConferenceInfo.setId(conferenceId1);
            memberConferenceInfo.setMember(member);
            memberConferenceInfo.setConferenceDetails(conferenceDetails);
            memberConferenceInfo.setConferenceEndDate(endDate1);
            memberConferenceInfo.setConferenceStartDate(startDate1);
            memberConferenceInfo.setConferenceTitle(conferenceTitle);
            memberConferenceInfo.setConferenceUrl(conferenceURL);
            memberConferenceInfo.setConferenceMemberRole(memberRole);
            dbsession.save(memberConferenceInfo);
            strMsg = "Information saved successfully.";

        } else if (act.equals("del")) {
            String conferenceId = request.getParameter("conferenceId").trim();
            Query q4 = dbsession.createQuery("delete from MemberConferenceInfo where id='" + conferenceId + "' ");
            q4.executeUpdate();
            strMsg = "Information removed successfully.";
        }

        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
//            strMsg = "Success!!! Member Training Information added ";
            response.sendRedirect("memberConferenceInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        } else {
            strMsg = "Error!!! When Conference Information added ";
            dbtrx.rollback();
            response.sendRedirect("memberConferenceInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        }
    } else {
        System.out.println("LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

%>
