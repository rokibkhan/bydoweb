<%@ include file="../header.jsp" %>


<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="about_banner_membership">
    <h2 class="display_41_saz text-white">membership Form</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Membership</a></li>
                <li class="breadcrumb-item active" aria-current="page">Membership Form</li>
            </ol>
        </nav>
    </div>
</div>


<section class="news-single-content pt-0">
    <div class="container">
        <div class="row">
            <div class="col-12">DOWNLOAD APPLICATION FORM FOR (ASSOCIATE MEMBERSHIP)
                <div class="news-content news_cont_saz">
                    <embed src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/Final_Membership_form_associate.pdf" type="application/pdf"   height="800px" width="100%">
                </div>
            </div>
        </div>
    </div>
</section><br>


<section class="news-single-content pt-0">
    <div class="container">
        <div class="row">
            <div class="col-12">DOWNLOAD APPLICATION FORM FOR (MEMBERSHIP)
                <div class="news-content news_cont_saz">
                    <embed src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/Final_Membership_form_member.pdf" type="application/pdf"   height="800px" width="100%">
                </div>
            </div>
        </div>
    </div>
</section><br>

<section class="news-single-content pt-0">
    <div class="container">
        <div class="row">
            <div class="col-12">DOWNLOAD APPLICATION FORM FOR (FELLOW MEMBERSHIP)
                <div class="news-content news_cont_saz">
                    <embed src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/Final_Membership_form_fellow.pdf" type="application/pdf"   height="800px" width="100%">
                </div>
            </div>
        </div>
    </div>
</section><br>


<%@ include file="../footer.jsp" %>
