
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    //   http://bangladeshsms.com/smsapi?api_key=C20013475bbdd6cbdd2ac1.58430946&type=text&contacts=+8801755656354&senderid=8801552146206&msg=Test%20message
    String sendUrl = "http://bangladeshsms.com/smsapi";
    String api_key = "C20013475bbdd6cbdd2ac1.58430946";
    String type = "text";
    String contacts = "+8801755656354";
    //    String senderid = "8801552146206";
    String senderid = "8804445629730";
    String msg = "message";

    HttpURLConnection.setFollowRedirects(true); // defaults to true
    // String url = "http://www.google.com";
    URL request_url = new URL(sendUrl);
    HttpURLConnection http_conn = (HttpURLConnection) request_url.openConnection();
    http_conn.setConnectTimeout(100000);
    http_conn.setReadTimeout(100000);
    http_conn.setInstanceFollowRedirects(true);
    http_conn.setRequestProperty("api_key", api_key);
    http_conn.setRequestProperty("type", type);
    http_conn.setRequestProperty("contacts", contacts);
    http_conn.setRequestProperty("senderid", senderid);
    http_conn.setRequestProperty("msg", msg);

    System.out.println("SSSS::::" + String.valueOf(http_conn.getResponseCode()));
    System.out.println("Response Message SSSS::::" + http_conn.getResponseMessage());

    int code = http_conn.getResponseCode(); // 200 = HTTP_OK
    System.out.println("Response    (Code):" + code);
    System.out.println("Response (Message):" + http_conn.getResponseMessage());

//    HttpURLConnection.setFollowRedirects(true); // defaults to true
//
//    String url = "http://dblapp.com/"
//    URL request_url = new URL(url);
//    HttpURLConnection http_conn = (HttpURLConnection) request_url.openConnection();
//    http_conn.setConnectTimeout(100000);
//    http_conn.setReadTimeout(100000);
//    http_conn.setInstanceFollowRedirects(true);
//    http_conn.setDoOutput(true);
//    PrintWriter out = new PrintWriter(http_conn.getOutputStream());
//    if (urlparameter != null) {
//        out.println(urlparameter);
//    }
//    out.close();
//    out = null;
//    System.out.println(String.valueOf(http_conn.getResponseCode()));
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);

    }

    String regMemberTempId = request.getParameter("regTempId") == null ? "" : request.getParameter("regTempId").trim();
    String regMemId = request.getParameter("regMemId") == null ? "" : request.getParameter("regMemId").trim();
    String regStep = request.getParameter("regStep") == null ? "" : request.getParameter("regStep").trim();

    String mobileNumber = "";

    String showTabOption = "";
    if (regStep.equals("3")) {
        showTabOption = " show";

    } else {
        showTabOption = "";
    }

    System.out.println("Header sessionIdH :: " + sessionIdH);
    System.out.println("Header userNameH :: " + userNameH);
    System.out.println("Header userStoreIdH :: " + memberIdH);


%>

<%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
    String msgDispalyConT, msgInfoText, sLinkOpt;
    if (!strMsg.equals("")) {
        msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {
        msgDispalyConT = "style=\"display: none;\"";
        msgInfoText = "";
    }
%>

<script type="text/javascript">
    function personalInfoValidation() {

        var genderChecked = $(".genderClass").prop('checked');
        if (!genderChecked) {
            // alert("Please select atlease one jobwork");
            $("#memberGenderErr").html("Please select gender").css({"color": "red", "font-size": "10px;"});
            return false;

        }
        ;
    }
</script>

<!-- Login & Register & About CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about.css">



<!-- Starting of Registration form -->
<div class="registration">
    <div class="container bg-white my-3 py-3">
        <h2 class="text-center mx-auto my-5 font-weight-bold text-dark" style="font-size:3.0rem;"><span style="color: #0044cc;">MEMBERSHIP</span> FROM</h2>
        <div class="row globalAlertInfoBoxConParentTT">
            <!-- .globalAlertInfoBoxConTT start -->
            <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                    <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                    <%=msgInfoText%>
                </div>
            </div>
            <!-- .globalAlertInfoBoxConTT end -->
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h3 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <span class="h4">Section 1: Personal Information</span>
                                </button>
                            </h3>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-12">
                                        <label for="inputName">* Required field</label>

                                    </div>
                                </div>
                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationPersonalInfoSubmitData.jsp" onsubmit="return personalInfoValidation()">
                                    <div class="form-row">
                                        <div class="form-group col-12">
                                            <label for="inputName">Name *</label>
                                            <input name="memberName" id="memberName" type="text" class="form-control" placeholder="Full Name" required>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputNationality">Father Name *</label>
                                            <input name="fatherName" id="fatherName" type="text" class="form-control" placeholder="Father Name" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="inputPOB">Mother Name *</label>
                                            <input name="motherName" id="motherName" type="text" class="form-control" placeholder="Mother Name" required>
                                        </div>
                                    </div>


                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label for="inputDOB">Date of Birth *</label>
                                            <input  name="memberDoB" id="memberDoB" type="text" class="form-control" placeholder="DD-MM-YYYY" required>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="inputAge">Age *</label>
                                            <input  name="memberAge" id="memberAge" type="text" class="form-control" placeholder="(On the next birth date) Years" required>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="inputPOB">Place of Birth *</label>
                                            <input  name="memberPlaceOfBirth" id="memberPlaceOfBirth" type="text" class="form-control" placeholder="Place of Birth" required>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="inputAge">Nationality *</label>
                                            <input name="memberNationality" id="memberNationality" type="text" class="form-control" placeholder="Nationality" required>
                                        </div>
                                    </div>
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-2 pt-0">Gender *</legend>
                                            <div class="col-sm-10">

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input genderClass" type="radio" name="memberGender" id="memberGenderMale" value="M" >
                                                    <label class="form-check-label radio_label1" for="memberGender">Male</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input genderClass" type="radio" name="memberGender" id="memberGenderFemale" value="F" >
                                                    <label class="form-check-label radio_label1" for="memberGender">Female</label>
                                                </div>
                                                <span id="memberGenderErr" class=""></span>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div class="form-row border border-light my-4">
                                        <h4 class="text-center m-3 w-100 font-weight-bold" > Phone </h4>
                                        <div class="form-group col-md-6">
                                            <label for="inputOffice">Office</label>
                                            <input id="memberOfficePhone" name="memberOfficePhone"  type="text" class="form-control" placeholder="Office phone"  required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="inputResidence">Residence</label>
                                            <input id="memberResidencePhone" name="memberResidencePhone" type="text" class="form-control" placeholder="Residence number" required>
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="form-group col-md-6">
                                            <label for="inputMobile">Mobile</label>
                                            <input id="memberMobile" name="memberMobile" type="text" class="form-control" placeholder="Mobile number"  required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="inputPOB">Email</label>
                                            <input id="memberEmail" name="memberEmail" type="email" class="form-control" placeholder="Email address" required>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputName">Membership Applying For</label>
                                            <input id="membershipApplyingFor" name="membershipApplyingFor" type="text" class="form-control" placeholder="Membership Applying For">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="inputName">Present IEB Membership Number <span class="text-muted">(if any)</span></label>
                                            <input  id="presentIEBmembershipNumber" name="presentIEBmembershipNumber" type="text" class="form-control" placeholder="Present IEB Membership Number">
                                        </div>
                                    </div>




                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingOneVerify">
                            <h3 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOneVerify" aria-expanded="true" aria-controls="collapseOneVerify">
                                    <span class="h4">Section 2: Verification</span>
                                </button>
                            </h3>
                        </div>

                        <div id="collapseOneVerify" class="collapse" aria-labelledby="headingOneVerify" data-parent="#accordion">
                            <div class="card-body">
                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="#">
                                    <p class="m-2">Please check your mobile inbox and add verification code and click verify button </p>

                                    <div class="form-row form-group">
                                        <label class="col-2 text-right" for="inputName">Reg No:</label>
                                        <div class="col-6">
                                            <p><%=regMemberTempId%></p>
                                            <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                            <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <label class="col-2 text-right" for="inputName">Mobile:</label>
                                        <div class="col-4">
                                            <p><%=mobileNumber%></p>
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <label class="col-2 text-right" for="inputName">Verification Code</label>
                                        <div class="col-2">
                                            <input name="memberVerifyCode" id="memberVerifyCode" type="text" class="form-control" placeholder="Code" required disabled="">
                                        </div>
                                        <div class="col-3">
                                            &nbsp;
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>                
                    <div class="card">
                        <div class="card-header" id="headingOneAddressInfo">
                            <h3 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOneAddressInfo" aria-expanded="true" aria-controls="collapseOneAddressInfo">
                                    <span class="h4">Section 3: Address Information</span>
                                </button>
                            </h3>
                        </div>

                        <div id="collapseOneAddressInfo" class="collapse" aria-labelledby="headingOneAddressInfo" data-parent="#accordion">
                            <div class="card-body">
                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationAddressInfoSubmitData.jsp">

                                    <div class="row">
                                        <div class="col-6">
                                            <p class="m-2 text-center">Mailing Address</p>

                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Country:</label>
                                                <div class="col-7">

                                                    <select  id="memberMailingAddressCountry" name="memberMailingAddressCountry" class="form-control input-sm customInput-sm" required>
                                                        <option value="">Select Country</option>
                                                        <%
                                                            Query countryMASQL = null;
                                                            Object[] objectMA = null;
                                                            String countryCodeMA = "";
                                                            String countryNameMA = "";
                                                            String selectedMA = "";
                                                            countryMASQL = dbsession.createSQLQuery("SELECT * FROM sy_country ORDER BY COUNTRY_NAME ASC");
                                                            for (Iterator itrMA = countryMASQL.list().iterator(); itrMA.hasNext();) {
                                                                objectMA = (Object[]) itrMA.next();
                                                                countryCodeMA = objectMA[0].toString();
                                                                countryNameMA = objectMA[1].toString();
                                                                if (countryCodeMA.equals("BD")) {
                                                                    selectedMA = " selected";
                                                                } else {
                                                                    selectedMA = "";
                                                                }


                                                        %> 
                                                        <option value=" <%=countryCodeMA%> " <%=selectedMA%>> <%=countryNameMA%> </option> 
                                                        <%
                                                            }

                                                        %>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">District:</label>
                                                <div class="col-7">
                                                    <select  id="memberMailingAddressDistrict" name="memberMailingAddressDistrict" onchange="showDistrictWiseThanaInfo(this.value, 'MA')" class="form-control input-sm customInput-sm" required>
                                                        <option value="">Select District</option>
                                                        <%                                                            Query districtMASQL = null;
                                                            Object[] objectMAd = null;
                                                            String districtIdMA = "";
                                                            String districtNameMA = "";
                                                            String districtMA = "";
                                                            districtMASQL = dbsession.createSQLQuery("SELECT * FROM district ORDER BY DISTRICT_NAME ASC");
                                                            for (Iterator itrMAd = districtMASQL.list().iterator(); itrMAd.hasNext();) {
                                                                objectMAd = (Object[]) itrMAd.next();
                                                                districtIdMA = objectMAd[0].toString();
                                                                districtNameMA = objectMAd[1].toString();


                                                        %> 
                                                        <option value=" <%=districtIdMA%> "> <%=districtNameMA%> </option> 
                                                        <%
                                                            }
                                                        %>

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Thana:</label>
                                                <div class="col-7" id="showThanaInfoMA">                                                    
                                                    <select  id="memberMailingAddressThana" name="memberMailingAddressThana" class="form-control input-sm customInput-sm" required>
                                                        <option value="">Select Thana</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Address Line1:</label>
                                                <div class="col-7">
                                                    <input name="memberMailingAddressLine1" id="memberMailingAddressLine1" type="text" class="form-control" placeholder="Address Line1" required>
                                                </div>
                                            </div>
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Address Line2:</label>
                                                <div class="col-7">
                                                    <input name="memberMailingAddressLine2" id="memberMailingAddressLine2" type="text" class="form-control" placeholder="Address Line2">
                                                </div>
                                            </div>
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Post Office:</label>
                                                <div class="col-4">
                                                    <input name="memberMailingAddressPostOffice" id="memberMailingAddressPostOffice" type="text" class="form-control" placeholder="Post Office" required>
                                                </div>
                                                <div class="col-3">
                                                    <input name="memberMailingAddressPostCode" id="memberMailingAddressPostCode" type="text" class="form-control" placeholder="Post Code" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <p class="m-2 text-center">Permanent Address</p>

                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Country:</label>
                                                <div class="col-7">
                                                    <select  id="memberPermanentAddressCountry" name="memberPermanentAddressCountry" class="form-control input-sm customInput-sm" required>
                                                        <option value="">Select Country</option>
                                                        <%
                                                            Query countryPASQL = null;
                                                            Object[] objectPA = null;
                                                            String countryCodePA = "";
                                                            String countryNamePA = "";
                                                            String selectedPA = "";
                                                            countryPASQL = dbsession.createSQLQuery("SELECT * FROM sy_country ORDER BY COUNTRY_NAME ASC");
                                                            for (Iterator itrPA = countryPASQL.list().iterator(); itrPA.hasNext();) {
                                                                objectPA = (Object[]) itrPA.next();
                                                                countryCodePA = objectPA[0].toString();
                                                                countryNamePA = objectPA[1].toString();
                                                                if (countryCodePA.equals("BD")) {
                                                                    selectedPA = " selected";
                                                                } else {
                                                                    selectedPA = "";
                                                                }


                                                        %> 
                                                        <option value=" <%=countryCodePA%> " <%=selectedPA%>> <%=countryNamePA%> </option> 
                                                        <%
                                                            }

                                                        %>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">District:</label>
                                                <div class="col-7">
                                                    <select  id="memberPermanentAddressDistrict" name="memberPermanentAddressDistrict" onchange="showDistrictWiseThanaInfo(this.value, 'PA')" class="form-control input-sm customInput-sm" required>
                                                        <option value="">Select District</option>
                                                        <%                                                            Query districtPASQL = null;
                                                            Object[] objectPAd = null;
                                                            String districtIdPA = "";
                                                            String districtNamePA = "";
                                                            String districtPA = "";
                                                            districtPASQL = dbsession.createSQLQuery("SELECT * FROM district ORDER BY DISTRICT_NAME ASC");
                                                            for (Iterator itrPAd = districtPASQL.list().iterator(); itrPAd.hasNext();) {
                                                                objectPAd = (Object[]) itrPAd.next();
                                                                districtIdPA = objectPAd[0].toString();
                                                                districtNamePA = objectPAd[1].toString();


                                                        %> 
                                                        <option value=" <%=districtIdPA%> "> <%=districtNamePA%> </option> 
                                                        <%
                                                            }
                                                        %>

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Thana:</label>
                                                <div class="col-7" id="showThanaInfoPA">                                                    
                                                    <select  id="memberPermanentAddressThana" name="memberPermanentAddressThana" class="form-control input-sm customInput-sm" required>
                                                        <option value="">Select Thana</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Address Line1:</label>
                                                <div class="col-7">
                                                    <input name="memberPermanentAddressLine1" id="memberPermanentAddressLine1" type="text" class="form-control" placeholder="Address Line1" required>
                                                </div>
                                            </div>
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Address Line2:</label>
                                                <div class="col-7">
                                                    <input name="memberPermanentAddressLine2" id="memberPermanentAddressLine2" type="text" class="form-control" placeholder="Address Line2">
                                                </div>
                                            </div>
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Post Office:</label>
                                                <div class="col-4">
                                                    <input name="memberPermanentAddressPostOffice" id="memberPermanentAddressPostOffice" type="text" class="form-control" placeholder="Post Office" required>
                                                </div>
                                                <div class="col-3">
                                                    <input name="memberPermanentAddressPostCode" id="memberPermanentAddressPostCode" type="text" class="form-control" placeholder="Post Code" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">

                                            <div class="form-row form-group">                                                
                                                <div class="col-6 offset-3">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>                
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <span class="h4">Section 4: Education Information</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">

                                <%
                                    Object[] object6 = null;
                                    String universityId = "";
                                    String universityShortName = "";
                                    String universityLongName = "";
                                    String universityName = "";
                                    String degreeNameX1 = "";
                                    String universityOptions = "";
                                    Query q6 = dbsession.createSQLQuery("select * FROM university ORDER BY university_short_name ASC");
                                    for (Iterator itr6 = q6.list().iterator(); itr6.hasNext();) {
                                        object6 = (Object[]) itr6.next();
                                        universityId = object6[0].toString();
                                        universityShortName = object6[1].toString();
                                        universityLongName = object6[2].toString() == null ? "" : object6[2].toString();

                                        if (universityLongName.equals("")) {
                                            universityName = universityShortName;
                                        } else {
                                            universityName = universityLongName + "(" + universityShortName + ")";
                                        }
                                        universityOptions = universityOptions + "<option value=\"" + universityId + "\">" + universityName + "</option>";

                                    }

                                    Object[] object7 = null;
                                    String resultTypeIdX = "";
                                    String resultTypeNameX = "";
                                    String resultTypeOptions = "";
                                    Query q7 = dbsession.createSQLQuery("select * FROM result_type ORDER BY result_type_id ASC");
                                    for (Iterator itr7 = q7.list().iterator(); itr7.hasNext();) {
                                        object7 = (Object[]) itr7.next();
                                        resultTypeIdX = object7[0].toString();
                                        resultTypeNameX = object7[1].toString();
                                        resultTypeOptions = resultTypeOptions + "<option value=\"" + resultTypeIdX + "\">" + resultTypeNameX + "</option>";

                                    }

                                    String yearOptions = "";
                                    // int yearStart = 1960 ;
                                    int yearEnd = 2019;//current Year
                                    // DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    //  Date date = new Date();
                                    //   String entryDate = dateFormat.format(date);
                                    for (int yearStart = 1960; yearStart <= yearEnd; yearStart++) {

                                        yearOptions = yearOptions + "<option value=\"" + yearStart + "\">" + yearStart + "</option>";
                                    }
                                %>

                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationEducationSubmitData.jsp">
                                    <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                    <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                    <div class="row">
                                        <div class="col-6">
                                            <p class="m-2 text-center text-dark">SSC/Equivalent</p>
                                            <input name="memberSSCDegreeTypeId" id="memberSSCDegreeTypeId" type="hidden" value="1">
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Institution:</label>
                                                <div class="col-8">
                                                    <input name="memberSSCInstitution" id="memberSSCInstitution" type="text" class="form-control" placeholder="Institution" required>
                                                </div>                                                    
                                            </div>  
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Board/University:</label>
                                                <div class="col-8">
                                                    <select name="memberSSCBoardUniversity" id="memberSSCBoardUniversity" class="form-control chosen">
                                                        <option value="">Select Board/University</option>                                                       
                                                        <%=universityOptions%>

                                                    </select>
                                                </div>                                                    
                                            </div>  
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Year of Passing:</label>
                                                <div class="col-8">
                                                    <select id="memberSSCPassingYear" name="memberSSCPassingYear"   class="custom-select">
                                                        <option value="">Select Year</option>
                                                        <%=yearOptions%>									
                                                    </select>
                                                </div>                                                    
                                            </div> 
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Result :</label>
                                                <div class="col-4">
                                                    <select name="memberSSCResultTypeId" id="memberSSCResultTypeId" class="form-control">
                                                        <option value="">Result Type </option>
                                                        <%=resultTypeOptions%>

                                                    </select>
                                                </div> 
                                                <div class="col-4">
                                                    <input name="memberSSCScoreClass" id="memberSSCScoreClass"  type="text" class="form-control" placeholder="Score/Class"  required>
                                                </div> 
                                            </div> 
                                        </div>  

                                        <div class="col-6">
                                            <p class="m-2 text-center text-dark">HSC/Equivalent</p>

                                            <input name="memberHSCDegreeTypeId" id="memberHSCDegreeTypeId" type="hidden" value="2">
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Institution:</label>
                                                <div class="col-8">
                                                    <input name="memberHSCInstitution" id="memberHSCInstitution" type="text" class="form-control" placeholder="Institution" required>
                                                </div>                                                    
                                            </div>  
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Board/University:</label>
                                                <div class="col-8">
                                                    <select name="memberHSCBoardUniversity" id="memberHSCBoardUniversity" class="form-control chosen">
                                                        <option value="">Select Board/University</option>
                                                        <%=universityOptions%>

                                                    </select>
                                                </div>                                                    
                                            </div>  
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Year of Passing:</label>
                                                <div class="col-8">
                                                    <select id="memberHSCPassingYear" name="memberHSCPassingYear"   class="custom-select">
                                                        <option value="">Select Year</option>
                                                        <%=yearOptions%>									
                                                    </select>
                                                </div>                                                    
                                            </div> 
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Result :</label>
                                                <div class="col-4">
                                                    <select name="memberHSCResultTypeId" id="memberHSCResultTypeId" class="form-control">
                                                        <option value="">Result Type </option>
                                                        <%=resultTypeOptions%>

                                                    </select>
                                                </div> 
                                                <div class="col-4">
                                                    <input name="memberHSCScoreClass" id="memberHSCScoreClass"  type="text" class="form-control" placeholder="Score/Class"  required>
                                                </div> 
                                            </div>                                                
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <p class="m-2 text-center text-dark">B.Sc. Engg./Equivalent </p>
                                            <input name="memberBSCDegreeTypeId" id="memberBSCDegreeTypeId" type="hidden" value="3">

                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">University:</label>
                                                <div class="col-8">
                                                    <select name="memberBSCBoardUniversity" id="memberBSCBoardUniversity" class="form-control chosen">
                                                        <option value="">Select University</option>                                                       
                                                        <%=universityOptions%>

                                                    </select>
                                                </div>                                                    
                                            </div>  
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Year of Passing:</label>
                                                <div class="col-8">
                                                    <select id="memberBSCPassingYear" name="memberBSCPassingYear"   class="custom-select" required>
                                                        <option value="">Select Year</option>
                                                        <%=yearOptions%>									
                                                    </select>
                                                </div>                                                    
                                            </div> 
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Result :</label>
                                                <div class="col-4">
                                                    <select name="memberBSCResultTypeId" id="memberBSCResultTypeId" class="form-control">
                                                        <option value="">Result Type </option>
                                                        <%=resultTypeOptions%>

                                                    </select>
                                                </div> 
                                                <div class="col-4">
                                                    <input name="memberBSCScoreClass" id="memberBSCScoreClass"  type="text" class="form-control" placeholder="Score/Class"  required>
                                                </div> 
                                            </div> 
                                        </div>  

                                        <div class="col-6">
                                            <p class="m-2 text-center text-dark">M.Sc./Ph.D/Equivalent </p>

                                            <input name="memberMSCDegreeTypeId" id="memberMSCDegreeTypeId" type="hidden" value="4">

                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">University:</label>
                                                <div class="col-8">
                                                    <select name="memberMSCBoardUniversity" id="memberMSCBoardUniversity" class="form-control chosen">
                                                        <option value="">Select University</option>
                                                        <%=universityOptions%>

                                                    </select>
                                                </div>                                                    
                                            </div>  
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Year of Passing:</label>
                                                <div class="col-8">
                                                    <select id="memberMSCPassingYear" name="memberMSCPassingYear"   class="custom-select">
                                                        <option value="">Select Year</option>
                                                        <%=yearOptions%>									
                                                    </select>
                                                </div>                                                    
                                            </div> 
                                            <div class="form-row form-group">
                                                <label class="form-control-label col-3 text-right" for="inputName">Result :</label>
                                                <div class="col-4">
                                                    <select name="memberMSCResultTypeId" id="memberMSCResultTypeId" class="form-control">
                                                        <option value="">Result Type </option>
                                                        <%=resultTypeOptions%>

                                                    </select>
                                                </div> 
                                                <div class="col-4">
                                                    <input name="memberMSCScoreClass" id="memberMSCScoreClass"  type="text" class="form-control" placeholder="Score/Class">
                                                </div> 
                                            </div>                                                
                                        </div>
                                    </div>                   
                                    <div class="row">
                                        <div class="col-12">

                                            <div class="form-row form-group">                                                
                                                <div class="col-6 offset-3">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div>                   

                                </form>                                
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <span class="h4">Section 5: Field of Engineering</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse <%=showTabOption%>" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">

                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationFieldEngineeringSubmitData.jsp">
                                    <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                    <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                    <div class="row">
                                        <div class="col-5">
                                            <div class="form-row form-group">
                                                <label class="col-3 col-form-label text-right" for="inputName">Division:</label>
                                                <div class="col-8">
                                                    <select name="memberDivisionId" id="memberDivisionId" class="form-control" required>
                                                        <option value="">Select Division</option>
                                                        <%
                                                            Object[] objectDiv = null;
                                                            String divisionId = "";
                                                            String divisionName = "";
                                                            String divisionOptions = "";
                                                            Query divisionSQL = dbsession.createSQLQuery("select * FROM member_division ORDER BY mem_division_name ASC");
                                                            for (Iterator itr9 = divisionSQL.list().iterator(); itr9.hasNext();) {
                                                                objectDiv = (Object[]) itr9.next();
                                                                divisionId = objectDiv[0].toString();
                                                                divisionName = objectDiv[1].toString();
                                                                divisionOptions = divisionOptions + "<option value=\"" + divisionId + "\">" + divisionName + "</option>";

                                                            }
                                                        %>
                                                        <%=divisionOptions%>
                                                    </select>
                                                </div>                                                    
                                            </div>                                                   
                                        </div>
                                        <div class="col-5">
                                            <div class="form-row form-group">
                                                <label class="col-3 col-form-label text-right " for="inputName">Sub-Division:</label>
                                                <div class="col-8">
                                                    <input name="memberSubDivision" id="memberSubDivision" type="text" class="form-control" placeholder="Sub-Division">
                                                </div>                                                    
                                            </div>                                                   
                                        </div> 
                                        <div class="col-2">
                                            &nbsp;
                                        </div>
                                    </div>
                                </form>



                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <span class="h4">Section 6: Professional Record</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse <%=showTabOption%>" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">

                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationProfessionalInfoSubmitData.jsp">
                                    <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                    <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >
                                    <div class="row">
                                        <div class="col-5 offset-3">
                                            <div class="form-row form-group">
                                                <label class="col-3 col-form-label text-right" for="inputName">Organization:</label>
                                                <div class="col-8">
                                                    <input name="memProfOrgName" id="memProfOrgName" type="text" class="form-control" placeholder="Oranization name">
                                                </div>                                                    
                                            </div> 
                                            <div class="form-row form-group">
                                                <label class="col-3 col-form-label text-right" for="inputName">Designation:</label>
                                                <div class="col-8">
                                                    <input name="memProfDesignation" id="memProfDesignation" type="text" class="form-control" placeholder="Designation name">
                                                </div>                                                    
                                            </div>   
                                            <div class="form-row form-group">
                                                <label class="col-3 col-form-label text-right" for="inputName">Start Date</label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="startDate" name="startDate"  placeholder="yyyy-mm-dd" required=""> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                                </div>                                                    
                                            </div>      
                                            <div class="form-row form-group">
                                                <label class="col-3 col-form-label text-right" for="inputName">End Date</label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control" id="endDate" name="endDate"  placeholder="yyyy-mm-dd" required=""> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                                </div>  
                                                <div class="col-1">
                                                    <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                                        <input type="checkbox" class="custom-control-input" id="customControlInline">
                                                        <label class="custom-control-label" for="customControlInline">Continue</label>
                                                    </div>
                                                </div>                                                                    
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">

                                            <div class="form-row form-group">                                                
                                                <div class="col-2 offset-5">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div>     
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingFourRecom">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourRecom" aria-expanded="false" aria-controls="collapseFourRecom">
                                    <span class="h4">Section 7: Recommendation Info</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFourRecom" class="collapse <%=showTabOption%>" aria-labelledby="headingFourRecom" data-parent="#accordion">
                            <div class="card-body">

                                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/registrationRecomendationInfoSubmitData.jsp">
                                    <input type="hidden" name="memberRegId" id="memberRegId" value="<%=regMemberTempId%>" >
                                    <input type="hidden" name="memberVerifyId" id="memberVerifyId" value="<%=regMemId%>" >

                                    <div class="row">
                                        <div class="col-4">
                                            <p class="m-2 text-center text-dark">Proposer</p>
                                            <div class="form-row form-group">
                                                <div class="col-8 offset-2">
                                                    <input name="proposerMemberName" id="proposerMemberName" type="text" class="form-control" placeholder="Member name" required>
                                                </div>                                                    
                                            </div> 
                                            <div class="form-row form-group">
                                                <div class="col-8 offset-2">
                                                    <input name="proposerMemberId" id="proposerMemberId" type="text" class="form-control" placeholder="Membership No" required>
                                                </div>                                                    
                                            </div>    

                                        </div>
                                        <div class="col-4">
                                            <p class="m-2 text-center text-dark">Seconder |</p>
                                            <div class="form-row form-group">
                                                <div class="col-8 offset-2">
                                                    <input name="seconderMemberName" id="seconderMemberName" type="text" class="form-control" placeholder="Member name" required>
                                                </div>                                                    
                                            </div> 
                                            <div class="form-row form-group">
                                                <div class="col-8 offset-2">
                                                    <input name="seconderMemberId" id="seconderMemberId" type="text" class="form-control" placeholder="Membership No" required>
                                                </div>                                                    
                                            </div>    

                                        </div>
                                        <div class="col-4">
                                            <p class="m-2 text-center text-dark">Seconder ||</p>
                                            <div class="form-row form-group">
                                                <div class="col-8 offset-2">
                                                    <input name="thirdMemberName" id="thirdMemberName" type="text" class="form-control" placeholder="Member name">
                                                </div>                                                    
                                            </div> 
                                            <div class="form-row form-group">
                                                <div class="col-8 offset-2">
                                                    <input name="thirdMemberId" id="thirdMemberId" type="text" class="form-control" placeholder="Membership No">
                                                </div>                                                    
                                            </div>    

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">

                                            <div class="form-row form-group">                                                
                                                <div class="col-2 offset-5">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div>     
                                </form>
                                <div class="row">
                                    <div class="col-12">
                                        <p class="text-dark" style="font-size: 0.8rem;">Note |: Proposer and Seconder must be at least 
                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i.Two Fellows and one Member for Fellowship
                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ii.One Fellow and two Members for Fellowship
                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; iii.Two Members for Associate Membership
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<!-- Ending of Registration form -->

<%

    dbsession.flush();
    dbsession.close();
%>

<%@ include file="../footer.jsp" %>
