<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    String memberName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String userEmail = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String centerId = "";

    Member member = null;

    q1 = dbsession.createQuery("from Member as member WHERE id=" + memberIdH + " ");

    Object[] object = null;

    for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
        member = (Member) itr.next();

        memberName = member.getMemberName();
        placeOfBirth = member.getPlaceOfBirth() == null ? "" : member.getPlaceOfBirth();
        dob = member.getDob();
        gender = member.getGender().trim();
        if (gender.equals("1")) {
            gender = "Male";
        } else {
            gender = "Female";
        }
        mobileNo = member.getMobile();
        phone1 = member.getPhone1();
        phone2 = member.getPhone2();
        userEmail = member.getEmailId();
        bloodGroup = member.getBloodGroup();
    }

    dbsession.flush();
    dbsession.close();


%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-user"></i> My Profile</div>
                <div class="card-block">

                    <p class="content_title_saz pb-2 m-3">Personal Information</p>

                    <form class="profile_form_horizontal_saz p_form">
                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2 ml-3 font-weight-bold">User ID</label>
                            <div class="col-sm-9"><p><%=userNameH%></p></div>
                        </div>

                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2 ml-3 font-weight-bold">Name</label>
                            <div class="col-sm-9"><p><%=memberName%></p></div>
                        </div>

                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2 ml-3 font-weight-bold">Email</label>
                            <div class="col-sm-9"><p><%=userEmail%></p></div>
                        </div>

                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2 ml-3 font-weight-bold">Mobile</label>
                            <div class="col-sm-9"><p><%=mobileNo%></p></div>
                        </div>
                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2 ml-3 font-weight-bold">Blood Group</label>
                            <div class="col-sm-9"><p><%=bloodGroup%></p></div>
                        </div>

                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2 ml-3 font-weight-bold">Gender</label>
                            <div class="col-sm-9"><p><%=gender%></p></div>
                        </div>


                    </form>
                    <!-- ============ End personal information form ================== -->



                    <p class="content_title_saz pb-2 m-3">Mailing Address</p>

                    <form class=" profile_form_horizontal_saz">

                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2 ml-3 font-weight-bold">Address</label>
                            <div class="col-sm-9"><p>House # 31/1 Keya Villa, Road # 11 (Sargent Golly), Kallayanpur, Dhaka-1207</p></div>
                        </div>


                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2">&nbsp;</label>
                            <div class="col-sm-5 align-self-right ml-3"><p><a href="#" class="btn btn-primary btn-sm">Edit Info</a></p></div>
                        </div>
                    </form>
                    <!-- ================ End billing information form ================= -->

                    <p class="content_title_saz pb-2 m-3">Permanent Address</p>

                    <form class=" profile_form_horizontal_saz">

                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2 ml-3 font-weight-bold">Address</label>
                            <div class="col-sm-9"><p>House # 31/1 Keya Villa, Road # 11 (Sargent Golly), Kallayanpur, Dhaka-1207</p></div>
                        </div>

                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2 ml-3 font-weight-bold">Mobile</label>
                            <div class="col-sm-9"><p>+88 01922136511</p></div>
                        </div>

                        <div class="form-group d-flex align-items-center">
                            <label class="form-control-label col-sm-2">&nbsp;</label>
                            <div class="col-sm-5 align-self-right ml-3"><p><a href="#" class="btn btn-primary btn-sm">Edit Info</a></p></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
