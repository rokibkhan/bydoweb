<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    String memberName = "";
    String membershipId = "";
    String memberPhone = "";
    java.util.Date dob = new Date();

    String billNo = "";
    String billYear = "";
    String billAmount = "0";
    int status = 0;
    String status1 = "";
    String dueDate = "";
    String paymentDate = "";
    String billType = "";

    String tillDate = "";
    MemberFee mpi = null;


%>

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>

<script type="text/javascript">
    function printInvoiceInfo(arg1) {
        var divToPrint = document.getElementById("divToPrint" + arg1);
        var popupWin = window.open('', '_blank', 'width=500,height=600');
        popupWin.document.open();
        popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
        popupWin.document.close();
    }
</script>


<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Payment Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->

                    <%

                        String totalPaymentDue = "0";
                        String dueSumSQL = "select sum(amount) from member_fee  WHERE  member_id=" + memberIdH + " AND status ='0'";

                        totalPaymentDue = dbsession.createSQLQuery(dueSumSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(dueSumSQL).uniqueResult().toString();
                        //   totalPaymentDue = Integer.parseInt(totalPaymentDue1);

                        System.out.println("Member Deatils :: totalPaymentDue :: " + totalPaymentDue);

                        Query totalTransIdSQL = null;
                        Object[] totalTransIdObj = null;
                        String totalTransIdFeeId = "";
                        String totalTransIdFeeIdTotal = "";
                        int totalTransIdFeeIdCount = 0;
                        int m = 1;
                        totalTransIdSQL = dbsession.createSQLQuery("SELECT * FROM member_fee  WHERE  member_id=" + memberIdH + " AND status ='0'");

                        if (!totalTransIdSQL.list().isEmpty()) {
                            for (Iterator totalTransIdItr = totalTransIdSQL.list().iterator(); totalTransIdItr.hasNext();) {
                                totalTransIdObj = (Object[]) totalTransIdItr.next();

                                totalTransIdFeeId = totalTransIdObj[0].toString();

                                if (m == 1) {
                                    totalTransIdFeeIdTotal = totalTransIdFeeIdTotal + totalTransIdFeeId;
                                } else {
                                    totalTransIdFeeIdTotal = totalTransIdFeeIdTotal + "-" + totalTransIdFeeId;
                                }

                                m++;
                                totalTransIdFeeIdCount++;
                            }
                        } else {
                            totalTransIdFeeIdTotal = "";
                        }


                    %>


                    <div class="container">
                        <div class="row my-4">
                            <div class="col-12">
                                <div class="card-header panel_heading_saz" style="padding-top: 4px; padding-bottom: 4px;">
                                    <i class="fa fa-list-alt"></i> Total Due Information
                                </div>
                            </div>

                        </div>
                    </div>

                    <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberPaymentProcessingTotalAmount.jsp?sessionid=<%=session.getId()%>" onSubmit="return fromDataSubmitValidation()">


                        <input value="total" name="memberPaymentType" id="memberPaymentType" type="hidden">
                        <input value="<%=totalTransIdFeeIdTotal%>" name="totalTransIdFeeIdTotal" id="totalTransIdFeeIdTotal" type="hidden">
                        <input value="<%=totalTransIdFeeIdCount%>" name="totalTransIdFeeIdCount" id="totalTransIdFeeIdCount" type="hidden">

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">Total Amount</label>

                            <div class="col-4">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">TK</span>
                                    </div>
                                    <input value="<%=totalPaymentDue%>" name="memberTypeTotalAmount" id="memberTypeTotalAmount" type="text" class="form-control" style="padding-top: .2rem ; padding-bottom: .2rem ;" disabled>  

                                    <input value="<%=totalPaymentDue%>" name="memberTypeTotalAmount1" id="memberTypeTotalAmount1" type="hidden">  
                                    <div class="input-group-append">
                                        <span class="input-group-text">.00</span>
                                    </div>
                                </div>                                
                                <div id="memberTypeTotalAmountErr"></div>
                            </div>


                        </div> 

                        <div class="form-group row">
                            <div class="col-sm-4 offset-sm-3">
                                <% if (!totalPaymentDue.equals("0")) {%>
                                <button type="submit" class="btn btn-primary btn-sm">Payment</button>
                                <% } %>
                            </div>                            
                        </div>
                    </form>



                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                        <thead>
                            <tr>
                                <th scope="col">Sl. No.</th>
                                <th scope="col">Bill No.</th>
                                <th scope="col">Bill Year</th>
                                <th scope="col">Payment Date</th>
                                <th scope="col">Bill Type</th>
                                <th scope="col">Bill Amount</th>
                                <th scope="col">Due Date</th>
                                <th scope="col">View Bill</th>
                                <th scope="col">Status</th> 
                                <!--                                <th scope="col">Pay Online</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <%

                                Query feeSQL = null;
                                Object[] feeObject = null;

                                String memberShipFeeTransId = "";
                                String memberShipFeeMemberId = "";
                                String memberShipFeePaidDate = "";
                                String memberShipFeeTxnDate = "";
                                String memberShipFeeAmount = "";
                                String memberShipFeeRefNo = "";
                                String memberShipFeeRefDesc = "";
                                String memberShipFeeStatus = "";
                                String memberShipFeeStatus1 = "";
                                String memberShipFeeCurrency = "BDT";
                                String payOnline = "";
                                String divToPrint = "";

                                feeSQL = dbsession.createSQLQuery("select f.txn_id,m.member_id,m.member_name,m.mobile, "
                                        + "f.ref_no,f.ref_description,f.txn_date,f.paid_date,f.amount,"
                                        + "f.status,f.due_date,f.bill_type,f.pay_option,y.member_fees_year_name  "
                                        + "FROM member_fee f,member_fees_year y,member m "
                                        + "WHERE f.member_id = m.id AND f.fee_year=y.member_fees_year_id AND "
                                        + "f.member_id= '" + memberIdH + "' "
                                        + "AND f.status IN(0,1) "
                                        + "ORDER BY f.txn_id DESC");

                                if (!feeSQL.list().isEmpty()) {
                                    int ip = 1;
                                    int i = 1;

                                    for (Iterator feeItr = feeSQL.list().iterator(); feeItr.hasNext();) {
                                        feeObject = (Object[]) feeItr.next();
                                        memberShipFeeTransId = feeObject[0].toString();
                                        membershipId = feeObject[1].toString();
                                        memberName = feeObject[2] == null ? "" : feeObject[2].toString();
                                        memberPhone = feeObject[3] == null ? "" : feeObject[3].toString();

                                        memberShipFeeRefNo = feeObject[4] == null ? "" : feeObject[4].toString();
                                        memberShipFeeRefDesc = feeObject[5] == null ? "" : feeObject[5].toString();

                                        memberShipFeeTxnDate = feeObject[6] == null ? "" : feeObject[6].toString();

                                        memberShipFeePaidDate = feeObject[7] == null ? "" : feeObject[7].toString();
                                        memberShipFeeAmount = feeObject[8].toString();

                                        billType = feeObject[11] == null ? "" : feeObject[11].toString();
                                        billYear = feeObject[13] == null ? "" : feeObject[13].toString();

                                        memberShipFeeStatus = feeObject[9] == null ? "" : feeObject[9].toString();

                                        System.out.println("memberShipFeeStatus :: " + memberShipFeeStatus);

                                        if (memberShipFeeStatus.equals("1")) {
                                            //memberShipFeeStatus1 = "<span class=\"btn btn-primary btn-sm waves-effect\">Paid</span>";
                                            memberShipFeeStatus1 = "Paid";
                                        }
                                        if (memberShipFeeStatus.equals("0")) {
                                            //  memberShipFeeStatus1 = "<span class=\"btn btn-danger btn-sm waves-effect\">Due</span>";
                                            memberShipFeeStatus1 = "Due";
                                            payOnline = "<a title=\"Pay Online\" href=\"" + GlobalVariable.baseUrl + "/member/memberPaymentProcessing.jsp?sessionid=" + session.getId() + "&act=add&&memberShipFeeTransId=" + memberShipFeeTransId + "\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-credit-card\"></i></a>";
//

                                        }
                                        if (memberShipFeeStatus.equals("2")) {
                                            //   memberShipFeeStatus1 = "<span class=\"btn btn-info btn-sm waves-effect\">Fail</span>";
                                            memberShipFeeStatus1 = "Fail";

                                        }

                                        if (memberShipFeeStatus.equals("")) {
                                            //  memberShipFeeStatus1 = "<span class=\"btn btn-danger btn-sm waves-effect\">Null</span>";
                                            memberShipFeeStatus1 = "Null";

                                        }
                                        //      billYear = mpi.getMemberFeesYear().getMemberFeesYearName() == null ? "" : mpi.getMemberFeesYear().getMemberFeesYearName();

//                                    billType = mpi.getBillType();
//                                    dueDate = mpi.getDueDate() == null ? "" : mpi.getDueDate().toString();
//                                    paymentDate = mpi.getPaidDate() == null ? "" : mpi.getPaidDate().toString();
                                        divToPrint = "<div id=\"divToPrint" + memberShipFeeTransId + "\" style=\"display:none;\"><!-- Start:  Print Container  -->"
                                                + "<html>"
                                                + "<head><title>Bill Invoice Print</title></head>"
                                                + "<body>"
                                                + "<div style=\"width: 790px; margin: 0px auto; margin-top: 1.5cm;\">"
                                                + "<table style=\"width: 95%; border-bottom:2px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                + "<tr>"
                                                + "<td colspan=\"2\" style=\"text-align: left; padding: 5px;\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/logo.png\" alt=\"IEB\"></td>"
                                                + "<td style=\"text-align: center;\">"
                                                + "<span style=\"font-size: 25px; font-weight: bold;\">BILL INVOICE</span><br>"
                                                + "<span style=\"font-size: 20px; font-weight: bold;\">The Institution of Engineers, Bangladesh (IEB)</span>"
                                                + "</td>"
                                                + "<td  colspan=\"2\" style=\"text-align: center;\">&nbsp;<br/></td>"
                                                + "</tr>"
                                                + "</table>"
                                                + "<table style=\"width: 95%; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                + "<tr>"
                                                + "<td style=\"text-align: center;\">"
                                                + "<table>"
                                                + "<tr><td style=\"text-align: left;\"><strong>Name </strong></td><td>:" + memberName + "</td></tr>"
                                                + "<tr><td style=\"text-align: left;\"><strong>Member ID </strong></td><td>:" + membershipId + "</td></tr>"
                                                + "<tr><td style=\"text-align: left;\"><strong>Phone </strong></td><td>:" + memberPhone + "</td></tr>"
                                                + "</table>"
                                                + "</td>"
                                                + "<td style=\"text-align: center;\">"
                                                + "<table>"
                                                + "<tr><td><strong>Date</strong></td><td>:" + memberShipFeeTxnDate + "</td></tr>"
                                                + "<tr><td><strong>Invoice No.</strong></td><td>:" + memberShipFeeTransId + "</td></tr>"
                                                + "<tr><td><strong>Status.</strong></td><td>:" + memberShipFeeStatus1 + "</td></tr>"
                                                + "</table>"
                                                + "</td>"
                                                + "</tr>"
                                                + "</table>"
                                                + "<table style=\"width: 95%; border: 1px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                + "<thead>"
                                                + "<tr>"
                                                + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Year</th>"
                                                + "<th style=\"border: 1px solid #ccc; text-align: center;\">Bill Type</th>"
                                                + "<th style=\"border: 1px solid #ccc; text-align: center;\">Amount</th>"
                                                + "</tr>"
                                                + "</thead>"
                                                + "<tbody style=\"font-size: 14px; text-align: center;\">"
                                                + "<tr>"
                                                + "<td style=\"border: 1px solid #ccc;\">" + billYear + "</td>"
                                                + "<td style=\"border: 1px solid #ccc;\">" + billType + "</td>"
                                                + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                + "</tr>"
                                                + "<tr>"
                                                + "<td colspan=\"2\" style=\"border: 1px solid #ccc; text-align: right; padding: 5px;\">Total :</td>"
                                                + "<td style=\"border: 1px solid #ccc;\">" + memberShipFeeAmount + "</td>"
                                                + "</tr>"
                                                + "</tbody>"
                                                + "</table> <!--End : invoice product list details-->"
                                                + "<table style=\"width: 95%; border-top:2px solid #ccc; border-collapse: collapse; margin-bottom: 20px;\"> <!--Start : invoice product list details-->"
                                                + "<tr>"
                                                + "<td colspan=\"2\" style=\"text-align: left; padding: 5px; text-align: center;\">"
                                                + "<strong>The Institution of Engineers, Bangladesh (IEB) </strong><br>"
                                                + "Rama,Dhaka-1000,Bangladesh.<br>"
                                                + "Call: 88-02-9559485, 88-02-9566336<br> E-mail: info@iebbd.org,Web: www.iebbd.org<br><br>"
                                                + "</td>"
                                                + "</tr>"
                                                + "</table>"
                                                + "</div>"
                                                + "</body>"
                                                + "</html>"
                                                + "</div><!-- End:  Print Container  -->";


                            %>
                            <tr>
                                <td><%=i%></td>
                                <td><%=memberShipFeeTransId%></td>
                                <td><%=billYear%></td>
                                <td><%=memberShipFeePaidDate%></td>
                                <td><%=billType%></td> 
                                <td><%=memberShipFeeAmount%></td>
                                <td><%=dueDate%></td> 
                                <td style="cursor:pointer">
                                    <a onclick="printInvoiceInfo('<%=memberShipFeeTransId%>');"  class="btn btn-default btn-sm">&nbsp; <i class="fa fa-file"></i>&nbsp; </a>

                                    <%=divToPrint%>
                                </td>

                                <td><%=memberShipFeeStatus1%></td> 
<!--                                <td  ><%//=payOnline%></td>-->

                            </tr>

                            <%                                i++;
                                    }
                                }

                                dbsession.flush();
                                dbsession.close();

                            %>


                        </tbody>
                    </table>




                    <!-- ============ End Education information Table ================== -->


                </div>
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
