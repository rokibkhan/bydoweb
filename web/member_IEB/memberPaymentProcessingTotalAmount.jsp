<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String memberFeeId = request.getParameter("memberFeeId") == null ? "" : request.getParameter("memberFeeId").trim();

    Query q1 = null;
    String instituteName = "";
    String boardUniversityName = "";
    java.util.Date dob = new Date();

    String billNo = "";
    String billYear = "";
    String billAmount = "0";
    int status = 0;
    String status1 = "";
    java.util.Date dueDate = null;
    java.util.Date paymentDate = null;
    String billType = "";

    String tillDate = "";
    MemberFee mpi = null;

    int i = 1;
    q1 = dbsession.createQuery("from MemberFee WHERE  member_id=" + memberIdH + " and txnId='" + memberFeeId + "'  ");

    for (Iterator itr = q1.list().iterator();
            itr.hasNext();) {
        mpi = (MemberFee) itr.next();
        billAmount = mpi.getAmount() == null ? "0" : mpi.getAmount().toString();

        billYear = mpi.getMemberFeesYear().getMemberFeesYearName() == null ? "" : mpi.getMemberFeesYear().getMemberFeesYearName();
        status = mpi.getStatus();
        if (status == 1) {
            status1 = "Paid";
        } else {
            status1 = "Due";
        }
        billType = mpi.getBillType();
        dueDate = mpi.getDueDate();
        paymentDate = mpi.getPaidDate();
    }

    String memberTypeTotalAmount = request.getParameter("memberTypeTotalAmount1") == null ? "" : request.getParameter("memberTypeTotalAmount1").trim();
    String totalTransIdFeeIdTotal = request.getParameter("totalTransIdFeeIdTotal") == null ? "" : request.getParameter("totalTransIdFeeIdTotal").trim();
    String totalTransIdFeeIdCount = request.getParameter("totalTransIdFeeIdCount") == null ? "" : request.getParameter("totalTransIdFeeIdCount").trim();

%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Payment Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->





                    <!-- ============ End Education information Table ================== -->
                    <p class="content_title_saz pb-2 m-3">Total Amout Payment Processing</p>
                    <div class="row">
                        <div class="col-sm-8">
                            <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="memberPaymentProcessingTotalAmountSubmitData.jsp?sessionid=<%=session.getId()%>&act=edit" onSubmit="return fromDataSubmitValidation()">

                                <input value="total" name="memberPaymentType" id="memberPaymentType" type="hidden">
                                <input value="<%=totalTransIdFeeIdTotal%>" name="totalTransIdFeeIdTotal" id="totalTransIdFeeIdTotal" type="hidden">
                                <input value="<%=totalTransIdFeeIdCount%>" name="totalTransIdFeeIdCount" id="totalTransIdFeeIdCount" type="hidden">


                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-3 ml-3 font-weight-bold">Bill Type</label>
                                    <div class="col-md-6">
                                        <input name="billYear" id="billYear" value="Total Due"  type="text" readonly=""  class="form-control" required="">                                                                                                              
                                    </div>
                                </div>


                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-md-3 ml-3 font-weight-bold">Amount (Tk.)</label>
                                    <div class="col-md-6">
                                        <input name="memberTypeTotalAmount" id="memberTypeTotalAmount" value="<%=memberTypeTotalAmount%>" placeholder="Total Amount" type="text" readonly="" class="form-control">                                    
                                    </div>
                                </div>

                                <div class="form-group d-flex align-items-center">
                                    <label class="form-control-label col-sm-3 ml-3 font-weight-bold">&nbsp;</label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-primary btn-sm ">Pay</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                    <!-- ============ End personal information form ================== -->




                </div>
            </div>
        </div>
    </div>
</div>

<%

    dbsession.flush();
    dbsession.close();
%>

<%@ include file="../footer.jsp" %>
