<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    Query q2 = null;

    MemberUpgradeRequest mupreg = new MemberUpgradeRequest();

    Member member = null;
    int upgradeReqId = 0;
    String upgradeReqDate = "";
    String upgradeReqFor = "";
    String upgradeReqDocOrgName = "";
    String upgradeReqDocName = "";
    String upgradeReqStatus = "";
    String upgradeReqStatus1 = "";

    

%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css"/>

<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">


<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Membership Upgrade Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        
                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->


                    <p class="content_title_saz pb-2 m-3">Upgrade Request Information</p>

                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                        <thead>
                            <tr>
                                <th scope="col">Request Date</th>
                                <th scope="col">Request For</th>
                                <th scope="col">Status</th>
                                <th scope="col">Documents</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%

                                Query upgradeReqSQL = dbsession.createQuery("from MemberUpgradeRequest WHERE  member_id=" + memberIdH + " ");

                                for (Iterator upgradeReqItr = upgradeReqSQL.list().iterator(); upgradeReqItr.hasNext();) {
                                    mupreg = (MemberUpgradeRequest) upgradeReqItr.next();

                                    upgradeReqId = mupreg.getId();
                                    upgradeReqDate = mupreg.getRequestDate().toString();
                                    upgradeReqFor = mupreg.getMemberTypeInfo().getMemberTypeName();
                                    upgradeReqDocOrgName = mupreg.getDocumentOrgName().toString();
                                    upgradeReqDocName = mupreg.getDocumentName().toString();
                                    upgradeReqStatus = mupreg.getStatus().toString();

                                    if (upgradeReqStatus.equals("0")) {
                                        upgradeReqStatus1 = "Pending";
                                    } else {
                                        upgradeReqStatus1 = "Approved";
                                    }

                            %>
                            <tr> 
                                <td><%=upgradeReqDate%></td>
                                <td><%=upgradeReqFor%></td> 
                                <td><%=upgradeReqStatus1%></td> 
                                <td><a title="View" href="<%out.print(GlobalVariable.baseUrl);%>/upload/document/<%=upgradeReqDocName%>" target="_blank" ><%=upgradeReqDocOrgName%> </a> </td> 
                                
                            </tr>

                            <%
                                }

                            %>
                        </tbody>
                    </table>






                    <!-- ============ End Education information Table ================== -->

                    <div class="container">
                        <div class="row my-4">
                            <div class="col-12">
                                <div class="card-header panel_heading_saz" style="padding-top: 4px; padding-bottom: 4px;">
                                    <i class="fa fa-list-alt"></i> Add Upgrade Require Information
                                </div>
                            </div>

                        </div>
                    </div>



                    <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/MemberUpgradeDocumentsUpload" enctype="multipart/form-data">

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">Upgrade To :</label>
                            <div class="col-sm-4">
                                <select name="memberTypeId" id="memberTypeId" class="form-control" required="">
                                    <option value="">Select member type</option>
                                    <%

                                        Object[] object5 = null;
                                        String memberTypeIdX = "";
                                        String dmemberTypeNameX = "";
                                        Query q5 = dbsession.createSQLQuery("select * FROM member_type_info ORDER BY member_type_id ASC");
                                        for (Iterator itr5 = q5.list().iterator(); itr5.hasNext();) {
                                            object5 = (Object[]) itr5.next();
                                            memberTypeIdX = object5[0].toString();
                                            dmemberTypeNameX = object5[1].toString();
                                    %>
                                    <option value="<%=memberTypeIdX%>"><%=dmemberTypeNameX%></option>

                                    <%
                                        }

                                    %>

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">Required Document</label>

                            <input name="documentTypeNID" id="documentTypeNID" value="9"  type="hidden">
                            <input name="memberNID" id="memberNID" value="<%=memberIdH%>"  type="hidden">
                            <div class="col-4">
                                <input name="fileNID" id="fileNID" onchange="upgradationDocumentUplaodFileTypeSize();"  type="file" class="form-control input-sm" style="padding-top: .2rem ; padding-bottom: .2rem ;"  required>  
                                <div id="fileNIDErr"></div>
                            </div>

                        </div> 





                        <div class="form-group row">

                            <div class="col-sm-6 offset-sm-3">
                                <button type="submit" class="btn btn-primary mr-3">Submit</button>
                            </div>
                        </div>





                    </form>
                    <!-- ================ End billing information form ================= -->
                    <div class="container">
                        <div class="row my-4">
                            <div class="col-12">
                                <p class="text-dark " style="font-size: 0.8rem;">Note |: Only .pdf file and maximum file size 10MP  allow. Please add minimum 2000 words report of experience.  
                                      </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.jquery.js"></script>
<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.js"></script>
<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);
%>/commonUtil/plugins/bower_components/chosen/docsupport/init.js"></script>

<%
    dbsession.flush();
    dbsession.close();
%>

<%@ include file="../footer.jsp" %>
