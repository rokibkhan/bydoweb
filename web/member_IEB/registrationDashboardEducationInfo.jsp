<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String registerAppId = "";
    String memberRegId = "";
    if (session.getAttribute("registerAppId") != null && session.getAttribute("memberRegId") != null) {

        sessionIdH = session.getId();
        registerAppId = session.getAttribute("registerAppId").toString();
        memberRegId = session.getAttribute("memberRegId").toString();

    } else {

        response.sendRedirect(GlobalVariable.baseUrl + "/member/registrationCheck.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    Query q2 = null;
    int degreeId = 0;
    String degreeName = "";
    String instituteName = "";
    String boardUniversityName = "";
    java.util.Date dob = new Date();
    String yearOfPassing = "";
    String resultTypeName = "";
    String result = "";

    MemberEducationInfoTemp mei = null;

    q1 = dbsession.createQuery("from MemberEducationInfoTemp where  member_id=" + registerAppId + " ");

    MemberTemp member = null;
    String memberSubdivisionName = "";
    String memberSubdivisionFullName = "";
    int educationfInfoId = 0;

    

%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css"/>

<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">


<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="registrationLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Education Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->



                    <table class="table table-hover table-striped table-bordered my-5" style="width:98%;margin:10px;">
                        <thead>
                            <tr>
                                <th scope="col">Degree</th>
                                <th scope="col">Board/University</th>
                                <th scope="col">Year of Passing</th>
                                <th scope="col">Score/Class</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <%
                                for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                                    mei = (MemberEducationInfoTemp) itr.next();

                                    educationfInfoId = mei.getId();
                                    degreeId = mei.getDegree().getDegreeId();
                                    degreeName = mei.getDegree().getDegreeName();

                                    instituteName = mei.getInstituteName() == null ? "" : mei.getInstituteName();

                                    boardUniversityName = mei.getUniversity().getUniversityLongName();
                                    yearOfPassing = mei.getYearOfPassing();
                                    resultTypeName = mei.getResultType().getResultTypeName();
                                    result = mei.getResult();

//                                    if (degreeId == 3 || degreeId == 4) {
//
//                                        q2 = dbsession.createQuery("from MemberTemp as member WHERE id=" + registerAppId + " ");
//
//                                        for (Iterator itr2 = q2.list().iterator(); itr2.hasNext();) {
//                                            member = (MemberTemp) itr2.next();
//
//                                         //   memberSubdivisionName = member.getSubDivision().getSubDivisionName();
//                                         //   memberSubdivisionFullName = member.getSubDivision().getFullName();
//                                        // memberSubdivisionName = member.getS
//                                        }
//                                    } else {
                                    memberSubdivisionFullName = "";
                                    //                                   }

                                    degreeName = degreeName + memberSubdivisionFullName;
                            %>
                            <tr>
                                <td><%=degreeName%></td>
                                <td><%=boardUniversityName%></td>
                                <td><%=yearOfPassing%></td>
                                <td><%=result%></td>
                                <td style="cursor:pointer">

                            </tr>
                            <%
                                }


                            %>

                        </tbody>
                    </table>



                    <!-- ================ End billing information form ================= -->

                </div>
            </div>
        </div>
    </div>
</div>

<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.jquery.js"></script>
<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.js"></script>
<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/init.js"></script>



<%@ include file="../footer.jsp" %>
