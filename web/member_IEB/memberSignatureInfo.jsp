<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        //     response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);
    } else {
        System.out.println("Header LogIn Required :: ");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query q1 = null;
    Query q2 = null;

    MemberSignature msig = new MemberSignature();

    Member member = null;
    int signatureId = 0;
    String signatureOrgName = "";
    String signatureFileName = "";
    String signatureFileName1 = "";

    

%>


<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/saz.css" rel="stylesheet" type="text/css"/>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/b4_style.css" rel="stylesheet" type="text/css"/>


<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/jqueryui/jquery-ui.min.css"/>

<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.css">
<!--Chosen CSS -->
<link rel="stylesheet" href="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.css">
<script type="text/javascript">
    function signatureDeleteInfo(arg1, arg2) {

        console.log("signatureDeleteInfo -> arg1 :: " + arg1 + " arg2:: " + arg2);

        var memberLGModal, taskLGModal, formX, btnInvInfo, url, sessionid, cccll;

        btnInvInfo = "<span id=\"errMsgShow\"><span><a id=\"btnDeleteComfirmation\" onclick=\"signatureDeleteInfoConfirm('" + arg1 + "','" + arg2 + "')\" class=\"btn btn-primary\">Confirm </a>&nbsp;"
                + "<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>";


        memberLGModal = $('#memberLGModal');
        //   memberLGModal = $('#taskLGModal');
        memberLGModal.find("#memberLGModalTitle").text("Signature Delete Confirmation");
        memberLGModal.find("#memberLGModalBody").html("");
        memberLGModal.find("#memberLGModalFooter").html(btnInvInfo);
        memberLGModal.modal('show');


    }



    function signatureDeleteInfoConfirm(arg1, arg2) {
        console.log("signatureDeleteInfoConfirm:: " + arg1);
        var memberLGModal;
        memberLGModal = $('#memberLGModal');



        $.post("signatureDeleteProcess.jsp", {deleteId: arg1, sessionid: arg2}, function (data) {

            console.log(data);

            if (data[0].responseMsgCode == 1) {
                $("#globalAlertInfoBoxConTT").html(data[0].responseMsgHTML).show().delay(3000).fadeOut("slow");
                $("#infoBox" + data[0].selectInvId).html("<td colspan=5 style=\"text-align: center;\"><h4>" + data[0].responseMsg + "</h4></td>");
                memberLGModal.modal('hide');
            } else {

            }


        }, "json");

    }

</script>

<br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <%@ include file="memberLeftSide.jsp" %>
        </div>

        <div class="col-sm-9">
            <div class="card card-info mb-3">
                <div class="card-header panel_heading_saz"><i class="fa fa-history"></i> Member Signature Information</div>
                <div class="card-block globalAlertInfoBoxConParentTT" style="min-height: 533px;">
                    <%                        String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();
                        String msgDispalyConT, msgInfoText, sLinkOpt;
                        if (!strMsg.equals("")) {
                            msgDispalyConT = "style=\"display: block; margin-top:5px;\"";
                            msgInfoText = "<strong>" + strMsg + "</strong> ";

                        } else {
                            msgDispalyConT = "style=\"display: none;\"";
                            msgInfoText = "";
                        }
                    %>

                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->


                    <p class="content_title_saz pb-2 m-3">Signature Information</p>

                    <table class="table table-hover table-bordered my-5" style="width:98%;margin:10px;">
                        <thead>
                            <tr>
                                <th scope="col">File Name</th>
                                <th scope="col">View</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                int singatureCounter = 0;
                                Query signatureSQL = dbsession.createQuery("from MemberSignature WHERE  member_id=" + memberIdH + " ");

                                for (Iterator signatureItr = signatureSQL.list().iterator(); signatureItr.hasNext();) {
                                    msig = (MemberSignature) signatureItr.next();

                                    signatureId = msig.getId();

                                    signatureOrgName = msig.getSignatureOrgName().toString();
                                    signatureFileName = msig.getSignatureFileName().toString();

                            %>
                            <tr id="infoBox<%=signatureId%>">
                                <td><%=signatureOrgName%></td>                           
                                <td><a title="View" href="<%out.print(GlobalVariable.baseUrl);%>/upload/document/<%=signatureFileName%>" target="_blank" ><%=signatureOrgName%> </a> </td> 
                                <td>
                                    <a title="Remove" onclick="signatureDeleteInfo(<% out.print("'" + signatureId + "','" + sessionIdH + "'");%>)" ><i class="fa fa-trash"></i> </a> 

                                </td> 

                            </tr>

                            <%
                                    singatureCounter++;
                                }

                            %>
                        </tbody>
                    </table>






                    <!-- ============ End Education information Table ================== -->

                    <div class="container">
                        <div class="row my-4">
                            <div class="col-12">
                                <div class="card-header panel_heading_saz" style="padding-top: 4px; padding-bottom: 4px;">
                                    <i class="fa fa-list-alt"></i> Upload Signature Information
                                </div>
                            </div>

                        </div>
                    </div>



                    <form class="form-horizontal profile_form_horizontal_saz p_form" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/MemberSignatureUpload" enctype="multipart/form-data">



                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label text-right">Signature file</label>


                            <input name="memberNID" id="memberNID" value="<%=memberIdH%>"  type="hidden">
                            <div class="col-4">
                                <input name="fileNID" id="fileNID" onchange="upgradationDocumentUplaodFileTypeSize();"  type="file" class="form-control input-sm" style="padding-top: .2rem ; padding-bottom: .2rem ;"  required>  
                                <div id="fileNIDErr"></div>
                            </div>

                        </div> 

                        <%
                            String btnSignatureDisable = "";
                            if (singatureCounter > 0) {
                                btnSignatureDisable = "disabled";
                            } else {
                                btnSignatureDisable = "";
                            }
                        %>



                        <div class="form-group row">

                            <div class="col-sm-6 offset-sm-3">
                                <button type="submit" class="btn btn-primary mr-3" <%=btnSignatureDisable%> >Submit</button>
                            </div>
                        </div>





                    </form>
                    <!-- ================ End billing information form ================= -->
                    <div class="container">
                        <div class="row my-4">
                            <div class="col-12">
                                <p class="text-dark " style="font-size: 0.8rem;">Note |: Only .pdf file and maximum file size 10MP  allow.  
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/chosen.jquery.js"></script>
<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/chosen/docsupport/prism.js"></script>
<!--Chosen JavaScript -->
<script src="<% out.print(GlobalVariable.baseUrl);
%>/commonUtil/plugins/bower_components/chosen/docsupport/init.js"></script>

<%
    dbsession.flush();
    dbsession.close();
    %>

<%@ include file="../footer.jsp" %>
