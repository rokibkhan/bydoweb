<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);

    }

   // System.out.println("Header sessionIdH :: " + sessionIdH);
 //   System.out.println("Header userNameH :: " + userNameH);
//    System.out.println("Header userStoreIdH :: " + memberIdH);

//    String sessionid = request.getParameter("sessionid").trim();
//
//    sessionid = session.getId();
//    String uid = session.getAttribute("username").toString().toUpperCase();    
//    String userStoreId = session.getAttribute("memberId").toString();
//    String sessionidH = request.getParameter("sessionid").trim();
//
//    sessionidH = session.getId();
//    String uidH = session.getAttribute("username").toString().toUpperCase();    
    //   String userStoreIdH = session.getAttribute("memberId").toString();
//System.out.println("uidH :: "+uidH);
    

%>
<!-- Font Awesome CSS -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

<!-- Login & Register & About CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about.css">




<!-- Starting of login form -->
<section id="home-section">
    <div class="dark-overlay">
        <div class="home-inner-Tahajjatt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 d-none d-lg-block">
                        <h1 class="display-4"><strong>Member Login Instructions</strong></h1>
                        <div class="d-flex flex-row">
                            <div class="p-2 align-self-start">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="p-2 align-self-end lead">
                                Please Enter IEB Membership ID and password to Login into the  Member Account Dashboard. 
                                

                            </div>
                        </div>
                        

                        <div class="d-flex flex-row">
                            <div class="p-2 align-self-start">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="p-2 align-self-end lead">
                                If you are Fellow, Member or Associate Member, Member ID Should start with F, M or A, then 
                                put the numerical character without using and special characters like (/,-, space). Example: F54787. 
                                If your ID number has only 4 digits numerical character, then just add a "0" before the numerical 
                                character and after F, M or A, Example: M09239.)
                            </div>
                        </div>

                        <div class="d-flex flex-row">
                            <div class="p-2 align-self-start">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="p-2 align-self-end lead">
                                If you don't have the password Click Forgot password or Get existing password, then follow as instructed in the next page.
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 bg-primary1" style="background-color: #1880ae;">
                        <div class="login-form">
                            <div class="card m-3 bg-primary1" style="background-color: #1880ae;">
                                <div class="card-body text-center">
                                    <h2 class="card-title text-white" style="font-size: 35px;"> Member Login</h2>
                                    <!--                                    <p class="card-text">Please enter your email and password</p>-->
                                </div>
                            </div>
                            <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/loginServlet">
                                <div style="display: block1; font-size:  small;color: red;text-align: center; " >                
                                    <%
                                        String rtnUser = "";

                                        try {
                                            rtnUser = session.getAttribute("rtnUser").toString();
                                            if (rtnUser.equalsIgnoreCase("1") || rtnUser != null) {
                                                out.println("Member ID or Password does not match.Please follow the instructions listed in the left.");
                                            } else {
                                                out.print(" ");
                                            }
                                        } catch (Exception ex) {
                                            // out.print(ex.getMessage());
                                        }

                                        %>

                                </div>
                                <div class="form-group text-center">
                                    <input id="username" name="username" type="text" class="form-control"  placeholder="Member ID (Ex. F01234, M12345, A12345)" style="width: 300px;" required>
                                </div>

                                <div class="form-group text-center">
                                    <input id="password" name="password" type="password" class="form-control d-inline-flex"  placeholder="Password" style="width: 300px;" required>
                                </div>
                                <!--                                <div class="form-check m-2 text-center">
                                                                    <label class="form-check-label" for="defaultCheck1">
                                                                        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                                                        Remember me
                                                                    </label>
                                
                                
                                                                </div>-->
                                <button type="submit" class="submit-button btn btn-lg mx-auto font-weight-bold text-white">Login</button>

                                <div class="forgot" style="margin-bottom: 10px;">
                                    <a href="<%=GlobalVariable.baseUrl%>/member/forgotPassword.jsp" style="text-decoration: none;">Forgot password?</a>
                                </div>
                                
                                <div class="forgot" style="margin-bottom: 10px;">
                                    <a href="<%=GlobalVariable.baseUrl%>/member/forgotPassword.jsp" style="text-decoration: none;">Get existing member password</a>
                                </div>
                                
                                <div class="forgot">
                                    <a href="<%=GlobalVariable.baseUrl%>/member/registration.jsp" style="text-decoration: none;">Apply Membership</a>
                                </div>
                               
                            </form>


                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</section>

<!-- Ending of login form -->


<%@ include file="../footer.jsp" %>
