<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>




<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="get_about_banner_Tah">
    <h2 class="display_41_saz">IEB History</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#" class="text-white">About</a></li>
                <li class="breadcrumb-item active" aria-current="page">IEB History</li>
            </ol>
        </nav>
    </div>
</div>


<!-- Starting of "What We Do" Section -->
<div class="what-we-do">
    <div class="container">
        <div class="row">
            <div class="col-md-12 p-3 align-self-center mt-4"><br>

                <p style="color:#666666">The Institution of Engineers, Bangladesh (IEB) is the most prestigious National Professional Organization of the country. It is registered under the Societies Registration Act of the country. IEB includes all disciplines of engineering. Currently, it has in its roll more than 41,545 engineers with about 30% in the category of Fellows, 60% Members and the rest as Associate Members. In addition there are a good number of Student Members. Since its establishments, IEB has been promoting and disseminating knowledge and practice of engineering and science. On of the major goal of IEB is to ensure the professional excellence and continuous professional development of the engineers in the country. It has also been working relentlessly to establish close and co-operation with the other professional bodies both in Bangladesh and outside the country.
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/about_img.jpg" class="float-right do-img img-fluid p-5" alt="What we do.png" /><br>

                    The Institutions mission has always been to serve the teeming millions through the practice of engineering science and continuously improving the professional standards of its members. The IEB as a national forum of engineers also addresses the common problems confronting the engineers. On different occasions, IEB came forward with its clear vision of the problems of development and formulated specific suggestions on these issues. IEB prepared and presented its recommendations on the Power Sector Reform Policy, Flood Control and Management, Public Administration Reform, Traffic Problem, National Pay Scale, Information Technology, Renewable Energy, Problems of Dhaka Metropolis and Integrated Approach Towards Solution of Endemic Problem faced by the public etc. to the Government on different occasions. Besides, IEB is also playing a significant role for the professional development of its valued members.</p>
            </div>



        </div>
    </div>



</div><br><br>


<div class="container">
    <div class="row">
        <div class="col-md-6">
            <img class="mt-5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/aim1.jpg" style="height: 500px; width: 500px">
        </div>
        <div class="col-md-6">
            <h2> Aims and Objectives</h2>
            <p> Promote and advance the science, practice and business of engineering in all its branches throughout Bangladesh and abroad.

                Promote efficiency in the engineering practices and profession.

                Regulate the professional activities and assist in maintaining high standards in the general conduct of its members.

                Lay down professional code of ethics and to make it mandatory for its members to abide by the same in their professional conduct.

                Help in the acquisition and interchange of technical knowledge among its members.

                Promote the professional interests and social welfare of its members.

                Encourage original research in engineering and conservation and economic utilisation of the country's materials and resources.

                Foster co-ordination with similar institutions in other countries and engineering universities, institutions and colleges in Bangladesh and in other countries, for mutual benefits in furthering the objects of the Institution.

                Diffuse among its members information on all matters affecting engineering and to encourage, assist and extend knowledge and information.

                Promote the study of engineering with a view to disseminating information obtained, for facilitating scientific, engineering and economic development of Bangladesh.

                Co-operate with various Government Agencies and Industrial and Commercial Enterprises connected with engineering and advising them in matters concerning the profession and practices of engineering and promotion of technical education.</p>
        </div>
    </div>

</div><br>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <H2> Background</H2>
            <p>With the emergence of Bangladesh as an independent nation in 1971 The Institute of Engineers, Pakistan was renamed as The Institution of Engineers, Bangladesh, which was founded in 1948 with its Headquarters at Dhaka, now capital of Bangladesh. After the independence of Pakistan, a number of farsighted and dynamic senior engineers took initiative to establish a professional forum of Engineers. Amongst them were late Engr. M.A. Jabbar, Engr. Hatem Ali Khan, Engr. A. Latif, acted as the pioneers of engineering profession of this country and played an important role towards achieving this objective. In spite of several obstacles created different vested groups, the pioneers with their strong will and determination succeeded in establishing the Institute of Engineers and got its foundation stone laid by the Governor General of Pakistan on 7th May 1948 at Dhaka.</p>
        </div>
        <div class="col-md-6">
            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/back.jpg" style="height: 350px">
        </div>
    </div>

</div><br><br>

<div class="container">
    <div>
        <h2> Organizational Structure:</h2>
        <p>With the increase in number of members and activities over the years, the Institution has grown considerably. Activities of The Institution have been expanding every year. A number of Centres and Sub-Centres have been established to cater the needs of the members. Within the country IEB has 18 Centres, 31 Sub-Centres and 10 Overseas Chapters, which are listed below :</p>
    </div>
</div><br><br>




<div class="container">

    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Centre</th>
                <th scope="col">#</th>
                <th scope="col">Centre</th>
                <th scope="col">#</th>
                <th scope="col">Centre</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Dhaka</td>
                <td>7</td>
                <td>Barisal</td>
                <td>13</td>
                <td>Narayangonj</td>
            </tr>

            <tr>
                <th scope="row">2.</th>
                <td>Chittagong</td>
                <td>8.</td>
                <td>Mymensingh</td>
                <td>14</td>
                <td>Rangadia</td>
            </tr>
            <tr>
                <th scope="row">3.</th>
                <td>Khulna</td>
                <td>9.</td>
                <td>Rangpur</td>
                <td>15.</td>
                <td>Jessore</td>
            </tr>
            <tr>
                <th scope="row">4.</th>
                <td>Rajshahi</td>
                <td>10.</td>
                <td>Ghorashal</td>
                <td>16.</td>
                <td>Ashugonj</td>
            </tr>

            <tr>
                <th scope="row">5.</th>
                <td>Comilla</td>
                <td>11.</td>
                <td>Bogra</td>
                <td>17.</td>
                <td>Faridpur</td>
            </tr>
            <tr>
                <th scope="row">6.</th>
                <td>Sylhet</td>
                <td>12.</td>
                <td>Gazipur</td>
                <td>18.</td>
                <td>Dinajpur</td>
            </tr>

        </tbody>
    </table>

</div>




<%@ include file="../footer.jsp" %>
