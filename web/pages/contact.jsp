<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>


<!-- Font Awesome CSS -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Our Custom CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/photo_contact_success_style.css">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>


<div class="get_contact_banner_Tah">
    <h2 class="display_41_saz text-white">Contact Us</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#" class="text-white">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contact</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of banner header -->
<!-- Start Contact Section -->
<section class="contact py-5" id="contact">
    <div class="container">

        <div class="row">
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                <div class="contact-single bg-light py-5 text-center">
                    <i class="fas fa-location-arrow"></i>
                    <h5 class="py-3 font-weight-bold">Headquarters</h5>
                    <div class="border border-dark w-25 mx-auto"></div>
                    <p class="pt-3">Rama,Dhaka-1000</p>
                    <p>Bangladesh.</p>
                </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                <div class="contact-single bg-light py-5 text-center">
                    <i class="fas fa-phone"></i>
                    <h5 class="py-3 font-weight-bold">Phone</h5>
                    <div class="border border-dark w-25 mx-auto"></div>
                    <p class="pt-3">88-02-9559485, 88-02-9566336</p>
                    <p>88-02-9556112, 88-02-9573343</p>
                </div>
            </div>

            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                <div class="contact-single bg-light py-5 text-center">
                    <i class="fas fa-fax"></i>
                    <h5 class="py-3 font-weight-bold">Fax</h5>
                    <div class="border border-dark w-25 mx-auto"></div>
                    <p class="pt-3">88-02-9562447</p>
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                <div class="contact-single bg-light py-5 text-center">
                    <i class="far fa-envelope"></i>
                    <h5 class="py-3 font-weight-bold">Email</h5>
                    <div class="border border-dark w-25 mx-auto"></div>
                    <p class="pt-3">iebhq@gmail.com</p>
                    <p>info@iebbd.org</p>
                </div>
            </div>

        </div>

    </div>

</section>
<!-- End Contact Section -->
<section class="contact1 py-5 bg-primary1" id="contact" style="background-color: #17519C;">
    <div class="container">

        <div class="row">

            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 offset-md-4">
                <h3 class="text-center text-white py-3">Office Time</h3>
                <div class="border border-light w-25 mx-auto"></div>
                <p class="text-center text-white py-1">Saturday to Thursday at 2:00 PM to 9:00 PM</p>
                <p class="text-center text-white">(Closed Friday and Government Holidays only).</p>
                <p class="text-center text-white">*For Special Occasions IEB may be open.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">


            </div>

        </div>

    </div>
</section>



<section class="container-fluid bg-white" style="margin-bottom: 0;">
    <div class="container">
        <h2 class="text-center pt-5">Department wise Contact List</h2>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="text-center bg-info">
                    <tr>
                        <th>Department</th>
                        <th>Name & designation</th>
                        <th>Mobile/pabx</th>
                        <th>E-mail ID</th>
                        <th>Responsibility</th>
                    </tr>
                </thead>
                <tbody class="text-center bg-light">
                    <tr>
                        <td rowspan="5" class="align-middle font-weight-bold">ADMINISTRATION</td>
                        <td>Engr. Sunirmol Mondal <br /> Executive Officer</td>
                        <td>140</td>
                        <td>eo@iebbd.org</td>
                        <td>All kinds of Administrative Information.</td>
                    </tr>
                    <tr>
                        <td>Md. Mahtab Jamil <br /> Asstt. Executive Officer</td>
                        <td>128</td>
                        <td>Info.iebhq@gmail.com</td>
                        <td>All kinds of Administrative & International Information.</td>
                    </tr>
                    <tr>
                        <td>Md. Amir Hossain <br /> Executive Assistant <br /> (Establishment)</td>
                        <td>132</td>
                        <td></td>
                        <td>All kinds of Establishment Information.</td>
                    </tr>
                    <tr>
                        <td>Md. Ruhul Amin <br /> Executive Assistant <br /> (Dispatch)</td>
                        <td>127</td>
                        <td></td>
                        <td>All kinds of Dispatch Information.</td>
                    </tr>
                    <tr>
                        <td>Md. Saidul Haque <br /> Executive Assistant <br /> (Store)</td>
                        <td></td>
                        <td></td>
                        <td>All kinds of Store Information.</td>
                    </tr>
                    <tr>
                        <td rowspan="7" class="align-middle font-weight-bold">ACADEMIC & INTERNATIONAL</td>
                        <td>Md. Jashim Uddin <br /> Asstt. Executive Officer</td>
                        <td>118</td>
                        <td>prottoejashim@gmail.com</td>
                        <td>All kinds of Academic & Publication (Engineering News) help.</td>
                    </tr>
                    <tr>
                        <td>A. K. M. Sohrab Hossain Bhuiyan <br /> Librarian</td>
                        <td>123</td>
                        <td>eo@iebbd.org</td>
                        <td>All kinds of Archive & Library Information.</td>
                    </tr>
                    <tr>
                        <td rowspan="3">Mintu Dakua <br /> Executive Assistant <br /> (Membership) <br /> Md. Delwar Hossain <br /> Assistant <br /> Din. Md. Rasel Amin <br /> Jr. Assistant</td>
                        <td>146</td>
                        <td rowspan="3" class="align-middle">membership.ieb@gmail.com</td>
                        <td rowspan="3" class="align-middle">All Kinds of Membership Information.</td>
                    </tr>
                    <tr>
                        <td>01812141694 <br /> (156)</td>
                    </tr>
                    <tr>
                        <td>01920836067</td>
                    </tr>
                    <tr>
                        <td>Md. Sabir Ahmed <br /> Executive Assistant <br /> (Examination)</td>
                        <td>133</td>
                        <td>amie@iebbd.org</td>
                        <td rowspan="2" class="align-middle">All kinds of Examination Information.</td>
                    </tr>
                    <tr>
                        <td>Mohammad Hossain <br /> Sr. Assistant (Examination)</td>
                        <td>01912800512</td>
                        <td>info.iebamie@gmail.com</td>
                    </tr>
                    <tr>
                        <td rowspan="3" class="align-middle font-weight-bold">INFORMATION & TECHNOLOGY</td>
                        <td> <br /> IT Officer</td>
                        <td>138</td>
                        <td>prottoejashim@gmail.com</td>
                        <td rowspan="3" class="align-middle">All Kinds of Website & Membership Database Information.</td>
                    </tr>
                    <tr>
                        <td>Md. Shah Jalal <br /> Executive Assistant (IT)</td>
                        <td>01735799776 <br /> 138</td>
                        <td>shahjalal776@gmail.com</td>
                    </tr>
                    <tr>
                        <td>Md. Arafatul Islam <br /> IT Assistant</td>
                        <td>138</td>
                        <td>info@arafatit.com</td>
                    </tr>
                    <tr>
                        <td rowspan="4" class="align-middle font-weight-bold">ENGINEERING</td>
                        <td>Engr. Asif Abedin <br /> Construction & Maintenance Engineer</td>
                        <td>143</td>
                        <td>engr.asifabedin@gmail.com</td>
                        <td>All kinds of Engineering & Estate Information.</td>
                    </tr>
                    <tr>
                        <td>Md. Nurul Islam <br /> Estate Superintendent</td>
                        <td>144</td>
                        <td></td>
                        <td>All kinds of rent (Auditorium, Seminar room & Council hall) information.</td>
                    </tr>
                    <tr>
                        <td>Md. Belal Hossain <br /> Asstt. Construction & Maintenance Engineer</td>
                        <td>124</td>
                        <td></td>
                        <td>All kinds of Engineering & Estate Information.</td>
                    </tr>
                    <tr>
                        <td>Orjun Kumar Sarker <br /> Sub-Divisional Engineer (Electrical)</td>
                        <td></td>
                        <td></td>
                        <td>All kinds of Electrical Information.</td>
                    </tr>
                    <tr>
                        <td rowspan="2" class="align-middle font-weight-bold"></td>
                        <td>Ratan Chandra <br /> Sr. Assistant (Lift & AC)</td>
                        <td>131</td>
                        <td></td>
                        <td>All kinds of Electrical Information.</td>
                    </tr>
                    <tr>
                        <td>Md. Hafizur Rahman <br /> Caretaker</td>
                        <td>144</td>
                        <td></td>
                        <td>All kinds of Caretaking Information.</td>
                    </tr>
                    <tr>
                        <td rowspan="3" class="align-middle font-weight-bold">ACCOUNTS & FINANCE</td>
                        <td>M. M. Ariful Islam <br /> Asstt. Executive Officer (F&A)</td>
                        <td>141</td>
                        <td>arif77666@yahoo.com</td>
                        <td rowspan="3" class="align-middle">All kinds of Accounts & Financial Information.</td>
                    </tr>
                    <tr>
                        <td>Kazi Akter Faruque <br /> Asstt. Executive Officer (F&A)</td>
                        <td>150</td>
                        <td>kaziakterfaruque@gmail.com</td>
                    </tr>
                    <tr>
                        <td>Md. Sofiqul Islam <br /> Accountant</td>
                        <td>151</td>
                        <td>sofiqieb@gmail.com</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>



<!-- Start touch-map Section -->
<section class="touch-map">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12  text-center py-5" style="    background-image: linear-gradient(to right top, #d4e6f1, #cdedf0, #d1f2e6, #e2f5d8, #fcf3cf);">
                <h4 class="mb-5 font-weight-bold"> KEEP IN TOUCH </h4>



                <form class="form-single mx-auto" name="frmContact" id="frmContact">
                    <div class="form-row mb-3" id="msgContact" style="display: block;"></div>
                    <div class="form-row mb-3">

                        <div class="col-md-6">
                            <input  name="contactSubject" id="contactSubject" type="text" class="form-control bg-transparent border-dark" placeholder="Subject">
                        </div>
                        <div class="col-md-6">
                            <input name="contactName" id="contactName" type="text" class="form-control bg-transparent border-dark" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-row mb-3">
                        <div class="col-md-6">
                            <input name="contactEmail" id="contactEmail" type="email" class="form-control bg-transparent border-dark" placeholder="Email">
                        </div>
                        <div class="col-md-6">
                            <input name="contactPhone" id="contactPhone" type="text" class="form-control bg-transparent border-dark" placeholder="Phone">
                        </div>
                    </div>
                    <div class="row mb-5">
                        <div class="col-md-12">
                            <textarea name="contactMessage" id="contactMessage" class="form-control bg-transparent border-dark" rows="3" placeholder="Message"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button id="btnFrmContact" name="btnFrmContact" type="submit" class="btn btn-white btn-lg" style="width: 100%; background-color: #000022;color: white;">SEND MESSAGE</button>


                            <!--                                    <button type="submit" class="btn btn-white bg-white btn-lg" id="btnFrmContact" name="btnFrmContact">SEND MESSAGE</button>-->

                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</section>
<!-- End touch-map Section -->


<%@ include file="../footer.jsp" %>
