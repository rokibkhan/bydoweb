<%@page import="java.util.*" %>
<a href="../../../../IEB Doc/demo/IEB___Shohag/IEB___/Aims&objective.html"></a>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>




<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<div class="get_about_banner_Tah">
    <h2 class="display_41_saz">Aims & Objective</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb"> 
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#" class="text-white">About</a></li>
                <li class="breadcrumb-item active" aria-current="page">Aims & Objective</li>
            </ol>
        </nav>
    </div>
</div>




<!-- Starting of "What We Do" Section -->
	<div class="what-we-do">
		<div class="container">
			<div class="row">
				<div class="col-md-12 p-3 align-self-center mt-4 mb-4">
					<h2> THE AIMS AND OBJECTIVES OF THE INSTITUTION ARE: </h2><br>
                    <ul>
					<li class="h6" style="color:#666666">To build Better World.</li><br>
                    <li class="h6" style="color:#666666">To promote   and   advance  the   science,  practice   and business of engineering in all its branches throughout Bangladesh and abroad.    </li><br>
                    <li class="h6" style="color:#666666"> To promote efficiency in the engineering practices and profession.  </p><br>
                    <li class="h6" style="color:#666666"> To  regulate the  professional  activities  and  assist in maintaining high standards in the general conduct of its members.    </li><br>
                    <li class="h6" style="color:#666666"> To lay down professional Code of Ethics and to make it mandatory for its members to abide by the same in their professional conduct. </li><br>
                    <li class="h6" style="color:#666666"> To help in the acquisition and interchange of technical knowledge among its members.  </li><br>
                    <li class="h6" style="color:#666666"> To promote the professional interests and social welfare of its members.    </li><br>
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/Aim-obj.png" class="float-right do-img img-fluid rounded-circle p-5 mx-auto" alt="What we do.png" />
                    <li class="h6" style="color:#666666"> To   encourage   original   research   in   engineering   and conservation  & economic utilization of the countrys materials and resources.   </li><br>
                    <li class="h6" style="color:#666666"> To foster co-ordination with similar institutions in other countries and engineering universities, institutions and colleges in Bangladesh and in other countries, for mutual benefits in furthering the objects of The Institution.</li><br>
                    <li class="h6" style="color:#666666"> To diffuse among its members information on all matters affecting engineering and to encourage, assist and extend knowledge   and   information    connected  therewith   by establishment and promotion of lectures, discussions or correspondence   ;   by      holding   of  conferences,   by publication    of    papers,    periodicals    and    journals, proceedings, reports, books, circulars and maps or other literary undertaking ; by encouraging research works or by the formation of library or libraries and collection of models, designs, drawings and other articles of interest in connection with engineering or otherwise whatsoever.  </li><br>
                    <li class="h6" style="color:#666666"> To promote the study of engineering with a view to disseminat.</li><br>
				
				
					
				</div>
			</div>
		</div>
	</div>




<%@ include file="../footer.jsp" %>
