<%@ include file="../header.jsp" %>

<section>

    <nav aria-label="breadcrumb"> 
        <div class="container">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">MemberShip</a></li>
                <li class="breadcrumb-item active" aria-current="page">About MemberShip</li>
            </ol>
        </div>
    </nav>
</section>

<!-- Login & Register & About CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about.css" rel="stylesheet">

<!-- News front image section -->
<!--<section class="news-banner">
    <div class="conainer">
        <img src="<%//out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/success_story_img_1.jpg" class="img-fluid banner-img" alt="Front Image">
    </div>
</section>-->
<!-- News front image section -->

<!-- Start of details of news -->
<section class="news-single-content pt-0">
    <div class="container">
        <h2 class="display-41">About MemberShip</h2>
        <div class="row">
            <div class="col-12">
                <div class="news-content">
                    <div class="media mt-5">
                        
                        <div class="media-body">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <br /><p>Porttitor nullam integer porta vivamus proin venenatis consectetur, potenti ultrices elementum arcu est fringilla phasellus, placerat auctor litora consectetur praesent netus aptent ut ornare netus dictum vivamus et volutpat, nunc lorem enim urna 
                                pellentesque egestas aenean tincidunt, torquent felis orci nibh aliquam et praesent placerat 
                                eleifend sagittis ut magna consequat nibh turpis.</p><br />
                            <p>Vitae donec turpis platea class cras iaculis vitae, imperdiet aenean adipiscing facilisis aptent vivamus placerat morbi ultrices libero consectetur fermentum taciti nec taciti conubia. Sodales id netus venenatis imperdiet integer felis ipsum hac pellentesque ultrices eros varius, 
                                gravida consectetur praesent mattis sem praesent ad aptent accumsan nisi tristique id, curabitur at lorem ac torquent</p>
                        </div>
                    </div>
                    <hr />
                    <blockquote class="blockquote bg-light text-left p-5">
                        <p class="mb-0">Phasellus netus porta conubia habitasse libero risus mauris fames aenean faucibus ligula, himenaeos nec tortor lacus sociosqu </p>
                        <i class="fas fa-quote-right float-right"></i>
                        <footer class="blockquote-footer">S Valentin</footer>
                    </blockquote>
                    <hr />
                    <div class="media-body ml-5 pl-5">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/event_img_2.jpg" alt="Blog Picture 3" class="img-fluid ml-3 float-right" />
                        <p>Preparatory unicorn growled over coy bid pushed extraordinarily ouch much cockatoo heedless this that 
                            one guarded and the heinous this some far vigilantly some far grasshopper overcast besides sanctimonious and some due 
                            rudely well dove more owing far foretold some and python wherever cuckoo resolutely octopus rebuilt by spiteful lingeringly elephant guinea 
                            beauteous trod a bird nightingale one by emphatic inside giggly after off the aardvark ironic as rode frequent less so.</p>
                        <br/>

                        <p>Pretium class phasellus netus porta conubia habitasse libero risus mauris fames aenean faucibus ligula, himenaeos 
                            nec tortor lacus sociosqu suspendisse aliquam ullamcorper volutpat nisl primis imperdiet porttitor pretium class 
                            aenean nisi senectus leo varius in neque ultricies, ullamcorper magna molestie aptent risus lacus blandit dictum auctor 
                            mattis, hac pellentesque commodo pellentesque ligula nostra gravida 
                            senectus suspendisse scelerisque primis urna habitasse posuere </p>
                    </div>
                    <div class="media-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/event_img_2.jpg" alt="Blog Picture 3" class="img-fluid mr-3 my-2 float-left" />
                        <p>Lorem ipsum viverra molestie at hac tempus nulla, euismod metus libero nullam varius turpis justo mi, 
                            auctor tristique habitant proin pharetra litora vehicula tellus orci aenean
                            nisl sagittis est, duis litora aenean nisi volutpat, ligula massa fringilla felis integer morbi condimentum.</p>
                        <p class="pt-3">Rutrum vestibulum enim luctus integer suscipit tellus ut, donec potenti eu mollis vel ut dolor dictumst,
                            neque viverra scelerisque justo magna netus cras nibh ultricies enim aliquet fames curabitur. Porttitor nullam integer porta vivamus 
                            proin venenatis consectetur, potenti ultrices 
                            elementum arcu est fringilla phasellus, placerat auctor litora consectetur praesent netus aptent ut ornare netus dictum vivamus et </p>
                    </div>
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/event_img_2.jpg" alt="Blog Picture 3" class="img-fluid m-3 banner-img" />
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of details of news -->

<!-- Start of more news section -->
<section>
    <div class="container">
        <div class="post-heading pt-5">
            <h3 class="text-uppercase font-weight-bold">More News </h3>
            <hr class="custom-horizontal-border mr-5" />
        </div>
        <div class="row mb-5">
            <div class="col-md-3 col-sm-6">
                <div class="post-content pt-3">
                    <div class="post-content-img">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/event_img_1.jpg" alt="Post Image" class="img-fluid news-image  mb-3" />
                        <div class="post-content-img-overlay d-flex align-items-center justify-content-center"></div>
                        <a href="#" class="text-primary font-weight-bold news-link">Regulate the professional</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="post-content pt-3">
                    <div class="post-content-img">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_img_1.jpg" alt="Post Image" class="img-fluid news-image  mb-3" />
                        <div class="post-content-img-overlay d-flex align-items-center justify-content-center"></div>
                        <a href="#" class="text-primary font-weight-bold news-link">Regulate the professional</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="post-content pt-3">
                    <div class="post-content-img">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_img_3.jpg" alt="Post Image" class="img-fluid news-image mb-3" />
                        <div class="post-content-img-overlay d-flex align-items-center justify-content-center"></div><br />
                        <a href="#" class="text-primary font-weight-bold news-link">Regulate the professional</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="post-content pt-3">
                    <div class="post-content-img">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_img_2.jpg" alt="Post Image" class="img-fluid news-image  mb-3" />
                        <div class="post-content-img-overlay"></div>
                        <a href="#" class="text-primary font-weight-bold news-link">Regulate the professional</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of more news section -->


<%@ include file="../footer.jsp" %>
