<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>




<!-- banner-start -->

<section>
    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/about-us-banner.jpg">

</section><br><br><br>

<!-- banner-end -->

<section class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-sm-3">
                <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mission.png" width="80%">

            </div>
            <div class="col-md-6">
                <h4 class="text-info">OUR MISSION</h4>
                <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

            </div>

        </div>
        <div class="row">
            <div class="col-md-6 mb-sm-3">
                <h4 class="text-info">OUR VISION</h4>
                <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

            </div>
            <div class="col-md-6">
                <img class="img-fluid rounded" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/vision.png" width="80%">
            </div>

        </div>
    </div>
</section>

<section>
    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/reach-goal.jpg" >
</section>

<section class="container-fluid">
    <div class="container text-center py-5">
        <h2 class="font-weight-bold text-success">You?re joining a global classroom</h2>
        <p class="text-justify text-secondary">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley</p>
        <a href="contact-us.html">
            <button type="button" class="btn btn-outline-success">Learn more about admission</button>
        </a>

    </div>

</section>



<%@ include file="../footer.jsp" %>
