<%@page import="com.appul.servlet.getDate"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.appul.entity.ContactUs"%>
<%@page import="com.appul.util.getRegistryID"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Session"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>




<%
    Session dbsession = null;
    String qryparam = "";;

    dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";

    getRegistryID regId = new getRegistryID();
    String contactId = regId.getID(14);

    String strMsg = "";

    ContactUs contact = null;

    String contactName = request.getParameter("contactName") == null ? "" : request.getParameter("contactName").trim();
    String contactEmail = request.getParameter("contactEmail") == null ? "" : request.getParameter("contactEmail").trim();
    String contactPhone = request.getParameter("contactPhone") == null ? "" : request.getParameter("contactPhone").trim();
    String contactSubject = request.getParameter("contactSubject") == null ? "" : request.getParameter("contactSubject").trim();
    String contactMessage = request.getParameter("contactMessage") == null ? "" : request.getParameter("contactMessage").trim();

//    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    Date dateX = new Date();
//    String adddate = dateFormatX.format(dateX);
    getDate date = new getDate();
    String adduser = contactId;
    String addterm = InetAddress.getLocalHost().getHostName().toString();
    String addip = InetAddress.getLocalHost().getHostAddress().toString();

    if ((contactName != null && !contactName.isEmpty()) && (contactEmail != null && !contactEmail.isEmpty()) && (contactPhone != null && !contactPhone.isEmpty()) && (contactSubject != null && !contactSubject.isEmpty()) && (contactMessage != null && !contactMessage.isEmpty())) {

        contact = new ContactUs();

        contact.setContactUsId(Integer.parseInt(contactId));
        contact.setName(contactName);
        contact.setEmail(contactEmail);
        contact.setPhone(contactPhone);
        contact.setSubject(contactSubject);
        contact.setComments(contactMessage);
        contact.setAddDate(date.serverDate());
        contact.setAddIp(addip);
        contact.setAddTerm(addterm);
        contact.setModDate(date.serverDate());
        contact.setModTerm(addterm);
        contact.setModIp(addip);
        
        dbsession.save(contact);
        //    strMsg = "Information saved successfully.";

        dbsession.flush();
        dbtrx.commit();
        dbsession.close();

        if (dbtrx.wasCommitted()) {
//            strMsg = "Success!!! Member Training Information added ";
            // response.sendRedirect("pages/contact.jsp?strMsg=" + strMsg);

            responseCode = "1";
            responseMsg = "Success!!! Thank Your for contact with us";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                    + "</div>";

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
            jsonArr.add(json);

        } else {
            strMsg = "Error!!! When Publication Information added ";
            dbtrx.rollback();
            //   response.sendRedirect("pages/contact.jsp?strMsg=" + strMsg);

            responseCode = "0";
            responseMsg = "Error!!! When contact  with us";
            responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                    + "</div>";

            json.put("responseCode", responseCode);
            json.put("responseMsg", responseMsg);
            json.put("responseMsgHtml", responseMsgHtml);
            jsonArr.add(json);
        }

    } else {
        System.out.println("Contact Required :: ");
        responseCode = "0";
        responseMsg = "Error!!! When contact  with us";
        responseMsgHtml = "<div class=\"alert alert-success alert-dismissable\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        jsonArr.add(json);
    }
    
    
    out.println(jsonArr);
    

%>
