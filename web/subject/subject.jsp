<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    Session dbsessionCourse = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxRegistration = dbsessionCourse.beginTransaction();

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();

    String msgDispalyConT, msgInfoText, sLinkOpt;

    if (!strMsg.equals("")) {

        msgDispalyConT = "style=\"display: block; margin-top:5px;\"";

        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {

        msgDispalyConT = "style=\"display: none;\"";

        msgInfoText = "";

    }

%>



<!-- course section start -->

<section class="container-fluid py-5" style="background: url(<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/course-bg.png);background-size: 100% 100%;">

    <div class="container mb-3">

        <button type="button" class="btn btn-success btn-lg btn-block">Engineering / KA Unit Admission Test</button>

    </div>
    <div class="container bg-white py-5 phy">
        <div class="row">
            <div class="col-md-6 pb-sm-3 d-flex justify-content-sm-center sfx4">
                <img class="rounded-circle sfx5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/math.jpg" width="350" height="350">
            </div>
            <div class="col-md-6">
                <div class="container">

                    <div id="accordion">
                        <div class="card">
                            <div class="card-header course1">
                                <a class="card-link text-white" data-toggle="collapse" href="#collapseOne">
                                    <span>Mathematics 1</span>
                                    <i class="fa fa-chevron-down fa-lg float-right"></i>
                                </a>
                            </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                <div class="card-body">

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span>	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header course1">
                                <a class="collapsed card-link text-white" data-toggle="collapse" href="#collapseTwo">
                                    <span>Mathematics 2</span>
                                    <i class="fa fa-chevron-down fa-lg float-right"></i>
                                </a>
                            </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="card-body">

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                            <button class="btn btn-outline-secondary">
                                                <i class="fa fa-play-circle fa-2x align-middle"></i>
                                                <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                                <span>[Permutation and Combination]</span>	
                                            </button><br>
                                        </a>

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                            <button class="btn btn-outline-secondary mt-2">
                                                <i class="fa fa-play-circle fa-2x align-middle"></i>
                                                <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                                <span>[Permutation and Combination]</span> 	
                                            </button><br>
                                        </a>

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                            <button class="btn btn-outline-secondary mt-2">
                                                <i class="fa fa-play-circle fa-2x align-middle"></i>
                                                <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                                <span>[Permutation and Combination]</span> 	
                                            </button><br>
                                        </a>

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                            <button class="btn btn-outline-secondary mt-2">
                                                <i class="fa fa-play-circle fa-2x align-middle"></i>
                                                <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                                <span>[Permutation and Combination]</span> 	
                                            </button><br>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container bg-white py-5 phy mt-4">
    <div class="row">
        <div class="col-md-6 pb-sm-3 d-flex justify-content-sm-center sfx4">
            <img class="rounded-circle sfx5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/physics.png" width="350" height="350">
        </div>

        <div class="col-md-6">
            <div class="container">

                <div id="accordion">
                    <div class="card">
                        <div class="card-header course1">
                            <a class="card-link text-white" data-toggle="collapse" href="#collapseOne">
                                <span>Mathematics 1</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span>	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header course1">
                            <a class="collapsed card-link text-white" data-toggle="collapse" href="#collapseTwo">
                                <span>Mathematics 2</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <div class="card-body">

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span>	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="container bg-white py-5 phy mt-4">
    <div class="row">
        <div class="col-md-6 pb-sm-3 d-flex justify-content-sm-center sfx4">
            <img class="rounded-circle course2 sfx5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/chemistry.png" width="350" height="350">
        </div>

        <div class="col-md-6">
            <div class="container">

                <div id="accordion">
                    <div class="card">
                        <div class="card-header course1">
                            <a class="card-link text-white" data-toggle="collapse" href="#collapseOne">
                                <span>Mathematics 1</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span>	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header course1">
                            <a class="collapsed card-link text-white" data-toggle="collapse" href="#collapseTwo">
                                <span>Mathematics 2</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <div class="card-body">

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span>	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</section>





<%    dbsessionCourse.flush();

    dbsessionCourse.close();

%>

<%@ include file="../footer.jsp" %>
