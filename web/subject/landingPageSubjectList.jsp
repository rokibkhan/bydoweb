<%@page import="org.hibernate.Session"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.appul.util.GlobalVariable"%>

<%
    Session dbsessionSubject = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxSubject = dbsessionSubject.beginTransaction();
%>


<!-- /.course section start -->


<div class="container my-5">
    <div class="row">
        <div class="col-sm-6 col-md-3 text-center">
            <div class="container border border-info rounded my-3 bydo">
                <img class="mt-2" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/math.png" width="60">
                <p class="mt-2">Mathematics</p>
            </div>

        </div>
        <div class="col-sm-6 col-md-3 text-center">
            <div class="container border border-info rounded my-3 bydo" >
                <img class="mt-2" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/phy.png" width="60">
                <p class="mt-2">Physic</p>
            </div>

        </div>
        <div class="col-sm-6 col-md-3 text-center">
            <div class="container border border-info rounded my-3 bydo">
                <img class="mt-2" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/che.png" width="60">
                <p class="mt-2">Chemistry</p>
            </div>

        </div>
        <div class="col-sm-6 col-md-3 text-center">
            <div class="container my-3 bg-secondary bydo1">
                <img class="mt-4" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/more.png" width="40">
                <p class="mt-2 text-white text-center">View All</p>
            </div>

        </div>


    </div>
</div>

<!-- /.course section start -->


<%
    dbsessionSubject.clear();
    dbsessionSubject.close();
%> 


