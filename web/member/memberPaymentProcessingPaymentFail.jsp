<%@page import="com.ssl.commerz.parametermappings.SSLCommerzValidatorResponse"%>
<%@page import="com.ssl.commerz.TransactionResponseValidator"%>
<%@page import="com.ssl.commerz.SSLCommerz"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>
<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String strMsg = "";

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    //String regMemberTempId = request.getParameter("regTempId") == null ? "" : request.getParameter("regTempId").trim();
    
    String regMemCouId = request.getParameter("MemCouId") == null ? "" : request.getParameter("MemCouId").trim();
    String regMemId = request.getParameter("regMemId") == null ? "" : request.getParameter("regMemId").trim();
    String transId = request.getParameter("transId") == null ? "" : request.getParameter("transId").trim();

    String verify_sign = request.getParameter("verify_sign");

    System.out.println("verify_sign :: " + verify_sign);

    Enumeration paramNames = request.getParameterNames();
    Map<String, String> postData = new HashMap<String, String>();
    while (paramNames.hasMoreElements()) {
        String paramName = (String) paramNames.nextElement();
        //  out.print("<tr><td>" + paramName + "</td>\n");
        //   String paramValue = request.getHeader(paramName);
        String paramValue = request.getParameter(paramName);
        //  out.println("<td> " + paramValue + "</td></tr>\n");

        postData.put(paramName, paramValue);
    }

    Query feeSQL = null;
    Object[] feeObject = null;

    String memberShipFeeTransId = "";
    String memberShipFeeMemberId = "";
    String memberShipFeePaidDate = "";
    String memberShipFeeAmount = "";
    String memberShipFeeCurrency = "BDT";

    // feeSQL = dbsession.createSQLQuery("select * from member_fee_temp WHERE txn_id= " + transId + " AND status = 0");
    feeSQL = dbsession.createSQLQuery("select * from member_fee WHERE txn_id= " + transId + " AND status = 0");
    //  if (!feeSQL.list().isEmpty()) {
    for (Iterator feeItr = feeSQL.list().iterator(); feeItr.hasNext();) {
        feeObject = (Object[]) feeItr.next();
        memberShipFeeTransId = feeObject[0].toString();
        memberShipFeeMemberId = feeObject[1].toString();
        memberShipFeePaidDate = feeObject[5].toString();
        memberShipFeeAmount = feeObject[7].toString();

    }
    //   }

    //  out.println("memberShipFeeTransId :: " + memberShipFeeTransId);
    //   out.println("memberShipFeeMemberId :: " + memberShipFeeMemberId);
    //   out.println("memberShipFeePaidDate :: " + memberShipFeePaidDate);
    //   out.println("memberShipFeeAmount :: " + memberShipFeeAmount);
    //update  fee table Status and ref_no, ref_description
    String tranId = request.getParameter("tran_id");
    String valId = request.getParameter("val_id");
    String cardType = request.getParameter("card_type");
    String cardNo = request.getParameter("card_no");
    String bankTransactionID = request.getParameter("bank_tran_id");
    String cardIssuerBank = request.getParameter("card_issuer");
    String cardBrand = request.getParameter("card_brand");
    String cardBrandTranDate = request.getParameter("tran_date");
    String cardBrandTranCurrencyType = request.getParameter("currency_type");
    String cardBrandTranCurrencyAmount = request.getParameter("currency_amount");

    String ref_description = "tranId::" + tranId + " ::valId::" + valId + " ::cardType::" + cardType + " ::cardNo::" + cardNo + " ::bankTransactionID::" + bankTransactionID + " ::cardIssuerBank::" + cardIssuerBank + " ::cardBrand::" + cardBrand + " ::cardBrandTranDate::" + cardBrandTranDate + " ::cardBrandTranCurrencyAmount::" + cardBrandTranCurrencyAmount + " ::cardBrandTranCurrencyType::" + cardBrandTranCurrencyType;
    String ref_no = bankTransactionID;

    Query feeUpdSQL = dbsession.createSQLQuery("UPDATE  member_fee_temp SET ref_no='" + ref_no + "',ref_description ='" + ref_description + "',mod_user ='" + regMemId + "',mod_date=now() WHERE txn_id='" + transId + "'");

    feeUpdSQL.executeUpdate();
    
    
    //Query courseUpdSQL = dbsession.createSQLQuery("UPDATE  member_course SET mod_user ='" + regMemId + "',mod_date=now(),status='1' WHERE id='" + regMemCouId + "'");

    //courseUpdSQL.executeUpdate();
            
    
    dbtrx.commit();

    dbsession.flush();
    dbsession.close();
    //error

    //error
    strMsg = "Error!!! Payment transaction fail.Please check and try again.";
    //  dbtrx.rollback();
    // response.sendRedirect(GlobalVariable.baseUrl + "/member/registrationFeePaymentOption.jsp?regTempId=" + regMemberTempId + "&regMemId=" + regMemId + "&regStep=11&strMsg=" + strMsgPay + "");
    response.sendRedirect("memberPaymentProcessing.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);


%>
