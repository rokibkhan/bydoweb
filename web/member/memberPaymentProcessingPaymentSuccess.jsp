<%@page import="com.ssl.commerz.parametermappings.SSLCommerzValidatorResponse"%>
<%@page import="com.ssl.commerz.TransactionResponseValidator"%>
<%@page import="com.ssl.commerz.SSLCommerz"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>
<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String strMsg = "";

    MemberFee mFee = null;

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    MemberFee memberFee = null;

    //String regMemberTempId = request.getParameter("regTempId") == null ? "" : request.getParameter("regTempId").trim();
    String regMemCouId = request.getParameter("MemCouId") == null ? "" : request.getParameter("MemCouId").trim();
    String regMemId = request.getParameter("regMemId") == null ? "" : request.getParameter("regMemId").trim();
    String transId = request.getParameter("transId") == null ? "" : request.getParameter("transId").trim();

    String verify_sign = request.getParameter("verify_sign");

    System.out.println("verify_sign :: " + verify_sign);

    Enumeration paramNames = request.getParameterNames();
    Map<String, String> postData = new HashMap<String, String>();
    while (paramNames.hasMoreElements()) {
        String paramName = (String) paramNames.nextElement();
        //  out.print("<tr><td>" + paramName + "</td>\n");
        //   String paramValue = request.getHeader(paramName);
        String paramValue = request.getParameter(paramName);
        //  out.println("<td> " + paramValue + "</td></tr>\n");

        postData.put(paramName, paramValue);
    }

    Query feeSQL = null;
    Object[] feeObject = null;

    String memberShipFeeTransId = "";
    String memberShipFeeMemberId = "";
    String memberShipFeePaidDate = "";
    String memberShipFeeAmount = "";
    String memberShipFeeCurrency = "BDT";

    // feeSQL = dbsession.createSQLQuery("select * from member_fee_temp WHERE txn_id= " + transId + " AND status = 0");
    feeSQL = dbsession.createSQLQuery("select * from member_fee_temp WHERE txn_id= " + transId + " AND status = 0");
    //  if (!feeSQL.list().isEmpty()) {
    for (Iterator feeItr = feeSQL.list().iterator(); feeItr.hasNext();) {
        feeObject = (Object[]) feeItr.next();
        memberShipFeeTransId = feeObject[0].toString();
        memberShipFeeMemberId = feeObject[1].toString();
        memberShipFeePaidDate = feeObject[5].toString();
        memberShipFeeAmount = feeObject[7].toString();

    }
    //   }

  //  out.println("memberShipFeeTransId :: " + memberShipFeeTransId);
 //   out.println("memberShipFeeMemberId :: " + memberShipFeeMemberId);
 //   out.println("memberShipFeePaidDate :: " + memberShipFeePaidDate);
 //   out.println("memberShipFeeAmount :: " + memberShipFeeAmount);

    // com.ssl.commerz.TransactionResponseValidator.receiveSuccessResponse
//check transactionId from SSL Commerce
    /**
     * If following order validation returns true, then process transaction as
     * success. if this following validation returns false , then query status
     * if failed of canceled. Check request.get("status") for this purpose
     */
    //  TransactionResponseValidator transactionValidationResponse = new TransactionResponseValidator();
    //   boolean transactionValidationResponseBol = transactionValidationResponse.receiveSuccessResponse(postData);
    //   System.out.println("transactionValidationResponseVVV::" + transactionValidationResponseBol);
    SSLCommerz sslcz = new SSLCommerz(GlobalVariable.SSLCommerzStoreId, GlobalVariable.SSLCommerzStorePass, Boolean.parseBoolean(GlobalVariable.SSLCommerzStoreMode));

    boolean transactionValidationResponseBol = sslcz.orderValidate(memberShipFeeTransId, memberShipFeeAmount, memberShipFeeCurrency, postData);

    System.out.println("transactionValidationResponseBol:"+transactionValidationResponseBol);
    //  boolean transactionValidationResponseBol = false;
    if (transactionValidationResponseBol == true) {

        //update  fee table Status and ref_no, ref_description
        String tranId = request.getParameter("tran_id");
        String valId = request.getParameter("val_id");
        String cardType = request.getParameter("card_type");
        String cardNo = request.getParameter("card_no");
        String bankTransactionID = request.getParameter("bank_tran_id");
        String cardIssuerBank = request.getParameter("card_issuer");
        String cardBrand = request.getParameter("card_brand");
        String cardBrandTranDate = request.getParameter("tran_date");
        String cardBrandTranCurrencyType = request.getParameter("currency_type");
        String cardBrandTranCurrencyAmount = request.getParameter("currency_amount");

        String ref_description = "tranId::" + tranId + " ::valId::" + valId + " ::cardType::" + cardType + " ::cardNo::" + cardNo + " ::bankTransactionID::" + bankTransactionID + " ::cardIssuerBank::" + cardIssuerBank + " ::cardBrand::" + cardBrand + " ::cardBrandTranDate::" + cardBrandTranDate + " ::cardBrandTranCurrencyAmount::" + cardBrandTranCurrencyAmount + " ::cardBrandTranCurrencyType::" + cardBrandTranCurrencyType;
        String ref_no = bankTransactionID;
        String status = "1";
        String pay_option = "1";

        Query feeUpdSQL = dbsession.createSQLQuery("UPDATE  member_fee_temp SET ref_no='" + ref_no + "',ref_description ='" + ref_description +  "',mod_user ='" + regMemId + "',mod_date=now(),status='1' WHERE txn_id='" + transId + "'");

        feeUpdSQL.executeUpdate();
        
        
        
        
        //getRegistryID getId = new getRegistryID();
        //String idS = getId.getID(72);
        //int TxnId = Integer.parseInt(idS);
        
        
        
        Query SQL = dbsession.createSQLQuery("select txn_id,fee_year from member_fee_temp WHERE txn_id='" + transId  + "'");

        Object[] obj = null;
        
        int txn_id = 0;
        int paid_year = 0;
        //List<Object[]> collection1 = SQL.getResultList(); //collection of object arrays
        if (!SQL.list().isEmpty()) {

            //ResultSet rs = dbsession.executeQuery(SQL);

            for (Iterator it1 = SQL.list().iterator(); it1.hasNext();) {


                                                //obj = (SubjectInfo) it1.next();
                                                obj = (Object[]) it1.next();

                                                //IdSubject = obj[0].toString().trim();
                                                txn_id =  Integer.parseInt(obj[0].toString());
                                                paid_year = Integer.parseInt(obj[1].toString());

                                                
            }

     //List memberobj = SQL.list();
     //String id = memberobj.get(0).toString();
     //System.out.println("membername:"+id);
     
    }
        
        
        
        Query q1 = dbsession.createSQLQuery("INSERT INTO member_fee("
            + "txn_id,"
            + "member_id,"
            + "ref_no,"
            + "ref_description,"
            + "txn_date,"
            + "paid_date,"
            + "fee_year,"
            + "amount,"   
            + "status,"   
            + "pay_option,"    
            + "add_user,"
            + "add_date"            
            + ") VALUES("
            + "'" + transId + "',"
            + "'" + regMemId + "',"
            + "'" + ref_no + "',"
            + "'" + ref_description + "',"
            + "'" + cardBrandTranDate + "',"
            + "'" + cardBrandTranDate + "',"
            + "'" + paid_year + "',"
            + "'" + cardBrandTranCurrencyAmount + "',"
            + "'" + 1 + "',"
            + "'" + pay_option+ "',"        
            + "'" + regMemId  + "',"
            + "now()"
            
            + "  ) ");
    q1.executeUpdate();
        
        
        
        
        
        
        
        System.out.println(regMemCouId);
        
        //regMemCouId = session.getAttribute("memberCourseId").toString();
        
        //System.out.println("Session:"+regMemCouId);
        
        Query courseUpdSQL = dbsession.createSQLQuery("UPDATE  member_course SET mod_user ='" + regMemId + "',mod_date=now(),status='1' WHERE id='" + regMemCouId + "'");

        courseUpdSQL.executeUpdate();
        
        getRegistryID getId = new getRegistryID();
        String idCDS = getId.getID(70);
        int membercourseid = Integer.parseInt(idCDS);

        //session.setAttribute("memberId", idCDS);

        // session.setAttribute("memberCourseId", membercourseid);

        int courseid = Integer.parseInt(session.getAttribute("courseID").toString());

        Query q2 = dbsession.createSQLQuery("INSERT INTO member_course("
                + "id,"
                + "member_id,"
                + "course_id,"
                + "status,"
                + "add_user,"
                + "add_date"   
                + ") VALUES("
                + "" + membercourseid + ","
                + "'" + regMemId + "',"
                + "'" + courseid + "',"
                + "'" + 1 + "'," 
                + "'" + regMemId  + "',"
                + "now()"
            
                + "  ) ");
        q2.executeUpdate();
        
        dbtrx.commit();

        dbsession.flush();
        dbsession.close();
        //error
        strMsg = "Success!!! Your tanasaction is complete.";
        //  dbtrx.rollback();
        // response.sendRedirect(GlobalVariable.baseUrl + "/member/registrationFeePaymentOption.jsp?regTempId=" + regMemberTempId + "&regMemId=" + regMemId + "&regStep=11&strMsg=" + strMsgPay + "");
        //response.sendRedirect("memberPaymentProcessing.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        
        response.sendRedirect(GlobalVariable.baseUrl +"/course/coursedetails.jsp?courseid=" + courseid + "&strMsg=" + strMsg);

    } else {

        //error
        strMsg = "Error!!! Your tanasaction is not valid.Please check and try again.";
        //  dbtrx.rollback();
        // response.sendRedirect(GlobalVariable.baseUrl + "/member/registrationFeePaymentOption.jsp?regTempId=" + regMemberTempId + "&regMemId=" + regMemId + "&regStep=11&strMsg=" + strMsgPay + "");
        response.sendRedirect("memberPaymentProcessing.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
    }

%>
