
<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>


<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    String membername = "";
    String memberid = "";
    //membername = memberobj.get(2).toString();
    //System.out.print("membername:"+membername);
    String fathername = "";
    String mothername = "";
    String placeofbirth = "";
    String dob = "";
    String gender = "";
    String bloodgroup = "";
    String email = "";
    String phone = "";
    String Picture = "";

    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {
//        Session dbsession = HibernateUtil.getSessionFactory().openSession();
//        org.hibernate.Transaction dbtrx = null;
//        dbtrx = dbsession.beginTransaction();
//        session = request.getSession(true);
//        String mobile = "+" + request.getParameter("username").trim();
//        int memberId = (int) dbsession.createSQLQuery("select id FROM member WHERE mobile='" + mobile + "'").uniqueResult();
//        System.out.println(memberId);
//        session.setAttribute("username", mobile);
//        session.setAttribute("logstat", "Y");
//        session.setAttribute("memberId", memberId);
//
//        //System.out.println("ok");
//        sessionIdH = session.getId();
//        userNameH = mobile;
//        memberIdH = Integer.toString(memberId);
//        System.out.println(memberIdH);

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

    } else {
        System.out.println("IS IT HERE?");

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    //System.out.println("userNameH:"+userNameH);
    Query SQL = dbsession.createSQLQuery("select id,member_name,father_name,mother_name,dob,gender,blood_group,mobile,email_id,picture_name from member WHERE mobile='" + userNameH + "'");

    Object[] obj = null;
    //List<Object[]> collection1 = SQL.getResultList(); //collection of object arrays
    if (!SQL.list().isEmpty()) {

        //ResultSet rs = dbsession.executeQuery(SQL);
        for (Iterator it1 = SQL.list().iterator(); it1.hasNext();) {

            //obj = (SubjectInfo) it1.next();
            obj = (Object[]) it1.next();

            //IdSubject = obj[0].toString().trim();
            memberid = obj[0].toString();
            membername = obj[1].toString().trim();
            //membername = memberobj.get(2).toString();
            //System.out.print("membername:"+membername);
            //fathername = obj[3].toString() == null ? "" : obj[3].toString();
            fathername = obj[2] == null ? "" : obj[2].toString();
            mothername = obj[3] == null ? "" : obj[3].toString();
            //placeofbirth = obj[5].toString() == null ? "" : obj[5].toString();
            dob = obj[4] == null ? "" : obj[4].toString();
            gender = obj[5] == null ? "" : obj[5].toString();
            bloodgroup = obj[6] == null ? "" : obj[6].toString();
            phone = obj[7] == null ? "" : obj[7].toString();
            email = obj[8] == null ? "" : obj[8].toString();
            Picture = obj[9] == null ? "" : obj[9].toString();
        }

        //List memberobj = SQL.list();
        //String id = memberobj.get(0).toString();
        //System.out.println("membername:"+id);
    }


%>

<div class="container">
    <div class="row">
        <div class="col-md-2">
            <div class="mas-side-items">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberDashboard.jsp" class="btn btn-sm btn-block active text-left"><i class="fa fa-user-o"></i> OverView</a>
                <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberEditProfile.jsp" class="btn btn-sm btn-block text-left"><i class="fa fa-address-book-o"></i> Edit Profile</a>
                <a href="<%out.print(GlobalVariable.baseUrl);%>/member/myCourses.jsp" class="btn btn-sm btn-block text-left"><i class="fa fa-user-o"></i> My Courses</a>
                <a href="#" class="btn btn-sm btn-block text-left"><i class="fa fa-user-o"></i> My Quiz</a>
                <a href="<%out.print(GlobalVariable.baseUrl);%>/member/logout.jsp?sessionid=<% out.print(sessionIdH);%>" class="btn btn-sm btn-block text-left"><i class="fa fa-sign-out"></i> Logout</a>
            </div>
        </div>
        <div class="col-md-10">
            <div class="mas-banner-img">
                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/37586.jpg" alt="img"/>
            </div>
            <div class="container ta-pro">
                <div class="row">
                    <div class="col-md-3">
                        <img class="rounded teacher" src="<%out.print(GlobalVariable.baseUrl);%>/upload/member/<%out.print(Picture);%>" width="70%" style="margin-top: -100px;">
                    </div>
                    <div class="col-md-9">
                        <span class="font-weight-bold" style="font-size: 25px;"><%out.print(membername);%></span><br>
                        <span>Dhaka</span>
                    </div>
                </div>  
            </div>

            <div class="mas-main-sec">
                <div class="mas-main-sec-item">
                    <div class="mas-main-sec-item-header">
                        <span>Welcome <b><%out.print(membername);%></b> you have joined 2 weeks ago on <b>Bydo Academy</b></span>
                    </div>
                    <div class="mas-main-sec-item-content my-4">
                        <div class="row">

                            <div class="col-md-5  mas-con-items"><span><b>Email:</b> </span> <%out.print(email);%></div>

                            <div class="col-md-5 ml-md-3 mas-con-items"><span><b>Phone:</b> </span> <%out.print(phone);%></div>

                            <div class="col-md-5  mas-con-items"><span><b>College:</b> </span> Test College</div>
                            <div class="col-md-5 ml-md-3 mas-con-items"><span><b>Blood Group</b></span><%out.print(bloodgroup);%></div>

                            <div class="col-md-5  mas-con-items"><span><b>Date of Birth:</b> </span> <%out.print(dob);%></div>

                            <div class="col-md-5 ml-md-3 mas-con-items"><span><b>Gender:</b></span> <%out.print(gender);%></div>

                            <div class="col-md-5  mas-con-items"><span><b>Father Name:</b> </span> <%out.print(fathername);%></div>

                            <div class="col-md-5 ml-md-3 mas-con-items"><span><b>Mother Name:</b></span> <%out.print(mothername);%></div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="mas-main-sec">
                <div class="mas-main-sec-item">
                    <div class="mas-main-sec-item-header">
                        <span>Saved Videos</span>
                    </div>
                    <div class="mas-main-sec-item-content my-4">
                        <div class="row my-5">
                            <div class="col-md-3 col-sm-6 col-12 s-video"><a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/you.png" alt=""></a></div>
                            <div class="col-md-3 col-sm-6 col-12 s-video save-video"><a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/you.png" alt=""></a></div>
                            <div class="col-md-3 col-sm-6 col-12 s-video save-video"><a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/you.png" alt=""></a></div>
                            <div class="col-md-3 col-sm-6 col-12 s-video save-video"><a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/you.png" alt=""></a></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<%    dbsession.flush();
    dbsession.close();


%>
<%@ include file="../footer.jsp" %>
