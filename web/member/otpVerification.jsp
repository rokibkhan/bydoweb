<%-- 
    Document   : otpVerification
    Created on : Oct 24, 2020, 11:49:43 PM
    Author     : Zeeon
--%>

<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>

<div class="py-5" style="background: url(<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/bg.png);background-size: 100% 100%;">
    <div class="container bg-white phy">
        <div class="row">
            <div class="col-md-6 my-5 bg-white rounded-right">
                <div>
                    <img class="rounded mt-5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/education.png" alt="images" width="100%" />
                </div>
            </div>

            <div class="col-md-6 my-5">
                <!-- Default form login -->
                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/OtpVerification">

                    <p class="h4 mb-4 text-center text-danger">Verify Your Account</p>

                    <div id="error" style="display: block; font-size:  small;color: red;  " > 
                        <%
                            String rtnUser = "";

                            try {
                                rtnUser = session.getAttribute("strMsg").toString();
                                if (rtnUser != null) {
                                    out.println(rtnUser);
                                    session.removeAttribute("strMsg");
                                } else {
                                    out.print(" ");
                                }
                            } catch (Exception ex) {
                                // out.print(ex.getMessage());
                            }
                        %>                       
                    </div>


                    <!-- OTP -->

                    <label for="formInput">Enter Your OTP</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-user fa-lg" style="color:#fc7171"></i>
                            </span>
                        </div>
                        <input type="hidden" id="username" name="username" value="<%=request.getParameter("username")%>">
                        <input id="otp"name="otp" type="text" class="form-control" required="" placeholder="Enter OTP" aria-label="Username" aria-describedby="basic-addon1">
                    </div>

                    <!-- Submit Button -->
                    <div class="container text-center">
                        <button id="btn" class="btn btn-danger btn-lg my-4" type="submit">Submit</button>
                    </div>

                </form>
                <!-- Default form login -->
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
