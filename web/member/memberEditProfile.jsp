<%-- 
    Document   : memberEditProfile
    Created on : Apr 22, 2020, 11:11:00 AM
    Author     : Rokib
--%>


<%@page import="java.util.Date"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>


<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    String membername = "";
    String memberid = "";
     //membername = memberobj.get(2).toString();
     //System.out.print("membername:"+membername);
     String fathername = "";
     String mothername = "";
     String placeofbirth = "";
     String dob = "";
     String gender = "";
     String bloodgroup = "";
     String email = "";
     String Picture = "";
     String phone = "";

    //System.out.println("userNameH:"+session.getAttribute("username"));
    
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        //System.out.println("ok");
        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

    } else {

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    
    //System.out.println("userNameH:"+userNameH);
   
    //Query SQL = dbsession.createSQLQuery("select * from member WHERE member_id='" + userNameH  + "'");
    Query SQL = dbsession.createSQLQuery("select id,member_name,father_name,mother_name,dob,gender,blood_group,mobile,email_id,picture_name from member WHERE member_id='" + userNameH  + "'");
    
    Object[] obj = null;
    //List<Object[]> collection1 = SQL.getResultList(); //collection of object arrays
    if (!SQL.list().isEmpty()) {
        
        for (Iterator it1 = SQL.list().iterator(); it1.hasNext();) {
                                            

                                            //obj = (SubjectInfo) it1.next();
                                            obj = (Object[]) it1.next();
                                            /*
                                            //IdSubject = obj[0].toString().trim();
                                            memberid = obj[1].toString();
                                            membername = obj[2].toString().trim();
                                            
                                            //membername = memberobj.get(2).toString();
                                            //System.out.print("membername:"+membername);
                                            
                                            fathername = obj[3].toString() == null ? "" : obj[3].toString();
                                            mothername = obj[4].toString() == null ? "" : obj[4].toString();
                                            //placeofbirth = obj[5].toString() == null ? "" : obj[5].toString();
                                            dob = obj[6].toString() == null ? "" : obj[6].toString();
                                            gender = obj[7].toString() == null ? "" : obj[7].toString();
                                            bloodgroup = obj[8].toString() == null ? "" : obj[8].toString();
                                            phone = obj[11].toString();
                                            email = obj[12].toString();
                                            Picture = obj[17].toString();*/
                                                memberid = obj[0].toString();
                                            membername = obj[1].toString().trim();                                            
                                            //membername = memberobj.get(2).toString();
                                            //System.out.print("membername:"+membername);
                                            //fathername = obj[3].toString() == null ? "" : obj[3].toString();
                                            fathername = obj[2].toString() == null ? "" : obj[2].toString();
                                            mothername = obj[3].toString() == null ? "" : obj[3].toString();
                                            //placeofbirth = obj[5].toString() == null ? "" : obj[5].toString();
                                            dob = obj[4].toString() == null ? "" : obj[4].toString();
                                            gender = obj[5].toString() == null ? "" : obj[5].toString();
                                            bloodgroup = obj[6].toString() == null ? "" : obj[6].toString();
                                            phone = obj[7].toString();
                                            email = obj[8].toString();
                                            Picture = obj[9].toString();
     
        }
        
     //List memberobj = SQL.list();
     //String id = memberobj.get(0).toString();
     //System.out.println("membername:"+id);
     
     /*
     //memberid = memberobj.get(1).toString();
     //membername = memberobj.get(2).toString();
     //System.out.print("membername:"+membername);
     fathername = memberobj.get(3).toString();
     mothername = memberobj.get(4).toString();
     placeofbirth = memberobj.get(5).toString();
     dob = memberobj.get(6).toString();
     gender = memberobj.get(7).toString();
     bloodgroup = memberobj.get(8).toString();
     */
    }
    

%>

<%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();

    String msgDispalyConT, msgInfoText, sLinkOpt;

    if (!strMsg.equals("")) {

        msgDispalyConT = "style=\"display: block; margin-top:5px;\"";

        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {

        msgDispalyConT = "style=\"display: none;\"";

        msgInfoText = "";

    }

%>



<style type="text/css">
    
 .profile-img-container {
    position: absolute;
    width:50%;
}

 #imid:hover {
     
         content:   url('<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mousehover_1.png');
         margin-top: 80%;
         height:50%;    
}



.profile_img
{
 position: relative;   
}

.profile_img div{
 position: absolute;
 
 bottom: 0;
 right: 0;
 background: black;
 color: white;
 margin-bottom: 5px;
 font-family: sans-serif;
 opacity: 0;
 visibility: hidden;
 -webkit-transition: visibility 0s, opacity 0.5s linear; 
 transition: visibility 0s, opacity 0.5s linear;
}

.profile_img:hover{
 cursor: pointer;
}

.profile_img:hover div{
    
 width: 141px;
 padding: 8px 15px;
 visibility: visible;
 opacity: 0.7; 
}


/* Parent Container */
.content_img{
 position: relative;
 width: 200px;
 height: 200px;
 float: left;
 margin-right: 10px;
}

/* Child Text Container */
.content_img div{
 position: absolute;
 bottom: 0;
 right: 0;
 background: black;
 color: white;
 margin-bottom: 5px;
 font-family: sans-serif;
 opacity: 0;
 visibility: hidden;
 -webkit-transition: visibility 0s, opacity 0.5s linear; 
 transition: visibility 0s, opacity 0.5s linear;
}

/* Hover on Parent Container */
.content_img:hover{
 cursor: pointer;
}

.content_img:hover div{
 width: 150px;
 padding: 8px 15px;
 visibility: visible;
 opacity: 0.7; 
}
</style>

<div class="container">
    <%--
    <div class="content_img">
 <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/hh.jpg" width='100%' height='100%'>
 <div>Image 3 Text</div> 
</div>--%>
    <div class="row">
        <div class="col-md-2">
            <div class="mas-side-items">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberDashboard.jsp" class="btn btn-sm btn-block text-left"><i class="fa fa-user-o"></i> OverView</a>
                <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberEditProfile.jsp" class="btn btn-sm btn-block active text-left"><i class="fa fa-address-book-o"></i> Edit Profile</a>
                <a href="<%out.print(GlobalVariable.baseUrl);%>/member/myCourses.jsp" class="btn btn-sm btn-block text-left"><i class="fa fa-user-o"></i> My Courses</a>
                <a href="#" class="btn btn-sm btn-block text-left"><i class="fa fa-user-o"></i> My Quiz</a>
                <a href="<%out.print(GlobalVariable.baseUrl);%>/member/logout.jsp?sessionid=<% out.print(sessionIdH);%>" class="btn btn-sm btn-block text-left"><i class="fa fa-sign-out"></i> Logout</a>
            </div>
        </div>
        <div class="col-md-10">
            <div class="mas-banner-img">
                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/37586.jpg" alt="img"/>
            </div>
            <div class="container ta-pro">
                <div class="row">
                    <div class="col-md-3">
                        <div class="profile_img">
                        <img class="rounded teacher" src="<%out.print(GlobalVariable.baseUrl);%>/upload/member/<%out.print(Picture);%>"  style="margin-top: -100px;">
                        <div data-toggle="modal" data-target="#myModal" style="margin-right: 59px;margin-bottom: -1px;">Change Picture</div>      
                        </div>                   

                    </div>
                    <div class="col-md-9">
                        <span class="font-weight-bold" style="font-size: 25px;"><%out.print(membername);%></span><br>
                        
                    </div>
                </div>  
            </div>

            <div class="mas-main-sec">
                <div class="mas-main-sec-item">
                    <div class="mas-main-sec-item-header">
                        <span>Welcome <b><%out.print(membername);%></b> you have joined 2 weeks ago on <b>Bydo Academy</b></span>
                    </div>
                    
                    <div class="row globalAlertInfoBoxConParentTT">
                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->
                </div>

                    
                    <div class="mas-main-sec-item-content my-4">
                        
                         <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/member/memberEditProfileSubmitData.jsp" >
                        
                        <div class="row">

                            <div class="col-md-5  mas-con-items"><span><b>Email:</b> </span> <input type="text" name="emailid" id="emailid"  value="<%out.print(email);%>"/></div>

                            <div class="col-md-5 ml-md-3 mas-con-items"><span><b>Phone:</b> </span> <input type="text" name="phoneid" id="phoneid" value="<%out.print(phone);%>"/> </div>

                            <div class="col-md-5  mas-con-items"><span><b>College:</b> </span> <input type="text" name="collegeid" id="collegeid" value="Test College"/></div>
                            <div class="col-md-5 ml-md-3 mas-con-items"><span><b>Blood Group</b></span> <input type="text" name="bloodid" id="bloodid" value="<%out.print(bloodgroup);%>"/></div>
                            
                            <div class="col-md-5  mas-con-items"><span><b>Date of Birth:</b> </span> <input type="text" name="dobid" id="dobid" value="<%out.print(dob);%>"/></div>

                            <div class="col-md-5 ml-md-3 mas-con-items"><span><b>Gender:</b></span> <input type="text" name="genderid" id="genderid" value="<%out.print(gender);%>"/></div>

                            <div class="col-md-5  mas-con-items"><span><b>Father Name:</b> </span> <input type="text" name="fatherid" id="fatherid" value="<%out.print(fathername);%>"/></div>

                            <div class="col-md-5 ml-md-3 mas-con-items"><span><b>Mother Name:</b></span> <input type="text" name="motherid" id="motherid" value=" <%out.print(mothername);%>"/></div>

                        </div>
                            <button style="margin-left: 30%;" class="btn btn-danger btn-lg my-4" type="submit">Update</button>
                            
                         </form>
                    </div>
                </div>
            </div>
            <div class="mas-main-sec">
                <div class="mas-main-sec-item">
                    <div class="mas-main-sec-item-header">
                        <span>Saved Videos</span>
                    </div>
                    <div class="mas-main-sec-item-content my-4">
                        <div class="row my-5">
                            <div class="col-md-3 col-sm-6 col-12 s-video"><a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/you.png" alt=""></a></div>
                            <div class="col-md-3 col-sm-6 col-12 s-video save-video"><a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/you.png" alt=""></a></div>
                            <div class="col-md-3 col-sm-6 col-12 s-video save-video"><a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/you.png" alt=""></a></div>
                            <div class="col-md-3 col-sm-6 col-12 s-video save-video"><a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/you.png" alt=""></a></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
                        
     <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-header">
         <!-- <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>-->
         <p><b>Upload Photo</b></p>
        </div>
        <div class="modal-body">
          
          <div class="form-group row">
              <form data-toggle="validator" class="form-horizontal" name="techAdd" id="techAdd" method="POST" action="<%out.print(GlobalVariable.baseUrl);%>/MemberPictureUpload" onSubmit="return fromDataSubmitValidation1()" enctype="multipart/form-data">  
                                    
                                    <div class="col-md-9">
                                        <label for="eventShortDesc" class="control-label col-md-3">Photo</label>
                                        <div class="input-file-container">  
                                            <input name="file" id="file" accept="image/*" type="file" class="input-file" onchange="uplaodFileTypeJPGPNGAndSizeCheck('file', '5', 'fileErr', 'photoAddSubmitBtn');" >
                                            
                                        </div>
                                        <div id="fileErr" class="help-block with-errors" style="color: red;"></div> 
                                    </div>
                                    <br><br>
                                    <button type="submit" style="margin-left: 150px;" class="btn btn-info" id="photoAddSubmitBtn">Save</button>
                                    
                                    </form>
                                </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<%    dbsession.flush();
    dbsession.close();


%>
<%@ include file="../footer.jsp" %>
