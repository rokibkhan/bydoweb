<%-- 
    Document   : myCourses
    Created on : Apr 22, 2020, 11:21:53 AM
    Author     : Rokib
--%>

<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>

<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>


<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        
        
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();
        System.out.println("memberIdH:"+memberIdH);
        

    } else {

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    

%>

<div class="container">
    <div class="row">
        <div class="col-md-2">
            <div class="mas-side-items">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberDashboard.jsp" class="btn btn-sm btn-block text-left"><i class="fa fa-user-o"></i> OverView</a>
                <a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberEditProfile.jsp" class="btn btn-sm btn-block text-left"><i class="fa fa-address-book-o"></i> Edit Profile</a>
                <a href="<%out.print(GlobalVariable.baseUrl);%>/member/myCourses.jsp" class="btn btn-sm btn-block active text-left"><i class="fa fa-user-o"></i> My Courses</a>
                <a href="#" class="btn btn-sm btn-block text-left"><i class="fa fa-user-o"></i> My Quiz</a>
                <a href="<%out.print(GlobalVariable.baseUrl);%>/member/logout.jsp?sessionid=<% out.print(sessionIdH);%>" class="btn btn-sm btn-block text-left"><i class="fa fa-sign-out"></i> Logout</a>
            </div>
        </div>
        <div class="col-md-10">
            <!--<div class="mas-banner-img">
                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/37586.jpg" alt="img"/>
            </div>-->
            <div class="container ta-pro">
                <div class="row">
                    <!--<div class="col-md-3">
                        <img class="rounded teacher" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/hh.jpg" width="70%" style="margin-top: -100px;">
                    </div>
                    <div class="col-md-9">
                        <span class="font-weight-bold" style="font-size: 25px;">Engineering / KA Unit Admission Test</span><br>
                        
                    </div>-->
                </div>  
            </div>

            <div class="mas-main-sec">
                <div class="mas-main-sec-item">
                    <div class="mas-main-sec-item-header">
                        <span>My Course List</span>
                    </div>
                    <div class="mas-main-sec-item-content my-4">
                        
                        <%
                                    Object[] obj = null;
                                   
                                    String CourseName = "";
                                    String CourseShortName = "";
                                    String IdCourse = "";
                                    
                                    Query usrSQL = dbsession.createSQLQuery("SELECT id,course_id "
                                            + " FROM  member_course where member_id= '"+memberIdH+"'");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {
                                            
                                            System.out.println("ok");
                                            //obj = (SubjectInfo) it1.next();
                                            obj = (Object[]) it1.next();
                                            
                                            IdCourse = obj[1].toString().trim();
                                            
                                           Query q = dbsession.createSQLQuery("SELECT course_name FROM course_info where id_course= '" + IdCourse + "'");

                                            if (!q.list().isEmpty()) {
                                                CourseName = q.uniqueResult().toString();
                                            } else {
                                               // System.out.println("Do not Get Member ID of  " + username);
                                            }
                                            

                                            
                        %>
                        <div class="row">

                            <div class="col-md-8">
                                
                                 <form id="courseform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmCourse" action="<%out.print(GlobalVariable.baseUrl);%>/course/coursedetails.jsp">
                        <input type="hidden" name="courseid" id="courseid" value="<%=IdCourse%>" >

                         <input type="hidden" name="coursename" id="coursename" value="<%=CourseName%>" >
                         
                         <span><b>Taken Course:</b> </span>
                            <a href="#">                             
                                    
                                        
                                        
                                         <button class="btn btn-outline-secondary" >
                                        <span><%out.print(CourseName);%></span>	
                                         </button>
                                    <br>
                                </a>
                    </form>
                                 </div>

                            
                        </div>
                        <%
                            }
                          }
                        %>
                    </div>
                </div>
            </div>
                    
                    <!--
                    
            <div class="mas-main-sec">
                <div class="mas-main-sec-item">
                    <div class="mas-main-sec-item-header">
                        <span>Saved Videos</span>
                    </div>
                    <div class="mas-main-sec-item-content my-4">
                        <div class="row my-5">
                            <div class="col-md-3 col-sm-6 col-12 s-video"><a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/you.png" alt=""></a></div>
                            <div class="col-md-3 col-sm-6 col-12 s-video save-video"><a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/you.png" alt=""></a></div>
                            <div class="col-md-3 col-sm-6 col-12 s-video save-video"><a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/you.png" alt=""></a></div>
                            <div class="col-md-3 col-sm-6 col-12 s-video save-video"><a href="#"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/you.png" alt=""></a></div>
                        </div>
                    </div>
                </div>
            </div>

                        -->
        </div>
    </div>
</div>
<%    dbsession.flush();
    dbsession.close();


%>
<%@ include file="../footer.jsp" %>


