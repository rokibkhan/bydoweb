<%-- 
    Document   : newPassword
    Created on : Oct 24, 2020, 11:50:06 PM
    Author     : Zeeon
--%>

<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>

<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();

        response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + sessionIdH);

    }

    // System.out.println("Header sessionIdH :: " + sessionIdH);
    //   System.out.println("Header userNameH :: " + userNameH);
//    System.out.println("Header userStoreIdH :: " + memberIdH);
//    String sessionid = request.getParameter("sessionid").trim();
//
//    sessionid = session.getId();
//    String uid = session.getAttribute("username").toString().toUpperCase();    
//    String userStoreId = session.getAttribute("memberId").toString();
//    String sessionidH = request.getParameter("sessionid").trim();
//
//    sessionidH = session.getId();
//    String uidH = session.getAttribute("username").toString().toUpperCase();    
    //   String userStoreIdH = session.getAttribute("memberId").toString();
//System.out.println("uidH :: "+uidH);
    

%>
<div class="py-5" style="background: url(<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/bg.png);background-size: 100% 100%;">
    <div class="container bg-white phy">
        <div class="row">
            <div class="col-md-6 my-5 bg-white rounded-right">
                <div>
                    <img class="rounded mt-5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/education.png" alt="images" width="100%" />
                </div>
            </div>

            <div class="col-md-6 my-5">
                <!-- Default form login -->
                <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/NewPassword">

                    <p class="h4 mb-4 text-center text-danger">Create Password</p>

                    <div id="error" style="display: block; font-size:  small;color: red;  " > 
                        <%
                            String rtnUser = "";

                            try {
                                rtnUser = session.getAttribute("strMsg").toString();
                                if (rtnUser != null) {
                                    out.println(rtnUser);
                                    session.removeAttribute("strMsg");
                                } else {
                                    out.print(" ");
                                }
                            } catch (Exception ex) {
                                // out.print(ex.getMessage());
                            }
                        %>     
                    </div>

                    <!-- Password -->

                    <label for="formInput">Password</label>
                    <div class="input-group mb-3 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-lock fa-lg" style="color:#fc7171"></i>
                            </span>
                        </div>
                        <input type="hidden" id="username" name="username" value="<%=request.getParameter("username")%>">
                        <input id="password" name="password" type="password" class="form-control" required="" placeholder="Password" aria-label="password" aria-describedby="basic-addon1">
                    </div>

                    <!-- Confirm Password -->

                    <label for="formInput">Confirm Password</label>
                    <div class="input-group mb-3 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-lock fa-lg" style="color:#fc7171"></i>
                            </span>
                        </div>
                        <input id="password1" name="password1" type="password" class="form-control" required="" placeholder="Confirm Password" aria-label="password" aria-describedby="basic-addon1">
                    </div>

                    <!-- Submit -->
                    <div class="container text-center">
                        <button id="btn" class="btn btn-danger btn-lg my-4" type="submit">Change Password</button>
                    </div>
                </form>
                <!-- Default form login -->
            </div>
        </div>
    </div>
</div>


<%@ include file="../footer.jsp" %>
