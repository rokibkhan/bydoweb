<%@page import="com.appul.util.GlobalVariable"%>
<%
    // System.out.println("LLL:" + request.getRequestURL());
    String getURIStr = request.getRequestURI();
    // System.out.println("getURL:::" + getURIStr);

    String[] arrOfStr = getURIStr.split("/");

    String currentPageOpt = "";
    for (String curl : arrOfStr) {
        //  System.out.println(curl);

        currentPageOpt = curl;
    }

    // out.println("currentPageOpt:: " + currentPageOpt);
    String footerClass = "";

    if ((currentPageOpt.equalsIgnoreCase("index.jsp")) || (currentPageOpt.equalsIgnoreCase(""))) {
        footerClass = "footer";
    } else {
        footerClass = "footer1";
    }
%>


<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5cdfaa2ff26a120c"></script>

<footer class="<%=footerClass%>">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <div class="footer_top">
                    <div class="row">

                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <div class="footer_single_item">
                                <h4 class="footer_title">Resource Centre</h4>
                                <ul class="contact_link">
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/networks/division.jsp">All Engineering Divisions</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/activities/careerAdvancement.jsp">Career Advancement</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/activities/projectManagement.jsp">Project Management</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/activities/technicalEngineering.jsp">Technical Engineering</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/activities/inHouseCorporateTraining.jsp">In-House Corporate Training</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/activities/academicProgramme.jsp">Academic Programme</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <div class="footer_single_item">
                                <h4 class="footer_title">Membership</h4>
                                <ul class="contact_link">
                                    <%  if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) { %>

                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberDashboard.jsp?sessionid=<%=session.getId()%>">My Profile</a></li>

                                    <%}%>

                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/aboutMemberShip.jsp">About Membership</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/aboutMemberShipCriteria.jsp">Membership Criteria</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/howToGetMemberShip.jsp">How to get Membership</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/accreditation.jsp">Accreditation</a></li>                                    
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/search.jsp">Search your membership No.</a></li>

                                    <%
                                        if (session.getAttribute("registerAppId") != null && session.getAttribute("memberRegId") != null) {

                                            String sessionIdApp = session.getId();
                                            String registerAppIdApp = session.getAttribute("registerAppId").toString();
                                            String memberRegIdApp = session.getAttribute("memberRegId").toString();

                                            String checkAgrOptApp = "registrationDashboard.jsp?sessionid=" + sessionIdApp + "&registerAppId=" + registerAppIdApp;
                                    %>

                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/<%=checkAgrOptApp%>">My Application</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/registrationLogout.jsp">My Application Logout</a></li>

                                    <%
                                    } else {
                                    %>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/member/registrationCheck.jsp">Membership Application Check</a></li>
                                        <%
                                            }
                                        %>



                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <div class="footer_single_item">
                                <h4 class="footer_title">Quick Links</h4>
                                <ul class="contact_link">
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/index.jsp">Home</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/pages/about.jsp">About us</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/activities/scholarship.jsp">Scholarship</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/activities/awardsAndHonours.jsp">Awards & Honours</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/networks/center.jsp">Centre</a></li>
                                    <li><a href="<%out.print(GlobalVariable.baseUrl);%>/pages/iebBloodBank.jsp">Blood Bank</a></li>
                                    <li><a href="#">WebMail</a></li>
                                    <li><a target="_blank" href="http://123.49.3.254/">Old Site</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">

                            <div id="map" style="height: 90%"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3652.4044907930365!2d90.39909661464893!3d23.73295109536031!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8edcde4ebc7%3A0x7d9090df315baaa6!2sThe+Institution+of+Engineers%2C+Bangladesh!5e0!3m2!1sen!2sbd!4v1547635923118" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe></div>

                        </div>
                    </div>
                </div>
                <div class="footer_bottom">
                    <div class="footer_bottom_inner">
                        <p class="copy_right_text">� 2018 The Institution of Engineers, Bangladesh (IEB) | All Rights Reserved</p>
                        <ul class="footer_bottom_menu">
                            <li><a href="#">Sitemap</a></li>
                            <li><a href="#">Terms &amp; conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Support</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>



</footer>




<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/js/jquery-3.3.1.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/js/bootstrap.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/js/slick.min.js"></script>
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/video-player/jquery.mb.YTPlayer.js"></script>



<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/js/custom.js"></script>



<% if (currentPageOpt.equalsIgnoreCase("photogallery.jsp") || currentPageOpt.equalsIgnoreCase("newsDetails.jsp") || currentPageOpt.equalsIgnoreCase("engineeringNews.jsp") || currentPageOpt.equalsIgnoreCase("engineeringNewsDetails.jsp") || currentPageOpt.equalsIgnoreCase("events.jsp") || currentPageOpt.equalsIgnoreCase("eventsDetails.jsp")) { %>
<!-- Slider_Akass -->

<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/sliderengine/amazingslider.js"></script>
<link rel="stylesheet" type="text/css" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/sliderengine/amazingslider-1.css">
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/sliderengine/initslider-1.js"></script>

<%} %>



<% if (currentPageOpt.equalsIgnoreCase("news.jsp") || currentPageOpt.equalsIgnoreCase("newsDetails.jsp") || currentPageOpt.equalsIgnoreCase("engineeringNews.jsp") || currentPageOpt.equalsIgnoreCase("engineeringNewsDetails.jsp") || currentPageOpt.equalsIgnoreCase("events.jsp") || currentPageOpt.equalsIgnoreCase("eventsDetails.jsp")) { %>

<!-- Masonry (from isotope) JS -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/js/isotope.pkgd.min.js"></script>


<script type="text/javascript">


// Masonry JS for the news page
    $('.grid').isotope({
        itemSelector: '.grid-item',
        masonry: {
            columnWidth: 100
        }
    });

// More News and Event details section JS
    $(window).scroll(function () {

        //Foloowing if block is for the "More News" section in news.html
        if ($(this).scrollTop() > 510) {
            $('.other-news').fadeIn(3000);
        }

        //Foloowing if block is for the "Event details" section in single_event.html
        if ($(this).scrollTop() > 604) {
            $('.event-details').slideDown(5000);
        }
    });

</script>

<% } %>


<% if (currentPageOpt.equalsIgnoreCase("photogallery.jsp")) { %>

<!-- for only photogallary page

<!-- Lightbox JS -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/js/lightbox.js"></script>

<!-- Masonry (from isotope) JS -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/js/isotope.pkgd.min.js"></script>
<!-- Our Custom js -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/js/photo_custom.js"></script>
<% } %>



<% if (currentPageOpt.equalsIgnoreCase("videoGallery.jsp")) { %>


<!-- Masonry (from isotope) JS -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/js/isotope.pkgd.min.js"></script>
<!-- Our Custom js -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/js/photo_success_custom.js"></script>
<% } %>



<% if (currentPageOpt.equalsIgnoreCase("jobs.jsp")) { %>

<!-- Parallaxie Plugin -->
<script src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/js/parallaxie.js"></script>

<!-- Wow JS additional JS -->
<script type="text/javascript">

    $('.parallaxie').parallaxie({
        speed: 1.5
    });
</script>

<% }%>

<script type="text/javascript">


    $(function () {

        //hang on event of form with id=myform
        $("#reqSubscribeEmailOptFrm").submit(function (e) {

            //    alert("OK");
            //    btnSubscribeEmailOpt

            //prevent Default functionality
            e.preventDefault();

            var subscribeEmailOpt, subscribeEmailOptToken;

            subscribeEmailOpt = $("#subscribeEmailOpt").val();

            if (subscribeEmailOpt == '') {
                $("#subscribeEmailOpt").focus();
                //  $("#subscribeEmailOpt").css({"background-color1": "#fff", "border": "1px solid red"});
                return false;
            }

            subscribeEmailOptToken = "";

            $("#btnSubscribeEmailOpt").button('loading');

            $.post("subscribeRequestProcess.jsp", {
                subscribeEmailOptID: subscribeEmailOpt,
                subscribeEmailOptToken: subscribeEmailOptToken
            }, function (data) {

                if (data[0].responseCode == 1) {
                    $("#subscribeEmailOpt").val('');

                    //		reqShareMailModal.modal('hide');
                    //	    	 	$("#globalAlertInfoBoxCon").show();
                    $("#btnSubscribeEmailOpt").button('reset');
                    //		$("#btnSubscribeEmailOpt").button('complete') // button text will be "finished!"
                    $("#btnSubscribeEmailOpt").addClass("disabled");
                    $("#msgSubscribeEmailOpt").html(data[0].responseMsg);
                    $("#globalAlertInfoBoxCon").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
                }
                if (data[0].responseCode == 0) {
                    $("#btnSubscribeEmailOpt").button('reset');
                    $("#msgSubscribeEmailOpt").html(data[0].responseMsg);
                    $("#globalAlertInfoBoxCon").html(data[0].responseMsgHtml).show().delay(5000).fadeOut("slow");
                }

            }, "json");

        }); //end form




        //hang on event of form with id=myform
        $("#frmContact").submit(function (e) {

            //prevent Default functionality
            e.preventDefault();

            var conToken, contactName, contactEmail, contactPhone, contactSubject, contactMessage, url;



            contactName = $("#contactName").val();
            contactEmail = $("#contactEmail").val();
            contactPhone = $("#contactPhone").val();
            contactSubject = $("#contactSubject").val();
            contactMessage = $("#contactMessage").val();

            if (contactName == '') {
                $("#contactName").focus();
                $("#contactName").css({"background-color": "#fff", "border": "1px solid red"});
                return false;
            }
            if (contactEmail == '') {
                $("#contactEmail").focus();
                $("#contactEmail").css({"background-color": "#fff", "border": "1px solid red"});
                return false;
            }

            if (contactPhone == '') {
                $("#contactPhone").focus();
                $("#contactPhone").css({"background-color": "#fff", "border": "1px solid red"});
                return false;
            }

            if (contactSubject == '') {
                $("#contactSubject").focus();
                $("#contactSubject").css({"background-color": "#fff", "border": "1px solid red"});
                return false;
            }

            if (contactMessage == '') {
                $("#contactMessage").focus();
                $("#contactMessage").css({"background-color": "#fff", "border": "1px solid red"});
                return false;
            }

            loginTokenTXVC = '';

            $("#btnFrmContact").button('loading');

            conToken = "";

            //  console.log("contactPhone: " + contactPhone);

            url = '<%=GlobalVariable.baseUrl%>' + '/pages/contactInfoSubmitProcess.jsp';

            //   console.log("url: " + url);

            $.post(url, {conToken: conToken, contactName: contactName, contactEmail: contactEmail, contactPhone: contactPhone, contactSubject: contactSubject, contactMessage: contactMessage}, function (data) {
                //  console.log(data);

                if (data[0].responseCode == 1) {
                    //    $("#msgContact").html(data[0].responseMsg);
                    $("#msgContact").html(data[0].responseMsgHtml);
                    $("#contactName").val('');
                    $("#contactEmail").val('');
                    $("#contactPhone").val('');
                    $("#contactSubject").val('');
                    $("#contactMessage").val('');

                }
                if (data[0].responseCode == 0) {
                    // $("#msgContact").html(data[0].responseMsg);
                    $("#msgContact").html(data[0].responseMsgHtml);
                }

            }, "json");




        }); //end form

    }); //docReady function end
</script>


<!-- sample modal content -->
<div id="rupantorLGModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="rupantorLGModalTitle" style="text-align: left !important;">Large modal</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            </div>
            <div class="modal-body" id="rupantorLGModalBody">
                <h4>Overflowing text to show scroll behavior</h4>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
            </div>
            <div class="modal-footer" id="rupantorLGModalFooter">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- sample modal content -->
<div id="taskLGModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="taskLGModalTitle" style="text-align: left !important;"></h4> 
            </div>
            <div class="modal-body" id="taskLGModalBody">
            </div>
            <div class="modal-footer" id="taskLGModalFooter">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->    


<!-- sample modal content -->
<div id="memberLGModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="margin-top: 100px;">
        <div class="modal-content">           

            <div class="modal-header">
                <h5 class="modal-title" id="memberLGModalTitle" style="text-align: left !important;"></h5> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            </div>

            <div class="modal-body" id="memberLGModalBody">
            </div>
            <div class="modal-footer" id="memberLGModalFooter">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->    


<!--Modal: Name-->
<div id="videoPlayerModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Body-->
            <div class="modal-body mb-0 p-0" id="videoPlayerModalBody">

            </div>
            <!--Footer-->
            <div class="modal-footer justify-content-center modal-social-icon-partho" id="videoPlayerModalFooter">
                <span class="mr-4">Share</span>
                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                <!--Twitter-->
                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                <!--Google +-->
                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                <!--Linkedin-->
                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>
                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: Name-->

</body>

</html>