<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<!-- News & Notices & Single Notice CSS -->
<link href="<%=GlobalVariable.baseUrl%>/commonUtil/assets/css/news_events_style.css" rel="stylesheet">

<link href="<%=GlobalVariable.baseUrl%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>



<!-- Start of navigation -->
<section class="navigation_partho parallaxie text-center notice_banner_saz1" style="height:150px;background:url('<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/823-1080x600-blur_6.jpg')">
    <h3 class="h1 pl-5 mx-5 font-weight-bold text-white pt-5">Notice Details</h3>
    <nav aria-label="breadcrumb" class="w-100 pl-5">
        <ol class="breadcrumb h5" style="background-color: #e9ecef00" >
            <li class="breadcrumb-item"><a href="home_v4.html">Home</a></li> 
            <li class="breadcrumb-item"><a href="#">New & Notice</a></li>
            <li class="breadcrumb-item"><a href="#">Notice</a></li>
                <li class="breadcrumb-item active" aria-current="page">Notice Details</li>
        </ol>
    </nav>
</section>
<!-- End of navigation -->
<%
    Session dbsessionNotice = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxNotice = null;
    dbtrxNotice = dbsessionNotice.beginTransaction();

    String noticeIdX = request.getParameter("noticeIdX") == null ? "" : request.getParameter("noticeIdX").trim();

    // DateFormat dateFormatNotice = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatNotice = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateFormatNoticeM = new SimpleDateFormat("MMM");
    DateFormat dateFormatNoticeD = new SimpleDateFormat("dd");
    DateFormat dateFormatNoticeY = new SimpleDateFormat("Y");

    Date dateNoticeM = null;
    Date dateNoticeD = null;
    Date dateNoticeY = null;
    String newDateNoticeM = "";
    String newDateNoticeD = "";
    String newDateNoticeY = "";

    Query noticeSQL = null;
    Object noticeObj[] = null;
    String noticeId = "";
    String noticeTitle = "";
    String noticeDetials = "";

    String noticeDateTime = "";
    String noticeMonth = "";
    String noticeDay = "";
    String noticeYear = "";

    String noticeDownloadUrl = "";
    String noticeDetailsUrl = "";
    String noticeInfoContainer = "";
    int ntc = 1;


%>






<div class="container">
    <div class="row">
        <div class="col-md-8">

            <%                noticeSQL = dbsessionNotice.createSQLQuery("SELECT * FROM post_notice pn "
                        + "WHERE  pn.id_notice_content = '" + noticeIdX + "'");

                if (!noticeSQL.list().isEmpty()) {
                    for (Iterator itNotice = noticeSQL.list().iterator(); itNotice.hasNext();) {

                        noticeObj = (Object[]) itNotice.next();
                        noticeId = noticeObj[0].toString().trim();
                        noticeTitle = noticeObj[4].toString().trim();

                        noticeDetials = noticeObj[6].toString().trim();

                        noticeDateTime = noticeObj[8].toString().trim();

                        dateNoticeM = dateFormatNotice.parse(noticeDateTime);
                        newDateNoticeM = dateFormatNoticeM.format(dateNoticeM);

                        dateNoticeD = dateFormatNotice.parse(noticeDateTime);
                        newDateNoticeD = dateFormatNoticeD.format(dateNoticeD);

                        dateNoticeY = dateFormatNotice.parse(noticeDateTime);
                        newDateNoticeY = dateFormatNoticeY.format(dateNoticeY);

                        noticeDownloadUrl = noticeObj[7].toString().trim();

                        noticeDetailsUrl = GlobalVariable.baseUrl + "/notice/noticeDetails.jsp?noticeIdX=" + noticeId;

                        //  noticeInfoContainer = noticeInfoContainer + ""
                        //          + "<span><a class=\"notice_th_marquee_home\" href=\" " + noticeDetailsUrl + "\">" + noticeTitle + "</a></span> * ";
            %>

            <div class="news_cont_saz" style="padding: 0;">
                <div class="row">
                    <div class="col-md-1 bg-primary">

                    </div>
                    <div class="col-md-10">       
                        <p><i class="fas fa-calendar-check mr-3"></i> <%=newDateNoticeD%> <%=newDateNoticeM%>, <%=newDateNoticeY%></p>
                        <p><strong><%=noticeTitle%></strong> </p>
                        <p>
                            <%=noticeDetials%>
                        <p>
                            <br>
                            <a href="<%=noticeDownloadUrl%>" target="_blank" class="btn btn-sm btn-primary text-white"> <i class="fas fa-file"></i>  download</a>
                            <br><br>
                    </div>
                </div>

            </div>

            <%
                        ntc++;
                    }
                }

            %>



        </div>
        <!-- ./end col-md-8 -->

        <div class="col-md-4">

            <div class="news_cont_saz" style="padding: 0">
                <div class="card-header" style=" background: #fff;">
                    <p class="font-weight-bold text-center text-primary" style="font-size:1.10rem;">Recent Notices</p>
                </div>
                <div class="card-body p-3"> 
                    <p class="font-weight-bold text-left" style="margin-bottom: 2px !important; "> <a href="#">Associate Technical Engineer Analyst</a></p>
                    <p class="text-left">Perpay Inc.<p>
                    <p class="text-left">Philadelphia, PA, USA</p>
                    <hr>
                    <p class="font-weight-bold text-left" style="margin-bottom: 2px !important; "> <a href="#">Associate Technical Engineer Analyst</a></p>
                    <p class="text-left">Perpay Inc.<p>
                    <p class="text-left">Philadelphia, PA, USA</p>
                    <hr>
                    <p class="font-weight-bold text-left" style="margin-bottom: 2px !important; "> <a href="#">Associate Technical Engineer Analyst</a></p>
                    <p class="text-left">Perpay Inc.<p>
                    <p class="text-left">Philadelphia, PA, USA</p>
                </div>
            </div>
            <div class="news_cont_saz" style="padding: 0">
                <div class="card-header" style=" background: #fff;">
                    <p class="font-weight-bold text-center text-primary" style="font-size:1.10rem;">Recent Jobs</p>
                </div>
                <div class="card-body p-3"> 
                    <p class="font-weight-bold text-left" style="margin-bottom: 2px !important; "> <a href="#">Associate Technical Engineer Analyst</a></p>
                    <p class="text-left">Perpay Inc.<p>
                    <p class="text-left">Philadelphia, PA, USA</p>
                    <hr>
                    <p class="font-weight-bold text-left" style="margin-bottom: 2px !important; "> <a href="#">Associate Technical Engineer Analyst</a></p>
                    <p class="text-left">Perpay Inc.<p>
                    <p class="text-left">Philadelphia, PA, USA</p>
                    <hr>
                    <p class="font-weight-bold text-left" style="margin-bottom: 2px !important; "> <a href="#">Associate Technical Engineer Analyst</a></p>
                    <p class="text-left">Perpay Inc.<p>
                    <p class="text-left">Philadelphia, PA, USA</p>
                </div>
            </div>


        </div>
    </div>
</div>
<%    dbsessionNotice.clear();
    dbsessionNotice.close();

%>

<%@ include file="../footer.jsp" %>
