<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<!-- News & Notices & Single Notice CSS -->
<link href="<%=GlobalVariable.baseUrl%>/commonUtil/assets/css/news_events_style.css" rel="stylesheet">

<link href="<%=GlobalVariable.baseUrl%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>





<!-- Start of navigation -->
<section class="navigation_partho parallaxie text-center" style="height:150px;background:url('<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/News-Banner.jpg');position:relative;">
    <div style="position:absolute;top:0;left:0;width:100%;height:100%;background:rgba(0,0,0,0.34);">
        <h3 class="h1 pl-5 mx-5 font-weight-bold text-white pt-5">Notice</h3>
        <nav aria-label="breadcrumb" class="w-100 pl-5">
            <ol class="breadcrumb h5" style="background-color: #e9ecef00" >
                <li class="breadcrumb-item"><a href="home_v4.html">Home</a></li>
                <li class="breadcrumb-item"><a href="home_v4.html">News & Events</a></li>
                <li class="breadcrumb-item active text-white" aria-current="page" >Notice</li>
            </ol>
        </nav>
    </div>
</section>
<!-- End of navigation -->

<%
    Session dbsessionNotice = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxNotice = null;
    dbtrxNotice = dbsessionNotice.beginTransaction();

    // DateFormat dateFormatNotice = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatNotice = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateFormatNoticeM = new SimpleDateFormat("MMM");
    DateFormat dateFormatNoticeD = new SimpleDateFormat("dd");
    DateFormat dateFormatNoticeY = new SimpleDateFormat("Y");

    Date dateNoticeM = null;
    Date dateNoticeD = null;
    Date dateNoticeY = null;
    String newDateNoticeM = "";
    String newDateNoticeD = "";
    String newDateNoticeY = "";

    Query noticeSQL = null;
    Object noticeObj[] = null;
    String noticeId = "";
    String noticeTitle = "";

    String noticeDateTime = "";
    String noticeMonth = "";
    String noticeDay = "";
    String noticeYear = "";

    String noticeDownloadUrl = "";
    String noticeDetailsUrl = "";
    String noticeInfoContainer = "";
    int ntc = 1;


%>






<div class="container">
    <div class="row">
        <div class="col-md-8">

            <%                noticeSQL = dbsessionNotice.createSQLQuery("SELECT * FROM post_notice pn "
                        + "ORDER BY pn.id_notice_content DESC "
                        + "LIMIT 0,10");

                if (!noticeSQL.list().isEmpty()) {
                    for (Iterator itNotice = noticeSQL.list().iterator(); itNotice.hasNext();) {

                        noticeObj = (Object[]) itNotice.next();
                        noticeId = noticeObj[0].toString().trim();
                        noticeTitle = noticeObj[4].toString().trim();
                        noticeDateTime = noticeObj[8].toString().trim();

                        dateNoticeM = dateFormatNotice.parse(noticeDateTime);
                        newDateNoticeM = dateFormatNoticeM.format(dateNoticeM);

                        dateNoticeD = dateFormatNotice.parse(noticeDateTime);
                        newDateNoticeD = dateFormatNoticeD.format(dateNoticeD);

                        dateNoticeY = dateFormatNotice.parse(noticeDateTime);
                        newDateNoticeY = dateFormatNoticeY.format(dateNoticeY);

                        noticeDownloadUrl = noticeObj[7].toString().trim();

                        noticeDetailsUrl = GlobalVariable.baseUrl + "/notice/noticeDetails.jsp?noticeIdX=" + noticeId;

                        //  noticeInfoContainer = noticeInfoContainer + ""
                        //          + "<span><a class=\"notice_th_marquee_home\" href=\" " + noticeDetailsUrl + "\">" + noticeTitle + "</a></span> * ";
%>

            <div class="news_cont_saz" style="padding: 0;">
                <div class="row">
                    <div class="col-md-2 bg-primary event_date">
                        <span class="p_date">
                            <span class="day"><%=newDateNoticeD%></span>
                            <span class="mon"><%=newDateNoticeM%></span>
                            <span class="mon"><%=newDateNoticeY%></span>
                        </span>
                    </div>
                    <div class="col-md-10">                    
                        <p>
                            <%=noticeTitle%>
                        </p>
                        <a href="<%=noticeDetailsUrl%>" class="btn btn-sm btn-primary text-white"> <i class="fa fa-info-circle"></i>  details</a>
                        <a href="<%=noticeDownloadUrl%>" target="_blank" class="btn btn-sm btn-primary text-white"> <i class="fas fa-file"></i>  download</a>
                    </div>
                </div>

            </div>

            <%
                        ntc++;
                    }
                }

            %>



        </div>
        <!-- ./end col-md-8 -->

        <div class="col-md-4">
            <div class="news_cont_saz" style="padding: 0">
                <div class="card-header" style=" background: #fff;">
                    <p class="font-weight-bold text-center text-primary" style="font-size:1.10rem;">Recent Jobs</p>
                </div>
                <div class="card-body p-3"> 
                    <p class="font-weight-bold text-left" style="margin-bottom: 2px !important; "> <a href="#">Associate Technical Engineer Analyst</a></p>
                    <p class="text-left">Perpay Inc.<p>
                    <p class="text-left">Philadelphia, PA, USA</p>
                    <hr>
                    <p class="font-weight-bold text-left" style="margin-bottom: 2px !important; "> <a href="#">Associate Technical Engineer Analyst</a></p>
                    <p class="text-left">Perpay Inc.<p>
                    <p class="text-left">Philadelphia, PA, USA</p>
                    <hr>
                    <p class="font-weight-bold text-left" style="margin-bottom: 2px !important; "> <a href="#">Associate Technical Engineer Analyst</a></p>
                    <p class="text-left">Perpay Inc.<p>
                    <p class="text-left">Philadelphia, PA, USA</p>
                </div>
            </div>


        </div>
    </div>
</div>
<%    dbsessionNotice.clear();
    dbsessionNotice.close();

%>

<%@ include file="../footer.jsp" %>
