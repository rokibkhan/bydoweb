<%@ include file="header.jsp" %>

<link rel="stylesheet" type="text/css" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/owl.carousel/engine/style.css" />
<script type="text/javascript" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/owl.carousel/engine/jquery.js"></script>
<!-- start banner_wrap -->
<section class="banner_wrap">
    <div class="overlay1" style="z-index: 9"></div>

    <div id="wowslider-container1">
        <div class="ws_images">
            <ul>
                <li><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/banner/slidepicturemain.jpg" alt="slider" title="slide-picture-main" id="wows1_0"/></li>
                <li><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/banner/slidepicture.jpg" alt="slider" title="slide-picture" id="wows1_1"/></li>
                <li> <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/banner/slidepicture1.jpg" alt="slider" title="slide-picture1" id="wows1_2"/></li>
                <li><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/banner/slidepicture2.jpg" alt="slider" title="slide-picture2" id="wows1_3"/></li>
            </ul>
        </div>

        <div class="ws_shadow"></div>
    </div>	
    <script type="text/javascript" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/owl.carousel/engine/wowslider.js"></script>
    <script type="text/javascript" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/owl.carousel/engine/script.js"></script>






</section>
<!-- end banner_wrap -->
<!-- start about_wrap -->
<section class="notice_wrap">
    <div class="notice_caption">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                    <p style="z-index: 20">


                        <%@ include file="notice/landingPageNotice.jsp" %>


                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- start about_wrap -->
<section class="about_wrap">
    <div class="gear_icon">
        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gear-icon.png" alt="img">
    </div>
    <div class="container custom_pad">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                <div class="about_content">
                    <h5 class="about_sub_title">About</h5>
                    <h2 class="about_title">The Institution of Engineers, Bangladesh</h2>
                    <p class="about_para">The Institution of Engineers, Bangladesh (IEB) is the most prestigious National Professional Organization of the country. It is registered under the Societies Registration Act of the country. IEB includes all disciplines of engineering. Currently, it has in its roll more than 41,545 engineers with about 30% in the category of Fellows, 60% Members and the rest as Associate Members.</p>
                    
                    <div class="btn_group">
                        <a href="<%out.print(GlobalVariable.baseUrl);%>/member/registration.jsp" class="btn btn_glob btn_more btn-primary">Apply Membership</a>
                        <a href="<%out.print(GlobalVariable.baseUrl);%>/pages/about.jsp" class="btn btn_glob btn_apply btn-success">More Details</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                <div class="about_img_wrap">
                    <div class="about_img">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/about_img_home_page.jpg" alt="img" class="img-fluid img_1">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/about_Img-Shap_1.png" alt="img" class="shap_1">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/about_Img-Shap_2.png" alt="img" class="shap_2">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end about_wrap -->


<%@ include file="message/landingPageMessage.jsp" %>


<%@ include file="news/landingPageNews.jsp" %>


<%@ include file="events/landingPageEvents.jsp" %>


<%@ include file="photogallery/landingPagePhotoGallery.jsp" %>


<%@ include file="videogallery/landingPageVideoGallery.jsp" %>




<!-- start subcribe_wrap -->
<section class="subcribe_wrap">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <div class="subcribe_inner">
                    <div class="row d_align">
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <div class="subcribe_content">
                                <h3 class="subcribe_title">Subscribe Our Newsletter</h3>
                                <p class="subcribe_para">Be the first to receive the latest news and updates from IEB directly in your inbox.</p>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="subcribe_btn">
                                <a href="#" class="btn_sub">Subscribe</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end subcribe_wrap -->


<%@ include file="successstory/landingPageSuccessStory.jsp" %>





<!-- start subscribe_wrap_2 -->
<section class="subscribe_wrap_2" style="background-color: #fff;">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <div class="subscribe_inner">
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/about_Img-Shap_1.png" class="shape_1" alt="img">
                    <img src="<%out.print(GlobalVariable.baseUrl);
                         %>/commonUtil/assets/images/about_Img-Shap_1_2.png" class="shape_2" alt="img">
                    <h2 class="subscript_title text-center">Subscribe Our Newsletter</h2>
                    <p class="text-center sub_para">Be the first to receive the latest news and updates from IEB directly in your inbox.</p>
                    <form class="sub_form" id="reqSubscribeEmailOptFrm">
                        <div class="d_flex d_align">
                            <input name="subscribeEmailOpt" id="subscribeEmailOpt" class="form-control" type="email" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'">
                            <button name="btnSubscribeEmailOpt" id="btnSubscribeEmailOpt" class="btn" type="submit">Subscribe</button>
                        </div>
                        <span id="msgSubscribeEmailOpt" class="help-block" style="color: #fff;"></span>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end subscribe_wrap_2 -->


<%@ include file="footer.jsp" %>
