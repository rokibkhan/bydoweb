<%@page import="java.net.InetAddress"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Query q2 = null;
    Object obj[] = null;

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";
    getRegistryID regId = new getRegistryID();
    String subscribeId = regId.getID(14);

    String emailId = request.getParameter("subscribeEmailOptID").trim();

    String name = "";
  //  System.out.println("emailId: " + emailId);

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date dateX = new Date();
    String adddate = dateFormatX.format(dateX);
    String adduser = "";
    String addterm = InetAddress.getLocalHost().getHostName().toString();
    String addip = InetAddress.getLocalHost().getHostAddress().toString();

    q1 = dbsession.createSQLQuery("SELECT * FROM subscribers WHERE EMAIL_ADDRESS='" + emailId + "'");
    if (q1.list().isEmpty()) {

        Query q4 = dbsession.createSQLQuery("INSERT INTO subscribers("
                + "ID,"
                + "NAME,"
                + "EMAIL_ADDRESS,"
                + "ADD_DATE,"
                + "ADD_USER,"
                + "ADD_TERM,"
                + "ADD_IP) values( "
                + "'" + subscribeId + "',"
                + "'" + name + "',"
                + "'" + emailId + "',"
                + "'" + adddate + "',"
                + "'" + adduser + "',"
                + "'" + addterm + "',"
                + "'" + addip + "')");

        q4.executeUpdate();

        dbtrx.commit();
        if (dbtrx.wasCommitted()) {
            responseCode = "1";
            responseMsg = "Success!!! Email address added successfully";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> Success!!! Email address added successfully</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";

        } else {
            responseCode = "0";
            responseMsg = "Error!!! When Email address in system";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> Error!!! When Email address in system</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";
        }

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", emailId);
        jsonArr.add(json);

    } else {

        responseCode = "0";
        responseMsg = "Error!!! Email address already subscribe";
        responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> Error!!! No photo info found</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        json.put("requestId", emailId);
        jsonArr.add(json);
    }
    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>