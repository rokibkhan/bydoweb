<%@ include file="../header.jsp" %>


<!-- Lightbox CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/lightbox.css">	
<!-- Our Custom CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/photo_contact_style.css">

<section>

    <nav aria-label="breadcrumb"> 
        <div class="container">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Library</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </div>
    </nav>

    <!-- Photos Section starting -->
    <section class="my-masonry pt-0" id="portfolio">
        <div class="container">
            <h2 class="text-center mx-auto mt-5 w-50 py-3 border border-dark font-weight-bold text-dark"> Photo Gallery</h2>
            <div class="button-group filter-button-group text-center py-5">
                <button data-filter="*">All</button>
                <button data-filter=".event-1">Event-1</button>
                <button data-filter=".event-2">Event-2</button>
                <button data-filter=".event-3">Event-3</button>
            </div>
        </div>


        <div class="grid">
            <div class="grid-sizer"></div>

            <div class="grid-item grid-item--width2 grid-item--height1-3 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_1.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_1.jpg" alt="Masonry image 1" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height2 event-1 event-3">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_2.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_2.jpg" alt="Masonry image 2" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_3.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_3.jpg" alt="Masonry image 3" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height1-3 event-1 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_4.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_4.jpg" alt="Masonry image 6" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item event-3">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_5.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_5.jpg" alt="Masonry image 4" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item event-1">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_5.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_5.jpg" alt="Masonry image 5" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height1-3 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_1.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_1.jpg" alt="Masonry image 1" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height2 event-1 event-3">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_2.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_2.jpg" alt="Masonry image 2" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_3.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_3.jpg" alt="Masonry image 3" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height1-3 event-1 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_4.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_4.jpg" alt="Masonry image 6" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item event-3">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_5.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_5.jpg" alt="Masonry image 4" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item event-1">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_5.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_5.jpg" alt="Masonry image 5" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height1-3 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_1.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_1.jpg" alt="Masonry image 1" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height2 event-1 event-3">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_2.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_2.jpg" alt="Masonry image 2" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_3.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_3.jpg" alt="Masonry image 3" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height1-3 event-1 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_4.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_4.jpg" alt="Masonry image 6" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item event-3">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_5.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_5.jpg" alt="Masonry image 4" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item event-1">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_5.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_5.jpg" alt="Masonry image 5" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height1-3 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_1.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_1.jpg" alt="Masonry image 1" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height2 event-1 event-3">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_2.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_2.jpg" alt="Masonry image 2" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_3.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_3.jpg" alt="Masonry image 3" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height1-3 event-1 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_4.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_4.jpg" alt="Masonry image 6" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item event-3">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_5.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_5.jpg" alt="Masonry image 4" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item event-1">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_5.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_5.jpg" alt="Masonry image 5" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height1-3 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_1.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_1.jpg" alt="Masonry image 1" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height2 event-1 event-3">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_2.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_2.jpg" alt="Masonry image 2" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_3.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_3.jpg" alt="Masonry image 3" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item grid-item--width2 grid-item--height1-3 event-1 event-2">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_4.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_4.jpg" alt="Masonry image 6" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item event-3">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_5.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/gallary_img_5.jpg" alt="Masonry image 4" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
            <div class="grid-item event-1">
                <a href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_5.jpg" data-lightbox="example-set">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_5.jpg" alt="Masonry image 5" />
                    <div class="my-masonry-overlay d-flex justify-content-center align-items-center"></div>
                </a>
            </div>
        </div>

        <br><br>


        <div class="row">

            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 offset-lg-1 offset-md-1">

                <!--Carousel Wrapper-->
                <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
                    <!--Slides-->
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(88).jpg" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(121).jpg" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(31).jpg" alt="Third slide">
                        </div>
                    </div>
                    <!--/.Slides-->
                    <!--Controls-->
                    <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <!--/.Controls-->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-thumb" data-slide-to="0" class="active"> 
                            <img class="d-block w-300" src="https://mdbootstrap.com/img/Photos/Others/Carousel-thumbs/img%20(88).jpg" class="img-fluid">
                        </li>
                        <li data-target="#carousel-thumb" data-slide-to="1">
                            <img class="d-block w-300" src="https://mdbootstrap.com/img/Photos/Others/Carousel-thumbs/img%20(121).jpg"  class="img-fluid">
                        </li>
                        <li data-target="#carousel-thumb" data-slide-to="2">
                            <img class="d-block w-300" src="https://mdbootstrap.com/img/Photos/Others/Carousel-thumbs/img%20(31).jpg"  class="img-fluid">
                        </li>
                    </ol>
                </div>
                <!--/.Carousel Wrapper-->

            </div>

            <script type="text/javascript">


                $('.carousel').carousel()

            </script>
        </div>


    </section>
    <!-- Photos Section finishes -->

    <!-- Start of Pagination -->
    <nav class="other-news news-pagination mt-5">
        <ul class="pagination justify-content-center pagination-lg">
            <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
            </li>
            <li class="page-item active"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
                <a class="page-link" href="#">Next</a>
            </li>
        </ul>
    </nav>
    <!-- End of Pagination -->






    <%@ include file="../footer.jsp" %>
