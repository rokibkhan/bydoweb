<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>

<%@page import="com.appul.util.GlobalVariable"%>

<%@ include file="../header.jsp" %>


<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>



<!-- Start of navigation -->
<div class="about_banner_gallery">
    <h2 class="display_41_saz text-white"> Photo Album</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Gallery</a></li>
                <li class="breadcrumb-item active text-white" aria-current="page" >Photo Album</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of navigation -->

<!-- end photo_gallary_wrap -->
<section class="photo_gallary_wrap">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <h3 class="sec_title1 text-center" style="margin: 20px 0;">&nbsp; <span class="float-right"></h3>
            </div>
        </div>
        <div class="row">

            <%
                Session dbsessionPhotoGallary = HibernateUtil.getSessionFactory().openSession();
                org.hibernate.Transaction dbtrxPhotoGallary = dbsessionPhotoGallary.beginTransaction();

                Query q1 = null;
                Query q2 = null;
                Object objPhotoAlbum[] = null;
                Object objPhoto[] = null;

                int ix = 1;
                String photoAlbumId = "";
                String photoAlbumName = "";
                String photoAlbumShortName = "";
                String pictureId = "";
                String pictureTitle = "";
                String pictureCaption = "";
                String pictureOrginalName = "";
                String pictureName = "";
                String pictureThumb = "";
                String showingOrder = "";
                String photoAlbum = "";
                String photoSrc = "";
                String photoLink = "";
                String agrX = "";
                String albumDetailsUrl = "";

//                Query usrSQL = dbsessionPhotoGallary.createSQLQuery("SELECT "
//                        + "pg.id_picture,pg.picture_title,pg.picture_caption ,pg.picture_orginal_name,pg.picture_name,pg.picture_thumb ,pg.showing_order, "
//                        + "pc.category_name "
//                        + "FROM  picture_gallery_info pg,picture_category pc "
//                        + "WHERE pc.id_cat = pg.picture_category "
//                        + "ORDER BY pg.id_picture DESC LIMIT 0, 6");
                Query photoAlbumSQL = dbsessionPhotoGallary.createSQLQuery("SELECT * FROM  picture_category pc ORDER BY pc.id_cat DESC");

                if (!photoAlbumSQL.list().isEmpty()) {
                    for (Iterator itPhoto = photoAlbumSQL.list().iterator(); itPhoto.hasNext();) {

                        objPhotoAlbum = (Object[]) itPhoto.next();
                        photoAlbumId = objPhotoAlbum[0].toString().trim();
                        photoAlbumName = objPhotoAlbum[1].toString().trim();
                        photoAlbumShortName = objPhotoAlbum[2].toString().trim();

                        Query photoSQL = dbsessionPhotoGallary.createSQLQuery("SELECT "
                                + "pg.id_picture,pg.picture_title,pg.picture_caption ,pg.picture_orginal_name,pg.picture_name,pg.picture_thumb ,pg.showing_order "
                                + "FROM  picture_gallery_info pg "
                                + "WHERE pg.picture_category = '" + photoAlbumId + "' "
                                + "ORDER BY pg.id_picture DESC LIMIT 0, 1");

                        if (!photoSQL.list().isEmpty()) {
                            for (Iterator it1 = photoSQL.list().iterator(); it1.hasNext();) {

                                objPhoto = (Object[]) it1.next();
                                pictureId = objPhoto[0].toString().trim();
                                pictureTitle = objPhoto[1].toString().trim();
                                pictureCaption = objPhoto[2].toString().trim();
                                pictureOrginalName = objPhoto[3].toString().trim();
                                pictureName = objPhoto[4].toString().trim();
                                pictureThumb = objPhoto[5].toString().trim();
                                showingOrder = objPhoto[6].toString().trim();

                                photoSrc = GlobalVariable.imagePhotoDirLink + pictureName;
                                //    photoSrc = GlobalVariable.imageDirLink + "photos/" + pictureName;

                                photoLink = "<img src=\"" + photoSrc + "\" width=\"370\" height=\"246\"/>";

                                agrX = "'" + pictureId + "','" + session.getId() + "'";
                            }
                        }

                        albumDetailsUrl = GlobalVariable.baseUrl + "/photogallery/photogallery.jsp?albumIdX=" + photoAlbumId;


            %>


            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="photo_gallary_single_container" style="padding: 5px; border: solid #aaaaaa 1px; margin-bottom: 15px; border-radius:5px;">
                    <a href="<%=albumDetailsUrl%>" class="photo_gallary_single_item">
                        <div class="photo_gallary_img">
                            <img src="<%=photoSrc%>" alt="<%=pictureCaption%>" width="360" height="220" style="border-radius:0;">
                        </div>
                        <div class="hover_content">
                            <p><%=photoAlbumName%></p>
                        </div>
                    </a>

                </div>
            </div>

            <%
                        ix++;
                    }

                }

            %>





        </div>

    </div>
</section>
<!-- end photo_gallary_wrap -->



<%    dbsessionPhotoGallary.clear();

    dbsessionPhotoGallary.close();
%>



<%@ include file="../footer.jsp" %>
