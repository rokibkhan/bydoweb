<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@ include file="../header.jsp" %>

<%
    Session dbsessionPhotoGallary = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxPhotoGallary = dbsessionPhotoGallary.beginTransaction();

    String albumIdX = request.getParameter("albumIdX") == null ? "" : request.getParameter("albumIdX").trim();

    if (albumIdX.equals("")) {

        response.sendRedirect(GlobalVariable.baseUrl + "/index.jsp");
    }
    Query q1 = null;
    Query q2 = null;
    Object obj[] = null;

    int ix = 1;
    String pictureId = "";
    String pictureTitle = "";
    String pictureCaption = "";
    String pictureOrginalName = "";
    String pictureName = "";
    String pictureThumb = "";
    String showingOrder = "";
    String photoAlbum = "";
    String photoSrc = "";
    String photoLink = "";
    String agrX = "";
    String sliderImg = "";
    String topPictureList = "";
    String thumbPictureList = "";

    Query usrSQL = dbsessionPhotoGallary.createSQLQuery("SELECT "
            + "pg.id_picture,pg.picture_title,pg.picture_caption ,pg.picture_orginal_name,pg.picture_name,pg.picture_thumb ,pg.showing_order, "
            + "pc.category_name "
            + "FROM  picture_gallery_info pg,picture_category pc "
            + "WHERE pg.picture_category ='" + albumIdX + "' "
            + "AND pc.id_cat = pg.picture_category "
            + "ORDER BY pg.id_picture DESC");
    if (!usrSQL.list().isEmpty()) {
        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

            obj = (Object[]) it1.next();
            pictureId = obj[0].toString().trim();
            pictureTitle = obj[1].toString().trim();
            pictureCaption = obj[2].toString().trim();
            pictureOrginalName = obj[3].toString().trim();
            pictureName = obj[4].toString().trim();
            pictureThumb = obj[5].toString().trim();
            showingOrder = obj[6].toString().trim();
            photoAlbum = obj[7].toString().trim();

            photoSrc = GlobalVariable.imagePhotoDirLink + pictureName;
            //    photoSrc = GlobalVariable.imageDirLink + "photos/" + pictureName;

            topPictureList = topPictureList + "<li><img src=\"" + photoSrc + "\" alt=\"" + pictureTitle + "\"  title=\"" + pictureCaption + "\" /></li>";

            thumbPictureList = thumbPictureList + "<li><img src=\"" + photoSrc + "\" alt=\"1\" title=\"1\" /></li>";

        }
    }
%>



<script type="text/javascript">

</script>
<style>

</style>

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>



<!-- Start of navigation -->
<div class="about_banner_gallery">
    <h2 class="display_41_saz text-white"> Photo Gallery</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Gallery</a></li>
                <li class="breadcrumb-item active text-white" aria-current="page" >Photo Gallery</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of navigation -->
     
    
<!-- End of navigation -->
<section style="margin-top: 40px; margin-bottom: 40px;">
    <div class="row">
            <div class="col-12">
                <h2 class="message_tiele1 text-center" style="padding-bottom: 10px; font: 18px; color: #333333;font-family: 'Playfair Display', serif; "><%=photoAlbum%></h2>
                <hr align="center" width="10%" style="margin-bottom: 20px; border-top: 3px solid #fff;">
            </div>
        </div>
    <div id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:1100px;margin:0px auto 108px;">
        <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
            <ul class="amazingslider-slides" style="display:none;">

                <%=topPictureList%>                
            </ul>
            <ul class="amazingslider-thumbnails" style="display:none;">
                <%=thumbPictureList%>
            </ul>
        </div>
    </div>
</section>


<section>
    <div id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:1100px;margin:0px auto 108px;">
        <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
            <ul class="amazingslider-slides" style="display:none;">

                <%=topPictureList%>                
            </ul>
            <ul class="amazingslider-thumbnails" style="display:none;">
                <%=thumbPictureList%>
            </ul>
        </div>
    </div>
</section>
<!-- Photos Section finishes -->
<br><br>





<%@ include file="../footer.jsp" %>
