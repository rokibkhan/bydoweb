<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@ include file="../header.jsp" %>

<%
    Session dbsessionPhotoGallary = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxPhotoGallary = dbsessionPhotoGallary.beginTransaction();

    String albumIdX = request.getParameter("albumIdX") == null ? "" : request.getParameter("albumIdX").trim();

    if (albumIdX.equals("")) {

        response.sendRedirect(GlobalVariable.baseUrl + "/index.jsp");
    }
    Query q1 = null;
    Query q2 = null;
    Object obj[] = null;

    int ix = 1;
    String pictureId = "";
    String pictureTitle = "";
    String pictureCaption = "";
    String pictureOrginalName = "";
    String pictureName = "";
    String pictureThumb = "";
    String showingOrder = "";
    String photoAlbum = "";
    String photoSrc = "";
    String photoLink = "";
    String agrX = "";
    String sliderImg = "";
    String topPictureList = "";
    String thumbPictureList = "";

    Query usrSQL = dbsessionPhotoGallary.createSQLQuery("SELECT "
            + "pg.id_picture,pg.picture_title,pg.picture_caption ,pg.picture_orginal_name,pg.picture_name,pg.picture_thumb ,pg.showing_order, "
            + "pc.category_name "
            + "FROM  picture_gallery_info pg,picture_category pc "
            + "WHERE pg.picture_category ='" + albumIdX + "' "
            + "AND pc.id_cat = pg.picture_category "
            + "ORDER BY pg.id_picture DESC");
    if (!usrSQL.list().isEmpty()) {
        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

            obj = (Object[]) it1.next();
            pictureId = obj[0].toString().trim();
            pictureTitle = obj[1].toString().trim();
            pictureCaption = obj[2].toString().trim();
            pictureOrginalName = obj[3].toString().trim();
            pictureName = obj[4].toString().trim();
            pictureThumb = obj[5].toString().trim();
            showingOrder = obj[6].toString().trim();
            photoAlbum = obj[7].toString().trim();

            photoSrc = GlobalVariable.imagePhotoDirLink + pictureName;
            //    photoSrc = GlobalVariable.imageDirLink + "photos/" + pictureName;

            topPictureList = topPictureList + "<li><img src=\"" + photoSrc + "\" alt=\"" + pictureTitle + "\"  title=\"" + pictureCaption + "\" /></li>";

            thumbPictureList = thumbPictureList + "<li><img src=\"" + photoSrc + "\" alt=\"1\" title=\"1\" /></li>";

        }
    }
%>



<script type="text/javascript">

</script>
<style>

</style>



<!-- Start of navigation -->
<section class="navigation_partho parallaxie text-center" style="height:150px;background:url('<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/823-1080x600-blur_6.jpg')">
    <h3 class="h1 pl-5 mx-5 font-weight-bold text-white pt-5"><%=photoAlbum%></h3>
    <nav aria-label="breadcrumb" class="w-100 pl-5">
        <ol class="breadcrumb h5" style="background-color: #e9ecef00" >
            <li class="breadcrumb-item"><a href="home_v4.html">Home</a></li>
            <li class="breadcrumb-item"><a href="home_v4.html">Photo Album</a></li>
            <li class="breadcrumb-item active text-white" aria-current="page" ><%=photoAlbum%></li>
        </ol>
    </nav>
</section>
<!-- End of navigation -->
<section>
    <div id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:1100px;margin:0px auto 108px;">
        <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
            <ul class="amazingslider-slides" style="display:none;">

                <%=topPictureList%>                
            </ul>
            <ul class="amazingslider-thumbnails" style="display:none;">
                <%=thumbPictureList%>
            </ul>
        </div>
    </div>
</section>
<!-- Photos Section finishes -->
<br><br>



<!-- Start of Pagination -->
<!--<nav class="other-news news-pagination mt-5">
    <ul class="pagination justify-content-center pagination-lg">
        <li class="page-item disabled">
            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
        </li>
        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
            <a class="page-link" href="#">Next</a>
        </li>
    </ul>
</nav>-->
<!-- End of Pagination -->






<%@ include file="../footer.jsp" %>
