<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Query"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Session"%>

<%@page import="com.appul.util.GlobalVariable"%>


<%@ include file="../header.jsp" %>


<!-- News & Events & Single Event CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/photo_contact_success_style.css" rel="stylesheet">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>


<%
    Session dbsessionCommittee = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxCommittee = dbsessionCommittee.beginTransaction();

    Query queryCommittee = null;
    Object committeeObj[] = null;

    Object objCmmtMem[] = null;
    String idCommitteeMember = "";
    String committeeMemberId = "";
    String committeeMemberIEBId = "";
    String committeeMemberName = "";
    String committeeDesignationName = "";
    String committeeDepartmentName = "";
    String committeeDepartmentName1 = "";
    String profilePicture = "";
    String profilePictureUrl = "";
    String btnCommitteeMemberEdit = "";
    String btnCommitteeMemberDelete = "";
    String commleader = "";
    String classLeader = "";
    String commArgXn = "";
    String committeeMemberInfoCon = "";
    int cm = 1;
    
    String memberProfileUrl = "";

%>

<script type="text/javascript">

    function showDivisonCenterInfo(arg1) {
        var committeeType, formX, btnInvInfo, url, sessionid, cccll;
        console.log("showDivisonCenterInfo -> arg1 :: " + arg1 + " arg2:: ");

        //  ("#rupantorLGModalFooter").html(btnInvInfo);
        //  document.getElementById("committeeTypeSearch").disabled = true;

        committeeType = $("#committeeTypeSearch").val();

        url = '<%=GlobalVariable.baseUrl%>' + '/committee/committeeDivisionCenterShow.jsp';

        sessionarg1 = "";

        $.post(url, {sessionId: sessionarg1, committeeTypeId: committeeType}, function (data) {
//
            console.log(data);

            if (data[0].responseCode == 1) {


                //   $("#showJobApplyMsg"+ data[0].requestId).html(data[0].responseMsg);
                $("#divisionCenterCon").html(data[0].responseData);

                //$("#globalAlertInfoBoxConTT").html(data[0].responseMsgHtml).show().delay(3000).fadeOut("slow");
            }
            if (data[0].responseCode == 0) {
                document.getElementById("divisionCenterIdSearch").disabled = true;

                $("#divisionCenterCon").html("");

            }



        }, "json");


    }
</script>

<%   // String searchString = request.getParameter("searchString") == null ? "" : request.getParameter("searchString").trim();

    String searchString = "";

    String committeeTypeSearch = request.getParameter("committeeTypeSearch") == null ? "" : request.getParameter("committeeTypeSearch").trim();
    String committeeSessionSearch = request.getParameter("committeeSessionSearch") == null ? "" : request.getParameter("committeeSessionSearch").trim();

    String divisionCenterIdSearch = request.getParameter("divisionCenterIdSearch") == null ? "" : request.getParameter("divisionCenterIdSearch").trim();

    //division
    if (!committeeTypeSearch.equals("")) {
        committeeTypeSearch = committeeTypeSearch;
    } else {
        committeeTypeSearch = "4";
    }

    if (!divisionCenterIdSearch.equals("")) {
        divisionCenterIdSearch = divisionCenterIdSearch;
    } else {
        divisionCenterIdSearch = "HQ";
    }

    if (!committeeSessionSearch.equals("")) {
        committeeSessionSearch = committeeSessionSearch;
    } else {
        committeeSessionSearch = "2018-2019";
    }

    if ((committeeTypeSearch != null && !committeeTypeSearch.isEmpty()) && (divisionCenterIdSearch != null && !divisionCenterIdSearch.isEmpty()) && (committeeSessionSearch != null && !committeeSessionSearch.isEmpty())) {

        searchString = " WHERE c.committee_type = '" + divisionCenterIdSearch + "' AND c.com_cat_id = '" + committeeTypeSearch + "' AND c.committee_duration = '" + committeeSessionSearch + "'  ";
    }

//    if ((committeeTypeSearch != null && !committeeTypeSearch.isEmpty()) && (committeeSessionSearch != null && !committeeSessionSearch.isEmpty())) {
//
//        searchString = " WHERE c.com_cat_id = '" + committeeTypeSearch + "' AND c.committee_duration = '" + committeeSessionSearch + "'  ";
//    }
//    else if ((committeeTypeSearch != null && !committeeTypeSearch.isEmpty()) && (committeeSessionSearch != null && !committeeSessionSearch.isEmpty())) {
//       
//        searchString = " WHERE c.com_cat_id = '" + committeeTypeSearch + "' AND c.committee_duration = '" + committeeSessionSearch+ "'  ";
//    }

    

    

%>

<!-- Start of navigation -->
<div class="about_banner_committee">
<h2 class="display_41_saz text-white">Sub Center Committee</h2>
<div class="jb_breadcrumb">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item"><a href="home_v4.html">Home</a></li>
            <li class="breadcrumb-item"><a href="home_v4.html">Committee</a></li>
            <li class="breadcrumb-item active text-white">Sub Center Committee</li>
        </ol>
    </nav>
</div>
</div>
<!-- End of navigation -->



<!-- Comittee search bar starts -->
<section id="comittee_search_bar_partho1" class="bg-primary my-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="banner-div_partho text-center mx-auto py-3 font-weight-bold text-dark align-items-center pt-5">

                    <form class="form-group"  name="committeeSearch" id="committeeSearch" method="get" action="<%out.print(GlobalVariable.baseUrl);%>/committee/subCenterCommittee.jsp" >
                        <div class="row">

                            <input type="hidden" name="committeeTypeSearch" id="committeeTypeSearch" value="4">


                            <%
                                Query q1 = null;
                                Object obj[] = null;

                                String divisionId = "";
                                String divisionShortName = "";
                                String divisionName = "";
                                String divisionOptionCon = "";
                                String divisionInfoCon = "";
                                String divisionCenterSelected = "";

                                if (committeeTypeSearch.equals("1")) {
                                    divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"form-control input-sm\" required>"
                                            + "<option value=\"HQ\">Head Quarter</option>"
                                            + "</select>";
                                }
                                //division
                                if (committeeTypeSearch.equals("2")) {

                                    q1 = dbsessionCommittee.createSQLQuery("select * FROM member_division WHERE mem_division_id !='30' ORDER BY mem_division_id ASC");

                                    for (Iterator divisionItr = q1.list().iterator(); divisionItr.hasNext();) {
                                        obj = (Object[]) divisionItr.next();
                                        divisionId = obj[0].toString();
                                        divisionShortName = obj[1].toString();
                                        divisionName = obj[3].toString();

                                        if (divisionCenterIdSearch.equals(divisionId)) {
                                            divisionCenterSelected = "selected";
                                        } else {
                                            divisionCenterSelected = "";
                                        }
                                        divisionOptionCon = divisionOptionCon + "<option value=\"" + divisionId + "\" " + divisionCenterSelected + ">" + divisionName + "</option>";
                                    }

                                    divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"custom-select\" required>"
                                            + "<option>Select Division </option>"
                                            + "" + divisionOptionCon + ""
                                            + "</select>";

                                }
                                if (committeeTypeSearch.equals("3")) {
                                    q1 = dbsessionCommittee.createSQLQuery("select * FROM center WHERE center_type_id = '3' ORDER BY center_name ASC");

                                    for (Iterator divisionItr = q1.list().iterator(); divisionItr.hasNext();) {
                                        obj = (Object[]) divisionItr.next();
                                        divisionId = obj[0].toString();
                                        divisionName = obj[1].toString();

                                        if (divisionCenterIdSearch.equals(divisionId)) {
                                            divisionCenterSelected = "selected";
                                        } else {
                                            divisionCenterSelected = "";
                                        }

                                        divisionOptionCon = divisionOptionCon + "<option value=\"" + divisionId + "\" " + divisionCenterSelected + ">" + divisionName + "</option>";
                                    }

                                    divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"custom-select\" required>"
                                            + "<option>Select Centre </option>"
                                            + "" + divisionOptionCon + ""
                                            + "</select>";
                                }

                                if (committeeTypeSearch.equals("4")) {
                                    q1 = dbsessionCommittee.createSQLQuery("select * FROM center WHERE center_type_id = '4' ORDER BY center_name ASC");

                                    for (Iterator divisionItr = q1.list().iterator(); divisionItr.hasNext();) {
                                        obj = (Object[]) divisionItr.next();
                                        divisionId = obj[0].toString();
                                        divisionName = obj[1].toString();

                                        if (divisionCenterIdSearch.equals(divisionId)) {
                                            divisionCenterSelected = "selected";
                                        } else {
                                            divisionCenterSelected = "";
                                        }

                                        divisionOptionCon = divisionOptionCon + "<option value=\"" + divisionId + "\" " + divisionCenterSelected + ">" + divisionName + "</option>";
                                    }

                                    divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"custom-select\" required>"
                                            + "<option>Select Sub Centre </option>"
                                            + "" + divisionOptionCon + ""
                                            + "</select>";
                                }

                                if (committeeTypeSearch.equals("5")) {
                                    divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"form-control input-sm\" required>"
                                            + "<option value=\"HQ\">Head Quarter</option>"
                                            + "</select>";
                                }

                                if (committeeTypeSearch.equals("6")) {
                                    divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"form-control input-sm\" required>"
                                            + "<option value=\"HQ\">Head Quarter</option>"
                                            + "</select>";
                                }
                                if (committeeTypeSearch.equals("7")) {
                                    divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"form-control input-sm\" required>"
                                            + "<option value=\"HQ\">Head Quarter</option>"
                                            + "</select>";
                                }

                                if (committeeTypeSearch.equals("8")) {
                                    divisionInfoCon = "<select  name=\"divisionCenterIdSearch\" id=\"divisionCenterIdSearch\" class=\"form-control input-sm\" required>"
                                            + "<option value=\"HQ\">Head Quarter</option>"
                                            + "</select>";
                                }

                            %>
                            <div class="col-md-3 offset-md-2" id="divisionCenterCon">
                                <!--                                <select  name="divisionCenterIdSearch" id="divisionCenterIdSearch" class="custom-select">
                                                                    <option value="">Select </option>
                                                                </select>-->
                                <%=divisionInfoCon%>
                            </div>
                            <div class="col-md-3">  
                                <%                                    String committeeSessionSelected_1819 = "";
                                    String committeeSessionSelected_1617 = "";
                                    String committeeSessionSelected_1415 = "";
                                    String committeeSessionSelected_1213 = "";
                                    String committeeSessionSelected_1011 = "";

                                    if (committeeSessionSearch.equals("2018-2019")) {
                                        committeeSessionSelected_1819 = "selected";
                                    } else {
                                        committeeSessionSelected_1819 = "";
                                    }
                                    if (committeeSessionSearch.equals("2016-2017")) {
                                        committeeSessionSelected_1617 = "selected";
                                    } else {
                                        committeeSessionSelected_1617 = "";
                                    }
                                    if (committeeSessionSearch.equals("2014-2015")) {
                                        committeeSessionSelected_1415 = "selected";
                                    } else {
                                        committeeSessionSelected_1415 = "";
                                    }
                                    if (committeeSessionSearch.equals("2012-2013")) {
                                        committeeSessionSelected_1213 = "selected";
                                    } else {
                                        committeeSessionSelected_1213 = "";
                                    }
                                    if (committeeSessionSearch.equals("2010-2011")) {
                                        committeeSessionSelected_1011 = "selected";
                                    } else {
                                        committeeSessionSelected_1011 = "";
                                    }

                                %>
                                <select name="committeeSessionSearch" id="committeeSessionSearch" class="custom-select">
                                    <option selected>Session</option>                                    
                                    <option value="2018-2019" <%=committeeSessionSelected_1819%>>2018-2019</option>
                                    <option value="2016-2017" <%=committeeSessionSelected_1617%>>2016-2017</option>
                                    <option value="2014-2015" <%=committeeSessionSelected_1415%>>2014-2015</option>
                                    <option value="2012-2013" <%=committeeSessionSelected_1213%>>2012-2013</option>
                                    <option value="2010-2011" <%=committeeSessionSelected_1011%>>2010-2011</option>
                                </select>                            
                            </div>
                            <div class="col-md-1">
                                <button type="submit" name="committeeSearchSubmit" id="committeeSubmit" class="btn btn-success btn-inverse">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<%    Query cmmtSearchMemSQL = dbsessionCommittee.createSQLQuery("SELECT "
            + "cm.id_committee_member,m.member_id,m.member_name , "
            + "cd.designation_name, cdp.department_name, cm.leader,m.id,  "
            + "m.picture_name  "
            + "FROM  committee_member cm  "
            + "LEFT JOIN committee c ON c.id_committee = cm.id_committtee   "
            + "LEFT JOIN member m ON m.id = cm.member_id   "
            + "LEFT JOIN committee_designation  cd ON cd.id = cm.committee_designation  "
            + "LEFT JOIN committee_department  cdp ON cdp.id = cm.committee_department "
            + "" + searchString + " "
            + "ORDER BY cm.id_committee_member ASC");

//+ "ORDER BY cm.committee_showing_order ASC");
    System.out.println("SearchSQL::" + cmmtSearchMemSQL);

//out.println("SELECT "
//           + "cm.id_committee_member,m.member_id,m.member_name , "
//           + "cd.designation_name, cdp.department_name, cm.leader,m.id,  "
//           + "m.picture_name  "
//           + "FROM  committee_member cm  "
//           + "LEFT JOIN committee c ON c.id_committee = cm.id_committtee   "
//           + "LEFT JOIN member m ON m.id = cm.member_id   "
//           + "LEFT JOIN committee_designation  cd ON cd.id = cm.committee_designation  "
//           + "LEFT JOIN committee_department  cdp ON cdp.id = cm.committee_department "
//           + "" + searchString + " "
//           + "ORDER BY cm.id_committee_member ASC");

%>

<!-- Committee list  starts -->
<section class="committee">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <%                    if (!cmmtSearchMemSQL.list().isEmpty()) { %>


                <%
                    Object cmmitteeNameObj[] = null;
                    String cmmitteeName = "";
                    Query cmmitteeNameSQL = dbsessionCommittee.createSQLQuery("SELECT * FROM committee c WHERE c.com_cat_id = '" + committeeTypeSearch + "' AND c.committee_type = '" + divisionCenterIdSearch + "' AND c.committee_duration = '" + committeeSessionSearch + "'");
                    for (Iterator cmmitteeNameItr = cmmitteeNameSQL.list().iterator(); cmmitteeNameItr.hasNext();) {

                        cmmitteeNameObj = (Object[]) cmmitteeNameItr.next();
                        cmmitteeName = cmmitteeNameObj[2].toString().trim();
                    }
                %>
                <h2 class="display-41 text-center" style="margin: 20px 0;"><%=cmmitteeName%></h2>

                <div class="row my-3">


                    <%
                        for (Iterator itCM = cmmtSearchMemSQL.list().iterator(); itCM.hasNext();) {

                            objCmmtMem = (Object[]) itCM.next();
                            idCommitteeMember = objCmmtMem[0].toString().trim();
                            committeeMemberIEBId = objCmmtMem[1].toString().trim();
                            committeeMemberName = objCmmtMem[2].toString().trim();
                            committeeDesignationName = objCmmtMem[3].toString().trim();
                            committeeDepartmentName = objCmmtMem[4] == null ? "" : objCmmtMem[4].toString().trim();

                            if (!committeeDepartmentName.equals("")) {
                                committeeDepartmentName1 = "( " + committeeDepartmentName + " )";
                            } else {
                                committeeDepartmentName1 = "&nbsp;<br>&nbsp;";
                            }

                            commleader = objCmmtMem[5] == null ? "" : objCmmtMem[5].toString().trim();
                            if (commleader.equals("1")) {
                                classLeader = "leader";
                            } else {
                                classLeader = "";
                            }

                            committeeMemberId = objCmmtMem[6] == null ? "" : objCmmtMem[6].toString().trim();

                            System.out.println(committeeDesignationName + " ::" + committeeMemberId);
                            //    profilePicture = "M_38661.jpg";  
                            profilePicture = objCmmtMem[7] == null ? "" : objCmmtMem[7].toString().trim();
                            profilePictureUrl = GlobalVariable.imageMemberDirLink + profilePicture;

                            commArgXn = "'" + session.getId() + "','" + idCommitteeMember + "'";
                            
                            memberProfileUrl = GlobalVariable.baseUrl + "/member/search.jsp?membershipId=" + committeeMemberIEBId;

                            btnCommitteeMemberEdit = "<a title=\"Edit\" href=\"" + GlobalVariable.baseUrl + "/committeeManagement/committeeMemberUpdate.php?sessionid=" + session.getId() + "&idCommitteeMember=" + idCommitteeMember + "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil-square-o\"></i></a>";
                            btnCommitteeMemberDelete = "<a onClick=\"deleteCommitteeMemberInfo(" + commArgXn + ");\" title=\"Delete comittee member\"  class=\"btn btn-primary\"><i class=\"fa fa-trash-o\"></i></a>";

//                            committeeMemberInfoCon = committeeMemberInfoCon + "<div class=\"executive_single_item " + classLeader + "\">"
//                                    + "<div class=\"executive_slider_item\">"
//                                    + "<div class=\"executive_inner\">"
//                                    + "<div class=\"executive_img\">"
//                                    + "<img src=\"" + profilePictureUrl + "\" alt=\"" + committeeMemberName + "\">"
//                                    + "</div>"
//                                    + "<div class=\"hover_content\">"
//                                    + "<h3 class=\"member_name\">" + committeeMemberName + "</h3>"
//                                    + "<p class=\"member_title\">" + committeeDesignationName + "</p>"
//                                    + "<p class=\"ticket\">" + committeeMemberIEBId + "</p>"
//                                    + "<p class=\"catagory\">" + committeeDepartmentName1 + "</p>"
//                                    + "<ul class=\"follow_link\">"
//                                    + "<li><a href=\"#\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/fa-icon.png\" alt=\"img\"></a></li>"
//                                    + "<li><a href=\"#\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/in-icon.png\" alt=\"img\"></a></li>"
//                                    + "<li><a href=\"#\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/wifi-icon.png\" alt=\"img\"></a></li>"
//                                    + "</ul>"
//                                    + "</div>"
//                                    + "</div>"
//                                    + "</div>"
//                                    + "</div>";
//
//                            if ((cm % 5) == 0) {
//                                committeeMemberInfoCon = committeeMemberInfoCon + "</div><div class=\"row\">";
//                            }
                            committeeMemberInfoCon = committeeMemberInfoCon + "<div class=\"col-md-4 col-lg-4 col-sm-6 col-xs-12 mb-3\" style=\"min-height:260px;\">"
                                    + "<div class=\"card\" style=\"width:100%; background-color:#fff; border:none;\"><div class=\"card mx-auto d-flex flex-row align-items-stretch comittee-card-partho\" style=\"height:150px;max-width:10rem; background-color:#fff;\">"
                                    + "<img src=\"" + profilePictureUrl + "\" class=\"d-lg-block card-img-top bg-transparent\" style=\"background-size:cover;\" alt=\"" + committeeMemberName + "\">"
                                    + "</div>"
                                    + "</div>"
                                    + "<a target=\"_blank\" href=\"" + memberProfileUrl + "\">"
                                    + "<div class=\"card-body text-center pl-2\"  style=\"height:200px;min-width:7rem; overflow-y:auto; background-color:#fff;\">"
                                    + "<h4 class=\"card-title h6 mb-3\">" + committeeMemberName + "</h4>"
                                    + "<p class=\"card-text\">" + committeeMemberIEBId + "</p>"
                                    + "<p class=\"card-text\">" + committeeDesignationName + "</p>"
                                    + "<p class=\"card-text\">" + committeeDepartmentName1 + "</p>"
                                    + "</div>"
                                    + "</a>"
                                    + "<div class=\"card-footer social-icon_partho\" style=\"background-color:#fff; text-align:center\">"
                                    + "<i class=\"fab fa-facebook-f mr-3\"></i>"
                                    + "<i class=\"fab fa-twitter mr-3\"></i>"
                                    + "<i class=\"fab fa-linkedin-in mr-3\"></i>"
                                    + "</div>"
                                    + "</div>";

                            if ((cm % 3) == 0) {
                                committeeMemberInfoCon = committeeMemberInfoCon + "</div><div class=\"row my-3\">";
                            }

                            cm++;
                        }

                    %>


                    <%=committeeMemberInfoCon%>

                </div>

                <% } else { %> 

                <% if (!divisionCenterIdSearch.equals("HQ")) {%>
                <h1 class="text-center">No Committee Info Found</h1>
                <% } %>

                <% } %>
            </div>



            <%
//                System.out.println("LLL:" + request.getRequestURL());
//
//                String getURIStrCommittee = request.getRequestURI();
//                System.out.println("getURL:::" + getURIStrCommittee);
//
//                String[] arrOfStrCommittee = getURIStrCommittee.split("/");
//
//                String currentPageOptCommittee = "";
//                for (String curlCommittee : arrOfStrCommittee) {
//                    System.out.println(curlCommittee);
//
//                    currentPageOptCommittee = curlCommittee;
//                }
            %>

            <% if (!divisionCenterIdSearch.equals("HQ")) {%>
            <div class="col-md-3 d-none d-xl-block">
                <div class="card my-3">
                    <div class="card-header text-warning h5 font-weight-bold">
                        Sub Center Committee 
                    </div>
                    <ul class="list-group list-group-flush">

                        <%
                            Object divisionObjRight[] = null;

                            String divisionIdRight = "";
                            String divisionShortNameRight = "";
                            String divisionNameRight = "";
                            String divisionOptionConRight = "";
                            String divisionInfoConRight = "";
                            String committeeDetailsUrlRight = "";

                            Query divisionRightSQL = dbsessionCommittee.createSQLQuery("select * FROM center WHERE center_type_id = '4' ORDER BY center_name ASC");

                            for (Iterator divisionRightItr = divisionRightSQL.list().iterator(); divisionRightItr.hasNext();) {
                                divisionObjRight = (Object[]) divisionRightItr.next();
                                divisionIdRight = divisionObjRight[0].toString();
                                divisionShortNameRight = divisionObjRight[1].toString();

                                committeeDetailsUrlRight = GlobalVariable.baseUrl + "/committee/subCenterCommittee.jsp?committeeTypeSearch=4&divisionCenterIdSearch=" + divisionIdRight + "&committeeSessionSearch=2018-2019";

                        %>
                        <li class="list-group-item text-wrap" ><a class="lead" href="<%=committeeDetailsUrlRight%>" ><%=divisionShortNameRight%> Center</a></li>
                            <%                                    }

                            %>

                    </ul>
                </div>


            </div>
            <% }%>
        </div>
    </div>

</section>
<!-- Committee list  ends -->

<%     dbsessionCommittee.clear();
    dbsessionCommittee.close();
%>

<%@ include file="../footer.jsp" %>
