<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<!-- Login & Register & About CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about.css" rel="stylesheet">




<!-- Centre List Starts -->
<section id="centre_list">
    <div id="bg-img-ieb_mas" class="py-5 mb-4">
        <h2 style="color:#fff" class="text-center py-3 mx-auto w-50 font-weight-bold text-light"> IEB DIVISION COMMITTEE</h2>
    </div>
    <div class="container">

        <div class="row text-center">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <a href="#dhaka" class="fas fa-angle-double-right btn btn-primary d-inline-block mr-5"> Agriculture Engineering Division</a></li>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <a href="#khulna" class="fas fa-angle-double-right btn btn-secondary d-inline-block"> Chemical Engineering Division</a></li>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <a href="#rajshahi" class="fas fa-angle-double-right btn btn-success d-inline-block"> Civil Engineering Division</a></li>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <a href="#comilla" class="fas fa-angle-double-right btn btn-info d-inline-block"> Computer Science Engineering Division</a></li>
                    </div>

                </div>
            </div>	
            <div class="col-md-12 mt-4">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <a href="#sylhet" class="fas fa-angle-double-right btn btn-primary d-inline-block"> Electrical Engineering Division</a></li>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <a href="#barisal" class="fas fa-angle-double-right btn btn-secondary d-inline-block"> Mechanical Engineering Division</a></li>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <a href="#barisal" class="fas fa-angle-double-right btn btn-success d-inline-block"> Textile Engineering Division</a></li>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- Centre List Ends -->



<%@ include file="../footer.jsp" %>
