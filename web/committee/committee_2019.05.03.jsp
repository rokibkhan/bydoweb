<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Query"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Session"%>

<%@page import="com.appul.util.GlobalVariable"%>


<%@ include file="../header.jsp" %>

<%
    Session dbsessionCommittee = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxCommittee = dbsessionCommittee.beginTransaction();

    Query queryCommittee = null;
    Object committeeObj[] = null;

    Object objCmmtMem[] = null;
    String idCommitteeMember = "";
    String committeeMemberId = "";
    String committeeMemberIEBId = "";
    String committeeMemberName = "";
    String committeeDesignationName = "";
    String committeeDepartmentName = "";
    String committeeDepartmentName1 = "";
    String profilePicture = "";
    String profilePictureUrl = "";
    String btnCommitteeMemberEdit = "";
    String btnCommitteeMemberDelete = "";
    String commleader = "";
    String classLeader = "";
    String commArgXn = "";
    String committeeMemberInfoCon = "";
    int cm = 1;

%>

<section>

    <nav aria-label="breadcrumb"> 
        <div class="container">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Committee</a></li>
                <li class="breadcrumb-item active" aria-current="page">Committee Details</li>
            </ol>
        </div>
    </nav>
</section>

<!-- News & Events & Single Event CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/news_events_style.css" rel="stylesheet">
<!-- Font Awesome CSS -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

<!-- Event front image section -->
<!--<section class="news-banner">
    <div class="conainer">
        <div class="card bg-dark text-white">
            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_large_img_2.png" class="card-img img-fluid" alt="Banner Image">
            <div class="card-img-overlay img-fluid">
                <div class="event_date">
                    <span class="p_date bg_yellow  p-5">
                        <span class="day" style="font: 700 62px/45px 'product_sansbold';">13</span>
                        <span class="mon"  style="font: 400 40px/43px 'product_sansregular';">Oct</span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- Event front image section -->

<!-- Event details section starts -->
<section class="executive_wrap">
    <div class="container">
        <h2 class="display-41 text-center" style="margin: 20px 0;">Executive-Committee 2018-2019</h2>
        <div class="row">

            <%                Query cmmtMemSQL = dbsessionCommittee.createSQLQuery("SELECT "
                        + "cm.id_committee_member,m.member_id,m.member_name , "
                        + "cd.designation_name, cdp.department_name, cm.leader,m.id  "
                        + "FROM  committee_member cm  "
                        + "LEFT JOIN committee c ON c.id_committee = cm.id_committtee   "
                        + "LEFT JOIN member m ON m.id = cm.member_id   "
                        + "LEFT JOIN committee_designation  cd ON cd.id = cm.committee_designation  "
                        + "LEFT JOIN committee_department  cdp ON cdp.id = cm.committee_department "
                        + "WHERE m.status = '1' "
                        + "ORDER BY cm.committee_showing_order ASC");

                if (!cmmtMemSQL.list().isEmpty()) {
                    for (Iterator itCM = cmmtMemSQL.list().iterator(); itCM.hasNext();) {

                        objCmmtMem = (Object[]) itCM.next();
                        idCommitteeMember = objCmmtMem[0].toString().trim();
                        committeeMemberIEBId = objCmmtMem[1].toString().trim();
                        committeeMemberName = objCmmtMem[2].toString().trim();
                        committeeDesignationName = objCmmtMem[3].toString().trim();
                        committeeDepartmentName = objCmmtMem[4] == null ? "" : objCmmtMem[4].toString().trim();

                        if (!committeeDepartmentName.equals("")) {
                            committeeDepartmentName1 = "( " + committeeDepartmentName + " )";
                        } else {
                            committeeDepartmentName1 = "&nbsp;<br>&nbsp;";
                        }

                        commleader = objCmmtMem[5] == null ? "" : objCmmtMem[5].toString().trim();
                        if (commleader.equals("1")) {
                            classLeader = "leader";
                        } else {
                            classLeader = "";
                        }

                        committeeMemberId = objCmmtMem[6] == null ? "" : objCmmtMem[6].toString().trim();

                        System.out.println(committeeDesignationName + " ::" + committeeMemberId);
                        //    profilePicture = "M_38661.jpg";  
                        profilePicture = "no_image.jpg";
                        profilePictureUrl = GlobalVariable.imageMemberDirLink + committeeMemberId + ".jpg";

                        commArgXn = "'" + session.getId() + "','" + idCommitteeMember + "'";

                        btnCommitteeMemberEdit = "<a title=\"Edit\" href=\"" + GlobalVariable.baseUrl + "/committeeManagement/committeeMemberUpdate.php?sessionid=" + session.getId() + "&idCommitteeMember=" + idCommitteeMember + "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil-square-o\"></i></a>";
                        btnCommitteeMemberDelete = "<a onClick=\"deleteCommitteeMemberInfo(" + commArgXn + ");\" title=\"Delete comittee member\"  class=\"btn btn-primary\"><i class=\"fa fa-trash-o\"></i></a>";

                        committeeMemberInfoCon = committeeMemberInfoCon + "<div class=\"executive_single_item " + classLeader + "\">"
                                + "<div class=\"executive_slider_item\">"
                                + "<div class=\"executive_inner\">"
                                + "<div class=\"executive_img\">"
                                + "<img src=\"" + profilePictureUrl + "\" alt=\"" + committeeMemberName + "\">"
                                + "</div>"
                                + "<div class=\"hover_content\">"
                                + "<h3 class=\"member_name\">" + committeeMemberName + "</h3>"
                                + "<p class=\"member_title\">" + committeeDesignationName + "</p>"
                                + "<p class=\"ticket\">" + committeeMemberIEBId + "</p>"
                                + "<p class=\"catagory\">" + committeeDepartmentName1 + "</p>"
                                + "<ul class=\"follow_link\">"
                                + "<li><a href=\"#\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/fa-icon.png\" alt=\"img\"></a></li>"
                                + "<li><a href=\"#\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/in-icon.png\" alt=\"img\"></a></li>"
                                + "<li><a href=\"#\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/wifi-icon.png\" alt=\"img\"></a></li>"
                                + "</ul>"
                                + "</div>"
                                + "</div>"
                                + "</div>"
                                + "</div>";

                        if ((cm % 5) == 0) {
                            committeeMemberInfoCon = committeeMemberInfoCon + "</div><div class=\"row\">";
                        }

                        cm++;
                    }
                }

            %>




            <%=committeeMemberInfoCon%>



        </div>
    </div>
</section>
<!-- Event details section starts -->

<!-- Start of More Events Section -->

<!-- End of More Events Section -->
<%     dbsessionCommittee.clear();
    dbsessionCommittee.close();
    %>

<%@ include file="../footer.jsp" %>
