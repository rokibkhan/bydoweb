<%@page import="org.hibernate.Session"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.appul.util.GlobalVariable"%>

<%
    Session dbsessionCommittee = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxCommittee = dbsessionCommittee.beginTransaction();

    Query queryCommittee = null;
    Object committeeObj[] = null;

    Object objCmmtMem[] = null;
    String idCommitteeMember = "";
    String committeeMemberId = "";
    String committeeMemberIEBId = "";
    String committeeMemberName = "";
    String committeeDesignationName = "";
    String committeeDepartmentName = "";
    String committeeDepartmentName1 = "";
    String profilePicture = "";
    String profilePictureUrl = "";
    String btnCommitteeMemberEdit = "";
    String btnCommitteeMemberDelete = "";
    String commleader = "";
    String classLeader = "";
    String commArgXn = "";
    String committeeMemberInfoCon = "";
    int cm = 1;

%>


<!-- start executive_wrap -->
<section class="executive_wrap">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <h3 class="sec_title">Executive-Committee 2018-2019</h3>
            </div>
        </div>

        <div class="row">

            <%                Query cmmtMemSQL = dbsessionCommittee.createSQLQuery("SELECT "
                        + "cm.id_committee_member,m.member_id,m.member_name , "
                        + "cd.designation_name, cdp.department_name, cm.leader,m.id  "
                        + "FROM  committee_member cm  "
                        + "LEFT JOIN committee c ON c.id_committee = cm.id_committtee   "
                        + "LEFT JOIN member m ON m.id = cm.member_id   "
                        + "LEFT JOIN committee_designation  cd ON cd.id = cm.committee_designation  "
                        + "LEFT JOIN committee_department  cdp ON cdp.id = cm.committee_department "
                        + "WHERE m.status = '1' "
                        + "ORDER BY cm.committee_showing_order ASC");

                if (!cmmtMemSQL.list().isEmpty()) {
                    for (Iterator itCM = cmmtMemSQL.list().iterator(); itCM.hasNext();) {

                        objCmmtMem = (Object[]) itCM.next();
                        idCommitteeMember = objCmmtMem[0].toString().trim();
                        committeeMemberIEBId = objCmmtMem[1].toString().trim();
                        committeeMemberName = objCmmtMem[2].toString().trim();
                        committeeDesignationName = objCmmtMem[3].toString().trim();
                        committeeDepartmentName = objCmmtMem[4] == null ? "" : objCmmtMem[4].toString().trim();

                        if (!committeeDepartmentName.equals("")) {
                            committeeDepartmentName1 = "( " + committeeDepartmentName + " )";
                        } else {
                            committeeDepartmentName1 = "&nbsp;<br>&nbsp;";
                        }

                        commleader = objCmmtMem[5] == null ? "" : objCmmtMem[5].toString().trim();
                        if (commleader.equals("1")) {
                            classLeader = "leader";
                        } else {
                            classLeader = "";
                        }

                        committeeMemberId = objCmmtMem[6] == null ? "" : objCmmtMem[6].toString().trim();

                        System.out.println(committeeDesignationName + " ::" + committeeMemberId);
                        //    profilePicture = "M_38661.jpg";  
                        profilePicture = "no_image.jpg";
                        profilePictureUrl = GlobalVariable.imageMemberDirLink + committeeMemberId + ".jpg";

                        commArgXn = "'" + session.getId() + "','" + idCommitteeMember + "'";

                        btnCommitteeMemberEdit = "<a title=\"Edit\" href=\"" + GlobalVariable.baseUrl + "/committeeManagement/committeeMemberUpdate.php?sessionid=" + session.getId() + "&idCommitteeMember=" + idCommitteeMember + "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil-square-o\"></i></a>";
                        btnCommitteeMemberDelete = "<a onClick=\"deleteCommitteeMemberInfo(" + commArgXn + ");\" title=\"Delete comittee member\"  class=\"btn btn-primary\"><i class=\"fa fa-trash-o\"></i></a>";

                        committeeMemberInfoCon = committeeMemberInfoCon + "<div class=\"executive_single_item " + classLeader + "\">"
                                + "<div class=\"executive_slider_item\">"
                                + "<div class=\"executive_inner\">"
                                + "<div class=\"executive_img\">"
                                + "<img src=\"" + profilePictureUrl + "\" alt=\"" + committeeMemberName + "\">"
                                + "</div>"
                                + "<div class=\"hover_content\">"
                                + "<h3 class=\"member_name\">" + committeeMemberName + "</h3>"
                                + "<p class=\"member_title\">" + committeeDesignationName + "</p>"
                                + "<p class=\"ticket\">" + committeeMemberIEBId + "</p>"
                                + "<p class=\"catagory\">" + committeeDepartmentName1 + "</p>"
                                + "<ul class=\"follow_link\">"
                                + "<li><a href=\"#\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/fa-icon.png\" alt=\"img\"></a></li>"
                                + "<li><a href=\"#\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/in-icon.png\" alt=\"img\"></a></li>"
                                + "<li><a href=\"#\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/wifi-icon.png\" alt=\"img\"></a></li>"
                                + "</ul>"
                                + "</div>"
                                + "</div>"
                                + "</div>"
                                + "</div>";

                        if ((cm % 5) == 0) {
                            committeeMemberInfoCon = committeeMemberInfoCon + "</div><div class=\"row\">";
                        }

                        cm++;
                    }
                }

            %>




            <%=committeeMemberInfoCon%>



        </div>
</section>
<!-- end executive_wrap -->


<%

    dbsessionCommittee.clear();
    dbsessionCommittee.close();
%>