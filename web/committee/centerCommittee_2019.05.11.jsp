<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<!-- Login & Register & About CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about.css" rel="stylesheet">




<!-- Centre List Starts -->
<section id="centre_list">
    <div id="bg-img-ieb_mas" class="py-5 mb-4">
        <h2 style="color:#fff" class="text-center py-3 mx-auto w-50 font-weight-bold text-light"> IEB CENTRE COMMITTEE</h2>
    </div>
    <div class="container">

        <div class="row text-center">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <a href="#dhaka" class="fas fa-angle-double-right btn btn-primary d-inline-block mr-5"> Agriculture Engineering Division</a></li>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <a href="#khulna" class="fas fa-angle-double-right btn btn-secondary d-inline-block"> Chemical Engineering Division</a></li>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <a href="#rajshahi" class="fas fa-angle-double-right btn btn-success d-inline-block"> Civil Engineering Division</a></li>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <a href="#comilla" class="fas fa-angle-double-right btn btn-info d-inline-block"> Computer Science Engineering Division</a></li>
                    </div>

                </div>
            </div>	
            <div class="col-md-12 mt-4">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <a href="#sylhet" class="fas fa-angle-double-right btn btn-primary d-inline-block"> Electrical Engineering Division</a></li>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <a href="#barisal" class="fas fa-angle-double-right btn btn-secondary d-inline-block"> Mechanical Engineering Division</a></li>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <a href="#barisal" class="fas fa-angle-double-right btn btn-success d-inline-block"> Textile Engineering Division</a></li>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- Centre List Ends -->

<!-- Agriculture Engineering Division -->
<section id="dhaka" class="mt-4 text-center each_centre each_centre_mas">
    <div class="bg-danger info-header mb-5">
        <h1 class="text-light py-3">
            Agriculture Engineering Division
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card mb-4">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04037.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. WALIULLAH SIKDER</h5>
                        <p> F/04037</p>
                        <p class="font-weight-bold">CHAIRMAN</p>

                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-07893.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. MUSLIM UDDIN</h5>
                        <p>F/07893</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-07414.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. MOZAMMEL HAQUE</h5>
                        <p>F/07414</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-05333.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. SHAHADAT HOSSAIN (SHIBLU), PEng.</h5>
                        <p>F/05333</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Dhaka Centre Ends -->

<!-- Chemical Engineering Division Starts -->
<section id="chittagong" class="text-center each_centre each_centre_mas">
    <div class="bg-info info-header mb-5">
        <h1 class="text-light py-3">
            Chemical Engineering Division
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">
        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04198.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>PROF. DR. ENGR. MD. RAFIQUL ALAM</h5>
                        <p> F/04198</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-05028.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. PRABIR KUMAR SEN</h5>
                        <p>F/05028</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-03939.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. PRABIR KUMAR DEY</h5>
                        <p>F/03939</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-05580.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. RAFIQUL ISLAM MANIK</h5>
                        <p>F/05580</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Chittagong Centre Ends -->

<!-- Civil Engineering Division -->
<section id="khulna" class="text-center each_centre each_centre_mas">
    <div class="bg-danger info-header mb-5">
        <h1 class="text-light py-3">
            Civil Engineering Division
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">
        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04259.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. SHAFIQUE UDDIN</h5>
                        <p> F/04259</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-10830.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. DILU ARA</h5>
                        <p>F/10830</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-02671.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. KAMALUDDIN AHMED</h5>
                        <p>F/02671</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-11148.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>DR. ENGR. SOBAHAN MIA</h5>
                        <p>F/11148</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Civil Engineering Division -->

<!-- Computer Science Engineering Division -->
<section id="rajshahi" class="text-center each_centre each_centre_mas">
    <div class="bg-info info-header mb-5">
        <h1 class="text-light py-3">
            Computer Science Engineering Division
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">

                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-02725.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. LUTFUR RAHMAN</h5>
                        <p> F/02725</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/m.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5></h5>
                        <p>F/0000</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-11105.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. NIZAMUL HAQUE SARKER</h5>
                        <p>F/11105.jpg</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/M-24325.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. TAREQ MOSARRAF</h5>
                        <p>M/24325</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Computer Science Engineering Division -->

<!-- Electrical Engineering Division -->
<section id="comilla" class="text-center each_centre each_centre_mas">
    <div class="bg-danger info-header mb-5">
        <h1 class="text-light py-3">
            Electrical Engineering Division
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04633.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. ABUL BASHAR</h5>
                        <p> F/04633</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-12744.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MIRZA MOHAMMAD HAFIZ</h5>
                        <p>F/12744</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-02789.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MILAN CHANDRA MAJUMDER</h5>
                        <p>F/02789</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-10327.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. RAHMAT ULLAH KABIR</h5>
                        <p>F/10327</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Electrical Engineering Division -->

<!-- Mechanical Engineering Division -->
<section id="sylhet" class="text-center each_centre each_centre_mas">
    <div class="bg-info info-header mb-5">
        <h1 class="text-light py-3">
            Mechanical Engineering Division
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04551.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. SHOAIB AHMED MATIN</h5>
                        <p> F/04551</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-11383.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. HARONUR RASHID MULLAH</h5>
                        <p>F/11383</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-10034.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. A. S. M.  MOHSIN</h5>
                        <p>F/10034</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-05333.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. SHAHADAT HOSSAIN (SHIBLU), PEng.</h5>
                        <p>F/05333</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Mechanical Engineering Division -->


<!-- Textile Engineering Division -->
<section id="barisal" class="text-center each_centre each_centre_mas">
    <div class="bg-danger info-header mb-5">
        <h1 class="text-light py-3">
            Textile Engineering Division
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">
        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/m.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5></h5>
                        <p> F/0000</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/m.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5></h5>
                        <p>F/0000</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/m.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5></h5>
                        <p>F/0000</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/m.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>Md. Nafees Imtiaz</h5>
                        <p>F/0000</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Textile Engineering Division -->


<%@ include file="../footer.jsp" %>
