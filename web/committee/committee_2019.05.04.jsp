<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Query"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Session"%>

<%@page import="com.appul.util.GlobalVariable"%>


<%@ include file="../header.jsp" %>


<!-- News & Events & Single Event CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/photo_contact_success_style.css" rel="stylesheet">


<%
    Session dbsessionCommittee = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxCommittee = dbsessionCommittee.beginTransaction();

    Query queryCommittee = null;
    Object committeeObj[] = null;

    Object objCmmtMem[] = null;
    String idCommitteeMember = "";
    String committeeMemberId = "";
    String committeeMemberIEBId = "";
    String committeeMemberName = "";
    String committeeDesignationName = "";
    String committeeDepartmentName = "";
    String committeeDepartmentName1 = "";
    String profilePicture = "";
    String profilePictureUrl = "";
    String btnCommitteeMemberEdit = "";
    String btnCommitteeMemberDelete = "";
    String commleader = "";
    String classLeader = "";
    String commArgXn = "";
    String committeeMemberInfoCon = "";
    int cm = 1;

%>

<script type="text/javascript">

    function showDivisonCenterInfo() {
        alert("OK");
    }
</script>

<!-- Start of navigation -->
<section class="navigation_partho parallaxie text-center" style="height:150px;">
    <h3 class="h1 pl-5 mx-5 font-weight-bold text-white pt-5">Committee</h3>
    <nav aria-label="breadcrumb" class="w-100 pl-5">
        <ol class="breadcrumb h3" style="background-color: #e9ecef00" >
            <li class="breadcrumb-item"><a href="home_v4.html">Home</a></li>
            <li class="breadcrumb-item active text-white" aria-current="page" >Committee</li>
        </ol>
    </nav>
</section>
<!-- End of navigation -->



<!-- Comittee search bar starts -->
<section id="comittee_search_bar_partho">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="banner-div_partho text-center mx-auto w-75 py-3 font-weight-bold text-dark align-items-center pt-5">
                    <div class="row">
                        <div class="col">
                            <select  name="committeeType" id="committeeType" class="custom-select" onchange="showDivisonCenterInfo()">
                                <option selected>Select Committee Type...</option>
                                <option value="1">Central Executive Committee</option>
                                <option value="1">Division Committee</option>
                                <option value="3">Centre  Committee</option>
                                <option value="3">Sub-Center  Committee</option>
                                <option value="3">Overseas Chapter  Committee</option>
                                <option value="3">Student Chapter  Committee</option>
                                <option value="3">Mohila Committee</option>
                                <option value="3">Council Members Committee</option>
                            </select>
                        </div>
                        <div class="col" id="divisionCenterCon">
                            <select  name="divisionCenterId" id="divisionCenterId" class="custom-select">
                                <option selected>Select </option>

                            </select>
                        </div>
                        <div class="col">  
                            <select name="committeeSession" id="committeeSession" class="custom-select">
                                <option selected>Session...</option>
                                <option value="1">2017-18</option>
                                <option value="2">2016-17</option>
                                <option value="3">2015-16</option>
                                <option value="4">2014-15</option>
                                <option value="5">2013-12</option>
                                <option value="#">Previous</option>		
                            </select>


                            <a href="#" class="btn btn-outline-danger btn-lg border-light border-0 ml-2 text-center pl-3"><i class="fas fa-search search-icon_partho"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- Committee list  starts -->
<section class="committee">
    <div class="container">
        <div class="row">
            <div class="col-md-9">

                <h2 class="display-41 text-center" style="margin: 20px 0;">Executive-Committee 2018-2019</h2>

                <div class="row my-3">
                    <div class="col-12" style="min-height:200px;">
                        <div class="card mx-auto d-flex flex-row align-items-stretch comittee-card-partho" style="max-width:10rem;">
                            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_1.jpg" class="card-img-top bg-transparent" style="background-size:cover;" alt="News Image">
                        </div>
                        <div class="card-body text-center"  style="max-height:200px;min-width:7rem; overflow-y:auto">
                            <h4 class="card-title h5 mb-3">Engr. Md. Abdus Sabur</h4>
                            <p class="card-text">F06100</p>
                            <p class="card-text">President</p>
                        </div>
                        <div class="card-body text-center social-icon_partho" style="max-height:200px;min-width:7rem; overflow-y:auto">
                            <i class="fab fa-facebook-f mr-4"></i>
                            <i class="fab fa-twitter mr-4"></i>
                            <i class="fab fa-linkedin-in "></i>
                        </div>


                    </div>

                </div>
                <div class="row my-3">
                    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 mb-3" style="min-height:150px;">
                        <div class="card mx-auto d-flex flex-row align-items-stretch comittee-card-partho" style="height:150px;max-width:10rem;">
                            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_1.jpg" class="d-none d-lg-block card-img-top bg-transparent" style="background-size:cover;" alt="News Image">


                        </div>
                        <div class="card-body text-center pl-2"  style="height:150px;min-width:7rem; overflow-y:auto">
                            <h4 class="card-title h6 mb-3">Engr. Md. Abdus Sabur</h4>
                            <p class="card-text">F06100</p>
                            <p class="card-text">President</p>
                        </div>
                        <div class="card-footer social-icon_partho">
                            <i class="fab fa-facebook-f mr-3"></i>
                            <i class="fab fa-twitter mr-3"></i>
                            <i class="fab fa-linkedin-in mr-3"></i>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 mb-3" style="min-height:150px;">
                        <div class="card mx-auto d-flex flex-row align-items-stretch comittee-card-partho" style="height:150px;max-width:10rem;">
                            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg"  class="d-none d-lg-block card-img-top bg-transparent" style="background-size:cover;" alt="News Image">
                        </div>
                        <div class="card-body bg-light" style="height:150px;min-width:7rem;overflow-y:auto">
                            <h4 class="card-title h6 mb-3">Engr. S. M. Monjurul Haque Monju S. M. Monjurul Haque Monju</h4>
                            <p class="card-text">User ID</p>
                            <p class="card-text">Vice-President</p>
                        </div>
                        <div class="card-footer social-icon_partho">
                            <i class="fab fa-facebook-f mr-3"></i>
                            <i class="fab fa-twitter mr-3"></i>
                            <i class="fab fa-linkedin-in mr-3"></i>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 mb-3 " style="min-height:150px;" >
                        <div class="card mx-auto d-flex flex-row align-items-stretch comittee-card-partho" style="height:150px;max-width:10rem;">
                            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_4.jpg" class="d-none d-lg-block card-img-top bg-transparent" style="background-size:cover;" alt="News Image">

                        </div>
                        <div class="card-body" style="height:150px;min-width:7rem; overflow-y:auto">
                            <h4 class="card-title h6 mb-3">Mr. Name</h4>
                            <p class="card-text">User ID</p>
                            <p class="card-text">Vice-President</p>
                        </div>
                        <div class="card-footer social-icon_partho">
                            <i class="fab fa-facebook-f mr-3"></i>
                            <i class="fab fa-twitter mr-3"></i>
                            <i class="fab fa-linkedin-in mr-3"></i>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 mb-3 " style="min-height:150px;" >
                        <div class="card mx-auto d-flex flex-row align-items-stretch comittee-card-partho" style="height:150px;max-width:10rem;">
                            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_4.jpg" class="d-none d-lg-block card-img-top bg-transparent" style="background-size:cover;" alt="News Image">
                        </div>
                        <div class="card-body" style="height:150px;min-width:7rem; overflow-y:auto">
                            <h4 class="card-title h6 mb-3">Mr. Name</h4>
                            <p class="card-text">User ID</p>
                            <p class="card-text">Vice-President</p>
                        </div>
                        <div class="card-footer social-icon_partho">
                            <i class="fab fa-facebook-f mr-3"></i>
                            <i class="fab fa-twitter mr-3"></i>
                            <i class="fab fa-linkedin-in mr-3"></i>
                        </div>
                    </div>

                </div>
                <div class="row my-3">
                    <div class="col-12" style="min-height:200px;">
                        <div class="card mx-auto d-flex flex-row align-items-stretch comittee-card-partho" style="max-width:10rem;">
                            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_3.jpg" class="card-img-top bg-transparent" style="background-size:cover;" alt="News Image">
                        </div>
                        <div class="card-body text-center"  style="max-height:200px;min-width:7rem; overflow-y:auto">
                            <h4 class="card-title h5 mb-3">Engr. Md. Abdus Sabur</h4>
                            <p class="card-text">F06100</p>
                            <p class="card-text">President</p>
                        </div>
                        <div class="card-body text-center social-icon_partho" style="max-height:200px;min-width:7rem; overflow-y:auto">
                            <i class="fab fa-facebook-f mr-4"></i>
                            <i class="fab fa-twitter mr-4"></i>
                            <i class="fab fa-linkedin-in "></i>
                        </div>

                    </div>

                </div>

                <div class="row my-3">
                    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 mb-3" style="min-height:150px;">
                        <div class="card mx-auto d-flex flex-row align-items-stretch comittee-card-partho" style="height:150px;max-width:10rem;">
                            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_1.jpg" class="d-none d-lg-block card-img-top bg-transparent" style="background-size:cover;" alt="News Image">


                        </div>
                        <div class="card-body text-center pl-2"  style="height:150px;min-width:7rem; overflow-y:auto">
                            <h4 class="card-title h6 mb-3">Engr. Md. Abdus Sabur</h4>
                            <p class="card-text">F06100</p>
                            <p class="card-text">President</p>
                        </div>
                        <div class="card-footer social-icon_partho">
                            <i class="fab fa-facebook-f mr-3"></i>
                            <i class="fab fa-twitter mr-3"></i>
                            <i class="fab fa-linkedin-in mr-3"></i>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 mb-3" style="min-height:150px;">
                        <div class="card mx-auto d-flex flex-row align-items-stretch comittee-card-partho" style="height:150px;max-width:10rem;">
                            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg"  class="d-none d-lg-block card-img-top bg-transparent" style="background-size:cover;" alt="News Image">
                        </div>
                        <div class="card-body bg-light" style="height:150px;min-width:7rem;overflow-y:auto">
                            <h4 class="card-title h6 mb-3">Engr. S. M. Monjurul Haque Monju S. M. Monjurul Haque Monju</h4>
                            <p class="card-text">User ID</p>
                            <p class="card-text">Vice-President</p>
                        </div>
                        <div class="card-footer social-icon_partho">
                            <i class="fab fa-facebook-f mr-3"></i>
                            <i class="fab fa-twitter mr-3"></i>
                            <i class="fab fa-linkedin-in mr-3"></i>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 mb-3 " style="min-height:150px;" >
                        <div class="card mx-auto d-flex flex-row align-items-stretch comittee-card-partho" style="height:150px;max-width:10rem;">
                            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_4.jpg" class="d-none d-lg-block card-img-top bg-transparent" style="background-size:cover;" alt="News Image">

                        </div>
                        <div class="card-body" style="height:150px;min-width:7rem; overflow-y:auto">
                            <h4 class="card-title h6 mb-3">Mr. Name</h4>
                            <p class="card-text">User ID</p>
                            <p class="card-text">Vice-President</p>
                        </div>
                        <div class="card-footer social-icon_partho">
                            <i class="fab fa-facebook-f mr-3"></i>
                            <i class="fab fa-twitter mr-3"></i>
                            <i class="fab fa-linkedin-in mr-3"></i>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 mb-3 " style="min-height:150px;" >
                        <div class="card mx-auto d-flex flex-row align-items-stretch comittee-card-partho" style="height:150px;max-width:10rem;">
                            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_4.jpg" class="d-none d-lg-block card-img-top bg-transparent" style="background-size:cover;" alt="News Image">
                        </div>
                        <div class="card-body" style="height:150px;min-width:7rem; overflow-y:auto">
                            <h4 class="card-title h6 mb-3">Mr. Name</h4>
                            <p class="card-text">User ID</p>
                            <p class="card-text">Vice-President</p>
                        </div>
                        <div class="card-footer social-icon_partho">
                            <i class="fab fa-facebook-f mr-3"></i>
                            <i class="fab fa-twitter mr-3"></i>
                            <i class="fab fa-linkedin-in mr-3"></i>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-3 d-none d-xl-block">
                <div class="card my-3">
                    <div class="card-header text-warning h5 font-weight-bold">
                        Committees <i class="fas fa-angle-double-down float-right" style="color:rgb(255,0,0);cursor:pointer;"></i>
                    </div>
                    <ul class="list-group list-group-flush">
                        <a class="lead" href="#" ><li class="list-group-item text-wrap" >Executive Comittee</li></a>
                        <a class="lead" href="#"><li class="list-group-item">Central Committee</li></a>
                        <a class="lead" href="#"><li class="list-group-item">Divisional Comittee</li></a>
                        <a class="lead" href="#"><li class="list-group-item">Sub-divisional Comittee</li></a>
                    </ul>
                </div>
                <div class="card my-3">
                    <div class="card-header text-warning h5 font-weight-bold">
                        Archive <i class="fas fa-angle-double-down float-right" style="color:rgb(255,0,0);cursor:pointer;"></i>
                    </div>
                    <ul class="list-group list-group-flush">
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2017-18</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2016-17</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2015-16</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2014-15</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2013-14</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2012-13</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2011-12</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2010-11</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2009-10</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2008-09</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2007-08</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2006-07</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2005-06</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2004-05</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2003-04</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2002-03</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2001-02</li></a>
                        <a class="lead text-danger" href="#" ><li class="list-group-item" >Comittee of 2000-01</li></a>

                    </ul>
                </div>

            </div>
        </div>
    </div>

</section>
<!-- Committee list  ends -->


<!-- Event details section starts -->
<section class="executive_wrap">
    <div class="container">
        <h2 class="display-41 text-center" style="margin: 20px 0;">Executive-Committee 2018-2019</h2>
        <div class="row">

            <%                Query cmmtMemSQL = dbsessionCommittee.createSQLQuery("SELECT "
                        + "cm.id_committee_member,m.member_id,m.member_name , "
                        + "cd.designation_name, cdp.department_name, cm.leader,m.id  "
                        + "FROM  committee_member cm  "
                        + "LEFT JOIN committee c ON c.id_committee = cm.id_committtee   "
                        + "LEFT JOIN member m ON m.id = cm.member_id   "
                        + "LEFT JOIN committee_designation  cd ON cd.id = cm.committee_designation  "
                        + "LEFT JOIN committee_department  cdp ON cdp.id = cm.committee_department "
                        + "WHERE m.status = '1' "
                        + "ORDER BY cm.committee_showing_order ASC");

                if (!cmmtMemSQL.list().isEmpty()) {
                    for (Iterator itCM = cmmtMemSQL.list().iterator(); itCM.hasNext();) {

                        objCmmtMem = (Object[]) itCM.next();
                        idCommitteeMember = objCmmtMem[0].toString().trim();
                        committeeMemberIEBId = objCmmtMem[1].toString().trim();
                        committeeMemberName = objCmmtMem[2].toString().trim();
                        committeeDesignationName = objCmmtMem[3].toString().trim();
                        committeeDepartmentName = objCmmtMem[4] == null ? "" : objCmmtMem[4].toString().trim();

                        if (!committeeDepartmentName.equals("")) {
                            committeeDepartmentName1 = "( " + committeeDepartmentName + " )";
                        } else {
                            committeeDepartmentName1 = "&nbsp;<br>&nbsp;";
                        }

                        commleader = objCmmtMem[5] == null ? "" : objCmmtMem[5].toString().trim();
                        if (commleader.equals("1")) {
                            classLeader = "leader";
                        } else {
                            classLeader = "";
                        }

                        committeeMemberId = objCmmtMem[6] == null ? "" : objCmmtMem[6].toString().trim();

                        System.out.println(committeeDesignationName + " ::" + committeeMemberId);
                        //    profilePicture = "M_38661.jpg";  
                        profilePicture = "no_image.jpg";
                        profilePictureUrl = GlobalVariable.imageMemberDirLink + committeeMemberId + ".jpg";

                        commArgXn = "'" + session.getId() + "','" + idCommitteeMember + "'";

                        btnCommitteeMemberEdit = "<a title=\"Edit\" href=\"" + GlobalVariable.baseUrl + "/committeeManagement/committeeMemberUpdate.php?sessionid=" + session.getId() + "&idCommitteeMember=" + idCommitteeMember + "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil-square-o\"></i></a>";
                        btnCommitteeMemberDelete = "<a onClick=\"deleteCommitteeMemberInfo(" + commArgXn + ");\" title=\"Delete comittee member\"  class=\"btn btn-primary\"><i class=\"fa fa-trash-o\"></i></a>";

                        committeeMemberInfoCon = committeeMemberInfoCon + "<div class=\"executive_single_item " + classLeader + "\">"
                                + "<div class=\"executive_slider_item\">"
                                + "<div class=\"executive_inner\">"
                                + "<div class=\"executive_img\">"
                                + "<img src=\"" + profilePictureUrl + "\" alt=\"" + committeeMemberName + "\">"
                                + "</div>"
                                + "<div class=\"hover_content\">"
                                + "<h3 class=\"member_name\">" + committeeMemberName + "</h3>"
                                + "<p class=\"member_title\">" + committeeDesignationName + "</p>"
                                + "<p class=\"ticket\">" + committeeMemberIEBId + "</p>"
                                + "<p class=\"catagory\">" + committeeDepartmentName1 + "</p>"
                                + "<ul class=\"follow_link\">"
                                + "<li><a href=\"#\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/fa-icon.png\" alt=\"img\"></a></li>"
                                + "<li><a href=\"#\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/in-icon.png\" alt=\"img\"></a></li>"
                                + "<li><a href=\"#\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/wifi-icon.png\" alt=\"img\"></a></li>"
                                + "</ul>"
                                + "</div>"
                                + "</div>"
                                + "</div>"
                                + "</div>";

                        if ((cm % 5) == 0) {
                            committeeMemberInfoCon = committeeMemberInfoCon + "</div><div class=\"row\">";
                        }

                        cm++;
                    }
                }

            %>




            <%=committeeMemberInfoCon%>



        </div>
    </div>
</section>
<!-- Event details section starts -->

<!-- Start of More Events Section -->

<!-- End of More Events Section -->
<%     dbsessionCommittee.clear();
    dbsessionCommittee.close();
%>

<%@ include file="../footer.jsp" %>
