<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<!-- News & Events & Single Event CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/photo_contact_success_style.css" rel="stylesheet">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">

    function showVideoPlayerModal(arg1) {

        console.log("showVideoPlayerModal::: " + arg1);

        var videoPlayerModal, btnInfo, arg;

        //  arg = "'" + arg1 + "','" + arg2 + "'";
        videoPlayerModal = $('#videoPlayerModal');
        rupantorLGModal.find("#videoPlayerModalTitle").text("News Delete confirmation");
        //  btnInfo = "<a onclick=\"eventDeleteInfoConfirm(" + arg + ")\" class=\"btn btn-primary btn-sm\">Confirm</a> ";
        rupantorLGModal.find("#videoPlayerModalBody").html(arg1);

//            console.log(rupantorLGModal.find("#rupantorLGModalFooter").html(btnInfo));
        rupantorLGModal.modal('show');

    }

</script>
<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Object objVdo[] = null;
    Query videoSQL = null;

    DateFormat dateFormatNews = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatNewsM = new SimpleDateFormat("MMM");
    DateFormat dateFormatNewsD = new SimpleDateFormat("dd");
    DateFormat dateFormatNewsY = new SimpleDateFormat("Y");
    Date dateNewsM = null;
    Date dateNewsD = null;
    Date dateNewsY = null;
    String newDateNewsM = "";
    String newDateNewsD = "";
    String newDateNewsY = "";

    int vx = 1;
    String videoId = "";
    String videoTitle = "";
    String videoCaptions = "";
    String videoOriginalLink = "";
    String videoEmbedLink = "";
    String videoEmbedLink1 = "";
    String videoThumbnail = "";
    String videoThumbnailn = "";
    String videoThumbnailX = "";
    String videoThumbnailXn = "";
    String videoCategory = "";
    String agrXv = "";
    String bigvideoContailner = "";
    String smallvideoContailner = "";
    String videoLinkContainer = "";

    videoSQL = dbsession.createSQLQuery("SELECT ni.id_video,ni.video_category, ni.video_title, ni.video_caption, ni.video_orginal_link, ni.video_emded_link, "
            + "ni.feature_image, ni.image1  "
            + "FROM  video_gallery_info ni ORDER BY ni.id_video DESC");

    if (!videoSQL.list().isEmpty()) {
        for (Iterator it1 = videoSQL.list().iterator(); it1.hasNext();) {

            objVdo = (Object[]) it1.next();
            videoId = objVdo[0].toString().trim();
            videoCategory = objVdo[1].toString().trim();
            videoTitle = objVdo[2].toString().trim();
            videoCaptions = objVdo[3].toString().trim();
            videoOriginalLink = objVdo[4].toString().trim();
            videoEmbedLink = objVdo[5] == null ? "" : objVdo[5].toString().trim();

            videoEmbedLink1 = "<iframe width=\"250\" height=\"150\" src=\"" + videoEmbedLink + "\"></iframe>";

            videoThumbnail = objVdo[6] == null ? "" : objVdo[6].toString().trim();
            videoThumbnailn = "<img width=\"250\" height=\"150\" src=\"" + videoThumbnail + "\" alt=\"" + videoTitle + "\">";

            videoThumbnailX = objVdo[7] == null ? "" : objVdo[7].toString().trim();

            videoLinkContainer = "<div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">"
                    + "<iframe class=\"embed-responsive-item\" src=\" " + videoEmbedLink + "\" allowfullscreen></iframe>"
                    + "</div>"
                    + "<h5>" + videoTitle + "</h5>";

            bigvideoContailner = bigvideoContailner + "<div class=\"col-md-3\">"
                    + "<!--Modal: Name-->"
                    + "<div class=\"modal fade\" id=\"modal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">"
                    + "<div class=\"modal-dialog modal-lg\" role=\"document\">"
                    + "  <!--Content-->"
                    + "  <div class=\"modal-content\">"
                    + "      <!--Body-->"
                    + "      <div class=\"modal-body mb-0 p-0\">"
                    + "          <div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">"
                    + "              <iframe class=\"embed-responsive-item\" src=\" " + videoEmbedLink + "\" allowfullscreen></iframe>"
                    + "          </div>"
                    + "      </div>"
                    + "      <!--Footer-->"
                    + "      <div class=\"modal-footer justify-content-center modal-social-icon-partho\">"
                    + "          <span class=\"mr-4\">Share</span>"
                    + "          <a type=\"button\" class=\"btn-floating btn-sm btn-fb\"><i class=\"fab fa-facebook-f\"></i></a>"
                    + "          <!--Twitter-->"
                    + "          <a type=\"button\" class=\"btn-floating btn-sm btn-tw\"><i class=\"fab fa-twitter\"></i></a>"
                    + "          <!--Google +-->"
                    + "          <a type=\"button\" class=\"btn-floating btn-sm btn-gplus\"><i class=\"fab fa-google-plus-g\"></i></a>"
                    + "          <!--Linkedin-->"
                    + "          <a type=\"button\" class=\"btn-floating btn-sm btn-ins\"><i class=\"fab fa-linkedin-in\"></i></a>"
                    + "          <button type=\"button\" class=\"btn btn-outline-primary btn-rounded btn-md ml-4\" data-dismiss=\"modal\">Close</button>"
                    + "      </div>"
                    + "  </div>"
                    + "  <!--/.Content-->"
                    + "</div>"
                    + "</div>"
                    + "<!--Modal: Name-->"
                    + "<a onclick=\"showVideoPlayerModal('" + videoLinkContainer + "')\">"
                    + "<img class=\"img-fluid z-depth-1\" src=\"" + videoThumbnail + "\" alt=\"video\" data-toggle=\"modal\" data-target=\"#modal1\" style=\"width:300px;height:200px;\">"
                    + "<p class=\"p-2 mx-auto font-weight-bold text-info more-video-title\" style=\"cursor:pointer;\">" + videoTitle + "</p>"
                    + "</a>"
                    + "</div>";

        }
    }


%>





<!-- Start of navigation -->
<div class="about_banner_gallery">
    <h2 class="display_41_saz text-white"> Video Gallery</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Gallery</a></li>
                <li class="breadcrumb-item active text-white" aria-current="page" >Video Gallery</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of navigation -->



<!-- Video gallery section -->
<section class="video_gallery_partho">
    <div class="container">        
        <div class="row">
            <div class="col-md-12">
                <div class="post-heading pt-5">
                    <h3 class="text-uppercase font-weight-bold">Feature Videos </h3>
                    <hr class="custom-horizontal-border" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!--Carousel Wrapper-->
                <div id="video-carousel-example2" class="carousel slide carousel-fade" data-ride="carousel">
                    <!--Indicators-->
                    <ol class="carousel-indicators">
                        <li data-target="#video-carousel-example2" data-slide-to="0" class="active"></li>
                        <li data-target="#video-carousel-example2" data-slide-to="1"></li>
                        <li data-target="#video-carousel-example2" data-slide-to="2"></li>
                    </ol>
                    <!--/.Indicators-->

                    <!--Slides-->
                    <div class="carousel-inner" role="listbox">
                        <!-- First slide -->
                        <div class="carousel-item active" style="height: 350px;">
                            <!--Mask color-->
                            <div class="view">
                                <!--Video source-->
                                <video class="video-fluid" autoplay loop muted>
                                    <source src="https://mdbootstrap.com/img/video/Lines.mp4" type="video/mp4" />
                                </video>
                                <div class="mask rgba-indigo-light"></div>
                            </div>

                            <!--Caption-->
                            <div class="carousel-caption">
                                <div class="animated fadeInDown">
                                    <h3 class="h3-responsive">Saved Event Video 1</h3>
                                </div>
                            </div>
                            <!--Caption-->
                        </div>
                        <!-- /.First slide -->

                        <!-- Second slide -->
                        <div class="carousel-item" style="height: 350px;">
                            <!--Mask color-->
                            <div class="view">
                                <!--Video source-->
                                <video class="video-fluid" autoplay loop muted>
                                    <source src="https://mdbootstrap.com/img/video/animation-intro.mp4" type="video/mp4" />
                                </video>
                                <div class="mask rgba-purple-slight"></div>
                            </div>

                            <!--Caption-->
                            <div class="carousel-caption">
                                <div class="animated fadeInDown">
                                    <h3 class="h3-responsive">Saved Event Video 2</h3>
                                </div>
                            </div>
                            <!--Caption-->
                        </div>
                        <!-- /.Second slide -->

                        <!-- Third slide -->
                        <div class="carousel-item" style="height: 350px;">
                            <!--Mask color-->
                            <div class="view">
                                <!--Video source-->
                                <video class="video-fluid" autoplay loop muted>
                                    <source src="https://mdbootstrap.com/img/video/Lines.mp4" type="video/mp4" />
                                </video>
                                <div class="mask rgba-black-strong"></div>
                            </div>

                            <!--Caption-->
                            <div class="carousel-caption">
                                <div class="animated fadeInDown">
                                    <h3 class="h3-responsive">Saved Event Video 3</h3>
                                </div>
                            </div>
                            <!--Caption-->
                        </div>
                        <!-- /.Third slide -->
                    </div>
                    <!--/.Slides-->
                    <!--Controls-->
                    <a class="carousel-control-prev" href="#video-carousel-example2" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#video-carousel-example2" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <!--/.Controls-->
                </div>
                <!--Carousel Wrapper-->
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="post-heading pt-5">
                    <h3 class="text-uppercase font-weight-bold">More Videos </h3>
                    <hr class="custom-horizontal-border mr-5" />
                </div>
            </div>
        </div>
        <div class="row">

            <%=bigvideoContailner%>

        </div>

    </div>
</section>
<!-- End of more videos section -->

<!-- Start of Pagination -->
<nav class="mt-5">
    <ul class="pagination justify-content-center pagination-lg">
        <li class="page-item disabled">
            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
        </li>
        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
            <a class="page-link" href="#">Next</a>
        </li>
    </ul>
</nav>
<!-- End of Pagination -->
<%
    dbsession.clear();
    dbsession.close();
%>

<%@ include file="../footer.jsp" %>
