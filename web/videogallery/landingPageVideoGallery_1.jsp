

<%@page import="org.hibernate.Session"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.appul.util.GlobalVariable"%>

<!-- end photo_gallary_wrap -->
<section class="photo_gallary_wrap">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <h3 class="sec_title">Photo Gallery <span class="float-right"><a href="#" class="view_all">View All</a></span></h3>
            </div>
        </div>
        <div class="row">

            <%
                Session dbsession = HibernateUtil.getSessionFactory().openSession();
                org.hibernate.Transaction dbtrx = null;
                dbtrx = dbsession.beginTransaction();
                Query q1 = null;
                Query q2 = null;
                Object obj[] = null;

                int ix = 1;
                String pictureId = "";
                String pictureTitle = "";
                String pictureCaption = "";
                String pictureOrginalName = "";
                String pictureName = "";
                String pictureThumb = "";
                String showingOrder = "";
                String photoAlbum = "";
                String photoSrc = "";
                String photoLink = "";
                String agrX = "";

                Query usrSQL = dbsession.createSQLQuery("SELECT "
                        + "pg.id_picture,pg.picture_title,pg.picture_caption ,pg.picture_orginal_name,pg.picture_name,pg.picture_thumb ,pg.showing_order, "
                        + "pc.category_name "
                        + "FROM  picture_gallery_info pg,picture_category pc "
                        + "WHERE pc.id_cat = pg.picture_category "
                        + "ORDER BY pg.id_picture DESC LIMIT 0, 6");

                //+ "LEFT JOIN picture_category pc ON pc.id_cat = pg.picture_category "
                if (!usrSQL.list().isEmpty()) {
                    for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {

                        obj = (Object[]) it1.next();
                        pictureId = obj[0].toString().trim();
                        pictureTitle = obj[1].toString().trim();
                        pictureCaption = obj[2].toString().trim();
                        pictureOrginalName = obj[3].toString().trim();
                        pictureName = obj[4].toString().trim();
                        pictureThumb = obj[5].toString().trim();
                        showingOrder = obj[6].toString().trim();
                        photoAlbum = obj[7].toString().trim();

                        photoSrc = GlobalVariable.imagePhotoDirLink + pictureName;
                        photoLink = "<img src=\"" + photoSrc + "\" width=\"370\" height=\"246\"/>";

                        agrX = "'" + pictureId + "','" + session.getId() + "'";
            %>


            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a href="#" class="photo_gallary_single_item">
                    <div class="photo_gallary_img">
                        <img src="<%=photoSrc%>" alt="<%=pictureCaption%>">
                    </div>
                    <div class="hover_content">
                        <p><%=pictureCaption%></p>
                    </div>
                </a>
            </div>

            <%
                        ix++;
                    }

                }
                dbsession.clear();
                dbsession.close();
            %>

    
            
            
           
        </div>
    </div>
</section>
<!-- end photo_gallary_wrap -->

