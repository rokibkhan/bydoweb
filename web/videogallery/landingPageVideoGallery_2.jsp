

<%@page import="org.hibernate.Session"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.appul.util.GlobalVariable"%>

<!-- Start featured_video_wrap -->
<section class="featured_video_wrap">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <h3 class="sec_title">Featured Video <span class="float-right"><a href="#" class="view_all">View All</a></span></h3>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="featured_main_video featured_video_slider">
                    <div class="featured_main_video_item">
                        <div class="featured_video">
                            <div class="featured_video_shap"></div>
                            <div class="featured_video_img">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_4.jpg" alt="img">
                                <div class="videoWrapper videoWrapper169 js-videoWrapper">
                                    <iframe class="videoIframe js-videoIframe" data-src="https://www.youtube.com/embed/hgzzLIa-93c?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv"></iframe>
                                </div>
                            </div>
                            <div class="pay_icon d_flex d_align">
                                <div class="icon_inner">
                                    <a href="#" class="play-btn"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/v_play_icon.png" alt="IMG"></a>
                                </div>
                            </div>
                            <div class="hover_content">
                                <div class="details_content">
                                    <p class="news_date">30 Oct, 2018</p>
                                    <a href="#"><h3 class="news_title">Seminar: ?Scope of Digitalization of Irrigation <br>Development and Management Practices in</h3></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="featured_main_video_item">
                        <div class="featured_video">
                            <div class="featured_video_shap"></div>
                            <div class="featured_video_img">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_2.jpg" alt="img">
                                <div class="videoWrapper videoWrapper169 js-videoWrapper">
                                    <iframe class="videoIframe js-videoIframe" data-src="https://www.youtube.com/embed/hgzzLIa-93c?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv"></iframe>
                                </div>
                            </div>
                            <div class="pay_icon d_flex d_align">
                                <div class="icon_inner">
                                    <a href="#" class="play-btn"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/v_play_icon.png" alt="IMG"></a>
                                </div>
                            </div>
                            <div class="hover_content">
                                <div class="details_content">
                                    <p class="news_date">30 Oct, 2018</p>
                                    <a href="#"><h3 class="news_title">Seminar: ?Scope of Digitalization of Irrigation <br>Development and Management Practices in</h3></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="featured_main_video_item">
                        <div class="featured_video">
                            <div class="featured_video_shap"></div>
                            <div class="featured_video_img">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_3.jpg" alt="img">
                                <div class="videoWrapper videoWrapper169 js-videoWrapper">
                                    <iframe class="videoIframe js-videoIframe" data-src="https://www.youtube.com/embed/hgzzLIa-93c?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv"></iframe>
                                </div>
                            </div>
                            <div class="pay_icon d_flex d_align">
                                <div class="icon_inner">
                                    <a href="#" class="play-btn"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/v_play_icon.png" alt="IMG"></a>
                                </div>
                            </div>
                            <div class="hover_content">
                                <div class="details_content">
                                    <p class="news_date">30 Oct, 2018</p>
                                    <a href="#"><h3 class="news_title">Seminar: ?Scope of Digitalization of Irrigation <br>Development and Management Practices in</h3></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="featured_main_video_item">
                        <div class="featured_video">
                            <div class="featured_video_shap"></div>
                            <div class="featured_video_img">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_4.jpg" alt="img">
                                <div class="videoWrapper videoWrapper169 js-videoWrapper">
                                    <iframe class="videoIframe js-videoIframe" data-src="https://www.youtube.com/embed/hgzzLIa-93c?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv"></iframe>
                                </div>
                            </div>
                            <div class="pay_icon d_flex d_align">
                                <div class="icon_inner">
                                    <a href="#" class="play-btn"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/v_play_icon.png" alt="IMG"></a>
                                </div>
                            </div>
                            <div class="hover_content">
                                <div class="details_content">
                                    <p class="news_date">30 Oct, 2018</p>
                                    <a href="#"><h3 class="news_title">Seminar: ?Scope of Digitalization of Irrigation <br>Development and Management Practices in</h3></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="featured_main_video_item">
                        <div class="featured_video">
                            <div class="featured_video_shap"></div>
                            <div class="featured_video_img">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_5.jpg" alt="img">
                                <div class="videoWrapper videoWrapper169 js-videoWrapper">
                                    <iframe class="videoIframe js-videoIframe" data-src="https://www.youtube.com/embed/hgzzLIa-93c?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv"></iframe>
                                </div>
                            </div>
                            <div class="pay_icon d_flex d_align">
                                <div class="icon_inner">
                                    <a href="#" class="play-btn"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/v_play_icon.png" alt="IMG"></a>
                                </div>
                            </div>
                            <div class="hover_content">
                                <div class="details_content">
                                    <p class="news_date">30 Oct, 2018</p>
                                    <a href="#"><h3 class="news_title">Seminar: ?Scope of Digitalization of Irrigation <br>Development and Management Practices in</h3></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="featured_main_video_item">
                        <div class="featured_video">
                            <div class="featured_video_shap"></div>
                            <div class="featured_video_img">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_6.jpg" alt="img">
                                <div class="videoWrapper videoWrapper169 js-videoWrapper">
                                    <iframe class="videoIframe js-videoIframe" data-src="https://www.youtube.com/embed/hgzzLIa-93c?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv"></iframe>
                                </div>
                            </div>
                            <div class="pay_icon d_flex d_align">
                                <div class="icon_inner">
                                    <a href="#" class="play-btn"><img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/v_play_icon.png" alt="IMG"></a>
                                </div>
                            </div>
                            <div class="hover_content">
                                <div class="details_content">
                                    <p class="news_date">30 Oct, 2018</p>
                                    <a href="#"><h3 class="news_title">Seminar: ?Scope of Digitalization of Irrigation <br>Development and Management Practices in</h3></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="featured_sing_item_wrap featured_sing_slider">
                <div class="featured_single_item">
                    <div class="featured_video_small_item">
                        <div class="featured_video">
                            <div class="featured_video_img">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_2.jpg" alt="img">
                            </div>
                            <div class="pay_icon d_flex d_align">
                                <div class="icon_inner">
                                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/v_play_icon.png" alt="IMG">
                                </div>
                            </div>
                        </div>
                        <div class="featured_video_details">
                            <h3>Scope of Digitalization of Irrigation</h3>
                        </div>
                    </div>
                </div>
                <div class="featured_single_item">
                    <div class="featured_video_small_item">
                        <div class="featured_video">
                            <div class="featured_video_img">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_3.jpg" alt="img">
                            </div>
                            <div class="pay_icon d_flex d_align">
                                <div class="icon_inner">
                                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/v_play_icon.png" alt="IMG">
                                </div>
                            </div>
                        </div>
                        <div class="featured_video_details">
                            <h3>Scope of Digitalization of Irrigation</h3>
                        </div>
                    </div>
                </div>
                <div class="featured_single_item">
                    <div class="featured_video_small_item">
                        <div class="featured_video">
                            <div class="featured_video_img">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_3.jpg" alt="img">
                            </div>
                            <div class="pay_icon d_flex d_align">
                                <div class="icon_inner">
                                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/v_play_icon.png" alt="IMG">
                                </div>
                            </div>
                        </div>
                        <div class="featured_video_details">
                            <h3>Scope of Digitalization of Irrigation</h3>
                        </div>
                    </div>
                </div>
                <div class="featured_single_item">
                    <div class="featured_video_small_item">
                        <div class="featured_video">
                            <div class="featured_video_img">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_4.jpg" alt="img">
                            </div>
                            <div class="pay_icon d_flex d_align">
                                <div class="icon_inner">
                                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/v_play_icon.png" alt="IMG">
                                </div>
                            </div>
                        </div>
                        <div class="featured_video_details">
                            <h3>Scope of Digitalization of Irrigation</h3>
                        </div>
                    </div>
                </div>
                <div class="featured_single_item">
                    <div class="featured_video_small_item">
                        <div class="featured_video">
                            <div class="featured_video_img">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_5.jpg" alt="img">
                            </div>
                            <div class="pay_icon d_flex d_align">
                                <div class="icon_inner">
                                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/v_play_icon.png" alt="IMG">
                                </div>
                            </div>
                        </div>
                        <div class="featured_video_details">
                            <h3>Scope of Digitalization of Irrigation</h3>
                        </div>
                    </div>
                </div>
                <div class="featured_single_item">
                    <div class="featured_video_small_item">
                        <div class="featured_video">
                            <div class="featured_video_img">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_6.jpg" alt="img">
                            </div>
                            <div class="pay_icon d_flex d_align">
                                <div class="icon_inner">
                                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/v_play_icon.png" alt="IMG">
                                </div>
                            </div>
                        </div>
                        <div class="featured_video_details">
                            <h3>Scope of Digitalization of Irrigation</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end featured_video_wrap -->