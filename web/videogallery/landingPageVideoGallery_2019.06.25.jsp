

<%@page import="org.hibernate.Session"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.appul.util.GlobalVariable"%>




<%
    Session dbsessionVideoGallary = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxVideoGallary = dbsessionVideoGallary.beginTransaction();

    Object objVdo[] = null;

    int vx = 1;
    String videoId = "";
    String videoTitle = "";
    String videoCaptions1 = "";
    int videoCaptionsLegth = 0;
    String videoCaptions = "";
    String videoOriginalLink = "";
    String videoEmbedLink = "";
    String videoEmbedLink1 = "";
    String videoThumbnail = "";
    String videoThumbnailn = "";
    String videoThumbnailX = "";
    String videoThumbnailXn = "";
    String videoCategory = "";
    String agrXv = "";
    String bigvideoContailner = "";
    String smallvideoContailner = "";
    
    String videoLinkForHomeContainerX = "";

    Query videoSQL = dbsessionVideoGallary.createSQLQuery("SELECT ni.id_video,ni.video_category, ni.video_title, ni.video_caption, ni.video_orginal_link, ni.video_emded_link, "
            + "ni.feature_image, ni.image1  "
            + "FROM  video_gallery_info ni ORDER BY ni.id_video DESC");

    if (!videoSQL.list().isEmpty()) {
        for (Iterator it1 = videoSQL.list().iterator(); it1.hasNext();) {

            objVdo = (Object[]) it1.next();
            videoId = objVdo[0].toString().trim();
            videoCategory = objVdo[1].toString().trim();
            videoTitle = objVdo[2].toString().trim();
            videoCaptions1 = objVdo[3].toString().trim();
            videoCaptionsLegth = videoCaptions1.length();
            if (videoCaptionsLegth < 50) {
                videoCaptions = videoCaptions1;
            } else {
                videoCaptions = videoCaptions1.substring(0, 50);
            }

            videoOriginalLink = objVdo[4].toString().trim();
            videoEmbedLink = objVdo[5] == null ? "" : objVdo[5].toString().trim();

        //    out.println("videoEmbedLink ::" + videoEmbedLink);

            videoEmbedLink1 = "<iframe width=\"250\" height=\"150\" src=\"" + videoEmbedLink + "\"></iframe>";

            videoThumbnail = objVdo[6] == null ? "" : objVdo[6].toString().trim();
            videoThumbnailn = "<img width=\"250\" height=\"150\" src=\"" + videoThumbnail + "\" alt=\"" + videoTitle + "\">";

            videoThumbnailX = objVdo[7] == null ? "" : objVdo[7].toString().trim();

            videoLinkForHomeContainerX = "<a onclick=\"showHomeVideoPlay('" + videoEmbedLink + "','" + videoId + "');chain();\">";
            
            //  + "<iframe class=\"videoIframe js-videoIframe\" data-src=\"" + videoEmbedLink + "?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv\"></iframe>"
            
//              + "<div class=\"hover_content\">"
//                    + "<div class=\"details_content\">"
//                    + "<!-- p class=\"news_date\">30 Oct, 2018</p -->"
//                    + "<a href=\"#\"><h3 class=\"news_title\">" + videoCaptions + "</h3></a>"
//                    + "</div>"
//                    + "</div>"
            bigvideoContailner = bigvideoContailner + "<div class=\"featured_main_video_item\">"
                    + "<div class=\"featured_video\">"
                    + "<div id=\"featured_video_shap" + videoId + "\" class=\"featured_video_shap\"></div>"
                    + "<div id=\"videoframeContainer" + videoId + "\" class=\"featured_video_img\">"
                    + "<img src=\"" + videoThumbnailX + "\" alt=\"" + videoTitle + "\" height=\"600px;\">"
                    + "<div style=\"display:block;\" id=\"videoIframeContainer" + videoId + "\" class=\"videoWrapper videoWrapper169 js-videoWrapper\">"
                    + "</div>"
                    + "</div>"
                    + "<div id=\"pay_icon" + videoId + "\" class=\"pay_icon d_flex d_align\">"
                    + "<div class=\"icon_inner\">"                    
                    + "" + videoLinkForHomeContainerX + "<img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/v_play_icon.png\" alt=\"IMG\"></a>"
                    + "</div>"
                    + "</div>"
                  
                    + "</div>"
                    + "</div>";

            smallvideoContailner = smallvideoContailner + "<div class=\"featured_single_item\">"
                    + "<div class=\"featured_video_small_item\">"
                    + "<div class=\"featured_video\">"
                    + "<div class=\"featured_video_img\">"
                    + "<img src=\"" + videoThumbnailX + "\" alt=\"img\">"
                    + "</div>"
                    + "<div class=\"pay_icon d_flex d_align\">"
                    + "<div class=\"icon_inner\">"
                    + "<img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/v_play_icon.png\" alt=\"IMG\">"
                    + " </div>"
                    + "</div>"
                    + "</div>"
                    + "<div class=\"featured_video_details\">"
                    + "<h3>" + videoCaptions + "</h3>"
                    + "</div>"
                    + "</div>"
                    + "</div>";

            vx++;
        }
    }
    dbsessionVideoGallary.clear();
    dbsessionVideoGallary.close();

%>


<!-- Start featured_video_wrap -->
<section class="featured_video_wrap"  style="background-color: #fff; padding-top: 30px;">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <h3 class="sec_title">Featured Video <span class="float-right"><a href="<%out.print(GlobalVariable.baseUrl);%>/videogallery/videoGallery.jsp" class="view_all">View All</a></span></h3>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="featured_main_video featured_video_slider">

                    <%=bigvideoContailner%>


                </div>

            </div>
        </div>
        <div class="row">
            <div class="featured_sing_item_wrap featured_sing_slider">

                <%=smallvideoContailner%>

            </div>
        </div>

    </div>
</section>
<!-- end featured_video_wrap -->

