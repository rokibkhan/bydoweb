<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<!-- News & Events & Single Event CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/photo_contact_success_style.css" rel="stylesheet">


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Object objVdo[] = null;
    Query videoSQL = null;

    DateFormat dateFormatNews = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatNewsM = new SimpleDateFormat("MMM");
    DateFormat dateFormatNewsD = new SimpleDateFormat("dd");
    DateFormat dateFormatNewsY = new SimpleDateFormat("Y");
    Date dateNewsM = null;
    Date dateNewsD = null;
    Date dateNewsY = null;
    String newDateNewsM = "";
    String newDateNewsD = "";
    String newDateNewsY = "";

    Object objVdo[] = null;

    int vx = 1;
    String videoId = "";
    String videoTitle = "";
    String videoCaptions = "";
    String videoOriginalLink = "";
    String videoEmbedLink = "";
    String videoEmbedLink1 = "";
    String videoThumbnail = "";
    String videoThumbnailn = "";
    String videoThumbnailX = "";
    String videoThumbnailXn = "";
    String videoCategory = "";
    String agrXv = "";
    String bigvideoContailner = "";
    String smallvideoContailner = "";

    Query videoSQL = dbsessionDivision.createSQLQuery("SELECT ni.id_video,ni.video_category, ni.video_title, ni.video_caption, ni.video_orginal_link, ni.video_emded_link, "
            + "ni.feature_image, ni.image1  "
            + "FROM  video_gallery_info ni ORDER BY ni.id_video DESC LIMIT 0,4");

    if (!videoSQL.list().isEmpty()) {
        for (Iterator it1 = videoSQL.list().iterator(); it1.hasNext();) {

            objVdo = (Object[]) it1.next();
            videoId = objVdo[0].toString().trim();
            videoCategory = objVdo[1].toString().trim();
            videoTitle = objVdo[2].toString().trim();
            videoCaptions = objVdo[3].toString().trim();
            videoOriginalLink = objVdo[4].toString().trim();
            videoEmbedLink = objVdo[5] == null ? "" : objVdo[5].toString().trim();

            videoEmbedLink1 = "<iframe width=\"250\" height=\"150\" src=\"" + videoEmbedLink + "\"></iframe>";

            videoThumbnail = objVdo[6] == null ? "" : objVdo[6].toString().trim();
            videoThumbnailn = "<img width=\"250\" height=\"150\" src=\"" + videoThumbnail + "\" alt=\"" + videoTitle + "\">";

            videoThumbnailX = objVdo[7] == null ? "" : objVdo[7].toString().trim();

            bigvideoContailner = bigvideoContailner + "<div class=\"col-md-3\">"
                    + "<!--Modal: Name-->"
                    + "<div class=\"modal fade\" id=\"modal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">"
                    + "<div class=\"modal-dialog modal-lg\" role=\"document\">"
                    + "  <!--Content-->"
                    + "  <div class=\"modal-content\">"
                    + "      <!--Body-->"
                    + "      <div class=\"modal-body mb-0 p-0\">"
                    + "          <div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">"
                    + "              <iframe class=\"embed-responsive-item\" src=\" " + videoEmbedLink + "\" allowfullscreen></iframe>"
                    + "          </div>"
                    + "      </div>"
                    + "      <!--Footer-->"
                    + "      <div class=\"modal-footer justify-content-center modal-social-icon-partho\">"
                    + "          <span class=\"mr-4\">Share</span>"
                    + "          <a type=\"button\" class=\"btn-floating btn-sm btn-fb\"><i class=\"fab fa-facebook-f\"></i></a>"
                    + "          <!--Twitter-->"
                    + "          <a type=\"button\" class=\"btn-floating btn-sm btn-tw\"><i class=\"fab fa-twitter\"></i></a>"
                    + "          <!--Google +-->"
                    + "          <a type=\"button\" class=\"btn-floating btn-sm btn-gplus\"><i class=\"fab fa-google-plus-g\"></i></a>"
                    + "          <!--Linkedin-->"
                    + "          <a type=\"button\" class=\"btn-floating btn-sm btn-ins\"><i class=\"fab fa-linkedin-in\"></i></a>"
                    + "          <button type=\"button\" class=\"btn btn-outline-primary btn-rounded btn-md ml-4\" data-dismiss=\"modal\">Close</button>"
                    + "      </div>"
                    + "  </div>"
                    + "  <!--/.Content-->"
                    + "</div>"
                    + "</div>"
                    + "<!--Modal: Name-->"
                    + "<a>"
                    + "<img class=\"img-fluid z-depth-1\" src=\"" + videoThumbnail + "\" alt=\"video\" data-toggle=\"modal\" data-target=\"#modal1\" style=\"width:300px;height:200px;\">"
                    + "<p class=\"p-2 mx-auto font-weight-bold text-info more-video-title\" style=\"cursor:pointer;\">" + videoTitle + "</p>"
                    + "</a>"
                    + "</div>";

        }
    }

       

%>

<!-- Start of navigation -->
<section class="navigation_partho parallaxie text-center" style="height:200px;background:url('<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/pexels-photo-88476.jpeg');">
    <h3 class="h1 pl-5 mx-5 font-weight-bold text-white pt-5">Video Gallery</h3>
    <nav aria-label="breadcrumb" class="w-100 pl-5">
        <ol class="breadcrumb h3" style="background-color: #e9ecef00" >
            <li class="breadcrumb-item"><a href="home_v4.html">Home</a></li>
            <li class="breadcrumb-item active text-white" aria-current="page" >Video Gallery</li>
        </ol>
    </nav>
</section>
<!-- End of navigation -->

<!-- Video gallery section -->
<section class="video_gallery_partho">
    <div class="container">        
        <div class="row">
            <div class="col-md-12">
                <div class="post-heading pt-5">
                    <h3 class="text-uppercase font-weight-bold">Feature Videos </h3>
                    <hr class="custom-horizontal-border mr-5" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!--Carousel Wrapper-->
                <div id="video-carousel-example2" class="carousel slide carousel-fade" data-ride="carousel">
                    <!--Indicators-->
                    <ol class="carousel-indicators">
                        <li data-target="#video-carousel-example2" data-slide-to="0" class="active"></li>
                        <li data-target="#video-carousel-example2" data-slide-to="1"></li>
                        <li data-target="#video-carousel-example2" data-slide-to="2"></li>
                    </ol>
                    <!--/.Indicators-->

                    <!--Slides-->
                    <div class="carousel-inner" role="listbox">
                        <!-- First slide -->
                        <div class="carousel-item active">
                            <!--Mask color-->
                            <div class="view">
                                <!--Video source-->
                                <video class="video-fluid" autoplay loop muted>
                                    <source src="https://mdbootstrap.com/img/video/Lines.mp4" type="video/mp4" />
                                </video>
                                <div class="mask rgba-indigo-light"></div>
                            </div>

                            <!--Caption-->
                            <div class="carousel-caption">
                                <div class="animated fadeInDown">
                                    <h3 class="h3-responsive">Saved Event Video 1</h3>
                                </div>
                            </div>
                            <!--Caption-->
                        </div>
                        <!-- /.First slide -->

                        <!-- Second slide -->
                        <div class="carousel-item">
                            <!--Mask color-->
                            <div class="view">
                                <!--Video source-->
                                <video class="video-fluid" autoplay loop muted>
                                    <source src="https://mdbootstrap.com/img/video/animation-intro.mp4" type="video/mp4" />
                                </video>
                                <div class="mask rgba-purple-slight"></div>
                            </div>

                            <!--Caption-->
                            <div class="carousel-caption">
                                <div class="animated fadeInDown">
                                    <h3 class="h3-responsive">Saved Event Video 2</h3>
                                </div>
                            </div>
                            <!--Caption-->
                        </div>
                        <!-- /.Second slide -->

                        <!-- Third slide -->
                        <div class="carousel-item">
                            <!--Mask color-->
                            <div class="view">
                                <!--Video source-->
                                <video class="video-fluid" autoplay loop muted>
                                    <source src="https://mdbootstrap.com/img/video/Lines.mp4" type="video/mp4" />
                                </video>
                                <div class="mask rgba-black-strong"></div>
                            </div>

                            <!--Caption-->
                            <div class="carousel-caption">
                                <div class="animated fadeInDown">
                                    <h3 class="h3-responsive">Saved Event Video 3</h3>
                                </div>
                            </div>
                            <!--Caption-->
                        </div>
                        <!-- /.Third slide -->
                    </div>
                    <!--/.Slides-->
                    <!--Controls-->
                    <a class="carousel-control-prev" href="#video-carousel-example2" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#video-carousel-example2" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <!--/.Controls-->
                </div>
                <!--Carousel Wrapper-->
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="post-heading pt-5">
                    <h3 class="text-uppercase font-weight-bold">More Videos </h3>
                    <hr class="custom-horizontal-border mr-5" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->

            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_4.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->

            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_3.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->

            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>
                                <h5>Video Title</h5>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_2.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->


        </div>
        <div class="row my-5">
            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->

            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_4.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->

            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_3.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->

            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>
                                <h5>Video Title</h5>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_2.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->


        </div>
        <div class="row">
            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->

            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_4.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->

            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_3.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->

            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>
                                <h5>Video Title</h5>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_2.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->


        </div>
        <div class="row my-5">
            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->

            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_4.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->

            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_3.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->

            <div class="col-md-3">
                <!--Modal: Name-->
                <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">

                        <!--Content-->
                        <div class="modal-content">

                            <!--Body-->
                            <div class="modal-body mb-0 p-0">

                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
                                </div>
                                <h5>Video Title</h5>

                            </div>

                            <!--Footer-->
                            <div class="modal-footer justify-content-center modal-social-icon-partho">
                                <span class="mr-4">Share</span>
                                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                                <!--Twitter-->
                                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                                <!--Google +-->
                                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                                <!--Linkedin-->
                                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>

                                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>

                            </div>

                        </div>
                        <!--/.Content-->

                    </div>
                </div>
                <!--Modal: Name-->

                <a><img class="img-fluid z-depth-1" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_2.jpg" alt="video"
                        data-toggle="modal" data-target="#modal1" style="width:300px;height:200px;">
                    <h5 class="p-2 mx-auto font-weight-bold text-info more-video-title" style="cursor:pointer;">Video Title</h5></a>

            </div>
            <!-- Grid column -->


        </div>
    </div>
</section>
<!-- End of more videos section -->

<!-- Start of Pagination -->
<nav class="mt-5">
    <ul class="pagination justify-content-center pagination-lg">
        <li class="page-item disabled">
            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
        </li>
        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
            <a class="page-link" href="#">Next</a>
        </li>
    </ul>
</nav>
<!-- End of Pagination -->


<%@ include file="../footer.jsp" %>
