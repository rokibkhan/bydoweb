<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<!-- News & Events & Single Event CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/photo_contact_success_style.css" rel="stylesheet">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">

   

</script>
<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Object objVdo[] = null;
    Query videoSQL = null;

    DateFormat dateFormatNews = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatNewsM = new SimpleDateFormat("MMM");
    DateFormat dateFormatNewsD = new SimpleDateFormat("dd");
    DateFormat dateFormatNewsY = new SimpleDateFormat("Y");
    Date dateNewsM = null;
    Date dateNewsD = null;
    Date dateNewsY = null;
    String newDateNewsM = "";
    String newDateNewsD = "";
    String newDateNewsY = "";

    int vx = 1;
    String videoId = "";
    String videoTitle = "";
    String videoCaptions = "";
    String videoOriginalLink = "";
    String videoEmbedLink = "";
    String videoEmbedLink1 = "";
    String videoThumbnail = "";
    String videoThumbnailn = "";
    String videoThumbnailX = "";
    String videoThumbnailXn = "";
    String videoCategory = "";
    String agrXv = "";
    String bigvideoContailner = "";
    String smallvideoContailner = "";
    String videoLinkContainer = "";
    String videoLinkContainerX = "";

    videoSQL = dbsession.createSQLQuery("SELECT ni.id_video,ni.video_category, ni.video_title, ni.video_caption, ni.video_orginal_link, ni.video_emded_link, "
            + "ni.feature_image, ni.image1  "
            + "FROM  video_gallery_info ni ORDER BY ni.id_video DESC");

    if (!videoSQL.list().isEmpty()) {
        for (Iterator it1 = videoSQL.list().iterator(); it1.hasNext();) {

            objVdo = (Object[]) it1.next();
            videoId = objVdo[0].toString().trim();
            videoCategory = objVdo[1].toString().trim();
            videoTitle = objVdo[2].toString().trim();
            videoCaptions = objVdo[3].toString().trim();
            videoOriginalLink = objVdo[4].toString().trim();
            videoEmbedLink = objVdo[5] == null ? "" : objVdo[5].toString().trim();

            videoEmbedLink1 = "<iframe width=\"250\" height=\"150\" src=\"" + videoEmbedLink + "\"></iframe>";

            videoThumbnail = objVdo[6] == null ? "" : objVdo[6].toString().trim();
            videoThumbnailn = "<img width=\"250\" height=\"150\" src=\"" + videoThumbnail + "\" alt=\"" + videoTitle + "\">";

            videoThumbnailX = objVdo[7] == null ? "" : objVdo[7].toString().trim();

            videoLinkContainer = "<div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">"
                    + "<iframe class=\"embed-responsive-item\" src=\" " + videoEmbedLink + "\" allowfullscreen></iframe>"
                    + "</div>"
                    + "<h5>" + videoTitle + "</h5>";

            videoLinkContainerX = "<a onclick=\"showVideoPlayerModal('" + videoEmbedLink + "')\">";

            bigvideoContailner = bigvideoContailner + "<div class=\"col-md-3\">"
                    + "" + videoLinkContainerX + ""
                    + "<img class=\"img-fluid z-depth-1\" src=\"" + videoThumbnail + "\" alt=\"video\" data-toggle=\"modal\" data-target=\"#modal1\" style=\"width:300px;height:200px;\">"
                    + "<p class=\"p-2 mx-auto font-weight-bold text-info more-video-title\" style=\"cursor:pointer;\">" + videoTitle + "</p>"
                    + "</a>"
                    + "</div>";

        }
    }


%>





<!-- Start of navigation -->
<div class="about_banner_gallery">
    <h2 class="display_41_saz text-white"> Video Gallery</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Gallery</a></li>
                <li class="breadcrumb-item active text-white" aria-current="page" >Video Gallery</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of navigation -->



<!-- Video gallery section -->
<section class="video_gallery_partho">
    <div class="container">        

        <br/>
        <div class="row">

            <%=bigvideoContailner%>

        </div>

    </div>
</section>
<!-- End of more videos section -->

<!-- Start of Pagination -->
<nav class="mt-5">
    <ul class="pagination justify-content-center pagination-lg">
        <li class="page-item disabled">
            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
        </li>
        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
            <a class="page-link" href="#">Next</a>
        </li>
    </ul>
</nav>
<!-- End of Pagination -->
<%
    dbsession.clear();
    dbsession.close();
%>

<%@ include file="../footer.jsp" %>
