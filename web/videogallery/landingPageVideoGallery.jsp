

<%@page import="org.hibernate.Session"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.appul.util.GlobalVariable"%>




<%
    Session dbsessionVideoGallary = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxVideoGallary = dbsessionVideoGallary.beginTransaction();

    Object objVdo[] = null;

    int vx = 1;
    String videoId = "";
    String videoTitle = "";
    String videoCaptions1 = "";
    int videoCaptionsLegth = 0;
    String videoCaptions = "";
    String videoOriginalLink = "";
    String videoEmbedLink = "";
    String videoEmbedLink1 = "";
    String videoThumbnail = "";
    String videoThumbnailn = "";
    String videoThumbnailX = "";
    String videoThumbnailXn = "";
    String videoCategory = "";
    String agrXv = "";
    String bigvideoContailner = "";
    String smallvideoContailner = "";
    String videoLinkContainer = "";
    String videoLinkContainerX = "";

    String videoLinkForHomeContainerX = "";

    Query videoSQL = dbsessionVideoGallary.createSQLQuery("SELECT ni.id_video,ni.video_category, ni.video_title, ni.video_caption, ni.video_orginal_link, ni.video_emded_link, "
            + "ni.feature_image, ni.image1  "
            + "FROM  video_gallery_info ni ORDER BY ni.id_video DESC limit 0,4");

    if (!videoSQL.list().isEmpty()) {
        for (Iterator it1 = videoSQL.list().iterator(); it1.hasNext();) {

            objVdo = (Object[]) it1.next();
            videoId = objVdo[0].toString().trim();
            videoCategory = objVdo[1].toString().trim();
            videoTitle = objVdo[2].toString().trim();
            videoCaptions = objVdo[3].toString().trim();
            videoOriginalLink = objVdo[4].toString().trim();
            videoEmbedLink = objVdo[5] == null ? "" : objVdo[5].toString().trim();

            videoEmbedLink1 = "<iframe width=\"250\" height=\"150\" src=\"" + videoEmbedLink + "\"></iframe>";

            videoThumbnail = objVdo[6] == null ? "" : objVdo[6].toString().trim();
            videoThumbnailn = "<img width=\"250\" height=\"150\" src=\"" + videoThumbnail + "\" alt=\"" + videoTitle + "\">";

            videoThumbnailX = objVdo[7] == null ? "" : objVdo[7].toString().trim();

            videoLinkContainer = "<div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">"
                    + "<iframe class=\"embed-responsive-item\" src=\" " + videoEmbedLink + "\" allowfullscreen></iframe>"
                    + "</div>"
                    + "<h5>" + videoTitle + "</h5>";

            videoLinkContainerX = "<a onclick=\"showVideoPlayerModal('" + videoEmbedLink + "')\">";

            bigvideoContailner = bigvideoContailner + "<div class=\"col-md-3\">"
                    + "" + videoLinkContainerX + ""
                    + "<img class=\"img-fluid z-depth-1\" src=\"" + videoThumbnail + "\" alt=\"video\" data-toggle=\"modal\" data-target=\"#modal1\" style=\"width:300px;height:200px;\">"
                    + "<p class=\"p-2 mx-auto font-weight-bold text-info more-video-title\" style=\"cursor:pointer;\">" + videoTitle + "</p>"
                    + "</a>"
                    + "</div>";

            vx++;
        }
    }
    dbsessionVideoGallary.clear();
    dbsessionVideoGallary.close();

%>


<!-- Start featured_video_wrap -->
<section class="featured_video_wrap"  style="background-color: #fff; padding-top: 30px;">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <h3 class="sec_title">Featured Video <span class="float-right"><a href="<%out.print(GlobalVariable.baseUrl);%>/videogallery/videoGallery.jsp" class="view_all">View All</a></span></h3>
            </div>
        </div>

        <div class="row">

                <%=bigvideoContailner%>
        </div>


    </div>
</section>
<!-- end featured_video_wrap -->

