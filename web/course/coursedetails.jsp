<%-- 
    Document   : coursedetails
    Created on : Apr 25, 2020, 1:07:51 PM
    Author     : Rokib
--%>

<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>

<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    Session dbsessionCourse = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxRegistration = dbsessionCourse.beginTransaction();

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    
    System.out.println("CourseDetails");
    
    String courseid = request.getParameter("courseid").trim();
    
    System.out.println("CourseID:"+courseid);
    
    String coursename = "";
    
    Query regSQL = dbsessionCourse.createSQLQuery("select course_name from course_info WHERE id_course ='" + courseid + "'");
    if (!regSQL.list().isEmpty()) {
        
        coursename = regSQL.uniqueResult().toString();
    }
    
    //String coursename = request.getParameter("coursename").trim();
    
    System.out.println("CourseName:"+coursename);

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();

    String msgDispalyConT, msgInfoText, sLinkOpt;

    if (!strMsg.equals("")) {

        msgDispalyConT = "style=\"display: block; margin-top:5px;\"";

        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {

        msgDispalyConT = "style=\"display: none;\"";

        msgInfoText = "";

    }

%>
<style>
    ul.breadcrumb {
      padding: 10px 16px;
      list-style: none;
      background-color: #eee;
    }
    ul.breadcrumb li {
      display: inline;
      font-size: 14px;
    }
    ul.breadcrumb li+li:before {
      padding: 8px;
      color: black;
      content: "/\00a0";
    }
    ul.breadcrumb li a {
      color: #0275d8;
      text-decoration: none;
    }
    ul.breadcrumb li a:hover {
      color: #01447e;
      text-decoration: underline;
    }
</style>

 <div>
    <ul class="breadcrumb">
        <li><a href="<%out.print(GlobalVariable.baseUrl);%>">Home</a></li>
        <li><a href="<%out.print(GlobalVariable.baseUrl);%>/course/course.jsp">Course</a></li>
        
        <li>Course Details</li>
    </div>                        


<!-- course section start -->

<section class="container-fluid py-5" style="background: url(<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/course-bg.png);background-size: 100% 100%;">

    <div class="container mb-3">

        <button type="button" class="btn btn-success btn-lg btn-block"><%out.print(coursename);%></button>

    </div>
    
    
                <%
                    //Session dbsessionSubject = HibernateUtil.getSessionFactory().openSession();//.openSession();

                     //org.hibernate.Transaction dbtrx = dbsessionSubject.beginTransaction();
                     
                                //SubjectInfo obj = null;
                                
                                   Object[] obj = null;
                                   int collapse = 1; 
                                   int childcollapse = 31;
                                   int accordion = 1;
                                   int childaccordion = 21;
                                    String SubjectName = "";
                                    String IdSubject = "";
                                    String FeatureImage = "";
                                    Query usrSQL = dbsessionCourse.createSQLQuery("SELECT id_subject ,subject_name, feature_image"
                                            + " FROM  subject_info where course_id='"+courseid+"'");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {
                                            

                                            //obj = (SubjectInfo) it1.next();
                                            obj = (Object[]) it1.next();
                                            
                                            IdSubject = obj[0].toString().trim();
                                            SubjectName = obj[1].toString().trim();
                                            FeatureImage = obj[2].toString().trim();
                                            
                                            
                                            
                                            %>
       <div class="container bg-white py-5 phy mt-4">
    <div class="row">
        <div class="col-md-6 pb-sm-3 d-flex justify-content-sm-center sfx4">
            <img class="rounded-circle course2 sfx5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/<%out.print(FeatureImage);%>" width="350" height="350">
        </div>

        <div class="col-md-6">
            <div class="container">

                <div id="accordion<%out.print(accordion);%>">
                    
                    <%
                                    Object[] obj2 = null;
                                    
                                    String SessionName = "";
                                    String IdSession = "";
                                    //String SFeatureImage = "";
                                    Query sesSQL = dbsessionCourse.createSQLQuery("SELECT id_session ,session_name"
                                            + " FROM  session_info where subject_id = '" + IdSubject + "'");

                                    if (!sesSQL.list().isEmpty()) {
                                        for (Iterator it2 = sesSQL.list().iterator(); it2.hasNext();) {
                                            

                                            //obj = (SubjectInfo) it1.next();
                                            obj2 = (Object[]) it2.next();
                                            
                                            IdSession = obj2[0].toString().trim();
                                            SessionName = obj2[1].toString().trim();
                                            //FeatureImage = obj[2].toString().trim();
                        
                    %>
                    
                    <div class="card">
                        <div class="card-header course1">
                            <a class="card-link text-white" data-toggle="collapse" href="#collapse<%out.print(collapse);%>">
                                <span><%out.print(SessionName);%></span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapse<%out.print(collapse);%>" class="collapse" data-parent="#accordion<%out.print(accordion);%>">
                            <div class="card-body">
                                
                                <%
                                    Object[] obj3 = null;
                                   
                                    String ChapterName = "";
                                    String ChapterShortName = "";
                                    String IdChapter = "";
                                    //String SFeatureImage = "";
                                    Query chapSQL = dbsessionCourse.createSQLQuery("SELECT id_chapter ,chapter_name,chapter_short_name"
                                            + " FROM  chapter_info where session_id = '" + IdSession + "'");

                                    if (!chapSQL.list().isEmpty()) {
                                        for (Iterator it3 = chapSQL.list().iterator(); it3.hasNext();) {
                                            

                                            //obj = (SubjectInfo) it1.next();
                                            obj3 = (Object[]) it3.next();
                                            
                                            IdChapter = obj3[0].toString().trim();
                                            ChapterName = obj3[1].toString().trim();
                                            ChapterShortName = obj3[2].toString().trim();
                                            //FeatureImage = obj[2].toString().trim();
                        
                    %>
                    
                                
                    <%--
                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary" style="width: 65%;margin-bottom:1%;">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span style="font-weight: bold;"><%out.print(ChapterShortName);%></span><br>
                                        <span><%out.print(ChapterName);%></span>	
                                    </button> <br>
                                </a>
                                    
                    --%>            
                    
                    <div id="accordion<%out.print(childaccordion);%>">
                    <div class="card">
                        <div class="card-header course1">
                            <a class="card-link text-white" data-toggle="collapse" href="#collapse<%out.print(childcollapse);%>">
                                <span> <%out.print(ChapterName);%></span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapse<%out.print(childcollapse);%>" class="collapse" data-parent="#accordion<%out.print(childaccordion);%>">
                            <div class="card-body">
                                
                                
                                <%
                                    Object[] obj4 = null;
                                   
                                    String VideoTitle = "";
                                    String VideoEmbedLink = "";
                                    String IdVideo = "";                                    
                                    String VideoCaption = "";
                                    //String SFeatureImage = "";
                                    Query videoSQL = dbsessionCourse.createSQLQuery("SELECT id_video ,video_title,video_emded_link,video_caption"
                                            + " FROM  video_gallery_info where chapter_id = '" + IdChapter + "'");

                                    if (!videoSQL.list().isEmpty()) {
                                        for (Iterator it4 = videoSQL.list().iterator(); it4.hasNext();) {
                                            

                                            //obj = (SubjectInfo) it1.next();
                                            obj4 = (Object[]) it4.next();
                                            
                                            IdVideo = obj4[0].toString().trim();
                                            VideoTitle = obj4[1].toString().trim();
                                            VideoEmbedLink = obj4[2].toString().trim();
                                            VideoCaption = obj4[3].toString().trim();
                                            //FeatureImage = obj[2].toString().trim();
                        
                    %>
                    
                                 <form id="loginform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmLogin" action="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">

                                     <input type="hidden" name="coid" id="coid" value="<%=courseid%>" >
                                     
                                     <input type="hidden" name="coname" id="coname" value="<%=coursename%>" >
                                     
                                    <input type="hidden" name="sid" id="sid" value="<%=IdSubject%>" >

                                    <input type="hidden" name="seid" id="seid" value="<%=IdSession%>" >

                                    <input type="hidden" name="cid" id="cid" value="<%=IdChapter%>" >
                                    
                                    <input type="hidden" name="vid" id="vid" value="<%=IdVideo%>" >
                                    
                                     <input type="hidden" name="vsrc" id="vsrc" value="<%=VideoEmbedLink%>" >
                                     
                                     <input type="hidden" name="vtitle" id="vsrc" value="<%=VideoTitle%>" >
                                     
                                     <input type="hidden" name="vcaption" id="vsrc" value="<%=VideoCaption%>" >
                                     
                                     <a href="#">

                             <!--   <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp?sid=<%out.print(IdSubject);%>&seid=<%out.print(IdSession);%>&cid=<%out.print(IdChapter);%>&vid=<%out.print(IdVideo);%>&vsrc=<%out.print(VideoEmbedLink);%>">-->
                                    
                                         <button class="btn btn-outline-secondary" style="margin-bottom:1%;">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span ><%out.print(VideoTitle);%></span>	
                                    </button><br>
                                </a>
                                 </form>
                                    
                     
                                    <%
                                        
                                        }
                                    }
                                    %>               

                               
                            </div>
                        </div>
                    </div>
                    </div>
                                    
                                    <%
                                        childcollapse++;
                                        childaccordion++;
                                        }
                                    }
                                    %>
                                   
                        
                            </div>
                        </div>
                    </div>
                                    
                                <%
                                    collapse++;
                                    
                                    }
                                }
                                    accordion++;
                                %>
          <%--          
                    <div class="card">
                        <div class="card-header course1">
                            <a class="card-link text-white" data-toggle="collapse" href="#collapseOne">
                                <span> Mathematics 1</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span>	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header course1">
                            <a class="collapsed card-link text-white" data-toggle="collapse" href="#collapseTwo">
                                <span>Mathematics 2</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <div class="card-body">

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span>	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
          
          --%>

                </div>
            </div>
        </div>
    </div>
</div>
                                            <%
                                            //accordion++;
                                        }
                                    }


                                    

                    %>
                    <%--  
    
    <div class="container bg-white py-5 phy">
        <div class="row">
            <div class="col-md-6 pb-sm-3 d-flex justify-content-sm-center sfx4">
                <img class="rounded-circle sfx5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/math.jpg" width="350" height="350">
            </div>
            <div class="col-md-6">
                <div class="container">

                    <div id="accordion">
                        <div class="card">
                            <div class="card-header course1">
                                <a class="card-link text-white" data-toggle="collapse" href="#collapseOne">
                                    <span>Mathematics 1</span>
                                    <i class="fa fa-chevron-down fa-lg float-right"></i>
                                </a>
                            </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                <div class="card-body">

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span>	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header course1">
                                <a class="collapsed card-link text-white" data-toggle="collapse" href="#collapseTwo">
                                    <span>Mathematics 2</span>
                                    <i class="fa fa-chevron-down fa-lg float-right"></i>
                                </a>
                            </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="card-body">

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                            <button class="btn btn-outline-secondary">
                                                <i class="fa fa-play-circle fa-2x align-middle"></i>
                                                <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                                <span>[Permutation and Combination]</span>	
                                            </button><br>
                                        </a>

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                            <button class="btn btn-outline-secondary mt-2">
                                                <i class="fa fa-play-circle fa-2x align-middle"></i>
                                                <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                                <span>[Permutation and Combination]</span> 	
                                            </button><br>
                                        </a>

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                            <button class="btn btn-outline-secondary mt-2">
                                                <i class="fa fa-play-circle fa-2x align-middle"></i>
                                                <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                                <span>[Permutation and Combination]</span> 	
                                            </button><br>
                                        </a>

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                            <button class="btn btn-outline-secondary mt-2">
                                                <i class="fa fa-play-circle fa-2x align-middle"></i>
                                                <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                                <span>[Permutation and Combination]</span> 	
                                            </button><br>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<!--</div>-->

<div class="container bg-white py-5 phy mt-4">
    <div class="row">
        <div class="col-md-6 pb-sm-3 d-flex justify-content-sm-center sfx4">
            <img class="rounded-circle sfx5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/physics.png" width="350" height="350">
        </div>

        <div class="col-md-6">
            <div class="container">

                <div id="accordion">
                    <div class="card">
                        <div class="card-header course1">
                            <a class="card-link text-white" data-toggle="collapse" href="#collapseOne">
                                <span>Mathematics 1</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span>	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header course1">
                            <a class="collapsed card-link text-white" data-toggle="collapse" href="#collapseTwo">
                                <span>Mathematics 2</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <div class="card-body">

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span>	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="container bg-white py-5 phy mt-4">
    <div class="row">
        <div class="col-md-6 pb-sm-3 d-flex justify-content-sm-center sfx4">
            <img class="rounded-circle course2 sfx5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/chemistry.png" width="350" height="350">
        </div>

        <div class="col-md-6">
            <div class="container">

                <div id="accordion">
                    <div class="card">
                        <div class="card-header course1">
                            <a class="card-link text-white" data-toggle="collapse" href="#collapseOne">
                                <span>Mathematics 1</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span>	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header course1">
                            <a class="collapsed card-link text-white" data-toggle="collapse" href="#collapseTwo">
                                <span>Mathematics 2</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <div class="card-body">

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span>	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
                    --%>                    
</section>





<%    dbsessionCourse.flush();

    dbsessionCourse.close();
    



%>

<%@ include file="../footer.jsp" %>
