<%-- 
    Document   : videocommentlikeAdd
    Created on : Apr 19, 2020, 12:10:26 PM
    Author     : HP
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.time.LocalDate"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    Query q1 = null;
    Object obj[] = null;
    Query centerSQL = null;
    

    String centerId = "";
    String centerShortName = "";
    String centerName = "";
    String centerOptionCon = "";
    String centerInfoCon = "";
    String mainInfoContainer = "";

    JSONArray jsonArr = new JSONArray();
    JSONObject json = new JSONObject();
    String responseCode = "";
    String responseMsg = "";
    String responseMsgHtml = "";
    String videoName = "";

    String commentid = request.getParameter("commentid");
    
    String memberid = request.getParameter("memberid");
    
    int likecount = Integer.parseInt(request.getParameter("likecount"));
    
    System.out.println("videocommentLikeAdd :: commentid " + commentid);
    
    String commentfrom = request.getParameter("commentfrom");
    
    getRegistryID getId = new getRegistryID();
    String idS = getId.getID(69);
    int videocommentlikeId = Integer.parseInt(idS);

    //System.out.println("videoCenterInfoShow :: videoId " + videoId);
    
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date now = new Date();
    String strDateTime = sdfDate.format(now);

    
    Query videocommentAddSQL = dbsession.createSQLQuery("INSERT INTO video_comment_like_history("
                + "id,"
                + "member_id,"
                + "comment_id,"
                + "rating_date"                
                + ") VALUES("
                + "'" + videocommentlikeId + "',"
                + "'" + memberid + "',"        
                + "'" + commentid + "',"                
                + "'" + strDateTime + "'"
                + "  ) ");
    
    
        System.out.println("videocommentAddSQL :: videocommentAddSQL " + videocommentAddSQL);

        videocommentAddSQL.executeUpdate();
        
        
        
        dbtrx.commit();

        if (dbtrx.wasCommitted()) {
            //  strMsg = "Success !!! New Committee added";
            //  response.sendRedirect(GlobalVariable.baseUrl + "/committeeManagement/committeeAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg + "");

         dbtrx = null;
         dbtrx = dbsession.beginTransaction();
         Query q = dbsession.createSQLQuery("UPDATE video_comment "
                 + "SET like_count =  '" + (likecount+1) +"'"
                 
                 + " WHERE  id = "
                 + commentid );
                         
         
         q.executeUpdate();
         dbsession.flush();
         dbtrx.commit();
         //dbsession.close();
         
         if (dbtrx.wasCommitted()) {
             //strMsg = "Updated successfully";
             
            responseCode = "1";
            responseMsg = "Success!!! Comment add for video successfully";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                    + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                    + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                    + "</div>";
             //newsList.jsp?sessionid=4DAAD0AE8DD4EBDB6F2964C59495671D&read=Y&write=Y
             //http://localhost:8084/IEBADMIN/home.jsp?sessionid=4DAAD0AE8DD4EBDB6F2964C59495671D
             //response.sendRedirect(GlobalVariable.baseUrl +  "/newsManagement/newsList.jsp?sessionid=" + sessionIdH + "&read=Y&write=Y");
         }
         else
         {
            dbtrx.rollback();
            responseCode = "0";
            responseMsg = "Error!!! Like Count not updated";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

         }

        }
        else
        {
            dbtrx.rollback();
            responseCode = "0";
            responseMsg = "Error!!! Comment Like add  Failed";
            responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>"
                + "</div>";

        }
        
        
        //responseCode = "1";

        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        //json.put("mainInfoContainer", mainInfoContainer);
        //json.put("requestId", videoId);
        jsonArr.add(json);



    out.println(jsonArr);
    dbsession.flush();
    dbsession.close();


%>