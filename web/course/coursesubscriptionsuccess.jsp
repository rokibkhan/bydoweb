<%-- 
    Document   : coursesubscriptionsuccess
    Created on : Apr 27, 2020, 1:06:04 PM
    Author     : HP
--%>

<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>

<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>


<%@ include file="../header.jsp" %>
<%
    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";
    
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    
    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();
        
        
        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();
        System.out.println("memberIdH:"+memberIdH);
        

    } else {

        response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
        return;
    }

    

%>



<%    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();

    String msgDispalyConT, msgInfoText, sLinkOpt;

    if (!strMsg.equals("")) {

        msgDispalyConT = "style=\"display: block; margin-top:5px;\"";

        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {

        msgDispalyConT = "style=\"display: none;\"";

        msgInfoText = "";

    }

%>
<style>
    ul.breadcrumb {
      padding: 10px 16px;
      list-style: none;
      background-color: #eee;
    }
    ul.breadcrumb li {
      display: inline;
      font-size: 18px;
    }
    ul.breadcrumb li+li:before {
      padding: 8px;
      color: black;
      content: "/\00a0";
    }
    ul.breadcrumb li a {
      color: #0275d8;
      text-decoration: none;
    }
    ul.breadcrumb li a:hover {
      color: #01447e;
      text-decoration: underline;
    }
</style>

 
   <div>
    <ul class="breadcrumb">
        <li><a href="<%out.print(GlobalVariable.baseUrl);%>">Home</a></li>
        <li><a href="<%out.print(GlobalVariable.baseUrl);%>/course/course.jsp">Course</a></li>
        <li><a href="<%out.print(GlobalVariable.baseUrl);%>/course/coursesubscriptionsuccess.jsp">Course Subscription</a></li>
        <li>Course Subscription Success</li>
    </div>                        
    <div class="container">
    <div class="row">
        
        <div class="col-md-10">
            <!--<div class="mas-banner-img">
                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/37586.jpg" alt="img"/>
            </div>-->
            <div class="container ta-pro">
                <div class="row">
                    
                    
                    <!--<div class="col-md-3">
                        <img class="rounded teacher" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/mas/hh.jpg" width="70%" style="margin-top: -100px;">
                    </div>
                    <div class="col-md-9">
                        <span class="font-weight-bold" style="font-size: 25px;">Engineering / KA Unit Admission Test</span><br>
                        
                    </div>-->
                </div>  
            </div>

            <div class="mas-main-sec">
                <div class="mas-main-sec-item">
                    <div class="row globalAlertInfoBoxConParentTT">
                    <!-- .globalAlertInfoBoxConTT start -->
                    <div id="globalAlertInfoBoxConTT" class="col-md-12 globalAlertInfoBoxConTT" <%=msgDispalyConT%>>
                        <div class="alert alert-success alert-dismissable" style="border-left: 4px solid #4CAF50;">
                            <a href="#" class="close closeTT" data-dismiss="alert" aria-label="close">&times;</a>                             
                            <%=msgInfoText%>
                        </div>
                    </div>
                    <!-- .globalAlertInfoBoxConTT end -->
                </div>
                    <div class="mas-main-sec-item-content my-4">
                        
                        <%
                                    Object[] obj = null;
                                   
                                    String CourseName = "";
                                    String CourseShortName = "";
                                    String IdCourse = "";
                                    
                                    Query usrSQL = dbsession.createSQLQuery("SELECT id,course_id "
                                            + " FROM  member_course where member_id= '"+memberIdH+"'");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {
                                            
                                            System.out.println("ok");
                                            //obj = (SubjectInfo) it1.next();
                                            obj = (Object[]) it1.next();
                                            
                                            IdCourse = obj[1].toString().trim();
                                            
                                           Query q = dbsession.createSQLQuery("SELECT course_name FROM course_info where id_course= '" + IdCourse + "'");

                                            if (!q.list().isEmpty()) {
                                                CourseName = q.uniqueResult().toString();
                                            } else {
                                               // System.out.println("Do not Get Member ID of  " + username);
                                            }
                                            

                                            
                        %>
                        <div class="row">

                            <div class="col-md-8">
                                
                                 <form id="courseform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmCourse" action="<%out.print(GlobalVariable.baseUrl);%>/course/coursedetails.jsp">
                        <input type="hidden" name="courseid" id="courseid" value="<%=IdCourse%>" >

                         <input type="hidden" name="coursename" id="coursename" value="<%=CourseName%>" >
                         
                         <span><b>Subscription Course:</b> </span>
                            <a href="#">                             
                                    
                                        
                                        
                                         <button class="btn btn-outline-secondary" >
                                        <span><%out.print(CourseName);%></span>	
                                         </button>
                                    <br>
                                </a>
                    </form>
                                 </div>

                            
                        </div>
                        <%
                            }
                          }
                        %>
                    </div>
                </div>
            </div>
                    
        </div>
    </div>
</div>
<%    dbsession.flush();
    dbsession.close();


%>

<%@ include file="../footer.jsp" %>
