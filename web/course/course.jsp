<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>

<%@page import="com.appul.entity.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import="org.hibernate.*" %>

<%@ include file="../header.jsp" %>
<%
    Session dbsessionCourse = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxRegistration = dbsessionCourse.beginTransaction();

    String sessionIdH = "";
    String userNameH = "";
    String memberIdH = "";

    String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg").trim();

    String msgDispalyConT, msgInfoText, sLinkOpt;

    if (!strMsg.equals("")) {

        msgDispalyConT = "style=\"display: block; margin-top:5px;\"";

        msgInfoText = "<strong>" + strMsg + "</strong> ";

    } else {

        msgDispalyConT = "style=\"display: none;\"";

        msgInfoText = "";

    }

%>
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/css/style_v3.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/css/category.css" rel="stylesheet" type="text/css"/>
<style>
    ul.breadcrumb {
      padding: 10px 16px;
      list-style: none;
      background-color: #eee;
    }
    ul.breadcrumb li {
      display: inline;
      font-size: 14px;
    }
    ul.breadcrumb li+li:before {
      padding: 8px;
      color: black;
      content: "/\00a0";
    }
    ul.breadcrumb li a {
      color: #0275d8;
      text-decoration: none;
    }
    ul.breadcrumb li a:hover {
      color: #01447e;
      text-decoration: underline;
    }
    
    #viewContainer {
width:360px;
height:100%;
background:#00ff00;
position:absolute;
left:50%;
margin-left:-180px;
overflow:hidden; 
}

.slick-slide img {
    display: block;
}


.sr-only {
    position: absolute;
    width: 1px;
    height: 1px;
    margin: -1px;
    padding: 0;
    overflow: hidden;
    clip: rect(0,0,0,0);
    border: 0;
}

* {
    box-sizing: border-box;
}

.price-text--base-price__container--Xwk8v {
    align-items: center;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
}
.price-text--base-price__container--Xwk8v .price-text--base-price__discount--1J7vF {
    white-space: nowrap;
}
.price-text--semibold--DLyJV {
    font-weight: 700;
}
.price-text--medium--2clK9 {
    font-size: 18px;
}
.price-text--black--1qJbH {
    color: #29303b;
}

.price-text--base-price__container--Xwk8v .price-text--base-price__original--no-margin--2OrGQ {
    margin-right: 0;
}
.price-text--base-price__container--Xwk8v .price-text--base-price__original--98W0j {
    margin-right: 10px;
}
.price-text--xsmall--nWcmv {
    font-size: 13px;
}
.price-text--regular--2D_Ii {
    font-weight: 400;
}
.price-text--lighter--1OoLd {
    color: #686f7a;
}

.btn-explore {
    color: #fff;
    background-color: #007791;
    border: 1px solid transparent;
    display: inline-block;
    margin-bottom: 0;
    font-weight: 700;
    text-align: center;
    vertical-align: middle;
    touch-action: manipulation;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    white-space: nowrap;
    padding: 11px 12px;
    font-size: 15px;
    line-height: 1.35135;
    border-radius: 2px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>
<script type="text/javascript">
function AddtoCart(courseid,price,discount)
{
    //alert(courseid);
    //alert(discount);
    
        $.post("courseaddtocart.jsp", {courseid: courseid, courseprice: price, coursediscount: discount}, function (data) {

                console.log(data);

                if (data[0].responseCode == 1) {
                    console.log("success");
                    location.href = "<%out.print(GlobalVariable.baseUrl);%>/course/coursesubscription.jsp";
                     /*var url = '<%out.print(GlobalVariable.baseUrl);%>/course/coursesubscription.jsp';
                       var form = $('<form action="' + url + '" method="post">' +
                         '<input type="hidden" name="cartid" id="cartid" value="'+data[0].responseCode+'" >' +


                         //'<input type="text" name="api_url" value="' + Return_URL + '" />' +
                         '</form>');
                       $('body').append(form);
                       form.submit();
                    */
                } else {
                    //alert("Failed");
                    console.log("Failed");
                    //rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    //rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }


            }, "json");
}


function showLoginPopUp(cid,cprice,cdiscount) {
    //$(".tippy-popper").css({"visibility" : "hidden" });
    //$("#login_type").val(a);
    $("#cid").val(cid);
    $("#cprice").val(cprice);
    $("#cdiscount").val(cdiscount);
    
    $('.modal').modal('hide');
    $("#loginPopUp").modal('show');
    }

function LoginCheck()
    {
        //alert("ok");
        var user = $("#userid").val();
        var pwd = $("#password").val();
               
               
        $.post("coursesubscriptionlogin.jsp", {username: user, password:pwd }, function (data) {

                console.log(data);

                if (data[0].responseCode == 1) {
                    console.log("success");
                    AddtoCart($("#cid").val(),$("#cprice").val(),$("#cdiscount").val());
                    
                } else {
                    //alert("Failed");
                    console.log("Failed");
                    $("#errormessage").attr("style", "display: block;color: red");
                    $("#errormessage").html("");
                    $("#errormessage").append(data[0].responseMsg);
                    //rupantorLGModal.find("#rupantorLGModalBody").html('<h3>' + data[0].responseMsg + '</h3>');
                    //rupantorLGModal.find("#rupantorLGModalFooter").html("<button type=\"button\" class=\"btn btn-danger waves-effect text-left\" data-dismiss=\"modal\">Close</button>");
                }


            }, "json");

        
       
    }

</script>

<!-- course section start -->
<div class="">
<ul class="breadcrumb">
        <li><a href="http://localhost:8084/BYDOWEB">Home</a></li>
        
        <li>Course</li>
    </ul>
</div>

<!--
<section class="container-fluid py-5" style="background: url(<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/course-bg.png);background-size: 100% 100%;">

    <div class="container mb-3">

        <button type="button" class="btn btn-success btn-lg btn-block">Course List</button>

    </div>
    
    -->
                <%
                    //Session dbsessionSubject = HibernateUtil.getSessionFactory().openSession();//.openSession();

                     //org.hibernate.Transaction dbtrx = dbsessionSubject.beginTransaction();
                     
                                //SubjectInfo obj = null;
                                   int courseindex = 0;
                                   Object[] obj = null;
                                   int collapse = 1; 
                                   int childcollapse = 31;
                                   int accordion = 1;
                                   int childaccordion = 21;
                                    String CourseName = "";
                                    String CourseShortName = "";
                                    String IdCourse = "";
                                    String FeatureImage = "";
                                    String CourseDesc = "";
                                    String CourseeFee = "";
                                    String CourseED = "";
                                    String CourseTD = "";
                                    Query usrSQL = dbsessionCourse.createSQLQuery("SELECT ci.id_course ,ci.course_name,ci.course_short_name,ci.feature_image,ci.course_desc, cf.fee_amount,cf.ed,cf.td"
                                            + " FROM  course_info ci join course_fee_info cf on ci.id_course = cf.course_id ");

                                    if (!usrSQL.list().isEmpty()) {
                                        for (Iterator it1 = usrSQL.list().iterator(); it1.hasNext();) {
                                            

                                            //obj = (SubjectInfo) it1.next();
                                            obj = (Object[]) it1.next();
                                            
                                            IdCourse = obj[0].toString().trim();
                                            CourseName = obj[1].toString().trim();
                                            CourseShortName = obj[2].toString().trim();
                                            FeatureImage = obj[3].toString().trim();
                                            CourseDesc = obj[4].toString().trim();
                                            CourseeFee = obj[5].toString().trim();
                                            CourseED = obj[6].toString().trim();
                                            CourseTD = obj[7].toString().trim();
                                            if(courseindex % 2 == 0)
                                            {
                                            %> 
                                                    

            <div class="container">
                <div class="row">

            <div class="featured-course-unit-desktop--featured-course-unit-container--u2dxp">                                            

            <div class="featured-course-unit-desktop--featured-course-unit-container--u2dxp">
                 <div class="carousel-container">
            <div class="slick-slider slick-initialized" dir="ltr">               
                      <!--  <div class="slick-list">
            <div class="slick-track" style="width: 8120px; opacity: 1; transform: translate3d(-3480px, 0px, 0px);">
                -->
            <div>
                <div tabindex="-1" style="width: 100%; display: inline-block;">
                    <div class="featured-course-unit-desktop--single-course-unit-container--22x5G">
                        <div class="single-course-unit-desktop--single-course-unit-container--1FDZN">
                            <div class="single-course-unit-desktop--single-course-unit-content--1SR-P" data-purpose="single-course-unit-desktop">
                                <a href="https://www.udemy.com/course/ethical-hacking-and-network-security-applied/" class="single-course-unit-desktop--image-wrapper--3oWN1">
                                    <img alt="Hands-On Ethical Hacking and Network Security Applied 2020" style="border: 0;    vertical-align: middle;    max-width: 100%;    height: auto;    -ms-interpolation-mode: bicubic;" class="single-course-unit-desktop--course-image--2E1RF" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/2779876_a2b4_5.jpg" width="480" height="270">
                                </a>
                                <div class="single-course-unit-desktop--content-wrapper--e5Oom">
                                    <h3 class="single-course-unit-desktop--course-title--1bTyf">
                                        <a href="https://www.udemy.com/course/ethical-hacking-and-network-security-applied/"><%out.print(CourseName);%></a>
                                    </h3>
                                    <div class="single-course-unit-desktop--course-published-time--3lwBj">Last Updated May 2020</div>
                                    <!--
                                    <div class="single-course-unit-desktop--metadata-wrapper--33TCK">
                                        <span class="course-badge--course-badge--1AN7r">
                                            <span data-purpose="badge" class="badge badge-accented coral">
                                                <span class="badge-text">Hot &amp; New</span></span></span>
                                                <ul class="single-course-unit-desktop--meta-items--2H8gw"><li>13.5 total hours</li><li>151 lectures</li><li>All Levels</li></ul>
                                                <div class="single-course-unit-desktop--rating-wrapper--3Er90">
                                                    <span class="single-course-unit-desktop--star-container--1ES33">
                                                        <div aria-label="" data-purpose="star-rating-shell" class="star-rating-shell star-rating--star-rating--static--3wPvS star-rating--star-rating--tiny--2kjvX">
                                                            <div>
                                                                <span class="star-rating--review-star--Z6Nqj star-rating--review-star--unfilled--1aZxo"></span>
                                                                <span style="width: 100%;" class="star-rating--review-star--Z6Nqj star-rating--review-star--filled--2D0bO">

                                                                </span></div>
                                                            <div><span class="star-rating--review-star--Z6Nqj star-rating--review-star--unfilled--1aZxo"></span>
                                                                <span style="width: 100%;" class="star-rating--review-star--Z6Nqj star-rating--review-star--filled--2D0bO"></span></div>
                                                                <div><span class="star-rating--review-star--Z6Nqj star-rating--review-star--unfilled--1aZxo"></span>
                                                                    <span style="width: 100%;" class="star-rating--review-star--Z6Nqj star-rating--review-star--filled--2D0bO"></span></div>
                                                                    <div><span class="star-rating--review-star--Z6Nqj star-rating--review-star--unfilled--1aZxo"></span>
                                                                        <span style="width: 100%;" class="star-rating--review-star--Z6Nqj star-rating--review-star--filled--2D0bO"></span></div>
                                                                        <div><span class="star-rating--review-star--Z6Nqj star-rating--review-star--unfilled--1aZxo"></span>
                                                                            <span style="width: 74%;" class="star-rating--review-star--Z6Nqj star-rating--review-star--filled--2D0bO">

                                                                            </span></div></div>

                                                    </span>
                                                    <span class="single-course-unit-desktop--rating-value--mStjk">4.7</span>
                                                    <span class="single-course-unit-desktop--rating-count--r-KzO">(28 ratings)
                                                    </span>
                                                </div>
                                    </div>
                                    -->
                                                    <p class="single-course-unit-desktop--course-headline--XNw2j">
                                                    <%out.print(CourseDesc);%>    
                                                    </p>
                                                <!--    <a href="https://www.udemy.com/course/ethical-hacking-and-network-security-applied/" class="btn-explore">Explore course</a> -->

                                                    <form id="courseform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmCourse" action="<%out.print(GlobalVariable.baseUrl);%>/course/coursedetails.jsp">
                                                    <input type="hidden" name="courseid" id="courseid" value="<%=IdCourse%>" >

                                                     <input type="hidden" name="coursename" id="coursename" value="<%=CourseName%>" >
                                                        <a href="#">                             

                                                                     <button class="btn-explore" >


                                                                    <span>Explore Course</span>	
                                                                </button><br>
                                                            </a>
                                                    </form>
                                                        <!--
                                                            <form id="courseform" style="margin-top: 5%;" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmCourse" action="<%out.print(GlobalVariable.baseUrl);%>/course/coursesubscription.jsp">
                                                                <a href="#" ><span><i class="fa fa-shopping-cart"></i></span></a>
                                                                    <input type="hidden" name="courseid" id="courseid" value="<%=IdCourse%>" >

                                                                     <input type="hidden" name="coursename" id="coursename" value="<%=CourseName%>" >

                                                                     <input type="hidden" name="courseprice" id="courseprice" value="5000" >
                                                                        <a href="#">                             

                                                                            <button class="btn btn-outline-secondary" onclick="AddtoCart('<%=IdCourse%>','5000')">

                                                                          <span>Add  to Cart</span>	
                                                                           </button><br>
                                                                   </a>
                                                             </form> -->
                                                        <br>
                                                        <a href="#" >
                                                            <span><i class="fa fa-shopping-cart"></i></span></a>
                                                                            
                                                                   <a href="#">                             

                                                                       <%
                                                                           if(session.getAttribute("username") != null && session.getAttribute("memberId") != null)
                                                                            {
                                                                       %>
                                                                           <button class="btn btn-outline-secondary" onclick="AddtoCart('<%=IdCourse%>','<%=CourseeFee%>','0')"> <!-- style="background-color: #149ab0; color:white;  font-weight: bold;" --> 

                                                                           <span>Add  to Cart</span>	
                                                                           </button>
                                                                           <%
                                                                            }
                                                                            else
                                                                            {
                                                                           %>
                                                                           
                                                                            <button class="btn btn-outline-secondary" onclick="showLoginPopUp('<%=IdCourse%>','<%=CourseeFee%>','0')"> <!-- style="background-color: #149ab0; color:white;  font-weight: bold;" --> 

                                                                           <span>Add  to Cart</span>	
                                                                           </button>
                                                                           <%
                                                                            }
                                                                           %>
                                                                           <br>
                                                                   </a>

                                                    <div class="single-course-unit-desktop--price-wrapper--hpxBb">




                                                        <div class="price-text-container price-text--base-price__container--Xwk8v" data-purpose="price-text-container">
                                                            <div class="course-price-text price-text--base-price__discount--1J7vF price-text--black--1qJbH price-text--medium--2clK9 price-text--semibold--DLyJV" data-purpose="course-price-text">
                                                                <span class="sr-only">Current price</span>
                                                                <span><span>$9.99</span></span></div> &nbsp;&nbsp;
                                                            <div class="original-price-container price-text--base-price__original--98W0j price-text--lighter--1OoLd price-text--xsmall--nWcmv price-text--regular--2D_Ii price-text--base-price__original--no-margin--2OrGQ" data-purpose="original-price-container">
                                                                <div data-purpose="course-old-price-text">
                                                                    <span class="sr-only">Original Price</span>
                                                                    <span><s><span>$199.99</span></s></span></div></div></div></div></div>
                                <div class="single-course-unit-desktop--wishlist-button-container--3JjA0">
                                    <button id="wishlist-button--19" title="Add to Wishlist"  type="button" class="wishlist-button--style-primary--3DnBS wishlist-button--wishlist-btn--3Xy6s wishlist-button--size-sm--7bM1M btn btn-sm btn-link">
                                        <span class="sr-only">Wishlist</span>
                                        <span class="wishlist-button--wish-icon--XnBJz udi udi-heart">

                                        </span>
                                    </button>
                                </div>
                            </div>
                                <span style="font-size: 0px;">

                                </span>
                        </div>
                    </div>
                </div>
            </div>                                            

                                    <!--
            </div>
                        </div>  -->
            </div>
                                         </div>
                                    </div>




            </div>


            </div>                                                                    

                                    </div>
                                              <%
                                             }
                                            else
                                            {
                                              %>
                                              
                                              
                                              <div class="container">
                <div class="row">

            <div class="featured-course-unit-desktop--featured-course-unit-container--u2dxp">                                            

            <div class="featured-course-unit-desktop--featured-course-unit-container--u2dxp">
                 <div class="carousel-container">
            <div class="slick-slider slick-initialized" dir="ltr">               
                      <!--  <div class="slick-list">
            <div class="slick-track" style="width: 8120px; opacity: 1; transform: translate3d(-3480px, 0px, 0px);">
                -->
            <div>
                <div tabindex="-1" style="width: 100%; display: inline-block;">
                    <div class="featured-course-unit-desktop--single-course-unit-container--22x5G">
                        <div class="single-course-unit-desktop--single-course-unit-container--1FDZN">
                            <div class="single-course-unit-desktop--single-course-unit-content--1SR-P" data-purpose="single-course-unit-desktop">
                                
                                <div class="single-course-unit-desktop--content-wrapper--e5Oom">
                                    <h3 class="single-course-unit-desktop--course-title--1bTyf">
                                        <a href="https://www.udemy.com/course/ethical-hacking-and-network-security-applied/"><%out.print(CourseName);%></a>
                                    </h3>
                                    <div class="single-course-unit-desktop--course-published-time--3lwBj">Last Updated May 2020</div>
                                    <!--
                                    <div class="single-course-unit-desktop--metadata-wrapper--33TCK">
                                        <span class="course-badge--course-badge--1AN7r">
                                            <span data-purpose="badge" class="badge badge-accented coral">
                                                <span class="badge-text">Hot &amp; New</span></span></span>
                                                <ul class="single-course-unit-desktop--meta-items--2H8gw"><li>13.5 total hours</li><li>151 lectures</li><li>All Levels</li></ul>
                                                <div class="single-course-unit-desktop--rating-wrapper--3Er90">
                                                    <span class="single-course-unit-desktop--star-container--1ES33">
                                                        <div aria-label="" data-purpose="star-rating-shell" class="star-rating-shell star-rating--star-rating--static--3wPvS star-rating--star-rating--tiny--2kjvX">
                                                            <div>
                                                                <span class="star-rating--review-star--Z6Nqj star-rating--review-star--unfilled--1aZxo"></span>
                                                                <span style="width: 100%;" class="star-rating--review-star--Z6Nqj star-rating--review-star--filled--2D0bO">

                                                                </span></div>
                                                            <div><span class="star-rating--review-star--Z6Nqj star-rating--review-star--unfilled--1aZxo"></span>
                                                                <span style="width: 100%;" class="star-rating--review-star--Z6Nqj star-rating--review-star--filled--2D0bO"></span></div>
                                                                <div><span class="star-rating--review-star--Z6Nqj star-rating--review-star--unfilled--1aZxo"></span>
                                                                    <span style="width: 100%;" class="star-rating--review-star--Z6Nqj star-rating--review-star--filled--2D0bO"></span></div>
                                                                    <div><span class="star-rating--review-star--Z6Nqj star-rating--review-star--unfilled--1aZxo"></span>
                                                                        <span style="width: 100%;" class="star-rating--review-star--Z6Nqj star-rating--review-star--filled--2D0bO"></span></div>
                                                                        <div><span class="star-rating--review-star--Z6Nqj star-rating--review-star--unfilled--1aZxo"></span>
                                                                            <span style="width: 74%;" class="star-rating--review-star--Z6Nqj star-rating--review-star--filled--2D0bO">

                                                                            </span></div></div>

                                                    </span>
                                                    <span class="single-course-unit-desktop--rating-value--mStjk">4.7</span>
                                                    <span class="single-course-unit-desktop--rating-count--r-KzO">(28 ratings)
                                                    </span>
                                                </div>
                                    </div>
                                    -->
                                                    <p class="single-course-unit-desktop--course-headline--XNw2j">
                                                    <%out.print(CourseDesc);%>    
                                                    </p>
                                                <!--    <a href="https://www.udemy.com/course/ethical-hacking-and-network-security-applied/" class="btn-explore">Explore course</a> -->

                                                    <form id="courseform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmCourse" action="<%out.print(GlobalVariable.baseUrl);%>/course/coursedetails.jsp">
                                                    <input type="hidden" name="courseid" id="courseid" value="<%=IdCourse%>" >

                                                     <input type="hidden" name="coursename" id="coursename" value="<%=CourseName%>" >
                                                        <a href="#">                             

                                                                     <button class="btn-explore" >


                                                                    <span>Explore Course</span>	
                                                                </button><br>
                                                            </a>
                                                    </form>
                                                        <br>
                                                          <!--  <form id="courseform" style="margin-top: 5%;" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmCourse" action="<%out.print(GlobalVariable.baseUrl);%>/course/coursesubscription.jsp">
                                                                <a href="#" ><span><i class="fa fa-shopping-cart"></i></span></a>
                                                                    <input type="hidden" name="courseid" id="courseid" value="<%=IdCourse%>" >

                                                                     <input type="hidden" name="coursename" id="coursename" value="<%=CourseName%>" >

                                                                     <input type="hidden" name="courseprice" id="courseprice" value="5000" >
                                                                        <a href="#">                             

                                                                            <button class="btn btn-outline-secondary"> 

                                                                          <span>Add  to Cart</span>	
                                                                           </button><br>
                                                                   </a>
                                                             </form> -->
                                                        
                                                        
                                                        <a href="#" >
                                                            <span><i class="fa fa-shopping-cart"></i></span></a>
                                                                            
                                                                   <a href="#">                             

                                                                       <%
                                                                           if(session.getAttribute("username") != null && session.getAttribute("memberId") != null)
                                                                            {
                                                                       %>
                                                                           <button class="btn btn-outline-secondary" onclick="AddtoCart('<%=IdCourse%>','<%=CourseeFee%>','0')"> <!-- style="background-color: #149ab0; color:white;  font-weight: bold;" --> 

                                                                           <span>Add  to Cart</span>	
                                                                           </button>
                                                                           <%
                                                                            }
                                                                            else
                                                                            {
                                                                           %>
                                                                           
                                                                            <button class="btn btn-outline-secondary" onclick="showLoginPopUp('<%=IdCourse%>','<%=CourseeFee%>','0')"> <!-- style="background-color: #149ab0; color:white;  font-weight: bold;" --> 

                                                                           <span>Add  to Cart</span>	
                                                                           </button>
                                                                           <%
                                                                            }
                                                                           %>
                                                                           <br>
                                                                   </a>


                                                    <div class="single-course-unit-desktop--price-wrapper--hpxBb" style="right:480px;">




                                                        <div class="price-text-container price-text--base-price__container--Xwk8v" data-purpose="price-text-container">
                                                            <div class="course-price-text price-text--base-price__discount--1J7vF price-text--black--1qJbH price-text--medium--2clK9 price-text--semibold--DLyJV" data-purpose="course-price-text">
                                                                <span class="sr-only">Current price</span>
                                                                <span><span>$9.99</span></span></div> &nbsp;&nbsp;
                                                            <div class="original-price-container price-text--base-price__original--98W0j price-text--lighter--1OoLd price-text--xsmall--nWcmv price-text--regular--2D_Ii price-text--base-price__original--no-margin--2OrGQ" data-purpose="original-price-container">
                                                                <div data-purpose="course-old-price-text">
                                                                    <span class="sr-only">Original Price</span>
                                                                    <span><s><span>$199.99</span></s></span></div></div></div></div></div>
                                <a href="https://www.udemy.com/course/ethical-hacking-and-network-security-applied/" class="single-course-unit-desktop--image-wrapper--3oWN1">
                                <img alt="Hands-On Ethical Hacking and Network Security Applied 2020" style="border: 0;    vertical-align: middle;    max-width: 100%;    height: auto;    -ms-interpolation-mode: bicubic;" class="single-course-unit-desktop--course-image--2E1RF" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/2779876_a2b4_5.jpg" width="480" height="270">
                                </a>
                                <div class="single-course-unit-desktop--wishlist-button-container--3JjA0">
                                    <button id="wishlist-button--19" title="Add to Wishlist" type="button" class="wishlist-button--style-primary--3DnBS wishlist-button--wishlist-btn--3Xy6s wishlist-button--size-sm--7bM1M btn btn-sm btn-link">
                                        <span class="sr-only">Wishlist</span>
                                        <span class="wishlist-button--wish-icon--XnBJz udi udi-heart">

                                        </span>
                                    </button>
                                </div>
                            </div>
                                <span style="font-size: 0px;">

                                </span>
                        </div>
                    </div>
                </div>
            </div>                                            

                                    <!--
            </div>
                        </div>  -->
            </div>
                                         </div>
                                    </div>




            </div>


            </div>                                                                    

                                    </div>
<!--                                            
                                            

                           <!--                                            
       <div class="container bg-white py-5 phy mt-4">
    <div class="row">
        <div class="col-md-6 pb-sm-3 d-flex justify-content-sm-center sfx4">
         <img class="rounded-circle course2 sfx5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/<%out.print(FeatureImage);%>" width="350" height="350"> 
                    </div>

        <div class="col-md-6">
            <div class="container">
                <span><%out.print(CourseName);%></span>	<br>
                <span><i class="fa fa-dollar"></i>5000</span><br>
                <span><%out.print(CourseDesc);%></span>	<br>
                
                
                    <form id="courseform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmCourse" action="<%out.print(GlobalVariable.baseUrl);%>/course/coursesubscription.jsp">
                       <a href="#"><span><i class="fa fa-shopping-cart"></i></span></a>
                        <input type="hidden" name="courseid" id="courseid" value="<%=IdCourse%>" >

                         <input type="hidden" name="coursename" id="coursename" value="<%=CourseName%>" >
                         
                         <input type="hidden" name="courseprice" id="courseprice" value="5000" >
                            <a href="#">                             
                                    
                                         <button class="btn btn-outline-secondary" style="background-color: #149ab0; color:white;  font-weight: bold;">
                                        
                                        
                                        <span>Add  to Cart</span>	
                                    </button><br>
                                </a>
                    </form>
                 
                
                <br><br>
            <div>
                    <form id="courseform" class="form-horizontal form-material" method="POST" autocomplete="off" name="frmCourse" action="<%out.print(GlobalVariable.baseUrl);%>/course/coursedetails.jsp">
                        <input type="hidden" name="courseid" id="courseid" value="<%=IdCourse%>" >

                         <input type="hidden" name="coursename" id="coursename" value="<%=CourseName%>" >
                            <a href="#">                             
                                    
                                         <button class="btn btn-outline-secondary" >
                                        
                                        
                                        <span>Explore Course</span>	
                                    </button><br>
                                </a>
                    </form>
                </div>    
            </div>
        </div>
    </div>
</div>
                            
                            -->
                                            <%
                                                }
                                            courseindex++;
                                            //accordion++;
                                        }
                                    }


                                    

                    %>
                    <%--  
    
    <div class="container bg-white py-5 phy">
        <div class="row">
            <div class="col-md-6 pb-sm-3 d-flex justify-content-sm-center sfx4">
                <img class="rounded-circle sfx5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/math.jpg" width="350" height="350">
            </div>
            <div class="col-md-6">
                <div class="container">

                    <div id="accordion">
                        <div class="card">
                            <div class="card-header course1">
                                <a class="card-link text-white" data-toggle="collapse" href="#collapseOne">
                                    <span>Mathematics 1</span>
                                    <i class="fa fa-chevron-down fa-lg float-right"></i>
                                </a>
                            </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                <div class="card-body">

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span>	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header course1">
                                <a class="collapsed card-link text-white" data-toggle="collapse" href="#collapseTwo">
                                    <span>Mathematics 2</span>
                                    <i class="fa fa-chevron-down fa-lg float-right"></i>
                                </a>
                            </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="card-body">

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                            <button class="btn btn-outline-secondary">
                                                <i class="fa fa-play-circle fa-2x align-middle"></i>
                                                <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                                <span>[Permutation and Combination]</span>	
                                            </button><br>
                                        </a>

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                            <button class="btn btn-outline-secondary mt-2">
                                                <i class="fa fa-play-circle fa-2x align-middle"></i>
                                                <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                                <span>[Permutation and Combination]</span> 	
                                            </button><br>
                                        </a>

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                            <button class="btn btn-outline-secondary mt-2">
                                                <i class="fa fa-play-circle fa-2x align-middle"></i>
                                                <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                                <span>[Permutation and Combination]</span> 	
                                            </button><br>
                                        </a>

                                        <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                            <button class="btn btn-outline-secondary mt-2">
                                                <i class="fa fa-play-circle fa-2x align-middle"></i>
                                                <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                                <span>[Permutation and Combination]</span> 	
                                            </button><br>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<!--</div>-->

<div class="container bg-white py-5 phy mt-4">
    <div class="row">
        <div class="col-md-6 pb-sm-3 d-flex justify-content-sm-center sfx4">
            <img class="rounded-circle sfx5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/physics.png" width="350" height="350">
        </div>

        <div class="col-md-6">
            <div class="container">

                <div id="accordion">
                    <div class="card">
                        <div class="card-header course1">
                            <a class="card-link text-white" data-toggle="collapse" href="#collapseOne">
                                <span>Mathematics 1</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span>	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header course1">
                            <a class="collapsed card-link text-white" data-toggle="collapse" href="#collapseTwo">
                                <span>Mathematics 2</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <div class="card-body">

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span>	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="container bg-white py-5 phy mt-4">
    <div class="row">
        <div class="col-md-6 pb-sm-3 d-flex justify-content-sm-center sfx4">
            <img class="rounded-circle course2 sfx5" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/course/chemistry.png" width="350" height="350">
        </div>

        <div class="col-md-6">
            <div class="container">

                <div id="accordion">
                    <div class="card">
                        <div class="card-header course1">
                            <a class="card-link text-white" data-toggle="collapse" href="#collapseOne">
                                <span>Mathematics 1</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span>	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                                <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                    <button class="btn btn-outline-secondary mt-2">
                                        <i class="fa fa-play-circle fa-2x align-middle"></i>
                                        <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                        <span>[Permutation and Combination]</span> 	
                                    </button><br>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header course1">
                            <a class="collapsed card-link text-white" data-toggle="collapse" href="#collapseTwo">
                                <span>Mathematics 2</span>
                                <i class="fa fa-chevron-down fa-lg float-right"></i>
                            </a>
                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <div class="card-body">

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span>	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                    <a href="<%out.print(GlobalVariable.baseUrl);%>/course/classroom.jsp">
                                        <button class="btn btn-outline-secondary mt-2">
                                            <i class="fa fa-play-circle fa-2x align-middle"></i>
                                            <span>??? ??????? ? ?????? (????? ???????)</span><br>
                                            <span>[Permutation and Combination]</span> 	
                                        </button><br>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
                                  
</section>
--%>      

<div class="modal fade lgnModal" id="loginPopUp" style="left: -70px;">
    <div class="modal-dialog" style="max-width: 450px;margin: auto;margin-top: 10px;">
    <div class="modal-content">
        <div class="modal-header">
            <img src="Eduonix%20Cart_files/logo.webp" alt="logo" class="img-responsive logocommon"><button type="button" class="close" data-dismiss="modal">�</button>        </div>
        <div class="divLgnTxt position-relative">
            <table class="tableCommon">
                <tbody><tr>
                    <td class="position-relative" width="40%">
                        <div class="tableCommonTd borderLeft"> </div>
                    </td>
                    <td class="text-center lgnTxt" width="20%">
                        Login
                    </td>
                    <td class="position-relative" width="40%">
                        <div class="tableCommonTd borderRgt"> </div>
                    </td>
                </tr></tbody>
            </table>
        </div>
              <!--  <div class="row pt-4 mx-4 mb-1">
            <div class="col-sm-6 text-center p-0">
                <div class="socialSignUps" data-type="google" onclick="set_session('https://accounts.google.com/o/oauth2/auth?response_type=code&amp;redirect_uri=http%3A%2F%2Fwww.eduonix.com%2Flogin%2Fstate&amp;client_id=122036978168-7kvqqf1ij00onk4blefak4ql88abnm72.apps.googleusercontent.com&amp;scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me&amp;access_type=offline&amp;approval_prompt=auto')">
                    <i class="fab fa-google" style="margin-right: 16px;"></i>
                    <span>Google</span>
                </div>
            </div>
            <div class="col-sm-6 text-center p-0">
                <div class="socialSignUps" data-type="facebook" onclick="fblogin();">
                    <i class="fab fa-facebook-f" style="margin-right: 16px;"></i>
                    <span>Facebook</span>
                </div>
            </div>
        </div>-->
        
        
        

      <div class="modal-body text-center divModalBody">
          <span id="errormessage" style="display: none;color: red"></span>
          <br>
       <input type="hidden" value="ca10886fc75df80e097cfd63bc55f56203053beefa04038eb5239a8a5343af3c" id="login_csrf_token">
        <div class="position-relative">
            <input id="userid" name="userid"   class="form-control lgnemail" required="*" placeholder="Userid">
            <span class="fas fa-envelope lgnIcn"></span>   
        </div>
        <div id="errInput" class="errorMsg text-left"></div>
        
        <div class="position-relative">
            <input  id="password" name="password" type="password" required="*" class="form-control mb-0 lgnpass" placeholder="Password">
            <span class="fas fa-lock lgnIcn"></span>
        </div>
        <div id="errpw" class="errorMsg text-left"></div>
        <div class="text-left divSec mt-3">
            <div class="form-group form-check divRem" style="padding-left: 1rem;">
                <label class="form-check-label cursPt">
                <input class="form-check-input cursPt lgnrem" id="remember_me" type="checkbox" style="vertical-align: middle;"> Remember me
                </label>
            </div>
            <div style="display: inline-block;float: right;">
                 
                    <a href="#" data-toggle="modal" data-target="#frgotpassModal" class="lblForgotPass" onclick="showFgtPwdDiv('loginPopUp', 'forget', '');">Forgot Password?</a>
                            </div>
                    </div>
        
                   <input type="hidden"  id="cid"  >

                  <input type="hidden"  id="cdiscount"  >

                  <input type="hidden"   id="cprice"  >

        <div class="mt-3">
            <button class="btn btn-sign-main lgbtnldr position-relative loginBtn" onclick="LoginCheck()"><span class="spnLoader position-absolute t-0">
                
            </span>Log in</button>            <div class="errmsg" style="padding: 10px 0; font-size: .9em;"></div>
            <input type="hidden" class="" value="" id="set_email">
        </div>
        <div>
            <label>Not a Member? <b><a href="#" onclick="showSignUpPopUp('redirection', 'loginPopUp', 'signUpPopUp');">Sign up</a></b></label>
        </div>
        </div>
    </div>
  </div>
  </div>
    


<%    dbsessionCourse.flush();

    dbsessionCourse.close();
    



%>

<%@ include file="../footer.jsp" %>
