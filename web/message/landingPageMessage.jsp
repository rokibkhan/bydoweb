<%@page import="org.hibernate.Session"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.appul.util.GlobalVariable"%>

<%
    Session dbsessionMsgLandingPage = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxMsgLandingPage = dbsessionMsgLandingPage.beginTransaction();

    Query messageSQL = null;
    Object messageObj[] = null;
    String messageId = "";
    String messageName = "";
    String messageTitle = "";
    String messageShortDetails = "";
    String messageShortDetails1 = "";
    String messageShortDetailsX = "";
    String messageDetails = "";
    String messageDateTime = "";
    String messageFeatureImage = "";
    String messageFeatureImage1 = "";
    String messageFeatureImageUrl = "";
    String messageInfoPictureContainer = "";
    String messageInfoContainer = "";
    String messageInfoContainer1 = "";
    String messageDetailsUrl = "";
    String messageDetailsPresident = "";
    String messageDetailsSecretary = "";
    int mf = 1;

    messageSQL = dbsessionMsgLandingPage.createSQLQuery("SELECT mf.id_message, mf.message_name,mf.message_title, mf.message_short_desc, mf.message_desc, "
            + "mf.feature_image, mf.published, mf.add_user,mf.add_date  "
            + " FROM  message_from mf ORDER BY mf.showing_order ASC LIMIT 0,2");

    if (!messageSQL.list().isEmpty()) {
        for (Iterator it1 = messageSQL.list().iterator(); it1.hasNext();) {

            messageObj = (Object[]) it1.next();
            messageId = messageObj[0].toString().trim();
            messageName = messageObj[1].toString().trim();
            messageTitle = messageObj[2].toString().trim();
            messageShortDetails = messageObj[3].toString().trim();

            //  messageShortDetailsX = messageShortDetails.substring(0, 250);
            messageDetails = messageObj[4].toString().trim();

            messageFeatureImage = messageObj[5] == null ? "" : messageObj[5].toString().trim();

            //  messageFeatureImageUrl = GlobalVariable.imageDirLink + messageFeatureImage;
            messageFeatureImageUrl = GlobalVariable.baseUrl + "/upload/" + messageFeatureImage;

            messageDetailsUrl = GlobalVariable.baseUrl + "/message/messageDetails.jsp?messageIdX=" + messageId;
            messageDetailsPresident = GlobalVariable.baseUrl + "/message/messageFromPresident.jsp?messageIdX=" + messageId;
            messageDetailsSecretary = GlobalVariable.baseUrl + "/message/messageFromSecretary.jsp?messageIdX=" + messageId;

            if (mf == 1) {
                messageInfoContainer = messageInfoContainer + ""
                        + "<section class=\"spotlight\">"
                        + "<div class=\"image1\" style=\"margin: 0 0 0 3em;border-radius: 2%;box-shadow: 1px 1px 11px 0px #888888; display: block;\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/executive_img_1.jpg\" alt=\"\" /></div>"
                        + "<div class=\"content\">"
                        + "<blockquote style=\"box-shadow: 1px 1px 11px 0px #888888; border-radius: 10px;\">"
                        + "<h3 style=\"font-size: 1.45rem; margin-bottom:10px;\">Message from " + messageTitle + "</h3>"
                        + "<p>The Institution of Engineers, Bangladesh (IEB) is the most prestigious "
                        + "National Professional Organization of the country. It is registered under "
                        + "the Societies Registration Act of the country. IEB includes all "
                        + "disciplines of engineering. Currently, it has in its roll more than "
                        + "41,545 engineers with about 30% in the category of Fellows, "
                        + "60% Members and the rest as Associate Members.</p>"
                        + "... <a href=\"" + messageDetailsPresident + "\">more details</a>"
                        + "<span>" + messageName + "</span>"
                        + "</blockquote>"
                        + "</div>"
                        + "</section>";
            }
            if (mf == 2) {

                messageInfoContainer = messageInfoContainer + ""
                        + "<section class=\"spotlight\">"
                        + "<div class=\"image1\" style=\"margin: 0 3em 0 0;border-radius: 2%; box-shadow: 1px 1px 11px 0px #888888; display: block;\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/executive_img_7.jpg\" alt=\"\" /></div>"
                        + "<div class=\"content\">"
                        + "<blockquote style=\"box-shadow: 1px 1px 11px 0px #888888; border-radius: 10px;\">"
                        + "<h3 style=\"font-size: 1.45rem; margin-bottom:10px;\">Message from " + messageTitle + "</h3>"
                        + "<p>The Institution of Engineers, Bangladesh (IEB) is the most prestigious National Professional "
                        + "Organization of the country. It is registered under the Societies Registration Act of the country. "
                        + "IEB has in its roll more than 41,545 "
                        + "engineers with about 30% in the category of Fellows,60% Members  and the rest as Associate Members.</p>"                        
                        + "... <a href=\" " + messageDetailsSecretary + " \">more details</a>"
                        + "<span>" + messageName + "</span>"
                        + "</blockquote>"
                        + "</div>"
                        + "</section>";

            }

//            messageInfoContainer = messageInfoContainer + ""
//                    + "<div class=\"col-md-3 text-center\">"
//                    + "<a href=\"" + messageDetailsUrl + "\"><img src=\"" + messageFeatureImageUrl + "\" class=\"img-fluid rounded-circle mb-2\" style=\"border-radius: 60%;border: solid 5px #fff;\" width=\"100\" alt=\"" + messageName + "\" />"
//                    + "<h5 class=\"text-center text-dark\" style=\"font-size:16px; margin-bottom:12px; margin-bottom:7px;color:#333\">" + messageName + "</h5>"
//                    + "<p class=\"member_title1 text-center text-dark\" style=\"font-size:14px;color:#666\">" + messageTitle + "</p><a>"
//                    + "<div class=\"member_details_wrap\">"
//                    + "</div>"
//                    + "</div>";
            mf++;
        }
    }

    dbsessionMsgLandingPage.clear();
    dbsessionMsgLandingPage.close();
%> 

<!-- start committee_message_wrap -->
<section class="wrapper">
    <div class="row">
        <div class="col-12">
            <h2 class="message_tiele1 text-center" style="padding-bottom: 27px; font: 27px; color: #333333;font-family: 'Playfair Display', serif; ">Message</h2>
            <hr align="center" width="10%">
        </div>
    </div>
    <div class="inner alt" style="margin-top: 50px">
        
      

        <section class="spotlight">
            <div class="image"><img src="<%=GlobalVariable.baseUrl%>/commonUtil/assets/images/executive_img_1.jpg" alt="" /></div>
            <div class="content">
                <blockquote style="border-radius: 10px;">
                    <h3>Message from President</h3>
                    <p>The Institution of Engineers, Bangladesh (IEB) is the most prestigious 
                        National Professional Organization of the country. 
                        It is registered under the Societies Registration Act of the country. 
                        IEB includes all disciplines of engineering. Currently, 
                        it has in its roll more than 41,545 engineers with about 
                        30% in the category of Fellows, 60% Members.
                        
                    </p>
                    <p><a href="<%=messageDetailsPresident%>">more details</a></p>
                    <span>Engr Md. Abdus Sabur</span>
                </blockquote>
            </div>
        </section>

        <section class="spotlight">
            <div class="image"><img src="<%=GlobalVariable.baseUrl%>/commonUtil/assets/images/executive_img_7.jpg" alt="" /></div>
            <div class="content">
                <blockquote style="border-radius: 10px;">
                    <h3>Message from Honorary General Secretary</h3>
                    <p>The Institution of Engineers, Bangladesh (IEB) 
                        is the most prestigious National Professional Organization 
                        of the country. It is registered under the 
                        Societies Registration Act of the country. 
                        IEB includes all disciplines of engineering. 
                        Currently, it has in its roll more than 41,545 engineers                    
                    </p>
                    <p><a href="<%=messageDetailsSecretary%>">more details</a></p>
                    <span>Engr. Khandkar Manjur Morshed</span>
                </blockquote>
            </div>
        </section>
</section>
<!-- end committee_message_wrap -->