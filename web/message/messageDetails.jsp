<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Query"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="../header.jsp" %>

<%
    Session dbsessionMessage = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxMessage = dbsessionMessage.beginTransaction();

    String messageIdX = request.getParameter("messageIdX") == null ? "" : request.getParameter("messageIdX").trim();

    Query messageSQL = null;
    Object messageObj[] = null;
    String messageId = "";
    String messageName = "";
    String messageTitle = "";
    String messageShortDetails = "";
    String messageShortDetails1 = "";
    String messageShortDetailsX = "";
    String messageDetails = "";
    String messageDateTime = "";
    String messageFeatureImage = "";
    String messageFeatureImage1 = "";
    String messageFeatureImageUrl = "";
    String messageInfoPictureContainer = "";
    String messageInfoContainer = "";
    String messageInfoContainer1 = "";
    String messageDetailsUrl = "";
    int mf = 1;

    messageSQL = dbsessionMessage.createSQLQuery("SELECT mf.id_message, mf.message_name,mf.message_title, mf.message_short_desc, mf.message_desc, "
            + "mf.feature_image, mf.published, mf.add_user,mf.add_date  "
            + " FROM  message_from mf ORDER BY mf.showing_order ASC LIMIT 0,4");

    if (!messageSQL.list().isEmpty()) {
        for (Iterator it1 = messageSQL.list().iterator(); it1.hasNext();) {

            messageObj = (Object[]) it1.next();
            messageId = messageObj[0].toString().trim();
            messageName = messageObj[1].toString().trim();
            messageTitle = messageObj[2].toString().trim();
            messageShortDetails = messageObj[3].toString().trim();

            //  messageShortDetailsX = messageShortDetails.substring(0, 250);
            messageDetails = messageObj[4].toString().trim();

            messageFeatureImage = messageObj[5] == null ? "" : messageObj[5].toString().trim();

            //  messageFeatureImageUrl = GlobalVariable.imageDirLink + messageFeatureImage;
            messageFeatureImageUrl = GlobalVariable.baseUrl + "/upload/" + messageFeatureImage;

            //  messageFeatureImageUrl = GlobalVariable.imageDirLink + messageFeatureImage;
            messageFeatureImage1 = "<img width=\"200\" src=\"" + messageFeatureImageUrl + "\" alt=\"" + messageTitle + "\">";

            messageDetailsUrl = GlobalVariable.baseUrl + "/message/messageDetails.jsp?messageIdX=" + messageId;

            messageInfoContainer = messageInfoContainer + ""
                    + "<div class=\"col-md-3 text-center\">"
                    + "<a href=\"" + messageDetailsUrl + "\"><img src=\"" + messageFeatureImageUrl + "\" class=\"img-fluid rounded-circle mb-2\" width=\"180\" alt=\"" + messageName + "\" />"
                    + "<h4 class=\"text-center text-white\">" + messageName + "</h4>"
                    + "<p class=\"member_title text-center text-white\">" + messageTitle + "</p><a>"
                    + "<div class=\"member_details_wrap\">"
                    + "</div>"
                    + "</div>";

        }
    }

%> 


<section>

    <nav aria-label="breadcrumb"> 
        <div class="container">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Message</a></li>
            </ol>
        </div>
    </nav>
</section>

<!-- Login & Register & About CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about.css" rel="stylesheet">

<!-- News front image section -->
<!--<section class="news-banner">
    <div class="conainer">
        <img src="<%//out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/success_story_img_1.jpg" class="img-fluid banner-img" alt="Front Image">
    </div>
</section>-->
<!-- News front image section -->

<!-- Start of details of news -->
<section class="news-single-content pt-0">
    <div class="container">
        <h2 class="display-41 text-center">Message</h2>
        <div class="row">
            <div class="col-12">
                <div class="news-content">
                    <div class="media mt-5">
                        <div class="text-center mr-5">
                            <img class="mr-3 rounded-circle" src="<%=messageFeatureImageUrl%>" alt="Generic placeholder image">
                            <h5 class="mt-3"> <%=messageName%> </h5>
                            <p class="mt-3"> <%=messageTitle%> </p>
                            <div class="custom-horizontal-border"></div>
                        </div>

                        <div class="media-body">
                            <%=messageDetails%>
                        </div>
                    </div>
                    <hr />

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of details of news -->

<!-- Start of more news section -->
<section>
    <div class="container">
        <div class="post-heading pt-5">
            <h3 class="text-uppercase font-weight-bold">More News </h3>
            <hr class="custom-horizontal-border mr-5" />
        </div>
        <div class="row mb-5">
            <div class="col-md-3 col-sm-6">
                <div class="post-content pt-3">
                    <div class="post-content-img">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/event_img_1.jpg" alt="Post Image" class="img-fluid news-image  mb-3" />
                        <div class="post-content-img-overlay d-flex align-items-center justify-content-center"></div>
                        <a href="#" class="text-primary font-weight-bold news-link">Regulate the professional</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="post-content pt-3">
                    <div class="post-content-img">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_img_1.jpg" alt="Post Image" class="img-fluid news-image  mb-3" />
                        <div class="post-content-img-overlay d-flex align-items-center justify-content-center"></div>
                        <a href="#" class="text-primary font-weight-bold news-link">Regulate the professional</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="post-content pt-3">
                    <div class="post-content-img">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_img_3.jpg" alt="Post Image" class="img-fluid news-image mb-3" />
                        <div class="post-content-img-overlay d-flex align-items-center justify-content-center"></div><br />
                        <a href="#" class="text-primary font-weight-bold news-link">Regulate the professional</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="post-content pt-3">
                    <div class="post-content-img">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_img_2.jpg" alt="Post Image" class="img-fluid news-image  mb-3" />
                        <div class="post-content-img-overlay"></div>
                        <a href="#" class="text-primary font-weight-bold news-link">Regulate the professional</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of more news section -->
<%

    dbsessionMessage.clear();
    dbsessionMessage.close();
%>

<%@ include file="../footer.jsp" %>
