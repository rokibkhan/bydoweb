<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<!-- News & Events & Single Event CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/news_events_style.css" rel="stylesheet">

<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    // DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateFormatEventM = new SimpleDateFormat("MMMM"); //October
    DateFormat dateFormatEventMM = new SimpleDateFormat("MMM"); //Oct
    DateFormat dateFormatEventD = new SimpleDateFormat("dd");
    DateFormat dateFormatEventY = new SimpleDateFormat("Y");
    Date dateEventM = null;
    Date dateEventMM = null;
    Date dateEventD = null;
    Date dateEventY = null;
    String newDateEventM = "";
    String newDateEventMM = "";
    String newDateEventD = "";
    String newDateEventY = "";

    Date todayDate = new Date();
    String today = dateFormatEvent.format(todayDate); //2016-11-16

    Query eventSQL = null;
    Object eventObj[] = null;
    String eventId = "";
    String eventTitle = "";
    String eventDuration = "";
    String eventVenue = "";
    String eventDurationVanue = "";
    String eventShortDetails = "";
    String eventShortDetails1 = "";
    String eventShortDetailsX = "";
    String eventDetails = "";
    String eventDetails1 = "";
    String eventDetailsX = "";
    String eventDateTime = "";
    String eventMonth = "";
    String eventDay = "";
    String eventYear = "";
    String eventFeatureImage = "";
    String eventFeatureImage1 = "";
    String eventFeatureImageUrl = "";
    String topEventContainer = "";
    String topConClass = "";
    String topConClass1 = "";
    String topConClass2 = "";
    String topConClass3 = "";
    String bottomEventContainer = "";
    String topMarginClass = "";

    String eventInfoFirstContainer = "";
    String eventInfoContainer = "";
    String eventDetailsUrl = "";
    int ev = 1;

    String eventCategory = "";
    String keyNotesSpeakerConterner = "";

    String upcomingEventBtn = "";

    eventSQL = dbsession.createSQLQuery("SELECT ni.id_event, ni.event_title, ni.event_short_desc, ni.event_desc, "
            + "ni.event_date, ni.event_duration ,ni.event_venue, "
            + "ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4,ni.event_category "
            + "FROM  event_info ni "
            + "WHERE ni.event_category = '1' "
            + "ORDER BY ni.id_event DESC LIMIT 0,10");

    if (!eventSQL.list().isEmpty()) {
        for (Iterator it1 = eventSQL.list().iterator(); it1.hasNext();) {

            eventObj = (Object[]) it1.next();
            eventId = eventObj[0].toString().trim();
            eventTitle = eventObj[1].toString().trim();
            eventShortDetails = eventObj[2].toString().trim();

            //  eventShortDetailsX = eventShortDetails.substring(0, 250);
            eventShortDetailsX = eventShortDetails;

            eventDetails = eventObj[3].toString().trim();

            //   eventDetails1 = eventDetails.replaceAll("\\<.*?>", "");
            //   eventDetailsX = eventDetails1.substring(0, 250);
            eventDetailsX = eventDetails;

            eventDateTime = eventObj[4].toString().trim();

            Date date1 = dateFormatEvent.parse(eventDateTime);
            Date date2 = dateFormatEvent.parse(today);

            //   out.println("date1:: " + date1);
            //  out.println("date2:: " + date2);
            if ((date1.compareTo(date2) == 0) || (date1.compareTo(date2) > 0)) {
                //  out.println("Upcoming");
                upcomingEventBtn = "&nbsp;&nbsp;&nbsp;<span class=\"btn btn_glob btn_more bg_yellow text-white\">Upcoming Event</span>";

            } else {
                upcomingEventBtn = "";
            }

            dateEventM = dateFormatEvent.parse(eventDateTime);
            newDateEventM = dateFormatEventM.format(dateEventM);

            dateEventMM = dateFormatEvent.parse(eventDateTime);
            newDateEventMM = dateFormatEventMM.format(dateEventMM);

            dateEventD = dateFormatEvent.parse(eventDateTime);
            newDateEventD = dateFormatEventD.format(dateEventD);

            dateEventY = dateFormatEvent.parse(eventDateTime);
            newDateEventY = dateFormatEventY.format(dateEventY);

            eventDuration = eventObj[5].toString().trim();
            eventVenue = eventObj[6].toString().trim();

            eventDurationVanue = eventDuration + " <br> " + eventVenue;

            eventFeatureImage = eventObj[7] == null ? "" : eventObj[7].toString().trim();

            eventFeatureImageUrl = GlobalVariable.imageNewsDirLink + eventFeatureImage;

            eventFeatureImage1 = "<img width=\"200\" src=\"" + eventFeatureImageUrl + "\" alt=\"" + eventTitle + "\">";

            eventDetailsUrl = GlobalVariable.baseUrl + "/events/eventsDetails.jsp?eventIdX=" + eventId;

            //      String updateProfileUrl = GlobalVariable.baseUrl + "/eventManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&eventIdX=" + eventId + "&selectedTab=profile";
            //      String updateChangePassUrl = GlobalVariable.baseUrl + "/eventManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&eventIdX=" + eventId + "&selectedTab=changePass";
            if (ev > 4) {
                topMarginClass = "my-5";
            } else {
                topMarginClass = "";
            }

            eventCategory = eventObj[12].toString().trim();

            keyNotesSpeakerConterner = "";

            if (eventCategory.equals("1")) {

                Object speakerObj[] = null;
                String speakerId = "";
                String speakerName = "";
                String speakerDesignation = "";
                String speakerBio = "";
                String speakerShortBio = "";
                String speakerPicture = "";
                String speakerPictureUrl = "";
                String speakerPresentation = "";
                String speakerPresentationUrl = "";
                String speakerEditBrn = "";
                String speakerDeleteBrn = "";
                String speakerPresentationDownloadBrn = "";
                String argXn = "";
                String keyNotesSpeakerInfo = "";

                Query speakerSQL = dbsession.createSQLQuery("SELECT * FROM  speaker_info s WHERE s.id_event = '" + eventId + "' ORDER BY s.id_speaker ASC");

                if (!speakerSQL.list().isEmpty()) {
                    for (Iterator speakerItr = speakerSQL.list().iterator(); speakerItr.hasNext();) {

                        speakerObj = (Object[]) speakerItr.next();
                        speakerId = speakerObj[0].toString().trim();
                        speakerName = speakerObj[2].toString().trim();
                        speakerDesignation = speakerObj[3].toString().trim();

                        speakerShortBio = speakerObj[4] == null ? "" : speakerObj[4].toString().trim();
                        speakerBio = speakerObj[5].toString().trim();

                        //      /upload/seminarpptx/
                        speakerPicture = speakerObj[6] == null ? "" : speakerObj[6].toString().trim();
                        speakerPictureUrl = GlobalVariable.imageDirLink + "seminarpptx/" + speakerPicture;

                        speakerPresentation = speakerObj[7] == null ? "" : speakerObj[7].toString().trim();
                        speakerPresentationUrl = GlobalVariable.imageDirLink + "seminarpptx/" + speakerPresentation;

                        speakerPresentationDownloadBrn = "<a title=\"Download presentation\"  class=\"btn btn-primary\" href=\"" + speakerPresentationUrl + "\"><i class=\"icon-info\"></i> presentation download</a>";

                        keyNotesSpeakerInfo = keyNotesSpeakerInfo + ""
                                + "<h4>About Keynote Speaker</h4>"
                                + "<div class=\"row border-top border-bottom border-secondary m-b-10 p-b-10\" style=\"padding-top:10px; padding-bottom:10px; background:#fff;\">"
                                + "<div class=\"col-4\">"
                                + "<img style=\"max-width: 71%; margin-bottom:7px;\" src=\"" + speakerPictureUrl + "\" alt=\"" + speakerName + "\" class=\"img-fluid\"><br/>"
                                + "<a title=\"Speaker Details\" href=\"#\" class=\"btn btn-sm btn-primary text-white\"> <i class=\"fa fa-info-circle\"></i>  details</a>&nbsp;<a title=\"Download Presentation\" href=\"" + speakerPresentationUrl + "\" target=\"_blank\" class=\"btn btn-sm btn-success text-white\"> <i class=\"fas fa-file\"></i>  download</a>"
                                + "</div>"
                                + "<div class=\"col-8\">"
                                + "<p><strong>" + speakerName + "</strong></P>"
                                + "<p><strong>" + speakerDesignation + "r</strong></p>"
                                + "<p>" + speakerShortBio + "</p>"
                                + "</div>"
                                + "</div>";
                    }

                    keyNotesSpeakerConterner = keyNotesSpeakerInfo;
                } else {
                    keyNotesSpeakerConterner = "";
                }

            } else {
                keyNotesSpeakerConterner = "";
            }

            bottomEventContainer = bottomEventContainer + ""
                    + "<div class=\"row " + topMarginClass + " border-bottom border-secondary\">"
                    + "<div class=\"col-md-2\">"
                    + "<div class=\"event_date\" style=\"padding-top:15px;\">"
                    + "<span class=\"p_date bg_yellow\">"
                    + "<span class=\"day\">" + newDateEventD + "</span>"
                    + "<span class=\"mon\">" + newDateEventMM + "</span>"
                    + "</span>"
                    + "</div>"
                    + "</div>"
                    + "<div class=\"col-md-8\">"
                    + "<div class=\"event_content\" style=\"padding-top:15px;\">"
                    + "<div class=\"details_content\">"
                    + "<h3 class=\"news_title\"><a href=\"" + eventDetailsUrl + "\">" + eventTitle + "</a></h3>"
                    + "<ul class=\"pub_info\">"
                    + "<li class=\"info_item\">"
                    + "<div class=\"info d_flex d_align\">"
                    + "<span class=\"icon\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/clock-icon.png\" alt=\"img\"></span>"
                    + "<span class=\"text\">" + eventDuration + "</span>"
                    + "</div>"
                    + "</li>"
                    + "<li class=\"info_item\">"
                    + "<div class=\"info d_flex d_align\">"
                    + "<span class=\"icon\"><img src=\"" + GlobalVariable.baseUrl + "/commonUtil/assets/images/placehoder-icon.png\" alt=\"img\"></span>"
                    + "<span class=\"text\">" + eventVenue + "</span>"
                    + "</div>"
                    + "</li>"
                    + "</li>"
                    + "<li class=\"info_item\">"
                    + "<div class=\"info d_flex d_align\">"
                    + " " + upcomingEventBtn + " "
                    + "</div>"
                    + "</li>"
                    + "</ul>"
                    + "<p class=\"news_para\">" + eventDetailsX + "</p>"
                    + " " + keyNotesSpeakerConterner + " "
                    + "<a href=\"" + eventDetailsUrl + "\" class=\"btn btn-outline-success border-dark details-link my-3\">View Details</a>"
                    + "</div>"
                    + "</div>"
                    + "</div>"
                    + "<div class=\"col-md-2\">"
                    + "<div class=\"event_img\" style=\"padding-top:15px;\">"
                    + "<img src=\"" + eventFeatureImageUrl + "\" alt=\"img\" class=\"img-fluid\">"
                    + "</div>"
                    + "</div>"
                    + "</div>";

            ev++;
        }
    }

%>

<!-- Start of navigation -->
<section class="navigation_partho parallaxie text-center" style="height:150px;background:url('<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/News-Banner.jpg');position:relative;">
    <div style="position:absolute;top:0;left:0;width:100%;height:100%;background:rgba(0,0,0,0.34);">
        <h3 class="h1 pl-5 mx-5 font-weight-bold text-white pt-5">Seminars</h3>
        <nav aria-label="breadcrumb" class="w-100 pl-5">
            <ol class="breadcrumb h5" style="background-color: #e9ecef00" >
                <li class="breadcrumb-item"><a href="home_v4.html">Home</a></li>
                <li class="breadcrumb-item"><a href="home_v4.html">News & Events</a></li>
                <li class="breadcrumb-item active text-white">Seminars</li>
            </ol>
        </nav>
    </div>
</section>
<!-- End of navigation -->

<

<!-- Start of more events section -->
<section class="m-5">
    <div class="container">


        <%=bottomEventContainer%>

    </div>
</section>
<!-- End of more events section -->

<!-- Start of Pagination -->
<nav class="other-news news-pagination" style="display:none;">
    <ul class="pagination justify-content-center pagination-lg">
        <li class="page-item disabled">
            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
        </li>
        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
            <a class="page-link" href="#">Next</a>
        </li>
    </ul>
</nav>
<!-- End of Pagination -->


<%@ include file="../footer.jsp" %>
