<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Query"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Session"%>
<%@ include file="../header.jsp" %>

<%
    Session dbsessionEvents = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxEvents = dbsessionEvents.beginTransaction();

    String eventsIdX = request.getParameter("eventIdX") == null ? "" : request.getParameter("eventIdX").trim();

    //  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateFormatM = new SimpleDateFormat("MMM");
    DateFormat dateFormatD = new SimpleDateFormat("dd");
    DateFormat dateFormatY = new SimpleDateFormat("Y");
    Date dateM = null;
    Date dateD = null;
    Date dateY = null;
    String newDateM = "";
    String newDateD = "";
    String newDateY = "";

    Date dateEventM = null;
    Date dateEventD = null;
    Date dateEventY = null;
    String newDateEventM = "";
    String newDateEventD = "";
    String newDateEventY = "";

    Query eventSQL = null;
    Object eventObj[] = null;
    String eventId = "";
    String eventTitle = "";
    String eventDuration = "";
    String eventVenue = "";
    String eventDurationVanue = "";
    String eventShortDetails = "";
    String eventShortDetails1 = "";
    String eventShortDetailsX = "";
    String eventDetails = "";
    String eventDateTime = "";
    String eventMonth = "";
    String eventDay = "";
    String eventYear = "";
    String eventFeatureImage = "";
    String eventFeatureImage1 = "";
    String eventFeatureImageUrl = "";
    String eventInfoFirstContainer = "";
    String eventInfoContainer = "";
    String eventDetailsUrl = "";
    int ev = 1;

    eventSQL = dbsessionEvents.createSQLQuery("SELECT ni.id_event, ni.event_title, ni.event_short_desc, ni.event_desc, "
            + "ni.event_date, ni.event_duration ,ni.event_venue, "
            + "ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4 "
            + "FROM  event_info ni "
            + "WHERE ni.id_event = '" + eventsIdX + "'");

    if (!eventSQL.list().isEmpty()) {
        for (Iterator it1 = eventSQL.list().iterator(); it1.hasNext();) {

            eventObj = (Object[]) it1.next();
            eventId = eventObj[0].toString().trim();
            eventTitle = eventObj[1].toString().trim();
            eventShortDetails = eventObj[2].toString().trim();

            //  eventShortDetailsX = eventShortDetails.substring(0, 250);
            eventShortDetailsX = eventShortDetails;

            eventDetails = eventObj[3].toString().trim();

            eventDateTime = eventObj[4].toString().trim();

            dateM = dateFormat.parse(eventDateTime);
            newDateM = dateFormatM.format(dateM);

            dateD = dateFormat.parse(eventDateTime);
            newDateD = dateFormatD.format(dateD);

            dateEventY = dateFormatY.parse(eventDateTime);
            newDateEventY = dateFormatY.format(dateEventY);

            eventDuration = eventObj[5].toString().trim();
            eventVenue = eventObj[6].toString().trim();

            eventDurationVanue = eventDuration + " <br> " + eventVenue;

            eventFeatureImage = eventObj[7] == null ? "" : eventObj[7].toString().trim();

            eventFeatureImageUrl = GlobalVariable.imageNewsDirLink + eventFeatureImage;

            eventFeatureImage1 = "<img width=\"700\" src=\"" + eventFeatureImageUrl + "\" alt=\"" + eventTitle + "\">";

            eventDetailsUrl = GlobalVariable.baseUrl + "/events/eventsDetails.jsp?eventIdX=" + eventId;
        }
    }
%>

<!-- Start of navigation -->
<section class="navigation_partho parallaxie text-center" style="height:150px;background:url('<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/News-Banner.jpg');position:relative;">
    <div style="position:absolute;top:0;left:0;width:100%;height:100%;background:rgba(0,0,0,0.34);">
        <h3 class="h1 pl-5 mx-5 font-weight-bold text-white pt-5">Events</h3>
        <nav aria-label="breadcrumb" class="w-100 pl-5">
            <ol class="breadcrumb h5" style="background-color: #e9ecef00" >
                <li class="breadcrumb-item"><a href="home_v4.html">Home</a></li>
                <li class="breadcrumb-item"><a href="home_v4.html">News & Events</a></li>
                <li class="breadcrumb-item active text-white">Events</li>
            </ol>
        </nav>
    </div>
</section>
<!-- End of navigation -->

<!-- News & Events & Single Event CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/news_events_style.css" rel="stylesheet">
<!-- Font Awesome CSS -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

<!-- Event front image section -->
<!--<section class="news-banner">
    <div class="conainer">
        <div class="card bg-dark text-white">
            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_large_img_2.png" class="card-img img-fluid" alt="Banner Image">
            <div class="card-img-overlay img-fluid">
                <div class="event_date">
                    <span class="p_date bg_yellow  p-5">
                        <span class="day" style="font: 700 62px/45px 'product_sansbold';">13</span>
                        <span class="mon"  style="font: 400 40px/43px 'product_sansregular';">Oct</span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- Event front image section -->

<!-- Event details section starts -->
<section class="news-single-content event-details pt-0" style="display:none1;">
    <div class="container">
        <h2 class="display-41" style="padding-top: 20px; margin-bottom: 10px;"><%=eventTitle%></h2>
        <hr style="height: 2px; border-color: #ccc;">
        <div class="row">
            <div class="col-12">
                <div class="news-content">
                    <div class="media mt-5">
                        <div class="text-center mr-5">
                            <div class="p_date bg_yellow  p-3">
                                <p class="day" style="font: 600 82px/65px 'product_sansbold'; color: #fff;"><%=newDateD%></p>
                                <p class="mon"  style="font: 500 50px/53px 'product_sansregular'; color: #fff;"><%=newDateM%></p>
                                <p class="mon"  style="font: 400 40px/43px 'product_sansregular'; color: #fff;"><%=newDateEventY%></p>
                            </div>
                            <p><i class="fas fa-clock mr-3" style="font-size: 35px;margin-left: 20px !important; margin-top: 20px !important;"></i><span class="h4 font-weight-bold"><%=eventDuration%></span></p>
                            <p><i class="fas fa-map-marker-alt mr-3" style="font-size: 35px; margin-left: 20px !important;margin-top: 20px !important;"></i><span class="h4 font-weight-bold"><%=eventVenue%></span></p>
                            <div class="custom-horizontal-border"></div>
                        </div>

                        <div class="container">
                            <%=eventFeatureImage1%>
                            <br/><br/>
                            <div class="media-body line-letter-spacing">
                                <%=eventDetails%>
                            </div>

                        </div>


                    </div>



                </div>
            </div>
        </div>
    </div>
</section>
<!-- Event details section starts -->

<%

    DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateFormatEventM = new SimpleDateFormat("MMM");
    DateFormat dateFormatEventD = new SimpleDateFormat("dd");
    DateFormat dateFormatEventY = new SimpleDateFormat("Y");

    Query newsSQL = null;
    Object newsObj[] = null;
    String newsId = "";
    String newsTitle = "";
    String newsSlug = "";
    String newsShortDetails = "";
    String newsShortDetails1 = "";
    String newsShortDetailsX = "";
    String newsShortDetailsXFirstNews = "";
    String newsDetails = "";
    String newsDateTime = "";
    String newsFeatureImage = "";
    String newsFeatureImage1 = "";
    String newsFeatureImageUrl = "";
    String newsInfoFirstContainer = "";
    String newsInfoContainer = "";
    String newsDetailsUrl = "";
    int nw = 1;

    Query centerNewsListSQL = null;
    Object centerNewsListObj[] = null;
    String centerNewsId = "";
    String newsIdx = "";

    centerNewsListSQL = dbsessionEvents.createSQLQuery("SELECT * FROM event_news WHERE EVENT_ID = '" + eventsIdX + "' ORDER BY NEW_ID DESC");
    if (!centerNewsListSQL.list().isEmpty()) {

%>

<!-- start update_news_wrap -->
<section class="update_news_wrap" style="padding-bottom: 0;">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <h3 class="sec_title">Event Related News</h3>
            </div>
        </div>

        <%            for (Iterator centerNewsListItr = centerNewsListSQL.list().iterator(); centerNewsListItr.hasNext();) {
                centerNewsListObj = (Object[]) centerNewsListItr.next();

                centerNewsId = centerNewsListObj[0].toString().trim();
                newsIdx = centerNewsListObj[2].toString().trim();

                // out.println("newsIdx :: " + newsIdx);
                newsSQL = dbsessionEvents.createSQLQuery("SELECT ni.id_news, ni.news_title, ni.news_short_desc, ni.news_desc, "
                        + "ni.news_date, ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4  "
                        + "FROM  news_info ni "
                        + "WHERE  ni.id_news ='" + newsIdx + "'");

                if (!newsSQL.list().isEmpty()) {
                    for (Iterator itNews = newsSQL.list().iterator(); itNews.hasNext();) {

                        newsObj = (Object[]) itNews.next();
                        newsId = newsObj[0].toString().trim();
                        newsTitle = newsObj[1].toString().trim();
                        newsShortDetails = newsObj[2].toString().trim();

                        //    newsShortDetailsX = newsShortDetails.substring(0, 200);
                        //      System.out.println(" newsShortDetailsX0,200:: " +newsShortDetailsX );
                        //    newsShortDetailsXFirstNews = newsShortDetails.substring(0, 300);
                        newsShortDetails1 = newsShortDetails;

                        newsShortDetails = newsShortDetails;

                        newsDetails = newsObj[3].toString().trim();

                        //   newsDateTime = newsObj[4] == null ? "" : newsObj[4].toString().trim();
                        //  if (newsDateTime.equals("")) {
                        // newsDateTime = newsObj[10].toString().trim();
                        newsDateTime = newsObj[4] == null ? "" : newsObj[4].toString().trim();
                        System.out.println(" newsDateTime :: " + newsDateTime);
                        //   }

                        dateEventM = dateFormatEvent.parse(newsDateTime);
                        newDateEventM = dateFormatEventM.format(dateEventM);

                        dateEventD = dateFormatEvent.parse(newsDateTime);
                        newDateEventD = dateFormatEventD.format(dateEventD);

                        dateEventY = dateFormatEvent.parse(newsDateTime);
                        newDateEventY = dateFormatEventY.format(dateEventY);

                        newsFeatureImage = newsObj[5] == null ? "" : newsObj[5].toString().trim();
                        newsFeatureImageUrl = GlobalVariable.imageNewsDirLink + newsFeatureImage;

                        newsFeatureImage1 = "<img width=\"200\" src=\"" + newsFeatureImageUrl + "\" alt=\"" + newsTitle + "\">";

                        newsDetailsUrl = GlobalVariable.baseUrl + "/news/newsDetails.jsp?newsIdX=" + newsId;

                        //       String updateProfileUrl = GlobalVariable.baseUrl + "/newsManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&newsIdX=" + newsId + "&selectedTab=profile";
                        //      String updateChangePassUrl = GlobalVariable.baseUrl + "/newsManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&newsIdX=" + newsId + "&selectedTab=changePass";
                        newsInfoContainer = newsInfoContainer + ""
                                + "<div class=\"col-lg-4 col-md-6 col-sm-12\">"
                                + "<div class=\"news_single_item\">"
                                + "<div class=\"news_img\">"
                                + "<img src=\"" + newsFeatureImageUrl + "\" style=\" height:250px;\" alt=\"img\">"
                                + "</div>"
                                + "<div class=\"details_content\"  style=\"min-height:400px;\">"
                                + "<p class=\"news_date\">" + newDateEventD + " " + newDateEventM + ", " + newDateEventY + "</p>"
                                + "<a href=\"" + newsDetailsUrl + "\"><h3 class=\"news_title\">" + newsTitle + "</h3></a>"
                                + "<p class=\"news_para\">" + newsShortDetails + "</p>"
                                + "<a href=\"" + newsDetailsUrl + "\" class=\"view_details\">View Details</a>"
                                + "</div>"
                                + "</div>"
                                + "</div>";

                        nw++;
                    }
                }
            }
        %>



        <div class="row">
            <div class="col-12">
                <div class="news_small">
                    <div class="row">

                        <%=newsInfoContainer%>

                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="text-center learn_more_btn">
                    <a href="<%out.print(GlobalVariable.baseUrl);%>/news/news.jsp" class="learn_more">Load More</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end update_news_wrap -->

<%

    }

%>



<%    Query photoAlbumSQL = null;
    Object objPhotoAlbum[] = null;

    Query photoSQL = null;
    Object objPhoto[] = null;

    int ix = 1;
    String photoAlbumId = "";
    String photoAlbumName = "";
    String photoAlbumShortName = "";
    String pictureId = "";
    String pictureTitle = "";
    String pictureCaption = "";
    String pictureOrginalName = "";
    String pictureName = "";
    String pictureThumb = "";
    String showingOrder = "";
    String photoAlbum = "";
    String photoSrc = "";
    String photoLink = "";
    String agrX = "";
    String albumDetailsUrl = "";

    Query centerAlbumListSQL = null;
    Object centerAlbumListObj[] = null;
    String centerAlbumId = "";
    String albumIdx = "";

    centerAlbumListSQL = dbsessionEvents.createSQLQuery("SELECT * FROM album_event WHERE EVENT_ID = '" + eventsIdX + "' ORDER BY ALBUM_ID DESC");
    if (!centerAlbumListSQL.list().isEmpty()) {

%>

<!-- end photo_gallary_wrap -->
<section class="photo_gallary_wrap" style="padding-top: 20px;">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <h3 class="sec_title">Event Photo Album</h3>
            </div>
        </div>
        <div class="row">

            <%               for (Iterator centerAlbumListItr = centerAlbumListSQL.list().iterator(); centerAlbumListItr.hasNext();) {
                    centerAlbumListObj = (Object[]) centerAlbumListItr.next();

                    centerAlbumId = centerAlbumListObj[0].toString().trim();
                    albumIdx = centerAlbumListObj[1].toString().trim();

                    //  out.println("albumIdx :: " + albumIdx);
                    photoAlbumSQL = dbsessionEvents.createSQLQuery("SELECT * FROM  picture_category pc WHERE pc.id_cat='" + albumIdx + "'");

                    if (!photoAlbumSQL.list().isEmpty()) {
                        for (Iterator itPhoto = photoAlbumSQL.list().iterator(); itPhoto.hasNext();) {

                            objPhotoAlbum = (Object[]) itPhoto.next();
                            photoAlbumId = objPhotoAlbum[0].toString().trim();
                            photoAlbumName = objPhotoAlbum[1].toString().trim();
                            photoAlbumShortName = objPhotoAlbum[2].toString().trim();

                            photoSQL = dbsessionEvents.createSQLQuery("SELECT "
                                    + "pg.id_picture,pg.picture_title,pg.picture_caption ,pg.picture_orginal_name,pg.picture_name,pg.picture_thumb ,pg.showing_order "
                                    + "FROM  picture_gallery_info pg "
                                    + "WHERE pg.picture_category = '" + photoAlbumId + "' "
                                    + "ORDER BY pg.id_picture DESC LIMIT 0, 1");

                            if (!photoSQL.list().isEmpty()) {
                                for (Iterator it1 = photoSQL.list().iterator(); it1.hasNext();) {

                                    objPhoto = (Object[]) it1.next();
                                    pictureId = objPhoto[0].toString().trim();
                                    pictureTitle = objPhoto[1].toString().trim();
                                    pictureCaption = objPhoto[2].toString().trim();
                                    pictureOrginalName = objPhoto[3].toString().trim();
                                    pictureName = objPhoto[4].toString().trim();
                                    pictureThumb = objPhoto[5].toString().trim();
                                    showingOrder = objPhoto[6].toString().trim();

                                    photoSrc = GlobalVariable.imagePhotoDirLink + pictureName;
                                    //    photoSrc = GlobalVariable.imageDirLink + "photos/" + pictureName;

                                    photoLink = "<img src=\"" + photoSrc + "\" width=\"370\" height=\"246\"/>";

                                    agrX = "'" + pictureId + "','" + session.getId() + "'";
                                }
                            }

                            albumDetailsUrl = GlobalVariable.baseUrl + "/photogallery/photogallery.jsp?albumIdX=" + photoAlbumId;


            %>


            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="photo_gallary_single_container" style="padding: 5px; border: solid #aaaaaa 1px; margin-bottom: 15px; border-radius:5px;">
                    <a href="<%=albumDetailsUrl%>" class="photo_gallary_single_item">
                        <div class="photo_gallary_img">
                            <img src="<%=photoSrc%>" alt="<%=pictureCaption%>" width="360" height="220" style="border-radius:0;">
                        </div>
                        <div class="hover_content">
                            <p><%=photoAlbumName%></p>
                        </div>
                    </a>

                </div>
            </div>

            <%
                            ix++;
                        }

                    }
                }
            %>





        </div>

    </div>
</section>
<!-- end photo_gallary_wrap -->

<%
    }

%>

<%    Object objVdo[] = null;

    int vx = 1;
    String videoId = "";
    String videoTitle = "";
    String videoCaptions = "";
    String videoOriginalLink = "";
    String videoEmbedLink = "";
    String videoEmbedLink1 = "";
    String videoThumbnail = "";
    String videoThumbnailn = "";
    String videoThumbnailX = "";
    String videoThumbnailXn = "";
    String videoCategory = "";
    String agrXv = "";
    String bigvideoContailner = "";
    String smallvideoContailner = "";

    Query centerVideoListSQL = null;
    Object centerVideoListObj[] = null;
    String centerVideoId = "";
    String videoIdx = "";

    centerVideoListSQL = dbsessionEvents.createSQLQuery("SELECT * FROM event_video WHERE EVENT_ID = '" + eventsIdX + "' ORDER BY VIDEO_ID DESC");
    if (!centerVideoListSQL.list().isEmpty()) {

%>

<!-- end featured_video_wrap -->
<section class="featured_video_wrap" style="background-color: #fff; padding-top: 20px;">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <h3 class="sec_title">Event Video Gallery</h3>
            </div>
        </div>
        <%            for (Iterator centerVideoListItr = centerVideoListSQL.list().iterator(); centerVideoListItr.hasNext();) {
                centerVideoListObj = (Object[]) centerVideoListItr.next();

                centerVideoId = centerVideoListObj[0].toString().trim();
                videoIdx = centerVideoListObj[2].toString().trim();

                // out.println("videoIdx :: " + videoIdx);
                Query videoSQL = dbsessionEvents.createSQLQuery("SELECT ni.id_video,ni.video_category, ni.video_title, ni.video_caption, ni.video_orginal_link, ni.video_emded_link, "
                        + "ni.feature_image, ni.image1  "
                        + "FROM  video_gallery_info ni WHERE ni.id_video='" + videoIdx + "'");

                if (!videoSQL.list().isEmpty()) {
                    for (Iterator it1 = videoSQL.list().iterator(); it1.hasNext();) {

                        objVdo = (Object[]) it1.next();
                        videoId = objVdo[0].toString().trim();
                        videoCategory = objVdo[1].toString().trim();
                        videoTitle = objVdo[2].toString().trim();
                        videoCaptions = objVdo[3].toString().trim();
                        videoOriginalLink = objVdo[4].toString().trim();
                        videoEmbedLink = objVdo[5] == null ? "" : objVdo[5].toString().trim();

                        videoEmbedLink1 = "<iframe width=\"250\" height=\"150\" src=\"" + videoEmbedLink + "\"></iframe>";

                        videoThumbnail = objVdo[6] == null ? "" : objVdo[6].toString().trim();
                        videoThumbnailn = "<img width=\"250\" height=\"150\" src=\"" + videoThumbnail + "\" alt=\"" + videoTitle + "\">";

                        videoThumbnailX = objVdo[7] == null ? "" : objVdo[7].toString().trim();

                        bigvideoContailner = bigvideoContailner + "<div class=\"col-md-3\">"
                                + "<!--Modal: Name-->"
                                + "<div class=\"modal fade\" id=\"modal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">"
                                + "<div class=\"modal-dialog modal-lg\" role=\"document\">"
                                + "  <!--Content-->"
                                + "  <div class=\"modal-content\">"
                                + "      <!--Body-->"
                                + "      <div class=\"modal-body mb-0 p-0\">"
                                + "          <div class=\"embed-responsive embed-responsive-16by9 z-depth-1-half\">"
                                + "              <iframe class=\"embed-responsive-item\" src=\" " + videoEmbedLink + "\" allowfullscreen></iframe>"
                                + "          </div>"
                                + "      </div>"
                                + "      <!--Footer-->"
                                + "      <div class=\"modal-footer justify-content-center modal-social-icon-partho\">"
                                + "          <span class=\"mr-4\">Share</span>"
                                + "          <a type=\"button\" class=\"btn-floating btn-sm btn-fb\"><i class=\"fab fa-facebook-f\"></i></a>"
                                + "          <!--Twitter-->"
                                + "          <a type=\"button\" class=\"btn-floating btn-sm btn-tw\"><i class=\"fab fa-twitter\"></i></a>"
                                + "          <!--Google +-->"
                                + "          <a type=\"button\" class=\"btn-floating btn-sm btn-gplus\"><i class=\"fab fa-google-plus-g\"></i></a>"
                                + "          <!--Linkedin-->"
                                + "          <a type=\"button\" class=\"btn-floating btn-sm btn-ins\"><i class=\"fab fa-linkedin-in\"></i></a>"
                                + "          <button type=\"button\" class=\"btn btn-outline-primary btn-rounded btn-md ml-4\" data-dismiss=\"modal\">Close</button>"
                                + "      </div>"
                                + "  </div>"
                                + "  <!--/.Content-->"
                                + "</div>"
                                + "</div>"
                                + "<!--Modal: Name-->"
                                + "<a>"
                                + "<img class=\"img-fluid z-depth-1\" src=\"" + videoThumbnail + "\" alt=\"video\" data-toggle=\"modal\" data-target=\"#modal1\" style=\"width:300px;height:200px;\">"
                                + "<p class=\"p-2 mx-auto font-weight-bold text-info more-video-title\" style=\"cursor:pointer;\">" + videoTitle + "</p>"
                                + "</a>"
                                + "</div>";

                    }
                }
            }
        %>


        <div class="row">

            <%=bigvideoContailner%>

        </div>   



    </div>
</section>
<!-- end featured_video_wrap -->
<%  }

%>

<!-- Start of More Events Section -->
<section>
    <div class="container">
        <div class="post-heading pt-5">
            <h3 class="text-uppercase font-weight-bold">More Events </h3>
            <hr class="custom-horizontal-border mr-5" />
        </div>
        <div class="row mb-5">

            <%                Query eventMoreSQL = null;
                Object eventMoreObj[] = null;
                String eventMoreId = "";
                String eventMoreTitle = "";
                String eventMoreFeatureImage = "";
                String eventMoreFeatureImage1 = "";
                String eventMoreFeatureImageUrl = "";
                String eventMoreDetailsUrl = "";

                eventMoreSQL = dbsessionEvents.createSQLQuery("SELECT ni.id_event, ni.event_title, ni.event_short_desc, ni.event_desc, "
                        + "ni.event_date, ni.event_duration ,ni.event_venue, "
                        + "ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4 "
                        + "FROM  event_info ni "
                        + "WHERE  ni.id_event !='" + eventsIdX + "' "
                        + "ORDER BY ni.id_event DESC LIMIT 0,4");

                if (!eventMoreSQL.list().isEmpty()) {
                    for (Iterator it1 = eventMoreSQL.list().iterator(); it1.hasNext();) {

                        eventMoreObj = (Object[]) it1.next();
                        eventMoreId = eventMoreObj[0].toString().trim();
                        eventMoreTitle = eventMoreObj[1].toString().trim();

                        eventMoreFeatureImage = eventObj[7] == null ? "" : eventMoreObj[7].toString().trim();

                        //  eventMoreFeatureImageUrl = GlobalVariable.imageEventsDirLink + eventMoreFeatureImage;
                        eventMoreFeatureImageUrl = GlobalVariable.imageNewsDirLink + eventMoreFeatureImage;

                        eventMoreFeatureImage1 = "<img width=\"200\" src=\"" + eventMoreFeatureImageUrl + "\" alt=\"" + eventMoreTitle + "\" class=\"img-fluid news-image-single  mb-3\">";

                        eventMoreDetailsUrl = GlobalVariable.baseUrl + "/events/eventsDetails.jsp?eventIdX=" + eventMoreId;
            %>

            <div class="col-md-3 col-sm-6">
                <div class="post-content pt-3">
                    <div class="post-content-img">
                        <%=eventMoreFeatureImage1%>
                        <div class="post-content-img-overlay d-flex align-items-center justify-content-center" style="height: 50%;"></div>
                        <a href="<%=eventMoreDetailsUrl%>" class="text-primary font-weight-bold news-link"><%=eventMoreTitle%></a>
                    </div>
                </div>
            </div>

            <%
                    }
                }

            %>


        </div>
    </div>
</section>
<!-- End of More Events Section -->
<%    dbsessionEvents.clear();
    dbsessionEvents.close();
%>

<%@ include file="../footer.jsp" %>
