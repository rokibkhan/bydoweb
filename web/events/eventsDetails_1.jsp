<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Query"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Session"%>
<%@ include file="../header.jsp" %>

<%
    Session dbsessionEvents = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxEvents = dbsessionEvents.beginTransaction();

    String eventsIdX = request.getParameter("eventIdX") == null ? "" : request.getParameter("eventIdX").trim();

    //  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateFormatM = new SimpleDateFormat("MMM");
    DateFormat dateFormatD = new SimpleDateFormat("dd");
    DateFormat dateFormatY = new SimpleDateFormat("Y");
    Date dateM = null;
    Date dateD = null;
    Date dateY = null;
    String newDateM = "";
    String newDateD = "";
    String newDateY = "";

    Date dateEventM = null;
    Date dateEventD = null;
    Date dateEventY = null;
    String newDateEventM = "";
    String newDateEventD = "";
    String newDateEventY = "";

    Query eventSQL = null;
    Object eventObj[] = null;
    String eventId = "";
    String eventTitle = "";
    String eventDuration = "";
    String eventVenue = "";
    String eventDurationVanue = "";
    String eventShortDetails = "";
    String eventShortDetails1 = "";
    String eventShortDetailsX = "";
    String eventDetails = "";
    String eventDateTime = "";
    String eventMonth = "";
    String eventDay = "";
    String eventYear = "";
    String eventFeatureImage = "";
    String eventFeatureImage1 = "";
    String eventFeatureImageUrl = "";
    String eventInfoFirstContainer = "";
    String eventInfoContainer = "";
    String eventDetailsUrl = "";
    int ev = 1;

    eventSQL = dbsessionEvents.createSQLQuery("SELECT ni.id_event, ni.event_title, ni.event_short_desc, ni.event_desc, "
            + "ni.event_date, ni.event_duration ,ni.event_venue, "
            + "ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4 "
            + "FROM  event_info ni "
            + "WHERE ni.id_event = '" + eventsIdX + "'");

    if (!eventSQL.list().isEmpty()) {
        for (Iterator it1 = eventSQL.list().iterator(); it1.hasNext();) {

            eventObj = (Object[]) it1.next();
            eventId = eventObj[0].toString().trim();
            eventTitle = eventObj[1].toString().trim();
            eventShortDetails = eventObj[2].toString().trim();

            //  eventShortDetailsX = eventShortDetails.substring(0, 250);
            eventShortDetailsX = eventShortDetails;

            eventDetails = eventObj[3].toString().trim();

            eventDateTime = eventObj[4].toString().trim();

            dateM = dateFormat.parse(eventDateTime);
            newDateM = dateFormatM.format(dateM);

            dateD = dateFormat.parse(eventDateTime);
            newDateD = dateFormatD.format(dateD);

            dateEventY = dateFormatY.parse(eventDateTime);
            newDateEventY = dateFormatY.format(dateEventY);

            eventDuration = eventObj[5].toString().trim();
            eventVenue = eventObj[6].toString().trim();

            eventDurationVanue = eventDuration + " <br> " + eventVenue;

            eventFeatureImage = eventObj[7] == null ? "" : eventObj[7].toString().trim();

            eventFeatureImageUrl = GlobalVariable.imageEventsDirLink + eventFeatureImage;

            eventFeatureImage1 = "<img width=\"200\" src=\"" + eventFeatureImageUrl + "\" alt=\"" + eventTitle + "\">";

            eventDetailsUrl = GlobalVariable.baseUrl + "/events/eventsDetails.jsp?eventIdX=" + eventId;
        }
    }
%>

<section>

    <nav aria-label="breadcrumb"> 
        <div class="container">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Events</a></li>
                <li class="breadcrumb-item active" aria-current="page">Events Details</li>
            </ol>
        </div>
    </nav>
</section>

<!-- News & Events & Single Event CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/news_events_style.css" rel="stylesheet">
<!-- Font Awesome CSS -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

<!-- Event front image section -->
<!--<section class="news-banner">
    <div class="conainer">
        <div class="card bg-dark text-white">
            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_large_img_2.png" class="card-img img-fluid" alt="Banner Image">
            <div class="card-img-overlay img-fluid">
                <div class="event_date">
                    <span class="p_date bg_yellow  p-5">
                        <span class="day" style="font: 700 62px/45px 'product_sansbold';">13</span>
                        <span class="mon"  style="font: 400 40px/43px 'product_sansregular';">Oct</span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- Event front image section -->

<!-- Event details section starts -->
<section class="news-single-content event-details pt-0" style="display:none1;">
    <div class="container">
        <h2 class="display-41"><%=eventTitle%></h2>
        <div class="row">
            <div class="col-12">
                <div class="news-content">
                    <div class="media mt-5">
                        <div class="text-center mr-5">
                            <div class="p_date bg_yellow  p-3">
                                <p class="day" style="font: 600 82px/65px 'product_sansbold'; color: #fff;"><%=newDateD%></p>
                                <p class="mon"  style="font: 500 50px/53px 'product_sansregular'; color: #fff;"><%=newDateM%></p>
                                <p class="mon"  style="font: 400 40px/43px 'product_sansregular'; color: #fff;"><%=newDateEventY%></p>
                            </div>
                            <p><i class="fas fa-clock mr-3" style="font-size: 35px;margin-left: 20px !important; margin-top: 20px !important;"></i><span class="h4 font-weight-bold"><%=eventDuration%></span></p>
                            <p><i class="fas fa-map-marker-alt mr-3" style="font-size: 35px; margin-left: 20px !important;margin-top: 20px !important;"></i><span class="h4 font-weight-bold"><%=eventVenue%></span></p>
                            <div class="custom-horizontal-border"></div>
                        </div>

                        <div class="media-body line-letter-spacing">
                            <%=eventDetails%>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</section>
<!-- Event details section starts -->

<!-- Start of More Events Section -->
<section>
    <div class="container">
        <div class="post-heading pt-5">
            <h3 class="text-uppercase font-weight-bold">More Events </h3>
            <hr class="custom-horizontal-border mr-5" />
        </div>
        <div class="row mb-5">

            <%
                Query eventMoreSQL = null;
                Object eventMoreObj[] = null;
                String eventMoreId = "";
                String eventMoreTitle = "";
                String eventMoreFeatureImage = "";
                String eventMoreFeatureImage1 = "";
                String eventMoreFeatureImageUrl = "";
                String eventMoreDetailsUrl = "";

                eventMoreSQL = dbsessionEvents.createSQLQuery("SELECT ni.id_event, ni.event_title, ni.event_short_desc, ni.event_desc, "
                        + "ni.event_date, ni.event_duration ,ni.event_venue, "
                        + "ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4 "
                        + "FROM  event_info ni "
                        + "WHERE  ni.id_event !='" + eventsIdX + "' "
                        + "ORDER BY ni.id_event DESC LIMIT 0,4");

                if (!eventMoreSQL.list().isEmpty()) {
                    for (Iterator it1 = eventMoreSQL.list().iterator(); it1.hasNext();) {

                        eventMoreObj = (Object[]) it1.next();
                        eventMoreId = eventMoreObj[0].toString().trim();
                        eventMoreTitle = eventMoreObj[1].toString().trim();

                        eventMoreFeatureImage = eventObj[7] == null ? "" : eventMoreObj[7].toString().trim();

                      //  eventMoreFeatureImageUrl = GlobalVariable.imageEventsDirLink + eventMoreFeatureImage;
                        eventMoreFeatureImageUrl = GlobalVariable.imageNewsDirLink + eventMoreFeatureImage;

                        eventMoreFeatureImage1 = "<img width=\"200\" src=\"" + eventMoreFeatureImageUrl + "\" alt=\"" + eventMoreTitle + "\" class=\"img-fluid news-image-single  mb-3\">";

                        eventMoreDetailsUrl = GlobalVariable.baseUrl + "/events/eventsDetails.jsp?eventIdX=" + eventMoreId;
            %>

            <div class="col-md-3 col-sm-6">
                <div class="post-content pt-3">
                    <div class="post-content-img">
                        <%=eventMoreFeatureImage1%>
                        <div class="post-content-img-overlay d-flex align-items-center justify-content-center" style="height: 50%;"></div>
                        <a href="<%=eventMoreDetailsUrl%>" class="text-primary font-weight-bold news-link"><%=eventMoreTitle%></a>
                    </div>
                </div>
            </div>

            <%
                    }
                }

            %>


        </div>
    </div>
</section>
<!-- End of More Events Section -->
<%
    dbsessionEvents.clear();
    dbsessionEvents.close();
%>

<%@ include file="../footer.jsp" %>
