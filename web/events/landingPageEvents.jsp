<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.appul.util.GlobalVariable"%>

<%

    Session dbsessionEvents = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxEvents = dbsessionEvents.beginTransaction();

    //  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateFormatM = new SimpleDateFormat("MMM");
    DateFormat dateFormatD = new SimpleDateFormat("dd");
    DateFormat dateFormatY = new SimpleDateFormat("Y");
    Date dateM = null;
    Date dateD = null;
    String newDateM = "";
    String newDateD = "";
    String newDateY = "";
    
    Date todayDate = new Date();
    String today = dateFormat.format(todayDate); //2016-11-16

    Query eventSQL = null;
    Object eventObj[] = null;
    String eventId = "";
    String eventTitle = "";
    String eventDuration = "";
    String eventVenue = "";
    String eventDurationVanue = "";
    String eventShortDetails = "";
    String eventShortDetails1 = "";
    String eventShortDetailsX = "";
    String eventDetails = "";
    String eventDateTime = "";
    String eventMonth = "";
    String eventDay = "";
    String eventYear = "";
    String eventFeatureImage = "";
    String eventFeatureImage1 = "";
    String eventFeatureImageUrl = "";
    String eventInfoFirstContainer = "";
    String eventInfoContainer = "";
    String eventDetailsUrl = "";
    int ev = 1;
    
    String upcomingEventBtn = "";
%>
<!-- start update_news_wrap -->
<section class="update_news_wrap upcomimg_event_wrap" style="padding-top: 20px; background-color: #fff;">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <h3 class="sec_title">Recent Events <span class="float-right"><a href="<%out.print(GlobalVariable.baseUrl);%>/events/events.jsp" class="view_all">View All</a></span></h3>
            </div>
        </div>
        <%
            //   DateFormat dateFormatXn = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            eventSQL = dbsessionEvents.createSQLQuery("SELECT ni.id_event, ni.event_title, ni.event_short_desc, ni.event_desc, "
                    + "ni.event_date, ni.event_duration ,ni.event_venue, "
                    + "ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4 "
                    + "FROM  event_info ni "
                    + "ORDER BY ni.id_event DESC LIMIT 0,3");

            if (!eventSQL.list().isEmpty()) {
                for (Iterator it1 = eventSQL.list().iterator(); it1.hasNext();) {

                    eventObj = (Object[]) it1.next();
                    eventId = eventObj[0].toString().trim();
                    eventTitle = eventObj[1].toString().trim();
                    eventShortDetails = eventObj[2].toString().trim();

                    // eventShortDetailsX = eventShortDetails.substring(0, 250);
                    eventShortDetailsX = eventShortDetails;

                    eventDetails = eventObj[3].toString().trim();

                    eventDateTime = eventObj[4].toString().trim();

                    Date date1 = dateFormat.parse(eventDateTime);
                    Date date2 = dateFormat.parse(today);

                 //   out.println("date1:: " + date1);
                  //  out.println("date2:: " + date2);

                    if ((date1.compareTo(date2) == 0) || (date1.compareTo(date2) > 0)) {
                      //  out.println("Upcoming");
                        upcomingEventBtn = "&nbsp;&nbsp;&nbsp;<span class=\"btn btn_glob btn_more bg_yellow text-white\">Upcoming Event</span>";
                        
                    }else{
                        upcomingEventBtn = "";
                    }

                    dateM = dateFormat.parse(eventDateTime);
                    newDateM = dateFormatM.format(dateM);

                    dateD = dateFormat.parse(eventDateTime);
                    newDateD = dateFormatD.format(dateD);

                    eventDuration = eventObj[5].toString().trim();
                    eventVenue = eventObj[6].toString().trim();

                    eventDurationVanue = eventDuration + " <br> " + eventVenue;

                    eventFeatureImage = eventObj[7] == null ? "" : eventObj[7].toString().trim();

                    eventFeatureImageUrl = GlobalVariable.imageNewsDirLink + eventFeatureImage;

                    eventFeatureImage1 = "<img width=\"570\" height=\"370\" src=\"" + eventFeatureImageUrl + "\" alt=\"" + eventTitle + "\">";

                    eventDetailsUrl = GlobalVariable.baseUrl + "/events/eventsDetails.jsp?eventIdX=" + eventId;

                    //       String updateProfileUrl = GlobalVariable.baseUrl + "/eventManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&eventIdX=" + eventId + "&selectedTab=profile";
                    //      String updateChangePassUrl = GlobalVariable.baseUrl + "/eventManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&eventIdX=" + eventId + "&selectedTab=changePass";
                    if (ev == 1) {
                        eventInfoFirstContainer = eventInfoFirstContainer + ""
                                + "<div class=\"row\">"
                                + "<div class=\"col-12\">"
                                + "<div class=\"news_large bg_white\">"
                                + "<div class=\"row d_align\">"
                                + "<div class=\"col-lg-6 col-md-12\">"
                                + "<div class=\"news_img\">"
                                + "<img src=\" " + eventFeatureImageUrl + "\" alt=\"img\">"
                                + "<span class=\"p_date bg_yellow\">"
                                + "<span class=\"day\">" + newDateD + "</span>"
                                + "<span class=\"mon\">" + newDateM + "</span>"
                                + "</span>"
                                + "</div>"
                                + "</div>"
                                + "<div class=\"col-lg-6 col-md-12\">"
                                + "<div class=\"details_content\">"
                                + "<a href=\"" + eventDetailsUrl + "\"><h3 class=\"news_title\">" + eventTitle + "</h3></a>"
                                + "<ul class=\"pub_info\">"
                                + "<li class=\"info_item\">"
                                + "<div class=\"info d_flex d_align\">"
                                + "<span class=\"icon\">"
                                + "<img src=\" " + GlobalVariable.baseUrl + "/commonUtil/assets/images/clock-icon.png\" alt=\"img\">"
                                + "</span>"
                                + "<span class=\"text\">"
                                + " " + eventDuration + " "
                                + "</span>"
                                + "</div>"
                                + "</li>"
                                + "<li class=\"info_item\">"
                                + "<div class=\"info d_flex d_align\">"
                                + "<span class=\"icon\">"
                                + "<img src=\" " + GlobalVariable.baseUrl + "/commonUtil/assets/images/placehoder-icon.png\" alt=\"img\">"
                                + "</span>"
                                + "<span class=\"text\">"
                                + " " + eventVenue + " "
                                + "</span>"
                                + " " + upcomingEventBtn + " "
                                + "</div>"
                                + "</li>"
                                + "</ul>"
                                + "<p class=\"news_para\">" + eventShortDetailsX + "</p>"
                                + "<a href=\"" + eventDetailsUrl + "\" class=\"view_details read_more\">Read more</a>"
                                + "</div>"
                                + "</div>"
                                + "</div>"
                                + "</div>"
                                + "</div>"
                                + "</div>";
                    }
                    if (ev > 1) {
                        eventInfoContainer = eventInfoContainer + ""
                                + "<div class=\"event_single_item\">"
                                + "<div class=\"row\">"
                                + "<div class=\"col-lg-8 col-md-8 col-sm-12 col-xs-12\">"
                                + "<div class=\"event_item d_flex\">"
                                + "<div class=\"event_date\">"
                                + "<span class=\"p_date bg_yellow\">"
                                + "<span class=\"day\">" + newDateD + "</span>"
                                + "<span class=\"mon\">" + newDateM + "</span>"
                                + "</span>"
                                + "</div>"
                                + "<div class=\"event_content\">"
                                + "<div class=\"details_content\">"
                                + "<a href=\"" + eventDetailsUrl + "\"><h3 class=\"news_title\">" + eventTitle + "</h3></a>"
                                + "<ul class=\"pub_info\">"
                                + "<li class=\"info_item\">"
                                + "<div class=\"info d_flex d_align\">"
                                + "<span class=\"icon\">"
                                + "<img src=\" " + GlobalVariable.baseUrl + "/commonUtil/assets/images/clock-icon.png\" alt=\"img\">"
                                + "</span>"
                                + "<span class=\"text\">"
                                + " " + eventDuration + " "
                                + "</span>"
                                + "</div>"
                                + "</li>"
                                + "<li class=\"info_item\">"
                                + "<div class=\"info d_flex d_align\">"
                                + "<span class=\"icon\">"
                                + "<img src=\" " + GlobalVariable.baseUrl + "/commonUtil/assets/images/placehoder-icon.png\" alt=\"img\">"
                                + "</span>"
                                + "<span class=\"text\">"
                                + " " + eventVenue + " "
                                + "</span>"
                                + "</div>"
                                + "</li>"
                                + "</ul>"
                                + "<p class=\"news_para\">" + eventShortDetailsX + "</p>"
                                + "<a href=\"" + eventDetailsUrl + "\" class=\"view_details read_more\">Read more</a>"
                                + "</div>"
                                + "</div>"
                                + "</div>"
                                + "</div>"
                                + "<div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12\">"
                                + "<div class=\"event_img\">"
                                + "<img src=\" " + eventFeatureImageUrl + "\" alt=\"" + eventTitle + "\">"
                                + "</div>"
                                + "</div>"
                                + "</div>"
                                + "</div>";

                    }

                    ev++;
                }
            }


        %>


        <%=eventInfoFirstContainer%>

        <div class="row">
            <div class="col-12">

                <%=eventInfoContainer%>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center learn_more_btn">
                    <a href="<%out.print(GlobalVariable.baseUrl);%>/events/events.jsp" class="learn_more">Load More</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end update_news_wrap -->

<%
    dbsessionEvents.clear();
    dbsessionEvents.close();
%>