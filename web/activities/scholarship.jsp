<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/wadud.css">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<!-- Banner of Scholarship Page  -->
<div class="about_banner_activites">
    <h2 class="display_41_saz text-white">Scholarship</h3>
        <div class="jb_breadcrumb">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="background: none;">
                    <li class="breadcrumb-item"><a class="bi_wadud" href="#">Activities </a></li>
                    <li class="breadcrumb-item active">Scholarship</li>
                </ol>
            </nav>
        </div>
</div>
<!-- End of Banner of Scholarship Page -->


<!-- Starting of "More About Scholarship" Section -->
<div class="container">
    <div class="row pt-5">
        <div class="col-md-6">
            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
        <div class="col-md-6">
            <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/scholarship_image.jpg" class="do-img img-fluid pl-5" alt="Sholarship image.jpg" />
        </div>
    </div>
</div>
<!-- Ending of "More About Scholarship" section -->


<!--Start Banner of Scholarship List  -->
<div class="container-fluid mt-5 bg-primary">
    <div class="row">
        <div class="col-sm-12">
            <div class="p-3 scholarship_list_wadud">
                <h1 class="text-center pt-4 text-white">IEB Scholarship List</h1>
            </div>
        </div>
    </div>
</div>
<!--End Banner of Scholarship List  -->


<!--Start of table section-->
<section class="container-fluid">
    <div class="container p-5">
        <div class="table-responsive">
            <table class="table">
                <thead class="bg-info lead">
                    <tr>
                        <th class="text-white">Name</th>
                        <th class="text-white">Award</th>
                        <th class="text-white">Deadline</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Rachel Carson Book Award</td>
                        <td>$1,000</td>
                        <td>Varies</td>
                        <td><a class="btn btn-secondary" href="<%out.print(GlobalVariable.baseUrl);%>/activities/scholarshipDetails.jsp">See Details</a></td>
                    </tr>
                    <tr>
                        <td>Rachel Carson Book Award</td>
                        <td>$1,000</td>
                        <td>Varies</td>
                        <td><a class="btn btn-secondary" href="<%out.print(GlobalVariable.baseUrl);%>/activities/scholarshipDetails.jsp">See Details</a></td>
                    </tr>
                    <tr>
                        <td>Rachel Carson Book Award</td>
                        <td>$1,000</td>
                        <td>Varies</td>
                        <td><a class="btn btn-secondary" href="<%out.print(GlobalVariable.baseUrl);%>/activities/scholarshipDetails.jsp">See Details</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
<!--End of table section-->

<%@ include file="../footer.jsp" %>
