<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/wadud.css">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>



<!-- Banner of Scholarship Page  -->
<div class="about_banner_activites">
    <h2 class="display_41_saz text-white">Technical Activites</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a class="bi_wadud" href="#">Activities </a></li>
                <li class="breadcrumb-item active" aria-current="page">Technical Activites</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of Banner of Scholarship Page -->


<!-- Starting of "More About Scholarship" Section -->
<div class="container">
    <div class="row pt-5">
        <div class="col-md-6">
            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
        <div class="col-md-6">
            <img src="<%out.print(GlobalVariable.baseUrl);
                 %>/commonUtil/assets/images/scholarship_image.jpg" class="do-img img-fluid pl-5" alt="Sholarship image.jpg" />
        </div>
    </div>

    <br/><br/>

</div>
<!-- Ending of "More About Scholarship" section -->



<%@ include file="../footer.jsp" %>
