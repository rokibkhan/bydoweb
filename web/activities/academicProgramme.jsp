<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/photo_contact_success_style.css">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<!-- Banner of Scholarship Page  -->
<div class="about_banner_activites">
    <h2 class="display_41_saz text-white">Academic Programme</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a class="bi_wadud" href="#">Activities </a></li>
                <li class="breadcrumb-item active text-dark">Academic Programme</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of Banner of Scholarship Page --> 





<section id="dhaka" class="my-5 text-center each_erc_partho1 ">
    <div class="row my-5 bg-primary">
        <div class="col pt-5">
            <div class="info-header mb-5">
                <h1 class="text-white py-3">
                    leadership, retention, training and development
                </h1>
            </div>
        </div>
    </div>
    <div class="container align-baseline">
        <div class="row pt-5">
            <div class="col-12">

                <p class="text-justify">
                    Many businesses today recognize the need to put employees? professional development at the heart of company policy. Why? Their workers are demanding it. Talented professionals want to work for a company that will help them build and advance in their career.

                    However, finance leaders may not be doing enough to make these efforts a high priority. In a recent survey by Robert Half Management Resources, about one-fifth of workers (21 percent) polled said they have little or no confidence in senior management?s ability to support their personal growth and career advancement.

                    In this tight hiring market for skilled financial talent, finance leaders can?t afford to overlook the importance of supporting their employees? career advancement. Here are seven ways to show your workers that the firm ? and their boss ? are committed to helping them grow professionally:
                </p>


                <h1 class=" text-left" style="margin-top: 20px;">1. Take a personal interest</h1>

                <p class="text-justify" style="margin-top: 20px;" >
                    Make time to meet regularly with team members on a one-to-one basis to learn about their aspirations, expectations and frustrations. This outreach can help make your staff feel valued ? and lead to greater productivity and loyalty.

                    Also, help your employees to plot a career path within the company, so they can better visualize their future at your firm. Identify specific milestones for achievement, and the supporting resources employees will likely need to tap along their journey. Clear and consistent communication about career advancement opportunities can help workers feel more engaged and empowered.
                </p>

                <img src="assets/images/Careerheader.jpg" class="img-fluid akass_img" alt="Responsive image">

                <h1 class="text-justify text-left" style="margin-top: 20px;">2. Focus on learning</h1> 

                <p class="text-justify" style="margin-top: 20px;" >
                    Continued job training and education are important to an employee?s professional development. Millennial workers, in particular, tend to covet these learning opportunities. Ensure that courses and workshops ? paid for by the company ? are part of their career plan.

                    In addition to nurturing individual needs and specific skills, help employees keep up with what's happening in the wider industry. One cost-effective method is to host monthly lunchtime sessions with guest speakers on relevant topics, such as cloud technology or tax legislation changes. Also, reward top talent by sending them to industry conferences where they can learn and network.
                </p>
                <h1 class="text-justify text-left" style="margin-top: 20px;">3. Rotate employee roles</h1>
                <p class="text-justify" style="margin-top: 20px;" >
                    The human brain thrives on variety, and job rotation is a smart way to shake up your workers? daily routine. Encourage staff to work in different but related departments or positions. (A job rotation program can help to facilitate this.) These opportunities will help them to gain new skills, more appreciation of their colleagues? duties and a better understanding of the business. Meanwhile, the company, and you, will benefit from having a more motivated, informed and well-rounded accounting and finance team.
                </p>
                <h1 class="text-justify text-left" style="margin-top: 20px;">4. Encourage mentoring</h1>
                <p class="text-justify" style="margin-top: 20px;" >
                    Establishing a formal mentoring program might be one of the smartest moves a company can make ? for itself as well as for its workers? personal and career growth.

                    Though often seen as a transfer of knowledge from veteran employees to the less experienced, mentoring works both ways. While senior staff members can offer hard-earned insights and professional guidance to their junior colleagues, they also can benefit from the fresh perspectives and technological know-how of younger generations in the workforce.
                </p>
                <h1 class="text-justify text-left" style="margin-top: 20px;">5. Support work-life balance</h1>
                <p class="text-justify" style="margin-top: 20px;" >
                    Hard work is a prerequisite for career advancement, but that doesn?t necessarily mean regularly putting in 10-12 hours a day. Encourage your employees to work smart, maximize their efficiency, and leave time and energy for interests outside of work.

                    Take a proactive approach to your team?s well-being rather than waiting for problems like stress and burnout to bubble up. Embracing flexible policies that are designed to enhance work-life balance can make a huge difference to staff morale. It helps employees find the time to do their work, enjoy their life outside of the office, and take advantage of available professional development opportunities.
                </p>
                <h1 class="text-justify text-left" style="margin-top: 20px;">6. Paint the big picture</h1>
                <p class="text-justify" style="margin-top: 20px;" >
                    Reminding employees of their unique contributions to the company?s mission adds meaning to their role ? and can increase their motivation to expand their responsibilities and advance at the firm.

                    Don?t assume they already know how their work adds value, however. A recent survey by our company found that more than half of professionals (53 percent) wish they had more insight on how their day-to-day duties make a difference to the organization. So, be sure to provide regular updates about the organization?s progress toward key objectives, and also praise and reward employees for specific achievements that help drive the business toward those goals.
                </p>
                <h1 class="text-justify text-left" style="margin-top: 20px;">7. Create a succession planning program</h1>
                <p class="text-justify" style="margin-top: 20px;" >
                    Succession planning demonstrates to valued staff members that you not only want to invest in their professional development but also see them evolve into future leaders at the firm. That is a powerful message. So, don?t push this important process to the back burner. Create a succession plan for every key position in your organization. It can help motivate employees to learn the skills and knowledge needed for career advancement. (Plus, it?s a best practice for any business.)

                    If your workers are not achieving their potential because they feel overlooked, prioritizing their professional development and career advancement is an obvious solution. Make a clear effort to cultivate a growth environment for your workers. It?s an investment that can deliver huge returns for your staff and your business.
                </p>











            </div>



        </div>


    </div>	

</div>
</section>




<%@ include file="../footer.jsp" %>
