<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/wadud.css">

<!-- Banner of Scholarship Page  -->
<div class="scholarship_banner_wadud">
    <h3 class="scholarship_banner_title_wadud">Scholarship</h3>
    <div class="jb_breadcrumb_wadud">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent">
                <li class="breadcrumb-item"><a class="bi_wadud" href="#">Activities </a></li>
                <li class="breadcrumb-item"><a class="bi_wadud" href="#">Scholarship </a></li>
                <li class="breadcrumb-item active" aria-current="page">Details</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of Banner of Scholarship Page -->


<!-- Starting of "Scholarship Details" Section -->
<div class="container">
    <div class="row p-5">
        <div class="col-lg-8 pb-5">
            <div class="row">
                <h3 class="pb-5">Rachel Carson Book Award</h3>
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <thead>
                        <tr>
                            <th>AWARD</th>
                            <th>DEADLINE</th>
                            <th> PROVIDED BY</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>$1,000</td>
                            <td>Varies</td>
                            <td>IEB</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <h3 class="pt-5 pb-2">Description</h3>
                <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>
        </div>

        <div class="col-lg-4">
            <h3 class="d-block bg-success p-3 text-white">IEB Scholarship List</h3>
            <nav class="bg-white pt-3">
                <!-- Links -->
                <ul class="navbar-nav pl-3 right_menue_wadud">
                    <li class="nav-item">
                        <a href="#">Rachel Carson Book Award </a>
                        <hr />
                    </li>
                    <li class="nav-item">
                        <a href="#">American Legion National High School Oratorical Contest</a>
                        <hr />
                    </li>
                    <li class="nav-item">
                        <a href="#">Samsung American Legion Scholarship Program</a>
                        <hr />
                    </li>
                    <li class="nav-item">
                        <a href="#">Samsung American Legion Scholarship Program</a>
                        <hr />
                    </li>
                    <li class="nav-item">
                        <a href="#">Samsung American Legion Scholarship Program</a>
                        <hr />
                    </li>
                    <li class="nav-item">
                        <a href="#">Samsung American Legion Scholarship Program</a>
                        <hr />
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<!-- Ending of "Scholarship Details" section -->


<%@ include file="../footer.jsp" %>
