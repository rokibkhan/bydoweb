<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/photo_contact_success_style.css">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<!-- Banner of Scholarship Page  -->
<div class="about_banner_activites">
    <h2 class="display_41_saz text-white"> Project Management</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a class="bi_wadud" href="#">Activities </a></li>
                <li class="breadcrumb-item active text-dark">Project Management</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of Banner of Scholarship Page --> 

<!-- Dhaka Centre Starts -->

<section id="dhaka" class="my-5 text-center each_erc_partho1 ">
    <div class="row my-5 bg-primary">
        <div class="col pt-5">
            <div class="info-header mb-5">
                <h1 class="text-white py-3">
                    Leading teams and delivering successful outcomes.
                </h1>
            </div>
        </div>
    </div>
    <div class="container align-baseline">
        <div class="row pt-5">
            <div class="col-12">




                <h1 class=" text-left" style="margin-top: 20px;">Project management</h1>

                <p class="text-justify" style="margin-top: 20px;" >
                    The Institution of Engineers, Bangladesh (IEB) is the most prestigious National Professional Organization of the country. It is registered under the Societies Registration Act of the country. IEB includes all disciplines of engineering. Currently, it has in its roll more than 41,545 engineers with about 30% in the category of Fellows, 60% Members and the rest as Associate Members. In addition there are a good number of Student Members. Since its establishments, IEB has been promoting and disseminating knowledge and practice of engineering and science. On of the major goal of IEB is to ensure the professional excellence and continuous professional development of the engineers in the country. It has also been working relentlessly to establish close and co-operation with the other professional bodies both in Bangladesh and outside the country.

                    The Institutions mission has always been to serve the teeming millions through the practice of engineering science and continuously improving the professional standards of its members. The IEB as a national forum of engineers also addresses the common problems confronting the engineers. On different occasions, IEB came forward with its clear vision of the problems of development and formulated specific suggestions on these issues. IEB prepared and presented its recommendations on the Power Sector Reform Policy, Flood Control and Management, Public Administration Reform, Traffic Problem, National Pay Scale, Information Technology, Renewable Energy, Problems of Dhaka Metropolis and Integrated Approach Towards Solution of Endemic Problem faced by the public etc. to the Government on different occasions. Besides, IEB is also playing a significant role for the professional development of its valued members.
                </p>



            </div>



        </div>
        <div class="container">

            <div class="row my-4">
                <div class="col-md offset-md-0 mb-5 offset-sm-4">
                    <a href="career.html" style="    text-decoration: none; color: #212529;">
                        <div  class="card"  style="min-height:200px;max-width:20rem">
                            <div class="card-body" >
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/Career-Advancemen.jpg" alt="" class="img-fluid rounded w-60 mb-3">
                                <p style="margin-top: 14px;" class="lead font-weight-bold">Project <br>Management Practice</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                    <a href="project.html" style="    text-decoration: none; color: #212529;">
                        <div class="card" style="min-height:200px;max-width:20rem">
                            <div class="card-body">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/Project.jpg" alt="" class="img-fluid rounded w-60 mb-3">
                                <p style="margin-top: 14px;" class="lead font-weight-bold" >Project<br>Fundamentals
                                </p>

                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                    <a href="inhouse.html" style="    text-decoration: none; color: #212529;">
                        <div class="card" style="min-height:200px;max-width:20rem">
                            <div class="card-body">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/training.jpg" alt="" class="img-fluid rounded w-60 mb-3">
                                <p style="margin-top: 14px;" class="lead font-weight-bold">Diploma of <br>Project Management</p>

                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                    <a href="training.html" style="    text-decoration: none; color: #212529;">
                        <div class="card" style="min-height:200px;max-width:20rem;">
                            <div class="card-body">
                                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/technical-training.jpg" alt="" class="img-fluid rounded w-60 mb-3">
                                <p style="margin-top: 14px;" class="lead font-weight-bold">Enhancing <br>Project Performance</p>

                            </div>
                        </div>
                    </a>    
                </div>
            </div>

        </div>

        <div class="row pt-5">
            <div class="col-12">






                <h1 class=" text-left" style="margin-top: 20px;">Project management for engineers</h1>

                <p class="text-justify" style="margin-top: 20px;" >
                    Choose from a variety of study options to attain project management credentials while continuing to work. You can study online or in a blended format that combines online learning with in-person workshops. You can even have your prior experience assessed, with the potential to contribute to completion of this course.

                    Our program is tailored to your needs, with your previous experience and industry?s demands considered to ensure you learn the skills necessary for your specific occupation.
                </p>

            </div>



        </div>


    </div>  



</section>



<%@ include file="../footer.jsp" %>
