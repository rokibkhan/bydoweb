<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<!-- Login & Register & About CSS -->
<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/photo_contact_success_style.css">

        
 <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<!-- Banner of Scholarship Page  -->
<div class="about_banner_activites">
    <h2 class="display_41_saz text-white"> ERC Members</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a class="bi_wadud" href="#">Activities </a></li>
                <li class="breadcrumb-item active">Engineer's Recreational Centre</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of Banner of Scholarship Page -->       
        
        

<!-- Start ERC Section -->
<section class="contact py-5" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 mb-3">
                <div class="contact-single bg-light p-3 text-center">
                    <a href="#dhaka"><i class="fas fa-map-marker-alt"></i></a>
                    <h5 class="py-3 font-weight-bold">Dhaka</h5>
                    <div class="border border-dark w-25 mx-auto"></div>
                    <p class="pt-3">152/7, Some Road</p>
                    <p>Dhaka, Bangladesh.</p>
                </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 mb-3">
                <div class="contact-single bg-light p-3 text-center">
                    <a href="#chittagong"><i class="fas fa-map-marker-alt"></i></a>
                    <h5 class="py-3 font-weight-bold">Chittagong</h5>
                    <div class="border border-dark w-25 mx-auto"></div>
                    <p class="pt-3">152/7, Some Road</p>
                    <p>Chittagong, Bangladesh.</p>
                </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 mb-3" >
                <div class="contact-single bg-light p-3 text-center">
                    <a href="#rajshahi"><i class="fas fa-map-marker-alt"></i></a>
                    <h5 class="py-3 font-weight-bold">Rajshahi</h5>
                    <div class="border border-dark w-25 mx-auto"></div>
                    <p class="pt-3">152/7, Some Road</p>
                    <p>Rajshahi, Bangladesh.</p>
                </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 mb-3">
                <div class="contact-single bg-light p-3 text-center">
                    <a href="#khulna"><i class="fas fa-map-marker-alt"></i></a>
                    <h5 class="py-3 font-weight-bold">Khulna</h5>
                    <div class="border border-dark w-25 mx-auto"></div>
                    <p class="pt-3">152/7, Some Road</p>
                    <p>Khulna, Bangladesh.</p>
                </div>
            </div>

        </div>

    </div>
</section>
<!-- End ERC Section -->

<!-- Dhaka Centre Starts -->

<section id="dhaka" class="text-center each_erc_partho ">
    <div class="row bg-primary">
        <div class="col pt-5">
            <div class="info-header mb-5">
                <h1 class="text-white py-3">
                    Engineers' Recreation Centre, Dhaka
                </h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row my-4" class="justify-content-around">
            <div class="col-lg-5 offset-lg-2 col-12 offset-4 mb-5 text-center">
                <div class="card" style="min-height:300px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_1.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Xccccccccccccccccccccccccccccccccccccc</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-12 offset-4 offset-lg-0 text-center mb-5">
                <div class="card" style="min-height:300px;width:20rem;">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_3.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>

                    </div>
                </div>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-lg-4 col-md-4 mb-5 offset-sm-4 offset-md-0">
                <div class="card" style="min-height:250px;max-width:20rem;">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_6.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-5 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:250px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-5 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:250px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-md offset-md-0 mb-5 offset-sm-4">
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_6.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                <div class="card" style="min-height:200px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-4 ">
            <div class="col-md mb-5 mr-3 offset-sm-4 offset-md-0">
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_6.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md mb-5 mr-3 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md mb-5 mr-3 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md mb-5 no-gutters offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:200px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Dhaka Centre Ends -->

<!-- Chittagong Centre Starts -->
<section id="chittagong" class="my-5 text-center each_erc_partho ">
    <div class="row my-5 bg-success">
        <div class="col pt-5">
            <div class="info-header mb-5">
                <h1 class="text-white py-3">
                    Engineers' Recreation Centre, Chittagong
                </h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row my-4" class="justify-content-around">
            <div class="col-lg-5 offset-lg-2 col-12 offset-4 mb-5 text-center">
                <div class="card" style="min-height:300px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_1.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Xccccccccccccccccccccccccccccccccccccc</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-12 offset-4 offset-lg-0 text-center mb-5">
                <div class="card" style="min-height:300px;width:20rem;">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_3.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>

                    </div>
                </div>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-lg-4 col-md-4 mb-5 offset-sm-4 offset-md-0">
                <div class="card" style="min-height:250px;max-width:20rem;">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_6.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-5 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:250px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-5 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:250px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-md offset-md-0 mb-5 offset-sm-4">
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_6.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                <div class="card" style="min-height:200px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-4 ">
            <div class="col-md mb-5 mr-3 offset-sm-4 offset-md-0">
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_6.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md mb-5 mr-3 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md mb-5 mr-3 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md mb-5 no-gutters offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:200px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Chittagong Centre Ends -->

<!-- Rajshahi Centre Starts -->
<section id="rajshahi" class="my-5 text-center each_erc_partho ">
    <div class="row my-5 bg-danger">
        <div class="col pt-5">
            <div class="info-header mb-5">
                <h1 class="text-white py-3">
                    Engineers' Recreation Centre, Rajshahi
                </h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row my-4" class="justify-content-around">
            <div class="col-lg-5 offset-lg-2 col-12 offset-4 mb-5 text-center">
                <div class="card" style="min-height:300px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_1.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Xccccccccccccccccccccccccccccccccccccc</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-12 offset-4 offset-lg-0 text-center mb-5">
                <div class="card" style="min-height:300px;width:20rem;">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_3.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>

                    </div>
                </div>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-lg-4 col-md-4 mb-5 offset-sm-4 offset-md-0">
                <div class="card" style="min-height:250px;max-width:20rem;">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_6.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-5 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:250px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-5 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:250px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-md offset-md-0 mb-5 offset-sm-4">
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_6.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                <div class="card" style="min-height:200px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-4 ">
            <div class="col-md mb-5 mr-3 offset-sm-4 offset-md-0">
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_6.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md mb-5 mr-3 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md mb-5 mr-3 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md mb-5 no-gutters offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:200px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Rajshahi Centre Ends -->


<!-- Khulna Centre Starts -->
<section id="khulna" class="my-5 text-center each_erc_partho ">
    <div class="row my-5 bg-warning">
        <div class="col pt-5">
            <div class="info-header mb-5">
                <h1 class="text-white py-3">
                    Engineers' Recreation Centre, Khulna
                </h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row my-4" class="justify-content-around">
            <div class="col-lg-5 offset-lg-2 col-12 offset-4 mb-5 text-center">
                <div class="card" style="min-height:300px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_1.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Xccccccccccccccccccccccccccccccccccccc</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-12 offset-4 offset-lg-0 text-center mb-5">
                <div class="card" style="min-height:300px;width:20rem;">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_3.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>

                    </div>
                </div>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-lg-4 col-md-4 mb-5 offset-sm-4 offset-md-0">
                <div class="card" style="min-height:250px;max-width:20rem;">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_6.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-5 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:250px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-5 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:250px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-md offset-md-0 mb-5 offset-sm-4">
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_6.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md offset-md-0 mb-5 offset-sm-4"  >
                <div class="card" style="min-height:200px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-4 ">
            <div class="col-md mb-5 mr-3 offset-sm-4 offset-md-0">
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body"  >
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_6.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md mb-5 mr-3 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md mb-5 mr-3 offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:200px;max-width:20rem">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
            <div class="col-md mb-5 no-gutters offset-sm-4 offset-md-0"  >
                <div class="card" style="min-height:200px;max-width:20rem;">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/executive_img_5.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <p class="lead font-weight-bold">Mr. Y</p>
                        <p>Chairman</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Khulna Centre Ends -->


<%@ include file="../footer.jsp" %>
