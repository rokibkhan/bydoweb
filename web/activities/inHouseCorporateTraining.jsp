<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/photo_contact_success_style.css">


<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>

<!-- Banner of Scholarship Page  -->
<div class="about_banner_activites">
    <h2 class="display_41_saz text-white"> In House Corporate Training</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a class="bi_wadud" href="#">Activities </a></li>
                <li class="breadcrumb-item active text-dark">In House Corporate Training</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of Banner of Scholarship Page -->  


<!-- Dhaka Centre Starts -->

<section id="dhaka" class="my-5 text-center each_erc_partho1 ">
    <div class="row my-5 bg-primary">
        <div class="col pt-5">
            <div class="info-header mb-5">
                <h1 class="text-white py-3">
                    Run by experts, tailored to your career.
                </h1>
            </div>
        </div>
    </div>
    <div class="container align-baseline">
        <div class="row pt-5">
            <div class="col-12">




                <h1 class=" text-left" style="margin-top: 20px;">IEB in-house corporate training service</h1>

                <p class="text-justify" style="margin-top: 20px;" >
                    When your business depends on the skills of engineers, their professional development is more than an interest ? it?s an investment.

                    IEB is a trusted provider of in-house corporate training for businesses across Bangladesh. We work with you to customise and deliver cost-effective training programs for your teams. If you need training for groups larger than six, our in-house program will provide even greater value for your business.

                    IEB provides consultation and support, program customisation and multiple delivery options. There are over 70 courses and workshops to choose from, covering technical, business, and management skills. All of our courses contribute to your employees? Continuing Professional Development (CPD), providing added benefit in maintaining Chartered Status.
                </p>


                <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/inhtraining.jpg" class="img-fluid akass_img" alt="Responsive image">

            </div>

        </div>


    </div>  







</section>





<%@ include file="../footer.jsp" %>
