<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<link rel="stylesheet" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/wadud.css">

<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>
<%

    Session dbsessionRaD = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrxRaD = null;
    dbtrxRaD = dbsessionRaD.beginTransaction();

    Query pageSQL = dbsessionRaD.createSQLQuery("SELECT ni.id_page, ni.page_category, ni.page_title, "
            + "ni.page_short_desc, ni.page_desc, ni.published, ni.add_term "
            + "FROM  page_info ni WHERE ni.id_page='7'");

    Object[] pageObj = null;
    String pageId = "";
    String pageCategory = "";
    String pageTitle = "";
    String pageShortDetailsInfoXn = "";
    String pageDetailsInfoXn = "";
    String pagePublished = "";
    String pageAddUser = "";
    String pageAddTermInfoXn = "";

    if (!pageSQL.list().isEmpty()) {
        for (Iterator pageIt1 = pageSQL.list().iterator(); pageIt1.hasNext();) {

            pageObj = (Object[]) pageIt1.next();
            pageCategory = pageObj[1].toString().trim();

            pageTitle = pageObj[2].toString().trim();
            pageShortDetailsInfoXn = pageObj[3].toString().trim();
            pageDetailsInfoXn = pageObj[4].toString().trim();
            pageAddTermInfoXn = pageObj[6].toString().trim();

        }

    }

%>




<!-- Start of navigation -->
<div class="about_banner_activites">
    <h2 class="display_41_saz text-white"> R & D Activites</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Activities</a></li>
                <li class="breadcrumb-item active">R &amp; D Activities</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of navigation -->


<!-- Starting of "More About Scholarship" Section -->
<div class="container">
    <div class="row pt-5">
        <%=pageDetailsInfoXn%>
    </div>

    <br/><br/>

</div>
<!-- Ending of "More About Scholarship" section -->

<%    dbsessionRaD.clear();
    dbsessionRaD.close();
%>

<%@ include file="../footer.jsp" %>
