<%@page import="org.hibernate.Session"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.appul.util.GlobalVariable"%>

<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Query newsSQL = null;
    Object newsObj[] = null;
    String newsId = "";
    String newsTitle = "";
    String newsSlug = "";
    String newsShortDetails = "";
    String newsShortDetails1 = "";
    String newsShortDetailsX = "";
    String newsDetails = "";
    String newsDateTime = "";
    String featureImage = "";
    String featureImage1 = "";
    String featureImageUrl = "";
    String newsInfoFirstContainer = "";
    String newsInfoContainer = "";
    int nw = 1;


%>

<!-- start update_news_wrap -->
<section class="update_news_wrap">
    <div class="container custom_pad">
        <div class="row">
            <div class="col-12">
                <h3 class="sec_title">Updated News <span class="float-right"><a href="#" class="view_all">View All</a></span></h3>
            </div>
        </div>


        <%                newsSQL = dbsession.createSQLQuery("SELECT ni.id_news, ni.news_title, ni.news_short_desc, ni.news_desc, "
                    + "ni.news_date_time, ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4 "
                    + "FROM  news_info ni ORDER BY ni.id_news DESC "
                    + "LIMIT 0,4");

            if (!newsSQL.list().isEmpty()) {
                for (Iterator it1 = newsSQL.list().iterator(); it1.hasNext();) {

                    newsObj = (Object[]) it1.next();
                    newsId = newsObj[0].toString().trim();
                    newsTitle = newsObj[1].toString().trim();
                    newsShortDetails = newsObj[2].toString().trim();
                    
                    newsShortDetailsX = newsShortDetails.substring(0,100);
                    
                    System.out.println("newsShortDetailsX 0,200:: "+newsShortDetailsX);
                    
                    
                    newsShortDetails1 = newsShortDetails;
                    
                    newsDetails = newsObj[3].toString().trim();

                    newsDateTime = newsObj[4].toString().trim();

                    featureImage = newsObj[5] == null ? "" : newsObj[5].toString().trim();
                    featureImageUrl = GlobalVariable.imageNewsDirLink + featureImage;

                    featureImage1 = "<img width=\"200\" src=\"" + featureImageUrl + "\" alt=\"" + newsTitle + "\">";

                    //       String updateProfileUrl = GlobalVariable.baseUrl + "/newsManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&newsIdX=" + newsId + "&selectedTab=profile";
                    //      String updateChangePassUrl = GlobalVariable.baseUrl + "/newsManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&newsIdX=" + newsId + "&selectedTab=changePass";
                    if (nw == 1) {
                        newsInfoFirstContainer = newsInfoFirstContainer + ""
                                + "<div class=\"row\">"
                                + "<div class=\"col-12\">"
                                + "<div class=\"news_large bg_yellow\">"
                                + "<div class=\"row d_align\">"
                                + "<div class=\"col-lg-6 col-md-12 col-sm-12 col-xs-12\">"
                                + "<div class=\"news_img\">"
                                + "<img src=\"" + featureImageUrl + "\" alt=\"img\">"
                                + "</div>"
                                + "</div>"
                                + "<div class=\"col-lg-6 col-md-12 col-sm-12 col-xs-12\">"
                                + "<div class=\"details_content color_width\">"
                                + "<p class=\"news_date\">30 Oct, 2018</p>"
                                + "<a href=\"#\"><h3 class=\"news_title\">" + newsTitle + "</h3></a>"
                                + "<p class=\"news_para\">" + newsShortDetails + "</p>"                                
                                + "<a href=\"#\" class=\"view_details\">View Details</a>"
                                + "</div>"
                                + "</div>"
                                + "</div>"
                                + "</div>"
                                + "</div>"
                                + "</div>";

                    }

                    if (nw > 1) {
                        newsInfoContainer = newsInfoContainer + ""
                                + "<div class=\"col-lg-4 col-md-6 col-sm-12\">"
                                + "<div class=\"news_single_item\">"
                                + "<div class=\"news_img\">"
                                + "<img src=\"" + featureImageUrl + "\" style=\" height:250px;\" alt=\"img\">"
                                + "</div>"
                                + "<div class=\"details_content\"  style=\"min-height:335px;\">"
                                + "<p class=\"news_date\">30 Oct, 2018</p>"
                                + "<a href=\"#\"><h3 class=\"news_title\">" + newsTitle + "</h3></a>"
                                + "<p class=\"news_para\">" + newsShortDetailsX + "</p>"
                                + "<a href=\"#\" class=\"view_details\">View Details</a>"
                                + "</div>"
                                + "</div>"
                                + "</div>";
                    }

                    nw++;
                }
            }

        %>

        <%=newsInfoFirstContainer%>

        <div class="row">
            <div class="col-12">
                <div class="news_small">
                    <div class="row">

                        <%=newsInfoContainer%>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="news_small">
                    <div class="row">

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="news_single_item">
                                <div class="news_img">
                                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_img_1.jpg" alt="img">
                                </div>
                                <div class="details_content">
                                    <p class="news_date">30 Oct, 2018</p>
                                    <a href="#"><h3 class="news_title">Regulate the professional</h3></a>
                                    <p class="news_para">Regulate the professional activities and assist maintaining high standards in the general </p>
                                    <a href="#" class="view_details">View Details</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="news_single_item">
                                <div class="news_img">
                                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_img_2.jpg" alt="img">
                                </div>
                                <div class="details_content">
                                    <p class="news_date">30 Oct, 2018</p>
                                    <a href="#"><h3 class="news_title">Regulate the professional</h3></a>
                                    <p class="news_para">Regulate the professional activities and assist maintaining high standards in the general </p>
                                    <a href="#" class="view_details">View Details</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="news_single_item">
                                <div class="news_img">
                                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_img_3.jpg" alt="img">
                                </div>
                                <div class="details_content">
                                    <p class="news_date">30 Oct, 2018</p>
                                    <a href="#"><h3 class="news_title">Regulate the professional</h3></a>
                                    <p class="news_para">Regulate the professional activities and assist maintaining high standards in the general </p>
                                    <a href="#" class="view_details">View Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center learn_more_btn">
                    <a href="#" class="learn_more">Load More</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end update_news_wrap -->