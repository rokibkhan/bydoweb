<%@ include file="../header.jsp" %>

<!-- News & Events & Single Event CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/news_events_style.css" rel="stylesheet">

<!-- Start of top news section -->
<section class="top-news m-5">
    <div class="container">
        <div class="grid">
            <div class="grid-item grid-item--width2 grid-item--height1 grid-item-1" >
                <div class="overlay-text text-center">
                    <p class="overlay-p"> 30 Oct, 2018</p>
                    <h2 class="overlay-hw2-h">Regulate the profession 01</h2>
                </div>
                <img class="img-fluid news-image" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img.jpg" alt="Masonry image 5" />
            </div>
            <div class="grid-item grid-item-2" >
                <div class="overlay-text">
                    <p class="overlay-p"> 30 Oct, 2018</p>
                    <h2 class="overlay-h">Regulate the profession 02</h2>
                </div>
                <img class="img-fluid news-image" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_3.jpg" alt="Masonry image 5" />
            </div>
            <div class="grid-item grid-item-3" >
                <div class="overlay-text">
                    <p class="overlay-p"> 30 Oct, 2018</p>
                    <h2 class="overlay-h">Regulate the profession 03</h2>
                </div>
                <img class="img-fluid news-image" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/featured_img_4.jpg" alt="Masonry image 5" />
            </div>

        </div>
    </div>
</section>
<!-- end of top news section -->

<!-- Start of more news section -->
<section class="other-news m-5" style="display:none;">
    <div class="container">
        <h2 class="text-center mx-auto my-5 w-50 py-3 border border-dark font-weight-bold text-dark"> More News</h2>
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_large_img.jpg" class="card-img-top" alt="News Image">
                    <div class="card-body">
                        <p class="news_date">30 Oct, 2018</p>
                        <h5 class="card-title my-3">Regulate the professional</h5>
                        <p class="card-text mb-3">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-outline-success border-dark details-link">View Details</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_large_img.jpg" class="card-img-top" alt="News Image">
                    <div class="card-body">
                        <p class="news_date">30 Oct, 2018</p>
                        <h5 class="card-title my-3">Regulate the professional</h5>
                        <p class="card-text mb-3">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-outline-success border-dark details-link">View Details</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_large_img.jpg" class="card-img-top" alt="News Image">
                    <div class="card-body">
                        <p class="news_date">30 Oct, 2018</p>
                        <h5 class="card-title my-3">Regulate the professional</h5>
                        <p class="card-text mb-3">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-outline-success border-dark details-link">View Details</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md-4">
                <div class="card">
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_large_img.jpg" class="card-img-top" alt="News Image">
                    <div class="card-body">
                        <p class="news_date">30 Oct, 2018</p>
                        <h5 class="card-title my-3">Regulate the professional</h5>
                        <p class="card-text mb-3">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-outline-success border-dark details-link">View Details</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_large_img.jpg" class="card-img-top" alt="News Image">
                    <div class="card-body">
                        <p class="news_date">30 Oct, 2018</p>
                        <h5 class="card-title my-3">Regulate the professional</h5>
                        <p class="card-text mb-3">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-outline-success border-dark details-link">View Details</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_large_img.jpg" class="card-img-top" alt="News Image">
                    <div class="card-body">
                        <p class="news_date">30 Oct, 2018</p>
                        <h5 class="card-title my-3">Regulate the professional</h5>
                        <p class="card-text mb-3">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-outline-success border-dark details-link">View Details</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_large_img.jpg" class="card-img-top" alt="News Image">
                    <div class="card-body">
                        <p class="news_date">30 Oct, 2018</p>
                        <h5 class="card-title my-3">Regulate the professional</h5>
                        <p class="card-text mb-3">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-outline-success border-dark details-link">View Details</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_large_img.jpg" class="card-img-top" alt="News Image">
                    <div class="card-body">
                        <p class="news_date">30 Oct, 2018</p>
                        <h5 class="card-title my-3">Regulate the professional</h5>
                        <p class="card-text mb-3">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-outline-success border-dark details-link">View Details</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/news_large_img.jpg" class="card-img-top" alt="News Image">
                    <div class="card-body">
                        <p class="news_date">30 Oct, 2018</p>
                        <h5 class="card-title my-3">Regulate the professional</h5>
                        <p class="card-text mb-3">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-outline-success border-dark details-link">View Details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of more news section -->

<!-- Start of Pagination -->
<nav class="other-news news-pagination" style="display:none;">
    <ul class="pagination justify-content-center pagination-lg">
        <li class="page-item disabled">
            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
        </li>
        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
            <a class="page-link" href="#">Next</a>
        </li>
    </ul>
</nav>
<!-- End of Pagination -->


<%@ include file="../footer.jsp" %>
