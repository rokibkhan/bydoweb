<%@page import="java.util.Iterator"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Query"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="../header.jsp" %>

<%
    Session dbsessionNews = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsessionNews.beginTransaction();

    String newsIdX = request.getParameter("newsIdX") == null ? "" : request.getParameter("newsIdX").trim();

    // DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateFormatEventM = new SimpleDateFormat("MMM");
    DateFormat dateFormatEventD = new SimpleDateFormat("dd");
    DateFormat dateFormatEventY = new SimpleDateFormat("Y");

    Date dateEventM = null;
    Date dateEventD = null;
    Date dateEventY = null;
    String newDateEventM = "";
    String newDateEventD = "";
    String newDateEventY = "";

    Query newsSQL = null;
    Object newsObj[] = null;
    String newsId = "";
    String newsTitle = "";
    String newsSlug = "";
    String newsShortDetails = "";
    String newsShortDetails1 = "";
    String newsShortDetailsX = "";
    String newsDetails = "";
    String newsDateTime = "";
    String featureImage = "";
    String featureImage1 = "";
    String featureImageUrl = "";
    String newsInfoFirstContainer = "";
    String newsInfoContainer = "";
    String newsDetailsUrl = "";
    int nw = 1;

    newsSQL = dbsessionNews.createSQLQuery("SELECT ni.id_news, ni.news_title, ni.news_short_desc, ni.news_desc, "
            + "ni.news_date, ni.feature_image, ni.image1,ni.image2,ni.image3,ni.image4  "
            + "FROM  news_info ni "
            + "WHERE ni.id_news ='" + newsIdX + "'");

    if (!newsSQL.list().isEmpty()) {
        for (Iterator itNews = newsSQL.list().iterator(); itNews.hasNext();) {

            newsObj = (Object[]) itNews.next();
            newsId = newsObj[0].toString().trim();
            newsTitle = newsObj[1].toString().trim();
            newsShortDetails = newsObj[2].toString().trim();

            // newsShortDetailsX    = newsShortDetails.substring(0, 100);
            //      System.out.println(" newsShortDetailsX0,200:: " +newsShortDetailsX );
            newsShortDetails1 = newsShortDetails;

            newsDetails = newsObj[3].toString().trim();

            //   newsDateTime = newsObj[4] == null ? "" : newsObj[4].toString().trim();
            //  if (newsDateTime.equals("")) {
            // newsDateTime = newsObj[10].toString().trim();
            newsDateTime = newsObj[4] == null ? "" : newsObj[4].toString().trim();
            System.out.println(" newsDateTime :: " + newsDateTime);
            //   }

            dateEventM = dateFormatEvent.parse(newsDateTime);
            newDateEventM = dateFormatEventM.format(dateEventM);

            dateEventD = dateFormatEvent.parse(newsDateTime);
            newDateEventD = dateFormatEventD.format(dateEventD);

            dateEventY = dateFormatEvent.parse(newsDateTime);
            newDateEventY = dateFormatEventY.format(dateEventY);

            featureImage = newsObj[5] == null ? "" : newsObj[5].toString().trim();
            featureImageUrl = GlobalVariable.imageNewsDirLink + featureImage;

            featureImage1 = "<img width=\"200\" src=\"" + featureImageUrl + "\" alt=\"" + newsTitle + "\">";

            newsDetailsUrl = GlobalVariable.baseUrl + "/news/newsDetails.jsp?newsIdX=" + newsId;
        }
    }

%>

<section>

    <nav aria-label="breadcrumb"> 
        <div class="container">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">News</a></li>
                <li class="breadcrumb-item active" aria-current="page">News Details</li>
            </ol>
        </div>
    </nav>
</section>

<!-- Login & Register & About CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about.css" rel="stylesheet">

<!-- News front image section -->
<!--<section class="news-banner">
    <div class="conainer">
        <img src="<%//out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/success_story_img_1.jpg" class="img-fluid banner-img" alt="Front Image">
    </div>
</section>-->
<!-- News front image section -->

<!-- Start of details of news -->
<section class="news-single-content pt-0">
    <div class="container">
        <h2 class="display-41"><%=newsTitle%></h2>
        <div class="row">
            <div class="col-12">
                <div class="news-content">
                    <%=newsDetails%>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of details of news -->

<!-- Start of more news section -->
<section>
    <div class="container">
        <div class="post-heading pt-5">
            <h3 class="text-uppercase font-weight-bold">More News </h3>
            <hr class="custom-horizontal-border mr-5" />
        </div>
        <div class="row mb-5">
            <%
                Query newsMoreSQL = null;
                Object newsMoreObj[] = null;

                String newsMoreId = "";
                String newsMoreTitle = "";

                String newsMoreFeatureImage = "";
                String newsMoreFeatureImageUrl = "";
                String newsMoreFeatureImage1 = "";
                String newsMoreDetailsUrl = "";

                newsMoreSQL = dbsessionNews.createSQLQuery("SELECT ni.id_news, ni.news_title,"
                        + "ni.news_date, ni.feature_image "
                        + "FROM  news_info ni "
                        + "WHERE  ni.id_news !='" + newsIdX + "' "
                        + "ORDER BY ni.id_news DESC "
                        + "LIMIT 0,4");

                if (!newsMoreSQL.list().isEmpty()) {
                    for (Iterator itNewsMore = newsMoreSQL.list().iterator(); itNewsMore.hasNext();) {

                        newsMoreObj = (Object[]) itNewsMore.next();
                        newsMoreId = newsMoreObj[0].toString().trim();
                        newsMoreTitle = newsMoreObj[1].toString().trim();

                        newsMoreFeatureImage = newsMoreObj[3] == null ? "" : newsMoreObj[3].toString().trim();
                        newsMoreFeatureImageUrl = GlobalVariable.imageNewsDirLink + newsMoreFeatureImage;

                        newsMoreFeatureImage1 = "<img width=\"200\" src=\"" + newsMoreFeatureImageUrl + "\" alt=\"" + newsMoreTitle + "\" class=\"img-fluid news-image  mb-3\" >";

                        newsMoreDetailsUrl = GlobalVariable.baseUrl + "/news/newsDetails.jsp?newsIdX=" + newsMoreId;
            %>

            <div class="col-md-3 col-sm-6">
                <div class="post-content pt-3">
                    <div class="post-content-img">
                        <%=newsMoreFeatureImage1%>
                        <div class="post-content-img-overlay d-flex align-items-center justify-content-center" style="height: 50%;"></div>
                        <a href="<%=newsMoreDetailsUrl%>" class="text-primary font-weight-bold news-link"><%=newsMoreTitle%></a>
                    </div>
                </div>
            </div>

            <%
                    }
                }
            %>




        </div>
    </div>
</section>
<!-- End of more news section -->
<%
    dbsessionNews.clear();
    dbsessionNews.close();

%>

<%@ include file="../footer.jsp" %>
