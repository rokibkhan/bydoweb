<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<!-- Login & Register & About CSS -->
        <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style_login_register_about.css" rel="stylesheet">

<!-- Centre List Starts -->
<section id="centre_list">
    <div id="bg-img-ieb_mas" class="py-5 mb-4">
        <h2 style="color:#fff" class="text-center py-3 mx-auto w-50 font-weight-bold text-light"> IEB Sub-Centres</h2>
    </div>
    <div class="container">

        <div class="row text-center">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2 col-sm-12">
                        <a href="#dhaka" class="fas fa-angle-double-right btn btn-primary btn-block"> Dhaka</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#khulna" class="fas fa-angle-double-right btn btn-secondary btn-block"> Khulna</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#rajshahi" class="fas fa-angle-double-right btn btn-success btn-block"> Rajshahi</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#comilla" class="fas fa-angle-double-right btn btn-info btn-block"> Comilla</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#sylhet" class="fas fa-angle-double-right btn btn-danger btn-block"> Sylhet</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#barisal" class="fas fa-angle-double-right btn btn-dark btn-block"> Barisal</a></li>
                    </div>
                </div>
            </div>	
            <div class="col-md-12 mt-4">
                <div class="row">
                    <div class="col-md-2 col-sm-12">
                        <a href="#mymensingh" class="fas fa-angle-double-right btn btn-primary btn-block"> Mymensingh</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#rangpur" class="fas fa-angle-double-right btn btn-secondary btn-block"> Rangpur</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#ghorasal" class="fas fa-angle-double-right btn btn-success btn-block"> Ghorasal</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#bogra" class="fas fa-angle-double-right btn btn-info btn-block"> Bogra</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#gazipur" class="fas fa-angle-double-right btn btn-danger btn-block"> Gazipur</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#narayangonj" class="fas fa-angle-double-right btn btn-dark btn-block"> Narayangonj</a></li>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <div class="row">
                    <div class="col-md-2 col-sm-12">
                        <a href="#rangadia" class="fas fa-angle-double-right btn btn-primary btn-block"> Rangadia</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#jessore" class="fas fa-angle-double-right btn btn-secondary btn-block"> Jessore</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#ashugonj" class="fas fa-angle-double-right btn btn-success btn-block"> Ashugonj</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#faridpur" class="fas fa-angle-double-right btn btn-info btn-block"> Faridpur</a></li>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="#dinajpur" class="fas fa-angle-double-right btn btn-danger btn-block"> Dinajpur</a></li>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Centre List Ends -->

<!-- Dhaka Centre Starts -->
<section id="dhaka" class="mt-4 text-center each_centre each_centre_mas">
    <div class="bg-danger info-header mb-5">
        <h1 class="text-light py-3">
            Dhaka Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card mb-4">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04037.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. WALIULLAH SIKDER</h5>
                        <p> F/04037</p>
                        <p class="font-weight-bold">CHAIRMAN</p>

                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-07893.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. MUSLIM UDDIN</h5>
                        <p>F/07893</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-07414.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. MOZAMMEL HAQUE</h5>
                        <p>F/07414</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-05333.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. SHAHADAT HOSSAIN (SHIBLU), PEng.</h5>
                        <p>F/05333</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Dhaka Centre Ends -->

<!-- Chittagong Centre Starts -->
<section id="chittagong" class="text-center each_centre each_centre_mas">
    <div class="bg-info info-header mb-5">
        <h1 class="text-light py-3">
            Chittagong Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">
        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04198.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>PROF. DR. ENGR. MD. RAFIQUL ALAM</h5>
                        <p> F/04198</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-05028.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. PRABIR KUMAR SEN</h5>
                        <p>F/05028</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-03939.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. PRABIR KUMAR DEY</h5>
                        <p>F/03939</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-05580.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. RAFIQUL ISLAM MANIK</h5>
                        <p>F/05580</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Chittagong Centre Ends -->

<!-- Khulna Centre Starts -->
<section id="khulna" class="text-center each_centre each_centre_mas">
    <div class="bg-danger info-header mb-5">
        <h1 class="text-light py-3">
            Khulna Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">
        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04259.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. SHAFIQUE UDDIN</h5>
                        <p> F/04259</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-10830.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. DILU ARA</h5>
                        <p>F/10830</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-02671.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. KAMALUDDIN AHMED</h5>
                        <p>F/02671</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-11148.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>DR. ENGR. SOBAHAN MIA</h5>
                        <p>F/11148</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Khulna Centre Ends -->

<!-- Rajshahi Centre Starts -->
<section id="rajshahi" class="text-center each_centre each_centre_mas">
    <div class="bg-info info-header mb-5">
        <h1 class="text-light py-3">
            Rajshahi Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">

                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-02725.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. LUTFUR RAHMAN</h5>
                        <p> F/02725</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/m.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5></h5>
                        <p>F/0000</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-11105.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. NIZAMUL HAQUE SARKER</h5>
                        <p>F/11105.jpg</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/M-24325.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. TAREQ MOSARRAF</h5>
                        <p>M/24325</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Rajshahi Centre Ends -->

<!-- Comilla Centre Starts -->
<section id="comilla" class="text-center each_centre each_centre_mas">
    <div class="bg-danger info-header mb-5">
        <h1 class="text-light py-3">
            Cumilla Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04633.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. ABUL BASHAR</h5>
                        <p> F/04633</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-12744.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MIRZA MOHAMMAD HAFIZ</h5>
                        <p>F/12744</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-02789.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MILAN CHANDRA MAJUMDER</h5>
                        <p>F/02789</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-10327.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. RAHMAT ULLAH KABIR</h5>
                        <p>F/10327</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Comilla Centre Ends -->

<!-- Sylhet Centre Starts -->
<section id="sylhet" class="text-center each_centre each_centre_mas">
    <div class="bg-info info-header mb-5">
        <h1 class="text-light py-3">
            Sylhet Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04551.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. SHOAIB AHMED MATIN</h5>
                        <p> F/04551</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-11383.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. HARONUR RASHID MULLAH</h5>
                        <p>F/11383</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-10034.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. A. S. M.  MOHSIN</h5>
                        <p>F/10034</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-05333.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. SHAHADAT HOSSAIN (SHIBLU), PEng.</h5>
                        <p>F/05333</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Sylhet Centre Ends -->


<!-- Barisal Centre Starts -->
<section id="barisal" class="text-center each_centre each_centre_mas">
    <div class="bg-danger info-header mb-5">
        <h1 class="text-light py-3">
            Barisal Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">
        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/m.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5></h5>
                        <p> F/0000</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/m.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5></h5>
                        <p>F/0000</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/m.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5></h5>
                        <p>F/0000</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/m.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>Md. Nafees Imtiaz</h5>
                        <p>F/0000</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Barisal Centre Ends -->

<!-- Mymensingh Centre Starts -->
<section id="mymensingh" class="text-center each_centre each_centre_mas">
    <div class="bg-info info-header mb-5">
        <h1 class="text-light py-3">
            Mymensingh Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-02797.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. ABDUL MAJID</h5>
                        <p> F/02797</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-09139.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. SHIBENDRA NARAYAN GOPE</h5>
                        <p>F/09139</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-03656.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. ABDUL JABBER</h5>
                        <p>F/03656</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-09572.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. A. B. M. FARUK HOSSAIN.</h5>
                        <p>F/09572</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Mymensingh Centre Ends -->

<!-- Rangpur Centre Starts -->
<section id="rangpur" class="text-center each_centre each_centre_mas">
    <div class="bg-danger info-header mb-5">
        <h1 class="text-light py-3">
            Rangpur Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04738.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. FAKIR ABDUR ROB</h5>
                        <p> F/04738</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-08448.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. SHAHADAT HOSSAIN SARKER</h5>
                        <p>F/08448</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-01917.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. REZAUL KARIM</h5>
                        <p>F/01917</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-10704.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. MAHBUBUL ALAM KHAN</h5>
                        <p>F/10704</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Rangpur Centre Ends -->

<!-- Ghorasal Centre Starts -->
<section id="ghorasal" class="text-center each_centre each_centre_mas">
    <div class="bg-info info-header mb-5">
        <h1 class="text-light py-3">
            Ghorasal Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-05370.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. MOAZZEM HOSSAIN</h5>
                        <p> F/05370</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-10105.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MAHBUBUR RAHMAN</h5>
                        <p>F/10105</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04847.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. TARUN KANTI SARKAR</h5>
                        <p>F/04847</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-07044.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. NURUL ISLAM</h5>
                        <p>F/07044</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Ghorasal Centre Ends -->

<!-- Bogra Centre Starts -->
<section id="bogra" class="text-center each_centre each_centre_mas">
    <div class="bg-danger info-header mb-5">
        <h1 class="text-light py-3">
            Bogra Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">
        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-02240.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. A. F. M. ABDUL MATIN</h5>
                        <p> F/02240</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04776.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. SYED IFTEKHAR HOSSAIN</h5>
                        <p>F/04776</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-06974.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. MOKSEDUR RAHMAN</h5>
                        <p>F/06974</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/M-23654.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. MAHBUBUL ALAM CHOWDHURY</h5>
                        <p>M/23654</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Bogra Centre Ends -->

<!-- Gazipur Centre Starts -->
<section id="gazipur" class="text-center each_centre each_centre_mas">
    <div class="bg-info info-header mb-5">
        <h1 class="text-light py-3">
            Gazipur Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-05458.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>PROF. DR. ENGR. MOHAMMED ALAUDDIN</h5>
                        <p> F/05458</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-12073.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>DR. ENGR. MD. ISRAIL HOSSAIN</h5>
                        <p>F/12073</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-06430.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. A. K. M. NURUL ISLAM</h5>
                        <p>F/06430</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-07165.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>PROF. DR. ENGR. MD. KAMRUZZAMAN</h5>
                        <p>F/07165</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Gazipur Centre Ends -->

<!-- Narayangonj Centre Starts -->
<section id="narayangonj" class="text-center each_centre each_centre_mas">
    <div class="bg-danger info-header mb-5">
        <h1 class="text-light py-3">
            Narayangonj Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-09448.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. QUAZI MD. ZIAUL HOQUE</h5>
                        <p> F/09448</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-05640.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MOHAMMAD NASIR UDDIN</h5>
                        <p>F/05640</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/m.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. A. K. M. MANZUR KADIR</h5>
                        <p>F/06317</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-09662.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. HARUN-AR-RASHID</h5>
                        <p>F/09662</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Narayangonj Centre Ends -->

<!-- Rangadia Centre Starts -->
<section id="rangadia" class="text-center each_centre each_centre_mas">
    <div class="bg-info info-header mb-5">
        <h1 class="text-light py-3">
            Rangadia Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04289.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. AZIZUR RAHMAN CHOWDHURY</h5>
                        <p> F/04289</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-09119.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. ANUPAM CHOWDHURY</h5>
                        <p>F/09119</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-05011.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. RAFIQUL ISLAM</h5>
                        <p>F/05011</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04127.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. ESKANDER SABER AHAMMAD</h5>
                        <p>F/04127</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Rangadia Centre Ends -->

<!-- Jessore Centre Starts -->
<section id="jessore" class="text-center each_centre each_centre_mas">
    <div class="bg-danger info-header mb-5">
        <h1 class="text-light py-3">
            Jessore Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-06730.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. ABU HASAN</h5>
                        <p> F/06730</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-07340.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. SAIFUZZAMAN</h5>
                        <p>F/07340</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-06490.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. SHAHIDUL ALAM</h5>
                        <p>F/06490</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-13073.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. BENZUR RAHMAN</h5>
                        <p>F/13073</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Jessore Centre Ends -->

<!-- Ashugonj Centre Starts -->
<section id="ashugonj" class="text-center each_centre each_centre_mas">
    <div class="bg-info info-header mb-5">
        <h1 class="text-light py-3">
            Ashugonj Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-05943.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. A. M. M. SAZZADUR RAHMAN</h5>
                        <p> F/05943</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04083.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. RAFIQUL ISLAM</h5>
                        <p>F/04083</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/m.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. A. K. M. YAQUB</h5>
                        <p>F/05941</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-11158.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. KAMRUZZMAN BHUYAN</h5>
                        <p>F/11158</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Ashugonj Centre Ends -->

<!-- Faridpur Centre Starts -->
<section id="faridpur" class="text-center each_centre each_centre_mas">
    <div class="bg-danger info-header mb-5">
        <h1 class="text-light py-3">
            Faridpur Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-04220.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>DR. ENGR. MD. LUTFOR RAHMAN, PEng.</h5>
                        <p> F/04220</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/m.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5></h5>
                        <p>F/0000</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-07392.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. NUR HOSSAIN BHUIYAN</h5>
                        <p>F/07392</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-10149.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. AMINUR RAHMAN KHAN</h5>
                        <p>F/10149</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Faridpur Centre Ends -->

<!-- Dinajpur Centre Starts -->
<section id="dinajpur" class="text-center each_centre each_centre_mas">
    <div class="bg-info info-header mb-5">
        <h1 class="text-light py-3">
            Dinajpur Centre
        </h1>
        <p class="lead pb-3 text-light">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet omnis fuga maiores excepturi dolores explicabo.
        </p>
    </div>
    <div class="container pb-2">

        <div class="row my-4">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-01780.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MUHAMMAD MUSHARRAF HUSAIN</h5>
                        <p> F/01780</p>
                        <p class="font-weight-bold">CHAIRMAN</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-09081.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MOHAMMAD ABDUL HALIM</h5>
                        <p>F/09081</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ACADEMIC & HRD)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-09180.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. ABU JAFAR SIDDIK</h5>
                        <p>F/09180</p>
                        <p class="font-weight-bold">VICE - CHAIRMAN</p>
                        <p><small>(ADMIN. PROFESSIONAL & SW)</small></p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body card-body_mas">
                        <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/F-10051.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                        <h5>ENGR. MD. KHALILUR RAHMAN</h5>
                        <p>F/10051</p>
                        <p class="font-weight-bold">HONORARY SECRETARY</p>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="p-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="p-4">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Dinajpur Centre Ends -->


<%@ include file="../footer.jsp" %>
