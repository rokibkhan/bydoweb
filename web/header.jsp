<%@page import="java.util.Iterator"%>
<%@page import="com.appul.entity.Member"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="com.appul.util.GlobalVariable"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%

    Session dbsessionHead = HibernateUtil.getSessionFactory().openSession();

    org.hibernate.Transaction dbtrxHead = null;

    dbtrxHead = dbsessionHead.beginTransaction();

    String sessionIdHead = "";

    String userNameHead = "";

    String memberIdHead = "";

    Query queryHead = null;

    Object[] objectHead = null;

    String memberNameHead = "";

    String memberNameLastHead = "";

    String pictureNameHead = "";

    String pictureLinkHead = "";

    Member memberHead = null;

    if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdHead = session.getId();

        userNameHead = session.getAttribute("username").toString();

        memberIdHead = session.getAttribute("memberId").toString();

        queryHead = dbsessionHead.createQuery("from Member as member WHERE id=" + memberIdHead + " ");

        for (Iterator itrHead = queryHead.list().iterator(); itrHead.hasNext();) {

            memberHead = (Member) itrHead.next();

            memberNameHead = memberHead.getMemberName();

            //  System.out.println("memberNameHead :: " + memberNameHead);
            String[] arrOfStr = memberNameHead.split(" ");

            for (String a : arrOfStr) {

                //  System.out.println(a);
                memberNameLastHead = a;

            }

            pictureNameHead = memberHead.getPictureName();

            pictureLinkHead = GlobalVariable.baseUrl + "/upload/member/" + pictureNameHead;

        }

    }

    dbsessionHead.flush();

    dbsessionHead.close();





%>

<!DOCTYPE html>
<html>
    <head>
        <title>ByDo Academy</title>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
         <!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" ></script>-->
                 <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

       <!-- <link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" rel="stylesheet" />



       
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>


        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
        <link rel="stylesheet" type="text/css" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/style.css">
        
        <!--
        <link rel="stylesheet" type="text/css" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css">
        <link rel="stylesheet" type="text/css" href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/bootstrap/dist/css/bootstrap.min.css">
        -->
    </head>
    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg static-top">
            <div class="container">
                <a class="navbar-brand" href="<%out.print(GlobalVariable.baseUrl);%>">
                    <img class="img-fluid" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/logo2.png"  alt="logo" width="200" />
                </a>
                <button  class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                    <span class="navbar-toggler-icon"><i class="fa fa-bars"></i></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">

                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item">
                            <a class="nav-link text-secondary" href="<%out.print(GlobalVariable.baseUrl);%>">Home</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link text-secondary" href="<%out.print(GlobalVariable.baseUrl);%>/course/course.jsp">Course</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link text-secondary" href="<%out.print(GlobalVariable.baseUrl);%>/pages/about.jsp">About US</a>
                        </li>
                       <%if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {%>
                        </ul>   
                       <ul class="nav navbar-top-links navbar-right pull-right">

                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
                                <!--<img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/plugins/images/users/no_image.jpg" alt="user-img" width="36" class="img-circle">-->
                                <span>My Profile</span> 
                            </a>

                            <ul class="dropdown-menu dropdown-user animated flipInY">
                                <li style="margin-left: 5%;"><a href="<%out.print(GlobalVariable.baseUrl);%>/member/memberEditProfile.jsp?sessionid=<% out.print(sessionIdHead);%>"><i class="fa fa-user-o"></i> Edit Profile</a></li>
                                
                                
                                <li style="margin-left: 5%;" role="separator" class="divider"></li>
                                <li style="margin-left: 5%;"><a href="<%out.print(GlobalVariable.baseUrl);%>/member/logout.jsp?sessionid=<% out.print(sessionIdHead);%>"><i class="fa fa-power-off"></i> Logout</a></li>                                  
                            </ul>
                            <!-- /.dropdown-user -->
                        </li>
                        <!--<li class="right-side-toggle"> <a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>-->
                        <!-- /.dropdown -->
                    </ul>
                       <%}
                       else
                        {%>

                        
                         <li class="nav-item">
                            <a class="nav-link text-secondary" href="<%out.print(GlobalVariable.baseUrl);%>/member/login.jsp">Login <i class="fa fa-sign-in text-dark" style="font-size:20px;"></i></a> 
                        </li>

                        <%}%>
                       
                    </ul>
                </div>
            </div>
        </nav>
