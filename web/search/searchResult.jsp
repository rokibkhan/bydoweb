<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.appul.util.HibernateUtil"%>
<%@page import="org.hibernate.Query"%>

<%@ include file="../header.jsp" %>

<!-- News & Events & Single Event CSS -->
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/news_events_style.css" rel="stylesheet">
<link href="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/css/sazzad.css" rel="stylesheet" type="text/css"/>
<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    String searchQuery1 = "";
    String filterSQLStr = "";
    Object searchObj[] = null;

    int ix = 1;

    // String targetpage = "memberListFellow.jsp";
    String targetpageWithSession = "searchResult.jsp?sessionid=" + session.getId() + "&";
    String filterp = "";
    int limit = 10;
    int total_pages = 0;
    int stages = 3;
    int start = 0;
    String p = "";
    String p1 = "";
    String p2 = "";

    int prev = 0;
    int next = 0;
    double lastpage1 = 0;
    int lastpage = 0;
    int LastPagem1 = 0;

    String mainSearchString = request.getParameter("mainSearchString") == null ? "" : request.getParameter("mainSearchString").trim();

    if (!mainSearchString.equals("")) {
        filterSQLStr = " WHERE ev.event_title like '%" + mainSearchString + "%' OR ev.event_short_desc like '%" + mainSearchString + "%' OR ev.event_desc like '%" + mainSearchString + "%' OR ev.event_venue like '%" + mainSearchString + "%'";
        filterp = "&mainSearchString=" + mainSearchString + "&";
        System.out.println("mainSearchString filterSQLStr :: " + filterSQLStr);
        System.out.println("mainSearchString filterp :: " + filterp);
    } else {

        filterSQLStr = " ";
        filterp = "&mainSearchString=" + mainSearchString + "&";
        System.out.println("mainSearchString filterSQLStr :: " + filterSQLStr);
        System.out.println("mainSearchString filterp :: " + filterp);
    }

    System.out.println("filterSQLStr :: " + filterSQLStr);
    String pageNbr1 = request.getParameter("page") == null ? "1" : request.getParameter("page").trim();
    int pageNbr = Integer.parseInt(pageNbr1);

    System.out.println("pageNbr :: " + pageNbr);

    // DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateFormatEventM = new SimpleDateFormat("MMMM"); //October
    DateFormat dateFormatEventMM = new SimpleDateFormat("MMM"); //Oct
    DateFormat dateFormatEventD = new SimpleDateFormat("dd");
    DateFormat dateFormatEventY = new SimpleDateFormat("Y");
    Date dateEventM = null;
    Date dateEventMM = null;
    Date dateEventD = null;
    Date dateEventY = null;
    String newDateEventM = "";
    String newDateEventMM = "";
    String newDateEventD = "";
    String newDateEventY = "";

    Date todayDate = new Date();
    String today = dateFormatEvent.format(todayDate); //2016-11-16

    Query eventSQL = null;
    Object eventObj[] = null;
    String eventId = "";
    String eventTitle = "";
    String eventDuration = "";
    String eventVenue = "";
    String eventDurationVanue = "";
    String eventShortDetails = "";
    String eventShortDetails1 = "";
    String eventShortDetailsX = "";
    String eventDetails = "";
    String eventDetails1 = "";
    String eventDetailsX = "";
    String eventDateTime = "";
    String eventMonth = "";
    String eventDay = "";
    String eventYear = "";
    String eventFeatureImage = "";
    String eventFeatureImage1 = "";
    String eventFeatureImageUrl = "";
    String topEventContainer = "";
    String topConClass = "";
    String topConClass1 = "";
    String topConClass2 = "";
    String topConClass3 = "";
    String bottomEventContainer = "";
    String topMarginClass = "";

    String eventInfoFirstContainer = "";
    String eventInfoContainer = "";
    String eventDetailsUrl = "";
    int ev = 1;

    String eventCategory = "";
    String keyNotesSpeakerConterner = "";

    String upcomingEventBtn = "";

   

%>

<!-- Start of navigation -->
<div class="about_banner_activites">
    <h2 class="display_41_saz text-white"> Search Result</h2>
    <div class="jb_breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background: none;">
                <li class="breadcrumb-item"><a href="<%out.print(GlobalVariable.baseUrl);%>">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Search</a></li>
                <li class="breadcrumb-item active text-white">Search Result</li>
            </ol>
        </nav>
    </div>
</div>
<!-- End of navigation -->

<%
//    String searchCountSQL = "SELECT count(*) FROM  member m,member_type mt "
//            + "WHERE m.status = 1 "
//            + "AND m.id = mt.member_id "
//            + " " + filterSQLStr + " "
//            + "ORDER BY m.id DESC";
    String searchCountSQL = "SELECT count(*) FROM  event_info ev "
            + " " + filterSQLStr + " "
            + "ORDER BY ev.id_event DESC";

    System.out.println("searchResult searchCountSQL :: " + searchCountSQL);

    String total_pages1 = dbsession.createSQLQuery(searchCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(searchCountSQL).uniqueResult().toString();
    total_pages = Integer.parseInt(total_pages1);

    System.out.println("searchResult total_pages :: " + total_pages);

    if (pageNbr != 0) {
        start = (pageNbr - 1) * limit;
    } else {
        start = 0;
    }

    System.out.println("startLLLL :: " + start);
    System.out.println("limitLLLL :: " + limit);

    // Initial page num setup
    if (pageNbr == 0) {
        pageNbr = 1;
    }
    prev = pageNbr - 1;
    next = pageNbr + 1;
    lastpage1 = Math.ceil((double) total_pages / limit);
    //   DecimalFormat df = new DecimalFormat("#.##");
    //  System.out.print(df.format(lastpage1));

    // lastpage = (int) Math.ceil(total_pages / limit);
    lastpage = (int) lastpage1;
    LastPagem1 = (int) Math.round(lastpage - 1);

    System.out.println("total_pages :: " + total_pages);
    System.out.println("limit :: " + limit);
    System.out.println("lastpage1 :: " + lastpage1);
    System.out.println("lastpage :: " + lastpage);
    System.out.println("LastPagem1 :: " + LastPagem1);

    String paginate = "";
    if (lastpage > 1) {
        paginate = paginate + "<ul class='pagination' style=\"margin:0\">";

        // Previous
        if (pageNbr > 1) {
            p = "page=" + prev;
            // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">prev</a></li>";
            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">prev</a></li>";
        } else {
            //	$paginate.= "<span class='disabled'>previous</span>";
            paginate = paginate + "<li class=\"disabled\"><a>prev</a></li>";
        }

        int counter = 1;
        // Pages
        if (lastpage < 7 + (stages * 2)) { // Not enough pages to breaking it up

            for (counter = 1; counter <= lastpage; counter++) {
                if (counter == pageNbr) {
                    paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                } else {
                    p = "page=" + counter;
                    // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                    paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                }
            }
        } else if (lastpage > 5 + (stages * 2)) { // Enough pages to hide a few?
            // Beginning only hide later pages
            if (pageNbr < 1 + (stages * 2)) {
                for (counter = 1; counter < 4 + (stages * 2); counter++) {
                    if (counter == pageNbr) {
                        paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                    } else {
                        p = "page=" + counter;
                        //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                        paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                    }
                }
                paginate = paginate + "<li><a>...</a></li>";
                p1 = "page=" + LastPagem1;
                p2 = "page=" + lastpage;
                //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
            } // Middle hide some front and some back
            else if (lastpage - (stages * 2) > pageNbr && pageNbr > (stages * 2)) {
                p1 = "page=1";
                p2 = "page=2";
                //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                paginate = paginate + "<li><a>...</a></li>";
                for (counter = pageNbr - stages; counter <= pageNbr + stages; counter++) {
                    if (counter == pageNbr) {
                        paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                    } else {
                        p = "page=" + counter;
                        // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                        paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                    }
                }
                paginate = paginate + "<li><a>...</a></li>";
                p1 = "page=" + LastPagem1;
                p2 = "page=" + lastpage;
                //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                //   paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">" + lastpage + "</a></li>";
                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">" + LastPagem1 + "</a></li>";
                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">" + lastpage + "</a></li>";
            } // End only hide early pages
            else {
                p1 = "page=1";
                p2 = "page=2";
                //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p1 + "\">1</a></li>";
                //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p2 + "\">2</a></li>";
                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p1 + "\">1</a></li>";
                paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p2 + "\">2</a></li>";
                paginate = paginate + "<li><a>...</a></li>";
                //  for ( counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {

                for (counter = (int) Math.round(lastpage - (2 + (stages * 2))); counter <= lastpage; counter++) {
                    if (counter == pageNbr) {
                        paginate = paginate + "<li class=\"active\"><a>" + counter + "</a></li>";
                    } else {
                        p = "page=" + counter;
                        //  paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\">" + counter + "</a></li>";
                        paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\">" + counter + "</a></li>";
                    }
                }
            }
        }

        // Next
        if (pageNbr < counter - 1) {
            p = "page=" + next;
            // paginate = paginate + "<li><a href=\" " + targetpage + "?" + filterp + p + "\"'>next</a></li>";
            paginate = paginate + "<li><a href=\" " + targetpageWithSession + filterp + p + "\"'>next</a></li>";
        } else {
            //	$paginate.= "<span class='disabled'>next</span>";
            paginate = paginate + "<li class=\"disabled\"><a>next</a></li>";
        }

        paginate = paginate + "</ul>";
    }

    int minPaginationIndexLimit = ((pageNbr - 1) * limit);
    int maxPaginationIndexLimit = (pageNbr * limit);
    int lastProductIndex = total_pages;
    int minPaginationIndexLimitPlus = minPaginationIndexLimit + 1;
//                        if(minPaginationIndexLimitPlus < limit){
//                            minPaginationIndexLimitPlus = 1;
//                        }

    if (maxPaginationIndexLimit >= lastProductIndex) {
        maxPaginationIndexLimit = lastProductIndex;
    }

    String searchSQL = "SELECT ev.id_event, ev.event_title, ev.event_short_desc, ev.event_desc, "
            + "ev.event_date, ev.event_duration ,ev.event_venue, "
            + "ev.feature_image, ev.image1,ev.image2,ev.image3,ev.image4,ev.event_category "
            + "FROM  event_info ev "
            + " " + filterSQLStr + " "
            + "ORDER BY ev.id_event DESC  LIMIT " + start + ", " + limit + "";

    System.out.println("searchResult searchCountSQL :: " + searchCountSQL);
    System.out.println("searchResult searchSQL :: " + searchSQL);


%>

<!-- Start of more events section -->
<section class="m-5">
    <div class="row">

        <div class="col-md-6 offset-md-1" style="padding: 20px 0;">
                <%=paginate%>
        </div>

        <div class="col-md-4" style="padding-top: 23px;">
            <p class="text-right">
                Showing <%=minPaginationIndexLimitPlus + " to " + maxPaginationIndexLimit%> of  <%=total_pages%>
            </p>

        </div>
    </div>
    <div class="row">
        <div class="container">


            <%

                Query searchSQLQry = dbsession.createSQLQuery(searchSQL);

                if (!searchSQLQry.list().isEmpty()) {
                    for (Iterator searchItr = searchSQLQry.list().iterator(); searchItr.hasNext();) {

                        searchObj = (Object[]) searchItr.next();

                        eventId = searchObj[0].toString().trim();
                        eventTitle = searchObj[1].toString().trim();
                        eventShortDetails = searchObj[2].toString().trim();

                        //  eventShortDetailsX = eventShortDetails.substring(0, 250);
                        eventShortDetailsX = eventShortDetails;

                        eventDetails = searchObj[3].toString().trim();

                        //   eventDetails1 = eventDetails.replaceAll("\\<.*?>", "");
                        //   eventDetailsX = eventDetails1.substring(0, 250);
                        eventDetailsX = eventDetails;

                        eventDetailsUrl = GlobalVariable.baseUrl + "/events/eventsDetails.jsp?eventIdX=" + eventId;

                        //      String updateProfileUrl = GlobalVariable.baseUrl + "/eventManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&eventIdX=" + eventId + "&selectedTab=profile";
                        //      String updateChangePassUrl = GlobalVariable.baseUrl + "/eventManagement/syUserListUpdateUserInfo.jsp?sessionid=" + session.getId() + "&eventIdX=" + eventId + "&selectedTab=changePass";
                        if (ev > 4) {
                            topMarginClass = "my-5";
                        } else {
                            topMarginClass = "";
                        }

            %>

            <div class="row<%=topMarginClass%> border-bottom border-secondary">
                <div class="col-md-12">
                    <div class="event_content" style="padding-top:15px;">
                        <div class="details_content">
                            <h3 class="news_title"><a href="<%=eventDetailsUrl%>"><%=eventTitle%></a></h3>
                            <p class="news_para"><%=eventDetailsX%></p>                 
                            <a href="<%=eventDetailsUrl%>" class="btn btn-outline-success border-dark details-link my-3">View Details</a>
                        </div>
                    </div>
                </div>
            </div>

            <%

                        ev++;
                        minPaginationIndexLimitPlus++;
                        ix++;
                    }
                }
            %>

        </div>
    </div>
    <div class="row">

        <div class="col-md-6 offset-md-1" style="padding: 20px 0;">
            <%=paginate%>
        </div>

        <div class="col-md-4" style="padding-top: 23px;">
            <p class="text-right">
                Showing <%=minPaginationIndexLimitPlus + " to " + maxPaginationIndexLimit%> of  <%=total_pages%>
            </p>

        </div>
    </div>
</section>
<!-- End of more events section -->


<%@ include file="../footer.jsp" %>
