<%@page import="com.appul.util.GlobalVariable"%>
<%
    // System.out.println("LLL:" + request.getRequestURL());
    String getURIStr = request.getRequestURI();
    // System.out.println("getURL:::" + getURIStr);

    String[] arrOfStr = getURIStr.split("/");

    String currentPageOpt = "";
    for (String curl : arrOfStr) {
        //  System.out.println(curl);

        currentPageOpt = curl;
    }

    // out.println("currentPageOpt:: " + currentPageOpt);
    String footerClass = "";

//    if ((currentPageOpt.equalsIgnoreCase("index.jsp")) || (currentPageOpt.equalsIgnoreCase(""))) {
//        footerClass = "footer";
//    } else {
//        footerClass = "footer1";
//    }
%>


<!-- Footer -->
<footer class="page-footer font-small pt-4" style="background: url(<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/footer.png);background-size: 100% 100%;">

    <!-- Footer Links -->
    <div class="container text-center text-md-left">

        <!-- Grid row -->
        <div class="row">

            <!-- Grid column -->
            <div class="col-md-3 col-sm-12  my-md-4 my-0 mt-4 mb-1">

                <!-- Content -->
                <a href="<%out.print(GlobalVariable.baseUrl);%>">
                    <img src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/footer-logo.png" width="100%">
                </a>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none">

            <!-- Grid column -->
            <div class="col-md-3 col-sm-12  my-md-4 my-0 mt-4 mb-1 text-white">

                <!-- Links -->
                <h5 class="font-weight-bold text-uppercase mb-4">join us</h5>

                <ul class="list-unstyled">
                    <a href="#">
                        <li class="text-white">Become Teacher</li>
                    </a>
                    <a href="#">
                        <li class="text-white">Become Student</li>
                    </a>
                    <a href="#">
                        <li class="text-white">Become Both</li>
                    </a>
                    <a href="#">
                        <li class="text-white">Become Partnership</li>
                    </a>
                </ul>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none">

            <!-- Grid column -->
            <div class="col-md-3 col-sm-12  my-md-4 my-0 mt-4 mb-1">

                <!-- Contact details -->
                <h5 class="font-weight-bold text-uppercase mb-4 text-white">Support</h5>

                <ul class="list-unstyled">
                    <a href="<%out.print(GlobalVariable.baseUrl);%>/pages/privacypolicy.jsp">
                        <li class="text-white">Terms & Condition</li>
                    </a>
                    <a href="<%out.print(GlobalVariable.baseUrl);%>/pages/privacypolicy.jsp">
                        <li class="text-white">Privacy & Policy</li>
                    </a>
                    <a href="#">
                        <li class="text-white">Copyright Issue</li>
                    </a>
                    <a href="#">
                        <li class="text-white">Get Help</li>
                    </a>
                </ul>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none">

            <!-- Grid column -->
            <div class="col-md-3 col-sm-12 text-center my-4">

                <!-- Social buttons -->
                <h5 class="font-weight-bold text-uppercase mb-4 text-white">Follow Us</h5>

                <!-- Facebook -->
                <a href="https://www.facebook.com/engr.md.abu.hasan.masud/?__tn__=%2Cd%2CP-R&eid=ARD7geCzPGm7tLJvKu6UhLewpovIqJheXp2vBJcGITAfSJujDI8Pv3uHTtoBEskD624ZLBnZMRERNJBY">
                    <img class="rounded-circle" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/facebook.png" width="17%">
                </a>
                <!-- Facebook -->
                <a href="https://www.youtube.com/channel/UC1TT9VBDda8gjMBcGYeBaNA">
                    <img class="rounded-circle" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/youtube.png" width="17%">
                </a>
                <!-- Facebook -->
                <a href="#">
                    <img class="rounded-circle" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/images/linkedin.png" width="17%">
                </a>


            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="text-center py-3 text-muted" style="background-color: #3E4685">&COPY; 2020 Copyright:
        <a class="text-white" href="index.html">BYDO ACADEMY</a>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->







<script type="text/javascript" src="<%out.print(GlobalVariable.baseUrl);%>/commonUtil/assets/js/bydo.js"></script>




<!-- sample modal content -->
<div id="rupantorLGModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="rupantorLGModalTitle" style="text-align: left !important;">Large modal</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            </div>
            <div class="modal-body" id="rupantorLGModalBody">
                <h4>Overflowing text to show scroll behavior</h4>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
            </div>
            <div class="modal-footer" id="rupantorLGModalFooter">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- sample modal content -->
<div id="taskLGModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="taskLGModalTitle" style="text-align: left !important;"></h4> 
            </div>
            <div class="modal-body" id="taskLGModalBody">
            </div>
            <div class="modal-footer" id="taskLGModalFooter">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->    


<!-- sample modal content -->
<div id="memberLGModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="margin-top: 100px;">
        <div class="modal-content">           

            <div class="modal-header">
                <h5 class="modal-title" id="memberLGModalTitle" style="text-align: left !important;"></h5> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            </div>

            <div class="modal-body" id="memberLGModalBody">
            </div>
            <div class="modal-footer" id="memberLGModalFooter">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->    


<!--Modal: Name-->
<div id="videoPlayerModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Body-->
            <div class="modal-body mb-0 p-0" id="videoPlayerModalBody">

            </div>
            <!--Footer-->
            <div class="modal-footer justify-content-center modal-social-icon-partho" id="videoPlayerModalFooter">
                <span class="mr-4">Share</span>
                <a type="button" class="btn-floating btn-sm btn-fb"><i class="fab fa-facebook-f"></i></a>
                <!--Twitter-->
                <a type="button" class="btn-floating btn-sm btn-tw"><i class="fab fa-twitter"></i></a>
                <!--Google +-->
                <a type="button" class="btn-floating btn-sm btn-gplus"><i class="fab fa-google-plus-g"></i></a>
                <!--Linkedin-->
                <a type="button" class="btn-floating btn-sm btn-ins"><i class="fab fa-linkedin-in"></i></a>
                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: Name-->

</body>

</html>