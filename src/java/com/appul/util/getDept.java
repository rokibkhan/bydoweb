/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import com.appul.util.HibernateUtil;
import com.appul.entity.SyDept;
import java.util.*;
import org.hibernate.*;

/**
 *
 * @author Akter
 */
public class getDept {

    Session dbsession = null;
    org.hibernate.Transaction dbtrx = null;

    public String getDeptName(String deptID) {
        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();
        Query q1 = dbsession.createQuery("from SyDept where deptId='" + deptID + "'");
        Iterator itr = q1.list().iterator();


        String deptName = "";

        if (itr.hasNext()) {
            SyDept codept = (SyDept) itr.next();
            deptName = codept.getDeptName();
        } else {
            deptName = "";
        }
        dbtrx.commit();
        dbsession.close();
        return deptName;
    }

    public String getDeptID(String deptName) {
        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();
        Query q1 = dbsession.createQuery("from SyDept where deptName='" + deptName + "'");
        Iterator itr = q1.list().iterator();


        String deptID = "";

        if (itr.hasNext()) {
            SyDept codept = (SyDept) itr.next();
            deptID = codept.getDeptId();
        } else {
            deptID = "";
        }
        dbtrx.commit();
        dbsession.close();
        return deptID;
    }
}
