package com.appul.util;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class dateNext{
	
	public static void main(String args[]){
		
		int days=0;
		String retdate="";
		
		if (args.length> 0){
			days=Integer.parseInt(args[0].trim());
			
			try{
				
				System.out.println((getNextDate(days)));	
			} catch(Exception e){
				e.printStackTrace();
			}
		} else {
			System.out.println("Parameter for days is required");
		}
		
	}
	
	
	public static String getNextDate(int afterdays) throws Exception{
		String todate=getDate();
		String nextdate=retDate(todate,afterdays);		
		return nextdate;
	}
	
	
	public	static String getDate() {
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		    Date date = new Date();
		    return dateFormat.format(date);
	}

	public static String retDate(String strDate, Integer daytoAdd){
	
		//int daytoAdd=0;
		String retdate=null;
		//daytoAdd=Integer.parseInt(strDayToAdd);
			
		int date = Integer.parseInt(strDate);
		Calendar cal = dateToCalendar(date);
		cal.add(Calendar.DATE, daytoAdd);
		date = calendarToDate(cal);
		//System.out.println(date);
		retdate=Integer.toString(date);
		return retdate;
	}

	public static Calendar dateToCalendar(int date) {
		int day = date % 100;
		//System.out.println(day);
		int month = (date/100) % 100 - 1;
		//System.out.println(month);
		int year = date / 10000;
		//System.out.println(year);
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, day);
		//System.out.println(cal);
		return cal;
	}

	public static int calendarToDate(Calendar cal) {
		int day = cal.get(Calendar.DATE);
		//System.out.println("day "+day);
		int month = cal.get(Calendar.MONTH) + 1;
		//System.out.println("month "+month);
		int year = cal.get(Calendar.YEAR);
		//System.out.println("year "+year);
		//System.out.println(year * 10000 + month * 100 + day);
		return year * 10000 + month * 100 + day;
	}

	
public	static Date getFormatedDate(String dateIn) throws Exception{

		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
	    Date dateOut = dateFormat.parse(dateIn);	   	    
	    return dateOut;
	}

}