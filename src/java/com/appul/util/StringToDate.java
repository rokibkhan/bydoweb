/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Akter
 */
public class StringToDate {

    public static Date getDateTime(String strDate) {
        Date date = null;
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            date = dateFormat.parse(strDate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getDate(String strDate) {
        Date date = null;
        String newDate = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat dateFormat1 = new SimpleDateFormat("dd-MMM-yy");
            date = dateFormat.parse(strDate);
            newDate = dateFormat1.format(date);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return newDate;
    }

}
