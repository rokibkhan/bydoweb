/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author HP
 */
public class TimeZoneConversion {

    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
//    private static final String DATE_FORMAT = "dd-M-yyyy hh:mm:ss a";

    public static void main(String[] args) {

        String dateInString = "2016-10-21 05:31:00";
//        String dateInString = "29-10-2016 12:34:55 PM";
        String timeZone = "America/Chicago";
//        String timeZone = "America/Montreal";
//        String timeZone = "GMT-6";

//        System.out.println(getTimeZoneConvert(timeZone, dateInString));

    }

    public static String getTimeZoneConvert(String timeZone, String dateInString) {
        String dateTime1 = "";
//        String dateInString = "29-10-2016 12:34:55 PM";
        LocalDateTime ldt = LocalDateTime.parse(dateInString, DateTimeFormatter.ofPattern(DATE_FORMAT));

        ZoneId singaporeZoneId = ZoneId.of("GMT0");
//        System.out.println("TimeZone : " + singaporeZoneId);

        //LocalDateTime + ZoneId = ZonedDateTime
        ZonedDateTime asiaZonedDateTime = ldt.atZone(singaporeZoneId);
//        System.out.println("Date (GMT0) : " + asiaZonedDateTime);

//        ZoneId newYokZoneId = ZoneId.of("America/New_York");
        ZoneId newYokZoneId = ZoneId.of(timeZone);

//        System.out.println("TimeZone : " + newYokZoneId);

        ZonedDateTime nyDateTime = asiaZonedDateTime.withZoneSameInstant(newYokZoneId);
        
        
//        System.out.println("Date (New York) : " + nyDateTime);

        DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_FORMAT);
//        System.out.println("\n---DateTimeFormatter---");
//        System.out.println("Date (Singapore) : " + format.format(asiaZonedDateTime));
//        System.out.println("Date (New York) : " + format.format(nyDateTime));
        return format.format(nyDateTime);
    }
}
