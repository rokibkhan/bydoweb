/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import com.appul.entity.SyUser;
import java.sql.SQLException;
import org.hibernate.*;

/**
 *
 * @author Akter
 */
public class UserHelper {

    Session dbsession = null;

    public String getUserPass(String UserID) throws SQLException {
        SyUser user = null;
        String password = null;

        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction tx = null;
        try {
            tx = dbsession.beginTransaction();

            Query q = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id=(SELECT id FROM member where member_id= '" + UserID + "') ");

            password = q.uniqueResult().toString();

        } catch (Exception e) {
            System.out.println("Could not resolve User ID: " + UserID);
            password = "123";

        }
        dbsession.clear();
        dbsession.close();
        return password;
    }

    public String getMemberId(String UserID) throws SQLException {
        SyUser user = null;
        String memberId = null;

        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction tx = null;
        try {
            tx = dbsession.beginTransaction();

            Query q = dbsession.createSQLQuery("SELECT id FROM member where member_id= '" + UserID + "'");

            memberId = q.uniqueResult().toString();
            tx.commit();

        } catch (Exception e) {
            System.out.println("Could not resolve User ID: " + UserID);
            memberId = "0";

        }
        dbsession.flush();
        dbsession.close();
        return memberId;
    }

    public int setUserPass(String UserID, String NewPass) {

        int rec = -1;
        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction tx = null;
        try {
            tx = dbsession.beginTransaction();
            SyUser syuser = (SyUser) dbsession.get(SyUser.class, UserID);
            syuser.setUserPassword(NewPass);
            dbsession.flush();
            tx.commit();

        } catch (HibernateException e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            dbsession.close();
        }
        return rec;
        //return "test";
    }

}
