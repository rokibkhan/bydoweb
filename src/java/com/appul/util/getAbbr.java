/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import com.appul.util.HibernateUtil;
import com.appul.entity.SyAbbr;
import java.util.*;
import org.hibernate.*;

/**
 *
 * @author Mijanur Rahman
 */
public class getAbbr {

    Session dbsession = null;
    org.hibernate.Transaction dbtrx = null;

    public String getLong(String shortCode) {
          dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();
        Query q1 = dbsession.createQuery("from SyAbbr where shortcode='" + shortCode + "'");
        Iterator itr = q1.list().iterator();


        String longCode = "";

        if (itr.hasNext()) {
            SyAbbr abbr = (SyAbbr) itr.next();
            longCode = abbr.getLongcode();
        } else {
            longCode = "";
        }
        dbtrx.commit();
        dbsession.close();
        return longCode;
    }
    public String getShort(String longCode) {
         dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();
        Query q1 = dbsession.createQuery("from SyAbbr where longcode='" + longCode + "'");
        Iterator itr = q1.list().iterator();


        String shortCode = "";

        if (itr.hasNext()) {
            SyAbbr abbr = (SyAbbr) itr.next();
            shortCode = abbr.getShortcode();
        } else {
            shortCode = "";
        }
        dbtrx.commit();
        dbsession.close();
        return shortCode;
    }
     
   
}
