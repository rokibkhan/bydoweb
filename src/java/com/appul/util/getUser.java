/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import com.appul.util.HibernateUtil;
import com.appul.entity.SyUser;
import java.util.*;
import org.hibernate.*;

/**
 *
 * @author Akter
 */
public class getUser {

    Session dbsession = null;
    org.hibernate.Transaction dbtrx = null;

    
    public String checkUserNameAlreadyExits(String userID) {
        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();
        Query q1 = dbsession.createQuery("from SyUser where userId='" + userID + "'");
        Iterator itr = q1.list().iterator();

        

        String userNameExits = null;

        if (itr.hasNext()) {
            SyUser user = (SyUser) itr.next();
            userNameExits = "1";
        } else {
            userNameExits = "0";
        }
        dbtrx.commit();
        dbsession.close();
        

        return userNameExits;
    }
    
    public String getUserName(String userID) {
        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();
        Query q1 = dbsession.createQuery("from SyUser where userId='" + userID + "'");
        Iterator itr = q1.list().iterator();

        

        String userName = "";

        if (itr.hasNext()) {
            SyUser user = (SyUser) itr.next();
            userName = user.getUserName();
        } else {
            userName = "";
        }
        dbtrx.commit();
        dbsession.close();

        return userName;
    }

    public String getUserID(String userName) {
        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();
        Query q1 = dbsession.createQuery("from SyUser where userName='" + userName + "'");
        Iterator itr = q1.list().iterator();


        String userID = "";

        if (itr.hasNext()) {
            SyUser user = (SyUser) itr.next();
            userID = user.getUserId();
        } else {
            userID = "";
        }
        dbtrx.commit();
        dbsession.close();
        return userID;
    }

    public String getUserPass(String userID) {
        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();
        Query q1 = dbsession.createQuery("from SyUser where userId='" + userID + "'");
        Iterator itr = q1.list().iterator();


        String userPass = "";

        if (itr.hasNext()) {
            SyUser user = (SyUser) itr.next();
            userPass = user.getUserPassword();
        } else {
            userPass = "";
        }
        dbtrx.commit();
        dbsession.close();
        return userPass;
    }
}
