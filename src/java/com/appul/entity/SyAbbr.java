package com.appul.entity;
// Generated Nov 13, 2020 1:16:58 PM by Hibernate Tools 4.3.1



/**
 * SyAbbr generated by hbm2java
 */
public class SyAbbr  implements java.io.Serializable {


     private String shortcode;
     private String longcode;

    public SyAbbr() {
    }

	
    public SyAbbr(String shortcode) {
        this.shortcode = shortcode;
    }
    public SyAbbr(String shortcode, String longcode) {
       this.shortcode = shortcode;
       this.longcode = longcode;
    }
   
    public String getShortcode() {
        return this.shortcode;
    }
    
    public void setShortcode(String shortcode) {
        this.shortcode = shortcode;
    }
    public String getLongcode() {
        return this.longcode;
    }
    
    public void setLongcode(String longcode) {
        this.longcode = longcode;
    }




}


