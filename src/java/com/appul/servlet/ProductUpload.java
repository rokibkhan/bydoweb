/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.getRegistryID;
import com.appul.util.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

@WebServlet(name = "ProductUpload", urlPatterns = {"/ProductUpload"})
@MultipartConfig
public class ProductUpload extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String userId = request.getSession(true).getAttribute("username").toString();

            Part styleIdP = null;
            Part productNameP = null;
            Part productShortNameP = null;
            Part productShortDescP = null;
            Part productDescP = null;
            Part colorIdP = null;
            Part colorImageP = null;
            Part productImageP = null;

            String man = request.getPart("man") == null ? "0" : "1";
            String woman = request.getPart("woman") == null ? "0" : "2";
            String kid = request.getPart("kid") == null ? "0" : "3";
            String polo = request.getPart("polo") == null ? "0" : "4";
            String crew = request.getPart("crew") == null ? "0" : "5";
            String jacket = request.getPart("jacket") == null ? "0" : "6";
            String pant = request.getPart("pant") == null ? "0" : "7";
            String shorts = request.getPart("shorts") == null ? "0" : "8";
            String shortSleeve = request.getPart("shortSleeve") == null ? "0" : "9";
            String longSleeve = request.getPart("longSleeve") == null ? "0" : "10";
            String vneck = request.getPart("vneck") == null ? "0" : "11";
            String longApron = request.getPart("longApron") == null ? "0" : "12";
            String shortApron = request.getPart("shortApron") == null ? "0" : "13";
            String vestApron = request.getPart("vestApron") == null ? "0" : "14";
            String waistApron = request.getPart("waistApron") == null ? "0" : "15";

            String[] productAttribute = {man, woman, kid, polo, crew, jacket, pant, shorts, shortSleeve, longSleeve, vneck, longApron, shortApron, vestApron, waistApron};

            Scanner styleIdS = null;
            Scanner colorIdS = null;
            Scanner productNameS = null;
            Scanner productShortNameS = null;
            Scanner productShortDescS = null;
            Scanner productDescS = null;
            Scanner productIdS = null;

            int productId = 0;
            String productId1 = "";

            String styleId = "";
            String productName = "";
            String colorId = "";
            String productShortName = "";
            String productShortDesc = "";
            String productDesc = "";

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            Session dbsession = null;

            String entryDate = dateFormat.format(date);
 

            dbsession = HibernateUtil.getSessionFactory().openSession();
            SessionImpl sessionImpl = (SessionImpl) dbsession;
            Connection con = sessionImpl.connection();

            InetAddress ip;
            String hostname = "";
            String ipAddress = "";
            try {
                ip = InetAddress.getLocalHost();
                hostname = ip.getHostName();
                ipAddress = ip.getHostAddress();

            } catch (UnknownHostException e) {

                e.printStackTrace();
            }
            PreparedStatement ps = null;
            Part productIdP = request.getPart("categoryId");
            productIdS = new Scanner(productIdP.getInputStream());
            String parrentId = productIdS.nextLine();

            styleIdP = request.getPart("productStyleCode");
            productNameP = request.getPart("productName");
            productShortNameP = request.getPart("productShortName");
            productDescP = request.getPart("editor");
            productShortDescP = request.getPart("productShortDesc");
            colorIdP = request.getPart("colorId");
            colorImageP = request.getPart("colorImage");
            productImageP = request.getPart("productImage");

            styleIdS = new Scanner(styleIdP.getInputStream());
            productNameS = new Scanner(productNameP.getInputStream());
            productShortNameS = new Scanner(productShortNameP.getInputStream());
            productDescS = new Scanner(productDescP.getInputStream());
            productShortDescS = new Scanner(productShortDescP.getInputStream());
            colorIdS = new Scanner(colorIdP.getInputStream());

            styleId = styleIdS.nextLine();
            productName = productNameS.nextLine();
            productShortName = productShortNameS.nextLine();
            productDesc = productDescS.nextLine();
            productShortDesc = productShortDescS.nextLine();
            colorId = colorIdS.nextLine();
            getRegistryID getId = new getRegistryID();
            productId1 = getId.getID(18);
            productId = Integer.parseInt(productId1);

            ps = con.prepareStatement("insert into product values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            ps.setInt(1, productId);
            ps.setString(2, styleId);
            ps.setString(3, productName);
            ps.setString(4, productShortName);
            ps.setString(5, productShortDesc);
            ps.setString(6, productDesc);
            ps.setBinaryStream(7, productImageP.getInputStream(), (int) productImageP.getSize());
            ps.setString(8, colorId);
            // size must be converted to int otherwise it results in error
            ps.setBinaryStream(9, colorImageP.getInputStream(), (int) colorImageP.getSize());
            ps.setString(10, parrentId);
            ps.setString(11, entryDate);
            ps.setString(12, userId);
            ps.setString(13, hostname);
            ps.setString(14, ipAddress);
            ps.setString(15, "");
            ps.setString(16, "");
            ps.setString(17, "");
            ps.setString(18, null);
            ps.executeUpdate();
            con.commit();
//            System.out.println("PS#####" + ps);
            ps.close();
            con.close();
            dbsession.close();
            getRegistryID regId1 = new getRegistryID();
            String productAttributeId = "";

            dbsession = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrx = null;
            dbtrx = dbsession.beginTransaction();

            for (int i = 0; i < productAttribute.length; i++) {
                if (!productAttribute[i].equalsIgnoreCase("0")) {
                    productAttributeId = regId1.getID(25);
                    Query q1 = dbsession.createSQLQuery("insert into product_attribute(id,product_id,attribute_id,published) values( '" + productAttributeId + "','" + productId + "','" + productAttribute[i] + "','1' )");
                    q1.executeUpdate();
                }
            }
            dbsession.flush();
            dbtrx.commit();
            dbsession.close();

            out.println("Successfully Added Product  Image.");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            out.close();
        }
    }
}
