/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.Query;

import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

@WebServlet(name = "MemberCertificateUpload", urlPatterns = {"/MemberCertificateUpload"})
@MultipartConfig
public class MemberCertificateUpload extends HttpServlet {

    private boolean isMultipart;
    private String filePath;
//    private int maxFileSize = 5000 * 1024;
//    private int maxMemSize = 4000 * 1024;
     private int maxFileSize = 5 * 1024 * 1024;  //5242880; // 5MB -> 5 * 1024 * 1024
    private int maxMemSize = 4 * 1024 * 1024;
    private File file;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(39);
        int certificateId = Integer.parseInt(idS);
        int memberId = 0;

        String sessionIdH = "";
        String userNameH = "";
        String strMsg = "";

        HttpSession session = request.getSession(false);
        userNameH = session.getAttribute("username").toString().toUpperCase();
        sessionIdH = session.getId();

        filePath = GlobalVariable.imagePhotoUploadPath;

        // Check that we have a file upload request
        isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");
        //  java.io.PrintWriter out = response.getWriter();

        if (!isMultipart) {

            strMsg = "ERROR!!! Form enctype type is not multipart/form-data";
            response.sendRedirect(GlobalVariable.baseUrl + "/member/memberCertificateInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

        }

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);

        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax(maxFileSize);

        try {
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);

            // Process the uploaded file items
            Iterator item = fileItems.iterator();

            Random random = new Random();
            int rand1 = Math.abs(random.nextInt());
            int rand2 = Math.abs(random.nextInt());
            
            DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
            Date dateFileToday = new Date();
            String filePrefixToday = formatterFileToday.format(dateFileToday);

            // long uniqueNumber = System.currentTimeMillis();
            String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".pdf";

            String name = "";
            String value = "";

            String fieldName = "";
            String fileName = "";
            String contentType = "";
            String certificateShortName = "";
            String certificateLongName = "";

            while (item.hasNext()) {
                FileItem fi = (FileItem) item.next();
                if (!fi.isFormField()) {

                    // Get the uploaded file parameters
                    fieldName = fi.getFieldName();
                    fileName = fi.getName();
                    contentType = fi.getContentType();
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();

                    // Write the file
                    if (fileName.lastIndexOf("\\") >= 0) {
                        file = new File(filePath + fileName1);
                    } else {
                        file = new File(filePath + fileName1);
                    }
                    fi.write(file);
                } else {

                    name = fi.getFieldName();
                    value = fi.getString();
                }

                if (name.equalsIgnoreCase("certificateShortName")) {
                    certificateShortName = value;
                }
                if (name.equalsIgnoreCase("certificateLongName")) {
                    certificateLongName = value;
                }
                if (name.equalsIgnoreCase("memberId")) {
                    memberId = Integer.parseInt(value);
                }

                 
            }

            DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateX = new Date();
            String adddate = dateFormatX.format(dateX);
            String adduser = userNameH;

            Session dbsession = HibernateUtil.getSessionFactory().openSession();
            SessionImpl sessionImpl = (SessionImpl) dbsession;
            Connection con = sessionImpl.connection();

            PreparedStatement ps = con.prepareStatement("insert into member_certificate values(?,?,?,?,?)");

            ps.setInt(1, certificateId);
            ps.setInt(2, memberId);
            ps.setString(3, certificateShortName);
            ps.setString(4, certificateLongName);
            ps.setString(5, fileName1);
            int i = ps.executeUpdate();
            con.commit();
            con.close();
            dbsession.close();

            if (i == 1) {

                strMsg = "Photo Added Successfully";
                System.out.println(strMsg);
                response.sendRedirect(GlobalVariable.baseUrl + "/member/memberCertificateInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

            } else {

                strMsg = "Error!!! When photo add";
                System.out.println(strMsg);
                response.sendRedirect(GlobalVariable.baseUrl + "/member/memberCertificateInfo.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

            }

        } catch (Exception ex) {
            System.out.println(ex);
            System.out.println(strMsg);
        }

    }
}
