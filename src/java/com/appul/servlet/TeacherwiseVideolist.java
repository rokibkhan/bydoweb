/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.appul.entity.*;
import com.appul.util.HibernateUtil;
import com.google.gson.Gson;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author HP
 */
@WebServlet(name = "TeacherwiseVideolist", urlPatterns = {"/TeacherwiseVideolist"})
public class TeacherwiseVideolist extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TeacherwiseVideolist</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet TeacherwiseVideolist at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        System.out.println("Teacher Wise Video List");
        
        String teacherid = request.getParameter("teacherid");
        System.out.println("teacherid:"+teacherid);
       
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        //org.hibernate.Transaction dbtrx = null;
        //dbtrx = dbsession.beginTransaction();
        
        Query SQL = dbsession.createSQLQuery("SELECT id_video,chapter_id,teacher_id,video_category,video_title,video_caption,video_emded_link"
                                            + " FROM  video_gallery_info where teacher_id = '"+ teacherid+"'");

        
        
        Gson gson = new Gson();
        List<VideoGalleryInfo> videolist = new ArrayList<VideoGalleryInfo>();
        JSONArray jvideolist = new JSONArray();
       // List<VideoComment> prodlist = SQL.list();

        Object[] obj2 = null;
        Object[] obj3 = null;
        Object[] obj4 = null;
        Object[] obj5 = null;
        
        VideoGalleryInfo vgi;            
        ChapterInfo ci; 
        int i = 0;
        

        for (Iterator it2 = SQL.list().iterator(); it2.hasNext();) {
            
            obj2 = (Object[]) it2.next();
            JSONObject videoObj = new JSONObject();
            
            videoObj.put("idVideo",obj2[0].toString()); 
            
            vgi = new VideoGalleryInfo();            
            ci = new ChapterInfo();  
            SessionInfo si;  
            
            vgi.setIdVideo(Integer.parseInt(obj2[0].toString()));
            
            Query cSQL = dbsession.createSQLQuery("SELECT id_chapter,session_id,chapter_name"
                                            + " FROM  chapter_info where id_chapter = '"+ Integer.parseInt(obj2[1].toString()) +"'");

            //var c =     from prod in 
            
            
            for (Iterator it3 = cSQL.list().iterator(); it3.hasNext();) {
                si = new SessionInfo();  


                obj3 = (Object[]) it3.next();
                
                videoObj.put("idChapter",obj3[0].toString()); 
                ci.setIdChapter(Integer.parseInt(obj3[0].toString()));
                
                
                Query sSQL = dbsession.createSQLQuery("SELECT id_session,subject_id,session_name"
                                            + " FROM  session_info where id_session = '"+ Integer.parseInt(obj3[1].toString()) +"'");
                
                
                for (Iterator it4 = sSQL.list().iterator(); it4.hasNext();) {
                    obj4 = (Object[]) it4.next();
                    //videoObj.put("idSession",obj4[0].toString()); 
                    si.setIdSession(Integer.parseInt(obj4[0].toString()));
                    
                    videoObj.put("idSession",obj4[0].toString()); 
                    
                    videoObj.put("idSubject",obj4[1].toString()); 
                    
                    si.setSubjectInfo(new SubjectInfo(Integer.parseInt(obj4[1].toString()), "", ""));
                    
                    videoObj.put("sessionName",obj4[2].toString()); 
                    si.setSessionName(obj4[2].toString());
                    
                Query coSQL = dbsession.createSQLQuery("SELECT id_course,course_name from course_info where id_course = (SELECT course_id"
                                            + " FROM  subject_info where id_subject = '"+ Integer.parseInt(obj4[1].toString()) +"')");
                
                
                for (Iterator it5 = coSQL.list().iterator(); it4.hasNext();) {
                    obj5 = (Object[]) it5.next();
                    videoObj.put("idCourse",obj5[0].toString()); 
                    videoObj.put("courseName",obj5[1].toString()); 
                }
                    
                
                
                }
                
                //ci.setSessionInfo(new SessionInfo(Integer.parseInt(obj3[1].toString()), "", ""));
                ci.setSessionInfo(si);
                
                videoObj.put("chapterName",obj3[2].toString()); 
                ci.setChapterName(obj3[2].toString());
                
            }
            
            vgi.setChapterInfo(ci);
            
            videoObj.put("videoTitle",obj2[4].toString()); 
            vgi.setVideoTitle(obj2[4].toString());
            
            videoObj.put("videoCaption",obj2[5].toString()); 
            vgi.setVideoCaption(obj2[5].toString());
            
            videoObj.put("videoEmdedLink",obj2[6].toString()); 
            vgi.setVideoEmdedLink(obj2[6].toString());
            
            //mb.setMemberUniversities(new MemberUniversity(1, mb, new University()));
            //vc.setSolveStatus((obj2[2].toString()));
            //vc.setCommmentDesc(obj2[4].toString());
            //vc.setVideoId(Integer.parseInt(obj2[5].toString()));
            //vc.setCommmentFrom(Integer.parseInt(obj2[6].toString()));            
            //vc.setCommmentDate(Date.parse(obj2[5].toString()));
            videolist.add(vgi);
            jvideolist.add(i,videoObj);
            i++;
            //videocommentlist.add(new VideoComment(Integer.parseInt(obj2[0].toString()),Integer.parseInt(obj2[1].toString()),
             //      1,11,(byte)1));
            
        }

        //products.add(new VideoComment(1,0,1,11,(byte)1));
        //products.add(new VideoComment(2,0,1,12,(byte)1));
        //products.add(new VideoComment(3,0,1,13,(byte)1));
        PrintWriter out = response.getWriter();
        //out.print(gson.toJson(videolist));
        out.print(jvideolist);
        out.flush();
        out.close();

        dbsession.flush();

        dbsession.close();

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
