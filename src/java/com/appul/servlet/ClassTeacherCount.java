/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.json.simple.JSONObject;

/**
 *
 * @author HP
 */
@WebServlet(name = "ClassTeacherCount", urlPatterns = {"/ClassTeacherCount"})
public class ClassTeacherCount extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ClassTeacherCount</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ClassTeacherCount at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        
        System.out.println("Class Teacher Count");
        
        //String teacherid = request.getParameter("teacherid");
        //System.out.println("teacherid:"+teacherid);
       
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        //org.hibernate.Transaction dbtrx = null;
        //dbtrx = dbsession.beginTransaction();
        
        Query vSQL = dbsession.createSQLQuery("SELECT count(id_video)"
                                            + " FROM  video_gallery_info  ");
        String VideoCount = "";
        
        if (!vSQL.list().isEmpty()) {
            VideoCount = vSQL.uniqueResult().toString();
            System.out.println("VideoCount:"+VideoCount);
        } else {
           // System.out.println("Do not Get Member ID of  " + username);
        }
        
        
        Query tSQL = dbsession.createSQLQuery("SELECT count(m.id)"
                                            + " FROM  member m join member_type mt on m.id = mt.member_id "
                                            +" where mt.member_type_id = 1");
        String TeacherCount = "";
        
        if (!tSQL.list().isEmpty()) {
            TeacherCount = tSQL.uniqueResult().toString();
            System.out.println("TeacherCount:"+TeacherCount);
        } else {
           // System.out.println("Do not Get Member ID of  " + username);
        }
        
        JSONObject Obj = new JSONObject();
        
        Obj.put("videocount",VideoCount);
        Obj.put("teachercount",TeacherCount);
        
        
        
        PrintWriter out = response.getWriter();
        //out.print(gson.toJson(colist));
        out.print(Obj);
        out.flush();
        out.close();
        
        
        dbsession.flush();

        dbsession.close();

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
