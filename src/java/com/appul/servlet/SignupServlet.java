/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.entity.Member;
import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.RandomString;
import com.appul.util.getRegistryID;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Zeeon
 */
@WebServlet(name = "SignupServlet", urlPatterns = {"/SignupServlet"})
public class SignupServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        try {
            String fullName = request.getParameter("memberFullName").trim();
            String mobileNumber = request.getParameter("memberPhone").trim();
            String gender = request.getParameter("gender").trim();
            String dob = request.getParameter("dob").trim();
            String memberTypeId = request.getParameter("usertype").trim();

            String firstLetter = mobileNumber.substring(0, 1);
            String adjustLettter1 = "+88";
            String adjustLettter2 = "88";
            String adjustLettter3 = "+";

            String mobileNumber1 = "";
            String mobileNumber2 = "";
            String strMsg = "";

            if (firstLetter.equals("0")) {
                mobileNumber1 = adjustLettter1 + mobileNumber;
                mobileNumber2 = adjustLettter2 + mobileNumber;

                System.out.println("firstLetter :: 0 :: mobileNumber1 ::" + mobileNumber1);
                System.out.println("firstLetter :: 0 :: mobileNumber2 ::" + mobileNumber2);
            }
            else if (firstLetter.equals("8")) {
                mobileNumber1 = adjustLettter3 + mobileNumber;
                mobileNumber2 = adjustLettter3 + mobileNumber;
                System.out.println("firstLetter :: 8 :: mobileNumber1 ::" + mobileNumber1);
                System.out.println("firstLetter :: 8 :: mobileNumber2 ::" + mobileNumber2);
            }
            else if (firstLetter.equals("+")) {
                mobileNumber1 = mobileNumber;
                mobileNumber2 = mobileNumber;

                System.out.println("firstLetter :: + :: mobileNumber1 ::" + mobileNumber1);
                System.out.println("firstLetter :: + :: mobileNumber2 ::" + mobileNumber2);

            } else {
                strMsg = "Invalid Mobile Number";
                response.sendRedirect(GlobalVariable.baseUrl + "/member/registration.jsp?strMsg=" + strMsg);
            }

            getRegistryID getId = new getRegistryID();

            RandomString randNumber = new RandomString();

            SendSMS sendsms = new SendSMS();
            SendEmail sendemail = new SendEmail();

            Random random = new Random();
            int rand1 = Math.abs(random.nextInt());
            int rand2 = Math.abs(random.nextInt());

            long uniqueNumber = System.currentTimeMillis();

            String memberUniqueNumber = uniqueNumber + "-" + rand1 + "-" + rand2;

            DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
            Date dateToday = new Date();

            Member member = null;
            int memId = 0;
            String dbPass = "";
            String adddate = "";

            Encryption encryption = new Encryption();

            

            Query qMember = null;
            Query memberKeySQL = null;

            String ratingDate = "";
            Query postContentShareRatingAddSQL = null;

            Query postContentRatingHistoryAddSQL = null;

            Query memberAddSQL = null;

            String ed = dateFormatYmd.format(dateToday);
            String td = "2099-12-31";

            String memberCountryCode = "BD";
            String tempPassStatus = "2"; //2->for switch force password change ;1->already change password
            //String memberTypeId = "1"; //1->member;2->doctor;
            String memberCustomOrder = "999999";

            qMember = dbsession.createQuery(" from Member where mobile = '" + mobileNumber1 + "'");
            if (qMember.list().isEmpty()) {
                String idS = getId.getID(43);
                int regMemberId = Integer.parseInt(idS);

                String regMemberTopicsId = getId.getID(75);

                //String pinCode = randNumber.randomString(4,2);
                String member_temp_pass_phone = randNumber.randomString(4, 2);
                String member_temp_pass_email = randNumber.randomString(6, 2);

                String tempPassEnc = encryption.getEncrypt(member_temp_pass_phone);
                InetAddress ip;
                String hostname = "";
                String ipAddress = "";
                try {
                    ip = InetAddress.getLocalHost();
                    hostname = ip.getHostName();
                    ipAddress = ip.getHostAddress();

                } catch (UnknownHostException e) {

                    e.printStackTrace();
                }
                adddate = dateFormatX.format(dateToday);
                String adduser = Integer.toString(memId);
                String status = "1";

                ratingDate = dateFormatX.format(dateToday);

                //insert member
                //insert member_credential
                //insert member_type
                //insert member_custom_order
                //insert member_view_setting
                //insert member_deviceid
                //insert member_designation
                //insert member_additional_info
                //insert member_organization
                //insert member_rating_point
                //insert member_social_link
                //insert member_profile_bg
                //insert member_career_objective
                //insert member_admin_setting
                //insert member_topics
                //insert address_book
                //insert member_address
                memberAddSQL = dbsession.createSQLQuery("INSERT INTO member("
                        + "id,"
                        + "member_id,"
                        + "member_name,"
                        + "home_district,"
                        + "home_thana,"
                        + "mobile,"
                        + "dob,"
                        + "gender,"
                        + "country_code,"
                        + "status,"
                        + "add_user,"
                        + "add_date,"
                        + "add_term,"
                        + "add_ip,"
                        + "email_id,"
                        + "mod_date"
                        + ") VALUES("
                        + "'" + regMemberId + "',"
                        + "'" + regMemberId + "',"
                        + "'" + fullName + "',"
                        + "'" + 0 + "',"
                        + "'" + 0 + "',"
                        + "'" + mobileNumber1 + "',"
                        + "'" + dob + "',"
                        + "'" + gender + "',"
                        + "'" + memberCountryCode + "',"
                        + "'" + status + "',"
                        + "'" + adduser + "',"
                        + "'" + adddate + "',"
                        + "'" + hostname + "',"
                        + "'" + ipAddress + "',"
                        + "'" + "" + "',"
                        + "'" + adddate + "'"
                        + "  ) ");
                System.out.println("memberAddSQL ::" + memberAddSQL);

                memberAddSQL.executeUpdate();

                Query mCredentialAddSQL = dbsession.createSQLQuery("INSERT INTO member_credential("
                        + "id,"
                        + "member_id,"
                        + "member_key,"
                        + "status,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'" + tempPassEnc + "',"
                        + "'" + tempPassStatus + "',"
                        + "'" + adddate + "'"
                        + "  ) ");

                System.out.println("mCredentialAddSQL ::" + mCredentialAddSQL);

                mCredentialAddSQL.executeUpdate();

                Query mMemberTypeAddSQL = dbsession.createSQLQuery("INSERT INTO member_type("
                        + "id,"
                        + "member_id,"
                        + "member_type_id,"
                        + "ed,"
                        + "td"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'" + memberTypeId + "',"
                        + "'" + ed + "',"
                        + "'" + td + "'"
                        + "  ) ");

                System.out.println("mCredentialAddSQL ::" + mMemberTypeAddSQL);

                mMemberTypeAddSQL.executeUpdate();

                Query mMemberCustomOrderAddSQL = dbsession.createSQLQuery("INSERT INTO member_custom_order("
                        + "id,"
                        + "member_id,"
                        + "custom_order,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'" + memberCustomOrder + "',"
                        + "'" + adddate + "'"
                        + "  ) ");

                System.out.println("mMemberCustomOrderAddSQL ::" + mMemberCustomOrderAddSQL);

                mMemberCustomOrderAddSQL.executeUpdate();
                Query mMemberViewSettingAddSQL = dbsession.createSQLQuery("INSERT INTO member_view_setting("
                        + "id,"
                        + "member_id,"
                        + "mobile_view,"
                        + "phone1_view,"
                        + "phone2_view,"
                        + "email_view,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'1',"
                        + "'1',"
                        + "'1',"
                        + "'1',"
                        + "'" + adddate + "'"
                        + "  ) ");

                System.out.println("mMemberViewSettingAddSQL ::" + mMemberViewSettingAddSQL);

                mMemberViewSettingAddSQL.executeUpdate();

                Query mMemberDeviceIdAddSQL = dbsession.createSQLQuery("INSERT INTO member_deviceid("
                        + "id,"
                        + "member_id,"
                        + "add_date,"
                        + "add_term,"
                        + "add_ip"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'" + adddate + "',"
                        + "'" + hostname + "',"
                        + "'" + ipAddress + "'"
                        + "  ) ");

                System.out.println("mMemberDeviceIdAddSQL ::" + mMemberDeviceIdAddSQL);

                mMemberDeviceIdAddSQL.executeUpdate();

                Query mMemberAdditionalInfoAddSQL = dbsession.createSQLQuery("INSERT INTO member_additional_info("
                        + "id,"
                        + "member_id,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'" + adddate + "'"
                        + "  ) ");

                System.out.println("mMemberAdditionalInfoAddSQL ::" + mMemberAdditionalInfoAddSQL);

                mMemberAdditionalInfoAddSQL.executeUpdate();

                Query mMemberOrganizationAddSQL = dbsession.createSQLQuery("INSERT INTO member_organization("
                        + "id,"
                        + "member_id,"
                        + "organization_id,"
                        + "status,"
                        + "add_date,"
                        + "add_term,"
                        + "add_ip"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'1',"
                        + "'1',"
                        + "'" + adddate + "',"
                        + "'" + hostname + "',"
                        + "'" + ipAddress + "'"
                        + "  ) ");

                System.out.println("mMemberOrganizationAddSQL ::" + mMemberOrganizationAddSQL);

                mMemberOrganizationAddSQL.executeUpdate();

                Query mMemberRatingPointAddSQL = dbsession.createSQLQuery("INSERT INTO member_rating_point("
                        + "id,"
                        + "member_id,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'" + adddate + "'"
                        + "  ) ");

                System.out.println("mMemberRatingPointAddSQL ::" + mMemberRatingPointAddSQL);

                mMemberRatingPointAddSQL.executeUpdate();

                Query mMemberSocialLinkAddSQL = dbsession.createSQLQuery("INSERT INTO member_social_link("
                        + "id,"
                        + "member_id,"
                        + "link_type_id"
                        //+ "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'1'"
                        //+ "'" + adddate + "'"
                        + "  ) ");

                System.out.println("mMemberSocialLinkAddSQL ::" + mMemberSocialLinkAddSQL);

                mMemberSocialLinkAddSQL.executeUpdate();

                Query mMemberProfileBgAddSQL = dbsession.createSQLQuery("INSERT INTO member_profile_bg("
                        + "id,"
                        + "member_id,"
                        + "bg_file_name,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'no_image.jpg',"
                        + "'" + adddate + "'"
                        + "  ) ");

                System.out.println("mMemberProfileBgAddSQL ::" + mMemberProfileBgAddSQL);

                mMemberProfileBgAddSQL.executeUpdate();

                Query mMemberCareerObjectiveAddSQL = dbsession.createSQLQuery("INSERT INTO member_career_objective("
                        + "id,"
                        + "member_id,"
                        + "objectives,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'',"
                        + "'" + adddate + "'"
                        + "  ) ");

                System.out.println("mMemberCareerObjectiveAddSQL ::" + mMemberCareerObjectiveAddSQL);

                mMemberCareerObjectiveAddSQL.executeUpdate();

                Query mMemberAdminViewSettingAddSQL = dbsession.createSQLQuery("INSERT INTO member_admin_setting("
                        + "id,"
                        + "member_id,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'" + adddate + "'"
                        + "  ) ");

                System.out.println("mMemberAdminViewSettingAddSQL ::" + mMemberAdminViewSettingAddSQL);

                mMemberAdminViewSettingAddSQL.executeUpdate();

                Query mMemberTopicsAddSQL = dbsession.createSQLQuery("INSERT INTO member_topics("
                        + "id,"
                        + "member_id,"
                        + "topics_id,"
                        + "add_user,"
                        + "add_date"
                        + ") VALUES("
                        + "" + regMemberTopicsId + ","
                        + "" + regMemberId + ","
                        + "'1',"
                        + "" + regMemberId + ","
                        + "'" + adddate + "'"
                        + "  ) ");

                System.out.println("mMemberTopicsAddSQL ::" + mMemberTopicsAddSQL);

                mMemberTopicsAddSQL.executeUpdate();

                //insert address_book
                //insert member_address
                dbtrx.commit();

                if (dbtrx.wasCommitted()) {

                    //  String smsMsg = "Your verification code is" + member_temp_pass_phone + " Email code is " + member_temp_pass_email;
                    // String smsMsg = "Your JoyBangla temporary password is " + member_temp_pass_phone + ".Please change password after successfully login.";
                    String smsMsg = "# " + member_temp_pass_phone + " is your BYDO App OTP code.Never share this code with anyone. 4/w+ZFwk7Zq";//E9KLFIsWT/b";

                    //  String receipentMobileNo = "+8801755656354";
                    String receipentMobileNo = mobileNumber;
                    //    System.out.println(SendSMS(sms, receipentMobileNo));

                    //  String sendsms1 = sendsms.sendSMS(smsMsg, receipentMobileNo);
                    String sendsms1 = sendsms.sendSMS(smsMsg, receipentMobileNo);

                    strMsg = "Success!!! Personal Information Updated";
                    response.sendRedirect(GlobalVariable.baseUrl + "/member/otpVerification.jsp?username=" + mobileNumber1);

                } else {
                    strMsg = "Error!!! and try again.";
                    dbtrx.rollback();
                    response.sendRedirect(GlobalVariable.baseUrl + "/member/registration.jsp?strMsg=" + strMsg);
                }
            } else {
                strMsg = "Member Already Exists!";
                response.sendRedirect(GlobalVariable.baseUrl + "/member/registration.jsp?strMsg=" + strMsg);

            }

        } catch (Exception theException) {
            System.out.println(theException);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
