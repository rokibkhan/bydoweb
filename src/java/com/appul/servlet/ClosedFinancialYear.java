/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * Author : Akter   
 */
package com.appul.servlet;

import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;
import java.net.InetAddress;
import java.net.UnknownHostException;

import java.util.Iterator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.apache.log4j.Logger;

public class ClosedFinancialYear {

    public static Logger logger = Logger.getLogger("ClosedFinancialYear.class");

    public static String genTempBill(String billRef, String userId, String userStoreId) {

        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        String result = "";

        String fromDate = null;
        String toDate = null;

        String sqlstr = "select FINANCIAL_YEAR,ED,TD from FINANCIAL_YEAR "
                + "where STATUS in('G') and STORE_ID='" + userStoreId + "' ";
        Query qBillRef = dbsession.createSQLQuery(sqlstr);

        if (qBillRef.list().size() > 0) {
            logger.info("Another Financial Year Closing is Ongoing");
            return "AAnother Financial Year Closing is Ongoing";
        }


        /*check any other Posting ongoing*/
        sqlstr = "select FINANCIAL_YEAR,ED,TD from FINANCIAL_YEAR "
                + "where STATUS='P' and STORE_ID='" + userStoreId + "'";
        qBillRef = dbsession.createSQLQuery(sqlstr);

        if (qBillRef.list().size() > 0) {
            logger.info("Another Financial Year Closing is Ongoing");
            return "Another Financial Year Closing is Ongoing";

        }

        /* this code have to move to front end later */
        sqlstr = "select FINANCIAL_YEAR,ED,TD from FINANCIAL_YEAR "
                + "where STATUS in('O','H') and financial_year='" + billRef + "' and STORE_ID='" + userStoreId + "'";
        qBillRef = dbsession.createSQLQuery(sqlstr);

        if (qBillRef.list().size() <= 0) {
            logger.info("No Financial Year Closing is due");
            return "No Financial Year Closing is due";
        }

        try {
            sqlstr = "update FINANCIAL_YEAR set STATUS='G' where STATUS not in('G','P','C') and financial_year='" + billRef + "' and STORE_ID='" + userStoreId + "'";
            System.out.println(sqlstr);
            Query qUpdateBillRef = dbsession.createSQLQuery(sqlstr);
            int upcount = qUpdateBillRef.executeUpdate();

            System.out.println("upcount=======" + upcount);

            if (upcount == 1) {

                sqlstr = "update FINANCIAL_YEAR set STATUS='H' where STATUS='G' and financial_year='" + billRef + "' and STORE_ID='" + userStoreId + "'";
                qUpdateBillRef = dbsession.createSQLQuery(sqlstr);
                upcount = qUpdateBillRef.executeUpdate();

                String voucherType = "JV";
                String description = "Closing Balance for the Year" + billRef;
                String voucherDate = "";
                String plevelId = "";
                String amount = "";
                String vChildId = "";
                String ed = "";
                String td = "";

                Query q1 = null;
                Query q2 = null;

                Object[] object1 = null;
                Object[] object2 = null;

                getRegistryID getId = new getRegistryID();
                String vId = getId.getID(5);

                InetAddress ip;
                String hostname = "";
                String ipAddress = "";
                try {
                    ip = InetAddress.getLocalHost();
                    hostname = ip.getHostName();
                    ipAddress = ip.getHostAddress();

                } catch (UnknownHostException e) {
                }

                q1 = dbsession.createSQLQuery("select gl.retained_earning,f.ed,f.td, DATE_ADD(f.td, INTERVAL 3 HOUR)post_date, sum(amount) amount\n"
                        + " from voucher_child v,gl_code g,gl_type gt,financial_year f,g_general_gl gl\n"
                        + " where gt.gl_type_id  in (3,4)\n"
                        + " and g.gl_type_id=gt.gl_type_id\n"
                        + " and v.plevel_id=g.gl_code\n"
                        + " and v.del_or_up=0\n"
                        + " and g.store_id='" + userStoreId + "'\n"
                        + " and v.voucher_date>=f.ed and  v.voucher_date<=f.td\n"
                        + " and f.financial_year='" + billRef + "'\n"
                        + " and gl.store_id='" + userStoreId + "'\n"
                        + " and now() between gl.ed and gl.td\n"
                        + " and f.store_id='" + userStoreId + "'\n"
                        + " group by gl.retained_earning,f.ed,f.td, DATE_ADD(f.td, INTERVAL 3 HOUR)");
                if (!q1.list().isEmpty()) {
                    for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {

                        object2 = (Object[]) itr1.next();
                        plevelId = object2[0].toString();
                        ed = object2[1].toString();
                        td = object2[2].toString();
                        voucherDate = object2[3].toString();
                        amount = object2[4].toString();

                        vChildId = getId.getID(6);

                        Query qVoucherChild6 = dbsession.createSQLQuery("insert into VOUCHER_CHILD(ID,VOUCHER_ID,VOUCHER_TYPE,VOUCHER_DATE,PLEVEL_ID,ACCOUNT_NO,PAYEE,AMOUNT,ADD_DATETIME,ADD_USER,ADD_IP,DEL_OR_UP,STORE_ID) values(" + vChildId + "," + vId + ",'" + voucherType + "','" + voucherDate + "','" + plevelId + "','',''," + amount + ",now(),'" + userId + "','" + ipAddress + "','0','" + userStoreId + "') ");
//                        Query qVoucherChild6 = dbsession.createSQLQuery("insert into VOUCHER_CHILD(ID,VOUCHER_ID,VOUCHER_TYPE,VOUCHER_DATE,PLEVEL_ID,ACCOUNT_NO,PAYEE,AMOUNT,ADD_DATETIME,ADD_USER,ADD_IP,DEL_OR_UP) values(V_CHILD_ID.nextval," + vId + ",'" + voucherType + "',to_date('" + voucherDate + "','YYYY-MM-DD'),'" + accountHead + "','" + chequeNo + "','" + payeeId + "'," + drValue + ",systimestamp,'" + userId + "','" + ipAddress + "','0') ");
                        qVoucherChild6.executeUpdate();

                    }

                }
                q2 = dbsession.createSQLQuery("select v.plevel_id,f.ed,f.td, DATE_ADD(f.td, INTERVAL 3 HOUR)post_date, sum(amount) amount\n"
                        + " from voucher_child v,gl_code g,gl_type gt,financial_year f,g_general_gl gl\n"
                        + " where gt.gl_type_id not in (3,4)\n"
                        + " and g.gl_type_id=gt.gl_type_id\n"
                        + " and v.plevel_id=g.gl_code\n"
                        + " and v.del_or_up=0\n"
                        + " and g.store_id='" + userStoreId + "'\n"
                        + " and v.voucher_date>=f.ed and  v.voucher_date<=f.td\n"
                        + " and f.financial_year='" + billRef + "'\n"
                        + " and gl.store_id='" + userStoreId + "'\n"
                        + " and now() between gl.ed and gl.td\n"
                        + " and f.store_id='" + userStoreId + "'\n"
                        + " group by v.plevel_id,f.ed,f.td, DATE_ADD(f.td, INTERVAL 3 HOUR)");
                if (!q2.list().isEmpty()) {
                    for (Iterator itr1 = q2.list().iterator(); itr1.hasNext();) {

                        object2 = (Object[]) itr1.next();
                        plevelId = object2[0].toString();
                        ed = object2[1].toString();
                        td = object2[2].toString();
                        voucherDate = object2[3].toString();
                        amount = object2[4].toString();

                        vChildId = getId.getID(6);

                        Query qVoucherChild6 = dbsession.createSQLQuery("insert into VOUCHER_CHILD(ID,VOUCHER_ID,VOUCHER_TYPE,VOUCHER_DATE,PLEVEL_ID,ACCOUNT_NO,PAYEE,AMOUNT,ADD_DATETIME,ADD_USER,ADD_IP,DEL_OR_UP,STORE_ID) values(" + vChildId + "," + vId + ",'" + voucherType + "','" + voucherDate + "','" + plevelId + "','',''," + amount + ",now(),'" + userId + "','" + ipAddress + "','0','" + userStoreId + "') ");
//                        Query qVoucherChild6 = dbsession.createSQLQuery("insert into VOUCHER_CHILD(ID,VOUCHER_ID,VOUCHER_TYPE,VOUCHER_DATE,PLEVEL_ID,ACCOUNT_NO,PAYEE,AMOUNT,ADD_DATETIME,ADD_USER,ADD_IP,DEL_OR_UP) values(V_CHILD_ID.nextval," + vId + ",'" + voucherType + "',to_date('" + voucherDate + "','YYYY-MM-DD'),'" + accountHead + "','" + chequeNo + "','" + payeeId + "'," + drValue + ",systimestamp,'" + userId + "','" + ipAddress + "','0') ");
                        qVoucherChild6.executeUpdate();

                    }

                }

                Query qVoucher = dbsession.createSQLQuery(" insert into voucher (ID,VOUCHER_ID,VOUCHER_TYPE,PREPARED,CHECKEDBY,APPROVED,DESCRIPTION,VOUCHER_DATE,ADD_DATETIME,ADD_USER,ADD_IP,DEL_OR_UP,STORE_ID) values(" + vId + "," + vId + ",'" + voucherType + "','','','','" + description + "','" + voucherDate + "',now(),'" + userId + "','" + ipAddress + "','0','" + userStoreId + "' ) ");
                qVoucher.executeUpdate();

                ///String getPrevBal = ("select OPER_ID, sum(DEBIT)-sum(CREDIT) from AR_TXN group by OPER_ID");
                sqlstr = "update FINANCIAL_YEAR set STATUS='C' where FINANCIAL_YEAR='" + billRef + "' and store_id='" + userStoreId + "' ";
                qUpdateBillRef = dbsession.createSQLQuery(sqlstr);
                upcount = qUpdateBillRef.executeUpdate();

                q2 = dbsession.createSQLQuery("insert into voucher_child_hist select * from voucher_child where voucher_date>='" + ed + "' and voucher_date<='" + td + "' and store_id='" + userStoreId + "'");
                q2.executeUpdate();

                q2 = dbsession.createSQLQuery("insert into voucher_hist select * from voucher  where voucher_date>='" + ed + "' and voucher_date<='" + td + "' and store_id='" + userStoreId + "'");
                q2.executeUpdate();

                q2 = dbsession.createSQLQuery("delete from voucher_child where voucher_date>='" + ed + "' and voucher_date<='" + td + "' and store_id='" + userStoreId + "'");
                q2.executeUpdate();

                q2 = dbsession.createSQLQuery("delete from voucher  where voucher_date>='" + ed + "' and voucher_date<='" + td + "' and store_id='" + userStoreId + "'");
                q2.executeUpdate();
 
                result = "Successfully Generated";
                logger.info("Update record into FINANCIAL_YEAR set STATUS");
                dbtrx.commit();

            }

            dbsession.close();
        } catch (Exception e) {
            dbtrx.rollback();
            dbsession.close();
            e.printStackTrace();
            dbsession.close();
            result = e.getMessage();
        }
        return result;

    }

}
