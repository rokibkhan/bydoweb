/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import java.io.PrintWriter;
import org.json.simple.JSONObject;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONArray;
/**
 *
 * @author HP
 */
@WebServlet(name = "CouponCheck", urlPatterns = {"/CouponCheck"})
public class CouponCheck extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CouponCheck</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CouponCheck at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        String coupon = request.getParameter("coupon");
        
        String courseid = request.getParameter("courseid");
        
        String cartid = request.getParameter("cartid");
        
        System.out.println("coupon:"+coupon);
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();
        
        Object[] obj = null;
        Object[] obj2 = null;
        Object[] obj3 = null;
        Object[] obj4 = null;
        
        int discountid = 0;
        String discount_title = "";
        String discount_description = "";
        int uses = 0;
        int max_uses = 0;
        int discount_type = 0;
        double fixed_amount = 0.0D;
        double percentage_amount = 0.0D;
        String ed = "";
        String td = "";
        int member_discount_code_id = 0;
        int member_id_discount_code = 0;
        int course_discount_code_id = 0;
        int course_id_discount_code = 0;
        int discountcheck = 0;
        String sessionIdH = "";
        String userNameH = "";
        String memberIdH = "";
        
        double rate = 0.0D;
        double discount = 0.0D;
        double updatediscount = 0.0D;
        double amount = 0.0D;
        
        
        JSONArray jsonArr = new JSONArray();
        JSONObject json = new JSONObject();
        String responseCode = "";
        String responseMsg = "";
        String responseMsgHtml = "";
    
        HttpSession session = request.getSession(true);
        if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();


        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();
        
        try {
        
        Query SQL = dbsession.createSQLQuery("SELECT ID,DISCOUNT_TITLE,DISCOUNT_DESCRIPTION,USES,MAX_USES,DISCOUNT_TYPE,FIXED_AMOUNT,PERCENTAGE_AMOUNT,ED,TD"
                                            + " FROM  discount where DISCOUNT_CODE = '"+ coupon+"'");

        
         for (Iterator it = SQL.list().iterator(); it.hasNext();) {
            
            obj = (Object[]) it.next();
            //JSONObject videoObj = new JSONObject();
            
            //videoObj.put("idVideo",obj[0].toString()); 
            discountid = Integer.parseInt(obj[0].toString());
            discount_title = obj[1].toString();
            discount_description = obj[2].toString();
            uses = Integer.parseInt(obj[3].toString());
            max_uses = Integer.parseInt(obj[4].toString());
            discount_type = Integer.parseInt(obj[5].toString());
            fixed_amount = Double.parseDouble(obj[6].toString());
            percentage_amount = Double.parseDouble(obj[7].toString());
            ed = obj[8].toString();
            td = obj[9].toString();
            
            
            Query caSQL = dbsession.createSQLQuery("SELECT ID,MEMBER_ID,COURSE_ID,RATE,DISCOUNT,AMOUNT,STATUS"
                                            + " FROM  cart where ID = '"+ cartid+"' and MEMBER_ID ='"+ memberIdH+"'");

                for (Iterator it4 = caSQL.list().iterator(); it4.hasNext();) {
                    obj4 = (Object[]) it4.next();
                    
                    rate = Double.parseDouble(obj4[3].toString());
                    discount = Double.parseDouble(obj4[4].toString());
                    amount = Double.parseDouble(obj4[5].toString());
                    
                    
                }
            
            if(discount_type == 1)
            {
                Query mdSQL = dbsession.createSQLQuery("SELECT id,member_id,status"
                                            + " FROM  member_discount_code where discount_id = '"+ discountid+"'");

                for (Iterator it2 = mdSQL.list().iterator(); it2.hasNext();) {
                    obj2 = (Object[]) it2.next();
                    
                    member_discount_code_id = Integer.parseInt(obj2[0].toString());
                    member_id_discount_code = Integer.parseInt(obj2[1].toString());
                    discountcheck = 1;
                    updatediscount = fixed_amount;
                
                }
                
            }
            else if(discount_type == 2)
            { 
                Query cdSQL = dbsession.createSQLQuery("SELECT ID,COURSE_ID,status"
                                            + " FROM  course_discount_code where discount_id = '"+ discountid+"'");

                for (Iterator it3 = cdSQL.list().iterator(); it3.hasNext();) {
                    obj3 = (Object[]) it3.next();
                    
                    course_discount_code_id = Integer.parseInt(obj3[0].toString());
                    course_id_discount_code = Integer.parseInt(obj3[1].toString());
                    if(Integer.parseInt(courseid) == course_id_discount_code)
                    {
                        discountcheck = 1;
                        updatediscount = (rate * percentage_amount)/100;
                    }
                    
                }
                
            }
                
            if(discountcheck == 1)
            {

                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
                Date now = new Date();
                
                //System.out.println("todaydate:"+todayDate);
                String tDate = sdfDate.format(now);
                Date todayDate = sdfDate.parse(tDate);
                
                
                Date effdate = sdfDate.parse(ed);
                Date tilldate = sdfDate.parse(td);
                    
                
                
                if( (uses < max_uses) && ( (todayDate.after(effdate) && todayDate.before(tilldate) || ( todayDate.equals(effdate) || todayDate.equals(tilldate)))))
                {
                    
                    getRegistryID getId = new getRegistryID();
                    String idS = getId.getID(74);
                    int discounthistoryId = Integer.parseInt(idS);
                    
                    Query q1 = dbsession.createSQLQuery("INSERT INTO discount_uses_history("
                    + "id,"
                    + "member_id,"
                    + "discount_id,"
                    + "uses_date,"
                    + "add_date,"
                    + "add_user,"
                    + "add_term"
                    
                    + ") VALUES("
                    + "'" + discounthistoryId + "',"
                    + "'" + memberIdH + "',"
                    + "'" + discountid + "',"
                    + "now(),"
                    + "now(),"
                    + "'" + memberIdH + "',"                    
                    
                    + "2020"                            
                    + "  ) ");
            q1.executeUpdate();
            
            
            Query disUpdSQL = dbsession.createSQLQuery("UPDATE  discount SET uses='" + (uses+1) + "',MOD_USER ='" + memberIdH +  "',MOD_DATE=now() WHERE ID='" + discountid + "'");

            disUpdSQL.executeUpdate();
            
            
            
            
            
            
            
            Query cartUpdSQL = dbsession.createSQLQuery("UPDATE  cart SET DISCOUNT='" + updatediscount +"',AMOUNT ='" + (rate-updatediscount)  + "',MOD_USER ='" + memberIdH +  "',MOD_DATE=now() WHERE ID='" + cartid + "'");

            cartUpdSQL.executeUpdate();
            
            dbsession.flush();
            dbtrx.commit();
            dbsession.close();
            
             if (dbtrx.wasCommitted()) {

                 
                responseCode = "1";
                responseMsg = "You have discount for the course succesfully";
                responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                            + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>";

                
                //response.sendRedirect(GlobalVariable.baseUrl + "/course/coursesubscriptionsuccess.jsp?strMsg=" + strMsg);
                //response.sendRedirect("coursesubscriptionsuccess.jsp?strMsg=" + strMsg);
                //response.sendRedirect("registrationVerification.jsp?regTempId=" + member_temp_id + "&regMemId=" + regMemberId + "&regStep=2&strMsg=" + strMsg);
            } else {
                responseCode = "0";
                responseMsg = "Error!!! You have no coupon";
                responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                            + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>";


                dbtrx.rollback();
                //response.sendRedirect("coursesubscription.jsp?strMsg=" + strMsg);
            }
            
        
             

             }
                
            }
            
         }
         
         PrintWriter out = response.getWriter();
        //out.print(teacherlist);
        json.put("rate", rate);
        json.put("discount", updatediscount);
        json.put("amount", (rate-updatediscount));
        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        //json.put("mainInfoContainer", mainInfoContainer);
        //json.put("requestId", videoId);
        jsonArr.add(json);
        out.println(jsonArr);
        out.flush();
        out.close();

  
    
            
                } catch (ParseException ex) {
                    Logger.getLogger(CouponCheck.class.getName()).log(Level.SEVERE, null, ex);
                }
    }
            else
            {
                response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
            }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        String coupon = request.getParameter("coupon");
        
        String courseid = request.getParameter("courseid");
        
        String cartid = request.getParameter("cartid");
        
        System.out.println("coupon:"+coupon);
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();
        
        Object[] obj = null;
        Object[] obj2 = null;
        Object[] obj3 = null;
        Object[] obj4 = null;
        
        int discountid = 0;
        String discount_title = "";
        String discount_description = "";
        int uses = 0;
        int max_uses = 0;
        int discount_type = 0;
        double fixed_amount = 0.0D;
        double percentage_amount = 0.0D;
        String ed = "";
        String td = "";
        int member_discount_code_id = 0;
        int member_id_discount_code = 0;
        int course_discount_code_id = 0;
        int course_id_discount_code = 0;
        int discountcheck = 0;
        String sessionIdH = "";
        String userNameH = "";
        String memberIdH = "";
        
        double rate = 0.0D;
        double discount = 0.0D;
        double updatediscount = 0.0D;
        double amount = 0.0D;
        
        
        JSONArray jsonArr = new JSONArray();
        JSONObject json = new JSONObject();
        String responseCode = "";
        String responseMsg = "";
        String responseMsgHtml = "";
    
        HttpSession session = request.getSession(true);
        if (session.getAttribute("username") != null && session.getAttribute("memberId") != null) {

        sessionIdH = session.getId();


        userNameH = session.getAttribute("username").toString();
        memberIdH = session.getAttribute("memberId").toString();
        
        try {
        
        Query SQL = dbsession.createSQLQuery("SELECT ID,DISCOUNT_TITLE,DISCOUNT_DESCRIPTION,USES,MAX_USES,DISCOUNT_TYPE,FIXED_AMOUNT,PERCENTAGE_AMOUNT,ED,TD"
                                            + " FROM  discount where DISCOUNT_CODE = '"+ coupon+"'");

        
         for (Iterator it = SQL.list().iterator(); it.hasNext();) {
            
            obj = (Object[]) it.next();
            //JSONObject videoObj = new JSONObject();
            
            //videoObj.put("idVideo",obj[0].toString()); 
            discountid = Integer.parseInt(obj[0].toString());
            discount_title = obj[1].toString();
            discount_description = obj[2].toString();
            uses = Integer.parseInt(obj[3].toString());
            max_uses = Integer.parseInt(obj[4].toString());
            discount_type = Integer.parseInt(obj[5].toString());
            fixed_amount = Double.parseDouble(obj[6].toString());
            percentage_amount = Double.parseDouble(obj[7].toString());
            ed = obj[8].toString();
            td = obj[9].toString();
            
            
            /*Query caSQL = dbsession.createSQLQuery("SELECT ID,MEMBER_ID,COURSE_ID,RATE,DISCOUNT,AMOUNT,STATUS"
                                            + " FROM  cart where ID = '"+ discountid+"' and MEMBER_ID= '"+ memberIdH+"'");

                for (Iterator it4 = caSQL.list().iterator(); it4.hasNext();) {
                    obj4 = (Object[]) it4.next();
                    
                    rate = Double.parseDouble(obj4[3].toString());
                    discount = Double.parseDouble(obj4[4].toString());
                    amount = Double.parseDouble(obj4[5].toString());
                    
                    
                }
            */
            if(discount_type == 1)
            {
                Query mdSQL = dbsession.createSQLQuery("SELECT id,member_id,status"
                                            + " FROM  member_discount_code where discount_id = '"+ discountid+"'");

                for (Iterator it2 = mdSQL.list().iterator(); it2.hasNext();) {
                    obj2 = (Object[]) it2.next();
                    
                    member_discount_code_id = Integer.parseInt(obj2[0].toString());
                    member_id_discount_code = Integer.parseInt(obj2[1].toString());
                    discountcheck = 1;
                    updatediscount = fixed_amount;
                
                }
                
            }
            else if(discount_type == 2)
            { 
                Query cdSQL = dbsession.createSQLQuery("SELECT ID,COURSE_ID,status"
                                            + " FROM  course_discount_code where discount_id = '"+ discountid+"'");

                for (Iterator it3 = cdSQL.list().iterator(); it3.hasNext();) {
                    obj3 = (Object[]) it3.next();
                    
                    course_discount_code_id = Integer.parseInt(obj3[0].toString());
                    course_id_discount_code = Integer.parseInt(obj3[1].toString());
                    if(Integer.parseInt(courseid) == course_id_discount_code)
                    {
                        discountcheck = 1;
                        updatediscount = (rate * percentage_amount)/100;
                    }
                    
                }
                
            }
                
            if(discountcheck == 1)
            {

                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
                Date todayDate = sdfDate.parse(new Date().toString());
                
                System.out.println("todaydate:"+todayDate);
                //String tDate = sdfDate.format(now);
                //Date todayDate = sdfDate.parse(tDate);
                
                
                Date effdate = sdfDate.parse(ed);
                Date tilldate = sdfDate.parse(td);
                    
                
                
                if( (uses < max_uses) && ( (todayDate.after(effdate) && todayDate.before(tilldate) || ( todayDate.equals(effdate) || todayDate.equals(tilldate)))))
                {
                    
                    getRegistryID getId = new getRegistryID();
                    String idS = getId.getID(74);
                    int discounthistoryId = Integer.parseInt(idS);
                    
                    Query q1 = dbsession.createSQLQuery("INSERT INTO discount_uses_history("
                    + "id,"
                    + "member_id,"
                    + "discount_id,"
                    + "uses_date,"
                    + "add_date,"
                    + "add_user,"
                    + "add_term"
                    
                    + ") VALUES("
                    + "'" + discounthistoryId + "',"
                    + "'" + memberIdH + "',"
                    + "'" + discountid + "',"
                    + "now(),"
                    + "now(),"
                    + "'" + memberIdH + "',"                    
                    
                    + "2020"                            
                    + "  ) ");
            q1.executeUpdate();
            
            
            Query disUpdSQL = dbsession.createSQLQuery("UPDATE  discount SET uses='" + (uses+1) + "',MOD_USER ='" + memberIdH +  "',MOD_DATE=now() WHERE ID='" + discountid + "'");

            disUpdSQL.executeUpdate();
            
            
            
            
            
            
            
            Query cartUpdSQL = dbsession.createSQLQuery("UPDATE  cart SET DISCOUNT='" + updatediscount +"',AMOUNT ='" + (rate-updatediscount)  + "',MOD_USER ='" + memberIdH +  "',MOD_DATE=now() WHERE ID='" + cartid + "'");

            cartUpdSQL.executeUpdate();
            
            dbsession.flush();
            dbtrx.commit();
            dbsession.close();
            
             if (dbtrx.wasCommitted()) {

                responseCode = "1";
                responseMsg = "You have discount for the course succesfully";
                responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                            + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>";

                
                //response.sendRedirect(GlobalVariable.baseUrl + "/course/coursesubscriptionsuccess.jsp?strMsg=" + strMsg);
                //response.sendRedirect("coursesubscriptionsuccess.jsp?strMsg=" + strMsg);
                //response.sendRedirect("registrationVerification.jsp?regTempId=" + member_temp_id + "&regMemId=" + regMemberId + "&regStep=2&strMsg=" + strMsg);
            } else {
                responseCode = "0";
                responseMsg = "Error!!! You have no coupon";
                responseMsgHtml = "<div class=\"alert alert-primary alert-dismissable fade in\" style=\"border-left: 4px solid #4CAF50;\">"
                            + "<a href=\"#\" class=\"close closeTT\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"
                            + "<i class=\"fa fa-check-circle-o fa-lg\"></i> " + responseMsg + "</span><a href=\"#\" class=\"closed\" data-dismiss=\"alert\">&times;</a>";


                dbtrx.rollback();
                //response.sendRedirect("coursesubscription.jsp?strMsg=" + strMsg);
            }
            
        
             

             }
                
            }
            
         }
         
         PrintWriter out = response.getWriter();
        //out.print(teacherlist);
        
        json.put("responseCode", responseCode);
        json.put("responseMsg", responseMsg);
        json.put("responseMsgHtml", responseMsgHtml);
        //json.put("mainInfoContainer", mainInfoContainer);
        //json.put("requestId", videoId);
        jsonArr.add(json);
        out.println(jsonArr);
        out.flush();
        out.close();

  
    
            
                } catch (ParseException ex) {
                    Logger.getLogger(CouponCheck.class.getName()).log(Level.SEVERE, null, ex);
                }
    }
            else
            {
                response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
            }
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
