/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.GlobalVariable;
import org.apache.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.http.client.utils.URIBuilder;

/**
 *
 * @author Akter and Tahajjat
 * @bccOption 1-> add BCC; 0 or else->not add BCC
 */
public class SendEmail {

    public static Logger logger = Logger.getLogger("SendEmail");
    private static String USER_AGENT = "Mozilla/5.0";

    @SuppressWarnings("empty-statement")
    public static String sendEmail(String messageSubject, String messageBody, String receipentEmail, String bccOption) throws IOException, URISyntaxException, MessagingException {

        String host = GlobalVariable.SMTPHost;
        String userName = GlobalVariable.SMTPUserName;
        String password = GlobalVariable.SMTPUserPassword;

        String fromEmailId = GlobalVariable.fromEmailId;
        String fromEmailIdCustom = GlobalVariable.fromEmailIdCustom;

        String bccEmailId = GlobalVariable.bccEmailId;
        
        String ret = "";
        String result = "";

        boolean sessionDebug = false;
// Create some properties and get the default Session.
        Properties props = System.getProperties();
        props.put("mail.host", host);
        props.put("mail.transport.protocol", "smtp");
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", host);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");

        javax.mail.Authenticator auth = new javax.mail.Authenticator() {
            public javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        };

        javax.mail.Session mailSession = javax.mail.Session.getInstance(props, auth);
        mailSession.setDebug(sessionDebug);

        // Get the default Session object.
//    javax.mail.Session mailSession = javax.mail.Session.getDefaultInstance(properties);
        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(mailSession);
            // Set From: header field of the header.
            message.setFrom(new InternetAddress(fromEmailId));
            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(receipentEmail));

            if (bccOption.equals("1")) {
                message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bccEmailId));
            }

            // Set Subject: header field
            message.setSubject(messageSubject);
            // Now set the actual message
            BodyPart messageBodyPart = new MimeBodyPart();

            messageBodyPart.setContent(messageBody, "text/html; charset=UTF-8");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            messageBodyPart = new MimeBodyPart();
            /*
            String fileName = "/app/APPUL/webapps/APPUL/Reports/invoice/" + txnId + ".pdf";
//                String fileName = "/Users/Akter/NetBeansProjects/APPUL/web/Reports/invoice/" +txnId+ ".pdf";
            String fileName1 = txnId + ".pdf";

            javax.activation.DataSource source = new FileDataSource(fileName);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(fileName1);
            multipart.addBodyPart(messageBodyPart);
             */

            message.setContent(multipart, "text/html");

            System.out.println("message " + message);
            // Send message
            Transport.send(message);
            result = "Sent message successfully....";

            return result;

        } catch (MessagingException mex) {
            mex.printStackTrace();
            result = "Error: unable to send message....";

            return result;
        }

    }

//    public static void main(String args[]) throws IOException, URISyntaxException {
//        String sms = "This is test Message";
//        String receipentMobileNo = "+8801955538279";
//        System.out.println("SMS : " + sms);
//        System.out.println("receipentMobileNo : " + receipentMobileNo);
//        System.out.println(SendSMS(sms, receipentMobileNo));
//    }
}
