/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.DBAccess;
import com.appul.util.getRegistryID;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet(name = "TeamUpload", urlPatterns = {"/TeamUpload"})
@MultipartConfig
public class TeamUpload extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

//            Part playerId = request.getPart("playerId");
            Part firstName = request.getPart("shortName");
            Part lastName = request.getPart("fullName");
            Part coachId = request.getPart("coachId");
            Part countryId = request.getPart("countryId");
//            Part userId = request.getPart("username");

//            Scanner playerIdS = new Scanner(playerId.getInputStream());
            Scanner firstNameS = new Scanner(firstName.getInputStream());
            Scanner lastNameS = new Scanner(lastName.getInputStream());
            Scanner coachIdS = new Scanner(coachId.getInputStream());

            Scanner countryIdS = new Scanner(countryId.getInputStream());
//            Scanner userIdS = new Scanner(userId.getInputStream());

//            String playerId1 = playerIdS.nextLine();
            String firstName1 = firstNameS.nextLine();
            String lastName1 = lastNameS.nextLine();
            String coachId1 = coachIdS.nextLine();

            String countryId1 = countryIdS.nextLine();
//            String userId1 = userIdS.nextLine();

            System.out.println(firstName1 + "|" + lastName1);
            Part photo = request.getPart("file1");

            getRegistryID getId = new getRegistryID();
            String id = getId.getID(17);

            DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            Date date = new Date();
            String entryDate = dateFormat.format(date);
//            System.out.println("Entry Date="+entryDate);
            // Connect to MySql
            DBAccess dbconn = new DBAccess();
            Connection con = dbconn.dbsrc();

            PreparedStatement ps = con.prepareStatement("insert into team values(?,?,?,?,?,?)");

            ps.setString(1, id);
            ps.setString(2, firstName1);
            ps.setString(3, lastName1);
            // size must be converted to int otherwise it results in error
            ps.setBinaryStream(4, photo.getInputStream(), (int) photo.getSize());
            ps.setString(5, coachId1);
            ps.setString(6, countryId1);
            ps.executeUpdate();

//            con.commit();
            con.close();
            out.println("Team Added Successfully.");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            out.close();
        }
    }
}
