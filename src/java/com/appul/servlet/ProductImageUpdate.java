/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

@WebServlet(name = "ProductImageUpdate", urlPatterns = {"/ProductImageUpdate"})
@MultipartConfig
public class ProductImageUpdate extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String userId = request.getSession(true).getAttribute("username").toString();
            Part styleIdP = request.getPart("styleId");
            Part productIdP = request.getPart("productId");

            Part file1P = request.getPart("file1");
            Part file2P = request.getPart("file2");

            Scanner styleIdS = new Scanner(styleIdP.getInputStream());
            Scanner productIdS = new Scanner(productIdP.getInputStream());
            
            String productCode = productIdS.nextLine();
            int productId = Integer.parseInt(productCode);


            InetAddress ip;
            String hostname = "";
            String ipAddress = "";
            try {
                ip = InetAddress.getLocalHost();
                hostname = ip.getHostName();
                ipAddress = ip.getHostAddress();

            } catch (UnknownHostException e) {

                e.printStackTrace();
            }

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String entryDate = dateFormat.format(date);
//            DBAccess dbconn = new DBAccess();
//            Connection con = dbconn.dbsrc();

            Session dbsession = HibernateUtil.getSessionFactory().openSession();
            SessionImpl sessionImpl = (SessionImpl) dbsession;
            Connection con = sessionImpl.connection();

            PreparedStatement ps = con.prepareStatement("update product set product_image=?,color_image=? where product_id=? ");

            // size must be converted to int otherwise it results in error
            ps.setBinaryStream(1, file1P.getInputStream(), (int) file1P.getSize());
            ps.setBinaryStream(2, file2P.getInputStream(), (int) file2P.getSize());

            ps.setInt(3, productId);

            ps.executeUpdate();
            con.commit();
            ps.close();
            con.close();
            dbsession.close();
            out.println("Successfully Added Product Image.");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            out.close();
        }
    }
}
