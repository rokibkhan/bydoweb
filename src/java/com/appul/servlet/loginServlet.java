/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.entity.Member;
import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Akter
 */
public class loginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int errorCode = 0;
        PrintWriter out = response.getWriter();
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        HttpSession session = request.getSession(true);

        try {

            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String firstLetter = username.substring(0, 1);
            String adjustLettter1 = "+88";
            String adjustLettter2 = "+";

            String mobileNumber1 = "";

            if (firstLetter.equals("0")) {
                mobileNumber1 = adjustLettter1 + username;
            }
            if (firstLetter.equals("8")) {
                mobileNumber1 = adjustLettter2 + username;
            }
            if (firstLetter.equals("+")) {
                mobileNumber1 = username;
            }

            Member member = null;
            Encryption encryption = new Encryption();

            int memId = 0;
            String dbPass = "";
            String dbPassStatus = "";

            Query qMember = null;
            Query memberKeySQL = null;
            Object[] memberKeyObj = null;

            qMember = dbsession.createQuery(" from Member where mobile = '" + mobileNumber1 + "'");
            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();

                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key,status FROM member_credential where member_id='" + memId + "'");

                    for (Iterator memberKeyItr = memberKeySQL.list().iterator(); memberKeyItr.hasNext();) {
                        memberKeyObj = (Object[]) memberKeyItr.next();
                        dbPass = memberKeyObj[0].toString();
                        dbPassStatus = memberKeyObj[1] == null ? "" : memberKeyObj[1].toString();

                        if (dbPass.equalsIgnoreCase(encryption.getEncrypt(password))) {
                            session.setAttribute("username", mobileNumber1);
                            session.setAttribute("logstat", "Y");
                            session.setAttribute("memberId", memId);

                            response.sendRedirect(GlobalVariable.baseUrl + "/member/memberDashboard.jsp?sessionid=" + session.getId());
                        } else {
                            //Password wrong
                            errorCode = 1;
                            session.setAttribute("rtnUser", errorCode);
                            response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
                        }
                    }
                }
            } else {
                //Member Not found
                errorCode = 1;
                session.setAttribute("rtnUser", errorCode);
                response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
                
            }

        } catch (Exception theException) {
            System.out.println(theException);
        } finally {
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
