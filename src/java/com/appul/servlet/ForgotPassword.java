/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.entity.AddressBook;
import com.appul.entity.Member;
import com.appul.entity.MemberAddress;
import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.RandomString;
import com.appul.util.getRegistryID;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Zeeon
 */
@WebServlet(name = "ForgotPassword", urlPatterns = {"/ForgotPassword"})
public class ForgotPassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();
        HttpSession session = request.getSession(true);
        try {
            String memberId = "";
            String memberMobileNumber = "";

            getRegistryID getId = new getRegistryID();

            String adddate = "";

            DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
            Date dateToday = new Date();

            memberMobileNumber = request.getParameter("username").trim();

            memberId = memberMobileNumber;

            String firstLetter = memberMobileNumber.substring(0, 1);
            String adjustLettter1 = "+88";
            String adjustLettter2 = "88";
            String adjustLettter3 = "+";

            String mobileNumber1 = "";
            String mobileNumber2 = "";

            if (firstLetter.equals("0")) {
                mobileNumber1 = adjustLettter1 + memberMobileNumber;
                mobileNumber2 = adjustLettter2 + memberMobileNumber;

                System.out.println("firstLetter :: 0 :: mobileNumber1 ::" + mobileNumber1);
                System.out.println("firstLetter :: 0 :: mobileNumber2 ::" + mobileNumber2);
            }
            else if (firstLetter.equals("8")) {
                mobileNumber1 = adjustLettter3 + memberMobileNumber;
                mobileNumber2 = adjustLettter3 + memberMobileNumber;
                System.out.println("firstLetter :: 8 :: mobileNumber1 ::" + mobileNumber1);
                System.out.println("firstLetter :: 8 :: mobileNumber2 ::" + mobileNumber2);
            }
            else if (firstLetter.equals("+")) {
                mobileNumber1 = memberMobileNumber;
                mobileNumber2 = memberMobileNumber;

                System.out.println("firstLetter :: + :: mobileNumber1 ::" + mobileNumber1);
                System.out.println("firstLetter :: + :: mobileNumber2 ::" + mobileNumber2);

            } else {
                String strMsg = "Invalid Phone Number";
                session.setAttribute("strMsg", strMsg);
                dbsession.flush();
                dbsession.close();
                response.sendRedirect(GlobalVariable.baseUrl + "/member/forgotPassword.jsp");
            }

            Member member = null;
            MemberAddress memberAddress = null;
            AddressBook addressBook = null;

            int memId = 0;
            String memberName = "";
            String dbPass = "";
            String memberEmail = "";
            String memberMobile = "";
            String memberType = "";
            String fatherName = "";
            String motherName = "";
            String placeOfBirth = "";
            java.util.Date dob = new Date();
            String gender = "";
            String mobileNo = "";
            String phone1 = "";
            String phone2 = "";
            String bloodGroup = "";
            String pictureName = "";
            String pictureLink = "";
            String strMsg = "";

            Encryption encryption = new Encryption();
            Query qMember = null;

            SendSMS sendsms = new SendSMS();
            SendEmail sendemail = new SendEmail();

            RandomString randNumber = new RandomString();

            Random random = new Random();
            int rand1 = Math.abs(random.nextInt());
            int rand2 = Math.abs(random.nextInt());

            long uniqueNumber = System.currentTimeMillis();

            String memberUniqueNumber = uniqueNumber + "-" + rand1 + "-" + rand2;

            //String pinCode = randNumber.randomString(4,2);
            String member_pass_code_phone = randNumber.randomString(4, 2);
            String member_pass_code_email = randNumber.randomString(6, 2);

            qMember = dbsession.createQuery(" from Member where mobile = '" + mobileNumber1 + "' AND status = '1'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();

                    memId = member.getId();

                    memberName = member.getMemberName();

                    memberMobile = member.getMobile() == null ? "" : member.getMobile();
                    phone1 = member.getPhone1() == null ? "" : member.getPhone1();
                    phone2 = member.getPhone2() == null ? "" : member.getPhone2();
                    memberEmail = member.getEmailId() == null ? "" : member.getEmailId();

                    int otpcount = 0;

                    String today = dateFormatYmd.format(dateToday);
                    Query memberOtpCount = dbsession.createSQLQuery("SELECT count(otp) FROM  member_otp  where member_id = '" + memId + "' AND status = '1' AND  DATE(otp_sent_date) = '" + today + "'");

                    if (!memberOtpCount.list().isEmpty()) {
                        otpcount = Integer.parseInt(memberOtpCount.uniqueResult().toString());

                    } else {
                        // System.out.println("Do not Get Member ID of  " + username);
                    }

                    //check New Mobile Number with old Mobile number
                    if (otpcount >= 5) {
                        //OTP count over
                        strMsg = "Your OTP quota for today fulfilled";
                        request.setAttribute("strMsg", strMsg);
                        dbsession.flush();
                        dbsession.close();
                        response.sendRedirect(GlobalVariable.baseUrl + "/member/forgotPassword.jsp?");
                    }
                    String smsMsg = "# " + member_pass_code_phone + " is your BYDO App OTP code.Never share this code with anyone. 4/w+ZFwk7Zq";

                    //  String receipentMobileNo = "+8801755656354";
                    String receipentMobileNo = memberMobile;

                    //    System.out.println(SendSMS(sms, receipentMobileNo));
                    String sendsms1 = sendsms.sendSMS(smsMsg, receipentMobileNo);

                    //
                    if (!(sendsms1.equals("sending failed!") || sendsms1.equals("Exception or server unreachable"))) {
                        String memberotpId = getId.getID(76);
                        InetAddress ip;
                        String hostname = "";
                        String ipAddress = "";
                        try {
                            ip = InetAddress.getLocalHost();
                            hostname = ip.getHostName();
                            ipAddress = ip.getHostAddress();

                        } catch (UnknownHostException e) {

                            e.printStackTrace();
                        }

                        adddate = dateFormatX.format(dateToday);
                        String adduser = Integer.toString(memId);
                        String status = "1";

                        Query memberOtpAddSQL = dbsession.createSQLQuery("INSERT INTO member_otp("
                                + "id,"
                                + "member_id,"
                                + "otp,"
                                + "otp_type,"
                                + "otp_sent_date,"
                                + "status,"
                                + "ed,"
                                + "td,"
                                + "add_user,"
                                + "add_date,"
                                + "add_term,"
                                + "add_ip,"
                                + "mod_date"
                                + ") VALUES("
                                + "'" + memberotpId + "',"
                                + "'" + memId + "',"
                                + "'" + member_pass_code_phone + "',"
                                + "'" + "FORGET_PASSWORD" + "',"
                                + "'" + adddate + "',"
                                + "'" + status + "',"
                                + "'" + adddate + "',"
                                + "'" + "2020-09-30" + "',"
                                + "'" + adduser + "',"
                                + "'" + adddate + "',"
                                + "'" + hostname + "',"
                                + "'" + ipAddress + "',"
                                + "'" + adddate + "'"
                                + "  ) ");

                        System.out.println("memberOtpAddSQL ::" + memberOtpAddSQL);

                        memberOtpAddSQL.executeUpdate();

                    }
                    String newPassEnc = new Encryption().getEncrypt(member_pass_code_phone);

                    Query passUpdateSQL = dbsession.createSQLQuery("UPDATE  member_credential SET member_key ='" + newPassEnc + "',update_time=now(),status= '2' WHERE member_id='" + memId + "'");
                    passUpdateSQL.executeUpdate();
                    dbtrx.commit();

                    if (dbtrx.wasCommitted()) {
                        response.sendRedirect(GlobalVariable.baseUrl + "/member/otpVerification.jsp?username=" + mobileNumber1);
                    } else {
                        //Password Not updated
                        strMsg = "There was an error! Try Again Later";
                        session.setAttribute("strMsg", strMsg);
                        dbsession.flush();
                        dbsession.close();
                        response.sendRedirect(GlobalVariable.baseUrl + "/member/forgotPassword.jsp");
                    }

                }
            } else {
                //Member not found
                strMsg = "Member Not Found!";
                session.setAttribute("strMsg", strMsg);
                dbsession.flush();
                dbsession.close();
                response.sendRedirect(GlobalVariable.baseUrl + "/member/forgotPassword.jsp");
            }

        } catch (Exception theException) {
            System.out.println(theException);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
