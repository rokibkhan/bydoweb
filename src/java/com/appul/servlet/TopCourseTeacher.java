/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.entity.CourseInfo;
import com.appul.entity.CourseOwner;
import com.appul.entity.Member;
import com.appul.entity.SessionInfo;
import com.appul.entity.SubjectInfo;
import com.appul.entity.VideoGalleryInfo;
import com.appul.util.HibernateUtil;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author HP
 */
@WebServlet(name = "TopCourseTeacher", urlPatterns = {"/TopCourseTeacher"})
public class TopCourseTeacher extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TopCourseTeacher</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet TopCourseTeacher at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        System.out.println("Top Teacher of Course");
        
        //String teacherid = request.getParameter("teacherid");
        //System.out.println("teacherid:"+teacherid);
       
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        //org.hibernate.Transaction dbtrx = null;
        //dbtrx = dbsession.beginTransaction();
        
        Query SQL = dbsession.createSQLQuery("SELECT id,member_id,course_id"
                                            + " FROM  course_owner  ");
        
        JSONArray teacherlist = new JSONArray();
       // List<VideoComment> prodlist = SQL.list();

        Object[] obj = null;
        Object[] obj2 = null;
        Object[] obj3 = null;        
        
        int i = 0;
        
        for (Iterator it = SQL.list().iterator(); it.hasNext();) {
            
            JSONObject teacherObj = new JSONObject();
                                            
            obj = (Object[]) it.next();
            
            teacherObj.put("cid",obj[0].toString());                        
            
            Query sSQL = dbsession.createSQLQuery("SELECT id_subject,subject_name"
                                            + " FROM  subject_info where course_id = '"+ Integer.parseInt(obj[2].toString()) +"'");

            //var c =     from prod in 
            
            
            for (Iterator it2 = sSQL.list().iterator(); it2.hasNext();) {
               // si = new SessionInfo();  

                obj2 = (Object[]) it2.next();
            
                /*ci = new CourseInfo();
                        
                Set<String> si = new HashSet<>();
                si.add(obj2[1].toString());
                ci.setSubjectInfos(si);
                co.setCourseInfo(ci);*/
                
               teacherObj.put("subjectname",obj2[1].toString());
            }
            
            
            
            Query mSQL = dbsession.createSQLQuery("SELECT id,member_id,member_name,picture_name"
                                            + " FROM  member where id = '"+ Integer.parseInt(obj[1].toString()) +"'");

            //var c =     from prod in 
            
            
            for (Iterator it3 = mSQL.list().iterator(); it3.hasNext();) {             
                obj3 = (Object[]) it3.next();
                teacherObj.put("id",obj3[0].toString());                                                                    
                teacherObj.put("teachername",obj3[2].toString());
                teacherObj.put("teacherpicturename",obj3[3].toString());
            }
            
            teacherlist.add(i,teacherObj);
            i++;
        }
        
        PrintWriter out = response.getWriter();
        //out.print(gson.toJson(colist));
        out.print(teacherlist);
        out.flush();
        out.close();

        dbsession.flush();

        dbsession.close();

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
