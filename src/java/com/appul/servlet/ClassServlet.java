/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.appul.entity.*;
import com.appul.util.HibernateUtil;
import com.google.gson.Gson;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author HP
 */
@WebServlet(name = "ClassServlet", urlPatterns = {"/ClassServlet"})
public class ClassServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ClassServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ClassServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        System.out.println("ClassServlet");
       //   String name = request.getParameter("name");
       //response.getWriter().print("Hello "+ name + "!!");
       
        String videoid = request.getParameter("vid");
        System.out.println("videoid:"+videoid);
       
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
       // org.hibernate.Transaction dbtrx = null;
       // dbtrx = dbsession.beginTransaction();
        
        Query SQL = dbsession.createSQLQuery("SELECT *"
                                            + " FROM  video_comment where video_id ='"+videoid+"' order by commment_date desc ");

        
        
        Gson gson = new Gson();
        List<VideoComment> videocommentlist = new ArrayList<VideoComment>();

       // List<VideoComment> prodlist = SQL.list();

        Object[] obj2 = null;
        
        

        for (Iterator it2 = SQL.list().iterator(); it2.hasNext();) {
            
            VideoComment vc = new VideoComment();            
            obj2 = (Object[]) it2.next();
            vc.setId(Integer.parseInt(obj2[0].toString()));
            vc.setParent(Integer.parseInt(obj2[1].toString()));
            vc.setLikeCount(Integer.parseInt(obj2[2].toString()));
            //vc.setSolveStatus((obj2[2].toString()));
            vc.setCommmentDesc(obj2[4].toString());
            vc.setVideoId(Integer.parseInt(obj2[5].toString()));
            vc.setCommmentFrom(Integer.parseInt(obj2[6].toString()));            
            //vc.setCommmentDate(Date.parse(obj2[5].toString()));
            videocommentlist.add(vc);
            //videocommentlist.add(new VideoComment(Integer.parseInt(obj2[0].toString()),Integer.parseInt(obj2[1].toString()),
             //      1,11,(byte)1));
            
        }

        //products.add(new VideoComment(1,0,1,11,(byte)1));
        //products.add(new VideoComment(2,0,1,12,(byte)1));
        //products.add(new VideoComment(3,0,1,13,(byte)1));
        PrintWriter out = response.getWriter();
        out.print(gson.toJson(videocommentlist));
        out.flush();
        out.close();

        dbsession.flush();

        dbsession.close();


    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
