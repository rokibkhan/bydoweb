/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.appul.entity.*;
import com.appul.util.HibernateUtil;
import com.google.gson.Gson;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author HP
 */
@WebServlet(name = "MemberInfo", urlPatterns = {"/MemberInfo"})
public class MemberInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MemberInfo</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MemberInfo at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
          //String videoid = request.getParameter("vid");
        System.out.println("MEMBERINFO:");
       
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        //org.hibernate.Transaction dbtrx = null;
        //dbtrx = dbsession.beginTransaction();
        
        Query SQL = dbsession.createSQLQuery("SELECT m.id,m.member_id,m.member_name,m.picture_name,u.university_short_name,u.university_long_name,si.subject_name"
                                            + " FROM  member m join member_type mt on m.id = mt.member_id "
                                            +" join member_university mu on m.id = mu.member_id "
                                            +" join university u on u.university_id = mu.univerity_id  "
                                            +" join subject_teacher st on m.id = st.member_id"
                                            +" join subject_info si on st.subject_id = si.id_subject where mt.member_type_id = 1 order by m.id");        
        
        
        
        //List<Member> memberlist = new ArrayList<Member>();

       // List<VideoComment> prodlist = SQL.list();

        Object[] obj = null;
        
        Object[] obj2 = null;
        
        JSONArray teacherlist = new JSONArray();
        int i = 0;

        for (Iterator it = SQL.list().iterator(); it.hasNext();) {
            
            //Member mb = new Member();   
            //MemberUniversity mu = new MemberUniversity();
            JSONObject teacherObj = new JSONObject();
            
            obj = (Object[]) it.next();
            teacherObj.put("id",obj[0].toString()); 
            teacherObj.put("memberId",obj[1].toString()); 
            teacherObj.put("memberName",obj[2].toString()); 
            teacherObj.put("memberPctureName",obj[3].toString()); 
            teacherObj.put("memberUniversityShortName",obj[4].toString()); 
            teacherObj.put("memberUniversityLongName",obj[5].toString()); 
            teacherObj.put("memberSubjectName",obj[6].toString());
            
            /*Query stSQL = dbsession.createSQLQuery("SELECT si.subject_name"
                                            + " FROM  subject_teacher st join subject_info si on st.subject_id = si.id_subject where st.member_id ='"+Integer.parseInt(obj[0].toString())+"'");
                                            
            
            for (Iterator it2 = stSQL.list().iterator(); it2.hasNext();) {

                //Member mb = new Member();   
                //MemberUniversity mu = new MemberUniversity();
                
                //obj2 = (Object[]) it2.next();
                //obj2 = (Object[]) it2.next();
                teacherObj.put("memberSubjectName",obj[0].toString());
            }*/
            
           
            //mb.setId(Integer.parseInt(obj[0].toString()));
            //mb.setMemberId(obj[1].toString());
            //mb.setMemberName(obj[2].toString());
            //mb.setMemberUniversities(mu);
            //mb.setMemberUniversities(new MemberUniversity(1, mb, new University()));
            //vc.setSolveStatus((obj2[2].toString()));
            //vc.setCommmentDesc(obj2[4].toString());
            //vc.setVideoId(Integer.parseInt(obj2[5].toString()));
            //vc.setCommmentFrom(Integer.parseInt(obj2[6].toString()));            
            //vc.setCommmentDate(Date.parse(obj2[5].toString()));
            teacherlist.add(i,teacherObj);
            //memberlist.add(mb);
            i++;
            //videocommentlist.add(new VideoComment(Integer.parseInt(obj2[0].toString()),Integer.parseInt(obj2[1].toString()),
             //      1,11,(byte)1));
            
        }

        //products.add(new VideoComment(1,0,1,11,(byte)1));
        //products.add(new VideoComment(2,0,1,12,(byte)1));
        //products.add(new VideoComment(3,0,1,13,(byte)1));
        PrintWriter out = response.getWriter();
        out.print(teacherlist);
        out.flush();
        out.close();

        dbsession.flush();

        dbsession.close();

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
