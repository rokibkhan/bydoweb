/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.entity.AddressBook;
import com.appul.entity.Member;
import com.appul.entity.MemberAddress;
import com.appul.entity.MemberOrganization;
import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.RandomString;
import com.appul.util.getRegistryID;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Zeeon
 */
@WebServlet(name = "NewPassword", urlPatterns = {"/NewPassword"})
public class NewPassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();
        HttpSession session = request.getSession(true);
        try {
            getRegistryID getId = new getRegistryID();

            DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
            Date dateToday = new Date();

            String key = "";
            String mobileNumber = "";
            String verifyCode = "";

            String memberId = "";
            String givenPassword = "";
            String givenPassword1 = "";

            String givenPasswordEnc = "";
            String givenPassword1Enc = "";

            String adddate = "";

            mobileNumber = request.getParameter("username").trim();
            givenPassword = request.getParameter("password").trim();
            givenPassword1 = request.getParameter("password1").trim();

            System.out.println("Password :: " + givenPassword);
            System.out.println("Password1 :: " + givenPassword1);

            String firstLetter = mobileNumber.substring(0, 1);
            String adjustLettter1 = "+88";
            String adjustLettter2 = "+";

            String mobileNumber1 = "";

            if (firstLetter.equals("0")) {
                mobileNumber1 = adjustLettter1 + mobileNumber;
            }
            else if (firstLetter.equals("8")) {
                mobileNumber1 = adjustLettter2 + mobileNumber;
            }
            else if (firstLetter.equals("+")) {
                mobileNumber1 = mobileNumber;
            }
            else {
                
                String strMsg = "Invalid Phone Number";
                session.setAttribute("strMsg", strMsg);
                dbsession.flush();
                dbsession.close();
                response.sendRedirect(GlobalVariable.baseUrl + "/member/newPassword.jsp?username=" + mobileNumber1);
            }
            Member member = null;
            MemberOrganization mOrganization = null;
            MemberAddress memberAddress = null;
            AddressBook addressBook = null;

            int memId = 0;
            String memberName = "";
            String dbPass = "";
            String dbPassStatus = "";
            String memberEmail = "";
            String memberType = "";
            String fatherName = "";
            String motherName = "";
            String placeOfBirth = "";
            java.util.Date dob = new Date();
            String gender = "";
            String mobileNo = "";
            String phone1 = "";
            String phone2 = "";
            String bloodGroup = "";
            String pictureName = "";
            String pictureLink = "";

            int memberCenterId = 0;
            String memberCenterName = "";

            int memberDivisionId = 0;
            String memberDivisionShortName = "";
            String memberDivisionFullName = "";

            int memberAddressId = 0;
            String memberAddressType = "";

            String addressType = "";
            String consumerType = "";

            String mAddressLine1 = "";
            String mAddressLine2 = "";
            int mThanaId = 0;
            String mThanaId1 = "";
            String mThanaName = "";
            int mDistrictId = 0;
            String mDistrictName = "";
            String mCountry = "Bangladesh";
            String mZipCode = "";

            String pAddressLine1 = "";
            String pAddressLine2 = "";
            int pThanaId = 0;
            String pThanaName = "";
            int pDistrictId = 0;
            String pDistrictName = "";
            String pCountry = "Bangladesh";
            String pZipCode = "";

            String organizationName = "";
            String organizationLogo = "";
            String organizationLogoLink = "";
            String unitName = "";
            String nirbachoniAson = "";
            String ratingPoint = "";
            String likeCount = "";
            String shareCount = "";
            String uploadCount = "";
            String bloodDonationCount = "";

            Encryption encryption = new Encryption();

            Query qMember = null;
            Query memberKeySQL = null;

            Query mMemberTypeSQL = null;
            Object[] mMemberTypeObj = null;

            String mMemberTypeId = "";
            String mMemberTypeName = "";

            Query qMemberAddress = null;
            Object[] mAddressObj = null;
            Object[] pAddressObj = null;

            Query memberLifeSQL = null;
            Object[] memberLifeObj = null;
            String memberLifeStatus = "";
            String memberLifeStatusText = "";
            String strMsg = "";

            InetAddress ip;
            String hostname = "";
            String ipAddress = "";

            try {
                ip = InetAddress.getLocalHost();
                hostname = ip.getHostName();
                ipAddress = ip.getHostAddress();

            } catch (UnknownHostException e) {

                e.printStackTrace();
            }

            qMember = dbsession.createQuery(" from Member where mobile = '" + mobileNumber1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();

                    memId = member.getId();
                    memberName = member.getMemberName();
                    fatherName = member.getFatherName() == null ? "" : member.getFatherName();
                    motherName = member.getMotherName() == null ? "" : member.getMotherName();

                    dob = member.getDob();
                    gender = member.getGender() == null ? "" : member.getGender().trim();

                    mobileNo = member.getMobile() == null ? "" : member.getMobile();
                    phone1 = member.getPhone1() == null ? "" : member.getPhone1();
                    phone2 = member.getPhone2() == null ? "" : member.getPhone2();
                    memberEmail = member.getEmailId() == null ? "" : member.getEmailId();
                    bloodGroup = member.getBloodGroup() == null ? "" : member.getBloodGroup();

                    unitName = member.getUnitName() == null ? "" : member.getUnitName();
                    nirbachoniAson = member.getNirbachoniAson() == null ? "" : member.getNirbachoniAson();

                    Query mOrganizationSQL = dbsession.createQuery(" from MemberOrganization WHERE member_id='" + memId + "'");

                    System.out.println("API :: forcePasswordChange API memberOrganizationSQL ::" + mOrganizationSQL);

                    if (!mOrganizationSQL.list().isEmpty()) {
                        for (Iterator mOrganizationItr = mOrganizationSQL.list().iterator(); mOrganizationItr.hasNext();) {

                            mOrganization = (MemberOrganization) mOrganizationItr.next();

                            organizationName = mOrganization.getOrganizationInfo().getOrganizationName() == null ? "" : mOrganization.getOrganizationInfo().getOrganizationName();
                            organizationLogo = mOrganization.getOrganizationInfo().getOrgLogo() == null ? "" : mOrganization.getOrganizationInfo().getOrgLogo();

                            System.out.println("API :: forcePasswordChange API organizationName ::" + organizationName);
                            System.out.println("API :: forcePasswordChange API organizationLogo ::" + organizationLogo);

                            organizationLogoLink = GlobalVariable.imageDirLink + "company/" + organizationLogo;
                        }
                    }

                    pictureName = member.getPictureName() == null ? "" : member.getPictureName();

                    pictureLink = GlobalVariable.imageMemberDirLink + pictureName;

                    mMemberTypeSQL = dbsession.createSQLQuery("SELECT mti.member_type_id,mti.member_type_name from member_type mt, member_type_info mti WHERE mt.member_type_id = mti.member_type_id AND mt.member_id='" + memId + "' AND SYSDATE() between mt.ed and mt.td");

                    for (Iterator mMemberTypeItr = mMemberTypeSQL.list().iterator(); mMemberTypeItr.hasNext();) {

                        mMemberTypeObj = (Object[]) mMemberTypeItr.next();
                        mMemberTypeId = mMemberTypeObj[0].toString();
                        mMemberTypeName = mMemberTypeObj[1].toString();

                    }
                    Query memberAddressSQL = dbsession.createQuery(" from MemberAddress WHERE member_id='" + memId + "'");

                    if (!memberAddressSQL.list().isEmpty()) {
                        for (Iterator memberAddressItr = memberAddressSQL.list().iterator(); memberAddressItr.hasNext();) {
                            memberAddress = (MemberAddress) memberAddressItr.next();

                            memberAddressId = memberAddress.getId();
                            memberAddressType = memberAddress.getAddressType();

                            if (memberAddressType.equals("M")) {
                                mAddressLine1 = memberAddress.getAddressBook().getAddress1() == null ? "" : memberAddress.getAddressBook().getAddress1();
                                mAddressLine2 = memberAddress.getAddressBook().getAddress2() == null ? "" : memberAddress.getAddressBook().getAddress2();

                                mZipCode = memberAddress.getAddressBook().getZipcode() == null ? "" : memberAddress.getAddressBook().getZipcode();

                            }
                            if (memberAddressType.equals("P")) {
                                pAddressLine1 = memberAddress.getAddressBook().getAddress1() == null ? "" : memberAddress.getAddressBook().getAddress1();
                                pAddressLine2 = memberAddress.getAddressBook().getAddress2() == null ? "" : memberAddress.getAddressBook().getAddress2();

                                pZipCode = memberAddress.getAddressBook().getZipcode() == null ? "" : memberAddress.getAddressBook().getZipcode();

                            }
                        }

                    }
                    if (givenPassword.equalsIgnoreCase(givenPassword1)) {
                        givenPasswordEnc = encryption.getEncrypt(givenPassword);

                        //update member_credential table member_key and status
                        //update member status
                        Query mCredentialUpdate = dbsession.createSQLQuery("UPDATE  member_credential SET member_key ='" + givenPasswordEnc + "',update_time=now(),status= '1' WHERE member_id='" + memId + "'");
                        mCredentialUpdate.executeUpdate();

                        Query mMemberUpdate = dbsession.createSQLQuery("UPDATE  member SET status ='1',mod_date=now(),mod_term='" + hostname + "',mod_ip= '" + ipAddress + "' WHERE id='" + memId + "'");
                        mMemberUpdate.executeUpdate();

                        dbtrx.commit();

                        if (dbtrx.wasCommitted()) {
                            dbsession.flush();
                            dbsession.close();
                            response.sendRedirect(GlobalVariable.baseUrl + "/member/login.jsp");
                        } else {
                            //Password not updated
                            strMsg = "There was a problem updating your password. Try Again later!";
                            session.setAttribute("strMsg", strMsg);
                            dbsession.flush();
                            dbsession.close();
                            response.sendRedirect(GlobalVariable.baseUrl + "/member/newPassword.jsp?username=" + mobileNumber1);
                        }
                    } else {
                        //Passwords Don't match
                        strMsg = "Passwords did not match!";
                        session.setAttribute("strMsg", strMsg);
                        dbsession.flush();
                        dbsession.close();
                        response.sendRedirect(GlobalVariable.baseUrl + "/member/newPassword.jsp?username=" + mobileNumber1);
                    }
                }
            } else {
                //Member Not found
                strMsg = "There was a problem!";
                session.setAttribute("strMsg", strMsg);
                dbsession.flush();
                dbsession.close();
                response.sendRedirect(GlobalVariable.baseUrl + "/member/newPassword.jsp?username=" + mobileNumber1);
            }

        } catch (Exception theException) {
            System.out.println(theException);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
