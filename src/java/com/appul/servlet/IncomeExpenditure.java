package com.appul.servlet;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.appul.util.HibernateUtil;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

/**
 *
 * @author Akter
 */
public class IncomeExpenditure {

    public static Logger logger = Logger.getLogger("IncomeExpenditure.class");
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();

    java.sql.Statement stmt = null;
    String comName = null;
    String address = null;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    String daFormate = this.df.format(new Date());
    String sDate = "";
    java.text.DecimalFormat decf = new java.text.DecimalFormat("##,##0.00");
    String trPeriod = "";
    String startDate = "";
    String endDate = "";
    int year = 0;
    int fYear = 0;
    int month = 0;
    double excess = 0.0D;

    public int MakeIncomeExpenditure(String sDate, String eDate) throws SQLException {
        int ret = 0;
//
//        if (!eDate.isEmpty()) {
//            this.year = Integer.parseInt(eDate.substring(0, 4));
//            this.month = Integer.parseInt(eDate.substring(5, 7));
//            if (this.trPeriod.equals("1")) {
//                this.startDate = (this.year + "-01-01");
//                this.endDate = (this.year + "-12-31");
//            } else if (this.month >= 7) {
//                this.startDate = (this.year + "-07-01");
//                int startYear = this.year + 1;
//                this.endDate = (startYear + "-06-30");
//            } else {
//                int startYear = this.year - 1;
//                this.startDate = (startYear + "-07-01");
//                this.endDate = (this.year + "-06-30");
//            }
//        }

        this.startDate = sDate;
        this.endDate = eDate;
        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        System.out.println(this.startDate);
        System.out.println(this.endDate);
        try {
//            this.con = dbconn.dbsrc();
            this.stmt = this.con.createStatement();
            this.stmt.executeUpdate("TRUNCATE TABLE temp_incom_expenditure");

            int count = 0;
            int check = 0;
            String cksLevel = "";
            String ckfLevel = "";
            double incomVal = 0.0D;
            double totalIncom = 0.0D;
            double incomeVal = 0.0D;
            double totalIncome = 0.0D;
            double totalexcess = 0.0D;
            String incomQuery = "SELECT acc_flevel.flevel_name, acc_slevel.slevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (   acc_slevel INNER JOIN acc_plevel ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE (acc_chi2_type.acc_c2name = 'Income') AND(voucher_child.voucher_date BETWEEN to_date( '" + this.startDate + "','YYYY-MM-DD')  AND  to_date( '" + this.endDate + "','YYYY-MM-DD') ) AND(voucher_child.del_or_up = 0) GROUP BY acc_flevel.flevel_name, acc_slevel.slevel_name";

            java.sql.ResultSet rsCountIn = this.stmt.executeQuery(incomQuery);
            while (rsCountIn.next()) {
                count = rsCountIn.getRow();
            }
            String[] fLevel = new String[count];
            String[] sLevel = new String[count];
            java.sql.ResultSet rsValue = this.stmt.executeQuery(incomQuery);
            int k = 0;
            while (rsValue.next()) {
                fLevel[k] = rsValue.getString("flevel_name");
                sLevel[k] = rsValue.getString("slevel_name");
                k++;
            }
            for (int i = 0; i < count; i++) {
                String flevel = fLevel[i];
                String slevel = sLevel[i];
                if (!ckfLevel.equals(flevel)) {
                    this.stmt.executeUpdate("insert into temp_incom_expenditure values('" + flevel + "','','','','','')");
                    ckfLevel = flevel;
                }
                String l2NameQuer = "SELECT acc_slevel.slevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (    acc_slevel INNER JOIN acc_plevel ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE (acc_chi2_type.acc_c2name = 'Income') AND(acc_slevel.slevel_name = '" + slevel + "')" + " AND(voucher_child.voucher_date <= to_date( '" + this.endDate + "','YYYY-MM-DD')) AND(voucher_child.del_or_up = 0)  GROUP BY acc_slevel.slevel_name";

                java.sql.ResultSet rsL2Name = this.stmt.executeQuery(l2NameQuer);
                while (rsL2Name.next()) {
                    incomVal = rsL2Name.getDouble("amount");
                    totalIncom += incomVal;
                }

                if (incomVal < 0.0D) {
                    incomeVal = incomVal * -1.0D;
                    this.stmt.executeUpdate("insert into temp_incom_expenditure values('','" + slevel + "','','" + this.decf.format(incomeVal) + "','','')");
                } else {
                    this.stmt.executeUpdate("insert into temp_incom_expenditure values('','" + slevel + "','','" + this.decf.format(incomVal) + "','','')");
                }
                check = 1;
            }
            if (check == 1) {
                if (totalIncom < 0.0D) {
                    totalIncome = totalIncom * -1.0D;
                    this.stmt.executeUpdate("insert into temp_incom_expenditure values('','','','','" + this.decf.format(totalIncome) + "','')");
                } else {
                    this.stmt.executeUpdate("insert into temp_incom_expenditure values('','','','','" + this.decf.format(totalIncom) + "','')");
                }
                check = 0;
            }

            int count1 = 0;
            int check1 = 0;
            String cksLevelEx = "";
            String ckfLevelEx = "";
            double expVal = 0.0D;
            double totalExp = 0.0D;
            double expTotal = 0.0D;
            double totalExpense = 0.0D;
            String expQuery = "SELECT acc_flevel.flevel_name, acc_slevel.slevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (   acc_slevel INNER JOIN acc_plevel ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE (acc_chi2_type.acc_c2name = 'Expenditures') AND(voucher_child.voucher_date <=  to_date( '" + this.endDate + "','YYYY-MM-DD')) AND(voucher_child.del_or_up = 0) GROUP BY acc_flevel.flevel_name, acc_slevel.slevel_name";

            java.sql.ResultSet rsCountEx = this.stmt.executeQuery(expQuery);
            while (rsCountEx.next()) {
                count1 = rsCountEx.getRow();
            }
            String[] fLevelEx = new String[count1];
            String[] sLevelEx = new String[count1];
            java.sql.ResultSet rsValueEx = this.stmt.executeQuery(expQuery);
            int a = 0;
            while (rsValueEx.next()) {
                fLevelEx[a] = rsValueEx.getString("flevel_name");
                sLevelEx[a] = rsValueEx.getString("slevel_name");
                a++;
            }
            for (int i = 0; i < count1; i++) {
                String flevelEx = fLevelEx[i];
                String slevelEx = sLevelEx[i];
                if (!ckfLevelEx.equals(flevelEx)) {
                    this.stmt.executeUpdate("insert into temp_incom_expenditure values('" + flevelEx + "','','','','','')");
                    ckfLevelEx = flevelEx;
                }
                String l2NameQuer = "SELECT acc_slevel.slevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (    acc_slevel INNER JOIN acc_plevel ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE (acc_chi2_type.acc_c2name = 'Expenditures') AND(acc_slevel.slevel_name = '" + slevelEx + "')" + " AND(voucher_child.voucher_date <=to_date( '" + this.endDate + "','YYYY-MM-DD')) AND(voucher_child.del_or_up = 0)" + " GROUP BY acc_slevel.slevel_name";

                java.sql.ResultSet rsL2Name = this.stmt.executeQuery(l2NameQuer);
                while (rsL2Name.next()) {
                    expVal = rsL2Name.getDouble("amount");
                    totalExp += expVal;
                }

                if (expVal < 0.0D) {
                    expTotal = expVal * -1.0D;
                    this.stmt.executeUpdate("insert into temp_incom_expenditure values('','" + slevelEx + "','','" + this.decf.format(expTotal) + "','','')");
                } else {
                    this.stmt.executeUpdate("insert into temp_incom_expenditure values('','" + slevelEx + "','','" + this.decf.format(expVal) + "','','')");
                }
                check1 = 1;
            }
            if (check1 == 1) {
                if (totalExp < 0.0D) {
                    totalExpense = totalExp * -1.0D;
                    this.stmt.executeUpdate("insert into temp_incom_expenditure values('','','','','" + this.decf.format(totalExpense) + "','')");
                } else {
                    this.stmt.executeUpdate("insert into temp_incom_expenditure values('','','','','" + this.decf.format(totalExp) + "','')");
                }
                check1 = 0;
            }
            this.excess = (totalIncom + totalExp);
            if (this.excess < 0.0D) {
                totalexcess = this.excess * -1.0D;
                this.stmt.executeUpdate("insert into temp_incom_expenditure values('Excess/(Deficit)','','','','','" + this.decf.format(totalexcess) + "')");
            } else {
                this.stmt.executeUpdate("insert into temp_incom_expenditure values('Excess/(Deficit)','','','','','" + this.decf.format(this.excess) + "')");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            ret = 0;
        } finally {
            this.con.commit();
            this.con.close();
            ret = 1;
            dbsession.flush();
            dbtrx.commit();
            dbsession.close();
            logger.info("Income & Expenditure Preparation Completed");
            System.out.println("Income & Expenditure Preparation Completed");
        }

        return ret;
    }

}
