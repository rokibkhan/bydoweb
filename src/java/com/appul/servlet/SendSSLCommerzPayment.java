/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.GlobalVariable;
import com.ssl.commerz.Utility.ParameterBuilder;
import org.apache.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.client.utils.URIBuilder;

/**
 *
 * @author Akter
 */
public class SendSSLCommerzPayment {

    public static Logger logger = Logger.getLogger("SendSMS");
    private static String USER_AGENT = "Mozilla/5.0";

    @SuppressWarnings("empty-statement")
    public static String sendSSLCommerzPaymentRequest(String orderId, String orderAmount) throws IOException, URISyntaxException {

        String output = "";
//        String urlString = "http://www.bangladeshsms.com/smsapi?api_key=C20013475bbdd6cbdd2ac1.58430946&type=text&senderid=8804445629730&msg=test&contacts=+8801755569701";
        String sendUrlHost = GlobalVariable.sendUrlHost;
        String sendUrlApi = GlobalVariable.sendUrlApi;
        String apiKey = GlobalVariable.apiKey;
        String type = GlobalVariable.type;
        String senderid = GlobalVariable.senderid;
        String ret = "";

        String storeId = "ieb5c9c8b9a864bf";
        String storePass = "ieb5c9c8b9a864bf@ssl";

        //   String trxId = request.get("tran_id");
        String trxId = orderId;
        /**
         * Get your AMOUNT and Currency FROM DB to initiate this Transaction
         */
        String amount = orderAmount;
        String currency = "BDT";
        // define("SSLCZ_STORE_ID", "ieb5c9c8b9a864bf");
//define("SSLCZ_STORE_PASSWD", "ieb5c9c8b9a864bf@ssl");

        String sslczURL = "https://sandbox.sslcommerz.com/";
        //  String sslczURL = "https://securepay.sslcommerz.com/";
        String submitURL = "gwprocess/v3/api.php";
        String validationURL = "validator/api/validationserverAPI.php";
        String checkingURL = "validator/api/merchantTransIDvalidationAPI.php";

        String sendUrlHost1 = "www.sandbox.sslcommerz.com";
        String sendUrlApi1 = "/gwprocess/v3/api.php";

        /**
         * All parameters in payment order should be constructed in this
         * follwing postData Map keep an eye on success fail url correctly.
         * insert your success and fail URL correctly in this Map
         */
        Map<String, String> postData = ParameterBuilder.constructRequestParameters();

        
       
        
        postData.put("store_id", storeId);
        postData.put("store_passwd", storePass);
        
         System.out.println("\n postData : " + postData);

        try {

            URIBuilder builder = new URIBuilder()
                    .setScheme("https")
                    .setHost(sendUrlHost1)
                    .setPath(sendUrlApi1);
//                    .addParameter("api_key", apiKey)
//                    .addParameter("contacts", receipentMobileNo)
//                    .addParameter("senderid", senderid)
//                    .addParameter("type", type)
//                    .addParameter("msg", msg);

            URL url = builder.build().toURL();

            System.out.println("\nSending 'GET' request to URL : " + url);
            //    System.out.println("Response Code : " + responseCode);

            try {

                String urlParameters = ParameterBuilder.getParamsString(postData, true);
                byte[] postDataBytes = urlParameters.getBytes();
                int postDataLength = postDataBytes.length;

                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setConnectTimeout(5000);
                con.setReadTimeout(5000);

                con.setInstanceFollowRedirects(false);
                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                con.setRequestProperty("charset", "utf-8");
                con.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                con.setUseCaches(false);
                con.setDoOutput(true);
                con.getOutputStream().write(postDataBytes);

                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String outputLine;
                while ((outputLine = br.readLine()) != null) {
                    output = output + outputLine;
                }
                br.close();
                return output;

            } catch (java.net.SocketTimeoutException e) {
                return "Exception or server unreachable";
            } catch (java.io.IOException e) {
                return "Exception or server unreachable";
            }

        } catch (IOException | URISyntaxException e) {
            return "Exception or server unreachable";
        }

    }

    public static String getParamsString(Map<String, String> params, boolean urlEncode) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (urlEncode) {
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            } else {
                result.append(entry.getKey());
            }

            result.append("=");
            if (urlEncode) {
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            } else {
                result.append(entry.getValue());
            }
            result.append("&");
        }

        String resultString = result.toString();
        return resultString.length() > 0
                ? resultString.substring(0, resultString.length() - 1)
                : resultString;
    }

    public static Map<String, String> constructRequestParameters() {
        // CREATING LIST OF POST DATA
        String baseUrl = "http://localhost:9000/";//Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
        Map<String, String> postData = new HashMap<String, String>();
        postData.put("total_amount", "150.00");
        postData.put("tran_id", "TESTASPNET1234");
        postData.put("success_url", baseUrl + "ssl-success-page");
        postData.put("fail_url", "https://sandbox.sslcommerz.com/developer/fail.php");
        postData.put("cancel_url", "https://sandbox.sslcommerz.com/developer/cancel.php");
        postData.put("version", "3.00");
        postData.put("cus_name", "ABC XY");
        postData.put("cus_email", "abc.xyz@mail.co");
        postData.put("cus_add1", "Address Line On");
        postData.put("cus_add2", "Address Line Tw");
        postData.put("cus_city", "City Nam");
        postData.put("cus_state", "State Nam");
        postData.put("cus_postcode", "Post Cod");
        postData.put("cus_country", "Countr");
        postData.put("cus_phone", "0111111111");
        postData.put("cus_fax", "0171111111");
        postData.put("ship_name", "ABC XY");
        postData.put("ship_add1", "Address Line On");
        postData.put("ship_add2", "Address Line Tw");
        postData.put("ship_city", "City Nam");
        postData.put("ship_state", "State Nam");
        postData.put("ship_postcode", "Post Cod");
        postData.put("ship_country", "Countr");
        postData.put("value_a", "ref00");
        postData.put("value_b", "ref00");
        postData.put("value_c", "ref00");
        postData.put("value_d", "ref00");
        return postData;
    }

//    public static void main(String args[]) throws IOException, URISyntaxException {
//        String sms = "This is test Message";
//        String receipentMobileNo = "+8801955538279";
//        System.out.println("SMS : " + sms);
//        System.out.println("receipentMobileNo : " + receipentMobileNo);
//        System.out.println(SendSMS(sms, receipentMobileNo));
//    }
}
