/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.entity.Member;
import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.RandomString;
import com.appul.util.getRegistryID;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Zeeon
 */
@WebServlet(name = "OtpVerification", urlPatterns = {"/OtpVerification"})
public class OtpVerification extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            Session dbsession = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Transaction dbtrx = null;
            dbtrx = dbsession.beginTransaction();
            HttpSession session = request.getSession(true);

            getRegistryID getId = new getRegistryID();

            DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
            Date dateToday = new Date();


            String mobileNumber = "";
            String verifyCode = "";

            String memberId = "";
            String givenPassword = "";

            String adddate = "";

            mobileNumber = request.getParameter("username").trim();
            verifyCode = request.getParameter("otp").trim();
            String firstLetter = mobileNumber.substring(0, 1);
            String adjustLettter1 = "+88";
            String adjustLettter2 = "+";

            String mobileNumber1 = "";

            if (firstLetter.equals("0")) {
                mobileNumber1 = adjustLettter1 + mobileNumber;
                System.out.println("firstLetter :: 0 :: mobileNumber1 ::" + mobileNumber1);
            }
            if (firstLetter.equals("8")) {
                mobileNumber1 = adjustLettter2 + mobileNumber;
                System.out.println("firstLetter :: 8 :: mobileNumber1 ::" + mobileNumber1);
            }
            if (firstLetter.equals("+")) {
                mobileNumber1 = mobileNumber;
                System.out.println("firstLetter :: + :: mobileNumber1 ::" + mobileNumber1);

            }

            String strMsg = "";

            Member member = null;
            int memId = 0;
            String dbPass = "";
            Encryption encryption = new Encryption();

            Query qMember = null;
            Query memberKeySQL = null;

            qMember = dbsession.createQuery(" from Member where mobile = '" + mobileNumber1 + "'");
            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();

                    memId = member.getId();

                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();
                    System.out.println("API :: signupVerification API dbPass ::" + dbPass);
                    System.out.println("API :: signupVerification API verifyCode ::" + encryption.getEncrypt(verifyCode));
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(verifyCode))) {
                        dbsession.flush();
                        dbsession.close();
                        response.sendRedirect(GlobalVariable.baseUrl + "/member/newPassword.jsp?username=" + mobileNumber1);
                    } else {
                        //Verification failed
                        strMsg = "Verification Code Wrong";
                        session.setAttribute("strMsg", strMsg);
                        dbsession.flush();
                        dbsession.close();
                        response.sendRedirect(GlobalVariable.baseUrl + "/member/otpVerification.jsp?username=" + mobileNumber1);

                    }
                }
            } else {
                //Member not found,Something wrong
                strMsg = "Something went wrong, try again later";
                session.setAttribute("strMsg", strMsg);
                dbsession.flush();
                dbsession.close();
                response.sendRedirect(GlobalVariable.baseUrl + "/member/otpVerification.jsp?username=" + mobileNumber1);
            }

        } catch (Exception theException) {
            theException.printStackTrace();
            System.out.println(theException);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
